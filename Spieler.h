#ifndef SPIELER
#define SPIELER


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "System.hpp"
#include "Legacy.hpp"
#include "InterfaceValidateEntryVector.hpp"
#include "InterfaceMessageBadgeVector.hpp"
#include "UserInput.hpp"
#include "OperationDisplayDriver.hpp"
#include "XMLProperties.h"
extern "C" {
#include "User.h"
}

using namespace std;

// extern the LegactSetup object that implement the setup data structure this defined in Operation.cpp file.
 extern LegacySetup SystemSetup;


class Spieler
{

  public:
	 Spieler(){printf("spieler constructor\n");};
	~Spieler(){};
    char useProxy[8],proxy[500],proxyPort[10],useProxyAuth[8],proxyUserName[50],proxyPassword[50];
	char timeOut[6];
	bool useProxyBool;
	bool useProxyAuthBool;
	bool useSoapActionHeader;
    //static bool communication;
	void initSetupSystemDataStructure(void);
	void SetupSystem(void);
    void ParseAndDisplayResponse(OperationDisplayDriver * oper,char* response,char* defaultMessage);

	void RunHeartBeatThread(); // create the heartbeat thread
	static void * RunHeartBeatFunction(void* id);
	// create the thread that responsible to get the prject tasks and update the DB table

	static void DispalyIdentifyFpuError(OperationDisplayDriver * oper,UF_BYTE Error);
private:

public:

};

#endif
