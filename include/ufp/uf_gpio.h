/**
 *  	Definitions of GPIO related constants
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */

 /*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */
 

 #ifndef __UNIFINGERGPIO_H__
#define __UNIFINGERGPIO_H__

#define UF_GPIO_SUP_DATA_LEN	8

 typedef enum {
 	UF_GPIO_0 = 0,
 	UF_GPIO_1 = 1,
 	UF_GPIO_2 = 2,
 	UF_GPIO_3 = 3,
 	UF_GPIO_4 = 4,
 	UF_GPIO_5 = 5,
 	UF_GPIO_6 = 6,
 	UF_GPIO_7 = 7,
} UF_GPIO_INDEX;

typedef enum {
	UF_GPIO_DISABLE 			= 0,
	UF_GPIO_INPUT				= 1,
	UF_GPIO_OUTPUT			= 2,	
	UF_GPIO_SHARED_IO			= 3,
	UF_GPIO_WIEGAND_INPUT	= 4,
	UF_GPIO_WIEGAND_OUTPUT	= 5,
} UF_GPIO_MODE;

typedef enum {
	UF_GPIO_IN_ENROLL						= 0x01,
	UF_GPIO_IN_IDENTIFY					= 0x02,			
	UF_GPIO_IN_DELETE_ALL					= 0x03,
	UF_GPIO_IN_DELETE_ALL_BY_CONFIRM	= 0x04,
} UF_GPIO_INPUT_FUNC;

typedef enum {
	UF_GPIO_IN_ACTIVE_HIGH	= 0x01,
	UF_GPIO_IN_ACTIVE_LOW	= 0x02,
	UF_GPIO_IN_RISING_EDGE	= 0x03,
	UF_GPIO_IN_FALLING_EDGE	= 0x04,
} UF_GPIO_INPUT_ACTIVATION;

typedef enum {
	UF_GPIO_OUT_ENROLL_WAIT_FINGER	= 0x81,
	UF_GPIO_OUT_ENROLL_PROCESSING	= 0x82,
	UF_GPIO_OUT_ENROLL_SUCCESS		= 0x83,
	UF_GPIO_OUT_ENROLL_FAIL			= 0x84,
	UF_GPIO_OUT_MATCH_WAIT_FINGER	= 0x85,
	UF_GPIO_OUT_MATCH_PROCESSING	= 0x86,
	UF_GPIO_OUT_MATCH_SUCCESS		= 0x87,
	UF_GPIO_OUT_MATCH_FAIL			= 0x88,
	UF_GPIO_OUT_DELETE_WAIT			= 0x89,
	UF_GPIO_OUT_DELETE_PROCESSING	= 0x8a,
	UF_GPIO_OUT_DELETE_SUCCESS		= 0x8b,
	UF_GPIO_OUT_DELETE_FAIL			= 0x8c,
	UF_GPIO_OUT_BEEP					= 0x8d
} UF_GPIO_OUTPUT_FUNC;

typedef enum {
	UF_GPIO_OUT_ACTIVE_HIGH	= 0x82,
	UF_GPIO_OUT_ACTIVE_LOW	= 0x84,
	UF_GPIO_OUT_HIGH_BLINK	= 0x83,
	UF_GPIO_OUT_LOW_BLINK	= 0x85,
} UF_GPIO_OUTPUT_LEVEL;
#endif
