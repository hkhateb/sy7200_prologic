/**
 *  	Definitions of system parameters
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */



/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

#ifndef __UNIFINGERSYSPARAMETER_H__
#define __UNIFINGERSYSPARAMETER_H__

#include "uf_type.h"

 typedef enum {
 	UF_SYS_TIMEOUT				= 0x62,
	UF_SYS_ENROLL_MODE			= 0x65,
	UF_SYS_SECURITY_LEVEL		= 0x66,
	UF_SYS_ENCRYPTION_MODE		= 0x67,
	UF_SYS_SENSOR_TYPE			= 0x68,
	UF_SYS_IMAGE_FORMAT			= 0x6c,
	UF_SYS_MODULE_ID			= 0x6d,
	UF_SYS_FIRMWARE_VERSION	= 0x6e,
	UF_SYS_SERIAL_NUMBER		= 0x6f,
	UF_SYS_BAUDRATE				= 0x71,
	UF_SYS_AUX_BAUDRATE			= 0x72,
	UF_SYS_ENROLLED_FINGER		= 0x73,
	UF_SYS_AVAILABLE_FINTER		= 0x74,
	UF_SYS_SEND_SCAN_SUCCESS	= 0x75,
	UF_SYS_ASCII_PACKET			= 0x76,
	UF_SYS_ROTATE_IMAGE			= 0x77,
	UF_SYS_SENSITIVIY			= 0x80,
	UF_SYS_IMAGE_QUALITY		= 0x81,
	UF_SYS_AUTO_RESPONSE		= 0x82,
	UF_SYS_NETWORK_MODE			= 0x83,
	UF_SYS_FREE_SCAN			= 0x84,
	UF_SYS_PROVISIONAL_ENROLL	= 0x85,
	UF_SYS_PASS_WHEN_EMPTY		= 0x86,
	UF_SYS_RESPONSE_DELAY		= 0x87,
	UF_SYS_MATCHING_TIMEOUT	= 0x88,
	UF_SYS_BUILD_NO				= 0x89,
	UF_SYS_ENROLL_DISPLACEMENT	= 0x8a,
	UF_SYS_TEMPLATE_SIZE 		= 0x64,
	UF_SYS_ROTATION				= 0x78,
} UF_SYS_PARAM;


typedef enum {
	UF_BAUD9600		= 0x31,
	UF_BAUD19200		= 0x32,
	UF_BAUD38400		= 0x33,
	UF_BAUD57600		= 0x34,
	UF_BAUD115200		= 0x35,
} UF_BAUDRATE;



#endif
