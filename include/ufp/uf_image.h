/**
 *  	Definitions of imagel related constants
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

 #ifndef __UNIFINGERIMAGE_H__
#define __UNIFINGERIMAGE_H__

#include "uf_type.h"

#define UF_IMAGE_HEADER_SIZE	7


typedef enum {
	UF_GRAY_IMAGE			= 0x30,
	UF_BINARY_IMAGE		= 0x31,
	UF_4BIT_GRAY_IMAGE	= 0x32,
} UF_IMAGE_TYPE;

typedef struct {
	UF_INT32	width;
	UF_INT32	height;
	UF_INT32	compressed;
	UF_INT32	encrypted;
	UF_INT32	format;
	UF_INT32	img_len;
	UF_INT32	template_len;
	UF_BYTE		buffer[200*1024]; // max size is 200K
} uf_image_t;

int uf_get_imsage_size( UF_BYTE* image_data );
#endif

