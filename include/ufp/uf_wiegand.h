/**
 *  	Definitions of GPIO related constants
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

#ifndef __UNIFINGERWIEGAND_H__
#define __UNIFINGERWIEGAND_H__

#define UF_WIEGAND_FC_BITS	0x08

typedef enum {
	UF_WIEGAND_NO_ACTION			= 0x00,
	UF_WIEGAND_ENROLL				= 0x12,
	UF_WIEGAND_VERIFY				= 0x22,
	UF_WIEGAND_DELETE					= 0x42,
	UF_WIEGAND_OUTPUT_MATCHED_ID	= 0x5a,
} UF_WIEGAND_FUNCTION;


typedef enum {
	UF_WIEGAND_INPUT		= 0x00,
	UF_WIEGAND_OUTPUT	= 0x01,
} UF_WIEGAND_PORT;

typedef enum {
	UF_WIEGAND_CLEAR		= 0x00,
	UF_WIEGAND_REMAIN	= 0x01,
} UF_WIEGAND_GET_INPUT_OPTION;
#endif

