#include	"wsIntrn.h"

static void		formSetTimeDate(webs_t wp, char_t *path, char_t *query);

void formDefineDateTime(void)
{

    websFormDefine(T("SetTimeDate"), formSetTimeDate);
}


static void		formSetTimeDate(webs_t wp, char_t *path, char_t *query);
{
    char_t	*month, *day, *year, *hour, *min, *sec,*set;
  
    bool_t bDisable;
    int	nCheck;

	a_assert(wp);

	month = websGetVar(wp, T("month"), T("")); 
	day = websGetVar(wp, T("day"), T("")); 
	year = websGetVar(wp, T("year"), T("")); 
	hour = websGetVar(wp, T("hour"), T("")); 
	min = websGetVar(wp, T("min"), T("")); 
	sec = websGetVar(wp, T("sec"), T("")); 
    set = websGetVar(wp, T("set"), T("")); 
    
    websHeader(wp);
	websMsgStart(wp);
    
    printf("mon=%s day=%s year=%s hour=%s min=%s sec=%s",month,day,year,hour,min,sec);
    
    websMsgEnd(wp);
	websFooter(wp);
	websDone(wp, 200);
}