// have a nice day. changing this file does not force make to compile??????????
#include	"wsIntrn.h"

#define		NONE_OPTION		T("<NONE>")
#define		MSG_START		T("<body><h2>")
#define		MSG_END			T("</h2></body>")

static void		formSetTimeDate(webs_t wp, char_t *path, char_t *query);
static void		websMsgStart(webs_t wp);
static void		websMsgEnd(webs_t wp);

// get the current date and time From The Clock,
// not from javascript, which will retrieve the browsers date and time
static void GetTimeAsString(char * buffer);
static int GetSeconds();
static int GetMinutes();
static int GetHour();
static int GetDayOfWeek();
static int GetDayOfMonth();
static int GetMonth();
static int GetYear();


static int		aspGetClockTimeHour(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetClockTimeMinute(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetClockTimeSeconds(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetClockDateMonth(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetClockDateDay(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetClockDateYear(int eid, webs_t wp, int argc, char_t **argv);


void formDefineDateTime(void)
{

    websFormDefine(T("SetTimeDate"), formSetTimeDate);

	websAspDefine(T("GetClockTimeHour"), aspGetClockTimeHour);
	websAspDefine(T("GetClockTimeMinute"), aspGetClockTimeMinute);
	websAspDefine(T("GetClockTimeSeconds"), aspGetClockTimeSeconds);
	websAspDefine(T("GetClockDateMonth"), aspGetClockDateMonth);
	websAspDefine(T("GetClockDateDay"), aspGetClockDateDay);
	websAspDefine(T("GetClockDateYear"), aspGetClockDateYear);
}


static void		formSetTimeDate(webs_t wp, char_t *path, char_t *query)
{
    char_t	*month, *day, *year, *hour, *min, *sec,*set;

    char buffer[20] = "date " ;
    int res1,res2;
    a_assert(wp);
	month = websGetVar(wp, T("month"), T(""));
	day = websGetVar(wp, T("day"), T(""));
	year = websGetVar(wp, T("year"), T(""));
	hour = websGetVar(wp, T("hour"), T(""));
	min = websGetVar(wp, T("min"), T(""));
	sec = websGetVar(wp, T("sec"), T(""));
    set = websGetVar(wp, T("set"), T(""));

    websHeader(wp);
	websMsgStart(wp);

    //printf("mon=%s day=%s year=%s hour=%s min=%s sec=%s",month,day,year,hour,min,sec);
    //printf("we are here\n");


    strcat(buffer,month);
    strcat(buffer,day);
    strcat(buffer,hour);
    strcat(buffer,min);
    strcat(buffer,year);strcat(buffer,".");
    strcat(buffer,sec);

#ifdef WIN32
    res1 = 0;
    res2 =  0;
#else
    res1 = system(buffer);
    res2 =  system("hwclock -w");
#endif
    if( res1 == 0 && res2 == 0 )
        websWrite(wp, T("Date & Time Updated Successfully"));
    else
        websWrite(wp, T("Failed To Update Time & Date  "));
    websMsgEnd(wp);
	websFooter(wp);
	websDone(wp, 200);
}

static int aspGetClockDateMonth(int eid, webs_t wp, int argc, char_t **argv)
{
    //char* clockdate = NULL;
	char clockdate[3];

    int	  nBytes;


	int theMonth = GetMonth();
    a_assert(wp);
	sprintf(clockdate,"%2d", theMonth);
	if(!clockdate || strcmp(clockdate,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		return nBytes;
	}
	if(clockdate) nBytes = websWrite(wp,T(clockdate));
	return nBytes;
}

static int aspGetClockDateDay(int eid, webs_t wp, int argc, char_t **argv)
{
    //char* clockdate = NULL;
	char clockdate[3];


	int	  nBytes;


	int theDay = GetDayOfMonth();

	a_assert(wp);

	sprintf(clockdate,"%2d", theDay);

	if(!clockdate || strcmp(clockdate,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		return nBytes;
	}
	if(clockdate) nBytes = websWrite(wp,T(clockdate));
	return nBytes;
}

static int aspGetClockDateYear(int eid, webs_t wp, int argc, char_t **argv)
{
    //char* clockdate = NULL;
	char clockdate[5];

    int	  nBytes;


	int theYear = GetYear();
    a_assert(wp);
	sprintf(clockdate,"%4d", theYear);

	if(!clockdate || strcmp(clockdate,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		return nBytes;
	}
	if(clockdate) nBytes = websWrite(wp,T(clockdate));
	return nBytes;
}


static int aspGetClockTimeHour(int eid, webs_t wp, int argc, char_t **argv)
{
    //char* clocktime = NULL;
	char clocktime[3];

    int	  nBytes;


	int theHour = GetHour();
    a_assert(wp);
	sprintf(clocktime,"%2d", theHour);

	if(!clocktime || strcmp(clocktime,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		return nBytes;
	}
	if(clocktime) nBytes = websWrite(wp,T(clocktime));
	return nBytes;
}

static int aspGetClockTimeMinute(int eid, webs_t wp, int argc, char_t **argv)
{
    //char* clocktime = NULL;
	char clocktime[3];

    int	  nBytes;


	int theMinute = GetMinutes();
    a_assert(wp);
	sprintf(clocktime,"%2d", theMinute);

	if(!clocktime || strcmp(clocktime,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		return nBytes;
	}
	if(clocktime) nBytes = websWrite(wp,T(clocktime));
	return nBytes;
}


static int aspGetClockTimeSeconds(int eid, webs_t wp, int argc, char_t **argv)
{
    //char* clocktime = NULL;
	char clocktime[3];


    int	  nBytes;


	int theSeconds = GetSeconds();
    a_assert(wp);
	sprintf(clocktime,"%2d", theSeconds);

	if(!clocktime || strcmp(clocktime,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		return nBytes;
	}
	if(clocktime) nBytes = websWrite(wp,T(clocktime));
	return nBytes;
}



static void	websMsgStart(webs_t wp)
{
	websWrite(wp, MSG_START);
}

static void	websMsgEnd(webs_t wp)
{
	websWrite(wp, MSG_END);
}


///////////////////////////////////////////////////////////////////////////////

static int GetYear()
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	return optimeinfo->tm_year + 1900;
}

static int GetMonth()
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	return optimeinfo->tm_mon;
}

static int GetDayOfMonth()
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	return optimeinfo->tm_mday;
}

static int GetDayOfWeek()
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	return optimeinfo->tm_wday;
}

static int GetHour()
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	return optimeinfo->tm_hour;
}

static int GetMinutes()
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	return optimeinfo->tm_min;
}

static int GetSeconds()
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	return optimeinfo->tm_sec;
}


static void GetTimeAsString(char * buffer)
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	sprintf(buffer, "%0*d", 2, optimeinfo->tm_hour);
	buffer+=2;
	sprintf(buffer, "%0*d", 2, optimeinfo->tm_min);
	buffer+=2;
	sprintf(buffer, "%0*d", 2, optimeinfo->tm_sec);
}

// eof
