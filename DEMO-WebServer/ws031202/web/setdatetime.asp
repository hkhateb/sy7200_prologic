<html>
<!- Copyright (c) Go Ahead Software Inc., 2000-2000. All Rights Reserved. ->
<head>
<title>Set Date And Time</title>
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" href="style/normal_ws.css" type="text/css">
<% language=javascript %>
</head>

<body>
<h1>Set Time And Date</h1>
<form name="date" action=/goform/SetTimeDate onsubmit="return checkInput()" method=POST>
<script language="JavaScript">
function checkInput()
{


    if(date.year.value < 1975)
    {
        alert('Please Insert legal Year');
        date.year.focus();
        return false;
  	}

    if(date.min.value < 10 )
        date.min.value = '0' + date.min.value ;
    if(date.hour.value < 10 )
        date.hour.value = '0' + date.hour.value ;
    if(date.sec.value < 10 )
        date.sec.value = '0' + date.sec.value ;


    if(60 < date.min.value || date.min.value < 0)
    {
        alert('Please Insert legal minutes');
        date.min.focus();
        return false;
  	}
  	if (date.min.value==null||date.min.value=="")
    {
        alert('Please Insert minutes');date.min.focus();return false;
    }

    if(24 < date.hour.value || date.hour.value < 0)
    {
	   alert('Please Insert legal houres');
	   date.hour.focus();
       return false;
    }
    if (date.hour.value==null||date.hour.value=="")
    {
        alert('Please Insert houres');date.hour.focus();return false;
    }

    if(60 < date.sec.value || date.sec.value < 0)
    {
	  alert('Please Insert legal seconds');
	   date.sec.focus();
      return false;
    }
    if (date.sec.value==null||date.sec.value=="")
    {
        alert('Please Insert sec');date.sec.focus();return false;
    }
    if(31 < date.day.value || date.day.value < 1)
	{
	  alert('Please Insert legal day');
	  date.day.focus();
	  return false;
    }
    return true;
}

function setMonth()
{
 var theInput = document.getElementById("smonth");
 theInput.selectedIndex = <%GetClockDateMonth();%>;
}  // setMonth

function setHours()
{
// var d = new Date();
// var currHour = d.getHours();
 var currHour = <%GetClockTimeHour();%>;
 theInput = document.getElementById("hh");
 theInput.value = currHour;
}




function setMin()
{
// var d = new Date();
// var currMin = d.getMinutes();
	var currMin = <%GetClockTimeMinute();%>;
 theInput = document.getElementById("mm");
 theInput.value = currMin;
}

function setSec()
{
// var d = new Date();
// var currSec = d.getSeconds();
	var currSec = <%GetClockTimeSeconds();%>;
 theInput = document.getElementById("ss");
 theInput.value = currSec;
}

</script>

<table>
<tr>
    <td>
	  <fieldset>
	  <legend>
	    DATE:
	  </legend>
	Month:
	<select name=month title="Month" id="smonth" >
	        <option value ="01">01</option>
            <option value ="02">02</option>
            <option value ="03">03</option>
            <option value ="04">04</option>
            <option value ="05">05</option>
            <option value ="06">06</option>
            <option value ="07">07</option>
            <option value ="08">08</option>
            <option value ="09">09</option>
            <option value ="10">10</option>
            <option value ="11">11</option>
            <option value ="12">12</option>
         </select>
<script>setMonth();</script>
        Day:
	<input id="dd" type="text" maxlength=2  name="day" title="Day" size=1 value=<%GetClockDateDay();%> />
         Year:
	<input id="yy" type="text" maxlength=4  name="year" title="Year" size=2 value=<%GetClockDateYear();%> />
       </fieldset>
     </td>
</tr>
<tr>
    <td>
      <fieldset>
       <legend>
	  TIME:
       </legend>
       Hour:
       <input type=text maxlength=2  name="hour" id="hh" title="hours" size=1>
       <script>
   	setHours()
       </script>
       Min:
       <input  id="mm" type=text maxlength=2  name="min" title="minuts" size=1 value="00">
       <script>
   	setMin()
       </script>
       Sec:
       <input id="ss" type=text maxlength=2  name="sec" title="seconds" size=1 value="00">
       <script>
   	setSec()
       </script>
       </fieldset>
     </td>
</tr>
<tr>
   <td>
     <input type="submit" value="SET" />
   </td>
</tr>
</table>
</form>

</body>
</html>
