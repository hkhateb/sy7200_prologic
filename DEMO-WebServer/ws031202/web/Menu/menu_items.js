// items structure
// each item is the array of one or more properties:
// [text, link, settings, subitems ...]
// use the builder to export errors free structure if you experience problems with the syntax

var MENU_ITEMS = [
	['Home','../overview.htm', {'tw' : 'view'}],
	['SY7000 Manual',null, null,
		['English','../docs/SY7000.pdf', {'tw' : 'view'}],
		['Hebrew','../docs/SY7000.pdf', {'tw' : 'view'}]
	],
	['Terminal Settings', null, null,
		['Time Setting','../setdatetime.asp', {'tw' : 'view'}],
		['Operation Mode','../clockmode.asp',{'tw' : 'view'}]
	],
	['Web Service Settings', null, null,
		['Web Service Settings','../setwebser.asp', {'tw' : 'view'}]
	],
	['Connections' ,null,null,
	    ['Proxy Server' ,'../setproxy.asp',{'tw' : 'view'}],
	    ['View Network' ,'../viewnetset.asp',{'tw' : 'view'}]
	],
	['User Management','../um.htm',{'tw' : 'view'},
	    ['Add a User','../adduser.asp', {'tw' : 'view'}],
	    ['Delete a User','../deluser.asp', {'tw' : 'view'}],
	    ['Save User Config','../savecfg.asp', {'tw' : 'view'}],
	    ['Load User Config','../loadcfg.asp', {'tw' : 'view'}]
	],
	['Finger Print','../um.htm',{'tw' : 'view'},
	    ['Enroll','../fingerprint.asp',{'tw' : 'view'},
	       ['Enroll By Scan','../fingerprint.asp',{'tw' : 'view'}],
	       ['Enroll By Template','../fingerprint.asp',{'tw' : 'view'}]
	    ],
	    ['Verification','../fingerprint.asp',{'tw' : 'view'},
	       ['Verification By Scan','../fingerprint.asp',{'tw' : 'view'}],
	       ['Verification By Template','../fingerprint.asp',{'tw' : 'view'}]
	    ],
	    ['Identification','../fingerprint.asp',{'tw' : 'view'},
	       ['Identification By Scan','../fingerprint.asp',{'tw' : 'view'}],
	       ['Identification By Template','../fingerprint.asp',{'tw' : 'view'}]
	    ],
	    ['Template Management','../fingerprint.asp',{'tw' : 'view'},
	       ['Delete Template','../fingerprint.asp',{'tw' : 'view'}],
	       ['Delete All Template','../fingerprint.asp',{'tw' : 'view'}],
	       ['Delete Template By Scan','../fingerprint.asp',{'tw' : 'view'}]
	    ]
	],
	['Terminal Reset','../um.htm',{'tw' : 'view'},
	    ['Reboot Terminal','../terminalreboot.asp',{'tw' : 'view'}],
	    ['Reset Application','../applicationreset.asp',{'tw' : 'view'}],
	    ['Factory Reset','../factoryreset.asp',{'tw' : 'view'}]
	]
];
