<html>
<!- Copyright (c) Go Ahead Software Inc., 2000-2000. All Rights Reserved. ->
<head>
<title>Set Date And Time</title>
<meta http-equiv="Pragma" content="no-cache">
<link rel="stylesheet" href="style/normal_ws.css" type="text/css">
<% language=javascript %>
<script language="JavaScript">

function EnableSoapActionHeader()
{
  document.forms[0].SoapActionHeader.disabled = !document.forms[0].SoapActionHeader.disabled;
}
</script>
</head>

<body>
<h1>Set Web Service</h1>
<form name="web" action=/goform/SetWebSer method=POST>
<table>
<tr>
    <td>
	  <fieldset>
	  <legend>
	    Web Service Link:
	  </legend>
	Link:
	<input id="li" type="text" name="link" title="link" size=90 value=<%GetCurrentLink();%>>
    </fieldset>
     </td>
</tr>
<tr>
    <td>
      <fieldset>
       <legend>
	   Web Service Account:
       </legend>
       Account:
	   <input id="ac" type="text" name="account" title="Acount" value=<%GetCurrentAccount();%>>
       </fieldset>
     </td>
</tr>
<tr>
    <td>
      <fieldset>
       <legend>
	   Web Service Clock Id:
       </legend>
       Clock Id:
	   <input id="clo" type="text" name="ClockId" title="ClockId" value=<%GetCurrentClockId();%>>
       </fieldset>
     </td>
</tr>
<tr>
     <td>
      <fieldset>
       <legend>
	   Web Service Time Out In Seconds:
       </legend>
       Time Out:
	   <input id="to" type="text" name="TimeOut" title="TimeOut" value=<%GetCurrentTimeOut();%>>
       </fieldset>
     </td>
</tr>
<tr>
     <td>
      <fieldset>
       <legend>
	   Web Service Heart Beat Timer In Seconds:
       </legend>
       Heart Beat Timer:
	   <input id="hb" type="text" name="HeartBeatTimer" title="HeartBeatTimer" value=<%GetCurrentHeartBeatTimer();%>>
       </fieldset>
     </td>
</tr>
<tr>
     <td>
      <fieldset>
       <legend>
	   Web Service Project Tasks Timer In Seconds:
       </legend>
       Project Tasks Timer:
	   <input id="pt" type="text" name="GetProjectTasks" title="GetProjectTasks" value=<%GetCurrentProjectTasksTimer();%>>
       </fieldset>
     </td>
</tr>
<tr>
     <td>
      <fieldset>
       <legend>
	   Web Service Headers:
       </legend>
        <%GetCurrentAddSoapHeader();%>
       </fieldset>
     </td>
</tr>

<tr>
    <td>
     <fieldset>
      <legend>
	  Enable SSL:
      </legend>
       <%GetVerifySSL();%>
     </fieldset>
    </td>
</tr>
<tr>
    <td>
     <fieldset>
      <legend>
	  SSL Certificate File:
      </legend>
			<input id="sf" type="text" name="SSLCertFile" title="sslcertfile" size=90 value=<%GetSSLCertFile();%>>
     </fieldset>
    </td>
</tr>

<tr>
   <td>
     <input type="submit" value="SET" />
   </td>
</tr>
</table>
</form>

</body>
</html>
