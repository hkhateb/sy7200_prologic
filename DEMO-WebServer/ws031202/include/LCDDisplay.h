#ifndef _LCDDISPLAY_H_
#define _LCDDISPLAY_H_

enum CursorState
{
	CURSOR_OFF = 0,
	CURSOR_UNDER,
	CURSOR_BLINK,
	CURSOR_BOTH
};

class LCDDisplay
{
public:
	LCDDisplay ();
	virtual ~LCDDisplay ();
	
	virtual void Write (int row, int col, const char * str);
	virtual void Clear ();
	virtual void DisplayContrast(int contrast);
	
	// Cursor
	virtual void SetCursorState (CursorState state);
};

#endif //_LCDDISPLAY_H_
