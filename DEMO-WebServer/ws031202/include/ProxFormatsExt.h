/*
	ProxFormats:  [Header]

    Proximity card format data tables.

	Copyright (c) 2006
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   07 Mar 2006
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/01 18:43:11 $

*/


/* Compilation options for ProxFormats. */
#define OPT_FORMAT_ADM34                1

/* Constant parameters for ProxFormats. */
#define PROX_ADM34_LENGTH               34
#define PROX_HID32NP_LENGTH             32
#define PROX_HID36NP_LENGTH             36
#define PROX_HID37NP_LENGTH             37

/* Type and structure definitions for ProxFormats. */


/* Proximity formats extension list for adding via proxsetup utility. */
/* Note:  Bit field start positions are from RH/LSB and start at 0. */
static const CardProxFormat_t ProxFormatExtensions [] = {

	#if OPT_FORMAT_ADM34
	/* Ademco 34 bit PassPoint Proximity Card */
	{
	.name =         "ADM34",
	.bit_length =   PROX_ADM34_LENGTH,
	.parity[0] = {
		.position =     (PROX_ADM34_LENGTH)-1,
		.type =         CardParityEven,
		.start =        (PROX_ADM34_LENGTH)/2,
		.length =       (PROX_ADM34_LENGTH)/2-1,
	},
	.parity[1] = {
		.position =     0,
		.type =         CardParityEven,
		.start =        1,
		.length =       (PROX_ADM34_LENGTH)/2-1,
	},
	.field[0] = {
		.start =        25,
		.length =       8,
		.delimiter =    '\0',
		.width =        3,
	},
	.field[1] = {
		.start =        21,
		.length =       4,
		.delimiter =    '-',
		.width =        2,
	},
	.field[2] = {
		.start =        1,
		.length =       20,
		.delimiter =    '-',
		.width =        6,
	},
},
	#endif

	{
	.name =         "",
	}
};

