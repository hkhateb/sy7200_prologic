#ifndef _SOUND_H_
#define _SOUND_H_

class Sound
{
public:
	Sound();
	virtual ~Sound();
	
	void Beep(int milliseconds, int toneHz);
};

#endif //_SOUND_H_
