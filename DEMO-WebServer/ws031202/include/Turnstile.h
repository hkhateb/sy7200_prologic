 /*
	Turnstile.h:

    Standard definitions for the Turnstile subsystem.

	Copyright (c) 2006
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   30 May 2006
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/01 18:43:11 $

*/


/* Include Remote I/O module definitions. */
#include "RemoteIO.h"

/* Module object and handle types. */
struct Turnstile;
typedef struct Turnstile * Turnstile_h;

/* Turnstile event enumeration. */
typedef enum {
	TE_NONE,

	TE_DI_ENTER_ACTUATION,
	TE_DI_ENTER_LIMIT_SW,
	TE_DI_EXIT_ACTUATION,
	TE_DI_EXIT_LIMIT_SW,

	TE_DO_ENTER_ACTUATE,
	TE_DO_EXIT_ACTUATE,
}
TurnstileEvent_e;

/* Trunstile alert output signals. */
typedef enum {
	TA_ACCESS_INVALID,

	MAX_TA
}
TrunstileAlert_e;

/* Digital I/O signal assignments. */
#define TURNSTILE_DO_ENTER_ACTUATE		0
#define TURNSTILE_DO_EXIT_ACTUATE		2
#define TURNSTILE_DO_ACCESS_INVALID		4

#define TURNSTILE_DI_ENTER_ACTUATION		0
#define TURNSTILE_DI_ENTER_LIMIT_SW		1
#define TURNSTILE_DI_EXIT_ACTUATION		4
#define TURNSTILE_DI_EXIT_LIMIT_SW		5

/* Digital I/O bit field values. */
#define TURNSTILE_BIT_DO_ENTER_ACTUATE		(0x1<<(TURNSTILE_DO_ENTER_ACTUATE))
#define TURNSTILE_BIT_DO_EXIT_ACTUATE		(0x1<<(TURNSTILE_DO_EXIT_ACTUATE))
#define TURNSTILE_BIT_DO_ALL			(TURNSTILE_BIT_DO_ENTER_ACTUATE | \
						TURNSTILE_BIT_DO_EXIT_ACTUATE)

#define TURNSTILE_BIT_DI_ENTER_ACTUATION	(0x1<<(TURNSTILE_DI_ENTER_ACTUATION))
#define TURNSTILE_BIT_DI_ENTER_LIMIT_SW		(0x1<<(TURNSTILE_DI_ENTER_LIMIT_SW))
#define TURNSTILE_BIT_DI_EXIT_ACTUATION		(0x1<<(TURNSTILE_DI_EXIT_ACTUATION))
#define TURNSTILE_BIT_DI_EXIT_LIMIT_SW		(0x1<<(TURNSTILE_DI_EXIT_LIMIT_SW))

/* Bit field combinations. */
#define TURNSTILE_BIT_DI_ENTER_ALL		(TURNSTILE_BIT_DI_ENTER_ACTUATION | \
						 TURNSTILE_BIT_DI_ENTER_LIMIT_SW)
#define TURNSTILE_BIT_DI_EXIT_ALL		(TURNSTILE_BIT_DI_EXIT_ACTUATION | \
						 TURNSTILE_BIT_DI_EXIT_LIMIT_SW)
#define TURNSTILE_BIT_DI_ALL			(TURNSTILE_BIT_DI_ENTER_ALL | \
						 TURNSTILE_BIT_DI_EXIT_ALL)


/* Function and function handle data types. */
typedef Code TurnstileEvent_f (void * data_p, TurnstileEvent_e event, Boolean new_state);
typedef TurnstileEvent_f * TurnstileEvent_fp;
typedef Code Turnstile_f (Turnstile_h turn_p, TurnstileEvent_fp callback_fh, void *data_p,
                RIOTime_t interval, Boolean report_all, Boolean error_stop);
typedef Turnstile_f * Turnstile_fp;

/* External function prototypes. */
Code    turnstile_init (Turnstile_h * turn_hp,
		RIOModule_h module_h, RIOModuleType_e type, RIOId_t id, Boolean verify,
		RIOComm_h comm_h);
Code    turnstile_release (Turnstile_h * turn_hp);

Turnstile_f	turnstile_scan;
Turnstile_f	turnstile_listen;

Code    turnstile_module_set (Turnstile_h turn_p, RIOModule_h module_h,
		RIOModule_h * prev_hp);
RIOModule_h
	turnstile_module_get (Turnstile_h turn_p);
RIOComm_h
	turnstile_comm_get (Turnstile_h turn_p);
void    turnstile_verbose_set (Turnstile_h turn_p, unsigned int verbose);
Code    turnstile_alert_out (Turnstile_h turn_p, UBin8 alert, Boolean state);

