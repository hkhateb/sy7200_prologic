/**
 *  	System dependent configuration
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */

 /*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

 #ifndef	__UNIFINGERCONFIG_H__
 #define __UNIFINGERCONFIG_H__

//
// Host
//
//#define	UF_SDK_WIN32	
//#define	UF_SDK_LINUX	


//
// Thread safety
//
#define	UF_SDK_SINGLE_THREAD		0
#define	UF_SDK_MULTI_THREAD		1

#define	UF_SDK_THREAD_MODEL		UF_SDK_SINGLE_THREAD

 #endif

