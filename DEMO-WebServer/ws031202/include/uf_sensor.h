/**
 *  	Definitions of Sensor related constants
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */

/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */
 
 #ifndef __UNIFINGERSENSOR_H__
#define __UNIFINGERSENSOR_H__


typedef enum {
	UF_SENSOR_INFINEON_FINGERTIP	= 0x30,
	UF_SENSOR_ATMEL_FINGERTIP		= 0x31,
	UF_SENSOR_AUTHENTEC_AF32		= 0x32,
	UF_SENSOR_OPTICAL					= 0x33,
	UF_SENSOR_ST_TOUCHCHIP			= 0x34,
	UF_SENSOR_BMF_BLP100				= 0x35,
	UF_SENSOR_OPTICAL2				= 0x36,
	UF_SENSOR_FUJITSU_MBF310			= 0x37,
	UF_SENSOR_OPTICAL3				= 0x38,
	UF_SENSOR_OPTICAL4				= 0x39,
	UF_SENSOR_AUTHENTEC_AFS8600	= 0x3a
} UF_SENSOR_TYPE;

#endif
