/*
	BitBuffer.h:

    Standard definitions for the BitBuffer subsystem.

	Copyright (c) 2005 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   10 Jun 2005
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/01 18:43:11 $

*/


#if ! defined _BITBUFFER_H
#define _BITBUFFER_H

#if ! defined _GEN_DEF
typedef int Boolean;
#endif

/* Basic bit buffer module parameter data types. */
typedef int BitValue_t;
typedef int BitStatus_t;
typedef unsigned int BitIndex_t;

/* Bit buffer data array element type and characteristics. */
typedef unsigned long BitBlock_t;
#define BitBlockShift   5
#define BitBlockSize    (1<<BitBlockShift)
#define BitBlockMask    (BitBlockSize-1)

/* Parameter data type for extraction and addition of bit substrings. */
typedef BitBlock_t BitString_t;
#define BitStringSize   (sizeof (BitString_t)*8)

/* BitBuffer object data structure. */
typedef struct {
	BitBlock_t * bits_p;    /* Pointer to bit data array. */
	BitIndex_t max_length;  /* Maximum length of bit block in bits. */
	BitIndex_t offset;      /* Offset in bits from start of bit block to ignore for indexing. */
	BitIndex_t length;      /* Current length of bit block in bits excluding offset. */
	BitIndex_t index;       /* Index in bits into bit block starting at offset. */
}
BitBuffer_t;


/* BitBuffer function type definitions. */
typedef BitValue_t      BitBufferGetNextBit_f (BitBuffer_t * const bb_p);
typedef BitStatus_t     BitBufferAddString_f (BitBuffer_t * const bb_p,
				BitString_t bit_string, BitIndex_t length);
typedef BitStatus_t     BitBufferInsertString_f (BitBuffer_t * const bb_p,
				BitIndex_t index, BitString_t bit_string,
				BitIndex_t length);
typedef BitString_t     BitBufferGetstring_f (const BitBuffer_t * const bb_p,
				BitIndex_t index, BitIndex_t length);
typedef BitString_t     BitBufferGetNextString_f (BitBuffer_t * const bb_p,
				BitIndex_t length);
typedef BitIndex_t      BitBufferStrip_f (BitBuffer_t * const bb_p, BitValue_t value);

typedef void            BitBufferDisplay_f (BitBuffer_t * const bb_p,
				BitIndex_t grouping, BitIndex_t active_length, Boolean both);
typedef void            BitBufferDisplaySubstrings_f (BitBuffer_t * const bb_p,
				BitIndex_t sub_length, BitIndex_t active_length, Boolean both);

/* BitBuffer function and function pointer type definitions. */
typedef BitBufferGetNextBit_f *         BitBufferGetNextBit_fp;
typedef BitBufferAddString_f *          BitBufferAddString_fp;
typedef BitBufferInsertString_f *       BitBufferInsertString_fp;
typedef BitBufferGetstring_f *          BitBufferGetstring_fp;
typedef BitBufferGetNextString_f *      BitBufferGetNextString_fp;
typedef BitBufferStrip_f *              BitBufferStrip_fp;

typedef BitBufferDisplay_f *            BitBufferDisplay_fp;
typedef BitBufferDisplaySubstrings_f *  BitBufferDisplaySubstrings_fp;


/* BitBuffer external functions. */
void            bit_buffer_offset_set (BitBuffer_t * const bb_p, BitIndex_t offset);
void            bit_buffer_complement (BitBuffer_t * const bb_p);
BitStatus_t     bit_buffer_add_bit (BitBuffer_t * const bb_p, BitValue_t bit);
BitValue_t      bit_buffer_get_bit (const BitBuffer_t * const bb_p, BitIndex_t index);
BitBufferGetNextBit_f           bit_buffer_get_next_bit;
BitBufferGetNextBit_f           bit_buffer_get_next_bit_reverse;
BitBufferAddString_f            bit_buffer_add_string_msb;
BitBufferAddString_f            bit_buffer_add_string_lsb;
BitBufferInsertString_f         bit_buffer_insert_string_msb;
BitBufferInsertString_f         bit_buffer_insert_string_lsb;
BitBufferGetstring_f            bit_buffer_get_string_msb;
BitBufferGetstring_f            bit_buffer_get_string_lsb;
BitBufferGetstring_f            bit_buffer_get_string_msb_reverse;
BitBufferGetstring_f            bit_buffer_get_string_lsb_reverse;
BitBufferGetNextString_f        bit_buffer_get_next_string_msb;
BitBufferGetNextString_f        bit_buffer_get_next_string_lsb;
BitBufferGetNextString_f        bit_buffer_get_next_string_msb_reverse;
BitBufferGetNextString_f        bit_buffer_get_next_string_lsb_reverse;
BitBufferStrip_f                bit_buffer_strip_leading;
BitBufferStrip_f                bit_buffer_strip_trailing;
BitBufferStrip_f                bit_buffer_strip_both;

BitBufferDisplay_f              bit_buffer_display;
BitBufferDisplay_f              bit_buffer_display_reverse;
BitBufferDisplaySubstrings_f    bit_buffer_display_substrings_msb;
BitBufferDisplaySubstrings_f    bit_buffer_display_substrings_lsb;
BitBufferDisplaySubstrings_f    bit_buffer_display_substrings_msb_reverse;
BitBufferDisplaySubstrings_f    bit_buffer_display_substrings_lsb_reverse;


/* Inline functions for BitBuffer. */
extern __inline__ void
		bit_buffer_reset (BitBuffer_t * const bb_p)
{
	bb_p->offset = 0;
	bb_p->length = 0;
	bb_p->index = 0;
	return;
}
extern __inline__ void
		bit_buffer_reset_index (BitBuffer_t * const bb_p)
{
	bb_p->index = 0;
	return;
}
extern __inline__ void
		bit_buffer_reset_index_reverse (BitBuffer_t * const bb_p)
{
	bb_p->index = bb_p->length ? bb_p->length - 1 : 0;
	return;
}
extern __inline__ void
		bit_buffer_init (BitBuffer_t * const bb_p, BitBlock_t * bits_p, BitIndex_t max_length)
{
	bb_p->bits_p = bits_p;
	bb_p->max_length = max_length;
	bit_buffer_reset (bb_p);
	return;
}
extern __inline__ BitIndex_t
		bit_buffer_length_get (const BitBuffer_t * const bb_p)
{
	return (bb_p->length);
}
extern __inline__ void
		bit_buffer_length_set (BitBuffer_t * const bb_p, BitIndex_t length)
{
	bb_p->length = length;
	return;
}
extern __inline__ BitIndex_t
		bit_buffer_offset_get (const BitBuffer_t * const bb_p)
{
	return (bb_p->offset);
}
extern __inline__ BitIndex_t
		bit_buffer_max_length_get (const BitBuffer_t * const bb_p)
{
	return (bb_p->max_length);
}

extern __inline__ BitBlock_t *
		bit_buffer_bits_p_get (const BitBuffer_t * const bb_p)
{
	return (bb_p->bits_p);
}

extern __inline__ BitIndex_t
		bit_string_count (BitString_t bits, BitIndex_t length)
{
	BitIndex_t count = 0;
	BitString_t mask = 0x1;
	while (length > 0) {
		if (mask & bits) count++;
		mask <<= 1;
		length--;
	}
	return (count);
}

extern __inline__ BitStatus_t
		bit_buffer_add_string_msb (BitBuffer_t * const bb_p,
			BitString_t bit_string, BitIndex_t length)
{
	return (bit_buffer_insert_string_msb (bb_p, bb_p->length,
		bit_string, length));
}

extern __inline__ BitStatus_t
		bit_buffer_add_string_lsb (BitBuffer_t * const bb_p,
			BitString_t bit_string, BitIndex_t length)
{
	return (bit_buffer_insert_string_lsb (bb_p, bb_p->length,
		bit_string, length));
}

#endif

