/*
	SetupXML.h:

    Standard definitions for the SetupXML subsystem.

	Copyright (c) 2006
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   25 Oct 2006
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/01 18:43:11 $

*/

/* Include card driver level definitions. */
#include "Card.h"
#include "List.h"

typedef struct {
	struct list_head link;
	CardProxFormat_t format;
}
SetupProxFormatList_t;

/* External function prototypes. */
int	setup_input_XML_cards (FILE * stream, void * list_head,
		unsigned int verbose_level, FILE * verbose_output);

void    setup_display_cards_text (FILE * out, const CardProxFormat_t * form_p,
		const char * title);
void    setup_display_cards_xml (FILE * out, const CardProxFormat_t * form_p,
		const char * title);

