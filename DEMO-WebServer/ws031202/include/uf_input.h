/**
 *  	Definitions of Input related constants
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */

/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

 #ifndef __UNIFINGERINPUT_H__
#define __UNIFINGERINPUT_H__

typedef enum {
	UF_INPUT_NO_ACTION				= 0x00,
	UF_INPUT_ENROLL					= 0x10,
	UF_INPUT_IDENTIFY				= 0x30,
	UF_INPUT_DELETE					= 0x40,
	UF_INPUT_DELETE_ALL				= 0x49,
	UF_INPUT_ENROLL_BY_WIEGAND	= 0x11,
	UF_INPUT_VERIFY_BY_WIEGAND	= 0x21,
	UF_INPUT_DELETE_BY_WIEGAND	= 0x41,
	UF_INPUT_ENROLL_VERIFICATION					= 0x1a,
	UF_INPUT_ENROLL_BY_WIEGAND_VERIFICATION			= 0x1b,
	UF_INPUT_DELETE_VERIFICATION					= 0x4a,
	UF_INPUT_DELETE_BY_WIEGAND_VERIFICATION			= 0x4b,
	UF_INPUT_DELETE_ALL_VERIFICATION				= 0x4c,
	UF_INPUT_CANCEL									= 0x60,
} UF_INPUT_FUNC;

typedef enum {
	UF_INPUT_PORT0		= 0x00,
	UF_INPUT_PORT1		= 0x01,
	UF_INPUT_PORT2		= 0x02,
} UF_INPUT_PORT;

typedef enum {
	UF_INPUT_CLEAR		= 0x00,
	UF_INPUT_REMAIN		= 0x01
} UF_INPUT_OPTION;

typedef enum {
	UF_INPUT_INACTIVE		= 0x00,
	UF_INPUT_ACTIVE		= 1,
} UF_INPUT_STATUS;



#endif
