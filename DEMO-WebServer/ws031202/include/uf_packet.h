/**
 *  	Packet definition
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

#ifndef __UNIFINGERPACKETDEF_H__
#define __UNIFINGERPACKETDEF_H__

#include "uf_type.h"

//
// Constants
//
#define	UF_PACKET_START_CODE			0x40
#define	UF_NPACKET_START_CODE			0x41
#define	UF_PACKET_END_CODE				0x0a
#define	UF_PACKET_LEN					13
#define	UF_NPACKET_LEN					15

#define	UF_PACKET_COMMAND		0
#define	UF_PACKET_TERMINALID	1
#define	UF_PACKET_PARAM			2
#define	UF_PACKET_SIZE			3
#define	UF_PACKET_FLAG			4
#define	UF_PACKET_CHECKSUM		5

//
// Byte position of packet components
//
#define	UF_PACKET_START_CODE_POS	0
#define	UF_PACKET_COMMAND_POS		1
#define	UF_PACKET_PARAM_POS			2
#define	UF_PACKET_SIZE_POS			6
#define	UF_PACKET_FLAG_POS			10
#define	UF_PACKET_CHECKSUM_POS		11
#define	UF_PACKET_END_CODE_POS		12

#define	UF_NPACKET_START_CODE_POS	0
#define	UF_NPACKET_TERMINALID_POS	1
#define	UF_NPACKET_COMMAND_POS		3
#define	UF_NPACKET_PARAM_POS		4
#define	UF_NPACKET_SIZE_POS			8
#define	UF_NPACKET_FLAG_POS			12
#define	UF_NPACKET_CHECKSUM_POS		13
#define	UF_NPACKET_END_CODE_POS		14



#ifdef UF_USING_CPP
extern "C" 
{
#endif

//
// Aux functions
//
void uf_make_packet( UF_UINT8 command, UF_UINT32 param, UF_UINT32 size, UF_UINT8 flag, UF_BYTE* packet );
void uf_make_npacket( UF_UINT8 command, UF_UINT16 terminal_id, UF_UINT32 param, UF_UINT32 size, UF_UINT8 flag, UF_BYTE* npacket );

void uf_make_data_packet( UF_UINT8 command, UF_UINT16 index, UF_UINT16 num_of_packet, UF_UINT32 data_size, UF_BYTE* packet );
void uf_make_data_npacket( UF_UINT8 command, UF_UINT16 index, UF_UINT16 terminal_id, UF_UINT16 num_of_packet, UF_UINT32 data_size, UF_BYTE* npacket );
	
UF_UINT8 uf_calculate_checksum( UF_BYTE* packet, int size );
UF_UINT32 uf_calculate_data_checksum( UF_BYTE* packet, int size );
UF_UINT32 uf_get_packet_value( int component, UF_BYTE* packet );


#ifdef UF_USING_CPP
}
#endif

 #endif
