/*
	Splash.h:
	
    Definitions and function proptotypes for splash modules.

	Copyright (c) 2005 - Time America, Inc.
        Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   21 Nov 2005
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/01 18:43:11 $

    This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

    This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

    You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software Foundation,
Inc., 51 Franklin St, 5th Floor, Boston, MA 02110-1301 USA

*/	


/* Splash text line setup & field data types. */
typedef enum {
	LEFT,
	CENTER,
	RIGHT,
}
just_e;
typedef struct {
	const char * text_p;
	size_t length;
	size_t offset;
	just_e justification;
}
field_t;
typedef struct {
	field_t left, center, right;
}
setup_t;

/* Function prototypes for splash text. */
void    splash_text (void);	/* Board specific function see NE7000Cmd.c */
int     splash_text_format_line (int row, char * line_p, size_t columns);
void    splash_text_format_string (const char * form_p, char * line_p,
				   size_t columns);
size_t  splash_text_format_expand (const char * form_p, size_t form_len,
				   char * expand_p, size_t expand_max);
size_t  splash_text_format_substitute (const char * macro_p, size_t macro_len,
				       char * expand_p, size_t expand_max);
void    splash_text_format_parse (const char * form_p, size_t length,
				  setup_t * setup_p, size_t width);

