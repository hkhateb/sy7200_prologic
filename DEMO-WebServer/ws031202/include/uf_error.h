/**
 *  	Error/flag code of packet protocol
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */

/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */
 
 #ifndef __UNIFINGERERROR_H__
 #define __UNIFINGERERROR_H__

#include "uf_config.h"

typedef enum {
	UF_RET_SUCCESS	= 0,
	UF_RET_CONTINUE	= 1,

	// serial communication error
	UF_ERR_CANNOT_OPEN_SERIAL	= -1,
	UF_ERR_CANNOT_SETUP_SERIAL	= -2,
	UF_ERR_CANNOT_WRITE_SERIAL	= -3,
	UF_ERR_WRITE_SERIAL_TIMEOUT	= -4,
	UF_ERR_CANNOT_READ_SERIAL	= -5,
	UF_ERR_READ_SERIAL_TIMEOUT	= -6,
	UF_ERR_CHECKSUM_ERROR		= -7,
	UF_ERR_CANNOT_SET_TIMEOUT	= -8,

	// protocol error
	UF_ERR_NOT_FOUND				= -100,
	UF_ERR_SCAN_FAILED			= -101,
	UF_ERR_UNSUPPORTED			= -102,
	UF_ERR_TIMEOUT				= -103,
	UF_ERR_TRY_AGAIN				= -104,
	UF_ERR_MEM_FULL				= -105,
	UF_ERR_FINGER_LIMIT			= -106,
	UF_ERR_INVALID_ID				= -107,
	UF_ERR_EXIST_ID				= -108,
	UF_ERR_TIMEOUT_MATCH			= -109,
	UF_ERR_NOT_MATCH				= -110,
	UF_ERR_BUSY					= -111,
	UF_ERR_CANCELED				= -112,
	UF_ERR_DATA_ERROR			= -113,


	UF_ERR_UNKNOWN				= -999,
 } UF_RET_CODE;


 typedef enum {
 	UF_PROTO_RET_SUCCESS			= 0x61,
	UF_PROTO_RET_SCAN_SUCCESS	= 0x62,
	UF_PROTO_RET_SCAN_FAIL		= 0x63,
	UF_PROTO_RET_NOT_FOUND		= 0x69,
	UF_PROTO_RET_NOT_MATCH		= 0x6a,
	UF_PROTO_RET_TRY_AGAIN		= 0x6b,
	UF_PROTO_RET_TIME_OUT		= 0x6c,
	UF_PROTO_RET_MEM_FULL		= 0x6d,
	UF_PROTO_RET_EXIST_ID		= 0x6e,
	UF_PROTO_RET_CHECK_ID		= 0x70,
	UF_PROTO_RET_ADD_NEW		= 0x71,
	UF_PROTO_RET_FINGER_LIMIT	= 0x72,
	UF_PROTO_RET_CONTINUE		= 0x74,
	UF_PROTO_RET_UNSUPPORTED	= 0x75,
	UF_PROTO_RET_INVALID_ID		= 0x76,
	UF_PROTO_RET_AUTO_ID			= 0x79,
	UF_PROTO_RET_TIMEOUT_MATCH	= 0x7a,
	UF_PROTO_RET_BUSY			= 0x80,
	UF_PROTO_RET_CANCELED		= 0x81,
	UF_PROTO_RET_DATA_ERROR		= 0x82,
	UF_PROTO_RET_DATA_OK		= 0x83,
} UF_PROTOCOL_RET_CODE;


 #endif

