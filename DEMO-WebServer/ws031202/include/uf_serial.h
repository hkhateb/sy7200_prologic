/**
 *  	Wrapper API for serial communication
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */


/*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */
 
#ifndef __UNIFINGERSERIAL_H__
#define __UNIFINGERSERIAL_H__

#include "uf_type.h"
#include "uf_sys_parameter.h"

//
// Com Port Parameters 
// 	databit	8
//	stopbit	1
//	no parity
//

#ifdef UF_USING_CPP
extern "C" 
{
#endif

int uf_serial_open( const char* device_name );
int uf_serial_close();
int uf_serial_setup( int baudrate );
int uf_serial_read( UF_BYTE* buf, int size );
int uf_serial_write( UF_BYTE* buf, int size );
int uf_serial_set_read_timeout( int timeout );
int uf_serial_set_write_timeout( int timeout );

#ifdef UF_USING_CPP
}
#endif

#endif


