/*
	Timer.h

	External definitions and function prototypes for the timer module.

	Copyright (c) 1995,1997-1998,2006 - Alcom Communications

	Written by:   Alan J. Luse
	Written on:   16 May 1990
	  Based on:   Time_Def.h
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime:   01 Mar 2000 17:29:16  $
	     $Date: 2006/12/01 18:43:11 $

*/


#if ! defined _TIMER
#define _TIMER

/* Data type for timer intervals. */
typedef long TimerInterval_t;

/* Time interval conversion constants. */
#define MS_PER_SEC	((TimerInterval_t)1000)

/* Type and structure definitions for timers. */
typedef struct {
	TimerInterval_t date_time;
	}
Timer_t;

/* External function prototypes for timer module. */
void  	timer_reset (Timer_t * timer_hdl, TimerInterval_t milliseconds);
TimerInterval_t
	timer_elapsed (Timer_t * timer_hdl);
TimerInterval_t
	timer_left (Timer_t * timer_hdl);
Boolean	timer_done (Timer_t * timer_hdl);
int     timer_sleep (TimerInterval_t milliseconds);
void	timer_wait (TimerInterval_t milliseconds);

#endif

