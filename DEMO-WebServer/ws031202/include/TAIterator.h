#ifndef _TAIterator_H_
#define _TAIterator_H_

template < class I > class IIH {
 public:
  IIH ( I& ii ) : i ( ii ) {}
  operator I& () { return i ;}

  bool operator != ( const I& r ){
    return i != r ;
  }

  //  I ::value_type operator * () { return * i ;}

  IIH& operator ++ () { i ++ ; return * this ;}

 private:
  I& i ;

};

template < class I > IIH < I > new_iih ( I& i ) { return IIH < I > ( i );}
#endif //_TAIterator_H_
