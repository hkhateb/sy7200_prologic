/*
	str_def.h:

	Standard definitions and function prototypes for supplementary string
functions.

	Copyright (c) 2006
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.
	Copyright (c) 1992-1994,2000 - Alcom Communications

	Written by:   Alan J. Luse
	Written on:   10 Sep 1992
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime:   20 Jun 2001 14:57:04  $
	     $Date: 2006/12/01 18:43:11 $

*/

#if ! defined _STR_DEF

#ifdef __cplusplus
extern "C" {
#endif

/* Include standard definitions for size_t, etc. */
#include <StdDef.h>

/* Character to name field function type and data structure. */
typedef unsigned char StrCharValue;
typedef struct StrChar_t {
	StrCharValue value;
	char * name;
}
StrChar_t;

/* String cross sectional index data type. */
typedef int StringCSAIndex;

/* String cross sectional array bounds range and special data structure. */
typedef struct StringCSABounds {
	StringCSAIndex low, high;
	struct {
		const char * string;
		StringCSAIndex value;
		}
	special;
	}
StringCSABoundsData;


/* External function prototypes for string subsystem. */
char *	str_lower (char * string);
char *	str_upper (char * string);

char *	str_blank (const char * string);
char *	str_clip (char * string);

char *	str_unquote (char * string);
char *	str_parse (const char ** str_pp, size_t * length_p);
int	str_split (const char * string, char * list);
int	str_argv (const char ** argv, const char * list, int count,
		const char * initial);

const char *
	str_char_lookup (const StrChar_t * sc_list, StrCharValue value);
StrCharValue
	str_char_value (const StrChar_t * sc_list, const char * name);

int	str_path_next (const char * base, const char ** const paths_pp,
		char * filename, size_t fn_size);

#ifdef __cplusplus
}
#endif

#define _STR_DEF
#endif

