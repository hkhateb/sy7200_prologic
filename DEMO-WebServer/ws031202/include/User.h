/*
	User_Def.h:

    Standard definitions for the NE7000 system software User subsystem.

	Copyright (c) 2005
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   25 Jan 2005
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/01 18:43:11 $

*/


#if ! defined _USER_DEF

#include "Gen_Def.h"

/* Status codes for User subsystem. */
typedef enum {
	SC_USER_GENERIC = 100,
	SC_USER_MISSING_ADAPTOR,
	SC_USER_DEVICE_ADDRESS,
}
UserCode_e;

/* Key value constants. */
#define KEY_ENTER		'\n'
#define KEY_ESC 		0x1B
#define KEY_CLEAR		KEY_ESC
#if ! defined (ESC)
#define ESC			KEY_ESC
#endif

/* Indicator light definitions. */
#define USER_VISUAL_RED		1
#define USER_VISUAL_YEL		2
#define USER_VISUAL_GRN		3

/* Cursor definitions. */
#define USER_CURSOR_OFF		0x00
#define USER_CURSOR_UNDER	0x01
#define USER_CURSOR_BLINK	0x02
#define USER_CURSOR_BOTH	0x03

#ifdef WIN32


#define user_init();
#define user_open();
#define user_close();
#define user_input_initialize();
#define user_input_ready();
#define user_input_get();
#define user_output_initialize();
#define user_output_clear();
#define user_output_write(row, column, display);
#define user_output_cursor(mode);
#define user_output_test(rows, columns);
#define user_output_contrast(contrast);
#define user_visual_init();
#define user_visual_close();
#define user_visual_set(light, state);
#define user_audible_init();
#define user_audible_close();
#define user_audible_set(tone);
#define user_audible_volume(volume);
#define user_digital_init();
#define user_digital_close();
#define user_digital_set(output, state);
#define user_digital_get(output);
#define user_barcode_setup();
#define user_barcode_restore();
#define user_barcode_open(device, baudrate);
#define user_barcode_close();
#define user_barcode_read_settings(buffer, max_length);


#else
/* External function prototypes for User subsystem. */
Code    user_init (void);
Code    user_open (void);
Code    user_close (void);

Code	user_input_initialize (void);
Boolean user_input_ready (void);
char    user_input_get (void);

Code	user_output_initialize (void);
Code	user_output_clear (void);
Code	user_output_write (UBin8 row, UBin8 column, const char * display);
Code    user_output_cursor (Bit8 mode);
Code    user_output_test (UBin8 rows, UBin8 columns);
Code	user_output_contrast (UBin16 contrast);

int	user_visual_init (void);
void    user_visual_close (void);
int	user_visual_set (int light, int state);

int	user_audible_init (void);
void    user_audible_close (void);
int     user_audible_set (unsigned int tone);
int     user_audible_volume (unsigned int volume);

int	user_digital_init (void);
void    user_digital_close (void);
int	user_digital_set (int output, int state);
int     user_digital_get (int output);

int	user_barcode_setup (void);
int	user_barcode_restore (void);
int	user_barcode_open (const char * device, int baudrate);
void	user_barcode_close (void);
int     user_barcode_read_settings (Byte * buffer, int max_length);

#endif


#define user_input_init  user_init
#define user_input_close user_close

/* Internal low level user interface data and routines. */
typedef enum {
	CMD_LCD_INIT    = 0x10,
	CMD_LCD_CLEAR   = 0x11,
	CMD_LCD_WRITE   = 0x12,
	CMD_LCD_CURSOR  = 0x13,

	CMD_LCD_CONTRAST= 0x20,
}
UserCmd_e;
extern const size_t UserInterfaceHeaderLengthWrite;
Boolean	user_interface_input_poll (char * keycode_p);
Code	user_interface_output_send (const Byte * buffer, size_t length);

#define _USER_DEF
#endif


