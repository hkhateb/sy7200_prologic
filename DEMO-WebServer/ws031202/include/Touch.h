/*
	Touch.h:

    Definition of constants and structures, and declaration of functions
for the Touch subsystem.

	Copyright (c) 2006
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   29 Jun 2006
	 $Revision: 1.2 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/12 22:40:46 $

*/

#ifndef _TOUCH_CAL_H
#define _TOUCH_CAL_H

/* Need time value declarations. */
#ifdef __KERNEL__
#include <linux/time.h>
#else
#include <sys/time.h>
#endif

/* Definitions. */
#ifndef		OK
	#define OK		 0
	#define NOT_OK		-1
#endif

/* Scaling shift factor and rounding fraction for 3 point matrix values. */
#define SCALING_SHIFT		4
#define SCALING_ROUND		((0x1<<(SCALING_SHIFT))>>1)


/*** Types and structures. ***/
#if ! defined _GEN_DEF
typedef int Boolean;
#endif

/* Touch time interval type. */
typedef unsigned int TouchTime_t;

/* Touch or display coordinate data type. */
typedef signed int TouchCoord_t;

/* Touch or display point coordinate pair. */
typedef struct TouchPoint {
	TouchCoord_t x, y;
}
TouchPoint_t;

/* Single axis scaling data. */
typedef struct {
	TouchCoord_t min, max;
}
TouchCalibAxis_t;
/* Dual axis scaling data. */
typedef struct {
	TouchCalibAxis_t x, y;
}
TouchRect_t;

/* Calibration matrix. */
typedef struct Matrix {
	TouchCoord_t An;     /* A = An/denominator */
	TouchCoord_t Bn;     /* B = Bn/denominator */
	TouchCoord_t Cn;     /* C = Cn/denominator */
	TouchCoord_t Dn;     /* D = Dn/denominator */
	TouchCoord_t En;     /* E = En/denominator */
	TouchCoord_t Fn;     /* F = Fn/denominator */
	TouchCoord_t denominator;
}
TouchMatrix_t;

/* Touch or display point coordinate pair. */
typedef struct TouchParameters {
	TouchCoord_t shift;	/* Minimum raw position shift to detect movement. */
	TouchCoord_t stable;	/* Maximum raw position shift for stable touch. */
	TouchTime_t interval;	/* Touch screen polling interval in milliseconds. */
}
TouchParameters_t;

/* Touch event read data structure; same as event data in Nano-X. */
typedef struct ts_event {
	short x;
	short y;
	short pressure;
	short pad;
	struct timeval stamp;
}
TouchEvent_t;

/* Calibration display data types. */
typedef unsigned long TouchDisplayColor_t;


/*** External function prototypes. ***/

/* 3 point calibration functions. */
void	Touch3PtCalibratePoints (const TouchPoint_t * resolution_p,
		TouchPoint_t * display_p, int margin);
void	Touch3PtCalibrationRaw (TouchMatrix_t * matrix_p);
int	Touch3PtCalibrationCalc (TouchMatrix_t * matrix_p,
		const TouchPoint_t * display_p,
		const TouchPoint_t * touch_p);
int	Touch3PtDisplayPointCalc (const TouchMatrix_t * matrix_p,
		const TouchPoint_t * resolution_p,
		const TouchPoint_t * touch_p, TouchPoint_t * display_p);
void	Touch3PtRectConvert (TouchMatrix_t * matrix_p,
		const TouchPoint_t * resolution_p,
		const TouchRect_t * rect_p);
void	Touch3PtRectGenerate (const TouchMatrix_t * matrix_p,
		const TouchPoint_t * resolution_p,
		TouchRect_t * rect_p);

/* Rectangular (4 corner) calibration functions. */
void    TouchRectFlip (TouchRect_t * rect_p, Boolean flip_X, Boolean flip_Y);
void	TouchRectCalibratePoints (const TouchPoint_t * resolution_p,
		TouchPoint_t * display_p,
		int margin);
int	TouchRectDisplayPointCalc (const TouchRect_t * rect_p,
		const TouchPoint_t * resolution_p,
		const TouchPoint_t * touch_p,
		TouchPoint_t * display_p);
TouchCoord_t
	TouchRectDisplayPointScale (TouchCoord_t touch,
		const TouchCalibAxis_t * cal_p,
		const TouchCoord_t resolution);
int     TouchRectTouchPointCalc (const TouchRect_t * rect_p,
		const TouchPoint_t * resolution_p,
		const TouchPoint_t * display_p,
		TouchPoint_t * touch_p);
TouchCoord_t
	TouchRectTouchPointScale (TouchCoord_t display,
		const TouchCalibAxis_t * cal_p,
		const TouchCoord_t resolution);

/* Touch calibration file functions. */
int	TouchCalibrationFileRead (const char * filename,
		const TouchPoint_t * resolution_p, TouchMatrix_t * matrix_p);
int     TouchCalibrationFileReadSearch (const char * base, const char * paths,
		const TouchPoint_t * resolution_p, TouchMatrix_t * matrix_p,
		char * filename, unsigned int fn_size);
int	TouchCalibrationFileWrite (const char * filename,
		const TouchMatrix_t * matrix_p, Boolean rectangular,
		const TouchPoint_t * resolution_p);
int	TouchCalibrationFileWriteSearch (const char * base, const char * paths,
		const TouchMatrix_t * matrix_p, Boolean rectangular,
		const TouchPoint_t * resolution_p, char * filename, unsigned int fn_size);

/* Touch calibration display functions. */
int     TouchCalibrationDisplayInit (TouchDisplayColor_t foreground,
		TouchDisplayColor_t background);
void    TouchCalibrationDisplayClose (Boolean clear);
int     TouchCalibrationDisplayResolution (TouchPoint_t * resolution_p);
void    TouchCalibrationDisplayClear (void);
int     TouchCalibrationDisplayPoint (TouchPoint_t point);
int     TouchCalibrationDisplayTestTrack (int touch_fd, TouchDisplayColor_t foreground,
		TouchDisplayColor_t background, long timeout);

/* Touch data structure formatting and display functions. */
void	TouchOutputPoint (void * out, const TouchPoint_t * point_p,
		const char * title);
void    TouchOutputPointArray (void * out, const TouchPoint_t * point_p,
		int size, const char * title);
void    TouchOutputPointMapping (void * out, const TouchPoint_t * point1_p,
		const TouchPoint_t * point2_p, const char * title);
void	TouchOutputRect (void * out, const TouchRect_t * rect_p,
		const char * title);
void	TouchOutputMatrix (void * out, const TouchMatrix_t * matrix_p,
		const char * title);


/* Rectangular coordinate flip inline functions. */
		static inline void
TouchRectFlipAxis (TouchCalibAxis_t * cal_p)
{
	TouchCoord_t temp;
	temp = cal_p->min;
	cal_p->min = cal_p->max;
	cal_p->max = temp;
	return;
}
		static inline Boolean
TouchRectFlipCheck (TouchCalibAxis_t * cal_p)
{
	Boolean flip = 0;

	if (cal_p->max < cal_p->min) {
		TouchRectFlipAxis (cal_p);
		flip = 1;
	}

	return (flip);
}

/* Rounding macros */
#define round_div(num, div) (((((num)*2)/(div))+1)>>1)
#define round_divr(type, num, div) (((type)(((num)*2)/(div))+1)>>1)
#define round_percent(num, percent) (((((num)*(percent))/50)+1)>>1)

#endif /* _TOUCH_CAL_H */

