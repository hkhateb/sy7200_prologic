/**
 *  	UniFingerSDK Main API
 *
 *  	@author sjlee@suprema.co.kr
 *  	@see    
 */

 /*  
 *  Copyright (c) 2004 Suprema Co., Ltd. All Rights Reserved.
 * 
 *  This software is the confidential and proprietary information of 
 *  Suprema Co., Ltd. ("Confidential Information").  You shall not
 *  disclose such Confidential Information and shall use it only in
 *  accordance with the terms of the license agreement you entered into
 *  with Suprema.
 */

 #ifndef __UNIFINGERSDK_API__
 #define __UNIFINGERSDK_API__

#include <time.h>
#include "uf_type.h"
#include "uf_error.h"
#include "uf_enroll.h"
#include "uf_gpio.h"
#include "uf_sensor.h"
#include "uf_sys_parameter.h"
#include "uf_packet.h"
#include "uf_image.h"
#include "uf_encrypt.h"
#include "uf_wiegand.h"
#include "uf_input.h"
#include "uf_output.h"
#include "uf_module.h"
#include "uf_template.h"

#ifdef UF_USING_CPP
extern "C" 
{
#endif

//
// Module initialization & configuration
//
UF_RET_CODE uf_initialize( const char* device_name, UF_BAUDRATE baudrate, UF_BOOL ascii_packet, UF_BOOL network_mode, UF_UINT16 module_id, int read_timeout );
UF_RET_CODE uf_set_read_timeout( int timeout );
UF_RET_CODE uf_finalize();
UF_RET_CODE uf_poll_module( int timeout );
UF_RET_CODE uf_check_system_status( UF_SYSTEM_STAUTS* status );
UF_RET_CODE uf_cancel();
//UF_RET_CODE uf_upgrade_firmware( UF_BYTE* firmware, UF_UINT32 size, UF_UINT32 data_packet_size );


//
// System parameters
//
UF_RET_CODE uf_write_system_parameter( UF_SYS_PARAM param_id, UF_UINT32 param_value );
UF_RET_CODE uf_save_system_parameter();
UF_RET_CODE uf_read_system_parameter( UF_SYS_PARAM param_id, UF_UINT32* param_value );


//
// Enroll finger prints
//
UF_RET_CODE uf_enroll_by_scan( UF_UINT32 user_id, UF_ENROLL_OPTION option, UF_UINT32 *new_user_id );
UF_RET_CODE uf_enroll_by_scan_a( UF_UINT32 user_id, UF_ENROLL_OPTION option, UF_UINT32* new_user_id );
UF_RET_CODE uf_enroll_by_image( UF_UINT32 user_id, UF_BYTE* image, UF_UINT32 image_size, UF_ENROLL_OPTION option, UF_UINT32 *new_user_id );
UF_RET_CODE uf_enroll_by_template( UF_UINT32 user_id, UF_BYTE* templ, UF_UINT32 templ_size, UF_ENROLL_OPTION option, UF_UINT32 *new_user_id );
UF_RET_CODE uf_enroll_by_wiegand( UF_ENROLL_OPTION option, UF_UINT32* user_id );
UF_RET_CODE uf_enroll_by_wiegand_a( UF_ENROLL_OPTION option, UF_UINT32* user_id );
UF_RET_CODE uf_enroll_by_image_x( UF_UINT32 user_id, UF_BYTE* image, UF_UINT32 image_size, UF_ENROLL_OPTION option, UF_UINT32 data_packet_size, UF_UINT32 *new_user_id );


//
// Verify finger prints
//
UF_RET_CODE uf_verify_by_scan( UF_UINT32 user_id, UF_UINT32* sub_id );
UF_RET_CODE uf_verify_by_image( UF_UINT32 user_id, UF_BYTE* image, UF_UINT32 image_size, UF_UINT32* sub_id );
UF_RET_CODE uf_verify_by_template( UF_UINT32 user_id, UF_BYTE* templ, UF_UINT32 templ_size, UF_UINT32* sub_id );
UF_RET_CODE uf_verify_host_template_by_scan( UF_UINT32 num_of_templ, UF_BYTE* templ, UF_UINT32 templ_size );
UF_RET_CODE uf_verify_by_wiegand( UF_UINT32* user_id, UF_UINT32* sub_id );
UF_RET_CODE uf_verify_by_image_x( UF_UINT32 user_id, UF_BYTE* image, UF_UINT32 image_size, UF_UINT32 data_packet_size, UF_UINT32* sub_id );


//
// Identify finger prints
//
UF_RET_CODE uf_identify_by_scan( UF_UINT32* user_id, UF_UINT32* sub_id );
UF_RET_CODE uf_identify_by_scan_with_limit( UF_UINT16 lower_limit, UF_UINT16 upper_limit, UF_UINT32* user_id, UF_UINT32* sub_id  );
UF_RET_CODE uf_identify_by_image( UF_BYTE* image, UF_UINT32 image_size, UF_UINT32* user_id, UF_UINT32* sub_id  );
UF_RET_CODE uf_identify_by_image_with_limit( UF_UINT16 lower_limit, UF_UINT16 upper_limit, UF_BYTE* image, UF_UINT32 image_size, UF_UINT32* user_id, UF_UINT32* sub_id  );
UF_RET_CODE uf_identify_by_template( UF_BYTE* templ, UF_UINT32 templ_size, UF_UINT32* user_id, UF_UINT32* sub_id );
UF_RET_CODE uf_identify_by_template_with_limit( UF_UINT16 lower_limit, UF_UINT16 upper_limit, UF_BYTE* templ, UF_UINT32 templ_size, UF_UINT32* user_id, UF_UINT32* sub_id );
UF_RET_CODE uf_identify_by_image_x( UF_BYTE* image, UF_UINT32 image_size, UF_UINT32 data_packet_size, UF_UINT32* user_id, UF_UINT32* sub_id );
UF_RET_CODE uf_identify_by_image_with_limit_x( UF_UINT16 lower_limit, UF_UINT16 upper_limit, UF_BYTE* image, UF_UINT32 image_size, UF_UINT32 data_packet_size, UF_UINT32* user_id, UF_UINT32* sub_id  );


//
// Sensors	
//
UF_RET_CODE uf_calibrate_sensor();



//
// Image managemnt
//
UF_RET_CODE uf_scan_image( UF_UINT32* image_size );
UF_RET_CODE uf_scan_image_x( UF_UINT32 data_packet_size, UF_UINT32* image_size );
UF_RET_CODE uf_read_image( UF_UINT32* image_size );
UF_RET_CODE uf_read_image_x( UF_UINT32 data_packet_size, UF_UINT32* image_size );
UF_RET_CODE uf_get_image_data( uf_image_t* image, UF_UINT32 image_size );
UF_RET_CODE uf_get_image_data_x( uf_image_t* image,  UF_UINT32 image_size );

//
// Manage templates 
//
UF_RET_CODE uf_read_template( UF_UINT32 user_id, UF_UINT32* templ_size );
UF_RET_CODE uf_scan_template( UF_UINT32* templ_size );
UF_RET_CODE uf_delete_template( UF_UINT32 user_id );
UF_RET_CODE uf_delete_template_by_scan( );
UF_RET_CODE uf_delete_template_by_scan_a( );
UF_RET_CODE uf_delete_template_by_scan_with_limit( UF_UINT16 lower_limit, UF_UINT16 upper_limit );
UF_RET_CODE uf_delete_template_by_scan_with_limit_a( UF_UINT16 lower_limit, UF_UINT16 upper_limit );
UF_RET_CODE uf_delete_all_templates();
UF_RET_CODE uf_delete_all_templates_a();
UF_RET_CODE uf_delete_by_wiegand( UF_UINT32* user_id );
UF_RET_CODE uf_delete_by_wiegand_a( UF_UINT32* user_id );

UF_RET_CODE uf_fix_all_provisional_templates();
UF_RET_CODE uf_delete_all_provisional_templates();

UF_RET_CODE uf_get_all_user_ids( UF_UINT32* user_count );
UF_RET_CODE uf_check_user_id( UF_UINT32 user_id, UF_UINT32* num_of_templates );
UF_RET_CODE uf_get_user_data( UF_UINT32* user_ids, UF_UINT32 user_count );

UF_RET_CODE uf_write_admin_level( UF_UINT32 user_id, UF_ADMIN_LEVEL admin_level );
UF_RET_CODE uf_read_admin_level( UF_UINT32 user_id, UF_ADMIN_LEVEL* admin_level );
UF_RET_CODE uf_clear_admin_level();


//
// Encryption
//
UF_RET_CODE uf_write_encryption_key( UF_BYTE* key );
UF_RET_CODE uf_scan_template_with_challenge_data( UF_UINT32 challenge_data, UF_UINT32* encrypted_templ_size );

//
// GPIO
//
UF_RET_CODE uf_read_gpio_config( UF_GPIO_INDEX gpio_id, UF_GPIO_MODE* gpio_mode, UF_UINT32* num_of_data );
UF_RET_CODE uf_write_gpio_config( UF_GPIO_INDEX gpio_id, UF_GPIO_MODE gpio_mode, UF_UINT32 num_of_data, UF_BYTE* data );
UF_RET_CODE uf_disable_gpio();
UF_RET_CODE uf_reset_gpio();
UF_RET_CODE uf_get_gpio_data( UF_GPIO_MODE gpio_mode, UF_BYTE* data, UF_UINT32 num_of_data );

//
// Wiegand
//
UF_RET_CODE uf_write_wiegand_config( UF_WIEGAND_PORT port, UF_UINT16 fc_bits, UF_UINT8 format, UF_WIEGAND_FUNCTION function, UF_UINT32 fc_code );
UF_RET_CODE uf_read_wiegand_config( UF_WIEGAND_PORT port, UF_UINT16* fc_bits, UF_UINT8* format, UF_WIEGAND_FUNCTION* function, UF_UINT32* fc_code );
UF_RET_CODE uf_get_wiegand_input( UF_WIEGAND_GET_INPUT_OPTION option, UF_UINT32* wiegand_id, UF_UINT32* facility_code );
UF_RET_CODE uf_set_wiegand_output( UF_UINT32 wiegand_id );
UF_RET_CODE uf_set_wiegand_mapping( UF_INPUT_FUNC input_function, UF_UINT32 wiegand_id );
UF_RET_CODE uf_list_wiegand_mapping( UF_UINT32* num_of_mapping, UF_UINT32* data_size );
UF_RET_CODE uf_clear_wiegand_mapping();
	
//
// Input port
//
UF_RET_CODE uf_write_input_config( UF_INPUT_FUNC input_func, UF_UINT32 min_time, UF_INPUT_PORT port );
UF_RET_CODE uf_read_input_config( UF_INPUT_PORT port, UF_INPUT_FUNC* input_func, UF_UINT32* min_time );
UF_RET_CODE uf_get_input_status( UF_INPUT_PORT port, UF_INPUT_OPTION option, UF_INPUT_STATUS* status );

//
// Output port and LED
//
UF_RET_CODE uf_write_output_config( UF_UINT16 delay, UF_UINT8 count, UF_OUTPUT_EVENT event, UF_UINT16 low, UF_UINT16 high, UF_OUTPUT_PORT port );
UF_RET_CODE uf_read_output_config( UF_OUTPUT_EVENT event, UF_OUTPUT_PORT port, UF_UINT16* delay, UF_UINT8* count, UF_UINT16* low, UF_UINT16* high );
UF_RET_CODE uf_read_enabled_event( UF_OUTPUT_PORT port, UF_UINT32* num_of_events );
UF_RET_CODE uf_get_enabled_event_data( UF_BYTE* events, UF_UINT32 num_of_events );
UF_RET_CODE uf_set_output( UF_OUTPUT_PORT port, UF_OUTPUT_SETTING set_value );

//
// User memory access
//
UF_RET_CODE uf_get_user_memory_length( UF_UINT32* length );
UF_RET_CODE uf_write_user_memory( UF_BYTE* user_data, UF_UINT32 offset, UF_UINT32 length );
UF_RET_CODE uf_read_user_memory( UF_UINT32 offset, UF_UINT32 length, UF_UINT32* user_data_len );

//
// Time/Date functions
//
UF_RET_CODE uf_set_time( time_t time_val );
UF_RET_CODE uf_get_time( time_t* time_val );

//
// Module log
//
UF_RET_CODE uf_get_log_count( UF_UINT32* used_count, UF_UINT32* total_count );
UF_RET_CODE uf_upload_log_data( UF_BYTE* log_data, UF_UINT32 start_index, UF_UINT32 count, UF_UINT32* available_count );
UF_RET_CODE uf_delete_oldest_log( UF_UINT32 count, UF_UINT32* erased_count );

//
// Raw packet api
//
UF_RET_CODE uf_send_packet( UF_UINT8 command, UF_UINT32 param, UF_UINT32 size, UF_UINT8 flag );
UF_RET_CODE uf_receive_packet( UF_BYTE* packet );
UF_RET_CODE uf_send_data_packet( UF_UINT8 command, UF_UINT16 index, UF_UINT16 num_of_data, UF_UINT32 data_size );
UF_RET_CODE uf_get_raw_data( UF_BYTE* data_buf, UF_UINT32 data_size );
UF_RET_CODE uf_send_raw_data( UF_BYTE* data_buf, UF_UINT32 data_size );
UF_RET_CODE uf_send_raw_data_x(      UF_UINT8 command, UF_BYTE* data_buf, UF_UINT32 data_size, UF_UINT32 data_packet_size );



#ifdef UF_USING_CPP
}
#endif


#endif

 

