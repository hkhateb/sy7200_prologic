//managed pointer. When no references left, will delete its payload

#ifndef _TAManaged_H_
#define _TAManaged_H_

template < class T > class managed {
  int* c ;
  T* v ;
 public:
  managed (T* t = 0 ){
    c = new int ;
    *c = 1 ;
    v = t ;
  }

  managed ( const managed& r ){
    c = r .c ;
    *c ++ ;
    v = r .v ;
  }

  managed& operator = ( T* r ){
    deref ();
    c = new int ;
    *c = 1 ;
    v = r ;
    return * this ;
  }

  managed& operator = ( const managed& r ){
    if ( & r != this ){
      deref ();
      c = r .c ;
      *c ++ ;
      v = r .v ;
    }
    return * this ;
  }
  
  ~managed (){
    deref ();
  }


  operator T* () { return v ;}
  operator const T* () const { return v ;}

  T* operator -> () { return v ;}
  const T* operator -> () const { return v ;}

 private:
  void deref (){
    * c -- ;
    if ( 0 == * c ){
      delete v ;
      delete c ;
    }
  }

};

#endif //_TAManaged_H_
