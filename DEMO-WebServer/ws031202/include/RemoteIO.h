/*
	RemoteIO.h:

	Standard definitions for the RemoteIO subsystem.

	Copyright (c) 2006 - Time America, Inc.
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   15 May 2006
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/01 18:43:11 $

*/


#if ! defined _REMOTEIO

/* Include generic definitions. */
#include "Gen_Def.h"
#include <unistd.h>

/* Default communications parameters. */
#if defined (LIBNE_CYGWIN)
static const char RIO_DEFAULT_DEVICE_NAME [] = "/dev/com1";
#else
static const char RIO_DEFAULT_DEVICE_NAME [] = "/dev/tts/5";
#endif
#define RIO_DEFAULT_BAUDRATE	9600
#define RIO_DEFAULT_TIMEOUT	500
#define RIO_DEFAULT_ID		1

/* Status codes for the RemoteIO subsystem. */
#if ! defined (SC_BASE_RIO)
#define SC_BASE_RIO	2000
#endif
typedef enum {
	SC_RIO_GENERIC = SC_BASE_RIO,
	SC_RIO_NO_MEMORY,
	SC_RIO_COMM_NOT_OPEN,
	SC_RIO_WRITE_FAIL,
	SC_RIO_READ_FAIL,
	SC_RIO_READ_TIMEOUT,
	SC_RIO_READ_OVERRUN,
	SC_RIO_MSG_NAK,
	SC_RIO_MSG_DATA,
	SC_RIO_MSG_UNKNOWN,
	SC_RIO_COMMAND,
	SC_RIO_WRONG_MSG,
	SC_RIO_NO_MODULE,
	SC_RIO_UNKNOWN_MODULE,
	SC_RIO_WRONG_MODULE,
	SC_RIO_SMALL_BUFFER,
	SC_RIO_INTERRUPT,
	SC_RIO_BAD_RATE,
	SC_RIO_TIMEOUT,
	SC_RIO_DURATION,
	}
RemoteIOCode_e;


/* Module object and handle types. */
struct RIOComm;
typedef struct RIOComm * RIOComm_h;
struct RIOModule;
typedef struct RIOModule * RIOModule_h;
struct RIOModuleCapability;

/* Subsystem data types. */
typedef UBin8 RIOId_t;
typedef Bit16 RIOBits_t;
typedef UBin8 RIOChannel_t;
typedef SBin15 RIOAnalog_t;
typedef UBin32 RIOTime_t;

/* Module family enumeration. */
typedef enum {
	RIO_FAM_UNDEFINED,
	RIO_FAM_ADAM,
	MAX_RIO_FAM
}
RIOFamily_e;

/* Remote I/O module types. */
typedef enum {
	RIO_MOD_UNDEFINED = 0,

	RIO_MOD_ADAM_4050,
	RIO_MOD_ADAM_4055,
	RIO_MOD_ADAM_4056S,
	RIO_MOD_ADAM_4056SO,
	RIO_MOD_ADAM_4060,
	RIO_MOD_ADAM_4068,
	RIO_MOD_ADAM_4069,

	MAX_RIO_MOD
}
RIOModuleType_e;

/* Message type enumeration. */
typedef enum {
	RIO_MSG_UNKNOWN = 0,

	RIO_MSG_REPLY_ACK,
	RIO_MSG_REPLY_NAK,

	RIO_MSG_QUERY_VERSION,
	RIO_MSG_QUERY_MODULE,
	RIO_MSG_QUERY_CONFIG,
	RIO_MSG_QUERY_STATUS,
	RIO_MSG_QUERY_CAPTURE,
	RIO_MSG_QUERY_RESET,

	RIO_MSG_SAMPLE,
	RIO_MSG_SET_CONFIG,

	RIO_MSG_DIGITAL_IN,
	RIO_MSG_DIGITAL_IN_ALL,
	RIO_MSG_DIGITAL_OUT,
	RIO_MSG_DIGITAL_OUT_ALL,
	RIO_MSG_DIGITAL_OUT_GET,
	RIO_MSG_DIGITAL_OUT_GET_ALL,
	RIO_MSG_DIGITAL_IO_GET_ALL,

	MAX_RIO_MSG
}
RIOMessage_e;

/* Family specific protocol substate information. */
typedef struct {
	RIOMessage_e message;
	SBin31 primary;
}
RIOSubState_t;

/* Digital matching selectors. */
typedef enum {
	RIO_MATCH_OFF	= -1,
	RIO_MATCH_ALL	= 0,
	RIO_MATCH_ANY	= 1,
}
RIOMatch_e;


/* Data buffer structure. */
#if ! defined (MAX_DATA_BUFFER)
#define MAX_DATA_BUFFER		64
#endif
typedef struct {
	size_t size;
	char data [MAX_DATA_BUFFER];
}
RIOBuffer_t;

/* Module enumeration versus name table entry. */
typedef struct RIOTypeList {
	RIOModuleType_e type;
	const char * name;
}
RIOTypeList_t;

/* Subsystem data structures. */
typedef struct {
	/* Hex digits in fields. */
	UBin8 output;
	UBin8 input;
	UBin8 pad;
	UBin8 total;
}
RIODigitalReplyForm_t;

/* Matching and callback data types and structures. */
typedef Bit16 RIOEventFlags_t;
#define RIO_EVENTS_DIG_IN_ALL		0x8000
#define RIO_EVENTS_DIG_IN_ANY		0x4000
#define RIO_EVENTS_DIG_IN		(RIO_EVENTS_DIG_IN_ALL|RIO_EVENTS_DIG_IN_ANY)
#define RIO_EVENTS_DIG_OUT_ALL		0x2000
#define RIO_EVENTS_DIG_OUT_ANY		0x1000
#define RIO_EVENTS_DIG_OUT		(RIO_EVENTS_DIG_OUT_ALL|RIO_EVENTS_DIG_OUT_ANY)
#define RIO_EVENTS_DIG			(RIO_EVENTS_DIG_IN|RIO_EVENTS_DIG_OUT)
#define RIO_EVENTS_ANLG_HIGH		0x0080
#define RIO_EVENTS_ANLG_LOW		0x0040
#define RIO_EVENTS_ANLG_BAND		(RIO_EVENTS_ANLG_HIGH|RIO_EVENTS_ANLG_LOW)
#define RIO_EVENTS_ANLG_DELTA_PLUS	0x0008
#define RIO_EVENTS_ANLG_DELTA_MINUS	0x0004
#define RIO_EVENTS_ANLG_DELTA		(RIO_EVENTS_ANLG_DELTA_PLUS|RIO_EVENTS_ANLG_DELTA_MINUS)
#define RIO_EVENTS_ANLG			(RIO_EVENTS_ANLG_BAND|RIO_EVENTS_ANLG_DELTA)

typedef enum {
	RIO_WATCH_NONE = 0,

	RIO_WATCH_DIGITAL,
	RIO_WATCH_ANALOG,
	MAX_RIO_WATCH
}
RIOWatchType_e;


/*** Poll and watch data and types. ***/

typedef struct RIOPoll {
	RIOModule_h module_h;
	RIOMessage_e message;
	const char * data;
	RIOTime_t interval;
}
RIOPoll_t;

struct RIOWatch;
typedef Code RIOEventDigital_f (void * data_h, RIOModule_h module_h, const struct RIOWatch * watch_p,
		RIOEventFlags_t flags, RIOBits_t active, RIOBits_t bits);
typedef Code RIOEventAnalog_f (void * data_h, RIOModule_h module_h, const struct RIOWatch * watch_p,
		RIOEventFlags_t flags, RIOChannel_t channel, RIOAnalog_t value);
typedef RIOEventDigital_f * RIOEventDigital_fp;
typedef RIOEventAnalog_f * RIOEventAnalog_fp;

typedef struct {
	RIOBits_t invert;
	RIOBits_t pattern;
	RIOBits_t mask;
	RIOMatch_e match;
}
RIODigitalTrigger_t;
typedef struct RIODigitalWatch {
	struct {
		RIODigitalTrigger_t input;
		RIODigitalTrigger_t output;
	}
	trigger;
	struct {
		RIOEventDigital_fp func_p;
		void * data_h;
	}
	callback;
}
RIODigitalWatch_t;

typedef struct {
	RIOAnalog_t band;
	RIOAnalog_t delta;
}
RIOAnalogTrigger_t;
typedef struct RIOAnalogWatch {
	struct {
		RIOAnalogTrigger_t input [8];
		RIOAnalogTrigger_t output [4];
	}
	match;
	struct {
		RIOEventAnalog_fp func_p;
		void * data_h;
	}
	callback;
}
RIOAnalogWatch_t;

typedef struct RIOWatch {
	RIOModule_h module_h;
	RIOWatchType_e select;
	union {
		RIODigitalWatch_t digital;
		RIOAnalogWatch_t analog;
	};
}
RIOWatch_t;


/* Function and function handle data types. */
typedef RIOBits_t RIODigitalMatch_f (RIOBits_t input, RIOBits_t pattern, RIOBits_t mask);
typedef Code RIOFamilyForm_f (RIOBuffer_t * msg_p, RIOId_t id,
		RIOMessage_e message, const char * send_data);
typedef Code RIOFamilyParse_f (RIOBuffer_t * msg_p, RIOMessage_e * message_p,
		RIOId_t * id_p, const char ** data_pp, RIOSubState_t * sub_p);
typedef Code RIOFamilyDigitalIn_f (RIOBuffer_t * buf_p, const char * data_p,
		const struct RIOModuleCapability * cap_p,
		UBin8 bit, Boolean * state_p);
typedef Code RIOFamilyDigitalInAll_f (RIOBuffer_t * buf_p, const char * data_p,
                const struct RIOModuleCapability * cap_p, RIOBits_t * input_p);
typedef Code RIOFamilyDigitalOut_f (RIOBuffer_t * buf_p, RIOId_t id,
                const struct RIOModuleCapability * cap_p, UBin8 bit, Boolean state);
typedef Code RIOFamilyDigitalOutAll_f (RIOBuffer_t * buf_p, RIOId_t id,
		const struct RIOModuleCapability * cap_p, RIOBits_t bits);
typedef Code RIOFamilyDigitalOutGet_f (RIOBuffer_t * buf_p, const char * data_p,
                const struct RIOModuleCapability * cap_p, UBin8 bit, Boolean * state_p);
typedef Code RIOFamilyDigitalOutGetAll_f (RIOBuffer_t * buf_p, const char * data_p,
                const struct RIOModuleCapability * cap_p, RIOBits_t * output_p);
typedef Code RIOFamilyDigitalIOGetAll_f (RIOBuffer_t * buf_p, const char * data_p,
		const struct RIOModuleCapability * cap_p, RIOBits_t * input_p,
		RIOBits_t * output_p);

/* Module family capabilities data structures. */
typedef struct RIOCommCapability {
	struct {
		RIOFamily_e id;
		const char * name;
		RIOFamilyForm_f *		form;
		RIOFamilyParse_f *		parse;
	}
	family;
}
RIOCommCapability_t;

typedef struct RIOModuleCapability {
	RIOModuleType_e type;
	struct {
		const char * name;		/* As returned by module. */
		RIODigitalReplyForm_t digital;	/* Digital reply data format. */
	}
	config;
	struct {
		RIOFamily_e id;
		RIOFamilyDigitalIn_f *		digital_in;
		RIOFamilyDigitalInAll_f *	digital_in_all;
		RIOFamilyDigitalOut_f *		digital_out;
		RIOFamilyDigitalOutAll_f *	digital_out_all;
		RIOFamilyDigitalOutGet_f *	digital_out_get;
		RIOFamilyDigitalOutGetAll_f *	digital_out_get_all;
		RIOFamilyDigitalIOGetAll_f *	digital_io_get_all;
	}
	family;
}
RIOModuleCapability_t;


/* External data structures. */
extern const RIOCommCapability_t RIOComm_ADAM;
extern const RIOModuleCapability_t * const RIOList_ADAM [];

/* External function prototypes for RemoteIO subsystem. */
Code    rio_comm_open (RIOComm_h * comm_hp, const char * port,
		int baudrate, int timeout, RIOFamily_e family);
Code    rio_comm_close (RIOComm_h * comm_hp);
void    rio_comm_timeout_set (RIOComm_h comm_p, int timeout);
Code    rio_comm_write_buffer (RIOComm_h comm_p, RIOBuffer_t * msg_p);
Code    rio_comm_read_buffer (RIOComm_h comm_p, RIOBuffer_t * msg_p);
Code    rio_comm_form (RIOComm_h comm_p, RIOBuffer_t * msg_p, RIOId_t id,
		RIOMessage_e message, const char * send_data);
Code    rio_comm_parse (RIOComm_h comm_p, RIOBuffer_t * msg_p,
		RIOMessage_e * message_p, RIOId_t * id_p, const char ** data_pp);
Code    rio_comm_read (RIOComm_h comm_p, RIOBuffer_t * msg_p,
		RIOMessage_e * message_p, RIOId_t * id_p, const char ** data_pp);

Code    rio_module_init (RIOModule_h * module_hp, const char * name, RIOModuleType_e type,
		RIOId_t id, RIOComm_h comm_h, Boolean verify);
Code    rio_module_release (RIOModule_h * module_hp);
RIOId_t rio_module_id (RIOModule_h module_p);
RIOModuleType_e
	rio_module_type (RIOModule_h module_p);
const char *
	rio_module_type_name (RIOModule_h module_p);
const char *
	rio_module_tag_get (RIOModule_h module_p);
void    rio_module_tag_set (RIOModule_h module_p, const char * tag);
RIOComm_h
	rio_module_comm_get (RIOModule_h module_p);
const RIOModuleCapability_t * const
	rio_module_cap_get (RIOModule_h module_p);
Code    rio_module_transaction (RIOModule_h module_p, RIOMessage_e message,
		RIOId_t * id_p, RIOBuffer_t * send_p, RIOBuffer_t * reply_p,
		const char ** data_pp);
Code	rio_module_query (RIOModule_h module_p, RIOMessage_e message, RIOId_t * id_p,
                RIOBuffer_t * reply_p, const char ** data_pp);
Code    rio_module_query_string (RIOModule_h module_p, RIOMessage_e message,
		RIOId_t * id_p, char * reply_p, size_t size);
Code    rio_module_query_type (RIOModule_h module_p, RIOId_t * id_p,
		RIOModuleType_e * type_p, char * reply_p, size_t size);
RIOModuleType_e
	rio_module_lookup_type (const char * name);
const char *
	rio_module_lookup_type_name (RIOModuleType_e type);
const RIOModuleCapability_t * const
	rio_module_lookup_capabilties (RIOModuleType_e type, RIOFamily_e family);
Code    rio_module_digital_in (RIOModule_h module_p, UBin8 bit, Boolean * state_p);
Code    rio_module_digital_in_all (RIOModule_h module_p, RIOBits_t * input_p);
Code    rio_module_digital_out (RIOModule_h module_p, UBin8 bit, Boolean state);
Code    rio_module_digital_out_all (RIOModule_h module_p, RIOBits_t bits);
Code    rio_module_digital_out_get (RIOModule_h module_p, UBin8 bit, Boolean * state_p);
Code    rio_module_digital_out_get_all (RIOModule_h module_p, RIOBits_t * output_p);
Code    rio_module_digital_io_get_all (RIOModule_h module_p,
		RIOBits_t * input_p, RIOBits_t * output_p);
Code    rio_module_digital_out_pulse (RIOModule_h module_p, UBin8 bit,
		Boolean state, UBin32 duration);
Code    rio_module_digital_in_match (RIOModule_h module_p, RIOBits_t pattern,
		RIOBits_t mask, RIOMatch_e match, RIOTime_t rate,
		RIOTime_t timeout, RIOBits_t * input_p);

Code    rio_scan_poll (RIOComm_h comm_h, RIOPoll_t const * const * poll_lp,
		RIOWatch_t const * const * watch_lp, RIOTime_t timeout, Boolean error_stop);
Code    rio_scan_listen (RIOComm_h comm_h, RIOWatch_t const * const * watch_lp,
		RIOTime_t timeout, Boolean error_stop);

const char *
	rio_error (Code code);

/* Bit string matching functions. */
RIODigitalMatch_f		rio_digital_match_any;
RIODigitalMatch_f		rio_digital_match_all;

#define _REMOTEIO
#endif


