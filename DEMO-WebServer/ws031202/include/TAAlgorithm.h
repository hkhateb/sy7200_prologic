#ifndef _TAAlgorithm_H_
#define _TAAlgorithm_H_

//******************************************************************
template < class I1 , class P > I1 TAForeach ( I1 b , I1 e , P p ){
  while ( b != e ){
    p ( * b );
    ++ b ;
  }
  return b ;
}

template < class R , class C , class A1 > class TABinder {
 public:
  TABinder ( R (C::*f) ( A1 ) , A1 a ) : m_f ( f ) , m_a ( a ) {}
  R operator () ( C* c ){
    return ((c)->*m_f) ( m_a );
  }

 private:
  R (C::*m_f)(A1);
  A1 m_a ;
};

template < class R , class A1 , class A2 > class TABinder_f {
 public:
  TABinder_f ( R (*f) ( A1 , A2 ) , A2 a ) : m_f ( f ) , m_a ( a ) {}
  R operator () ( A1 a ){
    return m_f ( a , m_a );
  }

 private:
  R (*m_f) ( A1 , A2 );
  A2 m_a ;
};

template < class R , class C , class A1 > TABinder < R , C , A1 > 
TABind2nd ( R (C ::*f) ( A1 ), A1 a ){
  return TABinder < R , C , A1 > ( f , a );
}

template < class R , class A1 , class A2 > TABinder_f < R , A1 , A2 > 
TABind2nd ( R (*f) ( A1 , A2 ), A2 a ){
  return TABinder_f < R , A1 , A2 > ( f , a );
}

//******************************************************************
template < class I1 , class V > I1 TAFind ( I1 b , I1 e , V v ){
  while ( b != e && *b != v ){
    b ++ ;
  }
  return b ;
}

//******************************************************************
template < class I1 , class P > I1 TAFind_if ( I1 b , I1 e , P p ){
  while ( b != e && ! ( p ( * b ))){
    ++ b ;
  }
  return b ;
}


//******************************************************************
template < class I1 , class I2 > I2 TACopy ( I1 b , I1 e , I2 o ){
  while ( b != e ){
    *o = *b ;
    ++ o ;
    ++ b ;
  }
  return o ;
}

//******************************************************************
template < class I1 , class I2 , class O , class F > O TATransform ( I1 b , I1 e , I2 b2 , O o , F f ){
  while ( b != e ){
    *o = f ( *b , *b2 );
    b ++ ;
    b2 ++ ;
    o ++ ;
  }
  return o ;
}

//******************************************************************
template < class I1 , class O , class F > O TATransform ( I1 b , I1 e , O o , F f ){
  while ( b != e ){
    *o = f ( *b );
    b ++ ;
    o ++ ;
  }
  return o ;
}

template < class I , class V > void TASort ( I b , I e ){
  I i ;
  while ( b != e ){
    i = b ;
    while ( i != e ){
      if ( *b > *i ){
	V v = *b ;
	*b = *i ;
	*i = v ;
      }
      i ++ ;
    }
    b ++ ;
  }
}

template < class I , class V , class F > void TASort_if ( I b , I e , F f ){
  I i ;
  while ( b != e ){
    i = b ;
    while ( i != e ){
      if ( f ( * i , * b )){
	V v = *b ;
	*b = *i ;
	*i = v ;
      }
      i ++ ;
    }
    b ++ ;
  }
}

#endif //_TAAlgorithm_H_


