#ifndef _LED_H_
#define _LED_H_

enum Light
{
	LIGHT_RED = 1,
	LIGHT_YELLOW,
	LIGHT_GREEN
};

class LED
{
public:
	LED();
	virtual ~LED();
	void TurnOn(Light light);
	void TurnOff(Light light);
};

#endif //_LED_H_
