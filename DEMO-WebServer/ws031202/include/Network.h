//Copyright (C) Time America, Inc. All Rights Reserved
//$Id: Network.h,v 1.1 2005/11/11 23:53:21 walterm Exp $

#ifndef __NETWORK_H_INCLUDED_
#define __NETWORK_H_INCLUDED_

int getClockSerialNumber ( char* buf , unsigned int buflen );//id returned in buf. return is length of returned string (as given by strlen).

#endif //__NETWORK_H_INCLUDED_
