#ifndef _EVENT_H_
#define _EVENT_H_

enum Event
  {
    EVENT_NOOP = 0 ,
    EVENT_KEYBOARD,
    EVENT_BARCODE,
    EVENT_MAGCARD1,
    EVENT_MAGCARD2,
    EVENT_MAGCARD3,
    EVENT_PROXCARD,
    EVENT_TIMER,
    EVENT_SOCKET
  };

typedef void EventCallback_f (Event event, const char * data_p,
        unsigned int size, void * userdata_p);
typedef EventCallback_f * EventCallback_fp;
typedef EventCallback_f * EventCallback;

#endif //_EVENT_H_
