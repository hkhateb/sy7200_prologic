# Microsoft Developer Studio Generated NMAKE File, Based on webs.dsp
!IF "$(CFG)" == ""
CFG=webs - Win32 Debug SSL
!MESSAGE No configuration specified. Defaulting to webs - Win32 Debug SSL.
!ENDIF 

!IF "$(CFG)" != "webs - Win32 Release" && "$(CFG)" != "webs - Win32 Debug" && "$(CFG)" != "webs - Win32 Debug SSL" && "$(CFG)" != "webs - Win32 Release SSL"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "webs.mak" CFG="webs - Win32 Debug SSL"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "webs - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "webs - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "webs - Win32 Debug SSL" (based on "Win32 (x86) Application")
!MESSAGE "webs - Win32 Release SSL" (based on "Win32 (x86) Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "webs - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

ALL : "$(OUTDIR)\webs.exe" "$(OUTDIR)\webs.bsc"


CLEAN :
	-@erase "$(INTDIR)\asp.obj"
	-@erase "$(INTDIR)\asp.sbr"
	-@erase "$(INTDIR)\balloc.obj"
	-@erase "$(INTDIR)\balloc.sbr"
	-@erase "$(INTDIR)\base64.obj"
	-@erase "$(INTDIR)\base64.sbr"
	-@erase "$(INTDIR)\cgi.obj"
	-@erase "$(INTDIR)\cgi.sbr"
	-@erase "$(INTDIR)\datetime.obj"
	-@erase "$(INTDIR)\datetime.sbr"
	-@erase "$(INTDIR)\default.obj"
	-@erase "$(INTDIR)\default.sbr"
	-@erase "$(INTDIR)\ejlex.obj"
	-@erase "$(INTDIR)\ejlex.sbr"
	-@erase "$(INTDIR)\ejparse.obj"
	-@erase "$(INTDIR)\ejparse.sbr"
	-@erase "$(INTDIR)\emfdb.obj"
	-@erase "$(INTDIR)\emfdb.sbr"
	-@erase "$(INTDIR)\fingerprint.obj"
	-@erase "$(INTDIR)\fingerprint.sbr"
	-@erase "$(INTDIR)\form.obj"
	-@erase "$(INTDIR)\form.sbr"
	-@erase "$(INTDIR)\h.obj"
	-@erase "$(INTDIR)\h.sbr"
	-@erase "$(INTDIR)\handler.obj"
	-@erase "$(INTDIR)\handler.sbr"
	-@erase "$(INTDIR)\main.obj"
	-@erase "$(INTDIR)\main.sbr"
	-@erase "$(INTDIR)\md5c.obj"
	-@erase "$(INTDIR)\md5c.sbr"
	-@erase "$(INTDIR)\mime.obj"
	-@erase "$(INTDIR)\mime.sbr"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\misc.sbr"
	-@erase "$(INTDIR)\page.obj"
	-@erase "$(INTDIR)\page.sbr"
	-@erase "$(INTDIR)\ringq.obj"
	-@erase "$(INTDIR)\ringq.sbr"
	-@erase "$(INTDIR)\rom.obj"
	-@erase "$(INTDIR)\rom.sbr"
	-@erase "$(INTDIR)\security.obj"
	-@erase "$(INTDIR)\security.sbr"
	-@erase "$(INTDIR)\setclockmode.obj"
	-@erase "$(INTDIR)\setclockmode.sbr"
	-@erase "$(INTDIR)\setproxy.obj"
	-@erase "$(INTDIR)\setproxy.sbr"
	-@erase "$(INTDIR)\setWebSer.obj"
	-@erase "$(INTDIR)\setWebSer.sbr"
	-@erase "$(INTDIR)\sock.obj"
	-@erase "$(INTDIR)\sock.sbr"
	-@erase "$(INTDIR)\sockGen.obj"
	-@erase "$(INTDIR)\sockGen.sbr"
	-@erase "$(INTDIR)\sym.obj"
	-@erase "$(INTDIR)\sym.sbr"
	-@erase "$(INTDIR)\uemf.obj"
	-@erase "$(INTDIR)\uemf.sbr"
	-@erase "$(INTDIR)\um.obj"
	-@erase "$(INTDIR)\um.sbr"
	-@erase "$(INTDIR)\umui.obj"
	-@erase "$(INTDIR)\umui.sbr"
	-@erase "$(INTDIR)\url.obj"
	-@erase "$(INTDIR)\url.sbr"
	-@erase "$(INTDIR)\value.obj"
	-@erase "$(INTDIR)\value.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\webrom.obj"
	-@erase "$(INTDIR)\webrom.sbr"
	-@erase "$(INTDIR)\webs.obj"
	-@erase "$(INTDIR)\webs.sbr"
	-@erase "$(INTDIR)\websda.obj"
	-@erase "$(INTDIR)\websda.sbr"
	-@erase "$(INTDIR)\websuemf.obj"
	-@erase "$(INTDIR)\websuemf.sbr"
	-@erase "$(INTDIR)\XMLProperties.obj"
	-@erase "$(INTDIR)\XMLProperties.sbr"
	-@erase "$(OUTDIR)\webs.bsc"
	-@erase "$(OUTDIR)\webs.exe"
	-@erase "$(OUTDIR)\webs.map"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /Zp1 /ML /W3 /I "../include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "WEBS" /D "UEMF" /D "WIN" /D "DIGEST_ACCESS_SUPPORT" /D "USER_MANAGEMENT_SUPPORT" /FR"$(INTDIR)\\" /Fp"$(INTDIR)\webs.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/o"$(OUTDIR)\webs.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\asp.sbr" \
	"$(INTDIR)\balloc.sbr" \
	"$(INTDIR)\base64.sbr" \
	"$(INTDIR)\cgi.sbr" \
	"$(INTDIR)\datetime.sbr" \
	"$(INTDIR)\default.sbr" \
	"$(INTDIR)\ejlex.sbr" \
	"$(INTDIR)\ejparse.sbr" \
	"$(INTDIR)\emfdb.sbr" \
	"$(INTDIR)\fingerprint.sbr" \
	"$(INTDIR)\form.sbr" \
	"$(INTDIR)\h.sbr" \
	"$(INTDIR)\handler.sbr" \
	"$(INTDIR)\main.sbr" \
	"$(INTDIR)\md5c.sbr" \
	"$(INTDIR)\mime.sbr" \
	"$(INTDIR)\misc.sbr" \
	"$(INTDIR)\page.sbr" \
	"$(INTDIR)\ringq.sbr" \
	"$(INTDIR)\rom.sbr" \
	"$(INTDIR)\security.sbr" \
	"$(INTDIR)\setclockmode.sbr" \
	"$(INTDIR)\setproxy.sbr" \
	"$(INTDIR)\setWebSer.sbr" \
	"$(INTDIR)\sock.sbr" \
	"$(INTDIR)\sockGen.sbr" \
	"$(INTDIR)\sym.sbr" \
	"$(INTDIR)\uemf.sbr" \
	"$(INTDIR)\um.sbr" \
	"$(INTDIR)\umui.sbr" \
	"$(INTDIR)\url.sbr" \
	"$(INTDIR)\value.sbr" \
	"$(INTDIR)\webrom.sbr" \
	"$(INTDIR)\webs.sbr" \
	"$(INTDIR)\websda.sbr" \
	"$(INTDIR)\websuemf.sbr" \
	"$(INTDIR)\XMLProperties.sbr"

"$(OUTDIR)\webs.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=wsock32.lib kernel32.lib user32.lib libs\libexpat.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\webs.pdb" /map:"$(INTDIR)\webs.map" /machine:I386 /out:"$(OUTDIR)\webs.exe" 
LINK32_OBJS= \
	"$(INTDIR)\asp.obj" \
	"$(INTDIR)\balloc.obj" \
	"$(INTDIR)\base64.obj" \
	"$(INTDIR)\cgi.obj" \
	"$(INTDIR)\datetime.obj" \
	"$(INTDIR)\default.obj" \
	"$(INTDIR)\ejlex.obj" \
	"$(INTDIR)\ejparse.obj" \
	"$(INTDIR)\emfdb.obj" \
	"$(INTDIR)\fingerprint.obj" \
	"$(INTDIR)\form.obj" \
	"$(INTDIR)\h.obj" \
	"$(INTDIR)\handler.obj" \
	"$(INTDIR)\main.obj" \
	"$(INTDIR)\md5c.obj" \
	"$(INTDIR)\mime.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\page.obj" \
	"$(INTDIR)\ringq.obj" \
	"$(INTDIR)\rom.obj" \
	"$(INTDIR)\security.obj" \
	"$(INTDIR)\setclockmode.obj" \
	"$(INTDIR)\setproxy.obj" \
	"$(INTDIR)\setWebSer.obj" \
	"$(INTDIR)\sock.obj" \
	"$(INTDIR)\sockGen.obj" \
	"$(INTDIR)\sym.obj" \
	"$(INTDIR)\uemf.obj" \
	"$(INTDIR)\um.obj" \
	"$(INTDIR)\umui.obj" \
	"$(INTDIR)\url.obj" \
	"$(INTDIR)\value.obj" \
	"$(INTDIR)\webrom.obj" \
	"$(INTDIR)\webs.obj" \
	"$(INTDIR)\websda.obj" \
	"$(INTDIR)\websuemf.obj" \
	"$(INTDIR)\XMLProperties.obj"

"$(OUTDIR)\webs.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "webs - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

ALL : "$(OUTDIR)\webs.exe" "$(OUTDIR)\webs.bsc"


CLEAN :
	-@erase "$(INTDIR)\asp.obj"
	-@erase "$(INTDIR)\asp.sbr"
	-@erase "$(INTDIR)\balloc.obj"
	-@erase "$(INTDIR)\balloc.sbr"
	-@erase "$(INTDIR)\base64.obj"
	-@erase "$(INTDIR)\base64.sbr"
	-@erase "$(INTDIR)\cgi.obj"
	-@erase "$(INTDIR)\cgi.sbr"
	-@erase "$(INTDIR)\datetime.obj"
	-@erase "$(INTDIR)\datetime.sbr"
	-@erase "$(INTDIR)\default.obj"
	-@erase "$(INTDIR)\default.sbr"
	-@erase "$(INTDIR)\ejlex.obj"
	-@erase "$(INTDIR)\ejlex.sbr"
	-@erase "$(INTDIR)\ejparse.obj"
	-@erase "$(INTDIR)\ejparse.sbr"
	-@erase "$(INTDIR)\emfdb.obj"
	-@erase "$(INTDIR)\emfdb.sbr"
	-@erase "$(INTDIR)\fingerprint.obj"
	-@erase "$(INTDIR)\fingerprint.sbr"
	-@erase "$(INTDIR)\form.obj"
	-@erase "$(INTDIR)\form.sbr"
	-@erase "$(INTDIR)\h.obj"
	-@erase "$(INTDIR)\h.sbr"
	-@erase "$(INTDIR)\handler.obj"
	-@erase "$(INTDIR)\handler.sbr"
	-@erase "$(INTDIR)\main.obj"
	-@erase "$(INTDIR)\main.sbr"
	-@erase "$(INTDIR)\md5c.obj"
	-@erase "$(INTDIR)\md5c.sbr"
	-@erase "$(INTDIR)\mime.obj"
	-@erase "$(INTDIR)\mime.sbr"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\misc.sbr"
	-@erase "$(INTDIR)\page.obj"
	-@erase "$(INTDIR)\page.sbr"
	-@erase "$(INTDIR)\ringq.obj"
	-@erase "$(INTDIR)\ringq.sbr"
	-@erase "$(INTDIR)\rom.obj"
	-@erase "$(INTDIR)\rom.sbr"
	-@erase "$(INTDIR)\security.obj"
	-@erase "$(INTDIR)\security.sbr"
	-@erase "$(INTDIR)\setclockmode.obj"
	-@erase "$(INTDIR)\setclockmode.sbr"
	-@erase "$(INTDIR)\setproxy.obj"
	-@erase "$(INTDIR)\setproxy.sbr"
	-@erase "$(INTDIR)\setWebSer.obj"
	-@erase "$(INTDIR)\setWebSer.sbr"
	-@erase "$(INTDIR)\sock.obj"
	-@erase "$(INTDIR)\sock.sbr"
	-@erase "$(INTDIR)\sockGen.obj"
	-@erase "$(INTDIR)\sockGen.sbr"
	-@erase "$(INTDIR)\sym.obj"
	-@erase "$(INTDIR)\sym.sbr"
	-@erase "$(INTDIR)\uemf.obj"
	-@erase "$(INTDIR)\uemf.sbr"
	-@erase "$(INTDIR)\um.obj"
	-@erase "$(INTDIR)\um.sbr"
	-@erase "$(INTDIR)\umui.obj"
	-@erase "$(INTDIR)\umui.sbr"
	-@erase "$(INTDIR)\url.obj"
	-@erase "$(INTDIR)\url.sbr"
	-@erase "$(INTDIR)\value.obj"
	-@erase "$(INTDIR)\value.sbr"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\webrom.obj"
	-@erase "$(INTDIR)\webrom.sbr"
	-@erase "$(INTDIR)\webs.obj"
	-@erase "$(INTDIR)\webs.sbr"
	-@erase "$(INTDIR)\websda.obj"
	-@erase "$(INTDIR)\websda.sbr"
	-@erase "$(INTDIR)\websuemf.obj"
	-@erase "$(INTDIR)\websuemf.sbr"
	-@erase "$(INTDIR)\XMLProperties.obj"
	-@erase "$(INTDIR)\XMLProperties.sbr"
	-@erase "$(OUTDIR)\webs.bsc"
	-@erase "$(OUTDIR)\webs.exe"
	-@erase "$(OUTDIR)\webs.ilk"
	-@erase "$(OUTDIR)\webs.map"
	-@erase "$(OUTDIR)\webs.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /Zp4 /MLd /W3 /Gm /Zi /Od /I "../include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "WIN" /D "WEBS" /D "UEMF" /D "DIGEST_ACCESS_SUPPORT" /D "USER_MANAGEMENT_SUPPORT" /D "ASSERT" /D "DEV" /FR"$(INTDIR)\\" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/o"$(OUTDIR)\webs.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\asp.sbr" \
	"$(INTDIR)\balloc.sbr" \
	"$(INTDIR)\base64.sbr" \
	"$(INTDIR)\cgi.sbr" \
	"$(INTDIR)\datetime.sbr" \
	"$(INTDIR)\default.sbr" \
	"$(INTDIR)\ejlex.sbr" \
	"$(INTDIR)\ejparse.sbr" \
	"$(INTDIR)\emfdb.sbr" \
	"$(INTDIR)\fingerprint.sbr" \
	"$(INTDIR)\form.sbr" \
	"$(INTDIR)\h.sbr" \
	"$(INTDIR)\handler.sbr" \
	"$(INTDIR)\main.sbr" \
	"$(INTDIR)\md5c.sbr" \
	"$(INTDIR)\mime.sbr" \
	"$(INTDIR)\misc.sbr" \
	"$(INTDIR)\page.sbr" \
	"$(INTDIR)\ringq.sbr" \
	"$(INTDIR)\rom.sbr" \
	"$(INTDIR)\security.sbr" \
	"$(INTDIR)\setclockmode.sbr" \
	"$(INTDIR)\setproxy.sbr" \
	"$(INTDIR)\setWebSer.sbr" \
	"$(INTDIR)\sock.sbr" \
	"$(INTDIR)\sockGen.sbr" \
	"$(INTDIR)\sym.sbr" \
	"$(INTDIR)\uemf.sbr" \
	"$(INTDIR)\um.sbr" \
	"$(INTDIR)\umui.sbr" \
	"$(INTDIR)\url.sbr" \
	"$(INTDIR)\value.sbr" \
	"$(INTDIR)\webrom.sbr" \
	"$(INTDIR)\webs.sbr" \
	"$(INTDIR)\websda.sbr" \
	"$(INTDIR)\websuemf.sbr" \
	"$(INTDIR)\XMLProperties.sbr"

"$(OUTDIR)\webs.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=wsock32.lib kernel32.lib user32.lib advapi32.lib libs\libexpat.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\webs.pdb" /map:"$(INTDIR)\webs.map" /debug /machine:I386 /out:"$(OUTDIR)\webs.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\asp.obj" \
	"$(INTDIR)\balloc.obj" \
	"$(INTDIR)\base64.obj" \
	"$(INTDIR)\cgi.obj" \
	"$(INTDIR)\datetime.obj" \
	"$(INTDIR)\default.obj" \
	"$(INTDIR)\ejlex.obj" \
	"$(INTDIR)\ejparse.obj" \
	"$(INTDIR)\emfdb.obj" \
	"$(INTDIR)\fingerprint.obj" \
	"$(INTDIR)\form.obj" \
	"$(INTDIR)\h.obj" \
	"$(INTDIR)\handler.obj" \
	"$(INTDIR)\main.obj" \
	"$(INTDIR)\md5c.obj" \
	"$(INTDIR)\mime.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\page.obj" \
	"$(INTDIR)\ringq.obj" \
	"$(INTDIR)\rom.obj" \
	"$(INTDIR)\security.obj" \
	"$(INTDIR)\setclockmode.obj" \
	"$(INTDIR)\setproxy.obj" \
	"$(INTDIR)\setWebSer.obj" \
	"$(INTDIR)\sock.obj" \
	"$(INTDIR)\sockGen.obj" \
	"$(INTDIR)\sym.obj" \
	"$(INTDIR)\uemf.obj" \
	"$(INTDIR)\um.obj" \
	"$(INTDIR)\umui.obj" \
	"$(INTDIR)\url.obj" \
	"$(INTDIR)\value.obj" \
	"$(INTDIR)\webrom.obj" \
	"$(INTDIR)\webs.obj" \
	"$(INTDIR)\websda.obj" \
	"$(INTDIR)\websuemf.obj" \
	"$(INTDIR)\XMLProperties.obj"

"$(OUTDIR)\webs.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"

OUTDIR=.\DebugSSL
INTDIR=.\DebugSSL
# Begin Custom Macros
OutDir=.\DebugSSL
# End Custom Macros

ALL : "$(OUTDIR)\webs.exe"


CLEAN :
	-@erase "$(INTDIR)\asp.obj"
	-@erase "$(INTDIR)\balloc.obj"
	-@erase "$(INTDIR)\base64.obj"
	-@erase "$(INTDIR)\cgi.obj"
	-@erase "$(INTDIR)\datetime.obj"
	-@erase "$(INTDIR)\default.obj"
	-@erase "$(INTDIR)\ejlex.obj"
	-@erase "$(INTDIR)\ejparse.obj"
	-@erase "$(INTDIR)\emfdb.obj"
	-@erase "$(INTDIR)\fingerprint.obj"
	-@erase "$(INTDIR)\form.obj"
	-@erase "$(INTDIR)\h.obj"
	-@erase "$(INTDIR)\handler.obj"
	-@erase "$(INTDIR)\main.obj"
	-@erase "$(INTDIR)\md5c.obj"
	-@erase "$(INTDIR)\mime.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\page.obj"
	-@erase "$(INTDIR)\ringq.obj"
	-@erase "$(INTDIR)\rom.obj"
	-@erase "$(INTDIR)\security.obj"
	-@erase "$(INTDIR)\setclockmode.obj"
	-@erase "$(INTDIR)\setproxy.obj"
	-@erase "$(INTDIR)\setWebSer.obj"
	-@erase "$(INTDIR)\sock.obj"
	-@erase "$(INTDIR)\sockGen.obj"
	-@erase "$(INTDIR)\sym.obj"
	-@erase "$(INTDIR)\uemf.obj"
	-@erase "$(INTDIR)\um.obj"
	-@erase "$(INTDIR)\umui.obj"
	-@erase "$(INTDIR)\url.obj"
	-@erase "$(INTDIR)\value.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\vc60.pdb"
	-@erase "$(INTDIR)\webrom.obj"
	-@erase "$(INTDIR)\webs.obj"
	-@erase "$(INTDIR)\websda.obj"
	-@erase "$(INTDIR)\websSSL.obj"
	-@erase "$(INTDIR)\websuemf.obj"
	-@erase "$(INTDIR)\XMLProperties.obj"
	-@erase "$(OUTDIR)\webs.exe"
	-@erase "$(OUTDIR)\webs.ilk"
	-@erase "$(OUTDIR)\webs.map"
	-@erase "$(OUTDIR)\webs.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /Zp4 /MLd /W3 /Gm /Zi /Od /I "." /I "../include" /D "OPENSSL" /D "WEBS_SSL_SUPPORT" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "WIN" /D "WEBS" /D "UEMF" /D "DIGEST_ACCESS_SUPPORT" /D "USER_MANAGEMENT_SUPPORT" /D "ASSERT" /D "DEV" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/o"$(OUTDIR)\webs.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=wsock32.lib kernel32.lib user32.lib advapi32.lib libeay32.lib ssleay32.lib /nologo /subsystem:windows /incremental:yes /pdb:"$(OUTDIR)\webs.pdb" /map:"$(INTDIR)\webs.map" /debug /machine:I386 /out:"$(OUTDIR)\webs.exe" /pdbtype:sept /libpath:"openssl\deb" 
LINK32_OBJS= \
	"$(INTDIR)\asp.obj" \
	"$(INTDIR)\balloc.obj" \
	"$(INTDIR)\base64.obj" \
	"$(INTDIR)\cgi.obj" \
	"$(INTDIR)\datetime.obj" \
	"$(INTDIR)\default.obj" \
	"$(INTDIR)\ejlex.obj" \
	"$(INTDIR)\ejparse.obj" \
	"$(INTDIR)\emfdb.obj" \
	"$(INTDIR)\fingerprint.obj" \
	"$(INTDIR)\form.obj" \
	"$(INTDIR)\h.obj" \
	"$(INTDIR)\handler.obj" \
	"$(INTDIR)\main.obj" \
	"$(INTDIR)\md5c.obj" \
	"$(INTDIR)\mime.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\page.obj" \
	"$(INTDIR)\ringq.obj" \
	"$(INTDIR)\rom.obj" \
	"$(INTDIR)\security.obj" \
	"$(INTDIR)\setclockmode.obj" \
	"$(INTDIR)\setproxy.obj" \
	"$(INTDIR)\setWebSer.obj" \
	"$(INTDIR)\sock.obj" \
	"$(INTDIR)\sockGen.obj" \
	"$(INTDIR)\sym.obj" \
	"$(INTDIR)\uemf.obj" \
	"$(INTDIR)\um.obj" \
	"$(INTDIR)\umui.obj" \
	"$(INTDIR)\url.obj" \
	"$(INTDIR)\value.obj" \
	"$(INTDIR)\webrom.obj" \
	"$(INTDIR)\webs.obj" \
	"$(INTDIR)\websda.obj" \
	"$(INTDIR)\websSSL.obj" \
	"$(INTDIR)\websuemf.obj" \
	"$(INTDIR)\XMLProperties.obj"

"$(OUTDIR)\webs.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"

OUTDIR=.\ReleaseSSL
INTDIR=.\ReleaseSSL
# Begin Custom Macros
OutDir=.\ReleaseSSL
# End Custom Macros

ALL : "$(OUTDIR)\webs.exe"


CLEAN :
	-@erase "$(INTDIR)\asp.obj"
	-@erase "$(INTDIR)\balloc.obj"
	-@erase "$(INTDIR)\base64.obj"
	-@erase "$(INTDIR)\cgi.obj"
	-@erase "$(INTDIR)\datetime.obj"
	-@erase "$(INTDIR)\default.obj"
	-@erase "$(INTDIR)\ejlex.obj"
	-@erase "$(INTDIR)\ejparse.obj"
	-@erase "$(INTDIR)\emfdb.obj"
	-@erase "$(INTDIR)\fingerprint.obj"
	-@erase "$(INTDIR)\form.obj"
	-@erase "$(INTDIR)\h.obj"
	-@erase "$(INTDIR)\handler.obj"
	-@erase "$(INTDIR)\main.obj"
	-@erase "$(INTDIR)\md5c.obj"
	-@erase "$(INTDIR)\mime.obj"
	-@erase "$(INTDIR)\misc.obj"
	-@erase "$(INTDIR)\page.obj"
	-@erase "$(INTDIR)\ringq.obj"
	-@erase "$(INTDIR)\rom.obj"
	-@erase "$(INTDIR)\security.obj"
	-@erase "$(INTDIR)\setclockmode.obj"
	-@erase "$(INTDIR)\setproxy.obj"
	-@erase "$(INTDIR)\setWebSer.obj"
	-@erase "$(INTDIR)\sock.obj"
	-@erase "$(INTDIR)\sockGen.obj"
	-@erase "$(INTDIR)\sym.obj"
	-@erase "$(INTDIR)\uemf.obj"
	-@erase "$(INTDIR)\um.obj"
	-@erase "$(INTDIR)\umui.obj"
	-@erase "$(INTDIR)\url.obj"
	-@erase "$(INTDIR)\value.obj"
	-@erase "$(INTDIR)\vc60.idb"
	-@erase "$(INTDIR)\webrom.obj"
	-@erase "$(INTDIR)\webs.obj"
	-@erase "$(INTDIR)\websda.obj"
	-@erase "$(INTDIR)\websSSL.obj"
	-@erase "$(INTDIR)\websuemf.obj"
	-@erase "$(INTDIR)\XMLProperties.obj"
	-@erase "$(OUTDIR)\webs.exe"
	-@erase "$(OUTDIR)\webs.map"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /Zp1 /ML /W3 /I "." /I "../include" /D "OPENSSL" /D "WEBS_SSL_SUPPORT" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "WEBS" /D "UEMF" /D "WIN" /D "DIGEST_ACCESS_SUPPORT" /D "USER_MANAGEMENT_SUPPORT" /D "DEV" /Fp"$(INTDIR)\webs.pch" /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/o"$(OUTDIR)\webs.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=wsock32.lib kernel32.lib user32.lib advapi32.lib gdi32.lib libeay32.lib ssleay32.lib /nologo /subsystem:windows /incremental:no /pdb:"$(OUTDIR)\webs.pdb" /map:"$(INTDIR)\webs.map" /machine:I386 /out:"$(OUTDIR)\webs.exe" /libpath:"openssl\rel" 
LINK32_OBJS= \
	"$(INTDIR)\asp.obj" \
	"$(INTDIR)\balloc.obj" \
	"$(INTDIR)\base64.obj" \
	"$(INTDIR)\cgi.obj" \
	"$(INTDIR)\datetime.obj" \
	"$(INTDIR)\default.obj" \
	"$(INTDIR)\ejlex.obj" \
	"$(INTDIR)\ejparse.obj" \
	"$(INTDIR)\emfdb.obj" \
	"$(INTDIR)\fingerprint.obj" \
	"$(INTDIR)\form.obj" \
	"$(INTDIR)\h.obj" \
	"$(INTDIR)\handler.obj" \
	"$(INTDIR)\main.obj" \
	"$(INTDIR)\md5c.obj" \
	"$(INTDIR)\mime.obj" \
	"$(INTDIR)\misc.obj" \
	"$(INTDIR)\page.obj" \
	"$(INTDIR)\ringq.obj" \
	"$(INTDIR)\rom.obj" \
	"$(INTDIR)\security.obj" \
	"$(INTDIR)\setclockmode.obj" \
	"$(INTDIR)\setproxy.obj" \
	"$(INTDIR)\setWebSer.obj" \
	"$(INTDIR)\sock.obj" \
	"$(INTDIR)\sockGen.obj" \
	"$(INTDIR)\sym.obj" \
	"$(INTDIR)\uemf.obj" \
	"$(INTDIR)\um.obj" \
	"$(INTDIR)\umui.obj" \
	"$(INTDIR)\url.obj" \
	"$(INTDIR)\value.obj" \
	"$(INTDIR)\webrom.obj" \
	"$(INTDIR)\webs.obj" \
	"$(INTDIR)\websda.obj" \
	"$(INTDIR)\websSSL.obj" \
	"$(INTDIR)\websuemf.obj" \
	"$(INTDIR)\XMLProperties.obj"

"$(OUTDIR)\webs.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(INTDIR)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

	




!IF "$(NO_EXTERNAL_DEPS)" != "1"
!IF EXISTS("webs.dep")
!INCLUDE "webs.dep"
!ELSE 
!MESSAGE Warning: cannot find "webs.dep"
!ENDIF 
!ENDIF 


!IF "$(CFG)" == "webs - Win32 Release" || "$(CFG)" == "webs - Win32 Debug" || "$(CFG)" == "webs - Win32 Debug SSL" || "$(CFG)" == "webs - Win32 Release SSL"
SOURCE=..\asp.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\asp.obj"	"$(INTDIR)\asp.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\asp.obj"	"$(INTDIR)\asp.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\asp.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\asp.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\balloc.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\balloc.obj"	"$(INTDIR)\balloc.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\balloc.obj"	"$(INTDIR)\balloc.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\balloc.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\balloc.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\base64.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\base64.obj"	"$(INTDIR)\base64.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\base64.obj"	"$(INTDIR)\base64.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\base64.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\base64.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\cgi.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\cgi.obj"	"$(INTDIR)\cgi.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\cgi.obj"	"$(INTDIR)\cgi.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\cgi.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\cgi.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 


SOURCE=..\datetime.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\datetime.obj"	"$(INTDIR)\datetime.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\datetime.obj"	"$(INTDIR)\datetime.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\datetime.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\datetime.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\default.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\default.obj"	"$(INTDIR)\default.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\default.obj"	"$(INTDIR)\default.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\default.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\default.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\ejlex.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\ejlex.obj"	"$(INTDIR)\ejlex.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\ejlex.obj"	"$(INTDIR)\ejlex.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\ejlex.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\ejlex.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\ejparse.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\ejparse.obj"	"$(INTDIR)\ejparse.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\ejparse.obj"	"$(INTDIR)\ejparse.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\ejparse.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\ejparse.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\emfdb.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\emfdb.obj"	"$(INTDIR)\emfdb.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\emfdb.obj"	"$(INTDIR)\emfdb.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\emfdb.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\emfdb.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\fingerprint.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\fingerprint.obj"	"$(INTDIR)\fingerprint.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\fingerprint.obj"	"$(INTDIR)\fingerprint.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\fingerprint.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\fingerprint.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\form.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\form.obj"	"$(INTDIR)\form.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\form.obj"	"$(INTDIR)\form.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\form.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\form.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\h.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\h.obj"	"$(INTDIR)\h.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\h.obj"	"$(INTDIR)\h.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\h.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\h.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\handler.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\handler.obj"	"$(INTDIR)\handler.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\handler.obj"	"$(INTDIR)\handler.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\handler.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\handler.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=.\main.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\main.obj"	"$(INTDIR)\main.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\main.obj"	"$(INTDIR)\main.sbr" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\main.obj" : $(SOURCE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\main.obj" : $(SOURCE) "$(INTDIR)"


!ENDIF 

SOURCE=..\md5c.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\md5c.obj"	"$(INTDIR)\md5c.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\md5c.obj"	"$(INTDIR)\md5c.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\md5c.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\md5c.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\mime.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\mime.obj"	"$(INTDIR)\mime.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\mime.obj"	"$(INTDIR)\mime.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\mime.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\mime.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\misc.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\misc.obj"	"$(INTDIR)\misc.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\misc.obj"	"$(INTDIR)\misc.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\misc.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\misc.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\page.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\page.obj"	"$(INTDIR)\page.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\page.obj"	"$(INTDIR)\page.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\page.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\page.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\ringq.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\ringq.obj"	"$(INTDIR)\ringq.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\ringq.obj"	"$(INTDIR)\ringq.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\ringq.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\ringq.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\rom.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\rom.obj"	"$(INTDIR)\rom.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\rom.obj"	"$(INTDIR)\rom.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\rom.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\rom.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\security.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\security.obj"	"$(INTDIR)\security.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\security.obj"	"$(INTDIR)\security.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\security.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\security.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\setclockmode.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\setclockmode.obj"	"$(INTDIR)\setclockmode.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\setclockmode.obj"	"$(INTDIR)\setclockmode.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\setclockmode.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\setclockmode.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\setproxy.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\setproxy.obj"	"$(INTDIR)\setproxy.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\setproxy.obj"	"$(INTDIR)\setproxy.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\setproxy.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\setproxy.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\setWebSer.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\setWebSer.obj"	"$(INTDIR)\setWebSer.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\setWebSer.obj"	"$(INTDIR)\setWebSer.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\setWebSer.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\setWebSer.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\sock.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\sock.obj"	"$(INTDIR)\sock.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\sock.obj"	"$(INTDIR)\sock.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\sock.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\sock.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\sockGen.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\sockGen.obj"	"$(INTDIR)\sockGen.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\sockGen.obj"	"$(INTDIR)\sockGen.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\sockGen.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\sockGen.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\sym.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\sym.obj"	"$(INTDIR)\sym.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\sym.obj"	"$(INTDIR)\sym.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\sym.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\sym.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\uemf.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\uemf.obj"	"$(INTDIR)\uemf.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\uemf.obj"	"$(INTDIR)\uemf.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\uemf.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\uemf.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\um.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\um.obj"	"$(INTDIR)\um.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\um.obj"	"$(INTDIR)\um.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\um.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\um.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\umui.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\umui.obj"	"$(INTDIR)\umui.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\umui.obj"	"$(INTDIR)\umui.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\umui.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\umui.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 





































SOURCE=..\url.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\url.obj"	"$(INTDIR)\url.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\url.obj"	"$(INTDIR)\url.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\url.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\url.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\value.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\value.obj"	"$(INTDIR)\value.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\value.obj"	"$(INTDIR)\value.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\value.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\value.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\webrom.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\webrom.obj"	"$(INTDIR)\webrom.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\webrom.obj"	"$(INTDIR)\webrom.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\webrom.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\webrom.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\webs.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\webs.obj"	"$(INTDIR)\webs.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\webs.obj"	"$(INTDIR)\webs.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\webs.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\webs.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\websda.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\websda.obj"	"$(INTDIR)\websda.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\websda.obj"	"$(INTDIR)\websda.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\websda.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\websda.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\websSSL.c

!IF  "$(CFG)" == "webs - Win32 Release"

!ELSEIF  "$(CFG)" == "webs - Win32 Debug"

!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\websSSL.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\websSSL.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\websuemf.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\websuemf.obj"	"$(INTDIR)\websuemf.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\websuemf.obj"	"$(INTDIR)\websuemf.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\websuemf.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\websuemf.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 

SOURCE=..\XMLProperties.c

!IF  "$(CFG)" == "webs - Win32 Release"


"$(INTDIR)\XMLProperties.obj"	"$(INTDIR)\XMLProperties.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug"


"$(INTDIR)\XMLProperties.obj"	"$(INTDIR)\XMLProperties.sbr" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Debug SSL"


"$(INTDIR)\XMLProperties.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "webs - Win32 Release SSL"


"$(INTDIR)\XMLProperties.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 


!ENDIF 

