#include	"wsIntrn.h"
#include    "XMLProperties.h"

#define		NONE_OPTION		T("<NONE>")
#define		MSG_START		T("<body><h2>")
#define		MSG_END			T("</h2></body>")
#define     XML_FILE        "/home/terminal/bin/properties.xml"
static void		formSetProxy(webs_t wp, char_t *path, char_t *query);
static void		websMsgStart(webs_t wp);
static void		websMsgEnd(webs_t wp);

void formDefineSetProxy(void)
{

    websFormDefine(T("SetProxy"), formSetProxy);
}


static void formSetProxy(webs_t wp, char_t *path, char_t *query)
{
    char_t	*enableProxy, *proxy, *port , *enableAuth, *userName, *password;
    int res1,res2,res3,res4,res5,res6;
	a_assert(wp);
    //printf("we are here\n");
	enableProxy = websGetVar(wp, T("enableProxy"), T("")); 
	proxy = websGetVar(wp, T("proxy"), T("")); 
	port = websGetVar(wp, T("port"), T("")); 
	enableAuth = websGetVar(wp, T("enableAuth"), T(""));
	userName = websGetVar(wp, T("userName"), T(""));
	password = websGetVar(wp, T("password"), T(""));
	websHeader(wp);
	websMsgStart(wp);
    
     //printf("enableProxy=%s proxy=%s port=%s\n",enableProxy,proxy,port);
     //websWrite(wp, T("enableProxy=%s proxy=%s port=%s enableAuth=%s userName=%s password=%s"),enableProxy,proxy,port,enableAuth,userName,password);
     XMLPropertiesInit(XML_FILE);
	 if(strcmp(enableProxy,"true")==0)
	 {
	  res1 = SetProperty("UseProxy",enableProxy);
	  if(strcmp(proxy,""))res2 = SetProperty("Proxy",proxy);
	  else res2 = SetProperty("Proxy","NULL");
	  if(strcmp(port,""))res3 = SetProperty("ProxyPort",port);
	  else res3 = SetProperty("ProxyPort","NULL");
	 }
	 else
	 {
	  res1 = SetProperty("UseProxy","false");
	  res2 = SetProperty("Proxy","NULL");
	  res3 = SetProperty("ProxyPort","NULL");
	 }
	 if(strcmp(enableAuth,"true")==0)
	 { 	 
	  res4 = SetProperty("UseProxyAuthentication",enableAuth);
	  if(strcmp(userName,""))res5 = SetProperty("ProxyUserName",userName);
	  else res5 = SetProperty("ProxyUserName","NULL");
      if(strcmp(password,""))res6 = SetProperty("ProxyPassword",password);
	  else res6 = SetProperty("ProxyPassword","NULL");
	 }
	 else 
	 {
	  res4 = SetProperty("UseProxyAuthentication","false");
	  res5 = SetProperty("ProxyUserName","NULL");
      res6 = SetProperty("ProxyPassword","NULL");
	 }
	 XMLPropertiesFree();
    if(res1&&res2&&res3&res4&res5&res6)
       websWrite(wp, T("Proxy Configuration Succeed Please Reset the Clock"));
	else
	{   
		websWrite(wp, T("Proxy Configuration Failed"));
        printf("1 %d 2 %d 3 %d 4 %d 5 %d 6 %d\n",res1,res2,res3,res4,res5,res6);
	}
    websMsgEnd(wp);
	websFooter(wp);
	websDone(wp, 200);
}

static void	websMsgStart(webs_t wp)
{
	websWrite(wp, MSG_START);
}

static void	websMsgEnd(webs_t wp)
{
	websWrite(wp, MSG_END);
}