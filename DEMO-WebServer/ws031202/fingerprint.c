#include	"wsIntrn.h"

#include "uf_api.h"
#include "uf_serial.h"
#include "uf_module_log.h"
#include "uf_log.h"
#include "uf_packet.h"
#include "uf_command.h"


#define		NONE_OPTION		T("<NONE>")
#define		MSG_START		T("<body><h2>")
#define		MSG_END			T("</h2></body>")

static void		formEnrollTemplate(webs_t wp, char_t *path, char_t *query);
static void		websMsgStart(webs_t wp);
static void		websMsgEnd(webs_t wp);
void initFPU();
static int enableFPU = 0 ;

void formDefineFingerPrint(void)
{

    websFormDefine(T("EnrollTemplate"), formEnrollTemplate);
}

void initFPU(webs_t wp)
{
  if(!enableFPU)
  {
	char logstr[50];
	int mBaudRate;
	char mDeviceName[256];
	int mAsciiPacket =0;
	int mNetworkMode =0;
	int mModuleID = 1;
	int mTimeout =5000;   //timeout = 5000 ms
	UF_RET_CODE result;
	mBaudRate = UF_BAUD115200;
#ifdef WIN32
#else
	uf_finalize();
#endif
	strcpy(mDeviceName, "/dev/tts/3");
#ifdef WIN32
	result = 0;
#else
	result = uf_initialize( mDeviceName, (UF_BAUDRATE)mBaudRate, (UF_BOOL)mAsciiPacket, (UF_BOOL)mNetworkMode, mModuleID, mTimeout );
#endif
	if( result == UF_RET_SUCCESS || result == 0)
	{
		websWrite(wp,T("uf_initialize success - making fpUnitEnabled true<br>"));
        enableFPU = 1;
	}
	else
	{
      websWrite(wp,T("uf_initialize failed - making fpUnitEnabled false<br>"));
	}

  }
}
static void		formEnrollTemplate(webs_t wp, char_t *path, char_t *query)
{
  char_t	*templ = NULL;
  char_t    *fpUserId;
  unsigned long int UserId = 0;
  unsigned long int user_id;
  UF_RET_CODE resultEn;
  a_assert(wp);
  templ = websGetVar(wp, T("templ"), T(""));
  fpUserId = websGetVar(wp, T("fpUserId"), T(""));
  UserId =strtoul(fpUserId,NULL,0);
  wp->postData[wp->lenPostData] = '\0';
  
  
  if(!enableFPU)
   initFPU(wp);

#ifdef WIN32
#else
  system("killall SY7000");
#endif
#ifdef WIN32
  resultEn = 0;
#else
  resultEn =  uf_enroll_by_template( (UF_UINT32)UserId,wp->postData,wp->lenPostData,UF_ENROLL_ADD_NEW,&user_id);
#endif

 if( resultEn == UF_RET_SUCCESS || resultEn == 0)
  {
#ifdef WIN32
#else
      uf_cancel();
#endif
	  websWrite(wp,T("uf_enroll_by_scan success<br> "));
  }
  else
  {
#ifdef WIN32
#else
      uf_cancel();
#endif
	  websWrite(wp,T("uf_enroll_by_scan failed<br> "));
  }
//  websWrite(wp,T("UserId = %d size = %d result = %d initFPU = %d <br> data = %s"),UserId,wp->lenPostData,resultEn,result,wp->postData);
  
  printf("templ = %s\n",templ);
  websMsgEnd(wp);
  websFooter(wp);
  websDone(wp, 200);

}

static void	websMsgStart(webs_t wp)
{
	websWrite(wp, MSG_START);
}

static void	websMsgEnd(webs_t wp)
{
	websWrite(wp, MSG_END);
}