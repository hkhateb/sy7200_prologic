#include	"wsIntrn.h"
#include    "XMLProperties.h"

#define		NONE_OPTION		T("<NONE>")
#define		MSG_START		T("<body><h2>")
#define		MSG_END			T("</h2></body>")
//#ifdef WIN32
//#define     XML_FILE        "properties.xml"
//#else
//#define     XML_FILE        "/home/terminal/bin/properties.xml"
//#endif
#define     XML_FILE        "/home/terminal/bin/properties.xml"

static void		formWebSer(webs_t wp, char_t *path, char_t *query);
static int		aspGetCurrentLink(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetCurrentAccount(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetCurrentClockId(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetCurrentTimeOut(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetCurrentHeartBeatTimer(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetCurrentProjectTasksTimer(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetCurrentAddSoapHeader(int eid, webs_t wp, int argc, char_t **argv);
static int      aspGetCurrentUseDataBaseServer(int eid, webs_t wp, int argc, char_t **argv);
static void		websMsgStart(webs_t wp);
static void		websMsgEnd(webs_t wp);

static int      aspGetVerifySSL(int eid, webs_t wp, int argc, char_t **argv);
static int      aspGetSSLCertFile(int eid, webs_t wp, int argc, char_t **argv);


void formDefineSetWebSer(void)
{


    websFormDefine(T("SetWebSer"), formWebSer);
	websAspDefine(T("GetCurrentLink"), aspGetCurrentLink);
	websAspDefine(T("GetCurrentAccount"), aspGetCurrentAccount);
	websAspDefine(T("GetCurrentClockId"), aspGetCurrentClockId);
	websAspDefine(T("GetCurrentTimeOut"), aspGetCurrentTimeOut);
	websAspDefine(T("GetCurrentHeartBeatTimer"), aspGetCurrentHeartBeatTimer);
	websAspDefine(T("GetCurrentProjectTasksTimer"), aspGetCurrentProjectTasksTimer);
	websAspDefine(T("GetCurrentAddSoapHeader"), aspGetCurrentAddSoapHeader);
	websAspDefine(T("GetCurrentUseDataBaseServer"),aspGetCurrentUseDataBaseServer);

	websAspDefine(T("GetVerifySSL"),aspGetVerifySSL);
	websAspDefine(T("GetSSLCertFile"),aspGetSSLCertFile);
}


static void formWebSer(webs_t wp, char_t *path, char_t *query)
{
    char_t	*link, *account, *clockID , *timeOut, *heartBeat, *projectTasks , *useSoapActionHeader , *soapActionHeader,
		     *useSqlServer;

	char_t *verifyssl;
	char_t *sslcertfile;

    int res1,res2,res3,res4,res5,res6,res7,res8,res9, res10;
	a_assert(wp);
    //printf("we are here\n");
	link = websGetVar(wp, T("link"), T("")); 
	account = websGetVar(wp, T("account"), T("")); 
	clockID = websGetVar(wp, T("ClockId"), T("")); 
	timeOut = websGetVar(wp, T("TimeOut"), T(""));
	heartBeat = websGetVar(wp, T("HeartBeatTimer"), T(""));
	projectTasks = websGetVar(wp, T("GetProjectTasks"), T(""));
	useSoapActionHeader = websGetVar(wp, T("UseSopaActionHeader"), T(""));
	soapActionHeader = websGetVar(wp, T("SoapActionHeader"), T(""));
    useSqlServer = websGetVar(wp, T("UseSqlServer"), T(""));

	verifyssl   = websGetVar(wp, T("VerifySSL"), T(""));
    sslcertfile = websGetVar(wp, T("SSLCertFile"), T(""));

	websHeader(wp);
	websMsgStart(wp);
    
    //printf("link=%s account=%s clockID=%s\n",link,account,clockID);
    //websWrite(wp, T("link=%s account=%s clockID=%s"),link,account,clockID);
     XMLPropertiesInit(XML_FILE);
	 res1 = SetProperty("WebService",link);
     res2 = SetProperty("Account",account);
	 res3 = SetProperty("ClockId",clockID);
	 res4 = SetProperty("TimeOut",timeOut);
	 res5 = SetProperty("HeartBeatTimer",heartBeat);
     res6 = SetProperty("GetProjectTasksTimer",projectTasks);
	 if(strcmp(useSoapActionHeader,"true")==0)
	 {
	  res7 = SetProperty("UseSoapActionHeader",useSoapActionHeader);
	  if(strcmp(soapActionHeader,""))res8 = SetProperty("SoapActionHeader",soapActionHeader);
	  else res8 = SetProperty("SoapActionHeader","NULL");
	 }
	 else
	 {
	  res7 = SetProperty("UseSoapActionHeader","false");
	  res8 = SetProperty("SoapActionHeader","NULL");
	 }

     if(strcmp(verifyssl,"true")==0)
	 {
	  res9 = SetProperty("VerifySSL",verifyssl);
	  if(strcmp(sslcertfile,"")) res10 = SetProperty("SSLCertFile", sslcertfile);
	  else res10 = SetProperty("SSLCertFile","NULL");
	 }
	 else
	 {
	  res9 = SetProperty("VerifySSL","false");
	  res10= SetProperty("SSLCertFile", "NULL");
	 }

	 res10 = SetProperty("SSLCertFile",sslcertfile);

	 XMLPropertiesFree();
    if( res1 & res2 & res3 & res4 & res5 & res6 & res7 & res8 & res9 & res10)
       websWrite(wp, T("Web Service Configuration Succeed Please Reset the Clock"));
	else
	{   
		websWrite(wp, T("Web Service Configuration Failed"));
        printf("1 %d 2 %d 3 %d 4 %d 5 %d 6 %d 7 %d 8 %d 9 %d 10 %d\n",res1,res2,res3,res4,res5,res6,res7,res8,res9,res10);
	}
    websMsgEnd(wp);
	websFooter(wp);
	websDone(wp, 200);
}

static int aspGetCurrentLink(int eid, webs_t wp, int argc, char_t **argv)
{
    char* link = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    link = GetProperty("WebService");
	if(!link || strcmp(link,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	if(link) nBytes = websWrite(wp,T(link));
	XMLPropertiesFree();
	return nBytes;
}

static int aspGetCurrentAccount(int eid, webs_t wp, int argc, char_t **argv)
{
    char* account = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    account = GetProperty("Account");
	if(!account || strcmp(account,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	if(account) nBytes = websWrite(wp,T(account));
	XMLPropertiesFree();
	return nBytes;
}

static int aspGetCurrentClockId(int eid, webs_t wp, int argc, char_t **argv)
{
    char* ClockId = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    ClockId = GetProperty("ClockId");
	if(!ClockId || strcmp(ClockId,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	if(ClockId) nBytes = websWrite(wp,T(ClockId));
	XMLPropertiesFree();
	return nBytes;
}

static int aspGetCurrentTimeOut(int eid, webs_t wp, int argc, char_t **argv)
{
    char* TimeOut = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    TimeOut = GetProperty("TimeOut");
	if(!TimeOut || strcmp(TimeOut,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	if(TimeOut) nBytes = websWrite(wp,T(TimeOut));
	XMLPropertiesFree();
	return nBytes;
}

static int aspGetCurrentHeartBeatTimer(int eid, webs_t wp, int argc, char_t **argv)
{
    char* HeartBeatTimer = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    HeartBeatTimer = GetProperty("HeartBeatTimer");
	if(!HeartBeatTimer || strcmp(HeartBeatTimer,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	if(HeartBeatTimer) nBytes = websWrite(wp,T(HeartBeatTimer));
	XMLPropertiesFree();
	return nBytes;
}

static int aspGetCurrentProjectTasksTimer(int eid, webs_t wp, int argc, char_t **argv)
{
    char* ProjectTasksTimer = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    ProjectTasksTimer = GetProperty("GetProjectTasksTimer");
	if(!ProjectTasksTimer || strcmp(ProjectTasksTimer,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	if(ProjectTasksTimer) nBytes = websWrite(wp,T(ProjectTasksTimer));
	XMLPropertiesFree();
	return nBytes;
}

static int aspGetCurrentAddSoapHeader(int eid, webs_t wp, int argc, char_t **argv)
{
    char UseHeader[10];
	char* temp;
    char* Header = NULL;
	int	  nBytes1,nBytes2,nBytes3;
    nBytes1 = 0;nBytes2 = 0;nBytes3 = 0;
    UseHeader[0] = '\0';
	a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    temp = NULL;
	temp = GetProperty("UseSoapActionHeader");
	if(temp){strcpy(UseHeader,temp);}
	Header = GetProperty("SoapActionHeader");

    if(!Header || strcmp(Header,"NULL") == 0)
	    Header = NULL;
	
	if(strlen(UseHeader) > 0 && strcmp(UseHeader,"NULL"))
	{
		
		if(strcmp(UseHeader,"true") == 0)
		{
			
			nBytes1 = websWrite(wp,T("<input id=\"CHECKBOX1\" type=\"CHECKBOX\" name=\"UseSopaActionHeader\" checked onclick=\"EnableSoapActionHeader()\" value=\"true\">Add Soap Action Header In Request<br>"));
			if(Header)
				nBytes3 = websWrite(wp,T("Soap Action Header:<input id=\"sah\" type=\"text\" name=\"SoapActionHeader\" size=40 value= %s>"),Header);
			else
				nBytes3 = websWrite(wp,T("Soap Action Header:<input id=\"sah\" type=\"text\" name=\"SoapActionHeader\" size=40>"));
          XMLPropertiesFree();
          return  nBytes1+nBytes3;
		}
	}
	nBytes1 = websWrite(wp,T("<input id=\"CHECKBOX1\" type=\"CHECKBOX\" name=\"UseSopaActionHeader\" onclick=\"EnableSoapActionHeader()\" value=\"true\">Add Soap Action Header In Request<br>"));
	if(Header)
		nBytes3 = websWrite(wp,T("Soap Action Header:<input id=\"sah\" type=\"text\" name=\"SoapActionHeader\" disabled size=40 value= %s>"),
Header);
	else
		nBytes3 = websWrite(wp,T("Soap Action Header:<input id=\"sah\" type=\"text\" name=\"SoapActionHeader\" disabled size=40>"));
	XMLPropertiesFree();
	return  nBytes1+nBytes3;
		
}

static int      aspGetCurrentUseDataBaseServer(int eid, webs_t wp, int argc, char_t **argv)
{
	char UseSqlServer[10];
	char* temp;
	int	  nBytes1;
    nBytes1 = 0;
    UseSqlServer[0] = '\0';
	a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    temp = NULL;
	temp = GetProperty("UseSqlServer");
	if(temp){strcpy(UseSqlServer,temp);}
	
	if(strlen(UseSqlServer) > 0)
	{
		
		if(strcmp(UseSqlServer,"true") == 0)
		{
			
			nBytes1 = websWrite(wp,T("<input id=\"CHECKBOX2\" type=\"CHECKBOX\" name=\"UseSqlServer\" checked value=\"true\">Connect To Data Base Server<br>"));
          XMLPropertiesFree();
          return  nBytes1;
		}
	}
	nBytes1 = websWrite(wp,T("<input id=\"CHECKBOX2\" type=\"CHECKBOX\" name=\"UseSqlServer\" value=\"true\">Connect To Data Base Server<br>"));

	XMLPropertiesFree();
	return  nBytes1;
  
}

static void	websMsgStart(webs_t wp)
{
	websWrite(wp, MSG_START);
}

static void	websMsgEnd(webs_t wp)
{
	websWrite(wp, MSG_END);
}


static int      aspGetVerifySSL(int eid, webs_t wp, int argc, char_t **argv)
{
	char VerifySSL[10];
	char* temp;
	int	  nBytes1;
    nBytes1 = 0;
    VerifySSL[0] = '\0';
	a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    temp = NULL;
	temp = GetProperty("VerifySSL");
	if(temp){strcpy(VerifySSL,temp);}

	if(strlen(VerifySSL) > 0)
	{

		if(strcmp(VerifySSL,"true") == 0)
		{

			nBytes1 = websWrite(wp,T("<input id=\"CHECKBOX2\" type=\"CHECKBOX\" name=\"VerifySSL\" checked value=\"true\">Enable SSL<br>"));
          XMLPropertiesFree();
          return  nBytes1;
		}
	}
	nBytes1 = websWrite(wp,T("<input id=\"CHECKBOX2\" type=\"CHECKBOX\" name=\"VerifySSL\" value=\"true\">Enable SSL<br>"));

	XMLPropertiesFree();
	return  nBytes1;

}

static int      aspGetSSLCertFile(int eid, webs_t wp, int argc, char_t **argv)
{
    char* SSLCertFile = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    SSLCertFile = GetProperty("SSLCertFile");
	if(!SSLCertFile || strcmp(SSLCertFile,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	if(SSLCertFile) nBytes = websWrite(wp,T(SSLCertFile));
	XMLPropertiesFree();
	return nBytes;

}

//eof
