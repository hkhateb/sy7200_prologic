
#ifdef WIN32
#include <windows.h>
#endif

#include	"wsIntrn.h"
#include    "XMLProperties.h"
#include "Gen_Def.h"
#include "User.h"


#define		NONE_OPTION		T("<NONE>")
#define		MSG_START		T("<body><h2>")
#define		MSG_END			T("</h2></body>")
//#ifdef WIN32
//#define     XML_FILE        "properties.xml"
//#else
//#define     XML_FILE        "/home/terminal/bin/properties.xml"
//#endif
#define     XML_FILE        "/home/terminal/bin/properties.xml"
static void		formClockMode(webs_t wp, char_t *path, char_t *query);
static void		formApplicationReset(webs_t wp, char_t *path, char_t *query);
static void		formFactoryReset(webs_t wp, char_t *path, char_t *query);
static void		formTerminalReboot(webs_t wp, char_t *path, char_t *query);
static void		formPlayBuzzer(webs_t wp, char_t *path, char_t *query);
static void		formStopBuzzer(webs_t wp, char_t *path, char_t *query);
static void		formSetBuzzerVolume(webs_t wp, char_t *path, char_t *query);
static void		formSetContrastLevel(webs_t wp, char_t *path, char_t *query);
static int		aspGetSwipeAndGoStatus(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetFpVerificationStatus(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetFpUnitStatus(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetBadgeLength(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetReadersStatus(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetClockUpdateTime(int eid, webs_t wp, int argc, char_t **argv);
static int		aspIsSelectedFunctionKey(int eid, webs_t wp, int argc, char_t **argv);
static int		aspIsSelectedClockInLevels(int eid, webs_t wp, int argc, char_t **argv);
static int		aspSelectedBuzzerVolume(int eid, webs_t wp, int argc, char_t **argv);
static int		aspSelectedContrastLevel(int eid, webs_t wp, int argc, char_t **argv);

static int		aspGetOfflineModeStatus(int eid, webs_t wp, int argc, char_t **argv);
static int 		aspGetCurrentTemplatesAvailableTimer(int eid, webs_t wp, int argc, char_t **argv);
static int      aspGetTitleDisplay(int eid, webs_t wp, int argc, char_t **argv);

static int		aspGetBadgeOffset(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetBadgeValid(int eid, webs_t wp, int argc, char_t **argv);
static int		aspGetKeypadLength(int eid, webs_t wp, int argc, char_t **argv);

static int      aspGetKeypadNeedsFPV(int eid, webs_t wp, int argc, char_t **argv);
static int      aspGetCardNeedsFPV(int eid, webs_t wp, int argc, char_t **argv);

static void		websMsgStart(webs_t wp);
static void		websMsgEnd(webs_t wp);

void formDefineSetClockMode(void)
{
    websFormDefine(T("SetClockMode"), formClockMode);
    websFormDefine(T("ApplicationReset"), formApplicationReset);
	websFormDefine(T("FactoryReset"), formFactoryReset);
	websFormDefine(T("TerminalReboot"), formTerminalReboot);
    websFormDefine(T("PalyBuzzer"), formPlayBuzzer);
	websFormDefine(T("StopBuzzer"), formStopBuzzer);
	websFormDefine(T("SetBuzzerVolume"), formSetBuzzerVolume);
    websFormDefine(T("SetContrastLevel"), formSetContrastLevel);
	websAspDefine(T("GetSwipeAndGoStatus"), aspGetSwipeAndGoStatus);
    websAspDefine(T("GetFpVerificationStatus"), aspGetFpVerificationStatus);
	websAspDefine(T("GetFpUnitStatus"), aspGetFpUnitStatus);
    websAspDefine(T("GetReadersStatus"), aspGetReadersStatus);
    websAspDefine(T("GetBadgeLength"), aspGetBadgeLength);
    websAspDefine(T("GetClockUpdateTime"), aspGetClockUpdateTime);
    websAspDefine(T("IsSelectedFK"), aspIsSelectedFunctionKey);
	websAspDefine(T("IsSelectedClockInLevels"), aspIsSelectedClockInLevels);
	websAspDefine(T("SelectedBuzzerVolume"), aspSelectedBuzzerVolume);
	websAspDefine(T("SelectedContrastLevel"), aspSelectedContrastLevel);

	websAspDefine(T("GetOfflineModeStatus"), aspGetOfflineModeStatus);
	websAspDefine(T("GetCurrentTemplatesAvailableTimer"), aspGetCurrentTemplatesAvailableTimer);
	websAspDefine(T("GetTitleDisplay"), aspGetTitleDisplay);

	websAspDefine(T("GetKeypadNeedsFPV"), aspGetKeypadNeedsFPV);
	websAspDefine(T("GetCardNeedsFPV"),	aspGetCardNeedsFPV);
	websAspDefine(T("GetBadgeOffset"),  aspGetBadgeOffset);
	websAspDefine(T("GetBadgeValid"),   aspGetBadgeValid);
	websAspDefine(T("GetKeypadLength"), aspGetKeypadLength);
}

static void formClockMode(webs_t wp, char_t *path, char_t *query)
{
    char_t	 *swipeAndGo,*houres,*minutes,*seconds;
    char_t   *badgeLen ,*mag,*keypad,*barcode,*prox,*fprint,*code1,*code2,*clockIn,*inPunch,*outPunch,*swipePunch;
	char_t	 *code3, *code4;
	char_t    *ClockInLev1,*ClockInLev2,*ClockInLev3,*ClockInLev4;
	char_t    *FpVer,*FpUnit;
	char_t		*KeypadNeedsFPV, *CardNeedsFPV;
	char_t		*BadgeOffset, *BadgeValid, *KeypadLength;
	char_t		*OfflineMode;
	char_t		*TemplatesAvailableTimer;
	char_t		*TitleDisplay;
	char sources[6];
	int res1,res2,res3,res4,res5,res6,res7,res8,res9,res10,res11,res12,res13,res14,res15,res16,res17,res18,res19;
	int res20; // KeypadNeedsFPV;
	int res21; // CardNeedsFPV
	int res22; // badgeoffset
	int res23; // badgevalid
	int res24; // keypadlength
	char buffer[10];
	sources[0]='N';sources[1]='N';sources[2]='N';sources[3]='N';sources[4]='N';sources[5]='\0';
	a_assert(wp);
	swipeAndGo = websGetVar(wp, T("SwipeAndGo"), T(""));
	houres = websGetVar(wp, T("hour"), T(""));
	minutes = websGetVar(wp, T("min"), T(""));
	seconds = websGetVar(wp, T("sec"), T(""));
	badgeLen = websGetVar(wp, T("badgeLen"), T(""));
	code1 = websGetVar(wp, T("code1"), T(""));
	code2 = websGetVar(wp, T("code2"), T(""));
	code3 = websGetVar(wp, T("code3"), T(""));
	code4 = websGetVar(wp, T("code4"), T(""));
    keypad = websGetVar(wp, T("keypad"), T(""));
	mag = websGetVar(wp, T("magnetic"), T(""));
	barcode = websGetVar(wp, T("barcode"), T(""));
	prox = websGetVar(wp, T("proximity"), T(""));
	fprint = websGetVar(wp, T("fprint"), T(""));
	clockIn = websGetVar(wp, T("ClockIn"), T(""));
	inPunch = websGetVar(wp, T("InPunch"), T(""));
	outPunch = websGetVar(wp, T("OutPunch"), T(""));
	swipePunch = websGetVar(wp, T("SwipePunch"), T(""));
	ClockInLev1 = websGetVar(wp, T("ClockInLev1"), T(""));
	ClockInLev2 = websGetVar(wp, T("ClockInLev2"), T(""));
	ClockInLev3 = websGetVar(wp, T("ClockInLev3"), T(""));
	ClockInLev4 = websGetVar(wp, T("ClockInLev4"), T(""));
    FpVer = websGetVar(wp, T("FpVer"), T(""));
	FpUnit = websGetVar(wp, T("FpUnit"), T(""));
	KeypadNeedsFPV =	websGetVar(wp, T("KeypadNeedsFPV"), T(""));
	CardNeedsFPV	=	websGetVar(wp, T("CardNeedsFPV"), T(""));
	BadgeOffset = websGetVar(wp, T("BadgeOffset"), T(""));
	BadgeValid = websGetVar(wp, T("BadgeValid"), T(""));
	KeypadLength = websGetVar(wp, T("KeypadLength"), T(""));
	OfflineMode = websGetVar(wp, T("OfflineMode"), T(""));
	TemplatesAvailableTimer = websGetVar(wp, T("TemplatesAvailableTimer"), T(""));
	TitleDisplay = websGetVar(wp, T("TitleDisplay"), T(""));
	strcpy(buffer,houres);strcat(buffer,":");
    strcat(buffer,minutes);strcat(buffer,":");
	strcat(buffer,seconds);

	websHeader(wp);
	websMsgStart(wp);
	XMLPropertiesInit(XML_FILE);
	printf("we are here \n");
	printf("code1 = %s code2 = %s \n",code1,code2);
	if(code1 && strlen(code1)>0)
	{
		if((int)strlen(code1) == atoi(badgeLen))
		{
			if(strcmp(code1,code2) == 0)
				res5 = SetProperty("accesscode",code1);
			else
			{
				websWrite(wp, T("Code Do Not Match Please Try Again"));
				websMsgEnd(wp);
				websFooter(wp);
				websDone(wp, 200);
				return ;
			}
		}
		else
		{
		   websWrite(wp, T("Technical Code Length Must Be As Badge Length Please Try Again"));
		   websMsgEnd(wp);
		   websFooter(wp);
		   websDone(wp, 200);
		   return ;
		}
	}
	else
	{
		if(!code2 || strlen(code2) < 1)
           res5 = 1;
		else
		{
		   websWrite(wp, T("Technical Code Do Not Match Please Try Again"));
		   websMsgEnd(wp);
	       websFooter(wp);
	       websDone(wp, 200);
	       return ;
		}
	}

// reset only accesscode

	printf("code3 = %s code4 = %s \n",code3,code4);
	if(code3 && strlen(code3)>0)
	{
		if((int)strlen(code3) == atoi(badgeLen))
		{
			if(strcmp(code3,code4) == 0)
				res19 = SetProperty("resetaccesscode",code3);
			else
			{
				websWrite(wp, T("Code Do Not Match Please Try Again"));
				websMsgEnd(wp);
				websFooter(wp);
				websDone(wp, 200);
				return ;
			}
		}
		else
		{
		   websWrite(wp, T("Reset Only Code Length Must Be As Badge Length Please Try Again"));
		   websMsgEnd(wp);
		   websFooter(wp);
		   websDone(wp, 200);
		   return ;
		}
	}
	else
	{
		if(!code4 || strlen(code4) < 1)
           res19 = 1;
		else
		{
		   websWrite(wp, T("Reset Only Code Do Not Match Please Try Again"));
		   websMsgEnd(wp);
	       websFooter(wp);
	       websDone(wp, 200);
	       return ;
		}
	}

//
	if(strcmp(swipeAndGo,"true")==0)
	  res1 = SetProperty("TurnOnSwipAndGo","true");
	else
	 res1 = SetProperty("TurnOnSwipAndGo","false");

	if(strcmp(FpVer,"true")==0)
	{
	  if(strcmp(FpUnit,"true")==0)
		res14 = SetProperty("FPverification","true");

		if(strcmp(KeypadNeedsFPV,"true")==0)
			res20 = SetProperty("KeypadNeedsFPV","true");
		else
			res20 = SetProperty("KeypadNeedsFPV","false");

		if(strcmp(CardNeedsFPV,"true")==0)
			res21 = SetProperty("CardNeedsFPV","true");
		else
			res21 = SetProperty("CardNeedsFPV","false");

	}
	else
	{
	  if(strcmp(FpUnit,"true")==0)
		res14 = SetProperty("FPverification","false");
		if(strcmp(KeypadNeedsFPV,"true")==0)
			res20 = SetProperty("KeypadNeedsFPV","false");
		if(strcmp(CardNeedsFPV,"true")==0)
			res21 = SetProperty("CardNeedsFPV","false");

	}

    res22 = SetProperty("BadgeOffset",BadgeOffset);
	res23 = SetProperty("BadgeValid",BadgeValid);
	res24 = SetProperty("KeypadLength",KeypadLength);

	if(strcmp(OfflineMode,"true")==0)
	  res16 = SetProperty("OfflineMode","true");
	else
	 res16 = SetProperty("OfflineMode","false");

	if(strcmp(keypad,"true")==0)
       sources[0] = 'K';
	if(strcmp(mag,"true")==0)
	   sources[1] = 'S';
	if(strcmp(barcode,"true")==0)
	   sources[2] = 'W';
    if(strcmp(prox,"true")==0)
	   sources[3] = 'P';

	if(strcmp(FpUnit,"true")==0 && strcmp(fprint,"true")==0)
	   sources[4] = 'F';

	if(strcmp(FpUnit,"true")==0)
	  res15 = SetProperty("EnableFpUnit","true");
	else
	{
	  res15 = SetProperty("EnableFpUnit","false");
	  res14 = SetProperty("FPverification","false");
	  sources[4] = 'N';
	 }
	//else
	//{
       //if(strcmp(FpVer,"true")==0)
		//res14 = SetProperty("FPverification","false");
	//}

	res2 = SetProperty("UpdateTimeClock",buffer);
    res3 = SetProperty("BadgeLength",badgeLen);
	res4 = SetProperty("BadgeSources",sources);
	res6 = SetProperty("ClockIn",clockIn);
	res7 = SetProperty("InPunch",inPunch);
	res8 = SetProperty("OutPunch",outPunch);
	res9 = SetProperty("SwipePunch",swipePunch);
	res10 = SetProperty("ClockInLev1",ClockInLev1);
    res11 = SetProperty("ClockInLev2",ClockInLev2);
	res12 = SetProperty("ClockInLev3",ClockInLev3);
	res13 = SetProperty("ClockInLev4",ClockInLev4);

	res17 = SetProperty("TemplatesAvailableTimer", TemplatesAvailableTimer);
	res18 = SetProperty("Title", TitleDisplay);

	XMLPropertiesFree();
	if(res1 & res2 & res3 & res4 & res5 &
		res6 & res7 & res8 & res9 & res10 &
		res11 & res12 & res13 & res14 & res15 &
		res16 & res17 &res18 &res19
	  & res20 & res21 & res22 & res23 & res24
	)
	{
		websWrite(wp, T("Operation Mode Configuration Succeed Please Reset the Clock"));
	}
	else
	{
		websWrite(wp, T("Operation Mode Configuration Failed"));
	}
    websMsgEnd(wp);
	websFooter(wp);
	websDone(wp, 200);
}
static void formPlayBuzzer(webs_t wp, char_t *path, char_t *query)
{
  user_audible_init ();
  user_audible_set(1000);
  websRedirect(wp,"/clockmode.asp");
}

static void formStopBuzzer(webs_t wp, char_t *path, char_t *query)
{

  user_audible_set(0);
  user_audible_close ();
  websRedirect(wp,"/clockmode.asp");
}

static void		formSetBuzzerVolume(webs_t wp, char_t *path, char_t *query)
{
  char_t	 *level;
  int lev;
  user_audible_init ();
  level = websGetVar(wp, T("buzzer"), T(""));
  lev = 10 * atoi(level);
  user_audible_volume(lev);
  XMLPropertiesInit(XML_FILE);
  SetProperty("BuzzerVolume",level);
  XMLPropertiesFree();
  websRedirect(wp,"/clockmode.asp");
}

static void formSetContrastLevel(webs_t wp, char_t *path, char_t *query)
{
  char_t	 *level;
  int contrast = 0x8000;
  int levelInt ;
  char sys_cmd_buf[30];
  user_init();
  user_output_initialize();
  user_output_write(1, 2, "Contrast Adjustment");
  level = websGetVar(wp, T("contrast"), T(""));
  levelInt = atoi(level);
  if(levelInt == 15){user_output_contrast (0x1000);contrast = 0x1000;}
  if(levelInt == 14){user_output_contrast (0x2000);contrast = 0x2000;}
  if(levelInt == 13){user_output_contrast (0x3000);contrast = 0x3000;}
  if(levelInt == 12){user_output_contrast (0x4000);contrast = 0x4000;}
  if(levelInt == 11){user_output_contrast (0x5000);contrast = 0x5000;}
  if(levelInt == 10){user_output_contrast (0x6000);contrast = 0x6000;}
  if(levelInt == 9){user_output_contrast (0x7000);contrast = 0x7000;}
  if(levelInt == 8){user_output_contrast (0x8000);contrast = 0x8000;}
  if(levelInt == 7){user_output_contrast (0x9000);contrast = 0x9000;}
  if(levelInt == 6){user_output_contrast (0xA000);contrast = 0xA000;}
  if(levelInt == 5){user_output_contrast (0xB000);contrast = 0xB000;}
  if(levelInt == 4){user_output_contrast (0xC000);contrast = 0xC000;}
  if(levelInt == 3){user_output_contrast (0xD000);contrast = 0xD000;}
  if(levelInt == 2){user_output_contrast (0xE000);contrast = 0xE000;}
  if(levelInt == 1){user_output_contrast (0xF000);contrast = 0xF000;}
  sprintf( sys_cmd_buf,"fw_setenv contrast %d", contrast);
#ifdef WIN32
#else
  system( sys_cmd_buf);
#endif
  XMLPropertiesInit(XML_FILE);
  SetProperty("ContrastLevel",level);
  XMLPropertiesFree();
  websRedirect(wp,"/clockmode.asp");
}
static void formApplicationReset(webs_t wp, char_t *path, char_t *query)
{
	int res;
    a_assert(wp);
	websHeader(wp);
	websMsgStart(wp);

#ifdef WIN32
	res = 0;
#else
	res = system("/home/terminal/bin/ResetApplication.sh");
#endif
	if(res)
      websWrite(wp, T("Application Reset Failed Please Try Again"));
	else
	{
	  websWrite(wp, T("Application Reset Succeed Please Wait..."));
#ifdef WIN32
	  Sleep(7);
#else
	  sleep(7);
#endif
      websWrite(wp, T("<br>Application Reset Finished"));
	}
	websMsgEnd(wp);
	websFooter(wp);
	websDone(wp, 200);
}


static void formFactoryReset(webs_t wp, char_t *path, char_t *query)
{
	int res1,res2 = true;
    a_assert(wp);
	websHeader(wp);
	websMsgStart(wp);

	XMLPropertiesInit(XML_FILE);
	if(res2)
		res2 = SetProperty("WebService","http://212.179.32.124:446/Clocks/Service.asmx");
	if(res2)
		res2 = SetProperty("ClockId","NULL");
	if(res2)
		res2 = SetProperty("Account","NULL");
	if(res2)
		res2 = SetProperty("TimeOut","05");
	if(res2)
		res2 = SetProperty("HeartBeatTimer","05");
	if(res2)
		res2 = SetProperty("GetProjectTasksTimer","86400");
	if(res2)
		res2 = SetProperty("UseProxy","false");
	if(res2)
		res2 = SetProperty("Proxy","192.168.2.17");
	if(res2)
		res2 = SetProperty("ProxyPort","8080");
	if(res2)
		res2 = SetProperty("UseProxyAuthentication","false");
	if(res2)
		res2 = SetProperty("ProxyUserName","NULL");
	if(res2)
		res2 = SetProperty("ProxyPassword","NULL");
	if(res2)
		res2 = SetProperty("TurnOnSwipAndGo","false");
	if(res2)
		res2 = SetProperty("UseSoapActionHeader","true");
	if(res2)
		res2 = SetProperty("SoapActionHeader","Synel/Services");
	if(res2)
		res2 = SetProperty("ServerTimeZone","UTC-6");
	if(res2)
		res2 = SetProperty("ClockTimeZone","UTC+2");
	if(res2)
		res2 = SetProperty("UpdateTimeClock","03:00:00");
	if(res2)
		res2 = SetProperty("BadgeLength","14");
	if(res2)
		res2 = SetProperty("accesscode","00000000000000");

	if(res2)
		res2 = SetProperty("resetaccesscode","00000000000000");

	if(res2)
		res2 = SetProperty("BadgeSources","KSWNN");
	if(res2)
		res2 = SetProperty("ClockIn","F2");
	if(res2)
		res2 = SetProperty("InPunch","F10");
	if(res2)
		res2 = SetProperty("OutPunch","F12");
	if(res2)
		res2 = SetProperty("SwipePunch","F1");
	if(res2)
		res2 = SetProperty("ClockInLev1","PS");
	if(res2)
		res2 = SetProperty("ClockInLev2","PC");
	if(res2)
		res2 = SetProperty("ClockInLev3","TC");
	if(res2)
		res2 = SetProperty("ClockInLev4","AC");
	if(res2)
		res2 = SetProperty("BuzzerVolume","1");
	if(res2)
		res2 = SetProperty("ContrastLevel","10");
	if(res2)
		res2 = SetProperty("watchdog","ON");
	if(res2)
		res2 = SetProperty("autorestart","ON");
	if(res2)
		res2 = SetProperty("FPverification","false");
    if(res2)
		res2 = SetProperty("EnableFpUnit","false");
	if(res2)
		res2 = SetProperty("UseSqlServer","false");

	if(res2) res2 = SetProperty("Title", "Prologic/TEAMS");
	if(res2) res2 = SetProperty("OfflineMode","true");
	if(res2) res2 = SetProperty("TemplatesAvailableTimer", "86400");
	if(res2) res2 = SetProperty("VerifySSL", "false");
	if(res2) res2 = SetProperty("SSLCertFile", "");

    XMLPropertiesFree();
	if(res2)
	{
#ifdef WIN32
		res1 = 0;
#else
		res1 = system("/home/terminal/bin/ResetApplication.sh");
#endif
		if(res1)
	    {
			websWrite(wp, T("Factory Reset Failed Please Try Again"));
		}
		else
		{
			websWrite(wp, T("Factory Reset Succeed Please Wait..."));
#ifdef WIN32
			Sleep(7);
#else
			sleep(7);
#endif
			websWrite(wp, T("<br>Factory Reset Finished"));
		}
	}
	else
	{
	     websWrite(wp, T("Set Factory Reset Configuration Failed Please Try Again"));
	}
	websMsgEnd(wp);
	websFooter(wp);
	websDone(wp, 200);
}

static void formTerminalReboot(webs_t wp, char_t *path, char_t *query)
{
	int res;
    a_assert(wp);
	websHeader(wp);
	websMsgStart(wp);

#ifdef WIN32
	res = 0;
#else
    system("hwclock -w");
	res = system("reboot");
#endif
	if(res)
      websWrite(wp, T("Terminal Reboot Failed Please Try Again"));
	else
	{
	  websWrite(wp, T("Terminal Reboot Succeed Please Wait And Connect Back After Some Seconds"));
	}
	websMsgEnd(wp);
	websFooter(wp);
	websDone(wp, 200);


}
static int		aspGetFpUnitStatus(int eid, webs_t wp, int argc, char_t **argv)
{
    char* FpUnit = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    FpUnit = GetProperty("EnableFpUnit");
	if(FpUnit && strcmp(FpUnit,"true") == 0)
	{
		nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"FpUnit\" checked value=\"true\">"));
		XMLPropertiesFree();
		return nBytes;
	}
    nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"FpUnit\" value=\"true\">"));
	XMLPropertiesFree();
	return nBytes;
}
static int		aspGetFpVerificationStatus(int eid, webs_t wp, int argc, char_t **argv)
{
    char* FpVer = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    FpVer = GetProperty("FPverification");
	if(FpVer && strcmp(FpVer,"true") == 0)
	{
		nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"FpVer\" checked value=\"true\">"));
		XMLPropertiesFree();
		return nBytes;
	}
    nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"FpVer\" value=\"true\">"));
	XMLPropertiesFree();
	return nBytes;
}

static int		aspGetSwipeAndGoStatus(int eid, webs_t wp, int argc, char_t **argv)
{
    char* SwipeAndGo = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    SwipeAndGo = GetProperty("TurnOnSwipAndGo");
	if(SwipeAndGo && strcmp(SwipeAndGo,"true") == 0)
	{
		nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"SwipeAndGo\" checked value=\"true\">"));
		XMLPropertiesFree();
		return nBytes;
	}
    nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"SwipeAndGo\" value=\"true\">"));
	XMLPropertiesFree();
	return nBytes;
}
static int		aspGetBadgeLength(int eid, webs_t wp, int argc, char_t **argv)
{
    char* bl = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    bl = GetProperty("BadgeLength");
	if(!bl && strcmp(bl,"NULL") == 0)
	{
		nBytes = websWrite(wp,T("14"));
		XMLPropertiesFree();
		return nBytes;
	}
    nBytes = websWrite(wp,T(bl));
	XMLPropertiesFree();
	return nBytes;

}
static int		aspGetReadersStatus(int eid, webs_t wp, int argc, char_t **argv)
{
   char* sources = NULL;
    int	  nBytes1,nBytes2,nBytes3,nBytes4,nBytes5;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    sources = GetProperty("BadgeSources");
	if(!sources || strcmp(sources,"NULL") == 0 || strlen(sources) < 4)
	{
		nBytes1 = websWrite(wp,T("<input id=\"kp\" type=\"CHECKBOX\" name=\"keypad\" value=\"true\">KeyPad"));
		nBytes2 = websWrite(wp,T("<input id=\"mag\" type=\"CHECKBOX\" name=\"magnetic\" value=\"true\">Magnetic"));
		nBytes3 = websWrite(wp,T("<input id=\"br\" type=\"CHECKBOX\" name=\"barcode\" value=\"true\">Bar Code"));
		nBytes4 = websWrite(wp,T("<input id=\"prox\" type=\"CHECKBOX\" name=\"proximity\" value=\"true\">Proximity"));
		nBytes5 = websWrite(wp,T("<input id=\"fp\" type=\"CHECKBOX\" name=\"fprint\" value=\"true\">FPU Identification"));
		XMLPropertiesFree();
		return nBytes1+nBytes2+nBytes3+nBytes4+nBytes5;
	}
	if(sources[0] == 'K')
	    nBytes1 = websWrite(wp,T("<input id=\"kp\" type=\"CHECKBOX\" name=\"keypad\" checked value=\"true\">KeyPad"));
	else
		nBytes1 = websWrite(wp,T("<input id=\"kp\" type=\"CHECKBOX\" name=\"keypad\" value=\"true\">KeyPad"));
	if(sources[1] == 'S')
     nBytes2 = websWrite(wp,T("<input id=\"mag\" type=\"CHECKBOX\" name=\"magnetic\" checked value=\"true\">Magnetic"));
	else
		nBytes2 = websWrite(wp,T("<input id=\"mag\" type=\"CHECKBOX\" name=\"magnetic\" value=\"true\">Magnetic"));
	if(sources[2] == 'W')
       nBytes3 = websWrite(wp,T("<input id=\"br\" type=\"CHECKBOX\" name=\"barcode\" checked value=\"true\">Bar Code"));
	else
      nBytes3 = websWrite(wp,T("<input id=\"br\" type=\"CHECKBOX\" name=\"barcode\" value=\"true\">Bar Code"));
	if(sources[3] == 'P')
  nBytes4 = websWrite(wp,T("<input id=\"prox\" type=\"CHECKBOX\" name=\"proximity\" checked value=\"true\">Proximity"));
	else
     nBytes4 = websWrite(wp,T("<input id=\"prox\" type=\"CHECKBOX\" name=\"proximity\" value=\"true\">Proximity"));
	if(sources[4] == 'F')
  nBytes5 = websWrite(wp,T("<input id=\"fp\" type=\"CHECKBOX\" name=\"fprint\" checked value=\"true\">FPU Identification"));
	else
     nBytes5 = websWrite(wp,T("<input id=\"fp\" type=\"CHECKBOX\" name=\"fprint\" value=\"true\">FPU Identification"));

	XMLPropertiesFree();
	return nBytes1+nBytes2+nBytes3+nBytes4+nBytes5;
}
static int		aspGetClockUpdateTime(int eid, webs_t wp, int argc, char_t **argv)
{
    char* clock = NULL;
	char h[3],m[3],s[3];
    int	  nBytes1,nBytes2,nBytes3;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    clock = GetProperty("UpdateTimeClock");
	if(!clock || strcmp(clock,"NULL") == 0)
	{
		nBytes1 = websWrite(wp,T("<input type=text maxlength=2  name=\"hour\" id=\"hh\" title=\"hours\" size=1 value= \"02\"> : "));
		nBytes2 = websWrite(wp,T("<input type=text maxlength=2  name=\"min\" id=\"mm\" title=\"minuts\" size=1 value= \"00\"> : "));
		nBytes3 = websWrite(wp,T("<input type=text maxlength=2  name=\"sec\" id=\"ss\" title=\"seconds\" size=1 value= \"00\">"));

		XMLPropertiesFree();
		return nBytes1+nBytes2+nBytes3;
	}

    strncpy(h,clock,2);strncpy(m,clock+3,2);strncpy(s,clock+6,2);
	h[2] = '\0';m[2] = '\0';s[2] = '\0';
	nBytes1 = websWrite(wp,T("<input type=text maxlength=2  name=\"hour\" id=\"hh\" title=\"hours\" size=1 value= %s> : "),h);
	nBytes2 = websWrite(wp,T("<input type=text maxlength=2  name=\"min\" id=\"mm\" title=\"minuts\" size=1 value= %s> : "),m);
	nBytes3 = websWrite(wp,T("<input type=text maxlength=2  name=\"sec\" id=\"ss\" title=\"seconds\" size=1 value= %s>"),s);
	XMLPropertiesFree();
	return nBytes1+nBytes2+nBytes3;
}

static int aspIsSelectedFunctionKey(int eid, webs_t wp, int argc, char_t **argv)
{
    char* slectedKey = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    slectedKey = GetProperty(argv[0]);
	//printf("slectedKey = %s key = %s\n",slectedKey,argv[1]);
	if(slectedKey && strcmp(slectedKey,argv[1]) == 0)
	{
		nBytes = websWrite(wp,T("<option selected value =\"%s\">%s</option>"),argv[1],argv[1]);
		//printf("selected %s\n",argv[1]);
		XMLPropertiesFree();
		return nBytes;
	}
	else
	{
		nBytes = websWrite(wp,T("<option value =\"%s\">%s</option>"),argv[1],argv[1]);
		//printf("not selected %s\n",argv[1]);
		XMLPropertiesFree();
		return nBytes;
	}
}
static int		aspIsSelectedClockInLevels(int eid, webs_t wp, int argc, char_t **argv)
{
    char* slectedLevel = NULL;
	char level[20];
	int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
	slectedLevel = GetProperty(argv[0]);

	if(strcmp(argv[1],"NONE") == 0)
		strcpy(level,"NONE");
	if(strcmp(argv[1],"PS") == 0)
		strcpy(level,"Project Switch");
	if(strcmp(argv[1],"PC") == 0)
		strcpy(level,"Project Code");
	if(strcmp(argv[1],"TC") == 0)
		strcpy(level,"Task Code");
	if(strcmp(argv[1],"AC") == 0)
		strcpy(level,"Activity Code");

	if(slectedLevel && strcmp(slectedLevel,argv[1]) == 0)
	{
		nBytes = websWrite(wp,T("<option selected value =\"%s\">%s</option>"),argv[1],level);
		//printf("selected %s\n",argv[1]);
		XMLPropertiesFree();
		return nBytes;
	}
	else
	{
		nBytes = websWrite(wp,T("<option value =\"%s\">%s</option>"),argv[1],level);
		//printf("not selected %s\n",argv[1]);
		XMLPropertiesFree();
		return nBytes;
	}

}

static int		aspSelectedBuzzerVolume(int eid, webs_t wp, int argc, char_t **argv)
{
    char* slectedRadio = NULL;
	int slectedRadioInt = 100;
    int i ;
	int	  nBytes,nBytes1;
    nBytes = 0;
	a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    slectedRadio = GetProperty(argv[0]);
	if(slectedRadio)slectedRadioInt = atoi(slectedRadio);
	for(i = 1 ; i < 11 ; i ++)
	{
	  if(slectedRadioInt == i )
	  {
	    nBytes1 = websWrite(wp,T("<INPUT TYPE=RADIO checked onclick = SetBuzzerVolume(%d); NAME=\"buzzer\" VALUE=%d>"),i,i);
        nBytes = nBytes1 + nBytes;
	  }
	  else
	  {
	     nBytes1 = websWrite(wp,T("<INPUT TYPE=RADIO onclick = SetBuzzerVolume(%d); NAME=\"buzzer\" VALUE=%d>"),i,i);
		 nBytes = nBytes1 + nBytes;
	  }

	}
    XMLPropertiesFree();
	return nBytes;
}

static int		aspSelectedContrastLevel(int eid, webs_t wp, int argc, char_t **argv)
{
    char* slectedRadio = NULL;
	int slectedRadioInt = 100;
    int i ;
	int	  nBytes,nBytes1;
    nBytes = 0;
	a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    slectedRadio = GetProperty(argv[0]);
	if(slectedRadio)slectedRadioInt = atoi(slectedRadio);
	for(i = 1 ; i < 16 ; i ++)
	{
	  if(slectedRadioInt == i )
	  {
	    nBytes1 = websWrite(wp,T("<INPUT TYPE=RADIO checked onclick = SetContrast(%d); NAME=\"contrast\" VALUE=%d>"),i,i);
        nBytes = nBytes1 + nBytes;
	  }
	  else
	  {
	     nBytes1 = websWrite(wp,T("<INPUT TYPE=RADIO onclick = SetContrast(%d); NAME=\"contrast\" VALUE=%d>"),i,i);
		 nBytes = nBytes1 + nBytes;
	  }

	}
    XMLPropertiesFree();
	return nBytes;

}
static void	websMsgStart(webs_t wp)
{
	websWrite(wp, MSG_START);
}

static void	websMsgEnd(webs_t wp)
{
	websWrite(wp, MSG_END);
}

static int		aspGetOfflineModeStatus(int eid, webs_t wp, int argc, char_t **argv)
{
    char* OfflineMode = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    OfflineMode = GetProperty("OfflineMode");
	if(OfflineMode && strcmp(OfflineMode,"true") == 0)
	{
		nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"OfflineMode\" checked value=\"true\">"));
		XMLPropertiesFree();
		return nBytes;
	}
    nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"OfflineMode\" value=\"true\">"));
	XMLPropertiesFree();
	return nBytes;
}

static int aspGetCurrentTemplatesAvailableTimer(int eid, webs_t wp, int argc, char_t **argv)
{
    char* TemplatesAvailableTimer = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    TemplatesAvailableTimer = GetProperty("TemplatesAvailableTimer");
	if(!TemplatesAvailableTimer || strcmp(TemplatesAvailableTimer,"NULL") == 0)
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	if(TemplatesAvailableTimer) nBytes = websWrite(wp,T(TemplatesAvailableTimer));
	XMLPropertiesFree();
	return nBytes;
}

static int aspGetTitleDisplay(int eid, webs_t wp, int argc, char_t **argv)
{
    char* TitleDisplay = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    TitleDisplay = GetProperty("Title");
	if(!TitleDisplay)
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	else if ( TitleDisplay[0] == '\0')
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	else if ( strcmp(TitleDisplay,"NULL") == 0 )
	{
		nBytes = websWrite(wp,T(""));
		XMLPropertiesFree();
		return nBytes;
	}
	//if(TitleDisplay) nBytes = websWrite(wp,T(TitleDisplay));
	//testing
	if(TitleDisplay) nBytes = websWriteDataNonBlock(wp,T(TitleDisplay),gstrlen(TitleDisplay));
	//endtest
	XMLPropertiesFree();
	return nBytes;

}

static int aspGetKeypadNeedsFPV(int eid, webs_t wp, int argc, char_t **argv)
{
    char* KeypadNeedsFPV = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    KeypadNeedsFPV = GetProperty("KeypadNeedsFPV");

	if(KeypadNeedsFPV && strcmp(KeypadNeedsFPV,"true") == 0)
	{
		nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"KeypadNeedsFPV\" checked value=\"true\">"));
		XMLPropertiesFree();
		return nBytes;
	}
    nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"KeypadNeedsFPV\" value=\"true\">"));


	XMLPropertiesFree();
	return nBytes;

}

static int aspGetCardNeedsFPV(int eid, webs_t wp, int argc, char_t **argv)
{
    char* CardNeedsFPV = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    CardNeedsFPV = GetProperty("CardNeedsFPV");

	if(CardNeedsFPV && strcmp(CardNeedsFPV,"true") == 0)
	{
		nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"CardNeedsFPV\" checked value=\"true\">"));
		XMLPropertiesFree();
		return nBytes;
	}
    nBytes = websWrite(wp,T("<input id=\"CHECKB\" type=\"CHECKBOX\" name=\"CardNeedsFPV\" value=\"true\">"));


	XMLPropertiesFree();
	return nBytes;

}

static int aspGetBadgeOffset(int eid, webs_t wp, int argc, char_t **argv)
{
    char* bl = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    bl = GetProperty("BadgeOffset");
	if(!bl && strcmp(bl,"NULL") == 0)
	{
		nBytes = websWrite(wp,T("14"));
		XMLPropertiesFree();
		return nBytes;
	}
    nBytes = websWrite(wp,T(bl));
	XMLPropertiesFree();
	return nBytes;

}

static int aspGetBadgeValid(int eid, webs_t wp, int argc, char_t **argv)
{
    char* bl = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    bl = GetProperty("BadgeValid");
	if(!bl && strcmp(bl,"NULL") == 0)
	{
		nBytes = websWrite(wp,T("14"));
		XMLPropertiesFree();
		return nBytes;
	}
    nBytes = websWrite(wp,T(bl));
	XMLPropertiesFree();
	return nBytes;

}

static int aspGetKeypadLength(int eid, webs_t wp, int argc, char_t **argv)
{
    char* bl = NULL;
    int	  nBytes;
    a_assert(wp);
	XMLPropertiesInit(XML_FILE);
    bl = GetProperty("KeypadLength");
	if(!bl && strcmp(bl,"NULL") == 0)
	{
		nBytes = websWrite(wp,T("14"));
		XMLPropertiesFree();
		return nBytes;
	}
    nBytes = websWrite(wp,T(bl));
	XMLPropertiesFree();
	return nBytes;
}

//eof
