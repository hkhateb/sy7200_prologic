/* Include files for Testlegacy. */

#include "Spieler.h"



int main (int argc, char * argv [])

{


	// rearrange calls
	//OperationDisplayDriver * oper = new OperationDisplayDriver(); // init the  I/O operation system object
	Spieler demo; //create the demo object , this object contain all the methods we need for the demo application

	OperationDisplayDriver * oper = new OperationDisplayDriver(); // init the  I/O operation system object

	OperationStatusCode code;
    demo.initSetupSystemDataStructure();  //init the data structure for the system setup configuration
    demo.SetupSystem(); // init the setup configuration values
	//demo.RunHeartBeatThread();//creat thread for heart beat web service

	user_audible_init ( ); // init the LEDs controller
	user_visual_init ( );   // init the audio controller buzzer
	user_audible_volume(0); // set the buzzer volume to level 100.

    oper->RunIdleDisplayProcess(); // creat the thread of the idle display .




	while(true)
	{

		const char * msg = oper->GetOperation()->GetEmployeeMessage(oper->getBadgeBuffer());

		   if(msg != NULL && strlen(msg) != 0)
		   {
				user_output_clear();
				int col = ( 20 - strlen("BADGE NUMBER") ) / 2 + 1;
				if(msg == oper->getBadgeBuffer())
				   user_output_write(2, col, "BADGE NUMBER");
				col = ( 20 - strlen(msg) ) / 2 + 1;
				user_output_write(3, col, msg);
				// GetMessageTimeout return the time out in milliseconds
				usleep(oper->GetOperation()->GetMessageTimeout() * 1000); // usleep accept time in microseconds
		   }

		   // after we display the badge ,we wait for function .
		   OperationStatusCode code1 = oper->ReadFunction(TYPE_Employee);
		   if(code1 == SC_Allow )
		   {
			   //if the function is defined send it to the web service and display the response.
			  // unlock the LCD mutex that locked by the ReadBadge.
			  pthread_mutex_unlock( &OperationDisplayDriver::lockDisplay );
			 // pthread_cond_signal(&FPOperation::fprint_cond_flag);// wake finger print thread.
			  //if(oper->getSourceBadge() == SysInputSourceFprint)pthread_cond_signal(&FPOperation::fprint_cond_flag);
			  oper->Reset(); //reset the buffers;
		   }
		   else
		   {
			    //if the function not defined unlock the LCD mutex and continue to the next badge.
				pthread_mutex_unlock( &OperationDisplayDriver::lockDisplay );
				if(oper->getSourceBadge() == SysInputSourceFprint)pthread_cond_signal(&FPOperation::fprint_cond_flag);
				oper->Reset(); //reset the buffers;
		   }
		}

}


/*
	initSetupSystemDataStructure:

	allocate the setup system configuration tables we need to the demo .

Description:

	allocate the setup system configuration tables we need to the demo.


Arguments:
	void

Returns:
	void

*/




void Spieler::initSetupSystemDataStructure(void)

{
	Code code = SC_OKAY;

	//define static struct that contain the tables we need to the allocate to the setup system configuration.
	static struct {
		struct {
			InterfaceMessageBadgeHolder* badge; // table for employee messages
		}
		message;
		struct {
            //table that define the keys item
			//the maximum keys number is 13  ,from F1 to F12 and virtual key for swipe and go state
			//the characteristic char for every keys similar to a telephone keypad ,
			//F1='1' F2='2' ......F9='9' F11='0' F10='*' F12='#' (virtual)Swipe&GO = '?'
			SysFunctionKeyItem key [SysFunctionKeyMax];
			//table that define the levels detailes for every function key
			//length of the SysFunctionDetailItem table is the (function keys number)*(the maximum level for every key)
			//SysFunctionDetailMax = SysFunctionKeyMax * SysFunctionKeyLevelsMax = 13*6
			SysFunctionDetailItem detail [SysFunctionDetailMax];
		}
		function;
	}
	tables;

    tables.message.badge = new InterfaceMessageBadgeVector();
	//submit the employees masseges table in the setup system data structure
	SystemSetup.message.badge = tables.message.badge;
	//submit the function keys tables in the setup system data structure
	SystemSetup.function.key = tables.function.key;
	SystemSetup.function.detail = tables.function.detail;

	return;
}

/*
	SetupSystem:

	setup the system configuration .

Description:

	setup the system configuration .


Arguments:
	void

Returns:
	void

*/


void Spieler::SetupSystem()
{
   //setup prompt types.
	strcpy(SystemSetup.user.prompt.idle,"Updating SY7200"); // Idle Prompt Display 20 characters max
	strcpy(SystemSetup.user.prompt.invalid_source,"INVALID SOURCE");//Invalid Source Message 20 characters max
	strcpy(SystemSetup.user.prompt.invalid_badge,"INVALID BADGE");//Invalid Badge Message 20 characters max
	strcpy(SystemSetup.user.prompt.try_again,"PLEASE TRY AGAIN");//Please Try Again 20 characters max
	strcpy(SystemSetup.user.prompt.enter_function,"Update in Progress");//Enter Function Message 20 characters max

   //setup time out
   SystemSetup.user.timeout.data = 90; //General Data Input time-out tenths of seconds
   SystemSetup.user.timeout.function = 50;//Enter Function time-out	tenths of seconds
   SystemSetup.user.timeout.message = 10 /*30*/ ;//Message time-out	tenths of seconds
   SystemSetup.user.timeout.error = 30; //Error message time-out tenths of seconds

   XMLProperties* xmlParser = new XMLProperties("properties.xml");
   char badgeLen[10];
   char sources[6];
   badgeLen[0] = '\0';
   sources[0]='N';sources[1]='N';sources[2]='N';sources[3]='N';sources[4]='N';sources[5]='\0';
   SysInputSource so = SysInputSourceInvalid;
   xmlParser->GetProperty("BadgeLength",badgeLen);
   xmlParser->GetProperty("BadgeSources",sources);
   if(strlen(badgeLen) > 0)
   {
		//setup badge length from xml file configuration
	 SystemSetup.badge.length.maximum = atoi(badgeLen);
	 SystemSetup.badge.length.minimum = atoi(badgeLen);
	 SystemSetup.badge.length.valid = atoi(badgeLen);
	}
	else
	{
		//
	    SystemSetup.badge.length.maximum = 10;
		SystemSetup.badge.length.minimum = 10;
		SystemSetup.badge.length.valid = 10;
	}

    //setup badge source from the xml file configuration.
	if(sources[0] == 'K')
	   so = so | SysInputSourceKeypad;
	if(sources[1] == 'S')
       so = so | SysInputSourceMagnetic;
	if(sources[2] == 'W')
       so = so | SysInputSourceBarcode;
	if(sources[3] == 'P'){so = so | SysInputSourceProximity;}
	if(sources[4] == 'F')
	  so = so | SysInputSourceFprint;
	SystemSetup.source.initial = so;
	SystemSetup.source.function = SysInputSourceDefault;
	SystemSetup.source.supervisor = so;
	SystemSetup.source.super_employee = so;
    SystemSetup.source.configuration = so;


	char * tmp = NULL;
	bool btmp = false;
	//xmlParser->GetProperty("OfflineMode",tmp);
	tmp = xmlParser->GetProperty("OfflineMode"); // if Use Sql Server or not ?
	if(tmp && strcmp(tmp,"true") == 0)
	  btmp = true;
	else
	  btmp = false;
	if(tmp && strcmp(tmp,"1") == 0)
	  btmp = true;
	else
	  btmp = false;

	tmp = NULL;


}



/*
ParseAndDisplayResponse:

	This function parse the xml response format and display the message if exist if no it's display
	default massege .

Description:

	This function parse the xml response format and display the message if exist if no it's display
	default massege .

Arguments:
	oper - pointer to operation IO object to access the LCD ,Buzzer & LEDs...
	response - pointer to the buffer the contain the xml response .
    defaultMessage - the default message we display in case no message to display in the response

Returns:
	void

*/


void Spieler::ParseAndDisplayResponse(OperationDisplayDriver * oper,char* response,char* defaultMessage)
{

  int col;
  col = ( 20 - strlen(defaultMessage) ) / 2 + 1;
  char fnmsg[21];
  XMLProperties responseParser;  //create xml parser.
  char status[2];status[0] = '\0';
  char message[64];message[0] = '\0';
  responseParser.GetProperty(response,"Status",status);	 // get the response status element
  responseParser.GetProperty(response,"Message",message);// get the message from the response

  if(strcmp(status,"0") == 0)  // if status == 0 , no permission  for the user to the transaction
  {
        strcpy(fnmsg,"ACCESS DENIED"); //later we display "ACCESS DENIED" message
		col = ( 20 - strlen(fnmsg) ) / 2 + 1;
  }
  if(!(strcmp(status,"1"))) // if status == 0 , transaction was performed.
  {
		oper->GetOperation()->TurnOnLEDGreen(); // tunn on green led .
		oper->GetOperation()->Beep(TYPE_Accept); // beep accept tone buzzer .
  }
  else
  {
	   oper->GetOperation()->TurnOnLEDYellow(); // if status == 0 turn on yellow led.
	   oper->GetOperation()->Beep(TYPE_Reject);  // beep reject tone buzzer.
  }
  if(strlen(status) > 0 && strlen(message) > 0) // if we accept message in the response .
  {
       //cfgMgr.MenuFromBuffer("EMPLOYEE MESSAGE",message); // we use MenuFromBuffer method to display the message
														  // with the ability turning pages in case large message
  }
  else // in case no message or no status response
  {
	   user_output_clear();
	   if(strlen(status) == 0) // if no status we get exception response
	   {
			strcpy(fnmsg,"EXCEPTION RESPONSE");  // display "EXCEPTION RESPONSE"
			col = ( 20 - strlen(fnmsg) ) / 2 + 1;
			user_output_write(2, col, fnmsg);
			int col1 = ( 20 - strlen(oper->GetOperation()->GetPromptMessage(TYPE_Try_Again)) ) / 2 + 1;
			// display "PLEASE TRY AGAIN"
			user_output_write(3, col1,oper->GetOperation()->GetPromptMessage(TYPE_Try_Again));
	   }
	   else // if there is status and no message display the default message
	   {

		    if(defaultMessage && strlen(defaultMessage) > 0)
			{
               //cfgMgr.MenuFromBuffer("EMPLOYEE MESSAGE",defaultMessage);
			}
			else
			{
				col = ( 20 - strlen(fnmsg) ) / 2 + 1;
				user_output_write(2, col, fnmsg);
			}

	   }
	   int mtm = oper->GetOperation()->GetMessageTimeout(); // display message for time out seconds
	   if(mtm > 500)
	   {
			usleep(500000);
			oper->GetOperation()->Beep(TYPE_TurnOff);
			mtm -= 500;
		}
		usleep(mtm * 1000);
  }
  user_output_clear(); // clean the LCD
  oper->GetOperation()->Beep(TYPE_TurnOff); // turn off the buzzer
  oper->GetOperation()->TurnOffLEDGreen();  // trun off the green LED
  oper->GetOperation()->TurnOffLEDYellow();// trun off the yellow LED

}


/*
RunHeartBeatThread:

	This function create thread to send heart beat and check if there connection between the clock and the web service
	the executing start_routine  we supply to the thread is RunHeartBeatFunction.

Description:

	This function create thread to send heart beat and check if there connection between the clock and the web service
	the executing start_routine  we supply to the thread is RunHeartBeatFunction.


Arguments:
     void-

Returns:
	void-

*/
void Spieler::RunHeartBeatThread()
{
  pthread_t HeartBeatThread;
  pthread_attr_t HeartBeatThreadAttr;

  unsigned int s ;
  pthread_attr_init(&HeartBeatThreadAttr);
  pthread_attr_getstacksize ( & HeartBeatThreadAttr , & s );
  pthread_attr_setstacksize ( & HeartBeatThreadAttr , const_thread_stack_size ());

  int ret = 0;
  // create thread to send heart beat and check if there connection between the clock and the web service
  ret = pthread_create(&HeartBeatThread, &HeartBeatThreadAttr, RunHeartBeatFunction, NULL);
  if ( ret != 0 )
  {
		if( ret == EAGAIN ) printf("HeartBeat Thread : Out of Memory\n");
		else if ( ret == EINVAL ) printf( "HeartBeat Thread : Invalid Attribute\n");
		printf("HeartBeat Thread : Failed to Create HeartBeat Thread");
  }

}

/*
RunHeartBeatFunction:

	This function implement the executing start_routine  we supply to the heart beat thread , it's send heartbeat to the	web service and check if we accept response with permission status to access the web service and set the
	communication flag in the OperationDisplayDriver class to true , else the communication flag set to false   .

Description:

	This function implement the executing start_routine  we supply to the heart beat thread , it's send heartbeat to the	web service and check if we accept response with permission status to access the web service and set the
	communication flag in the OperationDisplayDriver class to true , else the communication flag set to false   .


Arguments:
     void-

Returns:
	void-

*/

void *Spieler::RunHeartBeatFunction(void * id)
{
	printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
	int firstTimes = 0;
    char HearBeatFreqStr[15];
	int heartBeatFrequency;
	HearBeatFreqStr[0] = '\0';
	// get the timer frequency of the heart beat from  the xml file "properties.xml"
	XMLProperties* confParser = new XMLProperties("properties.xml");
	confParser->GetProperty("HeartBeatTimer",HearBeatFreqStr);

	if(strlen(HearBeatFreqStr) > 0)  heartBeatFrequency = atol(HearBeatFreqStr);
	else heartBeatFrequency = 5;

	delete confParser;

printf("start HeartBeat.\n");


    while(true)
    {

		//get the time and date in format "%Y-%m-%dT%H:%M:%S"
		time_t optime;
		struct tm * optimeinfo;
		time ( &optime );
		optimeinfo = localtime ( &optime );

		char dateB[22];
		char timeB[22];
		dateB[0] = '\0';
        timeB[0] = '\0';
		strftime(timeB,21,"T%H:%M:%S",optimeinfo);
		strftime(dateB,21,"%Y-%m-%d", optimeinfo);

        char utcTime[50];
		utcTime[0] = '\0';
        strcpy(utcTime,dateB);
		strcat(utcTime,timeB);


	}
}


// eof
