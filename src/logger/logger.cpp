#include "logger.h"

Logger * Logger::logger = NULL;

/**
 * Constructor
 */
Logger::Logger()
{
	fappender = new FileAppender("sy.log");
}

Logger::~Logger()
{
	delete fappender;
}

/**
 * Get the logger instance
 */
Logger * Logger::getLogger()
{
	if( Logger::logger == NULL )
	{
		Logger::logger = new Logger();
	}
	return Logger::logger;
}

/**
 * get the app's log level
 */
FileAppender::LogLevel Logger::getAppLogLevel()
{
	return fappender->getLogLevel();
}

/**
 * Set the application's log level
 */
void Logger::setAppLogLevel(FileAppender::LogLevel level)
{
	return fappender->setLogLevel(level);
}

/**
 * log debug message
 */
void Logger::ldebug(char * msg)
{
	fappender->append(FileAppender::LEVEL_DEBUG, msg);
}

/**
 * log info message
 */
void Logger::linfo(char * msg)
{
	fappender->append(FileAppender::LEVEL_INFO, msg);
}

/**
 * log warning message
 */
void Logger::lwarn(char * msg)
{
	fappender->append(FileAppender::LEVEL_WARN, msg);
}

/**
 * log error message
 */
void Logger::lerror(char * msg)
{
	fappender->append(FileAppender::LEVEL_ERROR, msg);
}

/**
 * log fatal message
 */
void Logger::lfatal(char * msg)
{
	fappender->append(FileAppender::LEVEL_FATAL, msg);
}
