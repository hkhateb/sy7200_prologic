#include <time.h>
#include <pthread.h>

#include "fileappender.h"
#include "SystemGlobals.h"
#include "XMLProperties.h"

FileAppender::FileAppender(char * file)
{
	logFile = file;
	maxFileSize = 8192; //default
	maxBackupFiles = 7; //default
	logLevel = LEVEL_DEBUG; //default
	logOption = LOG_ON; //default

	XMLProperties props(XML_PROPERTIES_FILE);
	//char * log_option = props.GetProperty("logging");
	char * log_option = NULL;

	if( log_option != NULL ) {
		if( ! strcmp(log_option, "OFF")) logOption = LOG_OFF;
	}
	else
	{
		logOption = LOG_ON;
	}

	//char * log_level = props.GetProperty("log_level");
	char * log_level = NULL;

	if(log_level != NULL)
	{
		switch (atoi(log_level))
		{
			case 1 :
				logLevel = LEVEL_DEBUG;
				break;
			case 2 :
				logLevel = LEVEL_INFO;
				break;
			case 3 :
				logLevel = LEVEL_WARN;
				break;
			case 4 :
				logLevel = LEVEL_ERROR;
				break;
			case 5 :
				logLevel = LEVEL_FATAL;
				break;
		}
	}
	else
	{
		logLevel = LEVEL_DEBUG;
	}


}

FileAppender::~FileAppender()
{

}

// Make sure the logging level is >= the defined level
void FileAppender::append(LogLevel level, char * msg)
{
	if(logOption == LOG_ON)
	{
		if(level >= logLevel)
		{
			char fmsg[strlen(msg) + 58];
			formatMessage(level, msg, fmsg);
			append(fmsg);
		}
	}
}

void FileAppender::append(char * msg)
{
	// Open the Log File
	fp = fopen(logFile, "a");

	if( !fp )
	{
		fprintf(stderr, "Couldn't open the Log file\n");
		return;
	}

	fputs(msg, fp);
	fputs("\n", fp);

	// Check the file size to check if we need to rename
	fseek(fp, 0, SEEK_END);
    long size = ftell(fp);
    if(size > maxFileSize)
    {
    	renameLogFiles();
    }

	//close the file
	fclose(fp);
}

// Renames log files for backup
void FileAppender::renameLogFiles()
{
	// first rename all the backup files
	char index[3];
	for (int i = maxBackupFiles - 1; i >= 1; i--)
	{
		char newname[strlen(logFile) + 5];
		strcpy(newname, logFile);
		strcat(newname, ".");
		sprintf(index, "%d", i+1);
		strcat(newname, index);

		char oldname[strlen(logFile) + 5];
		strcpy(oldname, logFile);
		strcat(oldname, ".");
		sprintf(index, "%d", i);
		strcat(oldname, index);
		rename(oldname, newname);
	}

	// rename the current log file
	char newname[strlen(logFile) + 5];
	strcpy(newname, logFile);
	strcat(newname, ".1");
	rename(logFile, newname);
}

// Formats the Message. Adds Time and the Level info to the message
void FileAppender::formatMessage(LogLevel level, char * msg, char * fmsg)
{
	strcpy(fmsg, getFormattedTime());
	char threadid [10];
	//sprintf(threadid, "   %x", ( int ) pthread_self());
	sprintf(threadid, " pid=%d", ( int ) getpid());

	strcat(fmsg, threadid);

	switch(level)
	{
		case LEVEL_DEBUG:
			strcat(fmsg, " DEBUG-");
			break;
		case LEVEL_INFO:
			strcat(fmsg, " INFO-");
			break;
		case LEVEL_WARN:
			strcat(fmsg, " WARN-");
			break;
		case LEVEL_ERROR:
			strcat(fmsg, " ERROR-");
			break;
		case LEVEL_FATAL:
			strcat(fmsg, " FATAL-");
			break;
	}
	strcat(fmsg, msg);
}

// Gets formatted time
char * FileAppender::getFormattedTime()
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	char * time = asctime( optimeinfo );

	// Remove the "\n" at the end
	*(time + strlen(time) - 1) = '\0';
	return time;
}


