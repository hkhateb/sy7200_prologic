#ifndef _LOGGER_H_
#define _LOGGER_H_

#include "fileappender.h"

class Logger
{
private:
	//singleton
	static Logger * logger;
	FileAppender * fappender;
	//private constructor
	Logger();

public:
	// Singleton
	static	Logger *getLogger(void);
	virtual ~Logger();

	FileAppender::LogLevel getAppLogLevel();
	void setAppLogLevel(FileAppender::LogLevel level);

	// Logging methods
	void lwarn(char * msg);
	void ldebug(char * msg);
	void lerror(char * msg);
	void lfatal(char * msg);
	void linfo(char * msg);
};

#endif //_LOGGER_H_
