#ifndef _FILEAPPENDER_H_
#define _FILEAPPENDER_H_

#include <stdio.h>
#include <string.h>

using namespace std;

class FileAppender
{
public:
	FileAppender(char * file);
	virtual ~FileAppender();
	//Logging option (ON/OFF)
	enum LogOption
	{
		LOG_ON = 1,
		LOG_OFF
	};
		
	// Logging Levels
	enum LogLevel
	{
		LEVEL_DEBUG = 1,
		LEVEL_INFO,
		LEVEL_WARN,
		LEVEL_ERROR,
		LEVEL_FATAL
	};
	
	inline LogLevel getLogLevel() const { return logLevel; }
	inline void setLogLevel(LogLevel level) { this->logLevel = level; }
	
	inline int getMaxBackupFiles() const { return maxBackupFiles; }
	inline void setMaxBackupFiles(int num) { this->maxBackupFiles = num; }
	
	inline long getMaxFileSize() const { return maxFileSize; }
	inline void setMaxFileSize(long size) { this->maxFileSize = size; }
	
	inline char * getLogFile() const { return logFile; }
	inline void setLogFile(char * name) { this->logFile = name; }
	
	void append(LogLevel level, char * msg);
	
private:
	FILE *fp;
	int maxBackupFiles;
	long maxFileSize;
	LogLevel logLevel;
	LogOption logOption;
	
	char * logFile;
	
	void renameLogFiles();
	void append(char *);
	void formatMessage(LogLevel, char *, char *);
	char * getFormattedTime();
};

#endif //_FILEAPPENDER_H_
