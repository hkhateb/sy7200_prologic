#ifndef _LOG_H_
#define _LOG_H_

#ifdef __cplusplus
 extern "C" {
 #endif 

void logwarn(char *);
void logdebug(char *);
void logerror(char *);
void logfatal(char *);
void loginfo(char *);

#ifdef __cplusplus
 }
 #endif 

#endif //_LOG_H_
