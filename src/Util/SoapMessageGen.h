#ifndef _SOAPMESSAGEGEN_H_
#define _SOAPMESSAGEGEN_H_

#include <stdio.h>
#include <iostream>
#include <string>

#include "logger.h"
#include "debugprint.h"


#define SOAP_1_1_START "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n <soap:Header>\n"

#define SOAP_1_1_END   " </soap:Body>\n</soap:Envelope>"

#define SOAP_1_2_START "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\">\n <soap12:Header>\n"

#define SOAP_1_2_END    " </soap12:Body>\n</soap12:Envelope>"

#define SOAP_1_1_VER "1.1"

#define SOAP_1_2_VER "1.2"

class SoapMessageGen
{
 public:
	 SoapMessageGen(){xml[0] = '\0'; useSoapActionHeader = false; soapActionHeader = NULL; };
	 SoapMessageGen(bool useSAH , char*  SAH);
     ~SoapMessageGen();
	 char* ClockIn(char* ClockId ,char* Account,char* empId,char* utcTime,char* projectSwitch,char* group,char* project, char* task,char* activity,char* soapVer);
	 //char* InPunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* group,char* soapVer);
     //char* OutPunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* soapVer);
	 char* Heartbeat(char* ClockId ,char* Account ,char* utcTime , char* soapVer );
	 //char* GetProjectTasks(char* ClockId ,char* Account ,char* soapVer);
	 char* GetActivities(char* ClockId ,char* Account ,char* soapVer);
	 char* SwipePunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* soapVer);
	 char* LocalTime(char* ClockId ,char* Account,char* soapVer);
	 char* SetHeader(char* ClockId ,char* Account ,char* soapVer);

     char* SwipePunchWithActivityCode(char* ClockId ,char* Account,char* empId, char * activityCode, char* utcTime, char* soapVer);
	 char* IsValidEmployee(char* ClockId ,char* Account,char* empId, char * utcTime, char* soapVer);
	 //
	 char* EmployeeUsesPin(char* ClockId ,char* Account,char* empId, char * utcTime, char* soapVer);
	 char* ValidEmployeePin(char* ClockId ,char* Account,char* empId, char* empPin, char * utcTime, char* soapVer);
	 char* GetPinTable(char* ClockId ,char* Account ,char* soapVer);
	 //
	 // fpm
	 //
	 char* SetFPTemplate(char* ClockId ,char* Account,char* empId, char* utcTime, char* templateId, char* fptemplate, char* soapVer);
	 char* PopulateFPTemplates(char* ClockId ,char* Account, char* soapVer);
	 char* TemplatesAvailable(char* ClockId ,char* Account, char* utcTime,char* soapVer);
	 //
	 char* GetFPTemplate(char* ClockId ,char* Account,char* empId, char* utcTime, char* soapVer);


	 char xml[2000] ;
private:
	bool useSoapActionHeader;
	char* soapActionHeader;
};

#endif //_SOAPMESSAGEGEN_H_


