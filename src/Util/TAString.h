#ifndef _TASTRING_H_
#define _TASTRING_H_

#include <string.h>

class TAString
{
private:
	char * value;
public:
	TAString(const char *);
	TAString(const TAString& str);
	virtual ~TAString();
	
	TAString substr(size_t index, size_t len = 0);
	const char * c_str();
	void operator=(const char * src);
	void operator=(const TAString & src);
};

#endif //_TASTRING_H_
