/**
 * Simple XML Properties object.
 * It can read properties from xml and and write properties to an xml file.
 * This supports only one level of xml, meaning, the root's
 * children are all leaves. It cannot handle multiple levels
 */

#include "XMLProperties.h"

XMLProperties::XMLProperties(const char * filename)
{
	//printf("%s %d create start\n",__FUNCTION__,__LINE__);
	// Registry File
	this->filename = filename;
	// Create the parser
	parser = XML_ParserCreate(NULL);
	if (! parser) 
	{
		#ifdef DEBUG
		printf("Couldn't allocate memory for parser\n");
		#endif
	}
	buffern = NULL;
	 //printf("%s %d create end\n",__FUNCTION__,__LINE__);
}

XMLProperties::XMLProperties()
{	
    //printf("%s %d create start\n",__FUNCTION__,__LINE__);
	parser = XML_ParserCreate(NULL);
		if (! parser) 
		{
			#ifdef DEBUG
			printf("Couldn't allocate memory for parser\n");
			#endif
		}
		buffern = NULL;
     //printf("%s %d create end\n",__FUNCTION__,__LINE__);
}
XMLProperties::~XMLProperties()
{
	//printf("%s %d free start\n",__FUNCTION__,__LINE__);
	if(parser) XML_ParserFree(parser);
	if(buffern) {free(buffern);buffern=NULL;}
	//printf("%s %d free end\n",__FUNCTION__,__LINE__);
}

int  XMLProperties::GetProperty(const char * key, char* keyValue)
{	
	FILE * fp ;
	int hd ;
	struct stat file_info;

	hd = open(filename, O_RDONLY) ;
    fstat(hd, &file_info);
    close(hd);

	fp = fopen(filename, "r");
	if( !fp )
	{
		//#ifdef DEBUG
		printf("Couldn't open the Properties file\n");
		//#endif
		return 0;
	}
	
	// Reset and reuse the parser
	XML_ParserReset(parser, NULL);
	XML_SetElementHandler(parser, startElementHandler, endElementHandler);
	XML_SetCharacterDataHandler(parser, characterDataHandler);
	
	char value[64];
	value[0] = '\0';
	
	PROP_P prop = (PROP_P) malloc( sizeof(PROP) );
	prop->key = const_cast<char *>(key);
	 
	if(keyValue)
	{
	   prop->value = keyValue;
	}
	else
	{
		if(fp)fclose(fp);
		if(prop)free( prop );
        return 0; //failed
	}
	prop->listKey = NULL;
	prop -> foundList = NO;
	prop->command = FINDKEY;
	prop->found = NO;

	prop->foundList = NO;
	prop->foundElm = NO;
	prop->dataElmFound = NO;
	prop->listElmNum = 0 ;
	prop->elmFrom = 0 ;
	prop->elmTo = 0;
	prop->dataElmNum = 0;
	prop->elmNumInRange = 0;
	
	XML_SetUserData(parser, prop);
	
	
	buffern =(char*)malloc(file_info.st_size + 1);
	bzero(buffern, file_info.st_size+1);
	for (;;) 
	{
		int done = 0;
		int len;		
		
		fread(buffern, 1, file_info.st_size+1, fp);
			
		if (ferror(fp)) 
		{
			#ifdef DEBUG
			printf("File Read error\n");
			#endif
            if(prop)free( prop );
			if(buffern){free(buffern);buffern=NULL;}
			if(fp)fclose(fp);
			return 0;
    	}
    	len = strlen(buffern);
    	done = feof(fp);
    	if (XML_Parse(parser, buffern, len, done) == XML_STATUS_ERROR) 
		{
			#ifdef DEBUG
			printf("Parse error at line %d:\n%s\n", 
					XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
			#endif
			if(buffern){free(buffern);buffern=NULL;}
			if(prop)free( prop );
			if(fp)fclose(fp);
			return 0;
		}
    	if(done) break;
	}
	
	// Cleanup
	if(fp)
	{
		fclose(fp);
	}	
	
	if(prop)free( prop );
	
	//if( strlen(value) != 0 ) {if(buffern){free(buffern);buffern=NULL;} return value;}
	if(buffern){free(buffern);buffern=NULL;}
	 return 1; //success
}

int  XMLProperties::GetProperty(const char *xmlBuffer , const char * key , char* keyValue)
{
    // Reset and reuse the parser
	XML_ParserReset(parser, NULL);
	XML_SetElementHandler(parser, startElementHandler, endElementHandler);
	XML_SetCharacterDataHandler(parser, characterDataHandler);

	char value[64];
	value[0] = '\0';
	
	PROP_P prop = (PROP_P) malloc( sizeof(PROP) );
	prop->key = const_cast<char *>(key);
	if(keyValue)
	{
	   prop->value = keyValue;
	}
	else
	{
		if(prop)free( prop );
        return 0; //failed
	}
	prop->command = FINDKEY;
	prop->listKey = NULL;
	prop -> foundList = NO;
	prop->found = NO;

	prop->foundList = NO;
	prop->foundElm = NO;
	prop->dataElmFound = NO;
	prop->listElmNum = 0 ;
	prop->elmFrom = 0 ;
	prop->elmTo = 0;
	prop->dataElmNum = 0;
	prop->elmNumInRange = 0;

	XML_SetUserData(parser, prop);
	
	int done =0;
	int len = 0;
	len = strlen(xmlBuffer);

    if (XML_Parse(parser,xmlBuffer, len, done) == XML_STATUS_ERROR) 
	{
		#ifdef DEBUG
		printf("Parse error at line %d:\n%s\n", 
		XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
		#endif
        if(prop)free( prop );
	    return 0; //failed

	}
    
	if(prop)free( prop );
	//if( strlen(value) != 0 ){return value;}
	
	 return 1; //success

}

list<ElmData>& XMLProperties::GetListElements(const char *xmlBuffer,const char * listKey,const char * elementKey,int from,int to)
{
    // Reset and reuse the parser
	XML_ParserReset(parser, NULL);
	XML_SetElementHandler(parser, startElementHandler, endElementHandler);
	XML_SetCharacterDataHandler(parser, characterDataHandler);
	
    PROP_P prop = (PROP_P) malloc( sizeof(PROP) );
	prop->key = const_cast<char *>(elementKey);
	prop->listKey = const_cast<char *>(listKey);
	prop->command = FINDLISTKEY;
	prop->found = NO;
    prop->foundList = NO;
	prop->foundElm = NO;
	prop->dataElmFound = NO;
	prop->listElmNum = 0 ;
	prop->elmFrom = from ;
	prop->elmTo = to;
	prop->dataElmNum = 0;
	prop->elmNumInRange = 0;

        char value[64];
	value[0] = '\0';
    prop->value = value;
	
	//erase all the old elements from the list
	list<ElmData>::iterator it1,it2;
	//it1 = listElmData.begin();
	//it2 =  listElmData.end();
    //listElmData.erase(it1,it2);
	listElmData.clear();
	
	prop->listElmData = &listElmData;
	prop->level = 0;
	prop->listLevel = 0;

	XML_SetUserData(parser, prop);
	
	int done =0;
	int len = 0;
	len = strlen(xmlBuffer);
    if (XML_Parse(parser,xmlBuffer, len, done) == XML_STATUS_ERROR) 
	{
		#ifdef DEBUG
		printf("Parse error at line %d:\n%s\n", 
		XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
		#endif
        if(prop)free( prop );
		it1 = listElmData.begin();
		it2 =  listElmData.end();
		listElmData.erase(it1,it2);
		return listElmData;
	}
	if(prop)free( prop );
    return listElmData;
}

bool XMLProperties::SetProperty(const char * key, const char * value)
{
	FILE * fp ;
	int hd ;
	struct stat file_info;

	hd = open(filename, O_RDONLY) ;
    fstat(hd, &file_info);
    close(hd);
    
    fp = fopen(filename, "r");
	if( !fp )
	{
		#ifdef DEBUG
		printf("Couldn't open the Properties file\n");
		#endif
		return false;
	}
	
	const char * tmpfile = "propertiestmp.xml";
	
	FILE * fw = fopen(tmpfile, "w");
	if( !fw )
	{
		#ifdef DEBUG
		printf("Couldn't open the file for write\n");
		#endif
		if( fp ) fclose( fp );
		return false;
	}

	if( fputs(XML_HEADER, fw) == EOF )
	{
		if( fp ) fclose( fp );
		if( fw ) fclose( fw );
		return false;
	}

	if( fputs("\n", fw) == EOF )
	{
		if( fp ) fclose( fp );
		if( fw ) fclose( fw );
		return false;
	}

	// Reset and reuse the parser
	XML_ParserReset(parser, NULL);
	XML_SetElementHandler(parser, startElementHandler, endElementHandler);
	XML_SetCharacterDataHandler(parser, characterDataHandler);

	PROP_P prop = (PROP_P) malloc( sizeof(PROP) );
	prop->key = const_cast<char *>(key);
	prop->value = const_cast<char *>(value);
	prop->command = SETKEY;
	prop->found = NO;
	prop->fw = fw;
	prop->status = CMD_STATUS_DEFAULT;
	prop->level = 0 ;

        prop->listKey = NULL;
	prop->foundList = NO;
	prop->foundElm = NO;
	prop->dataElmFound = NO;
	prop->listElmNum = 0 ;
	prop->elmFrom = 0 ;
	prop->elmTo = 0;
	prop->dataElmNum = 0;
	prop->elmNumInRange = 0;



	XML_SetUserData(parser, prop);

	//bzero(buffer, BUFFERSIZE);
	buffern =(char*)malloc(file_info.st_size + 1);
	bzero(buffern, file_info.st_size+1);

	for (;;) 
	{
		int done;
		int len;		
		
		fread(buffern, 1, file_info.st_size+1, fp);
			
		if (ferror(fp)) 
		{
			#ifdef DEBUG
			printf("File Read error\n");
			#endif
			prop->status = CMD_STATUS_DEFAULT;
			break;
    	}
    	len = strlen(buffern);
    	done = feof(fp);

    	if (XML_Parse(parser, buffern, len, done) == XML_STATUS_ERROR) 
		{
			#ifdef DEBUG
			printf("Parse error at line %d:\n%s\n", 
					XML_GetCurrentLineNumber(parser), XML_ErrorString(XML_GetErrorCode(parser)));
			#endif
			prop->status = CMD_STATUS_DEFAULT;
			break;
		}
    	if(done) break;
	}

	// Cleanup
	if(fp)
	{
		fclose(fp);
	}
	if(fw)
	{
		fclose(fw);
	}	

	// Rename the File
	if( prop->status == CMD_SETKEY_SUCCESS )
	{
		rename(tmpfile, filename);		
		if(prop)free( prop );
		if(buffern){free(buffern);buffern=NULL;}
		return true;
	}

	if(prop)free( prop );
	if(buffern){free(buffern);buffern=NULL;}	
	return false;
}

/**
 * Start Element Handler
 */
void XMLCALL startElementHandler(void * userData, const char * name, const char ** atts)
{
        int i = 0;
	if( userData != NULL )
	{
		PROP_P prop = (PROP_P)userData;
		(prop->level)++; // Increase The Level
		if( (prop->key) && strcmp( name, prop->key ) == 0 )
		{
			prop->found = YES;
			if(prop->command == FINDKEY && sizeof(prop->value) > 0)
				prop->value[0] = '\0';

		}
		if(prop->foundElm)
		{
			prop->dataElmFound = YES; strcpy(prop->dataElmKey,name);
			prop->dataElmNum++;
			ElmData elmData;

              		strcpy(elmData.key,prop->dataElmKey);
					//printf("%s %d elmData.key = %s\n",__FUNCTION__,__LINE__,elmData.key);
			//strncpy(elmData.data,s,len);
			//elmData.data[len] = '\0';
			elmData.elmIndexInList = prop->listElmNum;
			elmData.dataIndexInElm = prop->dataElmNum;
			  
              		prop->listElmData->push_back(elmData);
			//printf("%s %d\n",__FUNCTION__,__LINE__);
		}
		if(prop->foundList && prop->command == FINDLISTKEY && strcmp( name, prop->key ) == 0 && (prop->listLevel+1) == prop->level )
		{ 
		  if((prop->elmFrom <= prop->listElmNum+1 &&  prop->listElmNum+1 <= prop->elmTo))
		  {prop->foundElm = YES; prop->elmNumInRange++;} 
		  else
		    prop->foundElm = NO;    
		  prop->listElmNum++; 
		}	     
		if((prop->listKey) && prop->command == FINDLISTKEY && strcmp( name, prop->listKey ) == 0 )
		  { prop->foundList = YES; prop -> listLevel = prop->level; }
		//printf("key = %s foundList = %d level = %d foundElm = %d dataelm = %d \n",name,prop->foundList,prop->level,prop->foundElm,prop->dataElmNum);
		if( prop->command == SETKEY )
		{
			if( prop->status == CMD_FILE_WRITE_FAIL ) return;
			/*for(int i = 1; i < prop->level; i++) 
			{
				if( fputs("Y", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
			}*/
			if( fputs("<", prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( fputs(name, prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			for(i = 0 ; atts[i] != NULL ; i++) //set attribute
			{
			      if(fputs(" ", prop->fw) == EOF || fputs(atts[i], prop->fw) == EOF)
			      {
                                prop->status = CMD_FILE_WRITE_FAIL;
				return;
			      }
                              i++;
			      if(atts[i] != NULL)
			      {
                              	 if(fputs("=", prop->fw) == EOF || fputs("\"", prop->fw) == EOF || fputs(atts[i], prop->fw) == EOF ||
					fputs("\"", prop->fw) == EOF)
			      	 {
                                	prop->status = CMD_FILE_WRITE_FAIL;
					return;
			      	 }
			       }
			       else
				 break;
			}
			if( prop->level == 1 )
			{
				if( fputs(">\n", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
			}
			else 
			{
				if( fputs(">", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
			}
		}
	}
}

/**
 * End Element Handler
 */
void XMLCALL endElementHandler(void * userData, const char * name)
{
	if( userData != NULL )
	{
		PROP_P prop = (PROP_P)userData;
		(prop->level)--;
		if( prop->command == FINDKEY )
		{
			if( strcmp( name, prop->key ) == 0 )
			{
				prop->found = NO;
			}	
		}
        
		if(prop->command == FINDLISTKEY)
		{
		   if(strcmp(name,prop->dataElmKey) == 0)
			   prop->dataElmFound = NO;
		   if((prop->listKey) && strcmp( name, prop->listKey ) == 0 )
			{ prop->foundList = NO;}
		   if(prop->foundList && strcmp( name, prop->key ) == 0 && (prop->listLevel) == prop->level )
		   { prop->foundElm = NO; prop->dataElmNum = 0;}	
		}

		if( prop->command == SETKEY )
		{
			if( prop->status == CMD_FILE_WRITE_FAIL ) return;
			
			// Handle the case where in the key is not present in the xml file
			if( prop->level == 0 && prop->status != CMD_SETKEY_SUCCESS )
			{
				if( fputs(" <", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(prop->key, prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(">", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(prop->value, prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs("</", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(prop->key, prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				if( fputs(">\n", prop->fw) == EOF )
				{
					prop->status = CMD_FILE_WRITE_FAIL;
					return;
				}
				prop->status = CMD_SETKEY_SUCCESS;
			}
			
			// Handle the case where in the element is empty
			if( prop->found == YES )
			{
				if( prop->status != CMD_SETKEY_SUCCESS )
				{
					if( fputs(prop->value, prop->fw) == EOF )
					{
						prop->status = CMD_FILE_WRITE_FAIL;
						prop->found = NO;
						return;
					}
					prop->status = CMD_SETKEY_SUCCESS;
				}
				prop->found = NO;
			}
			
			if( fputs("</", prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( fputs(name, prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
			if( fputs(">\n", prop->fw) == EOF )
			{
				prop->status = CMD_FILE_WRITE_FAIL;
				return;
			}
		}
	}
}

/**
 * Character Data Handler
 */
void XMLCALL characterDataHandler(void * userData, const XML_Char * s, int len)
{
	int l;
	list<ElmData>::iterator it;
	if( userData != NULL )
	{
		PROP_P prop = (PROP_P)userData;
		if( prop->command == FINDKEY )
		{	
			if( prop->found == YES )
			{
				//printf("***%s prop->key = %s sizeof=%d\n",prop->value,prop->key);
				l = strlen(prop->value);
				strncpy(&(prop->value[l]), s, len);
				prop->value[l+len] = '\0';
				//printf("%s\n",prop->value);
			}
		}
		else if( prop->command == SETKEY )
		{
			if( prop->status == CMD_FILE_WRITE_FAIL ) return;
			// ignore tabs and newlines
			if(*s != '\n' && *s != '\t')
			{
				if( prop->found == YES )
				{
					if( fputs(prop->value, prop->fw) == EOF )
					{
						prop->status = CMD_FILE_WRITE_FAIL;
						return;
					}
					prop->status = CMD_SETKEY_SUCCESS;	
				}
				else
				{
					char data[len + 1];
					strncpy(data, s, len);
					data[len] = '\0';
					if( fputs(data, prop->fw) == EOF )
					{
						prop->status = CMD_FILE_WRITE_FAIL;
					}
				}
			}
		}

		else if(prop->command == FINDLISTKEY)
		{
		    if(prop->foundList && prop->foundElm && prop->dataElmFound)
		    {
		          /*prop->dataElmNum++;
			  ElmData elmData;
                          strcpy(elmData.key,prop->dataElmKey);
			  strncpy(elmData.data,s,len);
			  elmData.data[len] = '\0';
			  elmData.elmIndexInList = prop->listElmNum;
			  elmData.dataIndexInElm = prop->dataElmNum;  
                          prop->listElmData->push_back(elmData);*/
			
			  //printf("%s %d\n",__FUNCTION__,__LINE__);
			  it = prop->listElmData->end();
			  it--;
			  l = strlen((*it).data);
			  if(l+len > 599)len = 599-l;          //data is 600 byte therefore limit the data for 600 byte include the \0
			  strncpy(&((*it).data[l]), s, len);
			  (*it).data[l+len] = '\0';
			  //printf("%s %d (*it).key = %s (*it).data=%s\n",__FUNCTION__,__LINE__,(*it).key,(*it).data);
			  //printf("%s %d\n",__FUNCTION__,__LINE__);

			   /*if(prop->dataElmFound == YES)
			   {
				strncpy(prop->value, s, len);
				prop->value[len] = '\0';
				printf("%s -> %s listElmNum = %d dataElmNum = %d \n",prop->dataElmKey,prop->value,prop->listElmNum,prop->dataElmNum);
			   }*/
			   //printf("%s -> ",prop->elmTable[prop->elmNumInRange-1].key);
			   //printf("%s ",prop->elmTable[prop->elmNumInRange-1].key);
               //printf("elmIndexInList = %s",prop->elmTable[prop->elmNumInRange-1].elmIndexInList);
               //printf("dataIndexInElm = %s\n\n",prop->elmTable[prop->elmNumInRange-1].dataIndexInElm);
			}
		}
	}
}






