
#include "stringutil.h"

bool StringIsNull(char *s)
{
	return (*s == (char)NULL);
}

bool StringIsEmpty(char *s)
{
	return (s == (char*)NULL);
}

bool StringIsBlanks(char *s)
{
	int slen = 0;
	int clen = 0;
	bool allblanks = false;
	if ( *s != (char)NULL )
	{
		slen = strlen(s);
	}
	else
	{
		slen = 0;
		return false;
	}
	while ( *s++ == 0x020)
	{
		clen++;
	}
	if ( slen == clen )
	{
		return true;
	}
	return false;
}

void padstring(char * buffer,char * string,int maxlen)
{
	if ( string )
	{
		for(;*string && maxlen > 0;string++,buffer++,maxlen--)
			*buffer=(*string);
	}
  	if( maxlen > 0)
    for(;maxlen > 0;maxlen--,buffer++)
    	*buffer=' ';
  	return;
}
