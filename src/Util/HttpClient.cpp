#include <mcheck.h>
#include "HttpClient.h"
#include "stringutil.h"

extern Logger * logger;

#ifdef USE_MCHECK
#undef USE_CHECK
#endif

const int HTTP_FILE_MAX = 4096;

struct HTTPResultFile{
	char httpMem[HTTP_FILE_MAX];
	char httpBuf[8];
	unsigned long int httpSize;
	FILE * fOut;
};

void XML_Decode(char *);


//
//Decode the escaping of < and > so the XML can be parsed
//
void XML_Decode(char * buff)
{
	char * f;
	f = strstr(buff,"&lt;");
	while(f != NULL)
	{
		f[0] = '<';
		strcpy(f+1,f+4);
		f = strstr(f,"&lt;");
	}
	f = strstr(buff,"&gt;");
	while(f != NULL)
	{
		f[0] = '>';
		strcpy(f+1,f+4);
		f = strstr(f,"&gt;");
	}
}



static void *myrealloc(void *ptr, size_t size)
{
  /* There might be a realloc() out there that doesn't like reallocing
     NULL pointers, so we take care of it here */
  if(ptr)
    return realloc(ptr, size);
  else
    return malloc(size);
}

static size_t
WriteMemoryCallback(void *ptr, size_t size, size_t nmemb, void *data)
{
  //printf("%s %d\n",__FUNCTION__,__LINE__);
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)data;

  mem->memory = (char *)myrealloc(mem->memory, mem->size + realsize + 1);
  if (mem->memory) {
    memcpy(&(mem->memory[mem->size]), ptr, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
  }
  return realsize;
}

// prototype: static size_t CallbackFile(void *, size_t, size_t, void *)
size_t CallbackFile(void *ptr, size_t size, size_t nmemb, void *data)
{

	//DP("callbackfile entered\n");

	HTTPResultFile * res = (HTTPResultFile *)data;
	size_t realsize = size * nmemb;
	size_t cntr = 0;
	int len = 0;
	int written = 0;
	while(cntr < realsize){
		if(realsize - cntr > HTTP_FILE_MAX - 1){
			memcpy(res->httpMem,(char *)ptr + cntr, HTTP_FILE_MAX - 1);
			res->httpMem[HTTP_FILE_MAX-1] = 0;
			cntr = cntr + HTTP_FILE_MAX - 1;
		}else{
			memcpy(res->httpMem,(char *)ptr + cntr, realsize - cntr);
			res->httpMem[realsize - cntr] = 0;
			cntr = realsize;
		}
		///////
		XML_Decode(res->httpMem);
		///////
		//corner cases
		if(res->httpBuf[0] != 0){
			if(strlen(res->httpBuf) == 1 && res->httpMem[0] == 'g' && res->httpMem[1] == 't' && res->httpMem[2] == ';'){
				res->httpMem[0] = '>';
				strcpy(res->httpMem + 1, res->httpMem + 3);
			} else if(strlen(res->httpBuf) == 1 && res->httpMem[0] == 'l' && res->httpMem[1] == 't' && res->httpMem[2] == ';'){
				res->httpMem[0] = '<';
				strcpy(res->httpMem + 1, res->httpMem + 3);
			} else if(strlen(res->httpBuf) == 2 && res->httpMem[0] == 't' && res->httpMem[1] == ';'){
				if(res->httpBuf[1] == 'g')
					res->httpMem[0] = '>';
				else
					res->httpMem[0] = '<';
				strcpy(res->httpMem + 1, res->httpMem + 2);
			} else if(strlen(res->httpBuf) == 3 && res->httpMem[0] == ';'){
				if(res->httpBuf[1] == 'g')
					res->httpMem[0] = '>';
				else
					res->httpMem[0] = '<';
			}else{
				written += fwrite(res->httpBuf, 1, strlen(res->httpBuf), res->fOut);
			}
			res->httpBuf[0] = 0;
		}
		len = strlen(res->httpMem);
		if(res->httpMem[len-1] == '&'){
			res->httpBuf[0] = '&';
			res->httpBuf[1] = 0;
			res->httpMem[len-1] = 0;
		}
		if(res->httpMem[len-2] == '&' && res->httpMem[len-1] == 'g'){
			res->httpBuf[0] = '&';
			res->httpBuf[1] = 'g';
			res->httpBuf[2] = 0;
			res->httpMem[len-2] = 0;
		}
		if(res->httpMem[len-2] == '&' && res->httpMem[len-1] == 'l'){
			res->httpBuf[0] = '&';
			res->httpBuf[1] = 'l';
			res->httpBuf[2] = 0;
			res->httpMem[len-2] = 0;
		}
		if(res->httpMem[len-3] == '&' && res->httpMem[len-2] == 'g' && res->httpMem[len-1] == 't'){
			res->httpBuf[0] = '&';
			res->httpBuf[1] = 'g';
			res->httpBuf[2] = 't';
			res->httpBuf[3] = 0;
			res->httpMem[len-3] = 0;
		}
		if(res->httpMem[len-3] == '&' && res->httpMem[len-2] == 'l' && res->httpMem[len-1] == 't'){
			res->httpBuf[0] = '&';
			res->httpBuf[1] = 'l';
			res->httpBuf[2] = 't';
			res->httpBuf[3] = 0;
			res->httpMem[len-3] = 0;
		}
		written += fwrite(res->httpMem, 1, strlen(res->httpMem), res->fOut);

		//DP(".");
	}
	res->httpSize += written;

	//DP("\ncallbackfile: return\n");
	return realsize;
}


struct data {
  char trace_ascii; /* 1 or 0 */
};

static
void dump(const char *text,
          FILE *stream, unsigned char *ptr, size_t size,
          char nohex)
{
  size_t i;
  size_t c;

  unsigned int width=0x10;

  if(nohex)
    /* without the hex output, we can fit more on screen */
    width = 0x40;

  fprintf(stream, "%s, %10.10ld bytes (0x%8.8lx)\n",
          text, (long)size, (long)size);

  for(i=0; i<size; i+= width) {

    fprintf(stream, "%4.4lx: ", (long)i);

    if(!nohex) {
      /* hex not disabled, show it */
      for(c = 0; c < width; c++)
        if(i+c < size)
          fprintf(stream, "%02x ", ptr[i+c]);
        else
          fputs("   ", stream);
    }

    for(c = 0; (c < width) && (i+c < size); c++) {
      /* check for 0D0A; if found, skip past and start a new line of output */
      if (nohex && (i+c+1 < size) && ptr[i+c]==0x0D && ptr[i+c+1]==0x0A) {
        i+=(c+2-width);
        break;
      }
      fprintf(stream, "%c",
              (ptr[i+c]>=0x20) && (ptr[i+c]<0x80)?ptr[i+c]:'.');
      /* check again for 0D0A, to avoid an extra \n if it's at width */
      if (nohex && (i+c+2 < size) && ptr[i+c+1]==0x0D && ptr[i+c+2]==0x0A) {
        i+=(c+3-width);
        break;
      }
    }
    fputc('\n', stream); /* newline */
  }
  fflush(stream);
}

static
int my_trace(CURL *handle, curl_infotype type,
             char *data, size_t size,
             void *userp)
{
  struct data *config = (struct data *)userp;
  const char *text;
  (void)handle; /* prevent compiler warning */

  switch (type) {
  case CURLINFO_TEXT:
    fprintf(stderr, "== Info: %s", data);
  default: /* in case a new one is introduced to shock us */
    return 0;

  case CURLINFO_HEADER_OUT:
    text = "=> Send header";
    break;
  case CURLINFO_DATA_OUT:
    text = "=> Send data";
    break;
  case CURLINFO_SSL_DATA_OUT:
    text = "=> Send SSL data";
    break;
  case CURLINFO_HEADER_IN:
    text = "<= Recv header";
    break;
  case CURLINFO_DATA_IN:
    text = "<= Recv data";
    break;
  case CURLINFO_SSL_DATA_IN:
    text = "<= Recv SSL data";
    break;
  }

  dump(text, stderr, (unsigned char *)data, size, config->trace_ascii);
  return 0;
}

struct data config;

HttpClient::HttpClient(char* U ,long timeOut,const bool useProxy,const char* proxy,const char* proxyPort,const bool useProxyAuth,const char* username,const char* password,const bool useSAH , const char* SAH )
{
	//char propVerifySSL[8];
	//char propSSLCertFile[128];

	char propTrace[8];
	XMLProperties xmlParser("properties.xml");


  this->URL[0] = '\0';
  strcpy(this->URL,U);

  this->timeOut = timeOut;				// ************
////testing
//  this->timeOut = 500000; // long timeout for ws debugging
////endtest

  chunk.memory=NULL; /* we expect realloc(NULL, size) to work */
  chunk.size = 0;    /* no data at this point */
  this->useProxy = useProxy;
  if(useProxy){this->proxy =(char*)malloc(strlen(proxy)+1); strcpy(this->proxy,proxy);}
  else this->proxy = NULL;
  if(useProxy){this->proxyPort =(char*) malloc(strlen(proxyPort)+1); strcpy(this->proxyPort,proxyPort);}
  else this->proxyPort = NULL;
  this->useProxyAuthentication = useProxyAuth;
  if(useProxyAuthentication){this->proxyUserName =(char*)malloc(strlen(username)+1); strcpy(this->proxyUserName,username);}
  else this->proxyUserName = NULL;
  if(useProxyAuthentication){this->proxyPassword =(char*)malloc(strlen(password)+1); strcpy(this->proxyPassword,password);}
  else this->proxyPassword = NULL;
  this->useSoapActionHeader = useSAH;
  if(useSAH && strcmp(SAH,"NULL"))
  {
	  int len = strlen ( SAH );
	  this->soapActionHeader = (char*) malloc(len+1);
      strcpy(this->soapActionHeader,SAH);
  }
  else this->soapActionHeader = NULL;

  smg = new SoapMessageGen(useSoapActionHeader,soapActionHeader);
//testing
  //printf("soapActionHeader is %s\n", soapActionHeader);
//endtest


/// initialize tracing options from the properties.xml file


   this->	TraceHeartbeat = false;
   this->	TraceSwipePunch = false;
   this->	TraceGetActivities = false;
   this->	TraceLocalTime = false;
   this->	TraceIsValidEmployee = false;
   this->	TraceSetFPTemplate = false;
   this->	TraceGetFPTemplate = false;
   this->	TracePopulateFPTemplates = false;
   this->	TraceTemplatesAvailable = false;
   this->	TraceEmployeeUsesPin = false;
   this->	TraceValidEmployeePin = false;
   this->	TraceGetPinTable = false;
   this->	VerifySSL = false;


// ref
// method 1
//	XMLProperties* xmlParser = new XMLProperties("properties.xml");
//	char propTraceHeartbeat[8];
//	padstring(propTraceHeartbeat, NULL, 7);
//	propTraceHeartbeat[7] = '\0';
//	printf("before GetProp for TraceHeartBeat, padded value is [%s]\n", propTraceHeartbeat);
//	xmlParser->GetProperty("TraceHeartbeat", propTraceHeartbeat);
//	delete xmlParser; // release it immediately
//	if(strlen(propTraceHeartbeat))
//		printf("heartbeat trace value is %s\n", propTraceHeartbeat);
//	else
//		printf("could not get traceheartbeat value\n");
//
//	char propTrace[8];
//	XMLProperties* xmlParser = new XMLProperties("properties.xml");
// endref


	propTrace[0] = '\0';
	xmlParser.GetProperty("TraceHeartbeat", propTrace);
	if(strlen(propTrace))
	{
		//printf("TraceHeartbeat value is %s\n", propTrace);
		if ( strcmp(propTrace,"1") == 0 )
		{
			TraceHeartbeat = true;
		}
		else
		{
			TraceHeartbeat = false;
		}
	}
	else
	{
		//printf("TraceHeartbeat value is undefined. Default to false\n");
		TraceHeartbeat = false;
	}

	propTrace[0] = '\0';
	xmlParser.GetProperty("TraceSwipePunch", propTrace);
	if(strlen(propTrace))
	{
		//printf("TraceSwipePunch value is %s\n", propTrace);
		if ( strcmp(propTrace,"1") == 0 )
		{
			TraceSwipePunch = true;
		}
		else
		{
			TraceSwipePunch = false;
		}
	}
	else
	{
		//printf("TraceSwipePunch value is undefined. Default to false\n");
		TraceSwipePunch = false;
	}

	propTrace[0] = '\0';
	xmlParser.GetProperty("TraceGetActivities", propTrace);
	if(strlen(propTrace))
	{
		//printf("TraceGetActivities value is %s\n", propTrace);
		if ( strcmp(propTrace,"1") == 0 )
		{
			TraceGetActivities = true;
		}
		else
		{
			TraceGetActivities = false;
		}
	}
	else
	{
		//printf("TraceGetActivities value is undefined. Default to false\n");
		TraceGetActivities = false;
	}

	propTrace[0] = '\0';
	xmlParser.GetProperty("TraceLocalTime", propTrace);
	if(strlen(propTrace))
	{
		//printf("TraceLocalTime value is %s\n", propTrace);
		if ( strcmp(propTrace,"1") == 0 )
		{
			TraceLocalTime = true;
		}
		else
		{
			TraceLocalTime = false;
		}
	}
	else
	{
		//printf("TraceLocalTime value is undefined. Default to false\n");
		TraceLocalTime = false;
	}

	propTrace[0] = '\0';
	xmlParser.GetProperty("TraceIsValidEmployee", propTrace);
	if(strlen(propTrace))
	{
		//printf("TraceIsValidEmployee value is %s\n", propTrace);
		if ( strcmp(propTrace,"1") == 0 )
		{
			TraceIsValidEmployee = true;
		}
		else
		{
			TraceIsValidEmployee = false;
		}
	}
	else
	{
		//printf("TraceIsValidEmployee value is undefined. Default to false\n");
		TraceIsValidEmployee = false;
	}

	propTrace[0] = '\0';
	xmlParser.GetProperty("TraceSetFPTemplate", propTrace);
	if(strlen(propTrace))
	{
		//printf("TraceSetFPTemplate value is %s\n", propTrace);
		if ( strcmp(propTrace,"1") == 0 )
		{
			TraceSetFPTemplate = true;
		}
		else
		{
			TraceSetFPTemplate = false;
		}
	}
	else
	{
		//printf("TraceSetFPTemplate value is undefined. Default to false\n");
		TraceSetFPTemplate = false;
	}

	propTrace[0] = '\0';
	xmlParser.GetProperty("TraceGetFPTemplate", propTrace);
	if(strlen(propTrace))
	{
		//printf("TraceGetFPTemplate value is %s\n", propTrace);
		if ( strcmp(propTrace,"1") == 0 )
		{
			TraceGetFPTemplate = true;
		}
		else
		{
			TraceGetFPTemplate = false;
		}
	}
	else
	{
		//printf("TraceGetFPTemplate value is undefined. Default to false\n");
		TraceGetFPTemplate = false;
	}

	propTrace[0] = '\0';
	xmlParser.GetProperty("TracePopulateFPTemplates", propTrace);
	if(strlen(propTrace))
	{
		//printf("TracePopulateFPTemplates value is %s\n", propTrace);
		if ( strcmp(propTrace,"1") == 0 )
		{
			TracePopulateFPTemplates = true;
		}
		else
		{
			TracePopulateFPTemplates = false;
		}
	}
	else
	{
		//printf("TracePopulateFPTemplates value is undefined. Default to false\n");
		TracePopulateFPTemplates = false;
	}

	propTrace[0] = '\0';
	xmlParser.GetProperty("TraceTemplatesAvailable", propTrace);
	if(strlen(propTrace))
	{
		//printf("TraceTemplatesAvailable value is %s\n", propTrace);
		if ( strcmp(propTrace,"1") == 0 )
		{
			TraceTemplatesAvailable = true;
		}
		else
		{
			TraceTemplatesAvailable = false;
		}
	}
	else
	{
		//printf("TraceTemplatesAvailable value is undefined. Default to false\n");
		TraceTemplatesAvailable = false;
	}


	propTrace[0] = '\0';
	xmlParser.GetProperty("TraceGetPinTable", propTrace);
	if(strlen(propTrace))
	{
		//printf("TraceGetPinTable value is %s\n", propTrace);
		if ( strcmp(propTrace,"1") == 0 )
		{
			TraceGetPinTable = true;
		}
		else
		{
			TraceGetPinTable = false;
		}
	}
	else
	{
		//printf("TraceGetPinTable value is undefined. Default to false\n");
		TraceGetPinTable = false;
	}


	// move this to EOFunc delete xmlParser; // release it immediately


///

  curl = curl_easy_init();
  if(curl)
  {

	if(this->useProxy)
	{
		char proxylink[300];
		strcpy(proxylink,this->proxy);strcat(proxylink,":");strcat(proxylink,this->proxyPort);
		curl_easy_setopt(curl, CURLOPT_PROXY,proxylink);
        printf("proxy - > %s\n",proxylink);
	}
	if(this->useProxyAuthentication)
	{
	   char proxyUsrPwd[100];
	   strcpy(proxyUsrPwd,this->proxyUserName);strcat(proxyUsrPwd,":");strcat(proxyUsrPwd,this->proxyPassword);
       curl_easy_setopt(curl,CURLOPT_PROXYUSERPWD, proxyUsrPwd);
	   printf("proxyUsrPwd = %s \n",proxyUsrPwd);
	}

	curl_easy_setopt(curl, CURLOPT_HTTPPROXYTUNNEL, true);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
	curl_easy_setopt (curl, CURLOPT_POST, 1);
	curl_easy_setopt(curl, CURLOPT_URL,this->URL);

    // curl will try to determine this itself , although in some cases this must be set manually - this for new prologic servers
    curl_easy_setopt(curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_SSLv3);

	// reuse above declaration: XMLProperties* xmlParser = new XMLProperties("properties.xml");

#ifdef SUPPORT_SSL

//	//char propVerifySSL[8];
//	propVerifySSL[0] = '\0';

//	xmlParser->GetProperty("VerifySSL", propVerifySSL);

//	if(	strcasecmp("true", propVerifySSL )==0 ||
//		strcmp( "1",   propVerifySSL )==0
//	  )
//	{

//DP("ctor httpclient: verifyssl == true\n");

//    	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

//		//char propSSLCertFile[128];
//		propSSLCertFile[0] = '\0';

//		xmlParser->GetProperty("SSLCertFile", propSSLCertFile);
//		if ( strlen(propSSLCertFile) == 0 )
//		  {
//		    // do nothing. use defaults
//		  }
//		else
//		  {
//		    curl_easy_setopt(curl, CURLOPT_CAINFO, propSSLCertFile);
//		  }
//	}
//	else
//	{

//DP("ctor httpclient: verifyssl == false\n");

//		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
//	}

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
		//curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

#else

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
		//curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);

#endif

    curl_easy_setopt(curl,  CURLOPT_POSTFIELDS, smg->xml);
	curl_easy_setopt(curl,CURLOPT_TIMEOUT,this->timeOut);
	curl_easy_setopt(curl,CURLOPT_CONNECTTIMEOUT,this->timeOut);
    curl_easy_setopt(curl,CURLOPT_NOSIGNAL,false);



    //config.trace_ascii = 1; /* enable ascii tracing */
    //curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, my_trace);
    //curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &config);
    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

	count = 0;
	/*if(this->tunnel)
		curl_easy_setopt(curl, CURLOPT_HTTPPROXYTUNNEL, true); */
  }


}

HttpClient::HttpClient()
{
  this->useSoapActionHeader = false;
  this->soapActionHeader = NULL;
  count = 0;
}
void HttpClient::resetHttp()
{
  //DLOGN("%s %d", __FUNCTION__,__LINE__);
  if(curl){
	printf("HttpClient: curl cleanup\n");
	curl_easy_cleanup(curl);
	curl = NULL;
  }
  if(chunk.memory)
  {
    free(chunk.memory);
    chunk.memory = NULL;
  }
  curl = curl_easy_init();
  if(curl)
  {
    //printf("init new curl\n");
	if(this->useProxy)
	{
		char proxylink[300];
		strcpy(proxylink,this->proxy);strcat(proxylink,":");strcat(proxylink,this->proxyPort);
		curl_easy_setopt(curl, CURLOPT_PROXY,proxylink);
        //printf("proxy - > %s\n",proxylink);
	}
	if(this->useProxyAuthentication)
	{
	   char proxyUsrPwd[100];
	   strcpy(proxyUsrPwd,this->proxyUserName);strcat(proxyUsrPwd,":");strcat(proxyUsrPwd,this->proxyPassword);
       curl_easy_setopt(curl,CURLOPT_PROXYUSERPWD, proxyUsrPwd);
	   //printf("proxyUsrPwd = %s \n",proxyUsrPwd);
	}

	curl_easy_setopt(curl, CURLOPT_HTTPPROXYTUNNEL, true);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
	curl_easy_setopt (curl, CURLOPT_POST, 1);
	curl_easy_setopt(curl, CURLOPT_URL,this->URL);

	XMLProperties xmlParser("properties.xml");

#ifdef SUPPORT_SSL

	char propVerifySSL[8];
	propVerifySSL[0] = '\0';

	xmlParser.GetProperty("VerifySSL", propVerifySSL);

	if(	strcasecmp("true", propVerifySSL )==0 ||
		strcmp( "1",   propVerifySSL )==0
	  )
	{

    	curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1);

		char propSSLCertFile[128];
		propSSLCertFile[0] = '\0';

		xmlParser.GetProperty("SSLCertFile", propSSLCertFile);
		if ( strlen(propSSLCertFile) == 0 )
		  {
		    // do nothing. use defaults
		  }
		else
		  {
		    curl_easy_setopt(curl, CURLOPT_CAINFO, propSSLCertFile);
		  }
	}
	else
	{
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
	}

#else

		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);

#endif


	// curl will try to determine this itself , although in some cases this must be set manually - this for new prologic servers
	curl_easy_setopt(curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_SSLv3);

    curl_easy_setopt(curl,  CURLOPT_POSTFIELDS, smg->xml);
	curl_easy_setopt(curl,CURLOPT_TIMEOUT,this->timeOut);
	curl_easy_setopt(curl,CURLOPT_CONNECTTIMEOUT,this->timeOut);
    curl_easy_setopt(curl,CURLOPT_NOSIGNAL,false);

    //config.trace_ascii = 1; /* enable ascii tracing */
    //curl_easy_setopt(curl, CURLOPT_DEBUGFUNCTION, my_trace);
    //curl_easy_setopt(curl, CURLOPT_DEBUGDATA, &config);
    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

	count = 0;
  }
  count = 0;
}

HttpClient::~HttpClient()
{
 if(chunk.memory)
    free(chunk.memory);
 chunk.memory = NULL;
 if(curl){
	 printf("curl cleanup\n");
	 curl_easy_cleanup(curl);
	 curl = NULL;
 }
 if(proxy)free(proxy);
 if(proxyPort)free(proxyPort);
 if(proxyUserName)free(proxyUserName);
 if(proxyPassword)free(proxyPassword);
 if(soapActionHeader)free(soapActionHeader);
 delete smg;
}

void HttpClient::setURL(char* U)
{
  this->URL[0] = '\0';
  strcpy(this->URL,U);
}

void HttpClient::ResetMemory()
{
  if(chunk.memory)
     free(chunk.memory);
  chunk.memory = NULL;
  chunk.size = 0;
}

char* HttpClient::ClockIn(char* ClockId ,char* Account,char* empId,char* utcTime,char* projectSwitch,char* group,char*                            project,char* task,char* activity,char* soapVer , CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];

char * soapdata;

  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/ClockIn\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sClockIn\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }

	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

  smg->ClockIn(ClockId ,Account,empId,utcTime,projectSwitch,group,project,task,activity,soapVer);
//testing
//printf("ClockIn Request\n");
//printf("%s\n",soapdata);
//endtest
  Request(soapVer,headers,res);
//testing    Response is returned in chunk.memory
//printf("\n\n\n");
//printf("ClockIn Response\n");
	if ( *res == CURLE_OK)
	{
		printf("%s\n",chunk.memory);
	}
	else printf("response code is %d", *res);

//endtest
  curl_slist_free_all(headers);
  return chunk.memory;
}

//char* HttpClient::InPunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* group ,char* soapVer , CURLcode* res)
//{
//  struct curl_slist *headers=NULL;
//  char soapAction[200];
////testing
////char * soapdata;
////endtest
//  if(useSoapActionHeader)
//  {
//	  int len = strlen(soapActionHeader);
//	  if(soapActionHeader[len-1] != '/')
//		sprintf(soapAction,"SOAPAction: \"%s/InPunch\"",soapActionHeader);
//	  else
//        sprintf(soapAction,"SOAPAction: \"%sInPunch\"",soapActionHeader);
//	  headers = curl_slist_append(headers,soapAction);
//  }
////testing
////	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,
////endtest
//  smg->InPunch(ClockId ,Account,empId,utcTime,group ,soapVer);
////testing
////printf("InPunch Request\n");
////printf("%s\n",soapdata);
////endtest
//  Request(soapVer,headers,res);
////testing    Response is returned in chunk.memory
////printf("\n\n\n");
////printf("InPunch Response\n");
////	if ( *res == CURLE_OK)
////	{
////		printf("%s\n",chunk.memory);
////	}
////	else printf("response code is %d", *res);//
////endtest
//  curl_slist_free_all(headers);
//  return chunk.memory;
//}

//char* HttpClient::OutPunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* soapVer,CURLcode* res)
//{
//  struct curl_slist *headers=NULL;
//  char soapAction[200];
//  if(useSoapActionHeader)
//  {
//	  int len = strlen(soapActionHeader);
//	  if(soapActionHeader[len-1] != '/')
//		sprintf(soapAction,"SOAPAction: \"%s/OutPunch\"",soapActionHeader);
//	  else
//        sprintf(soapAction,"SOAPAction: \"%sOutPunch\"",soapActionHeader);
//	  headers = curl_slist_append(headers,soapAction);
//  }
//  smg->OutPunch(ClockId ,Account,empId,utcTime,soapVer);
//  Request(soapVer,headers,res);
//  curl_slist_free_all(headers);
//  return chunk.memory;
//}

char* HttpClient::HeartBeat(char* ClockId ,char* Account ,char* utcTime , char* soapVer,CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];
//testing
char * soapdata;
//endtest
  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/Heartbeat\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sHeartbeat\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }

	soapdata = smg->Heartbeat(ClockId ,Account,utcTime,soapVer);// to see the request-response, get a pointer to the smg->xml member,


	if ( TraceHeartbeat )
	{
		printf("Heartbeat Request\n");
		printf("%s\n",soapdata);
	}

  Request(soapVer,headers,res);


#ifdef USE_MCHECK
//DP("mcheck_check_all after Heartbeat Request\n");
mcheck_check_all();
//DP("mprobe - check chunkMemory from Heartbeat Request\n");
//mprobe(chunk.memory);
#endif

	if ( TraceHeartbeat )
	{
		printf("\n\n");
		printf("Heartbeat Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}



  curl_slist_free_all(headers);
  return chunk.memory;
}

char* HttpClient::HeartBeatFile(char* ClockId ,char* Account ,char* utcTime,char* soapVer , CURLcode* res,FILE *fOut)
{
	struct curl_slist *headers=NULL;
	char soapAction[200];
	//testing
	char * soapdata;
	//endtest
	if(useSoapActionHeader)
	{
	     int len = strlen(soapActionHeader);
		 if(soapActionHeader[len-1] != '/')
		  sprintf(soapAction,"SOAPAction: \"%s/Heartbeat\"",soapActionHeader);
		 else
		  sprintf(soapAction,"SOAPAction: \"%sHeartbeat\"",soapActionHeader);
		 headers = curl_slist_append(headers,soapAction);
	 }

     // to see the request-response, get a pointer to the smg->xml member,
	 //DLOGN("%s %d", __FUNCTION__,__LINE__);
	 soapdata = smg->Heartbeat(ClockId ,Account,utcTime,soapVer);// to see the request-response, get a pointer to the smg->xml member,
	 //DLOGN("%s %d", __FUNCTION__,__LINE__);

	if ( TraceGetActivities )
	{
		printf("Heartbeat Request\n");
		printf("%s\n",soapdata);
	}

	HTTPResultFile httpR;
	httpR.httpMem[0] = 0;
	httpR.httpBuf[0] = 0;
	httpR.httpSize = 0;
	httpR.fOut = fOut;
	//int ret = -1;
	//curl_easy_setopt(curl, CURLOPT_TIMEOUT, tout);//180second timeout because large transfer
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CallbackFile);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&httpR);

	//DLOGN("%s %d", __FUNCTION__,__LINE__);
	Request(soapVer,headers,res);
	//DLOGN("%s %d", __FUNCTION__,__LINE__);

	// reset options back to memory-based
	//curl_easy_setopt(curl,CURLOPT_TIMEOUT,this->timeOut);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

	#ifdef USE_MCHECK
	//DP("mcheck_check_all after GetActivities Request\n");
	mcheck_check_all();
	//DP("mprobe - check chunkMemory from GetActivities Request\n");
	//mprobe(chunk.memory);
	#endif

	if ( TraceGetActivities )
	{
		printf("\n\n");
		printf("Heartbeat File Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}
	curl_slist_free_all(headers);
	return NULL;
}

char* HttpClient::SwipePunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* soapVer , CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];

char * soapdata;

  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/SwipePunch\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sSwipePunch\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }

	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

  smg->SwipePunch(ClockId ,Account,empId,utcTime,soapVer);

	if ( TraceSwipePunch )
	{
		printf("SwipePunch Request\n");
		printf("%s\n",soapdata);
	}


  Request(soapVer,headers,res);


#ifdef USE_MCHECK
//DP("mcheck_check_all after SwipePunch Request\n");
mcheck_check_all();
//DP("mprobe - check chunkMemory from SwipePunch Request\n");
//mprobe(chunk.memory);
#endif

	if ( TraceSwipePunch )
	{
		printf("\n\n");
		printf("SwipePunch Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}

  curl_slist_free_all(headers);
  //printf("%s\n",chunk.memory);
  return chunk.memory;
}

//char* HttpClient::GetProjectTasks(char* ClockId ,char* Account , char* soapVer , CURLcode* res)
//{
//  struct curl_slist *headers=NULL;
//  char soapAction[200];
//  if(useSoapActionHeader)
//  {
//	  int len = strlen(soapActionHeader);
//	  if(soapActionHeader[len-1] != '/')
//		sprintf(soapAction,"SOAPAction: \"%s/GetProjectTasks\"",soapActionHeader);
//	  else
//        sprintf(soapAction,"SOAPAction: \"%sGetProjectTasks\"",soapActionHeader);
//	  headers = curl_slist_append(headers,soapAction);
//  }
//  smg->GetProjectTasks(ClockId ,Account,soapVer);
//  Request(soapVer,headers,res);
//  curl_slist_free_all(headers);
//  return chunk.memory;
//}

char* HttpClient::LocalTime(char* ClockId ,char* Account,char* soapVer,CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];
//testing
char * soapdata;
//endtest
  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/LocalTime\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sLocalTime\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }
	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

  smg->LocalTime(ClockId ,Account,soapVer);

	if ( TraceLocalTime )
	{
		printf("LocalTime Request\n");
		printf("%s\n",soapdata);
	}


  Request(soapVer,headers,res);


#ifdef USE_MCHECK
//DP("mcheck_check_all after LocalTime Request\n");
mcheck_check_all();
//DP("mprobe - check chunkMemory from LocalTime Request\n");
//mprobe(chunk.memory);
#endif

	if ( TraceLocalTime )
	{
		printf("\n\n");
		printf("LocalTime Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}
  curl_slist_free_all(headers);
  return chunk.memory;
}

char* HttpClient::GetActivities(char* ClockId ,char* Account , char* soapVer , CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];
//testing
char * soapdata;
//endtest
  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/GetActivities\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sGetActivities\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }
	 // to see the request-response, get a pointer to the smg->xml member,
	soapdata = smg->GetActivities(ClockId ,Account,soapVer);

	if ( TraceGetActivities )
	{
		printf("GetActivities Request\n");
		printf("%s\n",soapdata);
	}

   //printf("%s %d\n",__FUNCTION__,__LINE__);
   Request(soapVer,headers,res);
   //printf("%s %d\n",__FUNCTION__,__LINE__);


#ifdef USE_MCHECK
//DP("mcheck_check_all after GetActivities Request\n");
mcheck_check_all();
//DP("mprobe - check chunkMemory from GetActivities Request\n");
//mprobe(chunk.memory);
#endif

	if ( TraceGetActivities )
	{
		printf("\n\n");
		printf("GetActivities Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}
  curl_slist_free_all(headers);
  return chunk.memory;
}

/////////////////////////////////////////////////////////////////////////////////

char* HttpClient::GetPinTableFromFile(char* ClockId ,char* Account , char* soapVer , CURLcode* res, FILE *fOut)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];

char * soapdata;

  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/GetPinTable\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sGetPinTable\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }
	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

 smg->GetPinTable(ClockId ,Account,soapVer);

	if ( TraceGetPinTable )
	{
		printf("GetPinTable Request\n");
		printf("%s\n",soapdata);
	}


HTTPResultFile httpR ;
httpR.httpMem[0] = 0;
httpR.httpBuf[0] = 0;
httpR.httpSize = 0;
httpR.fOut = fOut;
curl_easy_setopt(curl, CURLOPT_TIMEOUT, 180);//180second timeout because large transfer
curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CallbackFile);
curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&httpR);

  Request(soapVer,headers,res);

// reset options back to memory-based
curl_easy_setopt(curl,CURLOPT_TIMEOUT,this->timeOut);
curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);


// no trace on writefile callbacks...
	if ( TraceGetPinTable )
	{
		printf("\n\n");
		printf("GetPinTable Response\n");
		if ( *res == CURLE_OK)
		{
			//printf("%s\n",chunk.memory);
			printf(" is Ok.\n");
		}
		else
		{
			 printf("response code is %d", *res);
			 printf("%s\n\n", chunk.memory);
		}
	}
  curl_slist_free_all(headers);
  //return chunk.memory;
  return NULL;
}

/////////////////////////////////////////////////////////////////////////////////



char* HttpClient::SwipePunchWithActivityCode(char* ClockId ,char* Account,char* empId,char* activity,char* utcTime,char* soapVer , CURLcode* res)
{
	//
	////// note : operation name is ClockIn
	//

  struct curl_slist *headers=NULL;
  char soapAction[200];
//testing
char * soapdata;
//endtest
  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/ClockIn\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sClockIn\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }
	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

  smg->SwipePunchWithActivityCode(ClockId ,Account,empId,activity,utcTime,soapVer);
//testing
//printf("SwipePunchWithActivityCode Request\n");
//printf("%s\n",soapdata);
//endtest
  Request(soapVer,headers,res);
//testing    Response is returned in chunk.memory
//printf("\n\n\n");
//printf("SwipePunchWithActivityCode Response\n");
	/*if ( *res == CURLE_OK)
	{
		printf("%s\n",chunk.memory);
	}
	else printf("response code is %d", *res);*/
//endtest

  curl_slist_free_all(headers);
  return chunk.memory;
}

char* HttpClient::IsValidEmployee(char* ClockId ,char* Account,char* empId, char * utcTime, char* soapVer,CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];

char * soapdata;

  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/IsValidEmployee\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sIsValidEmployee\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }


	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

  smg->IsValidEmployee(ClockId ,Account,empId,utcTime, soapVer);


	if ( TraceIsValidEmployee )
	{
		printf("IsValidEmployee Request\n");
		printf("%s\n",soapdata);
	}


  Request(soapVer,headers,res);


#ifdef USE_MCHECK
//DP("mcheck_check_all after IsValidEmployee Request\n");
mcheck_check_all();
//DP("mprobe - check chunkMemory from IsValidEmployee Request\n");
//mprobe(chunk.memory);
#endif

	if ( TraceIsValidEmployee )
	{
		printf("\n\n");
		printf("IsValidEmployee Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}

  curl_slist_free_all(headers);
  return chunk.memory;

}

//
// fpm
//
char* HttpClient::SetFPTemplate(char* ClockId ,char* Account,char* empId, char* utcTime, char* templateId, char* fptemplate, char* soapVer,CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];
//testing
char * soapdata;
//endtest
  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/SetFPTemplate\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sSetFPTemplate\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }
//testing
	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,
//endtest
  smg->SetFPTemplate(ClockId ,Account,empId,utcTime, templateId, fptemplate, soapVer);

	if ( TraceSetFPTemplate )
	{
		printf("SetFPTemplate Request\n");
		printf("%s\n",soapdata);
	}


  Request(soapVer,headers,res);


#ifdef USE_MCHECK
//DP("mcheck_check_all after SetFPTemplate Request\n");
mcheck_check_all();
//DP("mprobe - check chunkMemory from SetFPTemplate Request\n");
//mprobe(chunk.memory);
#endif

	if ( TraceSetFPTemplate )
	{
		printf("\n\n");
		printf("SetFPTemplate Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}

  curl_slist_free_all(headers);
  return chunk.memory;

}

char* HttpClient::GetFPTemplate(char* ClockId ,char* Account,char* empId, char* utcTime, char* soapVer,CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];
//testing
char * soapdata;
//endtest
  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/GetFPTemplate\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sGetFPTemplate\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }
//testing
	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,
//endtest
  smg->GetFPTemplate(ClockId ,Account,empId,utcTime, soapVer);

	if ( TraceGetFPTemplate )
	{
		printf("GetFPTemplate Request\n");
		printf("%s\n",soapdata);
	}


  Request(soapVer,headers,res);


#ifdef USE_MCHECK
//DP("mcheck_check_all after GetFPTemplate Request\n");
mcheck_check_all();
//DP("mprobe - check chunkMemory from GetFPTemplate Request\n");
//mprobe(chunk.memory);
#endif

	if ( TraceGetFPTemplate )
	{
		printf("\n\n");
		printf("GetFPTemplate Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}

  curl_slist_free_all(headers);
  return chunk.memory;

}



char* HttpClient::PopulateFPTemplates(char* ClockId ,char* Account, char* soapVer,CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];
//testing
char * soapdata;
//endtest
  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/PopulateFPTemplates\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sPopulateFPTemplates\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }

	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

  smg->PopulateFPTemplates(ClockId, Account, soapVer);

	if ( TracePopulateFPTemplates )
	{
		printf("PopulateFPTemplates Request\n");
		printf("%s\n",soapdata);
	}

	curl_easy_setopt(curl,CURLOPT_TIMEOUT,this->timeOut);

curl_easy_setopt(curl,CURLOPT_TIMEOUT,180); // two minute timeout?

  Request(soapVer,headers,res);

curl_easy_setopt(curl,CURLOPT_TIMEOUT,this->timeOut);


#ifdef USE_MCHECK
//DP("mcheck_check_all after PopulateFPTemplates Request\n");
mcheck_check_all();
//DP("mprobe - check chunkMemory from PopulateFPTemplates Request\n");
//mprobe(chunk.memory);
#endif

	if ( TracePopulateFPTemplates )
	{
		printf("\n\n");
		printf("PopulateFPTemplates Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}

  curl_slist_free_all(headers);
  return chunk.memory;

}

char* HttpClient::GetActivitiesFile(char* ClockId ,char* Account , char* soapVer , CURLcode* res,FILE *fOut,int tout)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];
//testing
char * soapdata;
//endtest
  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/GetActivities\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sGetActivities\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }
	 // to see the request-response, get a pointer to the smg->xml member,
	soapdata = smg->GetActivities(ClockId ,Account,soapVer);

	if ( TraceGetActivities )
	{
		printf("GetActivities Request\n");
		printf("%s\n",soapdata);
	}

   HTTPResultFile httpR;
   httpR.httpMem[0] = 0;
   httpR.httpBuf[0] = 0;
   httpR.httpSize = 0;
   httpR.fOut = fOut;
	//int ret = -1;
   curl_easy_setopt(curl, CURLOPT_TIMEOUT, tout);//180second timeout because large transfer
   curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CallbackFile);
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&httpR);

   printf("%s %d\n",__FUNCTION__,__LINE__);
   Request(soapVer,headers,res);
   printf("%s %d\n",__FUNCTION__,__LINE__);

   // reset options back to memory-based
   curl_easy_setopt(curl,CURLOPT_TIMEOUT,this->timeOut);
   curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

#ifdef USE_MCHECK
//DP("mcheck_check_all after GetActivities Request\n");
mcheck_check_all();
//DP("mprobe - check chunkMemory from GetActivities Request\n");
//mprobe(chunk.memory);
#endif

	if ( TraceGetActivities )
	{
		printf("\n\n");
		printf("GetActivities File Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}
  curl_slist_free_all(headers);
  return NULL;
}


char* HttpClient::PopulateFPTemplatesFile(char* ClockId ,char* Account, char* soapVer,CURLcode* res, FILE *fOut,int tout)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];
//testing
char * soapdata;
//endtest
  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/PopulateFPTemplates\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sPopulateFPTemplates\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }

	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

  smg->PopulateFPTemplates(ClockId, Account, soapVer);

// no trace available on file-based write callback
//	if ( TracePopulateFPTemplates )
//	{
//		printf("PopulateFPTemplates Request\n");
//		printf("%s\n",soapdata);
//	}

HTTPResultFile httpR;
httpR.httpMem[0] = 0;
httpR.httpBuf[0] = 0;
httpR.httpSize = 0;
httpR.fOut = fOut;
//int ret = -1;
curl_easy_setopt(curl, CURLOPT_TIMEOUT, tout);//180second timeout because large transfer
curl_easy_setopt(curl,CURLOPT_CONNECTTIMEOUT,60);
curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CallbackFile);
curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&httpR);

  Request(soapVer,headers,res);

// reset options back to memory-based
curl_easy_setopt(curl,CURLOPT_TIMEOUT,this->timeOut);
curl_easy_setopt(curl,CURLOPT_CONNECTTIMEOUT,this->timeOut);
curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);


// no trace available on file-based write callback
//#ifdef USE_MCHECK
////DP("mcheck_check_all after PopulateFPTemplates Request\n");
//mcheck_check_all();
////DP("mprobe - check chunkMemory from PopulateFPTemplates Request\n");
////mprobe(chunk.memory);
//#endif

// no trace available on file-based write callback
//	if ( TracePopulateFPTemplates )
//	{
//		printf("\n\n");
//		printf("PopulateFPTemplates Response\n");
//		if ( *res == CURLE_OK)
//		{
//			printf("%s\n",chunk.memory);
//		}
//		else printf("response code is %d", *res);
//	}
  curl_slist_free_all(headers);
  return NULL;

}


char* HttpClient::TemplatesAvailable(char* ClockId ,char* Account, char* utcTime,char* soapVer,CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];
//testing
char * soapdata;
//endtest
  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/TemplatesAvailable\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sTemplatesAvailable\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }

	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

  smg->TemplatesAvailable(ClockId ,Account, utcTime, soapVer);

	if ( TraceTemplatesAvailable )
	{
		printf("TemplatesAvailable Request\n");
		printf("%s\n",soapdata);
	}


  Request(soapVer,headers,res);


#ifdef USE_MCHECK
//DP("mcheck_check_all after TemplatesAvailable Request\n");
mcheck_check_all();
//DP("mprobe - check chunkMemory from TemplatesAvailable Request\n");
//mprobe(chunk.memory);
#endif

	if ( TraceTemplatesAvailable )
	{
		printf("\n\n");
		printf("TemplatesAvailable Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}

  curl_slist_free_all(headers);
  return chunk.memory;

}

//#endif

char* HttpClient::EmployeeUsesPin(char* ClockId ,char* Account,char* empId, char * utcTime, char* soapVer,CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];

char * soapdata;

  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/EmployeeUsesPin\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sEmployeeUses\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }


	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

  smg->EmployeeUsesPin(ClockId ,Account,empId,utcTime, soapVer);


	if ( TraceEmployeeUsesPin )
	{
		printf("EmployeeUsesPin Request\n");
		printf("%s\n",soapdata);
	}


  Request(soapVer,headers,res);


	if ( TraceEmployeeUsesPin )
	{
		printf("\n\n");
		printf("EmployeeUsesPin Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}

  curl_slist_free_all(headers);
  return chunk.memory;

}

char* HttpClient::ValidEmployeePin(char* ClockId ,char* Account,char* empId, char* empPin, char * utcTime, char* soapVer,CURLcode* res)
{
  struct curl_slist *headers=NULL;
  char soapAction[200];

char * soapdata;

  if(useSoapActionHeader)
  {
	  int len = strlen(soapActionHeader);
	  if(soapActionHeader[len-1] != '/')
		sprintf(soapAction,"SOAPAction: \"%s/ValidEmployeePin\"",soapActionHeader);
	  else
        sprintf(soapAction,"SOAPAction: \"%sValidEmployeePin\"",soapActionHeader);
	  headers = curl_slist_append(headers,soapAction);
  }


	soapdata = 		// to see the request-response, get a pointer to the smg->xml member,

  smg->ValidEmployeePin(ClockId ,Account,empId,empPin,utcTime, soapVer);


	if ( TraceValidEmployeePin )
	{
		printf("ValidEmployeePin Request\n");
		printf("%s\n",soapdata);
	}


  Request(soapVer,headers,res);


	if ( TraceValidEmployeePin )
	{
		printf("\n\n");
		printf("ValidEmployeePin Response\n");
		if ( *res == CURLE_OK)
		{
			printf("%s\n",chunk.memory);
		}
		else printf("response code is %d", *res);
	}

  curl_slist_free_all(headers);
  return chunk.memory;

}


/////////////////////////////////////////////////////////////////////////////////


void HttpClient::Request(char* soapVer,struct curl_slist *headers,CURLcode* res)
{
  //printf("%s %d start\n",__FUNCTION__,__LINE__);
	count++;
	//DLOGN("%s %d", __FUNCTION__,__LINE__);
  ResetMemory();
  //DLOGN("%s %d", __FUNCTION__,__LINE__);

  if(strcmp(soapVer,SOAP_1_1_VER)==0)
	headers = curl_slist_append(headers,"Content-Type: text/xml; charset=utf-8");
  if(strcmp(soapVer,SOAP_1_2_VER)==0)
	headers = curl_slist_append(headers,"Content-Type: application/soap+xml; charset=utf-8");
  if(curl)
  {
	 curl_easy_setopt (curl, CURLOPT_POSTFIELDSIZE, strlen (smg->xml));
     curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
  }
  //printf("befor curl_easy_perform res = %d curl=%d\n",*res,curl);
  //DLOGN("%s %d", __FUNCTION__,__LINE__);
  *res = curl_easy_perform(curl);
  //DLOGN("%s %d", __FUNCTION__,__LINE__);
  //printf("%s %d end\n",__FUNCTION__,__LINE__);
  //printf("res = %d\n",*res);
  /* always cleanup */
}

/*int
main (int argc, char * argv [])
{
  HttpClient http("https://www.schedulesource.net/Comair/teamwork/Services/V200/clock.asmx");
  //while(1){
  http.InPunch("UnitTest","859-767-2072","1491315","2007-12-13T00:59:36.0204412Z","0",SOAP_1_1_VER);
  printf("**********************************************\n");
  printf("%s\n",http.chunk.memory);
  http.OutPunch("UnitTest","859-767-2072","1491315","2007-12-13T00:59:36.0204412Z",SOAP_1_1_VER);
  printf("//////////////////////////////////////////////\n");
  printf("%s\n",http.chunk.memory);
  http.Heartbeat("UnitTest","859-767-2072","2007-12-13T00:59:36.0204412Z",SOAP_1_1_VER);
  printf("11111111111111111111111111111111111++++++++++++++++++++++\n");
  printf("%s\n",http.chunk.memory);
  //}
  return 0 ;
}*/

//eof
