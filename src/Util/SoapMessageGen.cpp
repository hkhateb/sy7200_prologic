#include "SoapMessageGen.h"

using namespace std;

SoapMessageGen::SoapMessageGen(bool useSAH , char*  SAH)
{
  this->useSoapActionHeader = useSAH;
  if(useSAH && strcmp(SAH,"NULL"))
  {
	  int len = strlen ( SAH );
	  this->soapActionHeader = (char*) malloc(len+1);
      strcpy(this->soapActionHeader,SAH);
  }
  else this->soapActionHeader = NULL;
}

SoapMessageGen::~SoapMessageGen()
{
 if(soapActionHeader)free(soapActionHeader);
}
char* SoapMessageGen::ClockIn(char* ClockId ,char* Account,char* empId,char* utcTime,char* projectSwitch,char* group,                                 char* project,char* task,char* activity,char* soapVer)
{
  xml[0] = '\0';
  char buf1[200];
  char buf2[200];
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
    strcpy(xml,SOAP_1_1_START);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
	 strcpy(xml,SOAP_1_2_START);
  char* startClockId = buf1;
  strcpy(startClockId,"  <ClockCredentials xmlns=\"") ;
  if(useSoapActionHeader)strcat(startClockId,soapActionHeader);
  else strcat(startClockId,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startClockId,"\">\n   <ClockId>");
  strcat(xml,startClockId);
  strcat(xml,ClockId);
  char* endClockId =  "</ClockId>\n";
  strcat(xml,endClockId);
  char* startAccount = "   <Account>";
  strcat(xml,startAccount);
  strcat(xml,Account);

  char* endAccount1 =  "</Account>\n  </ClockCredentials>\n </soap:Header>\n";
  char* endAccount2 =  "</Account>\n  </ClockCredentials>\n </soap12:Header>\n";
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,endAccount1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,endAccount2);

  char* startempId1 = buf1;
  char* startempId2 = buf2;
  strcpy(startempId1," <soap:Body>\n  <ClockIn xmlns=\"");
  strcpy(startempId2," <soap12:Body>\n  <ClockIn xmlns=\"");

  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId1,"\">\n   <empId>");

  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId2,"\">\n   <empId>");

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,startempId1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,startempId2);
  strcat(xml,empId);
  char* endempId = "</empId>\n";
  strcat(xml,endempId);
  //////////////////////////////////
  char* startutcTime = "   <utcTime>";
  strcat(xml,startutcTime);
  strcat(xml,utcTime);
  char* endutcTime = "</utcTime>\n";
  strcat(xml,endutcTime);
  ////////////////////////////////////////
  char* startprojectSwitch = "   <projectSwitch>";
  strcat(xml,startprojectSwitch);
  strcat(xml,projectSwitch);
  char* endprojectSwitch = "</projectSwitch>\n";
  strcat(xml,endprojectSwitch);
  ///////////////////////////////
  char* startgroup = "   <group>";
  strcat(xml,startgroup);
  strcat(xml,group);
  char* endgroup = "</group>\n";
  strcat(xml,endgroup);
  /////////////////////////////
  char* startproject = "   <project>";
  strcat(xml,startproject);
  strcat(xml,project);
  char* endproject = "</project>\n";
  strcat(xml,endproject);
  /////////////////////////////
  char* starttask = "   <task>";
  strcat(xml,starttask);
  strcat(xml,task);
  char* endtask = "</task>\n";
  strcat(xml,endtask);
  /////////////////////////////////
  char* startactivity = "   <activity>";
  strcat(xml,startactivity);
  strcat(xml,activity);
  char* endactivity = "</activity>\n  </ClockIn>\n";
  strcat(xml,endactivity);
  /////////////////////////////////////
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}

//char* SoapMessageGen::InPunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* group ,char* soapVer)
//{
//  xml[0] = '\0';
//  char buf1[200];
//  char buf2[200];
//  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
//    strcpy(xml,SOAP_1_1_START);
//  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
//	 strcpy(xml,SOAP_1_2_START);

//  char* startClockId = buf1;
//  strcpy(startClockId,"  <ClockCredentials xmlns=\"");
//  if(useSoapActionHeader)strcat(startClockId,soapActionHeader);
//  else strcat(startClockId,"http://ws.schedulesource.com/teamwork/services/");
//  strcat(startClockId,"\">\n   <ClockId>");
//  strcat(xml,startClockId);
//  strcat(xml,ClockId);
//  char* endClockId =  "</ClockId>\n";
//  strcat(xml,endClockId);
//  char* startAccount = "   <Account>";
//  strcat(xml,startAccount);
//  strcat(xml,Account);

//  char* endAccount1 =  "</Account>\n  </ClockCredentials>\n </soap:Header>\n";
//  char* endAccount2 =  "</Account>\n  </ClockCredentials>\n </soap12:Header>\n";
//  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
//	strcat(xml,endAccount1);
//  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
//    strcat(xml,endAccount2);
//  char* startempId1 = buf1;
//  char* startempId2 = buf2;
//  strcpy(startempId1," <soap:Body>\n  <InPunch xmlns=\"");
//  strcpy(startempId2," <soap12:Body>\n  <InPunch xmlns=\"");

//  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
//  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
//  strcat(startempId1,"\">\n   <empId>");
//  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
//  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
//  strcat(startempId2,"\">\n   <empId>");



//  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
//	strcat(xml,startempId1);
//  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
//    strcat(xml,startempId2);
//  strcat(xml,empId);

//  char* endempId = "</empId>\n";
//  strcat(xml,endempId);
//  char* startutcTime = "   <utcTime>";
//  strcat(xml,startutcTime);
//  strcat(xml,utcTime);
//  char* endutcTime = "</utcTime>\n";
//  strcat(xml,endutcTime);
//  char* startgroup = "   <group>";
//  strcat(xml,startgroup);
//  strcat(xml,group);
//  char* endgroup = "</group>\n  </InPunch>\n";
//  strcat(xml,endgroup);
//  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
//	strcat(xml ,SOAP_1_1_END);
//  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
//    strcat(xml ,SOAP_1_2_END);
//  return xml;
//}

//char* SoapMessageGen::OutPunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* soapVer)
//{
//  xml[0] = '\0';
//  char buf1[200];
//  char buf2[200];
//  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
//    strcpy(xml,SOAP_1_1_START);
//  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
//	 strcpy(xml,SOAP_1_2_START);

//  char* startClockId = buf1;
//  strcpy(startClockId,"  <ClockCredentials xmlns=\"");
//  if(useSoapActionHeader)strcat(startClockId,soapActionHeader);
//  else strcat(startClockId,"http://ws.schedulesource.com/teamwork/services/");
//  strcat(startClockId,"\">\n   <ClockId>");

//  strcat(xml,startClockId);
//  strcat(xml,ClockId);
//  char* endClockId =  "</ClockId>\n";
//  strcat(xml,endClockId);
//  char* startAccount = "   <Account>";
//  strcat(xml,startAccount);
//  strcat(xml,Account);

//  char* endAccount1 =  "</Account>\n  </ClockCredentials>\n </soap:Header>\n";
//  char* endAccount2 =  "</Account>\n  </ClockCredentials>\n </soap12:Header>\n";
//  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
//	strcat(xml,endAccount1);
//  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
//    strcat(xml,endAccount2);

//  char* startempId1 = buf1;
//  char* startempId2 = buf2;
//  strcpy(startempId1," <soap:Body>\n  <OutPunch xmlns=\"");
//  strcpy(startempId2," <soap12:Body>\n  <OutPunch xmlns=\"");

//  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
//  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
//  strcat(startempId1,"\">\n   <empId>");

//  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
//  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
//  strcat(startempId2,"\">\n   <empId>");

//  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
//	strcat(xml,startempId1);
//  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
//    strcat(xml,startempId2);
//  strcat(xml,empId);

//  char* endempId = "</empId>\n";
//  strcat(xml,endempId);
//  char* startutcTime = "   <utcTime>";
//  strcat(xml,startutcTime);
//  strcat(xml,utcTime);
//  char* endutcTime = "</utcTime>\n  </OutPunch>\n";
//  strcat(xml,endutcTime);
//  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
//	strcat(xml ,SOAP_1_1_END);
//  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
//    strcat(xml ,SOAP_1_2_END);
//  return xml;

//}

char* SoapMessageGen::Heartbeat(char* ClockId ,char* Account ,char* utcTime , char* soapVer)
{
  xml[0] = '\0';
  char buf1[200];
  char buf2[200];
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
    strcpy(xml,SOAP_1_1_START);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
	 strcpy(xml,SOAP_1_2_START);

  char* startClockId = buf1;
  strcpy(startClockId,"  <ClockCredentials xmlns=\"");
  if(useSoapActionHeader)strcat(startClockId,soapActionHeader);
  else strcat(startClockId,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startClockId,"\">\n   <ClockId>");
  strcat(xml,startClockId);
  strcat(xml,ClockId);
  char* endClockId =  "</ClockId>\n";
  strcat(xml,endClockId);
  char* startAccount = "   <Account>";
  strcat(xml,startAccount);
  strcat(xml,Account);

  char* endAccount1 =  "</Account>\n  </ClockCredentials>\n </soap:Header>\n";
  char* endAccount2 =  "</Account>\n  </ClockCredentials>\n </soap12:Header>\n";
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,endAccount1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,endAccount2);

  char* starutcTime1 = buf1;
  char* starutcTime2 = buf2;
  strcpy(starutcTime1," <soap:Body>\n  <Heartbeat xmlns=\"");
  strcpy(starutcTime2," <soap12:Body>\n  <Heartbeat xmlns=\"");
  if(useSoapActionHeader)strcat(starutcTime1,soapActionHeader);
  else strcat(starutcTime1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(starutcTime1,"\">\n   <utcTime>");

  if(useSoapActionHeader)strcat(starutcTime2,soapActionHeader);
  else strcat(starutcTime2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(starutcTime2,"\">\n   <utcTime>");

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,starutcTime1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,starutcTime2);
  strcat(xml,utcTime);
  char* endutcTime = "</utcTime>\n  </Heartbeat>\n";
  strcat(xml,endutcTime);
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}

//char* SoapMessageGen::GetProjectTasks(char* ClockId ,char* Account , char* soapVer)
//{
//  SetHeader(ClockId ,Account ,soapVer);
//  char buf1[200];
//  char buf2[200];
//  char* starutcTime1 = buf1;
//  char* starutcTime2 = buf2;
//  strcpy(starutcTime1," <soap:Body>\n  <GetProjectTasks xmlns=\"");
//  strcpy(starutcTime2," <soap12:Body>\n  <GetProjectTasks xmlns=\"");

//  if(useSoapActionHeader)strcat(starutcTime1,soapActionHeader);
//  else strcat(starutcTime1,"http://ws.schedulesource.com/teamwork/services/");
//  strcat(starutcTime1,"\" />\n");

//  if(useSoapActionHeader)strcat(starutcTime2,soapActionHeader);
//  else strcat(starutcTime2,"http://ws.schedulesource.com/teamwork/services/");
//  strcat(starutcTime2,"\" />\n");

//  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
//	strcat(xml,starutcTime1);
//  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
//    strcat(xml,starutcTime2);

//  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
//	strcat(xml ,SOAP_1_1_END);
//  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
//    strcat(xml ,SOAP_1_2_END);
//  return xml;
//}

char* SoapMessageGen::GetActivities(char* ClockId ,char* Account ,char* soapVer)
{
  SetHeader(ClockId ,Account ,soapVer);

  char buf1[200];
  char buf2[200];
  char* starutcTime1 = buf1;
  char* starutcTime2 = buf2;
  strcpy(starutcTime1," <soap:Body>\n  <GetActivities xmlns=\"");
  strcpy(starutcTime2," <soap12:Body>\n  <GetActivities xmlns=\"");

  if(useSoapActionHeader)strcat(starutcTime1,soapActionHeader);
  else strcat(starutcTime1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(starutcTime1,"\" />\n");

  if(useSoapActionHeader)strcat(starutcTime2,soapActionHeader);
  else strcat(starutcTime2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(starutcTime2,"\" />\n");

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,starutcTime1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,starutcTime2);

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;
}

char* SoapMessageGen::LocalTime(char* ClockId ,char* Account , char* soapVer)
{
  SetHeader(ClockId ,Account ,soapVer);
  char buf1[200];
  char buf2[200];
  char* starutcTime1 = buf1;
  char* starutcTime2 = buf2;
  strcpy(starutcTime1," <soap:Body>\n  <LocalTime xmlns=\"");
  strcpy(starutcTime2," <soap12:Body>\n  <LocalTime xmlns=\"");

  if(useSoapActionHeader)strcat(starutcTime1,soapActionHeader);
  else strcat(starutcTime1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(starutcTime1,"\" />\n");

  if(useSoapActionHeader)strcat(starutcTime2,soapActionHeader);
  else strcat(starutcTime2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(starutcTime2,"\" />\n");

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,starutcTime1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,starutcTime2);

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;
}

char* SoapMessageGen::SwipePunch(char* ClockId ,char* Account,char* empId,char* utcTime,char* soapVer)
{
  SetHeader(ClockId ,Account ,soapVer);

  char buf1[200];
  char buf2[200];
  char* startempId1 = buf1;
  char* startempId2 = buf2;
  strcpy(startempId1," <soap:Body>\n  <SwipePunch xmlns=\"");
  strcpy(startempId2," <soap12:Body>\n  <SwipePunch xmlns=\"");

  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId1,"\">\n   <empId>");
  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId2,"\">\n   <empId>");



  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,startempId1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,startempId2);
  strcat(xml,empId);

  char* endempId = "</empId>\n";
  strcat(xml,endempId);
  char* startutcTime = "   <utcTime>";
  strcat(xml,startutcTime);
  strcat(xml,utcTime);
  char* endutcTime = "</utcTime>\n  </SwipePunch>\n";
  strcat(xml,endutcTime);
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}


char* SoapMessageGen::SetHeader(char* ClockId ,char* Account , char* soapVer)
{
  xml[0] = '\0';
  char buf1[200];
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
    strcpy(xml,SOAP_1_1_START);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
	 strcpy(xml,SOAP_1_2_START);


  char* startClockId = buf1;
  strcpy(startClockId,"  <ClockCredentials xmlns=\"");
  if(useSoapActionHeader)strcat(startClockId,soapActionHeader);
  else strcat(startClockId,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startClockId,"\">\n   <ClockId>");

  strcat(xml,startClockId);
  strcat(xml,ClockId);
  char* endClockId =  "</ClockId>\n";
  strcat(xml,endClockId);
  char* startAccount = "   <Account>";
  strcat(xml,startAccount);
  strcat(xml,Account);

  char* endAccount1 =  "</Account>\n  </ClockCredentials>\n </soap:Header>\n";
  char* endAccount2 =  "</Account>\n  </ClockCredentials>\n </soap12:Header>\n";
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,endAccount1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,endAccount2);
  return xml;
}



char* SoapMessageGen::SwipePunchWithActivityCode(char* ClockId ,char* Account,char* empId,char* activityCode,char* utcTime,char* soapVer)
{



		//
		//// operation name is ClockIn
		//

  SetHeader(ClockId ,Account ,soapVer);

  char buf1[200];
  char buf2[200];
  char* startempId1 = buf1;
  char* startempId2 = buf2;
  strcpy(startempId1," <soap:Body>\n  <ClockIn xmlns=\"");
  strcpy(startempId2," <soap12:Body>\n  <ClockIn xmlns=\"");

  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId1,"\">\n   <empId>");
  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId2,"\">\n   <empId>");



  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,startempId1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,startempId2);
  strcat(xml,empId);

  char* endempId = "</empId>\n";
  strcat(xml,endempId);

  char* startac = "<activity>";
  strcat(xml,startac);
  if ( activityCode && strlen(activityCode) )
  {
  	strcat(xml,activityCode);
  }
  char* endac = "</activity>\n";
  strcat(xml,endac);

  char* startutcTime = "   <utcTime>";
  strcat(xml,startutcTime);
  strcat(xml,utcTime);
  char* endutcTime = "</utcTime>\n  </ClockIn>\n";
  strcat(xml,endutcTime);
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}

char* SoapMessageGen::IsValidEmployee(char* ClockId ,char* Account,char* empId,char* utcTime,char* soapVer)
{
  SetHeader(ClockId ,Account ,soapVer);

  char buf1[200];
  char buf2[200];
  char* startempId1 = buf1;
  char* startempId2 = buf2;
  strcpy(startempId1," <soap:Body>\n  <IsValidEmployee xmlns=\"");
  strcpy(startempId2," <soap12:Body>\n  <IsValidEmployee xmlns=\"");

  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId1,"\">\n   <EmployeeID>");
  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId2,"\">\n   <EmployeeID>");



  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,startempId1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,startempId2);
  strcat(xml,empId);

  char* endempId = "</EmployeeID>\n";
  strcat(xml,endempId);

  // 7/6/2009 utcTime is not passed at this time. adjust.
  //char* startutcTime = "   <utcTime>";
  //strcat(xml,startutcTime);
  //strcat(xml,utcTime);
  //char* endutcTime = "</utcTime>\n  </IsValidEmployee>\n";

  char* endutcTime = "</IsValidEmployee>\n";
  strcat(xml,endutcTime);
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}

char* SoapMessageGen::EmployeeUsesPin(char* ClockId ,char* Account,char* empId, char * utcTime, char* soapVer)
{
  SetHeader(ClockId ,Account ,soapVer);

  char buf1[200];
  char buf2[200];
  char* startempId1 = buf1;
  char* startempId2 = buf2;
  strcpy(startempId1," <soap:Body>\n  <EmployeeUsesPin xmlns=\"");
  strcpy(startempId2," <soap12:Body>\n  <EmployeeUsesPin xmlns=\"");

  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId1,"\">\n   <EmployeeID>");
  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId2,"\">\n   <EmployeeID>");



  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,startempId1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,startempId2);
  strcat(xml,empId);

  char* endempId = "</EmployeeID>\n";
  strcat(xml,endempId);

  // 7/6/2009 utcTime is not passed at this time. adjust.
  //char* startutcTime = "   <utcTime>";
  //strcat(xml,startutcTime);
  //strcat(xml,utcTime);
  //char* endutcTime = "</utcTime>\n  </EmployeeUsesPin>\n";

  char* endutcTime = "</EmployeeUsesPin>\n";
  strcat(xml,endutcTime);
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}

char* SoapMessageGen::ValidEmployeePin(char* ClockId ,char* Account,char* empId, char* empPin, char * utcTime, char* soapVer)
{
  SetHeader(ClockId ,Account ,soapVer);

  char buf1[200];
  char buf2[200];
  char* startempId1 = buf1;
  char* startempId2 = buf2;
  strcpy(startempId1," <soap:Body>\n  <ValidEmployeePin xmlns=\"");
  strcpy(startempId2," <soap12:Body>\n  <ValidEmployeePin xmlns=\"");

  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId1,"\">\n   <EmployeeID>");
  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId2,"\">\n   <EmployeeID>");

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,startempId1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,startempId2);
  strcat(xml,empId);

  char* endempId = "</EmployeeID>\n";
  strcat(xml,endempId);

  // 7/6/2009 utcTime is not passed at this time. adjust.
  //char* startutcTime = "   <utcTime>";
  //strcat(xml,startutcTime);
  //strcat(xml,utcTime);
  //char* endutcTime = "</utcTime>\n  </ValidEmployeePin>\n";

  char* startempin = "   <EmployeePin>";
  strcat(xml,startempin);
  strcat(xml,empPin);
  char* endempin = "</EmployeePin>\n";
  strcat(xml,endempin);

  char* endutcTime = "</ValidEmployeePin>\n";
  strcat(xml,endutcTime);
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}

char* SoapMessageGen::GetPinTable(char* ClockId ,char* Account ,char* soapVer)
{
  SetHeader(ClockId ,Account ,soapVer);

  char buf1[200];
  char buf2[200];
  char* starutcTime1 = buf1;
  char* starutcTime2 = buf2;
  strcpy(starutcTime1," <soap:Body>\n  <GetPinTable xmlns=\"");
  strcpy(starutcTime2," <soap12:Body>\n  <GetPinTable xmlns=\"");

  if(useSoapActionHeader)strcat(starutcTime1,soapActionHeader);
  else strcat(starutcTime1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(starutcTime1,"\" />\n");

  if(useSoapActionHeader)strcat(starutcTime2,soapActionHeader);
  else strcat(starutcTime2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(starutcTime2,"\" />\n");

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,starutcTime1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,starutcTime2);

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;
}


//
// fpm
//
char* SoapMessageGen::SetFPTemplate(char* ClockId ,char* Account,char* empId, char* utcTime, char* templateId, char* fptemplate, char* soapVer)
{
  xml[0] = '\0';
  char buf1[200];
  char buf2[200];
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
    strcpy(xml,SOAP_1_1_START);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
	 strcpy(xml,SOAP_1_2_START);
  char* startClockId = buf1;
  strcpy(startClockId,"  <ClockCredentials xmlns=\"") ;
  if(useSoapActionHeader)strcat(startClockId,soapActionHeader);
  else strcat(startClockId,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startClockId,"\">\n   <ClockId>");
  strcat(xml,startClockId);
  strcat(xml,ClockId);
  char* endClockId =  "</ClockId>\n";
  strcat(xml,endClockId);
  char* startAccount = "   <Account>";
  strcat(xml,startAccount);
  strcat(xml,Account);

  char* endAccount1 =  "</Account>\n  </ClockCredentials>\n </soap:Header>\n";
  char* endAccount2 =  "</Account>\n  </ClockCredentials>\n </soap12:Header>\n";
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,endAccount1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,endAccount2);

  char* startempId1 = buf1;
  char* startempId2 = buf2;
  strcpy(startempId1," <soap:Body>\n  <SetFPTemplate xmlns=\"");
  strcpy(startempId2," <soap12:Body>\n  <SetFPTemplate xmlns=\"");

  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId1,"\">\n   <BadgeId>");

  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId2,"\">\n   <BadgeId>");

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,startempId1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,startempId2);
  strcat(xml,empId);
  char* endempId = "</BadgeId>\n";
  strcat(xml,endempId);
  //////////////////////////////////
  char* startutcTime = "   <utcTime>";
  strcat(xml,startutcTime);
  strcat(xml,utcTime);
  char* endutcTime = "</utcTime>\n";
  strcat(xml,endutcTime);
  char* starttemplateid = "   <TemplateId>";
  strcat(xml,starttemplateid);
  strcat(xml,templateId);
  char* endtemplateid = "</TemplateId>\n";
  strcat(xml,endtemplateid);
  /////////////////////////////////
  char* startfptemplate = "   <Template>";
  strcat(xml,startfptemplate);
  strcat(xml,fptemplate);
  char* endfptemplate = "</Template>\n  </SetFPTemplate>\n";
  strcat(xml,endfptemplate);
  /////////////////////////////////////
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}

char* SoapMessageGen::GetFPTemplate(char* ClockId ,char* Account,char* empId, char* utcTime, char* soapVer)
{
  xml[0] = '\0';
  char buf1[200];
  char buf2[200];
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
    strcpy(xml,SOAP_1_1_START);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
	 strcpy(xml,SOAP_1_2_START);
  char* startClockId = buf1;
  strcpy(startClockId,"  <ClockCredentials xmlns=\"") ;
  if(useSoapActionHeader)strcat(startClockId,soapActionHeader);
  else strcat(startClockId,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startClockId,"\">\n   <ClockId>");
  strcat(xml,startClockId);
  strcat(xml,ClockId);
  char* endClockId =  "</ClockId>\n";
  strcat(xml,endClockId);
  char* startAccount = "   <Account>";
  strcat(xml,startAccount);
  strcat(xml,Account);

  char* endAccount1 =  "</Account>\n  </ClockCredentials>\n </soap:Header>\n";
  char* endAccount2 =  "</Account>\n  </ClockCredentials>\n </soap12:Header>\n";
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,endAccount1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,endAccount2);

  char* startempId1 = buf1;
  char* startempId2 = buf2;
  strcpy(startempId1," <soap:Body>\n  <GetFPTemplate xmlns=\"");
  strcpy(startempId2," <soap12:Body>\n  <GetFPTemplate xmlns=\"");

  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId1,"\">\n   <BadgeId>");

  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId2,"\">\n   <BadgeId>");

  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,startempId1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,startempId2);
  strcat(xml,empId);
  char* endempId = "</BadgeId>\n";
  strcat(xml,endempId);
  //////////////////////////////////
  char* startutcTime = "   <utcTime>";
  strcat(xml,startutcTime);
  strcat(xml,utcTime);
  char* endutcTime = "</utcTime>\n";
  strcat(xml,endutcTime);
  //
  char* endGetFPT = "  </GetFPTemplate>\n";
  strcat(xml, endGetFPT);
  /////////////////////////////////////
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}


char* SoapMessageGen::PopulateFPTemplates(char* ClockId ,char* Account, char* soapVer)
{
  SetHeader(ClockId ,Account ,soapVer);

  char buf1[200];
  char buf2[200];
  char* startempId1 = buf1;
  char* startempId2 = buf2;
  strcpy(startempId1," <soap:Body>\n  <PopulateFPTemplates xmlns=\"");
  strcpy(startempId2," <soap12:Body>\n  <PopulateFPTemplates xmlns=\"");

  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId1,"\" />\n");
  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId2,"\" />\n");
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,startempId1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,startempId2);

  //char* endutcTime = "</PopulateFPTemplates>\n";
  //strcat(xml,endutcTime);
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}

char* SoapMessageGen::TemplatesAvailable(char* ClockId ,char* Account, char* utcTime,char* soapVer)
{
  SetHeader(ClockId ,Account ,soapVer);

  char buf1[200];
  char buf2[200];
  char* startempId1 = buf1;
  char* startempId2 = buf2;
  strcpy(startempId1," <soap:Body>\n  <TemplatesAvailable xmlns=\"");
  strcpy(startempId2," <soap12:Body>\n  <TemplatesAvailable xmlns=\"");

  if(useSoapActionHeader)strcat(startempId1,soapActionHeader);
  else strcat(startempId1,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId1,"\">\n   <utcTime>");
  if(useSoapActionHeader)strcat(startempId2,soapActionHeader);
  else strcat(startempId2,"http://ws.schedulesource.com/teamwork/services/");
  strcat(startempId2,"\">\n   <utcTime>");
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml,startempId1);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml,startempId2);
  strcat(xml,utcTime);
  char* endempId = "</utcTime>\n";
  strcat(xml,endempId);

  char* endutcTime = "</TemplatesAvailable>\n";
  strcat(xml,endutcTime);
  if(strcmp(soapVer,SOAP_1_1_VER) == 0)
	strcat(xml ,SOAP_1_1_END);
  if(strcmp(soapVer,SOAP_1_2_VER) == 0)
    strcat(xml ,SOAP_1_2_END);
  return xml;

}



/*int
main (int argc, char * argv [])
{
  SoapMessageGen sp;
  printf("%s\n\n",sp.InPunch("UnitTest","859-767-2072","1491315","2007-12-13T00:59:36.0204412Z","0",SOAP_1_1_VER));
  printf("*********************************************************************\n");
  printf("%s\n\n",sp.Heartbeat("UnitTest","859-767-2072","2007-12-13T00:59:36.0204412Z",SOAP_1_2_VER));
  printf("*********************************************************************\n");
  printf("%s\n\n",sp.OutPunch("UnitTest","859-767-2072","2222222","2007-12-13T00:59:36.0204412Z",SOAP_1_1_VER));
  return 0 ;
}*/
