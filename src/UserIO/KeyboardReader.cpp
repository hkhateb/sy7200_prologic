#include <string.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "KeyboardReader.h"
#include "StdInput.h"

void *keyboardReader (void *data)
{
  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
	long * timeout = (long *)data;
	long tm = 0;
	char buffer[24];
	char * bdg = buffer;
	int pipe = open(PIPE_KEYBOARDREADER, O_WRONLY);
	if (pipe>0)
	{
		while(tm < (*timeout) * 1000)
		{
			if (user_input_ready()) 
			{
				char keypress = user_input_get ();
				if(keypress == KEY_ENTER) 
				{
					*(bdg) = '\0';
					write (pipe, buffer, strlen(buffer));
					break;
				}
				*(bdg++) = keypress;
				/*char disp[2];
				disp[0] = keypress;
				disp[1] = '\0';
				user_output_write (0, 0, disp);*/
			}
			else
			{
				usleep(25000);
			}
			tm += 25;
		}
	}
	close(pipe);
	return NULL;
}

