#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <typeinfo>
#include <errno.h>
#include "LCDHandler.h"

static const char * CONFIRM_MENU_YESNO			= "    (1-YES 2-NO)    ";

char LCDHandler::GetNextAlphaNumericChar(char ch)
{
	if(ch == ' ') 
	{
		//ch = 'a';
		ch = 'A';
	}
	else if(ch == 'z' || ch == 'Z') 
	{
		ch = '0';
	}
	else if(ch == '9') 
	{
		//ch = 'a';
		//ch = 'A';
		ch = '.';
	}
	else if(ch == '.')
	{
		ch = '-';
	}
	else if(ch == '-')
	{
		ch = 'A';
	}
	else 
	{
		ch++;
	}
	return ch;
}

char LCDHandler::GetPrevAlphaNumericChar(char ch)
{
	if(ch == ' ') 
	{
		//ch = '9';
		ch = '-';
	}
	else if(ch == '0') 
	{
		//ch = 'z';
		ch = 'Z';
	}
	else if(ch == 'a' || ch == 'A') 
	{
		//ch = '9';
		ch = '-';
	}
	else if(ch == '-')
	{
		ch = '.';
	}
	else if(ch == '.')
	{
		ch = '9';
	}
	else 
	{
		ch--;
	}
	return ch;
}

char LCDHandler::GetNextAlphaChar(char ch)
{
	if(ch == ' ') 
	{
		//ch = 'a';
		ch = 'A';
	}
	else if(ch == 'z' || ch == 'Z') 
	{
		//ch = 'a';
		ch = 'A';
	}
	else 
	{
		ch++;
	}
	return ch;
}

char LCDHandler::GetPrevAlphaChar(char ch)
{
	if(ch == ' ') 
	{
		//ch = 'z';
		ch = 'Z';
	}
	else if(ch == 'a' || ch == 'A') 
	{
		//ch = 'z';
		ch = 'Z';
	}
	else 
	{
		ch--;
	}
	return ch;
}

char LCDHandler::GetNextSSIDChar(char ch)
{
	if(ch == ' ') 
	{
		//ch = 'a';
		ch = 'A';
	}
	else if(ch == 'z' || ch == 'Z') 
	{
		ch = '0';
	}
	else if(ch == '9') 
	{
		ch = '_';
	}
	else if(ch == '_') 
	{
		ch = '-';
	}
	else if(ch == '-') 
	{
		ch = ' ';
	}
	else 
	{
		ch++;
	}
	return ch;
}

char LCDHandler::GetPrevSSIDChar(char ch)
{
	if(ch == ' ') 
	{
		ch = '-';
	}
	else if(ch == '-') 
	{
		ch = '_';
	}
	else if(ch == '_') 
	{
		ch = '9';
	}
	else if(ch == '0') 
	{
		//ch = 'z';
		ch = 'Z';
	}
	else if(ch == 'a' || ch == 'A') 
	{
		ch = ' ';
	}
	else 
	{
		ch--;
	}
	return ch;
}

char LCDHandler::GetNextHexChar(char ch)
{
	if(ch == '9') 
	{
		ch = 'A';
	}
	else if(ch == 'F') 
	{
		ch = '0';
	}
	else 
	{
		ch++;
	}
	return ch;
}

char LCDHandler::GetPrevHexChar(char ch)
{
	if(ch == 'A') 
	{
		ch = '9';
	}
	else if(ch == '0') 
	{
		ch = 'F';
	}
	else 
	{
		ch--;
	}
	return ch;
}

/**
 * TID can have values [0-9] OR [A-Z]
 */
char LCDHandler::GetNextTId(char ch)
{
	if(ch == '9')
	{
		ch = 'A';
	}
	else if(ch == 'Z') 
	{
		ch = '0';
	}
	else 
	{
		ch++;
	}
	return ch;
}

/**
 * TID can have values [0-9] OR [A-Z]
 */
char LCDHandler::GetPrevTId(char ch)
{
	if(ch == 'A') 
	{
		ch = '9';
	}
	else if(ch == '0') 
	{
		ch = 'Z';
	}
	else 
	{
		ch--;
	}
	return ch;
}

LCD_READ_STATE LCDHandler::ShowConfirmationDialog(const char * msg, bool & confirm)
{
start:	user_output_clear();
	int len = strlen(msg);
	int col = ( 20 - len ) / 2 + 1;
	user_output_write(2, col, msg);
	user_output_write(3, 1, CONFIRM_MENU_YESNO);
	
	int timeout = oper.GetDataTimeout();
	int tm = 0;
	char input;
	while(true)
	{
		if (user_input_ready ()) 
		{
			
			input = user_input_get ();
            switch(input)
		    {
			    case '1': // Do Not Use
				    oper.Beep(50,TYPE_KeyPress);
                    confirm = true;
                    goto end;;
                break;   
			    case '2': // Std Dates
				    oper.Beep(50, TYPE_KeyPress);
				    confirm = false;
				    goto end;
			    break;
			    case ESC:
			        oper.Beep(50, TYPE_KeyPress);
			        return READ_CLEAR; 
			    break;
			    default:
			        oper.Beep(150,TYPE_Reject);
                    oper.displayErrMessage("   NOT AVAILABLE    ");
                    goto start; 
                break;    
			}
		}
		else
		{
			usleep(10000);
			tm += 20;
			if(tm >= timeout)
			{
				confirm = false;
				return READ_TIMEDOUT;
			}
		}
	}
	end: return READ_SUCCESS;
}

