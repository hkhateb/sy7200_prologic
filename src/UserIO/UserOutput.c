/*
	UserInterface:

    Interface functions to communicate with the NE7000 user interface
board.

	Copyright (c) 2005
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   25 Jan 2005
	 $Revision: 1.1.1.1 $
	   $Author: AlanL $
	  $Modtime$
	     $Date: 2006/12/01 18:43:11 $

*/


/* Compilation optins for UserInterface. */
#define DEBUG                   0

/* Configuration include file for UserInterface. */
#include "common.h"

#if defined CONFIG_NE7000_USER || defined CONFIG_SPLASH_TEXT

/* Include files for UserInterface. */
#if defined CONFIG_LIBNE /* User space library with kernel drivers. */
#include <String.h>
#endif /* CONFIG_LIBNE */
#include <StdIO.h>
#include <FCntl.h>
#include <UniStd.h>
#include <sys/IOCtl.h>
#include <asm/TA7000_IO.h>
#include "Gen_Def.h"
#include "User.h"

/* Debug global defines. */
#if defined (DEBUG) && DEBUG > 0
	#define debugF(fmt, args...) printf("%s: " fmt, __func__, ## args)
	#define debugFX(lvl, fmt, args...) if (DEBUG >= lvl) debugF(fmt, ## args);
#else
	#define debugF(fmt, args...)
	#define debugFX(lvl, fmt, args...)
#endif

/* Constant parameters for UserInterface. */

/* Type and structure definitions for UserInterface. */

/* Internal static data for UserInterface. */

/* Function prototypes for UserInterface. */


/*
	user_output_initialize:

    Initialize or reinitialize the LCD display.

Description:

    The display is initialized, cleared and the cursor position set to row
1, column 1.

Arguments:
	void

Returns:
	Code    System status code of SC_OKAY for successful completion;

*/

		Code
user_output_initialize (void)

{
	Code code = SC_OKAY;
	const Byte command [] = {CMD_LCD_INIT};

	code = user_interface_output_send (command, sizeof command);

	return (code);
}


/*
	user_output_clear:

    Clear the LCD display.

Description:

    The display is cleared and the cursor position set to row 1, column 1.

Arguments:
	void

Returns:
	Code    System status code of SC_OKAY for successful completion;

*/

		Code
user_output_clear (void)

{
	Code code = SC_OKAY;
	const Byte command [] = {CMD_LCD_CLEAR};

	code = user_interface_output_send (command, sizeof command);

	return (code);
}


/*
	user_output_write:

    Write a string to the LCD display.

Description:

    Write the supplied string to the LCD display.  Printable ASCII
characters are displayed; certain control characters cause their normal
display actions (see Notes below).

Notes:

    The LCD Write command optionally positions the cursor then writes data
to successive display positions.  Its data consists of a pair of cursor
position bytes followed, optionally, by data to write to the display.  The
first position byte specifies the LCD character row while the second
position byte specifies the LCD character column.  The rows and columns
are numbered starting with 1.  A row or column position of 0 indicates the
respective value does not change instead the current cursor row and/or
column is used.

    Any bytes following the two positioning bytes are written to the
display as ASCII data starting at the present cursor position as defined
by the positioning bytes.  If encountered, any of the 32 character codes
in the range 00h to 1Fh are not sent to the display.  Some of the values
in this range perform their standard ASCII functions as follows:

	NewLine         '\n' [0Ah]   Position to column 1 of the next row.
	Carriage Return '\r' [0Dh]   Position to column 1 of the current row.
	Form Feed       '\f' [0Ch]   Position to row 1, column 1.

    Non-ASCII characters codes in the range 80h to FFh are sent to the
display without further processing and are assumed to be displayable
characters.  The resulting display is hardware dependent and left to the
Master Controller developer and driver for its specifics.

Arguments:
	row     Position to the specified row before writing;
		0 use current row setting.

	column  Position to the specified column before writing;
		0 use current column setting.

	display String to write to display (see Notes).

Returns:
	Code    System status code of SC_OKAY for successful completion;

*/

		Code
user_output_write (UBin8 row, UBin8 column, const char * display)

{
	Code code = SC_OKAY;
	Byte buffer [strlen (display) + UserInterfaceHeaderLengthWrite + 1];
	Byte * bp = buffer;
	const size_t max_len = sizeof buffer - UserInterfaceHeaderLengthWrite - 1;
	size_t length;

	*bp++ = CMD_LCD_WRITE;
	*bp++ = row;
	*bp++ = column;
	length = strlen (display);
	length = length < max_len ? length : max_len;
	*bp = '\0';
	debugFX (3, "Copy %d display characters to buffer.\n", length);
	strncat (bp, display, length);
	length += UserInterfaceHeaderLengthWrite;

	code = user_interface_output_send (buffer, length);

	return (code);
}


/*
	user_output_cursor:

    Set cursor mode for LCD display.

Description:

    The cursor mode for the display is set to the specified mode.  Cursor
can be off, blink character at cursor or display underline cursor.

Arguments:
	mode    Bit values from USER_CURSOR_* defines.

Returns:
	Code    System status code of SC_OKAY for successful completion;

*/

		Code
user_output_cursor (Bit8 mode)

{
	Code code = SC_OKAY;
	Byte command [2] = {CMD_LCD_CURSOR};

	command[1] = mode;

	code = user_interface_output_send (command, sizeof command);

	return (code);
}


/*
	user_output_test:

    Test LCD display.

Description:

    Perform a display test by filling the display region requested with a
test character.

Arguments:
	rows		Number of display rows to fill.
	
	columns		Number of display columns to fill.

Returns:
	void

*/

		Code
user_output_test (UBin8 rows, UBin8 columns)

{
	Code status = SC_OKAY;
	char buffer [columns+1];
	static const int test_char = 0xFF;

	buffer [columns] = '\0';
	memset (buffer, test_char, columns);

	user_output_write (1, 1, "");
	UBin8 ridx;
	for (ridx = 1; ridx <= rows; ridx++) {
		user_output_write (ridx, 1, buffer);
	}
	user_output_write (1, 1, "");

	return (status);
}


/*
	user_output_contrast:

    Set LCD contrast.

Description:

    Set bias voltage to character LCD to adjust contrast of display.

Notes:

    Only the high 10 bits of the contrast setting are significant in the
actual hardware setting.

Arguments:
	contrast	Unsigned 16 bit contrast setting.

Returns:
	Code		System status code of SC_OKAY for successful completion;

*/

		Code
user_output_contrast (UBin16 contrast)

{
	Code code = SC_OKAY;
	Byte buffer [3];

	buffer[0] = CMD_LCD_CONTRAST;
	buffer[1] = (contrast >> 8) & 0xFF;
	buffer[2] = contrast & 0xFF;

	code = user_interface_output_send (buffer, sizeof buffer);

	return (code);
}

#endif /* defined CONFIG_NE7000_USER || defined CONFIG_SPLASH_TEXT */

