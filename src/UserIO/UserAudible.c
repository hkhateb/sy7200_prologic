/*
	UserAudible:  [Module]

	The UserAudible module provides routines to control SY7000 data
	collection terminal Buzzer(audio controller ).

	Copyright (c) 2005 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   03 Jun 2005
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime$
	     $Date: 2005/09/01 22:27:58 $

*/


/* Compilation options for UserAudible. */
#define DEBUG		1
#undef DEBUG

/* Include files for UserAudible. */
#include <StdIO.h>
#include <FCntl.h>
#include <UniStd.h>
#include <sys/IOCtl.h>
#include <asm/TA7000_IO.h>
#include "Gen_Def.h"
#include "User.h"

/* Debug global defines. */
#ifdef DEBUG
	#define PRINTD(fmt, args...) printf("%s: " fmt, __func__, ## args)
#else
	#define PRINTD(fmt, args...)
#endif

/* Constant parameters for UserAudible. */
#define USER_AUDIBLE_DEVICE		"/dev/" TA7000_DRIVER_PATH_AUDIBLE

/* Type and structure definitions for UserAudible. */
typedef struct {
	int file;
	}
UserAudible_t;

/* Internal static data for UserAudible. */
static UserAudible_t _ua = {
	.file = -1,
	};

/* External global data for UserAudible. */

/* Function prototypes for UserAudible. */



/*
	user_audible_init:

	Initalize the audible output module.

Description:

	Open audible output device and set up for audible output.

See also:  user_audible_set

Arguments:
	void

Returns:
	int		>= 0 if successful, value is file handle;
			< 0 if an error occurred.

*/

		int
user_audible_init (void)

{

	PRINTD ("Initializing user audible outputs module.\n");

	_ua.file = open (USER_AUDIBLE_DEVICE, O_RDWR);

	return (_ua.file);
}


/*
	user_audible_close:

	Close the audible output module.

Description:

	Disable audible output and close device file.

See also:  user_audible_set

Arguments:
	void

Returns:
	int		>= 0 if successful, value is file handle;
			< 0 if an error occurred.

*/

		void
user_audible_close (void)

{

	PRINTD ("Closing user audible outputs module.\n");
	if (_ua.file >= 0) {
		close (_ua.file);
	}

	return;
}


/*
	user_audible_set:

	Set an indicator light output.

Description:

	Set the audible output tone (frequency) or shut it off.

See also:  user_audible_init

Arguments:
	tone		Frequency to output;
				0 to shut off audible output.
																							
Returns:
	int			0 if successful;
				<0 if an an error occurs.

*/

		int
user_audible_set (unsigned int tone)

{
	int status = 0;

	#ifdef DEBUG
	if (tone > 0) PRINTD ("Set %u Hz tone.\n", tone);
	else PRINTD ("Shut off audible output.\n");
	#endif

	status = ioctl (_ua.file, AUDIBLE_OUT_IOC_TONE, (unsigned long)tone);			

	return (status);
}
/*
	user_audible_set:

	Set an volume audible output module.

Description:

	Set the audible output tone volume.

See also:  user_audible_init

Arguments:
	volume		volume level to output ,the levels range from 10 to 100.
																							
Returns:
	int			0 if successful;
				<0 if an an error occurs.

*/

int     user_audible_volume (unsigned int volume)

{
	int status = 0;

	#if defined DEBUG && DEBUG > 0
	if (volume > 0) PRINTD ("Set %u volume.\n", volume);
	else PRINTD ("Shut off audible output.\n");
	#endif

	status = ioctl (_ua.file, AUDIBLE_OUT_IOC_VOLUME, (unsigned long)volume);			

	return (status);
}


