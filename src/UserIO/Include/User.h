/*
	User_Def.h:

    Standard definitions for the NE7000 system software User subsystem.

	Copyright (c) 2005
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   25 Jan 2005
	 $Revision: 1.6 $
	   $Author: walterm $
	  $Modtime$
	     $Date: 2006/01/06 00:58:09 $

*/


#if ! defined _USER_DEF

#include "Gen_Def.h"

/* Status codes for User subsystem. */
typedef enum {
	SC_USER_GENERIC = 100,
	SC_USER_MISSING_ADAPTOR,
	SC_USER_DEVICE_ADDRESS,
}
UserCode_e;

/* Key value constants. */
#define KEY_ENTER		'\n'
#define KEY_ESC 		0x1B
#define KEY_CLEAR		KEY_ESC
#if ! defined (ESC)
#define ESC			KEY_ESC
#endif

/* Indicator light definitions. */
#define USER_VISUAL_RED		1
#define USER_VISUAL_YEL		2
#define USER_VISUAL_GRN		3

/* Cursor definitions. */
#define USER_CURSOR_OFF		0x00
#define USER_CURSOR_UNDER	0x01
#define USER_CURSOR_BLINK	0x02
#define USER_CURSOR_BOTH	0x03

/* External function prototypes for User subsystem. */
Code    user_init (void);
Code    user_close (void);
Boolean user_input_ready (void);
char    user_input_get (void);
Code	user_output_initialize (void);
Code	user_output_clear (void);
Code	user_output_write (UBin8 row, UBin8 column, const char * display);
Code    user_output_cursor (Bit8 mode);
Code	user_output_contrast (UBin16 contrast);
Code    user_output_test (UBin8 rows, UBin8 columns);

int	user_visual_init (void);
void    user_visual_close (void);
int	user_visual_set (int light, int state);

int	user_audible_init (void);
void    user_audible_close (void);
int     user_audible_set (unsigned int tone);
int     user_audible_volume (unsigned int volume);

int	user_digital_init (void);
void    user_digital_close (void);
int	user_digital_set (int output, int state);
int     user_digital_get (int output);

int	user_barcode_setup (void);
int	user_barcode_restore (void);
int	user_barcode_open (const char * device, int baudrate);
void	user_barcode_close (void);
int     user_barcode_read_settings (Byte * buffer, int max_length);

#define user_input_init  user_init
#define user_input_close user_close

#define _USER_DEF
#endif


