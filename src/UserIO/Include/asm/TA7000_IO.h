/*
    TA7000_IO.h

    Definitions for ioctl commands values, device driver paths, etc.  for
TA7000 main controller I/O.

	Copyright (c) 2005 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   31 May 2005
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime$
	     $Date: 2005/09/01 22:27:58 $

*/


#ifndef _PPC_TA7000_IO_H
#define _PPC_TA7000_IO_H

#define TA7000_IOC_TYPE		'A'
#define TA7000_IOC_BASE		0x80

#define VISUAL_OUT_IOC_RESET	_IO(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x00)

#define VISUAL_OUT_IOC_W_BASE	_IOW(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x10, Boolean)
#define VISUAL_OUT_IOC_R_BASE	_IOR(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x10, Boolean)
#define VISUAL_OUT_IOC_W_RED	_IOW(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x10, Boolean)
#define VISUAL_OUT_IOC_R_RED	_IOR(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x10, Boolean)
#define VISUAL_OUT_IOC_W_YEL	_IOW(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x11, Boolean)
#define VISUAL_OUT_IOC_R_YEL	_IOR(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x11, Boolean)
#define VISUAL_OUT_IOC_W_GRN	_IOW(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x12, Boolean)
#define VISUAL_OUT_IOC_R_GRN	_IOR(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x12, Boolean)

#define AUDIBLE_OUT_IOC_RESET	_IO(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x20)

#define AUDIBLE_OUT_IOC_TONE	_IOW(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x30, Boolean)

#define AUDIBLE_OUT_IOC_VOLUME	_IOW(TA7000_IOC_TYPE, TA7000_IOC_BASE+0x31, Boolean)

/* Board specific device driver paths. */
#define TA7000_DEVICE_GROUP "dct"
#define TA7000_DRIVER_ROOT TA7000_DEVICE_GROUP
#define TA7000_DRIVER_NAME_VISUAL "visual"
#define TA7000_DRIVER_PATH_VISUAL TA7000_DRIVER_ROOT "/" TA7000_DRIVER_NAME_VISUAL
#define TA7000_DRIVER_NAME_AUDIBLE "audible"
#define TA7000_DRIVER_PATH_AUDIBLE TA7000_DRIVER_ROOT "/" TA7000_DRIVER_NAME_AUDIBLE
#define TA7000_DRIVER_NAME_CARDS "card"
#define TA7000_DRIVER_PATH_CARDS TA7000_DRIVER_ROOT "/" TA7000_DRIVER_NAME_CARDS
#define TA7000_DRIVER_NAME_PROX "prox"
#define TA7000_DRIVER_PATH_PROX TA7000_DRIVER_PATH_CARDS "/" TA7000_DRIVER_NAME_PROX

/* Kernel driver and board support definitions. */
#ifdef __KERNEL__

/* Device initialization entries. */
#if CONFIG_TA7000_IO
int visual_out_init (void);
int audible_out_init (void);
#endif
#if CONFIG_TA7000_CARD_READER
int card_reader_init (void);
#endif

/* Pointer to board specific device driver proc file directory. */
extern struct proc_dir_entry * TA7000_proc_device_dir;

#endif

#endif
