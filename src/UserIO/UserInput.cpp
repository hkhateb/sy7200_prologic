//Copyright (C) 2005 TimeAmerica, Inc. All Rights Reserved
//20051109 wjm
//$Id: UserInput.cpp,v 1.11 2006/06/23 23:16:50 deepali Exp $

#include <termios.h>
#include "logger.h"
#include "FPOperation.hpp"
#include "UserInput.hpp"
extern "C" {
#include "User.h"
}

extern FPOperation fpOpr;

//***********************************************************************
bool UserInput ::pause_reading = false;

int* UserInput ::in_fd = 0 ; //will be established on first call
unsigned int UserInput ::timeout_us = 1000 ;//default value at prog. start

char UserInput ::buffer [ 100 ];
char* UserInput ::bufEnd = buffer ;
char* UserInput ::bufP = buffer ;
fd_set UserInput ::set ;
UserInput ::source_t source_temp = UserInput ::OTHER;
bool UserInput ::TemplateScanReady = false;
bool UserInput::PartialScanReady = false;
bool UserInput::ScanFailed = false;

//***********************************************************************
namespace {
  //add/subtract badge reader sources here..
  //program adds stdin to the list.
  static const char* inputs [] = {
    "/dev/dct/card/prox",
    "/dev/dct/card/mag/1",
    "/dev/dct/card/mag/2",
    "/dev/dct/card/mag/3",
    "/dev/tts/1"
  };

  static const int inputCnt = sizeof ( inputs ) / sizeof ( const char* );

  int* in_fde = 0 ;
};

//***********************************************************************
void UserInput ::InitFds (){
  //if no user input, check for input on pipes
  //on first call, establish the fd's. They will stay
  if ( 0 == in_fd )
  {
    in_fd = new int [ inputCnt + 1 ];//provide for debug at the end
    if ( in_fd ){
      in_fde = in_fd + inputCnt ;
      int* i ;
      const char** n = inputs ;
      for ( i = in_fd ; i != in_fde ; i ++ ){
		  // printf("open the devices\n");
	*i = open ( *n , O_RDONLY /*| O_NONBLOCK */);
	//	printf ("%d\n",*i);
	n ++ ;
      }
      //      * i = 0 ; in_fde ++ ; //add stdin for debugging

		// Set the Baud Rate for the Serial port to 9600 and set to RAW mode
      	int port = in_fd[inputCnt - 1];
      	if (-1 <= port)
		{
			struct termios attributes;

			if (-1 < tcgetattr (port,&attributes))
			{
				cfsetispeed(&attributes, B9600);
				cfmakeraw(&attributes);
				tcsetattr (port, TCSANOW, &attributes);
			}
		}
    }
  }
}

//***********************************************************************
void UserInput::ReleaseFds ()
{
 if (in_fd )
  {
	  in_fde = in_fd + inputCnt ;
      int* i ;
      const char** n = inputs ;
      for ( i = in_fd ; i != in_fde ; i ++ )
	  {
	     //printf("close the devices\n");
		 close (*i);
		 *i = -1;
	     n ++ ;
      }
	  delete in_fd;
	  in_fd = 0;

 }
}
//***********************************************************************
int UserInput ::PrepSet (){
  int mx = -1 ;
  FD_ZERO ( & set );
  for ( int* i = in_fd ; i != in_fde ; i ++ ){
    if ( *i >= 0 ){
      FD_SET ( * i , & set );
      //figure out the max FD. Increment by one, as select requires it
      mx = mx < *i ? *i : mx ;
      //	printf("mx: %d\n",mx);
    }
  }
  return mx ;
}

//***********************************************************************
int UserInput ::DoSelect ( int mx ){
  //run a select on the FDSET
  timeval tv ;
  tv .tv_sec = 0;
  tv .tv_usec = 100000 ;
  //printf("m1\n");
  int res = select ( mx + 1 , & set , 0 , 0 , &tv );
 // printf("m2 res = %d\n",res);
  return res ;
}

//***********************************************************************
int UserInput ::ReadFile ( int fd ){
  //printf("m3\n");
  if ( fd < 0 || ! FD_ISSET ( fd , & set )){
    return 0 ;
  }

  //printf("m4 %d\n", fd );
  int res = read ( fd , buffer , sizeof ( buffer ) - 2 );
  //printf("\n buffer contains - %s", buffer);
  #ifdef DEBUG
  	printf("\n *** badge swiped  !!! ****");
  	printf("\n res = %d", res);
  #endif
   //res = -1 sometimes when badge is swiped fast multiple times..
  //if ( 0 == res ){
	if ( 0 >= res ){
    return 0 ;
  }

  //char logstring[300];
  //sprintf(logstring, "Badge swiped, buffer contains - %s ", buffer);
  //logger->info(logstring);

  bufP = buffer ;
  #ifdef DEBUG
  	printf("\n buffer contains - %s", buffer);
  	printf("\n bufP = %d", bufP);
  #endif
  bufEnd = bufP + res ;
  #ifdef DEBUG
  	printf("\n bufEnd = %d", bufEnd);
  #endif

  *bufEnd = 0 ;
  #ifdef DEBUG
    printf("\n buffer with res=%d, contains - %s\n", res, buffer);
  #endif
  //  printf("m4a %d\n" , res );
  //  printf("m5 %d %d %s", fd, res , buffer );
  return res ;
}

//***********************************************************************
int UserInput ::ReadBuffer ( char& c ){
  if ( bufP != bufEnd){
    c = *bufP ;
    bufP ++ ;
    return 1 ;
  } else {
    return 0 ;
  }
}
//***********************************************************************
//This method deletes the data from the buffer
void UserInput ::ClearBuffer ( ){

  while ( bufP != bufEnd){

    bufP ++ ;
  }

}

//***********************************************************************
//This method flushes all the data from all the pipes. This data is not yet read into buffer.
//This method will be called to throw away the subsequent badge swipes until next asked for.
//Only one badge will be buffered at a time.

void UserInput :: Flush () {

	InitFds ();
    if ( 0 == in_fd ){
      return ;
    }

    //prpeare an FDSET with all input fds
    int mx = PrepSet ();
    if ( -1 == mx ){
      return ;
    }

    //don't attempt select if no file descriptor present

    int res = DoSelect ( mx );
    if ( res > 0 ){
      //if more than one pipe is ready, last one wins.
       for ( int* i = in_fd, j = 0 ; i != in_fde ; i ++, j++ ){
      	if ( ReadFile ( * i ) > 0 )
			ClearBuffer();

		}
 	  }

 	  TemplateScanReady = false;
 	  PartialScanReady = false;
 	  ScanFailed = false;
}

//***********************************************************************
UserInput ::status_t UserInput ::GetChar ( char& c,  UserInput ::source_t & source ){
  //20051109 if buffered data available, return those
  if ( ReadBuffer ( c ) > 0 ){
	source = source_temp;
    return OK ;
  }

  //try reading until time out. acctime keeps track of the spent time
  unsigned int acctime = 0 ;
  while ( true )
  {
    //check for user input.
    if ( user_input_ready ()){
      source = UserInput ::KEYBOARD;
      c = user_input_get ();
      return OK ;
    }
    //printf("GetChar1\n");

    if( TemplateScanReady == true) {
	    source = UserInput ::OTHER;
		c = '*';
		//reset the flag
		TemplateScanReady = false;
		return OK;
	}

	if( PartialScanReady == true) {
	    source = UserInput ::OTHER;
		c = '#';
		//reset the flag
		PartialScanReady = false;
		return OK;
	}
	//printf("GetChar3\n");
	if( ScanFailed == true) {
	    source = UserInput ::OTHER;
		c = '@';
		//reset the flag
		ScanFailed = false;
		return OK;
	}
    InitFds ();
    if ( 0 == in_fd ){
      return FAIL ;
    }

    //prpeare an FDSET with all input fds
    int mx = PrepSet ();
    if ( -1 == mx ){
      return FAIL ;
    }
    //don't attempt select if no file descriptor present
    bool success ;
    success = false ;

    int res = DoSelect ( mx );
    if ( res > 0 ){
      //if more than one pipe is ready, last one wins.
  for ( int* i = in_fd, j = 0 ; i != in_fde ; i ++, j++ )
  {
	if ( ReadFile ( * i ) > 0 )
	{
	  if ( ReadBuffer ( c ) > 0 )
	  {
		  switch (j)
		  {
			  case 0 :
			  		source = UserInput ::PROX;
			  		break;
			  case 1 :
				   source = UserInput ::MAG;
			  		break;
			  case 2 :
			  		source = UserInput ::MAG;
			  		break;
			  case 3 :
			  		source = UserInput ::MAG;
			  		break;
			  case 4 :
			  		source = UserInput ::BARCODE;
			  		break;
		   }
		source_temp = source;
	    success = true ;
	  }
	}
   }
  }
   	if(fpOpr.FpScanValid)
	{
	  char UID[11];
	  sprintf(UID,"%d",(int)fpOpr.FpUserID);
	  strcpy(buffer,UID);
      bufP = buffer ;
      bufEnd = bufP + strlen(UID);
      *bufEnd = 0 ;
	  source = UserInput ::FPRINT;
      source_temp = source;
      success = true ;
      fpOpr.FpScanValid = false;
	  fpOpr.ScanCount == 0;
	}
    if ( success ){
     // printf("\n buffer II contains - %s", bufP);
	  //ReleaseFds ();
      return OK ;
    }

    //if short timeout, repeat until class timeout is reached
    acctime += 100000 ;
	//printf("acctime = %d timeout_us = %d\n ",acctime,timeout_us);
    if ( acctime > timeout_us ){
      //  ReleaseFds ();
		return TIMEOUT ;
    }
  }
}
//***********************************************************************



