/*
	UserVisual:  [Module]

    The UserVisual module provides routines to control TA7000 data
collection terminal visula indictors (LEDs).

	Copyright (c) 2005 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   31 May 2005
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime$
	     $Date: 2005/09/01 22:27:58 $

*/


/* Compilation options for UserVisual. */
#define DEBUG		1
#undef DEBUG

/* Include files for UserVisual. */
#include <StdIO.h>
#include <FCntl.h>
#include <UniStd.h>
#include <sys/IOCtl.h>
#include <asm/TA7000_IO.h>
#include "Gen_Def.h"
#include "User.h"

/* Debug global defines. */
#ifdef DEBUG
	#define PRINTD(fmt, args...) printf("%s: " fmt, __func__, ## args)
#else
	#define PRINTD(fmt, args...)
#endif

/* Constant parameters for UserVisual. */
#define USER_VISUAL_DEVICE		"/dev/" TA7000_DRIVER_PATH_VISUAL
#define USER_VISUAL_MAX			 3

/* Type and structure definitions for UserVisual. */
typedef struct {
	int file;
	}
UserVisual_t;

/* Internal static data for UserVisual. */
static UserVisual_t _uv = {
	.file = -1,
	};
#ifdef DEBUG
static const char * _light_names [USER_VISUAL_MAX] = {
	"Red",
	"Yellow",
	"Green",
};
#endif

/* External global data for UserVisual. */

/* Function prototypes for UserVisual. */



/*
	user_visual_init:

	Initalize the visual output module.

Description:

	Open visual output device and set up for visual output.

See also:  user_visual_set

Arguments:
	void

Returns:
	int		>= 0 if successful, value is file handle;
			< 0 if an error occurred.

*/

		int
user_visual_init (void)

{

	PRINTD ("Initializing user visual outputs module.\n");

	_uv.file = open (USER_VISUAL_DEVICE, O_RDWR);

	return (_uv.file);
}


/*
	user_visual_close:

    Close the visual output module.

Description:

    Close the visual output device file.

See also:  user_visual_set

Arguments:
	void

Returns:
	int		>= 0 if successful, value is file handle;
			< 0 if an error occurred.

*/

		void
user_visual_close (void)

{

	PRINTD ("Closing user visual outputs module.\n");
	if (_uv.file >= 0) {
		close (_uv.file);
	}

	return;
}


/*
	user_visual_set:

	Set an indicator light output.

Description:

	Set the output state for the specified indicator light to the On or Off.								

See also:  user_visual_init

Arguments:
	light		USER_VISUAL_RED (1) for Red LED;
				USER_VISUAL_YEL (2) for Yellow LED;
				USER_VISUAL_GRN (3) for Green LED;
	
	state		0 set light Off;
				non-zero set light On.
																							
Returns:
	int			0 if successful;
				<0 if an invalid light is requested or an error occurs.

*/

		int
user_visual_set (int light, int state)

{
	int status = 0;

	PRINTD ("Set %s light %s.\n", _light_names[light-1], state ? "ON" : "OFF");

	if (light > 0 AND light <= USER_VISUAL_MAX) {
		status = ioctl (_uv.file, VISUAL_OUT_IOC_W_BASE + (light-1),
						(unsigned long)state);			
	}
	else status = -1;

	return (status);
}


