/*
	UserDigital:  [Module]

    The UserDigital module provides routines to control NE7000 data
collection terminal external digital outputs.

	Copyright (c) 2005
	Alan J. Luse <AlanL@TimeAmerica.com>, TimeAmerica, Inc.

	Written by:   Alan J. Luse
	Written on:   01 Dec 2005
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime$
	     $Date: 2005/12/06 15:52:43 $

*/


/* Compilation options for UserDigital. */
#define DEBUG			0

/* Include files for UserDigital. */
#include <StdIO.h>
#include <FCntl.h>
#include <UniStd.h>
#include <sys/IOCtl.h>
#include <asm/NE7000_IO.h>
#include "Gen_Def.h"
#include "User.h"

/* Debug global defines. */
#if defined DEBUG && DEBUG > 0
	#define debugF(fmt, args...) printf("%s: " fmt, __func__, ## args)
#else
	#define debugF(fmt, args...)
#endif

/* Constant parameters for UserDigital. */
#define USER_DIGITAL_DEVICE		"/dev/" NE7000_DRIVER_PATH_DIGITAL

/* Type and structure definitions for UserDigital. */
typedef struct {
	int file;
	}
UserDigital_t;

/* Internal static data for UserDigital. */
static UserDigital_t _uv = {
	.file = -1,
	};
#if defined DEBUG && DEBUG > 0
static const char * _output_names [DIGITAL_OUT_MAX] = {
	"Ext1",
	"Ext2",
	"Ext3",
	"Ext4",
};
#endif

/* External global data for UserDigital. */

/* Function prototypes for UserDigital. */



/*
	user_digital_init:

    Initalize the digital output module.

Description:

    Open digital output device and set up for digital output.

See also:  user_digital_set

Arguments:
	void

Returns:
	int		>= 0 if successful, value is file handle;
			< 0 if an error occurred.

*/

		int
user_digital_init (void)

{

	debugF ("Initializing user digital outputs module.\n");

	_uv.file = open (USER_DIGITAL_DEVICE, O_RDWR);

	return (_uv.file);
}


/*
	user_digital_close:

    Close the digital output module.

Description:

    Close the digital output device file.

See also:  user_digital_set

Arguments:
	void

Returns:
	int		>= 0 if successful, value is file handle;
			< 0 if an error occurred.

*/

		void
user_digital_close (void)

{

	debugF ("Closing user digital outputs module.\n");
	if (_uv.file >= 0) {
		close (_uv.file);
	}

	return;
}


/*
	user_digital_set:

    Set an external digital output.

Description:

    Set the output state for the specified external digital output to the
On or Off.

See also:  user_digital_init

Arguments:
	output		1-4 for respective digital output.
	
	state		0 set output Off;
			non-zero set output On.
																							
Returns:
	int		0 if successful;
			<0 if an invalid output is requested or an error occurs.

*/

		int
user_digital_set (int output, int state)

{
	int status = 0;

	debugF ("Set %s output %s.\n", _output_names[output-1], state ? "ON" : "OFF");

	if (output > 0 AND output <= DIGITAL_OUT_MAX) {
		status = ioctl (_uv.file, DIGITAL_OUT_IOC_W_BASE + (output-1),
						(unsigned long)state);			
	}
	else status = -1;

	return (status);
}


/*
	user_digital_get:

    Get an external digital output's state.

Description:

    Get the output state of the specified external digital output.

See also:  user_digital_init

Arguments:
	output		1-4 for respective digital output.
																							
Returns:
	int		0 if output is Off;
			1 if output is On;
			<0 if an invalid output is requested or an error occurs.

*/

		int
user_digital_get (int output)

{
	int status = 0;

	if (output > 0 AND output <= DIGITAL_OUT_MAX) {
		status = ioctl (_uv.file, DIGITAL_OUT_IOC_R_BASE + (output-1),
						(unsigned long)0);			

		debugF ("Output %s is %s.\n", _output_names[output-1], state ? "ON" : "OFF");
	}
	else status = -1;

	return (status);
}


