#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <typeinfo>
#include <errno.h>
#include "LCDHandler.h"

/**
 * Reads a 10 character WEP Key.
 * Calling function needs to make sure 
 * that the buffer is of the the right length
 */
LCD_READ_STATE LCDHandler::Get64BitWEPKey(int row, int col, int timeout, char * buffer)
{
	static char disp[2];
	disp[1] = '\0';
	// 10 character long
	strcpy(buffer, "**********");
	user_output_write(row, col, buffer);
	strcpy(buffer, "0000000000");
	int numOfChars = 0;
	char input;
	user_output_cursor(USER_CURSOR_BOTH);
	if(col > 1)
	{
		user_output_write(row, col - 1, BLANKSPACE);
	}
	else
	{
		user_output_write(row - 1, 20, BLANKSPACE);
	}
	while(true) // 10 Characters in HEX Format
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout) 
		{
			usleep(10000);
			tm += 20; 
		}
		if(tm >= timeout) 
		{
			oper.Beep(TYPE_TimeOut);
			user_output_cursor(USER_CURSOR_OFF);
			return READ_TIMEDOUT;
		}
		input = user_input_get ();
		if( input == '\n' ) 
		{
			oper.Beep(TYPE_KeyPress);
			break;
		}
		if( input == ESC ) 
		{
			oper.Beep(TYPE_KeyPress);
			user_output_cursor(USER_CURSOR_OFF);
			return READ_CLEAR;
		}
		switch(input)
		{
			case '+':
				oper.Beep(TYPE_KeyPress);
				disp[0] = buffer[numOfChars] = GetNextHexChar(buffer[numOfChars]);
				user_output_write(row, numOfChars + col, disp);
				if(numOfChars == 0)
				{
					if(col > 1)
					{
						user_output_write(row, col - 1, BLANKSPACE);
					}
					else
					{
						user_output_write(row - 1, 20, BLANKSPACE);
					}
				}
				else
				{
					disp[0] = buffer[numOfChars - 1];
					user_output_write(row, numOfChars + col - 1, disp);
				}
				break;
			case '-':
				oper.Beep(TYPE_KeyPress);
				disp[0] = buffer[numOfChars] = GetPrevHexChar(buffer[numOfChars]);
				user_output_write(row, numOfChars + col, disp);
				if(numOfChars == 0)
				{
					if(col > 1)
					{
						user_output_write(row, col - 1, BLANKSPACE);
					}
					else
					{
						user_output_write(row - 1, 20, BLANKSPACE);
					}
				}
				else
				{
					disp[0] = buffer[numOfChars - 1];
					user_output_write(row, numOfChars + col - 1, disp);
				}
				break;
			case '>':
				oper.Beep(TYPE_KeyPress);
				if(numOfChars < 9)
				{
					disp[0] = buffer[numOfChars];
					user_output_write(row, numOfChars + col, disp);
					numOfChars++;
				}
				break;
			case '<':
				oper.Beep(TYPE_KeyPress);
				if(numOfChars > 0)
				{
					if(numOfChars == 1)
					{
						if(col > 1)
						{
							user_output_write(row, numOfChars + col - 2, BLANKSPACE);
						}
						else
						{
							user_output_write(row - 1, 20, BLANKSPACE);
						}
					}
					else
					{
						disp[0] = buffer[numOfChars - 2];	
						user_output_write(row, numOfChars + col - 2, disp);
					}
					numOfChars--;
				}
				break;
			default:
				if( (input < '0' || input > '9') ) 
				{
					oper.Beep(150, TYPE_Reject);
					continue;	
				}
				else
				{
					oper.Beep(TYPE_KeyPress);
					disp[0] = buffer[numOfChars] = input;
					user_output_write(row, numOfChars + col, disp);
					if(numOfChars == 9)
					{
						disp[0] = buffer[numOfChars - 1];
						user_output_write(row, numOfChars + col - 1, disp);
					}
					if(numOfChars < 9) numOfChars++;
				}
				break;
		}
	}
	user_output_cursor(USER_CURSOR_OFF);
	return READ_SUCCESS;
}

/**
 * Reads a 26 character WEP Key.
 * Calling function needs to make sure 
 * that the buffer is of the the right length
 */
LCD_READ_STATE LCDHandler::Get128BitWEPKey(int row, int col, int timeout, char * buffer)
{
	static char disp[2];
	disp[1] = '\0';
	
	/**
	 * 26 character long. It cannot fit in one line, 
	 * hence we will create two buffers
	 */
	char buffer1[21]; // Line 1
	char buffer2[7]; // Line 2
	strcpy(buffer1, "********************");
	strcpy(buffer2, "******");
	user_output_write(row, 0, buffer1);
	user_output_write(row + 1, 0, buffer2);
	strcpy(buffer1, "00000000000000000000");
	strcpy(buffer2, "000000");
	int numOfChars = 0;
	char input;
	user_output_cursor(USER_CURSOR_BOTH);
	if(col > 1)
	{
		user_output_write(row, col - 1, BLANKSPACE);
	}
	else
	{
		user_output_write(row - 1, 20, BLANKSPACE);
	}
	while(true) // 26 Characters in HEX Format
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout) 
		{
			usleep(10000);
			tm += 20; 
		}
		if(tm >= timeout) 
		{
			oper.Beep(TYPE_TimeOut);
			user_output_cursor(USER_CURSOR_OFF);
			return READ_TIMEDOUT;
		}
		input = user_input_get();
		if(input == '\n') 
		{
			oper.Beep(TYPE_KeyPress);
			break;
		}
		if(input == ESC) 
		{
			oper.Beep(TYPE_KeyPress);
			user_output_cursor(USER_CURSOR_OFF);
			return READ_CLEAR;
		}
		switch(input)
		{
			case '+':
				oper.Beep(TYPE_KeyPress);
				if(numOfChars < 20)
				{
					disp[0] = buffer1[numOfChars] = GetNextHexChar(buffer1[numOfChars]);	
					user_output_write(row, numOfChars + 1, disp);
				}
				else
				{
					disp[0] = buffer2[numOfChars - 20] = GetNextHexChar(buffer2[numOfChars - 20]);	
					user_output_write(row + 1, numOfChars - 19, disp);
				}
				if(numOfChars == 0)
				{
					if(col > 1)
					{
						user_output_write(row, col - 1, BLANKSPACE);
					}
					else
					{
						user_output_write(row - 1, 20, BLANKSPACE);
					}
				}
				else if(numOfChars == 20)
				{
					disp[0] = buffer1[19];
					user_output_write(row, 20, disp);
				}
				else if(numOfChars < 20)
				{
					disp[0] = buffer1[numOfChars - 1];
					user_output_write(row, numOfChars, disp);
				}
				else
				{
					disp[0] = buffer2[numOfChars - 20 - 1];
					user_output_write(row + 1, numOfChars - 20, disp);
				}
				break;
			case '-':
				oper.Beep(TYPE_KeyPress);
				if(numOfChars < 20)
				{
					disp[0] = buffer1[numOfChars] = GetPrevHexChar(buffer1[numOfChars]);	
					user_output_write(row, numOfChars + 1, disp);
				}
				else
				{
					disp[0] = buffer2[numOfChars - 20] = GetPrevHexChar(buffer2[numOfChars - 20]);	
					user_output_write(row + 1, numOfChars - 19, disp);
				}
				if(numOfChars == 0)
				{
					if(col > 1)
					{
						user_output_write(row, col - 1, BLANKSPACE);
					}
					else
					{
						user_output_write(row - 1, 20, BLANKSPACE);
					}
				}
				else if(numOfChars == 20)
				{
					disp[0] = buffer1[19];
					user_output_write(row, 20, disp);
				}
				else if(numOfChars < 20)
				{
					disp[0] = buffer1[numOfChars - 1];
					user_output_write(row, numOfChars, disp);
				}
				else
				{
					disp[0] = buffer2[numOfChars - 20 - 1];
					user_output_write(row + 1, numOfChars - 20, disp);
				}
				break;
			case '>':
				oper.Beep(TYPE_KeyPress);
				if(numOfChars < 25)
				{
					if(numOfChars < 20)
					{
						disp[0] = buffer1[numOfChars];
						user_output_write(row, numOfChars + 1, disp);
					}	
					else
					{
						disp[0] = buffer2[numOfChars - 20];
						user_output_write(row + 1, numOfChars - 19, disp);	
					}
					numOfChars++;
				}
				break;
			case '<':
				oper.Beep(TYPE_KeyPress);
				if(numOfChars > 0)
				{
					if(numOfChars == 1)
					{
						if(col > 1)
						{
							user_output_write(row, col - 1, BLANKSPACE);
						}
						else
						{
							user_output_write(row - 1, 20, BLANKSPACE);
						}
					}
					else if(numOfChars == 20)
					{
						disp[0] = buffer1[18];
						user_output_write(row, 19, disp);
					}
					else if(numOfChars == 21)
					{
						disp[0] = buffer1[19];
						user_output_write(row, 20, disp);
					}
					else if(numOfChars < 20)
					{
						disp[0] = buffer1[numOfChars - 2];
						user_output_write(row, numOfChars + col - 1, disp);
					}
					else
					{
						disp[0] = buffer2[numOfChars - 20 - 2];
						user_output_write(row + 1, numOfChars - 21, disp);
					}
					numOfChars--;
				}
				break;
			default:
				if( (input < '0' || input > '9') ) 
				{
					oper.Beep(150, TYPE_Reject);
					continue;	
				}
				else
				{
					oper.Beep(TYPE_KeyPress);
					if(numOfChars < 20)
					{
						disp[0] = buffer1[numOfChars] = input;
						user_output_write(row, numOfChars + 1, disp);
					}	
					else
					{
						disp[0] = buffer2[numOfChars - 20] = input;
						user_output_write(row + 1, numOfChars - 19, disp);	
					}
					if(numOfChars == 25)
					{
						disp[0] = buffer2[4];
						user_output_write(row + 1, 5, disp);	
					}
					if(numOfChars < 25) numOfChars++;
				}
				break;
		}
	}
	strcpy(buffer, buffer1);
	strcat(buffer, buffer2);
	user_output_cursor(USER_CURSOR_OFF);
	return READ_SUCCESS;
}

