#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <typeinfo>
#include <errno.h>
#include "LCDHandler.h"
#include "UserInput.hpp"

/**
 * this file contains all code related to LCD Display
 * 1 - 	Cursors - The cursor code is not really clean. The only interface
 * 		for cursor right now is just to turn the cursor on and off. There is
 * 		no API to place the cursor in a particular location. To solve this,
 * 		each time we need a cursor movement, we write the character previous to
 * 		the desired location. Its not a very clean way but it works
 * 2 - 	The LCDHandler class's implementation is split in two other files
 * 		1 - LCDWEPHandler.cpp - Has the WEP Code
 * 		2 - LCDUtil.cpp - Has utility functions
 *
 */

LCDHandler::LCDHandler()
{
}

LCDHandler::~LCDHandler()
{
}

/**
 * Reads Date from Keyboard.
 * Calling Function needs to meake sure that the buffer length
 * is sufficient to hold the date
 */
LCD_READ_STATE LCDHandler::ReadDate(int row, int col, char * buffer)
{
  if(row < 1 || row > 4) row = 1;
  if(col < 1 || col > 20) col = 1;
  strcpy(buffer, oper.GetSystemDate());
  int timeout = oper.GetDataTimeout();
  return ReadData(SysInputTypeNumeric, row, col, timeout, 8, 0, buffer);
}

LCD_READ_STATE LCDHandler::ReadTime(int row, int col, char * buffer)
{
  if(row < 1 || row > 4) row = 1;
  if(col < 1 || col > 20) col = 1;
  strcpy(buffer, oper.GetSystemTime());
  // GetSystemTime() return time in HHMMSS, we need only HHMM
  buffer[4] = '\0';
  int timeout = oper.GetDataTimeout();
  return ReadData(SysInputTypeNumeric, row, col, timeout, 4, 0, buffer);
}

LCD_READ_STATE LCDHandler::ReadData(SysInputType_e type, int row,
				    int col, int timeout, int maxChars,
				    int min, char * buffer,char* defaultData)
{
  static char disp[2];
  disp[1] = '\0';
  user_output_write(row, col, buffer);
  char input;
  int numOfChars = 0;
  bool firstESC = true;
  user_output_cursor(USER_CURSOR_BOTH);
  if(col > 1)
    {
      user_output_write(row, numOfChars + col - 1, BLANKSPACE);
    }
  else
    {
      user_output_write(row - 1, 20, BLANKSPACE);
    }


  if(defaultData != NULL && strcmp(defaultData,"") != 0)
  {
	  snprintf(buffer,maxChars+1,"%s",defaultData);
      numOfChars = strlen(buffer);
	  user_output_write(row,col, buffer);
	  disp[0] = buffer[numOfChars-2];disp[1] = '\0';
      user_output_write(row, numOfChars + col-2,disp);
  }

  while(true)
  {
      int tm = 0;
      while (!user_input_ready () && tm < timeout)
	{
	  usleep(10000); // Sleep for 10ms
	  tm += 20;
	}
      if(tm >= timeout)
	{
	  oper.Beep(TYPE_TimeOut);
	  user_output_cursor(USER_CURSOR_OFF);
	  return READ_TIMEDOUT;
	}
      input = user_input_get();
      if( input == '\n' )
	{
      buffer[numOfChars] = '\0';
	  oper.Beep(TYPE_KeyPress);
	  break;
	}
      if( input == ESC )
	  {
         if(firstESC && numOfChars > 0)
		 {
			numOfChars = 0 ;
			buffer[0] = '\0';
			user_output_write(row, 1, "                   ");
			user_output_write(row, col, buffer);
			firstESC = false;
			continue;
	     }
		 else
		 {
			oper.Beep(TYPE_KeyPress);
			user_output_cursor(USER_CURSOR_OFF);
			return READ_CLEAR;
		 }
	  }
      switch(input)
	{
	case '>':
	  oper.Beep(TYPE_KeyPress);
	  if(numOfChars < maxChars - 1)
	    {
	      disp[0] = buffer[numOfChars];
	      user_output_write(row, numOfChars + col, disp);
	      numOfChars++;
	    }
	  break;
	case '<':
	  oper.Beep(TYPE_KeyPress);
	  if(numOfChars > 0)
	    {
	      if(numOfChars == 1)
		{
		  if(col > 1)
		    {
		      user_output_write(row, numOfChars + col - 2, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 2];
		  user_output_write(row, numOfChars + col - 2, disp);
		}
	      numOfChars--;
	    }
	  break;
	case 'C':
	  if( buffer[numOfChars] >= 'A' && buffer[numOfChars] <= 'Z' )
	    {
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] += 32;
	      user_output_write(row, numOfChars + col, disp);
	    }
	  else if( buffer[numOfChars] >= 'a' && buffer[numOfChars] <= 'z' )
	    {
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] -= 32;
	      user_output_write(row, numOfChars + col, disp);
	    }
	  else
	    {
	      oper.Beep(150, TYPE_Reject);
	    }
	  if(numOfChars == 0)
	    {
	      if(col > 1)
		{
		  user_output_write(row, 	numOfChars + col - 1, BLANKSPACE);
		}
	      else
		{
		  user_output_write(row - 1, 	20, BLANKSPACE);
		}
	    }
	  else
	    {
	      disp[0] = buffer[numOfChars - 1];
	      user_output_write(row, numOfChars + col - 1, disp);
	    }
	  break;
	case '+':
	  switch(type)
	    {
	    case SysInputTypeAlphaNumeric:
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] = GetNextAlphaNumericChar(buffer[numOfChars]);
	      user_output_write(row, numOfChars + col, disp);
	      if(numOfChars == 0)
		{
		  if(col > 1)
		    {
		      user_output_write(row, 	numOfChars + col - 1, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 	20, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars + col - 1, disp);
		}
	      break;
	    case SysInputTypeAlpha:
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] = GetNextAlphaChar(buffer[numOfChars]);
	      user_output_write(row, numOfChars + col, disp);
	      if(numOfChars == 0)
		{
		  if(col > 1)
		    {
		      user_output_write(row, 	numOfChars + col - 1, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 	20, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars + col - 1, disp);
		}
	      break;
	    case SysInputTypeNumeric:
	    case SysInputTypeDefault:
	    case SysInputTypeDecimal:
	      oper.Beep(150, TYPE_Reject);
	      break;
	    }
	  break;
	case '-':
	  switch(type)
	    {
	    case SysInputTypeAlphaNumeric:
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] = GetPrevAlphaNumericChar(buffer[numOfChars]);
	      user_output_write(row, numOfChars + col, disp);
	      if(numOfChars == 0)
		{
		  if(col > 1)
		    {
		      user_output_write(row, 	numOfChars + col - 1, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 	20, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars + col - 1, disp);
		}
	      break;
	    case SysInputTypeAlpha:
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] = GetPrevAlphaChar(buffer[numOfChars]);
	      user_output_write(row, numOfChars + col, disp);
	      if(numOfChars == 0)
		{
		  if(col > 1)
		    {
		      user_output_write(row, 	numOfChars + col - 1, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 	20, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars + col - 1, disp);
		}
	      break;
	    case SysInputTypeNumeric:
	    case SysInputTypeDefault:
	    case SysInputTypeDecimal:
	      oper.Beep(150, TYPE_Reject);
	      break;
	    }
	  break;
	default:
	  switch(type)
	    {
	    case SysInputTypeNumeric:
	    case SysInputTypeAlphaNumeric:
	      if( (input < '0' || input > '9') )
		{
		  oper.Beep(150, TYPE_Reject);
		  continue;
		}
		  if( numOfChars < maxChars) numOfChars++;
		  firstESC = true;
	      oper.Beep(TYPE_KeyPress);
	      buffer[numOfChars-1] = input;
	      disp[0] = input;
	      user_output_write(row, numOfChars + col-1, disp);
	      if(numOfChars == maxChars )
		  {
		   disp[0] = buffer[numOfChars - 2];
		   user_output_write(row, numOfChars + col - 2, disp);
		  }
	      break;
	    case SysInputTypeAlpha:
	      oper.Beep(150, TYPE_Reject);
	      continue;
	    default:
	      continue;
	    }
	}
    }
  // Remove empty spaces if any
  int len = strlen(buffer);
  for(int i=0; i<len; i++)
    {
      if(buffer[i] == ' ') buffer[i] = '\0';

    }
  user_output_cursor(USER_CURSOR_OFF);
  return READ_SUCCESS;
}

/**
 * Please make sure the buffer is of right
 * length before calling this function
 */
LCD_READ_STATE LCDHandler::ReadDecimal(int row, int col, int timeout,
				       int maxChars, int min, int decimal, char * buffer,char* defaultData )
{

  int commaIndxBuf , numCharsUpdate ;
  int cursorIndx;
  // If the decimal then its just numeric
  if( decimal <= 0 )
  {
    return ReadData(SysInputTypeNumeric, row, col, timeout, maxChars, min, buffer,defaultData);
  }

  static char disp[2];
  disp[1] = '\0';
  int indexCal = maxChars;

  if(maxChars <= decimal)
  {
      for(int i = 0; i<decimal + 2; i++) buffer[i] = '0';
      buffer[1] = '.';
      commaIndxBuf = 1;
      buffer[decimal + 2] = '\0';
      indexCal = decimal + 1;
  }
  else
  {
      for(int i=0; i<maxChars+1; i++) buffer[i] = ' ';
      buffer[maxChars - decimal - 1] = '0';
      buffer[maxChars - decimal] = '.';
      commaIndxBuf = maxChars - decimal;
      for(int i=maxChars - decimal + 1; i <= maxChars; i++) buffer[i] = '0';
      buffer[maxChars + 1] = '\0';
  }


  int buflen = strlen(buffer);
  char orig[buflen + 1];
  strcpy(orig, buffer);
  col = 12 - buflen + 1;
  cursorIndx = -1;
  char input;
  int numOfChars = 0;
  bool clear = false;

  if(defaultData != NULL && strcmp(defaultData,"") != 0)
  {
	  char buftemp[20];
	  strcpy(buftemp,defaultData);

	  if(maxChars <= decimal)
	  {
		if(buftemp[commaIndxBuf] == '.')
		{
			snprintf(buffer,maxChars+1+1,"%s",defaultData);
			//strncpy(buffer,defaultData,maxChars+1);
			numOfChars = strlen(buffer)-1;
			user_output_write(row,col, buffer);
			disp[0] = buffer[numOfChars-1];disp[1] = '\0';
			user_output_write(row, numOfChars + col-1,disp);
		}
	  }
	  else
	  {
		 int commaIndxDefault = 0;
         for(int i=0; i<strlen(buftemp); i++)
		     if(buftemp[i] == '.') commaIndxDefault = i;

         if(commaIndxDefault)
		 {
			for(int i=commaIndxDefault + 1; i < (int)strlen(buftemp); i++)
			 if(commaIndxBuf<(commaIndxBuf + (i-commaIndxDefault)) && (commaIndxBuf + (i-commaIndxDefault))<maxChars+1)
			 {
				 buffer[commaIndxBuf + (i-commaIndxDefault)] = buftemp[i];
				 numOfChars++;
			 }
			for(int i=commaIndxDefault -1 ; i >= 0; i--)
			 if(0 <= (commaIndxBuf - (commaIndxDefault-i)) && (commaIndxBuf - (commaIndxDefault-i))<commaIndxBuf)
			 {
				 buffer[commaIndxBuf - (commaIndxDefault-i)] = buftemp[i];
                 numOfChars++;
			 }
		 }
	  }
  }

  for(;;)
  {
      //disp[0] = buffer[buflen-(numOfChars+2)];
     if(cursorIndx > 1)
     {
        disp[0] = buffer[cursorIndx-col-1];
        user_output_write(row, col, buffer);
        user_output_write(row,cursorIndx-1,disp);
        user_output_cursor(USER_CURSOR_BOTH);
     }
     else
     {
        user_output_write(row, col, buffer);
     }
      clear = false;


      while(true)
	{
	  int tm = 0;
	  while (!user_input_ready () && tm < timeout)
	  {
	      usleep(10000); // Sleep for 10ms
	      tm += 20;
	  }
	  if(tm >= timeout)
	  {
	      user_output_cursor(USER_CURSOR_OFF);
	      oper.Beep(TYPE_TimeOut);
	      return READ_TIMEDOUT;
	  }
	  input = user_input_get ();
	  if( input == '\n' )
	  {
	      oper.Beep(TYPE_KeyPress);
	      break;
	  }
	  if( input == ESC )
	  {

	      if(cursorIndx < 0)
	      {
	        oper.Beep(TYPE_KeyPress);
	        clear = true;
			numOfChars = 0;
	        break;
	      }
          else
          {
            oper.Beep(TYPE_KeyPress);
            user_output_cursor(USER_CURSOR_OFF);
            cursorIndx = -1;
			numOfChars = 0;
            continue;
          }

	  }
	  switch(input)
	  {
	    case '>':

	        if(cursorIndx == -1)
	        {
	                oper.Beep(TYPE_KeyPress);
	                cursorIndx = col+buflen-1;
	        }

	        else if(cursorIndx + 1 <= (col+buflen-1) )
	              {
	                oper.Beep(TYPE_KeyPress);
	                if(buffer[cursorIndx-col+1] == '.')
	                       cursorIndx = cursorIndx+2;
	                else
	                       cursorIndx++;
	               }
	               else
	               {
	                 oper.Beep(150, TYPE_Reject);
	                 continue;
	               }
	                if(cursorIndx-col-1 < buflen-1)
	                    disp[0] = buffer[cursorIndx-col-1];
	                else
	                    disp[0] = ' ';

                    (cursorIndx-col > commaIndxBuf)?numCharsUpdate=(col-(cursorIndx-buflen))
	                                      :numCharsUpdate=(col-(cursorIndx-buflen))-1;
                   // printf("numOfChars = %d numCharsUpdate = %d commaIndxBuf = %d \n",numOfChars,numCharsUpdate,commaIndxBuf);
                    if(numOfChars <= numCharsUpdate && cursorIndx-col -1 < commaIndxBuf-1)
                    {

                        if(cursorIndx-col-1 >= 0){
                            buffer[cursorIndx-col-1]=' ';
                            //user_output_write(row, col, buffer);
                            disp[0] = ' ';
                        }
                    }
                    user_output_write(row,cursorIndx-1,disp);
                    user_output_cursor(USER_CURSOR_BOTH);
                    //printf("col = %d cursorIndx = %d buflen = %d \n",col,cursorIndx,buflen);
                    continue;
	    break;
	    case '<':


	            if(cursorIndx == -1)
	            {
	               oper.Beep(TYPE_KeyPress);
	               cursorIndx = col+buflen-1;
	            }

	            else if(cursorIndx -1 >= col)
	                {
	                    oper.Beep(TYPE_KeyPress);
	                    if(buffer[cursorIndx-col-1] == '.')
	                        cursorIndx = cursorIndx-2;
	                    else
	                         cursorIndx--;
	                }
	                else
	                {
	                    oper.Beep(150, TYPE_Reject);
	                    continue;
	                }
	                if(cursorIndx-col-1 >= 0)
	                    disp[0] = buffer[cursorIndx-col-1];
	                else
	                    disp[0] = ' ';
                    if(buffer[cursorIndx-col] == ' ')
                    {
                        buffer[cursorIndx-col]='0';
                        user_output_write(row, col, buffer);
                    }
                    user_output_write(row,cursorIndx-1,disp);
                    user_output_cursor(USER_CURSOR_BOTH);
                   // printf("col = %d cursorIndx = %d buflen = %d \n",col,cursorIndx,buflen);
                    continue;
	    break;
	    default:;
	  }
	  if( (input < '0' || input > '9') )
	  {
	      oper.Beep(150, TYPE_Reject);
	      continue;
	  }

	  oper.Beep(TYPE_KeyPress);
	  if( numOfChars == maxChars && cursorIndx < 0 )
	  {
	      continue;
	  }
	  else
	  {


	    if(cursorIndx >=0)
	    {
	      buffer[cursorIndx-col] = input;

	      (cursorIndx-col > commaIndxBuf)?numCharsUpdate=(col-(cursorIndx-buflen))
	                                      :numCharsUpdate=(col-(cursorIndx-buflen))-1;

	      if(numCharsUpdate > numOfChars) numOfChars=numCharsUpdate;

	      //printf("num CHars = %d",numOfChars);
	      if(cursorIndx-col-1 >= 0)
	            disp[0] = buffer[cursorIndx-col-1];
	      else
	            disp[0] = ' ';
	      user_output_write(row, col, buffer);
	      user_output_write(row,cursorIndx-1,disp);
          user_output_cursor(USER_CURSOR_BOTH);

          continue;
	    }
	    for(int i=0; i < (int)strlen(buffer) - 1; i++)
		{
		  if( buffer[i] == '.' ) continue;
		  else if( buffer[i + 1] == '.' ) buffer[i] = buffer[i + 2];
		  else if( buffer[i] == ' ' && buffer[i+1] == '0' ) continue;
		  else buffer[i] = buffer[i + 1];
		}
	      buffer[indexCal] = input;
          user_output_write(row, col, buffer);
          numOfChars++;
	 }
	}
      if(!clear) break;
      else
	{
	  if(strcmp(buffer, orig) == 0)
	    {
	      return READ_CLEAR;
	    }
	  else
	    {
	      strcpy(buffer, orig);
	    }
	}
    }
  // Strip down white spaces in the front
  char temp[strlen(buffer) + 1];
  strcpy(temp, buffer);
  char * tmp = temp;
  while(*tmp == ' ') tmp++;
  strcpy(buffer, tmp);
  // if we accept less than the expected chars ignore the input chars
  //printf("numOfChars = %d\n",numOfChars);
  if( numOfChars < min )
    buffer[0]= '\0';
  user_output_cursor(USER_CURSOR_OFF);
  return READ_SUCCESS;
}

LCD_READ_STATE LCDHandler::DisplayTransaction(Trans * trans, int timeout, char * key)
{
  static char heading[128];
  static char transdata[128];
  static char line1[21];
  static char line2[21];
  line1[20] = line2[20] = '\0';
  int tm =0, hlen = 0, dlen = 0, curIndex = 0;

  // Build the Heading and Data

  // Date
  strcpy(heading, "DATE   ");
  strcpy(transdata, trans->GetTransactionDate());
  hlen += 7;
  dlen = strlen(transdata);
  strcpy(transdata + dlen, " ");
  dlen += 1;

  // Time
  strcpy(heading + hlen, "TIME   ");
  strcpy(transdata + dlen, trans->GetTransactionTime());
  hlen += 7;
  dlen = strlen(transdata);
  strcpy(transdata + dlen, " ");
  dlen += 1;

  try
    {
      SuperTrans * suptrans = dynamic_cast<SuperTrans *> (trans);
      if( suptrans != NULL )
	{

	  // Change the Date
      snprintf(transdata,6+1,"%s",suptrans->GetSuperDate());
      snprintf(transdata + 7,4+1,"%s",suptrans->GetSuperTime());
	  //strncpy(transdata, suptrans->GetSuperDate(), 6);
	  //strncpy(transdata + 7, suptrans->GetSuperTime(), 4);
	  transdata[11] = ' ';
	  hlen -= 2;
	  dlen -= 2;

	  // Add Supervisor Badge to the buffer
	  strcpy(heading + hlen, "SUP#");
	  hlen += 4;
	  strcpy(transdata + dlen, suptrans->SuperBadge);
	  int sblen = strlen(suptrans->SuperBadge);
	  dlen += sblen;
	  transdata[dlen++] = ' ';
	  transdata[dlen] = '\0';

	  if(sblen < 4)
	    {
	      for(int i=0; i<4-sblen; i++) transdata[dlen++] = ' ';
	      transdata[dlen] = '\0';
	    }
	  else
	    {
	      for(int i=0; i<=sblen-4; i++) heading[hlen++] = ' ';
	      heading[hlen] = '\0';
	    }
	}
    }
  catch (bad_cast)
    {
    }

  // FUNCTION
  strcpy(heading + hlen, "FN ");
  hlen += 3;
  const char * msg = oper.GetFunctionMessage(trans->Function);
  if( msg == NULL || strlen(msg) == 0 )
    {
      transdata[dlen++] = trans->Function;
      transdata[dlen++] = ' ';
      transdata[dlen++] = ' ';
      transdata[dlen] = '\0';
    }
  else
    {
      strcpy(transdata + dlen, msg);
      dlen = strlen(transdata);
      strcpy(transdata + dlen, " ");
      dlen += 1;
      int len = strlen(msg) - 2;
      for(int i=0; i<len; i++)
	{
	  heading[hlen++] = ' ';
	}
      heading[hlen] = '\0';
    }

  for(SysIndex j = 0; j < SysFunctionKeyLevelsMax; j++)
    {
      if(trans->Levels[j].Valid())
	{
	  if( j > 0 )
	    {
	      sprintf(heading + hlen, "LM%d ", j+1);
	      hlen += 4;
	      const char * lmsg = oper.GetFunctionMessageForLevel(trans->Function, j+1);
	      if(lmsg != NULL && strlen(lmsg) != 0)
		{
		  strcpy(transdata + dlen, lmsg);
		  dlen = strlen(transdata);
		  transdata[dlen++] = ' ';
		  transdata[dlen] = '\0';

		  int len = strlen(lmsg) - 3;
		  for(int i=0; i<len; i++)
		    {
		      heading[hlen++] = ' ';
		    }
		  heading[hlen] = '\0';
		}
	      else
		{
		  strcpy(transdata + dlen, "    ");
		  dlen += 4;
		}
	    }

	  sprintf(heading + hlen, "LD%d ", j+1);
	  hlen += 4;
	  if( trans->Levels[j].Data != NULL )
	    {
	      strcpy(transdata + dlen, trans->Levels[j].Data);
	      int ldlen = (int)strlen(trans->Levels[j].Data);
	      dlen += ldlen;
	      transdata[dlen++] = ' ';
	      transdata[dlen] = '\0';
	      if(ldlen < 3)
		{
		  for(int i=0; i<3-ldlen; i++) transdata[dlen++] = ' ';
		  transdata[dlen] = '\0';
		}
	      else
		{
		  for(int i=0; i<ldlen-3; i++) heading[hlen++] = ' ';
		  heading[hlen] = '\0';
		}
	    }
	  else
	    {
	      strcpy(transdata, "    ");
	      dlen += 4;
	    }
	}
    }

  snprintf(line1,21,"%s",heading);
  //strncpy(line1, heading, 20);
  snprintf(line2,21,"%s",transdata);
  //strncpy(line2, transdata, 20);
  user_output_clear();
  user_output_write(1, 1, line1);
  user_output_write(2, 1, line2);
  user_output_write(3, 1, "EDIT TRANSACTIONS");
  user_output_write(4, 1, "0-ADD 1-DEL 2-NEXT");

  char input;
  while(true)
    {
      tm = 0;
      while (!user_input_ready () && tm < timeout)
	{
	  tm += 20;
	  usleep(10000); // Sleep for 10ms
	}
      input = user_input_get();
      if(tm >= timeout)
	{
	  oper.Beep(TYPE_TimeOut);;
	  return READ_TIMEDOUT;
	}
      if( input == ESC )
	{
	  oper.Beep(TYPE_KeyPress);
	  return READ_CLEAR;
	}
      switch(input)
	{
	case '<':
	  oper.Beep(TYPE_KeyPress);
	  if(strlen(transdata) > 20)
	    {
	      if( curIndex > 0 ) curIndex--;
	      snprintf(line1,21,"%s",heading + curIndex);
	      snprintf(line2,21,"%s",transdata + curIndex);
	      //strncpy(line1, heading + curIndex, 20);
	      //strncpy(line2, transdata + curIndex, 20);
	      user_output_write(1, 1, line1);
	      user_output_write(2, 1, line2);
	    }
	  break;
	case '>':
	  oper.Beep(TYPE_KeyPress);
	  if(strlen(transdata) > 20)
	    {
	      if( curIndex < (int)strlen(transdata) - 20) curIndex++;
	      snprintf(line1,21,"%s",heading + curIndex);
	      snprintf(line2,21,"%s",transdata + curIndex);
	      //strncpy(line1, heading + curIndex, 20);
	      //strncpy(line2, transdata + curIndex, 20);
	      user_output_write(1, 1, line1);
	      user_output_write(2, 1, line2);
	    }
	  break;
	case '0':
	case '1':
	case '2':
	case '+':
	case '-':
	  *key = input;
	  return READ_SUCCESS;
	}
    }

  oper.Beep(TYPE_TimeOut);;
  return READ_TIMEDOUT;
}

LCD_READ_STATE LCDHandler::DisplayTransactionToEmployee(Trans * trans, int timeout, char * key)
{
  static char heading[21];
  static char transdata[21];

  int tm = 0;
  static char date[5];
  static char time[5];
  static char fn[3];
  static char data[7];

  date[0] = '\0';
  time[0] = '\0';
  fn[0] = '\0';
  data[0] = '\0';

  heading[0] = '\0';
  transdata[0] = '\0';

  // Build the Heading and Data

  // Date

  strcpy(heading, "DATE ");
  snprintf(date,4+1,"%s",trans->GetTransactionDate());
  //strncpy(date, trans->GetTransactionDate(), 4);
  date[4] = '\0';

  strcpy(transdata, date);
  strcat(transdata, " ");

  // Time
  strcat(heading, "TIME ");
  snprintf(time,4+1,"%s",trans->GetTransactionTime());
  //strncpy(time, trans->GetTransactionTime(), 4);
  time[4] = '\0';

  strcat(transdata, time);
  strcat(transdata, " ");

  // FUNCTION
  strcat(heading, "FN ");

  switch(trans->Function)
    {
    case '*':
      strcpy(fn, "10");
      break;

    case '#':
      strcpy(fn, "12");
      break;

    case '0':
      strcpy(fn, "11");
      break;

    case '1':
      strcpy(fn, "01");
      break;

    case '2':
      strcpy(fn, "02");
      break;

    case '3':
      strcpy(fn, "03");
      break;

    case '4':
      strcpy(fn, "04");
      break;

    case '5':
      strcpy(fn, "05");
      break;

    case '6':
      strcpy(fn, "06");
      break;

    case '7':
      strcpy(fn, "07");
      break;

    case '8':
      strcpy(fn, "08");
      break;

    case '9':
      strcpy(fn, "09");
      break;
    case '+':
      //finger template - ignore this
      *key = '+';
      return READ_SUCCESS;
      break;
    }

  fn[2] = '\0';

  strcat(transdata, fn);
  strcat(transdata, " ");

  strcat(heading, "DATA ");

  SysIndex j = 0;

  if(trans->Levels[j].Valid())
    {
      if( trans->Levels[j].Data != NULL )
	{
	  int len = (int)strlen(trans->Levels[j].Data);
	  if(len > 6)
	    {
		  snprintf(data,6+1,"%s",trans->Levels[j].Data);
	      //strncpy(data, trans->Levels[j].Data, 6);
	    }
	  else if(len != 0)
	    {
	      strcpy(data, trans->Levels[j].Data);
	    }
	  else
	    {
	      //6 spaces
	      strcpy(data, "      ");
	    }
	}
      else
	{
	  //6 spaces
	  strcpy(data, "      ");
	}
    }
  else
    {
      //Invalid level
      //6 spaces
      strcpy(data, "      ");
    }
  data[6] = '\0';

  strcat(transdata, data);

  int tlen = strlen(transdata);
  int hlen = strlen(heading);

  transdata[tlen] = '\0';
  heading[hlen] = '\0';

  user_output_clear();

  user_output_write(1, 1, heading);
  user_output_write(2, 1, transdata);


  char input;
  while(true)
    {
      tm = 0;
      while (!user_input_ready () && tm < timeout)
	{
	  tm += 20;
	  usleep(10000); // Sleep for 10ms
	}
      input = user_input_get();
      if(tm >= timeout)
	{
	  oper.Beep(TYPE_TimeOut);;
	  return READ_TIMEDOUT;
	}
      if( input == ESC )
	{
	  oper.Beep(TYPE_KeyPress);
	  return READ_CLEAR;
	}
      switch(input)
	{
	case ESC:

	case '<':

	case '>':
	  *key = input;
	  return READ_SUCCESS;
	}
    }

  oper.Beep(TYPE_TimeOut);;
  return READ_TIMEDOUT;
}

// Can accept alphanumeric or special characters.
// Can handle a max of 20 chars only.
LCD_READ_STATE LCDHandler::ReadSSID(int row, int col, int timeout, int maxChars, char * buffer)
{
  if( maxChars > 20 ) return READ_TIMEDOUT;
  static char disp[2];
  disp[1] = '\0';
  // 20 character long
  strcpy(buffer, BLANK_LINE);
  user_output_write(row, col, buffer);
  int numOfChars = 0;
  user_output_cursor(USER_CURSOR_BOTH);
  if(col > 1)
    {
      user_output_write(row, col - 1, BLANKSPACE);
    }
  else
    {
      user_output_write(row - 1, 20, BLANKSPACE);
    }
  char input;
  while(true) // 10 Characters in HEX Format
    {
      int tm = 0;
      while (!user_input_ready () && tm < timeout)
	{
	  usleep(10000);
	  tm += 20;
	}
      if(tm >= timeout)
	{
	  oper.Beep(TYPE_TimeOut);;
	  return READ_TIMEDOUT;
	}
      input = user_input_get ();
      if( input == '\n' )
	{
	  oper.Beep(TYPE_KeyPress);
	  break;
	}
      if( input == ESC )
	{
	  oper.Beep(TYPE_KeyPress);
	  user_output_cursor(USER_CURSOR_OFF);
	  return READ_CLEAR;
	}
      switch(input)
	{
	case 'C':
	  if( buffer[numOfChars] >= 'A' && buffer[numOfChars] <= 'Z' )
	    {
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] += 32;
	      user_output_write(row, numOfChars + 1, disp);
	      if(numOfChars == 0)
		{
		  if(col > 1)
		    {
		      user_output_write(row, col - 1, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 20, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars, disp);
		}
	    }
	  else if( buffer[numOfChars] >= 'a' && buffer[numOfChars] <= 'z' )
	    {
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] -= 32;
	      user_output_write(row, numOfChars + 1, disp);
	      if(numOfChars == 0)
		{
		  if(col > 1)
		    {
		      user_output_write(row, col - 1, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 20, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars, disp);
		}
	    }
	  else
	    {
	      oper.Beep(150, TYPE_Reject);
	    }
	  break;
	case '+':
	  oper.Beep(TYPE_KeyPress);
	  disp[0] = buffer[numOfChars] = GetNextSSIDChar(buffer[numOfChars]);
	  user_output_write(row, numOfChars + 1, disp);
	  if(numOfChars == 20)
	    {
	      disp[0] = buffer[numOfChars - 2];
	      user_output_write(row, 19, disp);
	    }
	  else if(numOfChars == 0)
	    {
	      if(col > 1)
		{
		  user_output_write(row, col - 1, BLANKSPACE);
		}
	      else
		{
		  user_output_write(row - 1, 20, BLANKSPACE);
		}
	    }
	  else
	    {
	      disp[0] = buffer[numOfChars - 1];
	      user_output_write(row, numOfChars, disp);
	    }
	  break;
	case '-':
	  oper.Beep(TYPE_KeyPress);
	  disp[0] = buffer[numOfChars] = GetPrevSSIDChar(buffer[numOfChars]);
	  user_output_write(row, numOfChars + 1, disp);
	  if(numOfChars == 19)
	    {
	      disp[0] = buffer[numOfChars - 1];
	      user_output_write(row, 19, disp);
	    }
	  else if(numOfChars == 0)
	    {
	      if(col > 1)
		{
		  user_output_write(row, col - 1, BLANKSPACE);
		}
	      else
		{
		  user_output_write(row - 1, 20, BLANKSPACE);
		}
	    }
	  else
	    {
	      disp[0] = buffer[numOfChars - 1];
	      user_output_write(row, numOfChars, disp);
	    }
	  break;
	case '>':
	  oper.Beep(TYPE_KeyPress);
	  if(numOfChars < maxChars - 1)
	    {
	      disp[0] = buffer[numOfChars];
	      user_output_write(row, numOfChars + 1, disp);
	      if(numOfChars == maxChars - 1)
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars, disp);
		}
	      numOfChars++;
	    }
	  break;
	case '<':
	  oper.Beep(TYPE_KeyPress);
	  if(numOfChars > 0)
	    {
	      if(numOfChars == 1)
		{
		  if(col > 1)
		    {
		      user_output_write(row, numOfChars + col - 2, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 20, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 2];
		  user_output_write(row, numOfChars + col - 2, disp);
		}
	      numOfChars--;
	    }
	  break;
	default:
	  if(input < '0' || input > '9')
	    {
	      oper.Beep(150, TYPE_Reject);
	      continue;
	    }
	  else
	    {
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] = input;
	      user_output_write(row, numOfChars + 1, disp);
	      if(numOfChars == maxChars - 1)
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars, disp);
		}
	      if(numOfChars < maxChars - 1) numOfChars++;
	    }
	  break;
	}
    }
  return READ_SUCCESS;
}

// Reads the IP Address in the format 000.000.000.000
LCD_READ_STATE LCDHandler::ReadIPAddress(int row, int col, int timeout, char * ipaddr)
{
  user_output_write(row, col, ipaddr);
  static char disp[2];
  disp[1] = '\0';
  int numOfChars = 0;
  char input;
  if(col > 1)
    {
      user_output_cursor(USER_CURSOR_BOTH);
      user_output_write(row, col - 1, BLANKSPACE);
    }
  while(true) // 12 Characters in IP Address Format 000.000.000.000
    {
      int tm = 0;
      while (!user_input_ready () && tm < timeout)
	{
	  usleep(10000);
	  tm += 20;
	}
      if(tm >= timeout)
	{
	  oper.Beep(TYPE_TimeOut);
	  user_output_cursor(USER_CURSOR_OFF);
	  return READ_TIMEDOUT;
	}
      input = user_input_get ();
      if( input == '\n' )
	{
	  oper.Beep(TYPE_KeyPress);
	  break;
	}
      if( input == ESC )
	{
	  oper.Beep(TYPE_KeyPress);
	  user_output_cursor(USER_CURSOR_OFF);
	  return READ_CLEAR;
	}
      switch(input)
	{
	case '>':
	  if(numOfChars < 14)
	    {
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = ipaddr[numOfChars];
	      user_output_write(row, numOfChars + col, disp);
	      numOfChars++;
	      if(numOfChars == 3 || numOfChars == 7 ||  numOfChars == 11 )
		{
		  disp[0] = ipaddr[numOfChars];
		  user_output_write(row, numOfChars + col, disp);
		  numOfChars++;
		}
	    }
	  break;
	case '<':
	  oper.Beep(TYPE_KeyPress);
	  if(numOfChars > 0)
	    {
	      if(numOfChars == 1)
		{
		  if(col > 1)
		    {
		      user_output_write(row, numOfChars + col - 2, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = ipaddr[numOfChars - 2];
		  user_output_write(row, numOfChars + col - 2, disp);
		}
	      numOfChars--;
	      if(numOfChars == 3 || numOfChars == 7 ||  numOfChars == 11 )
		{
		  disp[0] = ipaddr[numOfChars - 2];
		  user_output_write(row, numOfChars + col - 2, disp);
		  numOfChars--;
		}
	    }
	  break;
	default:
	  if(input < '0' || input > '9')
	    {
	      oper.Beep(150, TYPE_Reject);
	      continue;
	    }
	  oper.Beep(TYPE_KeyPress);
	  ipaddr[numOfChars] = input;
	  disp[0] = input;
	  user_output_write(row, numOfChars + col, disp);
	  if(numOfChars == 14)
	    {
	      disp[0] = ipaddr[numOfChars - 1];
	      user_output_write(row, numOfChars + col - 1, disp);
	    }
	  if( numOfChars < 14 ) numOfChars++;
	  // Do Not allow the user to  modify "." in the address
	  if(numOfChars == 3 || numOfChars == 7 ||  numOfChars == 11 )
	    {
	      disp[0] = ipaddr[numOfChars];
	      user_output_write(row, numOfChars + col, disp);
	      numOfChars++;
	    }
	}
    }
  user_output_cursor(USER_CURSOR_OFF);
  return READ_SUCCESS;
}

/**
 * Read the Terminal Id
 */
LCD_READ_STATE LCDHandler::ReadTerminalId(int row, int col, int timeout, char & ch)
{
  static char disp[2];
  disp[1] = '\0';
  disp[0] = ch;
  user_output_write(row, col, disp);
  char input;
  if(col > 1)
    {
      user_output_cursor(USER_CURSOR_BOTH);
      user_output_write(row, col - 1, BLANKSPACE);
    }
  while(true)
    {
      int tm = 0;
      while (!user_input_ready () && tm < timeout)
	{
	  usleep(10000);
	  tm += 20;
	}
      if(tm >= timeout)
	{
	  oper.Beep(TYPE_TimeOut);;
	  return READ_TIMEDOUT;
	}
      input = user_input_get ();
      if( input == '\n' )
	{
	  oper.Beep(TYPE_KeyPress);
	  break;
	}
      if( input == ESC )
	{
	  oper.Beep(TYPE_KeyPress);
	  return READ_CLEAR;
	}
      switch(input)
	{
	case '+':
	  oper.Beep(TYPE_KeyPress);
	  disp[0] = GetNextTId(disp[0]);
	  break;
	case '-':
	  oper.Beep(TYPE_KeyPress);
	  disp[0] = GetPrevTId(disp[0]);
	  break;
	default:
	  if( (input < '0' || input > '9') )
	    {
	      oper.Beep(150, TYPE_Reject);
	      continue;
	    }
	  else
	    {
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = input;
	    }
	  break;
	}
      user_output_write(row, col, disp);
      if(col > 1)
	{
	  user_output_cursor(USER_CURSOR_BOTH);
	  user_output_write(row, col - 1, BLANKSPACE);
	}
    }
  ch = disp[0];
  user_output_cursor(USER_CURSOR_OFF);
  return READ_SUCCESS;
}
//////////////////////////////////////////////////////////////////////////
void init_alpha_numeric_table()
{
  alpha_numeric_table[0][0]='S';alpha_numeric_table[0][1]='Y';alpha_numeric_table[0][2]='M';
  alpha_numeric_table[1][0]='Q';alpha_numeric_table[1][1]='Z';alpha_numeric_table[1][2]='.';
  alpha_numeric_table[2][0]='A';alpha_numeric_table[2][1]='B';alpha_numeric_table[2][2]='C';
  alpha_numeric_table[3][0]='D';alpha_numeric_table[3][1]='E';alpha_numeric_table[3][2]='F';
  alpha_numeric_table[4][0]='G';alpha_numeric_table[4][1]='H';alpha_numeric_table[4][2]='I';
  alpha_numeric_table[5][0]='J';alpha_numeric_table[5][1]='K';alpha_numeric_table[5][2]='L';
  alpha_numeric_table[6][0]='M';alpha_numeric_table[6][1]='N';alpha_numeric_table[6][2]='O';
  alpha_numeric_table[7][0]='P';alpha_numeric_table[7][1]='R';alpha_numeric_table[7][2]='S';
  alpha_numeric_table[8][0]='T';alpha_numeric_table[8][1]='U';alpha_numeric_table[8][2]='V';
  alpha_numeric_table[9][0]='W';alpha_numeric_table[9][1]='X';alpha_numeric_table[9][2]='Y';
}
/////////////////////////////////////////////////////////////////////////

/**
 * Should accept input from Keyboard / Barcode / Mag
 * Separated this from the rest of the code so that we dont
 * break the old code
 */
//LCD_READ_STATE LCDHandler::ReadLevelData(SysInputType_e type, int row,
//							int col, int timeout, int maxChars,
//							int min, char * buffer)
LCD_READ_STATE LCDHandler::ReadLevelData(SysInputType_e type, SysInputSource level_src,
					 int row, int col, int timeout, int maxChars,
					 int min, char * buffer,char* defaultData)
{
  static char disp[2];
  disp[1] = '\0';
  user_output_write(row, col, buffer);
  char input;
  int numOfChars = 0;
  int pos = 0;

  user_output_cursor(USER_CURSOR_BOTH);
  init_alpha_numeric_table();
  if(col > 1)
  {
      user_output_write(row, numOfChars + col - 1, BLANKSPACE);
  }
  else
  {
      user_output_write(row - 1, 20, BLANKSPACE);
  }
  UserInput ::TimeOut(timeout * 1000);
  UserInput ::Flush();
  int numKeyPress = 0;
  char previous = '*';
  int cont = 0;
  int tm = 0;
  bool firstESC = true;
  bool firstChar = true;
  buffer[0] = '\0';

  bool newChar;
  newChar = true;
  bool addPosInTimeOut;
  addPosInTimeOut = false;

  if(defaultData != NULL && strcmp(defaultData,"") != 0)
  {
	  snprintf(buffer,maxChars+1,"%s",defaultData);
	  //strncpy(buffer,defaultData,maxChars);
      numOfChars = strlen(buffer);
	  pos = numOfChars ;
	  if(pos < maxChars)pos++;
	  user_output_write(row,col, buffer);
	  disp[0] = buffer[pos-2];disp[1] = '\0';
      user_output_write(row, pos + col - 2,disp);
	  firstChar = false;
  }
  while(true)
  {
      cont = 0;
	  UserInput ::TimeOut(timeout * 1000);
      UserInput ::Flush();
      UserInput ::source_t source = UserInput ::OTHER;
      //printf("input befor GET CHAR : %c\n",input);
      UserInput ::status_t r = UserInput ::GetChar ( input, source );
	  //printf("input after GET CHAR : %c\n",input);
    // printf("%c ",input);
	if ( r == UserInput ::TIMEOUT )
	{
	  oper.Beep(TYPE_TimeOut);
	  user_output_cursor(USER_CURSOR_OFF);
	  return READ_TIMEDOUT;
	}
      else if( r != UserInput ::OK )
	{
	  continue;
	}
    if(source != UserInput::KEYBOARD)
	{
	  ///////
	  SysInputSource temp_source = SysInputSourceDefault ;
	  //SysInputSource level_source = 0x02;
	  switch (source)
	  {
	    case UserInput ::PROX :
	      temp_source = SysInputSourceProximity;
	      break;
	    case UserInput ::MAG :
	      temp_source = SysInputSourceMagnetic;
	      break;
	    case UserInput ::BARCODE :
	      temp_source = SysInputSourceBarcode;
	      break;
	    case UserInput ::OTHER :
	      temp_source = SysInputSourceDefault;
	      break;
	    default:
	      temp_source = SysInputSourceDefault ;
	      break ;
	   }

	  //printf("\n temp_source = %#x", temp_source);
	  if((level_src & temp_source) != temp_source)
	  {
	      user_output_clear();
	      char ivs[21];
	      int lenIvs = oper.GetPromptMessage(TYPE_InvalidSource,ivs,21);
	      if( lenIvs != 0 )
		{
		  int col = ( 20 - lenIvs ) / 2 + 1;
		  user_output_write(2, col, ivs);
		}
	      else
		{
		  user_output_write(2, 1, "   INVALID SOURCE   ");
		}

	      oper.TurnOnLEDYellow();
	      oper.Beep(TYPE_Error);
	      oper.TurnOffLEDYellow();
	      usleep(oper.GetErrorMsgTimeout() * 1000);
	      user_output_cursor(USER_CURSOR_OFF);
	      UserInput ::ClearBuffer();

	      return READ_TIMEDOUT;
	  }

	  else
	  {
	      //printf("\n valid source");
	  }

	  //////
	  if( input == '\n' )
	  {
	      //printf("we press enter \n");
	      if(numOfChars > -1)buffer[numOfChars] = '\0';
	      oper.Beep(TYPE_KeyPress);
	      break;
	  }
	  //if( numOfChars < maxChars )
	  buffer[pos] = input;
	  disp[0] = input;
	  user_output_write(row, pos + col, disp);
	  numOfChars++;
	  pos++;
	  continue;
	}
KEYPAD: if( input == '\n' )
	{
	  ////source is keypad
	  SysInputSource temp_source = SysInputSourceKeypad;
	  //printf("\n temp_source = %#x", temp_source);
	  if((level_src & temp_source) != temp_source)
	  {
	      user_output_clear();
	      char ivs[21];
	      int lenIvs = oper.GetPromptMessage(TYPE_InvalidSource,ivs,21);
	      if( lenIvs != 0 )
		{
		  int col = ( 20 - lenIvs ) / 2 + 1;
		  user_output_write(2, col, ivs);
		}
	      else
		{
		  user_output_write(2, 1, "   INVALID SOURCE   ");
		}
	      oper.TurnOnLEDYellow();
	      oper.Beep(TYPE_Error);
	      oper.TurnOffLEDYellow();
	      usleep(oper.GetErrorMsgTimeout() * 1000);
	      user_output_cursor(USER_CURSOR_OFF);
	      UserInput ::ClearBuffer();
	      return READ_TIMEDOUT;
	  }
	  ////
	  buffer[numOfChars] = '\0';
	  oper.Beep(TYPE_KeyPress);
	  break;
	}

    if( input == ESC )
	{
	  if(firstESC && numOfChars > 0)
	  {
	     numOfChars = 0 ;
		 pos = 0 ;
		 buffer[0] = '\0';
		 user_output_write(row, 1, "                   ");
          user_output_write(row, col, buffer);
		 firstESC = false;
		 firstChar = true;
		 continue;
	  }
	  else
	  {
		oper.Beep(TYPE_KeyPress);
		user_output_cursor(USER_CURSOR_OFF);
		return READ_CLEAR;
	  }
	}
	else
	 firstESC = true;

    switch(input)
	{
	case '>':
	  oper.Beep(TYPE_KeyPress);
	  if(pos < maxChars && pos < numOfChars)
	    {
	      if(buffer[pos] == ' ')break;
	      if((pos-1) > 0)disp[0] = buffer[pos-1];
	      user_output_write(row, pos + col-1, disp);
		  previous =  '*';
          newChar = false;
		  numKeyPress = 0 ;
		  pos++;
	    }
	  break;
	case '<':
	  oper.Beep(TYPE_KeyPress);
	  if( pos > 1)
	  {
	    if(pos == 2)
		{
		  if(col > 1)
		  {
			user_output_write(row, pos + col - 3, BLANKSPACE);
		  }
		}
		else
		{
			if((pos - 3) >= 0)disp[0] = buffer[pos - 3];
			user_output_write(row, pos + col - 3, disp);
		}
		previous =  '*';
        newChar = false;
		numKeyPress = 0 ;
		pos--;
	  }
	  break;
	case 'C':
	  if( buffer[numOfChars] >= 'A' && buffer[numOfChars] <= 'Z' )
	    {
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] += 32;
	      user_output_write(row, numOfChars + col, disp);
	    }
	  else if( buffer[numOfChars] >= 'a' && buffer[numOfChars] <= 'z' )
	    {
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] -= 32;
	      user_output_write(row, numOfChars + col, disp);
	    }
	  else
	    {
	      oper.Beep(150, TYPE_Reject);
	    }
	  if(numOfChars == 0)
	    {
	      if(col > 1)
		{
		  user_output_write(row, 	numOfChars + col - 1, BLANKSPACE);
		}
	      else
		{
		  user_output_write(row - 1, 	20, BLANKSPACE);
		}
	    }
	  else
	    {
	      disp[0] = buffer[numOfChars - 1];
	      user_output_write(row, numOfChars + col - 1, disp);
	    }
	  break;
	case '+':
	  switch(type)
	    {
	    case SysInputTypeAlphaNumeric:
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] = GetNextAlphaNumericChar(buffer[numOfChars]);
	      user_output_write(row, numOfChars + col, disp);
	      if(numOfChars == 0)
		 {
		  if(col > 1)
		    {
		      user_output_write(row, 	numOfChars + col - 1, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 	20, BLANKSPACE);
		    }
		}
	    else
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars + col - 1, disp);
		}
	      break;
	    case SysInputTypeAlpha:
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] = GetNextAlphaChar(buffer[numOfChars]);
	      user_output_write(row, numOfChars + col, disp);
	      if(numOfChars == 0)
		{
		  if(col > 1)
		    {
		      user_output_write(row, 	numOfChars + col - 1, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 	20, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars + col - 1, disp);
		}
	      break;
	    case SysInputTypeNumeric:
	    case SysInputTypeDefault:
	    case SysInputTypeDecimal:
	      oper.Beep(150, TYPE_Reject);
	      break;
	    }
	  break;
	case '-':
	  switch(type)
	    {
	    case SysInputTypeAlphaNumeric:
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] = GetPrevAlphaNumericChar(buffer[numOfChars]);
	      user_output_write(row, numOfChars + col, disp);
	      if(numOfChars == 0)
		{
		  if(col > 1)
		    {
		      user_output_write(row, 	numOfChars + col - 1, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 	20, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars + col - 1, disp);
		}
	      break;
	    case SysInputTypeAlpha:
	      oper.Beep(TYPE_KeyPress);
	      disp[0] = buffer[numOfChars] = GetPrevAlphaChar(buffer[numOfChars]);
	      user_output_write(row, numOfChars + col, disp);
	      if(numOfChars == 0)
		{
		  if(col > 1)
		    {
		      user_output_write(row, 	numOfChars + col - 1, BLANKSPACE);
		    }
		  else
		    {
		      user_output_write(row - 1, 	20, BLANKSPACE);
		    }
		}
	      else
		{
		  disp[0] = buffer[numOfChars - 1];
		  user_output_write(row, numOfChars + col - 1, disp);
		}
	      break;
	    case SysInputTypeNumeric:
	    case SysInputTypeDefault:
	    case SysInputTypeDecimal:
	      oper.Beep(150, TYPE_Reject);
	      break;
	    }
	  break;
	default:
	  switch(type)
	    {
	    case SysInputTypeNumeric:
	      if( (input < '0' || input > '9') )
		{
		  oper.Beep(150, TYPE_Reject);
		  continue;
		}
		  if(firstChar && pos ==0 && numOfChars == 0){pos = 1;numOfChars=1;}
	      oper.Beep(TYPE_KeyPress);
	      if((pos -1) >= 0)buffer[pos-1] = input;
	      disp[0] = input;
	      user_output_write(row, pos + col-1, disp);
	      if(pos == maxChars )
		  {
			if (pos == 0)
		    {

		      user_output_write(row, 	pos + col - 2, BLANKSPACE);
		    }
			else
		    {
		      if((pos -2) >= 0 )
			  {
				  disp[0] = buffer[pos - 2];
		          user_output_write(row, pos + col - 2, disp);
			  }
		    }
		  }
		  if(!firstChar)
		  {
			if((pos - 1) == numOfChars)
			{
			  if( numOfChars < maxChars )
			  {
				  numOfChars++;
				  if(pos < maxChars)pos++;
			  }
			}
			else
			{
				if((pos) <= numOfChars){if(pos < maxChars)pos++;}
			}
		  }
		  else
		  {
			firstChar = false;
			if(pos < maxChars)pos++;
		  }
		  break;
	    case SysInputTypeAlpha:
	      oper.Beep(150, TYPE_Reject);
	      continue;
	    case SysInputTypeAlphaNumeric:
			if( (input < '0' || input > '9') )
			{
				oper.Beep(150, TYPE_Reject);
				continue;
			}
			//if(firstChar && pos ==0 && numOfChars == 0){pos = 1;numOfChars=1;firstChar = false;}
			previous =  '*';
			while(input == previous || previous == '*')
			{
				char disp1[2];
				if(numKeyPress == 0)
				{
					//char temp;
					if(newChar && (numOfChars-1)>=0 && input == buffer[numOfChars-1] && tm < 1000)
					{
					 // printf("1111\n");
					  numKeyPress++;
					  buffer[pos-1] = alpha_numeric_table[input-48][numKeyPress-1];
					  disp[0] = alpha_numeric_table[input-48][numKeyPress-1];
					  if((pos-2) >= 0)disp1[0] = buffer[pos-2]; else disp1[0] = ' ';
					  disp1[1] = '\0';
					  user_output_write(row, pos + col-1, disp);
					  user_output_write(row,pos + col-2, disp1);
					}
					else
					{
					  if(newChar)
					  {
						  //printf("new char1 addPosInTimeOut = %d\n",addPosInTimeOut);
						  if((!addPosInTimeOut) && pos <= numOfChars && pos < maxChars)
						  {
							 pos++;
						  }
						  //if(pos < numOfChars)pos++;
						  if(( numOfChars < maxChars) && pos > numOfChars)
						  {
							numOfChars++;
							//pos++;
						  }
					  }
					  addPosInTimeOut = false;
					  //printf("if(newChar) pos = %d numOfChars = %d\n",pos,numOfChars);
					  if(pos-1 >= 0)buffer[pos-1] = input;
					  disp[0] = input;disp[1] = '\0';
					  if((pos-2) >= 0)disp1[0] = buffer[pos-2]; else disp1[0] = ' ';
					  disp1[1] = '\0';
					  user_output_write(row, pos + col-1, disp);
					  //if(pos == numOfChars)
						user_output_write(row,pos + col-2, disp1);
					  //if(pos < numOfChars)pos++;
					}
					//numOfChars++;
				}else
				{
					//printf("new char3 \n");
					int i = input-48;
					//printf("test char = %c %d\n", alpha_numeric_table[1][0],input);
					//printf("BBBBBBB char = %c\n",alpha_numeric_table[i][numKeyPress-1]);
					if(pos-1 >= 0)buffer[pos-1] = alpha_numeric_table[i][numKeyPress-1];
					disp[0] = alpha_numeric_table[i][numKeyPress-1];
                    if((pos-2) >= 0)disp1[0] = buffer[pos-2]; else disp1[0] = ' ';
					disp1[1] = '\0';
					user_output_write(row, pos + col-1, disp);
					user_output_write(row,pos + col-2, disp1);
					//numOfChars++;
				}
				tm = 0;
				while (!user_input_ready () && tm < 1000)
				{
					usleep(10000);
					tm += 20;
				}
				if(tm >= 1000)
				{
					//printf("FFFFFF %d\n",timeout);
					previous = '*';
					numKeyPress = 0 ;
					newChar = true;
					if((pos-1) >= 0)disp1[0] = buffer[pos-1]; else disp1[0] = ' ';
					disp1[1] = '\0';
					if(pos < maxChars)user_output_write(row,pos + col-1, disp1);
					if(pos < maxChars){pos++;addPosInTimeOut = true;}
					break;
				}
				previous = input;
				input = user_input_get ();
				if(input == '\n' || input == '<' || input == '>' || input == ESC)
				{
					previous = '*'; numKeyPress = 0 ;goto KEYPAD;
				}

				numKeyPress++;
				numKeyPress = numKeyPress % 4;
				newChar = false;
				if(previous != input)
				{
                  //printf("CCCCCCCC\n");
				  if(pos < maxChars){pos++;}
				  if((numOfChars < maxChars)){numOfChars++;}
				  if(pos-1 >= 0)buffer[pos-1] = input;
				  disp[0] = input;
				  if((pos-2) >= 0)disp1[0] = buffer[pos-2]; else disp1[0] = ' ';
				  disp1[1] = '\0';
				  user_output_write(row, pos + col-1, disp);
				  user_output_write(row,pos + col-2,disp1);
				  numKeyPress = 0 ;
				  newChar = true;
				}
				//printf("DDDDDDDD\n");
			}
			continue;
		default:
	      continue;
	    }
	}
   }
  // Remove empty spaces if any
  int len = strlen(buffer);

  for(int i=0; i<len; i++)
    {
      if(buffer[i] == ' ') buffer[i] = '\0';
    }
  user_output_cursor(USER_CURSOR_OFF);
  return READ_SUCCESS;
}

