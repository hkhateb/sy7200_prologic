/*
	TestUI:  [Module]

	The TestUI command exercises the UI7000 user interface board.

	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   01 Jan 1901
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime$
		 $Date: 2005/09/01 22:27:58 $

Description:

	The keypad input portion of the user interface board is tested by
regularly polling for key presses and displaying the resulting data.

*/


/* Include files for TestUI. */
#include <StdIO.h>
#include <StdLib.h>
#include <String.h>
#include <UniStd.h>
#include "Gen_Def.h"

#include "User.h"

#include <sys/IOCtl.h>
#include <asm/TA7000_IO.h>

/* Constant parameters for TestUI. */
static const char * NAME = "TestUI";
typedef enum {
	SC_HELP = 1,
	SC_NO_VISUAL_OUT,
	SC_NO_AUDIBLE_OUT,
	}
StatusCode_e;
#define NOTE_A			'A'
#define NOTE_B_FLAT		'B'
#define NOTE_B			'C'
#define NOTE_C			'D'
#define NOTE_C_SHARP	'E'
#define NOTE_D			'F'
#define NOTE_D_SHARP	'G'
#define NOTE_E			'H'
#define NOTE_F			'I'
#define NOTE_F_SHARP	'J'
#define NOTE_G			'K'
#define NOTE_A_FLAT		'L'

/* Type and structure definitions for TestUI. */
typedef struct {
	struct {
		Boolean poll;
		Boolean echo;
	}
	mode;
	struct {
		int visual;
		int audible;
	}
	enable;
	struct {
		int visual_out;
		int audible_out;
	}
	file;
}
TestData_t;

/* Internal static data for TestUI. */
static TestData_t _tui = {
	.mode = {
		.poll = YES,
		.echo = YES,
		},
	.enable = {
		.visual = YES,
		.audible = YES,
	},
	.file = {
		.visual_out = -1,
		.audible_out = -1,
	},
};

/* External global data for TestUI. */

/* Function prototypes for TestUI. */
static void     _do_visual (int vo_file, char keypress);
static void     _do_audible (int vo_file, char keypress);
static Code		_error (Code code);



/*
	TestUI:  [Command]

	User interface test command.

Description:



Usage:

	TestUI [options]

Arguments:


Options:

	-help or -h or -?
				Output help text to standard output.

Returns:

	Exit code of EXIT_SUCCESS (zero) if no errors occurred;
	otherwise EXIT_FAILURE (one).

Notes:

See also:

*/

		int
main (int argc, char * argv [])

{
	Code code = SC_OKAY;
	Boolean escaped = NO;

	for (;;) {
		printf ("%s: Testing user interface via I2C interface.\n"
			"   (Hit Clear Clear on keypad to exit.)\n",
				NAME);

		/* If requested, initialize light outputs and turn them all on. */
		if (_tui.enable.visual) {
			_tui.file.visual_out = user_visual_init ();
			if (_tui.file.visual_out <= 0) {
				_error (SC_NO_VISUAL_OUT);
				_tui.enable.visual = NO;
			}
			else {
				_tui.enable.visual = YES;

				user_visual_set (USER_VISUAL_RED, ON);
				user_visual_set (USER_VISUAL_YEL, ON);
				user_visual_set (USER_VISUAL_GRN, ON);
			}
		}

		/* If requested, initialize audible output and beep it. */
		if (_tui.enable.audible) {
			_tui.file.audible_out = user_audible_init ();
			if (_tui.file.audible_out <= 0) {
				_error (SC_NO_AUDIBLE_OUT);
				_tui.enable.audible = YES;
			}
			else {
				_tui.enable.audible = YES;

				user_audible_set (1000);
				sleep (1);
				user_audible_set (0);
			}

		}

		/* Initialize user keypad and display interface. */
		code = user_init ();
		if (code != SC_OKAY) break;

		user_output_clear ();

		/* Do a variety of positioning operations to write initial display. */
		user_output_write (3, 0, "UI7000 User I/F Test\n");
		user_output_write (0, 1, "abcdefghijklmnopqrst");
		user_output_write (0, 1, "12345678901234567890");
		user_output_write (0, 11, "ABCDEFGHIJ");
		user_output_write (1, 1, "");

		printf ("%s: Initialization complete; waiting for input.\n", NAME);

		while (_tui.mode.poll) {

			if (user_input_ready ()) {

				char keypress = user_input_get ();

				if (keypress == ESC) {
					if (escaped) break;
					puts ("<ESC>");
					escaped = YES;
				}
				else {
					putchar (keypress);
					escaped = NO;
				}

				if (_tui.enable.visual) {
					_do_visual (_tui.file.visual_out, keypress);
				}
				if (_tui.enable.audible) {
					_do_audible (_tui.file.audible_out, keypress);
                    }

				if (_tui.mode.echo) {
					if (keypress == ESC) {
						user_output_clear ();
					}
					else {
						char buffer [2];
						buffer[0] = keypress;
						buffer[1] = '\0';
						user_output_write (0, 0, buffer);
					}
				}

				fflush (stdout);
			}
		}
		
		user_close ();
		user_visual_close ();

		printf ("\n%s: Done\n", NAME);
		break;
	}

	if (code != SC_OKAY) code = _error (code);

	return (code == SC_OKAY ? EXIT_SUCCESS : EXIT_FAILURE);
}



/*
	_do_visual:

	The _do_visual internal function interprets key values to turn
indicator LEDs on and off.

Arguments:
	vo_file		Visual output device file handle.

	key			Key code.

Returns:
	void

*/

		static void
_do_visual (int vo_file, char keypress)

{
	unsigned int cmd = 0;
	unsigned long int state = OFF;

	switch (keypress) {
	case 'A':
		cmd = VISUAL_OUT_IOC_W_RED;
		state = ON;
		break;
	case 'B':
		cmd = VISUAL_OUT_IOC_W_YEL;
		state = ON;
		break;
	case 'C':
		cmd = VISUAL_OUT_IOC_W_GRN;
		state = ON;
		break;
	
	case 'D':
		cmd = VISUAL_OUT_IOC_W_RED;
		state = OFF;
		break;
	case 'E':
		cmd = VISUAL_OUT_IOC_W_YEL;
		state = OFF;
		break;
	case 'F':
		cmd = VISUAL_OUT_IOC_W_GRN;
		state = OFF;
		break;
	}

	if (cmd != 0) {
		ioctl (vo_file, cmd, (unsigned long)state);
	}

	return;
}


/*
	_do_audible:

	The _do_audible internal function interprets key values to manipulate
the audible tone output.

Arguments:
	ao_file		Audible output device file handle.

	key			Key code.

Returns:
	void

*/

		static void
_do_audible (int ao_file, char keypress)

{
	Boolean set = NO;
	unsigned int tone = 0;
	static unsigned int base_tone = 0;
	static unsigned int last_tone = 0;
	static char freq [6] = "";

	switch (keypress)
		{
	case NOTE_A:
		base_tone = tone = 440;
		break;
	case NOTE_B_FLAT:
		base_tone = tone = 466;
		break;
	case NOTE_B:
		base_tone = tone = 494;
		break;
	case NOTE_C:
		base_tone = tone = 523;
		break;
	case NOTE_C_SHARP:
		base_tone = tone = 554;
		break;
	case NOTE_D:
		base_tone = tone = 587;
		break;
	case NOTE_D_SHARP:
		base_tone = tone = 622;
		break;
	case NOTE_E:
		base_tone = tone = 659;
		break;
	case NOTE_F:
		base_tone = tone = 698;
		break;
	case NOTE_F_SHARP:
		base_tone = tone = 740;
		break;
	case NOTE_G:
		base_tone = tone = 784;
		break;
	case NOTE_A_FLAT:
		base_tone = tone = 831;
		break;

	case '<':
		tone = last_tone / 2;
		break;
	case '>':
		tone = last_tone * 2;
		break;
	case '+':
		if (last_tone <= 50) tone = last_tone + 1;
		else tone = round_percent (last_tone, 101);
		break;
	case '-':
		if (last_tone <= 50) tone = last_tone - 1;
		else tone = round_percent (last_tone, 99);
		break;

	case '0' ... '9':
		{
			size_t len;
			len = strlen (freq);
			if (len >= sizeof freq) break;
			freq [len++] = keypress;
			freq [len++] = '\0';
		}
		break;

	case '\n':
		if (strlen (freq) > 0) {
			base_tone = tone = atoi (freq);
			freq [0] = '\0';
		}
		else tone = base_tone;
		break;

	case ESC:
		tone = 0;
		set = YES;
		break;

	}

	if (tone != 0) set = YES;
	if (set) {
		ioctl (ao_file, AUDIBLE_OUT_IOC_TONE, (unsigned long)tone);
		last_tone = tone;
	}

	return;
}


/*
	_error:

	The _error internal function provides error reporting for command
execution failures.

Arguments:
	code		System status code.

Returns:
	Code		Normally same as code supplied;
				Set to SC_OKAY if error is not to be reported to
					command processor.

*/

		static Code
_error (Code code)

{
	char msg [256];
	const char * msg_p = msg;

	/* Default to no error message. */
	msg [0] = '\0';

	/* Dispatch base on status code. */
	switch (code)
		{

	case SC_OKAY:
	case SC_FAILURE:
		break;

	case SC_HELP:
		/* Clear bypass code. */
		code = SC_OKAY;
		break;

	case SC_NO_VISUAL_OUT:
		msg_p = "Unable to open visual output device.";
		break;
	case SC_NO_AUDIBLE_OUT:
		msg_p = "Unable to open audible output device.";
		break;

	case SC_USER_GENERIC:
		msg_p = "Generic user subsystem error.";
		break;
	case SC_USER_MISSING_ADAPTOR:
		msg_p = "I2C adaptor not found.";
		break;
	case SC_USER_DEVICE_ADDRESS:
		msg_p = "I2C device address not set.";
		break;


	default:
		sprintf (msg, "Command error (code %d).", code);
		break;

		}		

	/* Report any actual error message. */
	if (*msg_p != '\0') {
		fprintf (stderr, "\n%s: %s\n", NAME, msg_p);
		}

	return (code);
}

		
