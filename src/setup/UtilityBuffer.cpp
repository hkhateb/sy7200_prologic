/*
	Buffer:  [Class]

	The Buffer module provides a generic data buffer object.

	Copyright (c) 2000-2002 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   27 Nov 2000
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   19 Apr 2002 14:09:20  $
	     $Date: 2005/09/01 22:27:58 $

Description:

This module provides member functions for manipulating Buffer objects.

*/


/* Compilation options for Buffer. */

/* Include files for Buffer. */
#include <string.h>
#include <stdio.h>
//#include "Generic.hpp"
#include "Gen_Def.h"
#include "Utility.hpp"

/* Constant parameters for Buffer. */

/* Type and structure definitions for Buffer. */

/* Internal static data for Buffer. */

/* Function prototypes for Buffer. */


/*
	Buffer::AddData:

	Add the specified number of bytes of data to a buffer.

Description:

	Add the supplied data to the data in an Buffer.  If there is not enough
room in the existing buffer create a new buffer data block, copy the data
to the new data block and free the old data block.

Notes:

	Warning: Data added via this function is not terminated as a string.
Use the AddString function to treat the buffer contents as a traditional
C string value.

See also:  Buffer::AddString

Arguments:
	new_data	Pointer to new data to add to

	length		Number of bytes of new data.

Returns:
	size_t		Number of bytes of data added to buffer.

*/

		size_t
Buffer::AddData (const Byte * new_data, size_t length)

{

	//printf("AddData = %d \n",length);
	if (size + length > max_size) {
		Byte * odp = data;
		data = new Byte [max_size *= 2];
		memcpy (data, odp, size);
		delete [] odp;
		}

	memcpy (data+size, new_data, length);
	size += length;

	return (length);
}


/*
	Buffer::AddString:

	Add data in a string to a buffer.

Description:

	Add the supplied string to the data in an Buffer.  A string tereminating
null character is added to the buffer but not included in its size.  If
the existing data in the buffer ends with a null terminator overwrite the
terminator.

See also:  Buffer::AddData

Arguments:
	string		String data to add to

	separator	Separator string to add to buffer prior to
					new data if buffer already contains data;
				NULL if no separator is needed.

Returns:
	size_t		Number of bytes of data added to buffer; not including
				string termination null.

*/

		size_t
Buffer::AddString (const char * string, const char * separator)

{
	size_t start = size;
	
	if (size > 0 AND separator != NULL) AddString (separator);

	// Add string including null terminator.
	AddData ((const Byte *)string, strlen (string)+1);
	size--;	// Leave null terminator out of buffer size.

	return (size - start);
}


