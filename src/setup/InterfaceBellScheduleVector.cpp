#include "InterfaceBellScheduleVector.hpp"

InterfaceBellScheduleVector::InterfaceBellScheduleVector()
{
}

InterfaceBellScheduleVector::~InterfaceBellScheduleVector()
{
}

void InterfaceBellScheduleVector::add(SysBellScheduleItem assign)
{
    vect.push_back( assign );
}

void InterfaceBellScheduleVector::remove(SysBellScheduleItem item)
{
    
}

int InterfaceBellScheduleVector::size()
{
    return vect.size();
}

void InterfaceBellScheduleVector::clear()
{
    vect.clear();
}

SysBellScheduleItem InterfaceBellScheduleVector::Get(int index)
{
    SysBellScheduleItem& assign = *vect[index];
    return assign;
}


