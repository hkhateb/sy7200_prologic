#include "InterfaceMessageBadgeVector.hpp"

InterfaceMessageBadgeVector::InterfaceMessageBadgeVector()
{
}

InterfaceMessageBadgeVector::~InterfaceMessageBadgeVector()
{
}

void InterfaceMessageBadgeVector::add(SysMessageBadgeItem assign)
{
    vect.push_back( assign );
}

void InterfaceMessageBadgeVector::remove(SysMessageBadgeItem item)
{
    
}

int InterfaceMessageBadgeVector::size()
{
    return vect.size();
}

void InterfaceMessageBadgeVector::clear()
{
    vect.clear();
}

SysMessageBadgeItem InterfaceMessageBadgeVector::Get(int index)
{
    SysMessageBadgeItem& assign = *vect[index];
    return assign;
}

