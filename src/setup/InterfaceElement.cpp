/*
	InterfaceElement:  [Class]

	The InterfaceElement module provides core Interface subsystem member
functions.

	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   27 Nov 2000
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   19 Apr 2002 14:09:20  $
	     $Date: 2005/09/01 22:27:58 $

Description:

	This module provides core Interface subsystem functions.  It also provides
basic parse and format member functions for manipulating InterfaceElement
objects.

*/


/* Compilation options for InterfaceElement. */

/* Include files for InterfaceElement. */
#include "System.hpp"
#include "Interface.hpp"

/* Constant parameters for InterfaceElement. */

/* Type and structure definitions for InterfaceElement. */

/* Internal static data for InterfaceElement. */

/* Function prototypes for InterfaceElement. */


/*
	InterfaceElement::ParseData:

	Parse an InterfaceElement from the Buffer provided.

Description:

	Convert the supplied item data to the requested InterfaceElement
format using the data type, value, and format data supplied.

See also:  InterfaceElement::FormatData

Arguments:
	buffer		Reference to an Buffer in which formatted data is stored.

	obj_p		Pointer to object or region containing data for element.

Returns:
	Code		System status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;
				SC_INTERFACE_BAD_DATA if item data could not be parsed;

*/

		Code
InterfaceElement::ParseData (InterfaceBuffer & buffer, void * const obj_p) const

{
	Code code = SC_OKAY;

	for (;;)
		{

		code = SystemParse (buffer, obj_p);
		if (code != SC_INTERFACE_UNKNOWN_TYPE) break;

		code = BaseParse (buffer, obj_p);
		if (code != SC_INTERFACE_UNKNOWN_TYPE) break;

		code = ExtendParse (buffer, obj_p);
		if (code != SC_INTERFACE_UNKNOWN_TYPE) break;

		break;
		}

	/* External parse routines may return SC_FAILURE for parsing failures
		so translate to Interface subsystem status code. */
	if (code == SC_FAILURE) code = SC_INTERFACE_BAD_DATA;

	return (code);
}


/*
	InterfaceElement::FormatData:

	Format an InterfaceElement into the Buffer provided.

Description:

	Convert the supplied data element to the requested InterfaceElement
format using the data type, value, and format data supplied.

See also:  InterfaceElement::ParseData

Arguments:
	buffer		Reference to an Buffer in which formatted data is stored.

	obj_p		Pointer to object or region containing data for element.

Returns:
	Code		System status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;

*/

		Code
InterfaceElement::FormatData (InterfaceBuffer & buffer,
	const void * const obj_p) const

{
	Code code = SC_OKAY;

	for (;;)
		{

		code = SystemFormat (buffer, obj_p);
		if (code != SC_INTERFACE_UNKNOWN_TYPE) break;

		code = BaseFormat (buffer, obj_p);
		if (code != SC_INTERFACE_UNKNOWN_TYPE) break;

		code = ExtendFormat (buffer, obj_p);
		if (code != SC_INTERFACE_UNKNOWN_TYPE) break;

		break;
		}

	return (code);
}


