/*
	LegacySetup:  [Class]

	The LegacySetup module provides processing of legacy (ala TACWin32) time
and data configuration program files.

	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   13 May 2004
	 $Revision: 1.5 $
	   $Author: walterm $
	  $Modtime$
	     $Date: 2007/02/16 15:31:34 $

Description:

	This module

*/


/* Compilation options for LegacySetup. */

/* Include files for LegacySetup. */
#include <string.h>
#include <ctype.h>
#include "System.hpp"

#include "Legacy.hpp"
#include "Setup.hpp"
#include "SerialIO.hpp"
#include "Str_Def.h"

/* Constant parameters for LegacySetup. */
const char LegacySetupSeparator = '=';

/* Type and structure definitions for LegacySetup. */

/* Internal static data for LegacySetup. */

/* Function prototypes for LegacySetup. */


/*
	LegacySetupItem::Parse:

	Parse a legacy program line into tag and data fields.

Description:

	The program line supplied is scanned to split it into a two letter tag
field located before the equal ('=') sign and a data field comprised of
everything after the equal sign.

Examples:

	The program line:
	
		IS=SKW
		
would be split into a tag of "IS" and data string of "SKW".

See also:  LegacySetupItem::Process

Arguments:
	line		Legacy program line to parse.

Returns:
	Code		System status code of SC_OKAY for successful completion;

*/

		Code
LegacySetupItem::Parse (const char * line)

{
//	printf("\n inside parse");
	
	Code code = SC_OKAY;
	const char * lp = line;
	const char * sp;
	for (;;) {

		// Strip line of leading whitespace and check for
		// empty line or comment.
		lp = str_blank (lp);
		if (*lp == '\0' OR NOT isalnum (*lp)) {
			code = SC_LEGACY_EMPTY;
			break;
			}
		// Look for a separator character.
		sp = strchr (lp, LegacySetupSeparator);
		if (sp == NULL) {
			code = SC_LEGACY_FORMAT;
			break;
			}
	//	printf("\n parsing line = %s", line);
		// Copy tag to setup object and check if its all that's before separator.
		*tag = '\0';
		strncat (tag, lp, LegacyTagLength);
		lp += LegacyTagLength;
		lp = str_blank (lp);
		if (lp != sp) {
			code = SC_LEGACY_FORMAT;
			break;
			}
		// Move data portion of line to setup item data.
	//	printf("\n Reset()");
	//	printf("\n");
		Reset ();
		lp = str_blank (++sp);
	//	printf("\n AddString");
	//	printf("\n");
		AddString (lp);
	//	printf("\nResetPtr()");
	//	printf("\n");
		ResetPtr ();
	//	printf("\n break");
	//	printf("\n");
		break;
		}
	return (code);
}


/*
	LegacySetupItem::Process:

	Process a legacy program item.

Description:

	The previously parsed legacy program item is processed for inclusion in a
terminal configuration or prorgram.

See also:  LegacySetupItem::Parse

Arguments:
	sys_data	Reference to legacy system setup data structure.
	
Returns:
	Code		System status code of SC_OKAY for successful completion;

*/

		Code
LegacySetupItem::Process (LegacySetup & setup)

{
	Code code = SC_OKAY;
	for (;;) {
		const SetupElement * tag_p = NULL;
		if (NOT Valid ()) {
			code = SC_LEGACY_NULL;
			break;
			}

		for (;;) {
			if(strstr(tag, "CR") != NULL || strstr(tag, "ZZ") != NULL || strstr(tag, "MP") != NULL )
			{ //These are Function items
				tag_p = SetupTagsFunction->LookupLegacy (tag);
			}
			else if(strstr(tag, "SN") != NULL || strstr(tag, "T#") != NULL || strstr(tag, "RS") != NULL 
				|| strstr(tag, "MS") != NULL || strstr(tag, "MA") != NULL || strstr(tag, "MD") != NULL
				|| strstr(tag, "EX") != NULL || strstr(tag, "LS") != NULL || strstr(tag, "LT") != NULL
				|| strstr(tag, "LB") != NULL)
			{
				//These are  Config items
				tag_p = SetupTagsConfig->LookupLegacy (tag);
			}
			else
			{
				//Rest are program items
				tag_p = SetupTagsProgram->LookupLegacy (tag);
			}
			if (tag_p != NULL)  break;
			
			code = SC_SETUP_NO_ITEM;
			break;
		}
		
		if (code != SC_OKAY) break;
		code = tag_p->ParseData (*this, &setup);
		if (code != SC_OKAY) break;
		
		break;
	}

	return (code);
}

/*
	LegacySetupItem::Display

	Format the legacy item for display.

Description:

	The previously parsed legacy program item is formatted into the supplied
string for display.

See also:  LegacySetupItem::Parse

Arguments:
	buffer_p	Pointer to buffer string in which to format item. 	
	
Returns:
	size_t		Length of string data placed in buffer.

*/

		size_t
LegacySetupItem::Display (char * buffer_p)
	const

{
	size_t len = 0;

	for (;;) {

		if (NOT Valid ()) {
			*buffer_p = '\0';
			break;
			}

		// Format tag and data fields.
		len = sprintf (buffer_p, "\"%s\",\"%s\"", tag, data);

		break;
		}

	return (len);
}


/*
	LegacySetupItem::DisplayError

	Format the legacy item for display.

Description:

	The previously parsed legacy program item is formatted into the supplied
string for display.

See also:  LegacySetupItem::Display

Arguments:
	buffer_p	Pointer to buffer string in which to format item. 	
	
Returns:
	size_t		Length of string data placed in buffer.

*/

		size_t
LegacySetupItem::DisplayError (char * buffer_p)
	const

{
	char * bp = buffer_p;

	for (;;) {

		if (NOT Valid ()) {
			*buffer_p = '\0';
			break;
			}

		// Format tag and data fields.
		bp += sprintf (bp, "\"%s\",\"%s\"\n", tag, data);
		bp += sprintf (bp, " %*s , %*s%c\n", strlen (tag), "",
					   (data_p - (char *)data), "", '^');

		break;
		}

	return (bp-buffer_p);
}


/*
	LegacySetup::Defaults

	Set default legacy setup program values.

Description:

	Assign default values to all elements of a LegacySetup data structure.

See also:

Arguments:
	terminal	Terminal name to assign to setup data.

Returns:
	void

*/

		void
LegacySetup::Defaults (const char * terminal)

{

	name[0] = '\0';
	if (terminal != NULL) strncat (name, terminal, sizeof (name)-1);
	id = 0x00;

	mode.lock = NO;
	mode.configuration = NO;
	mode.access_only = NO;
	
	fptemplate.mode = '0';              //0= verify, 1 = identify. Default = verify   
	fptemplate.clear = '0';             //clear all templates. 1 = clear, 0 = retain
	fptemplate.deletebadge[0] = '\0';   //Delete all templates for this badge
	fptemplate.threshhold = '4';		//Reject Threshhold	- Sucurity level? default =4 (refer Matching SDK)
	
	/*fptemplate.templatepart1[0] = '\0';		//Fingerprint template (Base64 encode) would be spanning these 4 lines (f2 - f5), 128 bytes each 
	fptemplate.templatepart2[0] = '\0';
	fptemplate.templatepart3[0] = '\0';
	fptemplate.templatepart4[0] = '\0';*/
	
	fptemplate.templatestring[0] = '\0';
	
	//fptemplate.finger = NULL;
	fptemplate.finger.badge[0] = '\0';
	fptemplate.finger.fingerindex = '\0';
	fptemplate.finger.threshhold = '\0'; 
	fptemplate.finger.privilegelevel = '0';  //default privilege level
	
	badge.length.maximum = 10;
	badge.length.minimum = 10;
	badge.length.valid = 10;
	badge.length.offset = 0;
	badge.length.type = SysInputTypeDefault;
	badge.prefix.supervisor.entry[0] = '\0';
	badge.prefix.supervisor.recall[0] = '\0';
	badge.prefix.security[0] = '\0';
	badge.prefix.configuration[0] = '\0';
	badge.prefix.facility[0] = '\0';
	badge.swipe_n_go.limit[0] = '\0';
	badge.swipe_n_go.function = '\0';

	pin_length = 0;

	source.initial = SysInputSourceDefault;
	source.function = SysInputSourceDefault;
	source.supervisor = SysInputSourceDefault;
	source.super_employee = SysInputSourceDefault;
	source.configuration = SysInputSourceDefault;

	strcpy (user.prompt.idle, "");
	strcpy (user.prompt.invalid_source, "");
	strcpy (user.prompt.invalid_badge, "");
	strcpy (user.prompt.try_again, "");
	strcpy (user.prompt.enter_function, "");
	user.flip.clock = 3;
	user.flip.idle = 3;
	user.timeout.data = 30;
	user.timeout.function = 30;
	user.timeout.message = 30;
	user.timeout.supervisor = 30;
	user.timeout.error = 30;

	supervisor.use_default.date = NO;
	supervisor.use_default.time = NO;
	supervisor.delete_keys.setup = '\0';
	supervisor.delete_keys.execute = '\0';

	message.badge = NULL;

	function.key = NULL;
	function.detail = NULL;

	bell.schedule = NULL;
	bell.maintenance = NO;

	access.global = SysModeAuto;
	access.window.time.start = 0000;
	access.window.time.stop = 0000;
	access.window.days = 0x00;
	strcpy (access.message.always_on, "");
	strcpy (access.message.always_off, "");
	access.duration = 100;
	access.badge = NULL;

	#if 0
	for (int idx = 0; idx < SysProfileMessageDefault; idx++) {
		profile.defaults[idx].text[0] = '\0';
		profile.defaults[idx].flags = 0x00;
		profile.defaults[idx].timeout = 0;
		}
	#endif
	profile.defaults = NULL;
	profile.message = NULL;
	profile.entry = NULL;
	profile.assign = NULL;
	profile.mode.compress = NO;
	profile.mode.override = NO;
	profile.mode.report_fails = NO;

	validate.descriptions = NO;
	for (int idx = 0; idx < SysValidateLevelMax; idx++) {
		validate.level[idx].width = 4;
		validate.level[idx].include_badge = NO;
		validate.level[idx].error_message[0] = '\0';
		}
	validate.entry = NULL;

	print.enable = NO;
	for (int idx = 0; idx < SysPrintHeaderMax; idx++) {
		memset (print.header[idx].line, '\0', SysFieldMaxHeader+1);
		}
	print.LF_count = 0;
	print.reset[0] = '\0';

	finger.enable = OFF;
	finger.level = 0;
	finger.wait = 30;
	finger.security[0] = '\0';
	finger.table.memory = 0;
	finger.table.badge_length = 0;
	finger.table.prints = 1;

	clock.twenty_four_hour = NO;
	clock.daylight_savings = YES;

	hardware.lightbox = NO;

	communications.RS232.baudrate = BAUD_9600;
	communications.RS485.baudrate = BAUD_9600;
	communications.RS485.turnaround = 10; // milliseconds
	communications.modem.baudrate = BAUD_2400;
	communications.modem.answer.ring_delay = 1; // rings
	communications.modem.answer.time.start = 0000;
	communications.modem.answer.time.stop = 2359;
	communications.modem.external = NO;

	return;
}


/*
	LegacySetup::List

	List setup data.

Description:

	List data items using SetupElement lists.

See also:

Arguments:
	stream		Output stream on which to list formatted data.

	mode		YES include comments; NO tag lines only.

Returns:
	void

*/

		void
LegacySetup::List (FILE * stream, Boolean mode) const

{

	if (mode) fprintf (stream, "\n/*** Legacy Setup Program Listing\n");
	if (mode) fprintf (stream, "\n/** Terminal Setup:\n");
	SetupTagsProgram->ListLegacy (stream, this, mode);
	if (mode) fprintf (stream, "\n/** Configuration:\n");
	SetupTagsConfig->ListLegacy (stream, this, mode);
	if (mode) fprintf (stream, "\n/** Functions:\n");
	SetupTagsFunction->ListLegacy (stream, this, mode);

	return;
}

namespace {
  char* ld_tim ( time_t& d , char* i ){
    const unsigned int len = strlen ( "yyyymmddhhmm" );
    if ( 0 == i ){
      return i ;
    }

    if ( strlen ( i ) < len ){
      return i ;
    }

    tm t ;
    memset ( & t , 0 , sizeof ( t ));
    int r = sscanf ( i , "%4d%2d%2d%2d%2d" , & t .tm_year , & t .tm_mon , & t .tm_mday , & t .tm_hour , & t .tm_min );
    if ( r != 5 ){
      return 0 ;
    }

    t .tm_year -= 1900 ;
    t .tm_mon -= 1 ;
    d = mktime ( & t );    
    return i + len ;
  }

  char* st_tim ( char* i , const time_t& d ){
    tm t ;
    memset ( & t , 0 , sizeof ( t ));
    t = * localtime ( & d );
    t .tm_year += 1900 ;
    t .tm_mon += 1 ;
    sprintf ( i , "%04d%02d%02d%02d%02d" , t .tm_year , t .tm_mon , t .tm_mday , t .tm_hour , t .tm_min );
    return i + strlen ( i );
  }

  char* ld_int ( int& d , char* i ){
    if ( 0 == i ){
      return i ;
    }

    if ( strlen ( i ) < 3 ){
      return i ;
    }

    int r = sscanf ( i , "%3d" , & d );
    if ( r != 1 ){
      return 0 ;
    }

    return i + 3 ;
  }

  char* st_int ( char* i , const int& d ){
    sprintf ( i , "%03d" , d );
    return i + strlen ( i );
  }

};


char* LegacySetup ::d_s_a_t ::operator = ( char* i ){
  char* p = i ;
  i = ld_tim ( forward , i );
  if ( 0 == i ){
    return p ;
  }

  i = ld_tim ( backward , i );
  if ( 0 == i ){
    return p ;
  }

  i = ld_int ( skip_min , i );
  if ( 0 == i ){
    return p ;
  }

  return i ;
}

//"200703110200200711040200060"
// yyyymmddhhmmyyyymmddhhmm
void LegacySetup ::d_s_a_t ::as_string ( char* v , size_t l ) const {
  if ( l <= 27 ){//make sure there is enough room.
    *v = 0 ;
    return ;
  } 
  char* i = v ;
  i = st_tim ( i , forward );
  i = st_tim ( i , backward );
  i = st_int ( i , skip_min );
}
