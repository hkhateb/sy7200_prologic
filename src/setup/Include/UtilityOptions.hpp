/*
	UtililtyOptions.hpp:

	Application specific compilation options for utility functions
used by the Data Collection Terminal system.

	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   19 Aug 2004
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime$
	     $Date: 2005/09/01 22:27:58 $

*/

#if ! defined UTILITYOPTIONS_HPP
#define UTILITYOPTIONS_HPP

/* Serial I/O global name options. */
#define SIO_NAME_PARITY			1	/* Include parity mode names. */
#define SIO_NAME_BAUD_ALL		1	/* Include all baud rate names. */
#define SIO_NAME_BAUD_SHORT		1	/* Include short baud rate names. */

#endif

