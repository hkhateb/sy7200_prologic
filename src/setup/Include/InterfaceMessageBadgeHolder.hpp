#ifndef INTERFACEMESSAGEBADGEHOLDER_HPP
#define INTERFACEMESSAGEBADGEHOLDER_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "System.hpp"

class InterfaceMessageBadgeHolder{
public:

	InterfaceMessageBadgeHolder(){}
	virtual ~InterfaceMessageBadgeHolder(){}
	virtual void add(SysMessageBadgeItem)=0;
	virtual void remove(SysMessageBadgeItem)=0;
	virtual int size()=0;
	virtual void clear()=0;
	virtual SysMessageBadgeItem Get(int)=0;
};

#endif // INTERFACEMESSAGEBADGEHOLDER_HPP
