#ifndef INTERFACEPROFILEASSIGNHOLDER_HPP
#define INTERFACEPROFILEASSIGNHOLDER_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "System.hpp"

class InterfaceProfileAssignHolder{
public:

	InterfaceProfileAssignHolder(){}
	virtual ~InterfaceProfileAssignHolder(){}
	virtual void add(SysProfileAssignItem)=0;
	virtual void remove(SysProfileAssignItem)=0;
	virtual int size()=0;
	virtual void clear()=0;
	virtual SysProfileAssignItem Get(int)=0;
};

#endif // INTERFACEPROFILEASSIGNHOLDER_HPP
