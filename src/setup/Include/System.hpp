/*
	System.hpp:

	Standard definitions for the Data Collection Terminal (DCT) system software.

	Copyright (c) 1991-2002,2004 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   14 May 2004
	  Based on:   Project System.hpp's originally written on 27 Dec 1999
	 $Revision: 1.9 $
	   $Author: walterm $
	  $Modtime:   19 Apr 2002 14:02:16  $
	     $Date: 2007/02/05 22:12:36 $

*/

#if ! defined SYSTEM_HPP
#define SYSTEM_HPP

/* Get compiler standard definitions for NULL, size_t, etc. */
#include <stddef.h>
#include <string.h>

/* Get generic definitions and types. */
#include "Gen_Def.h"

/* Get standard library & I/O functions. */
#include <stdio.h>

//#include "InterfaceValidateEntryHolder.hpp"

/* Map case insensitive compares to regular compare. */
#define _stricmp strcmp
#define _strincmp strncmp

/* System data strings. */
extern const char SystemNameShort [];
extern const char SystemNameLong [];
extern const char SoftwareVersion [];
extern const char Copyright [];

/* Environment variable names. */
extern const char EnvironmentSetup [];
extern const char EnvironmentPath [];
extern const char EnvironmentCom [];

/* Disk file extension values. */
extern const char ExtensionCfgSys [];
extern const char ExtensionLog [];

/* Standard title maximum length. */
const size_t SysTitleMax = 32;

/* System status code group definitions. */
typedef enum SysCode {
	SC_EXIT = 2,
	SC_OPTION_BAD,
	SC_OPTION_VALUE,
	SC_OPTION_UNKNOWN,
	SC_BASE_SETUP		= 1100,
	SC_BASE_INTERFACE	= 1200,
	SC_BASE_LEGACY		= 2100,
	}
SysCode_e;

/* Include Utility definitions for data types. */
#include "Utility.hpp"

/* Extended system data types;
	note that some additional system types are mapped to other types
	elsewhere in this header. */
enum SysType_e {
	SYSTYPE_BASE = MAX_TYPE,
	SYSTYPE_Time,
	SYSTYPE_TimeWindow,
	SYSTYPE_AccessWindow,
	SYSTYPE_OnOffAuto,
	SYSTYPE_KeyPair,				// Setup & execute key pair.
	SYSTYPE_LegacyBadgeLenType,		// Legacy AcroComm badge length & type.
	SYSTYPE_LegacyBadgeMaxOff,		// Legacy AcroComm badge maximum & offset.
	SYSTYPE_LegacyHalfHeader,		// Legacy AcroComm split print header lines.
	SYSTYPE_LegacyMessage,			// Legacy employee message.
	SYSTYPE_LegacyFunctionKey,		// Legacy function key definition.
	SYSTYPE_LegacyFunctionDetail,	// Legacy function key detail.
	SYSTYPE_LegacyBell,				// Legacy bell schedule.
	SYSTYPE_LegacyAccessBadge,		// Legacy access badge.
	SYSTYPE_LegacyProfileMessage,	// Legacy profile message.
	SYSTYPE_LegacyProfileDefault,	// Legacy profile default.
	SYSTYPE_LegacyProfileEntry,		// Legacy profile entry.
	SYSTYPE_LegacyProfileAssign,	// Legacy profile assignment.
	SYSTYPE_LegacyValidLevels,		// Legacy validation levels.
	SYSTYPE_LegacyValidEntry,		// Legacy validation entry.
	SYSTYPE_LegacyFingerTable,		// Legacy fingerprint table setup.
	SYSTYPE_SupervisorPrefix,		//Supervisor Prefix
	SYSTYPE_ClearTemplates,			// clear all templates
	SYSTYPE_DeleteFinger,			//Delete finger for this badge
	SYSTYPE_FingerTemplateLine1,
	SYSTYPE_FingerTemplateLine2,
	SYSTYPE_FingerTemplateLine5,
	SYSTYPE_Daylight_savings_adjustment,
	MAX_SYSTYPE
};		

/** Basic data types. */

/* Index value data type for process data. */
typedef UBin8 SysIndex;
const SysIndex SysIndexNone = 0xFF;
struct SysIndexRange {
	SysIndex min, max;
};

/* Unit address data type. */
typedef UBin8 SysAddress;

/* Standard data type for process data. */
typedef SBin15 SysValue;

/* Display decimal point location value. */
typedef UBin8 SysDP;

/* Field length or offset values. */
typedef UBin8 SysFieldLength;

/* Delay & time values (and type mappings. */
typedef UBin8 SysDelayShort;
#define SYSTYPE_DelayShort TYPE_UBin8
typedef UBin16 SysDelay;
#define	SYSTYPE_Delay TYPE_UBin16
typedef UBin16 SysTime;
typedef UBin32 SysTimeSecs;

struct SysDate
{
    SysTime year;
    SysTime month;
    SysTime date;
};

/* Time conversion constants. */
static const SysTime SecondsPerMinute = 60;
static const SysTime MinutesPerHour = 60;
static const SysTime HoursPerDay = 24;

/* Time window data structure. */
struct SysTimeWindow {
	SysTime start;
	SysTime stop;
};

/* On, Off & Auto mode enumeration. */
enum SysOnOffAuto {
	SysModeOff = 0,
	SysModeAuto,
	SysModeOn,
};

enum SysFPIDMode {
	FpVerify = 0,
	FpIdentify
};

/* Data value range and band type definition. */
struct SysRange {
	SysValue min, max;
};
struct SysBand {
	SysValue min, max;
	SysValue width;
	SysDP decimal;
};

/* Data value with decimal point. */
struct SysDecimal {
	SysValue value;
	SysDP decimal;
};

/* Data value(s) with decimal and range. */
struct SysRangeValue : SysDecimal {
	SysRange range;
};

/*** Terminal program data types. */

/* Badge, display and other message maximum field lengths. */
const size_t SysFieldMaxBadgeLength				= 64;
const size_t SysFieldMaxBadgePrefix				= 64;
const size_t SysFieldMaxPrompt					= 20;
const size_t SysFieldMaxName					= 20;
const size_t SysFieldMaxHeader					= 40;
const size_t SysFieldMaxHeaderHalf				= (SysFieldMaxHeader/2);
const size_t SysFieldMaxLevel					= 16;
const size_t SysFieldMaxValidate				=
							SysFieldMaxLevel+SysFieldMaxBadgeLength;
const size_t SysFieldMaxSecurity				= 10;
const size_t SysFieldMaxPrintReset				= 30;

/* Badge or data input source flags. */
typedef Bit8 SysInputSource;
const SysInputSource SysInputSourceInvalid		= 0x00;
const SysInputSource SysInputSourceMagnetic		= 0x01;
const SysInputSource SysInputSourceKeypad		= 0x02;
const SysInputSource SysInputSourceBarcode		= 0x04;
const SysInputSource SysInputSourceProximity	= 0x08;
const SysInputSource SysInputSourceFprint       = 0x20;
const SysInputSource SysInputSourceDate			= 0x10;
const SysInputSource SysInputSourceDefault		= (SysInputSourceMagnetic |
												   SysInputSourceKeypad |
												   SysInputSourceBarcode |
												   SysInputSourceProximity |
												   SysInputSourceFprint);
												   
const SysInputSource SysInputSourceAll			= (SysInputSourceMagnetic |
												   SysInputSourceKeypad |
												   SysInputSourceBarcode |
												   SysInputSourceProximity |
												   SysInputSourceFprint);

const SysInputSource SysInputSourceProxMag		= (SysInputSourceProximity|
												   SysInputSourceMagnetic );
											   	   

/* Badge or data input data type flags. */
typedef UBin8 SysInputType;
enum SysInputType_e {
	SysInputTypeDefault			= 0,
	SysInputTypeAlpha			= 1,
	SysInputTypeNumeric			= 2,
	SysInputTypeAlphaNumeric	= 3,
	SysInputTypeDecimal			= 4,
};
const char * const SysInputTypeChars = " ANXD";
//const char * const SysInputTypeNums = " 1234";
const char * const SysInputTypeNums = "* 1234"; //* is for character template - added by Deepali
const char * const SysInputDecimals = "01234";
//const char * const SysInputChars = "0123456789";
const char * const SysInputChars = "0123456789:;<=>?";

const char * const SysFpIdModeChars = "01";
const char * const SysFpTemplateClearChars = "01";
const char * const SysFpRtChars = "1234567";  //Security levels per Matching SDK
const char * const SysFingerIndexChars = "0123456789";
const char * const SysFpPrivilegeLevelChars = "012";

/* Badge or data input data validation flags. */
typedef Bit8 SysInputValid;
const SysInputValid SysInputValidNone			= 0x00;
const SysInputValid SysInputValidVisual			= 0x01;
const SysInputValid SysInputValidTable			= 0x02;
const SysInputValid SysInputValidBoth			= 0x03;
const char * const SysInputValidChars = "0123";
typedef UBin8 SysInputTable;
const char * const SysInputTableChars = "0123";

/* Baud rate value. */
typedef UBin8 SysBaudRate;

/* Day of the week flags. */
typedef Bit8 SysDayOfWeek;
const SysDayOfWeek SysDayMonday					= 0x01;
const SysDayOfWeek SysDayTuesday				= 0x02;
const SysDayOfWeek SysDayWednesday				= 0x04;
const SysDayOfWeek SysDayThursday				= 0x08;
const SysDayOfWeek SysDayFriday					= 0x10;
const SysDayOfWeek SysDaySaturday				= 0x20;
const SysDayOfWeek SysDaySunday					= 0x40;
const SysDayOfWeek SysDayHoliday				= 0x80;
const SysIndex SysDayOfWeekMax					= 8;

/** 
 * Work Week Flags - Sunday - Saturday
 */
const SysDayOfWeek WrkWeekSunday				= 0x01;
const SysDayOfWeek WrkWeekMonday				= 0x02;
const SysDayOfWeek WrkWeekTuesday				= 0x04;
const SysDayOfWeek WrkWeekWednesday				= 0x08;
const SysDayOfWeek WrkWeekThursday				= 0x10;
const SysDayOfWeek WrkWeekFriday				= 0x20;
const SysDayOfWeek WrkWeekSaturday				= 0x40;

/*** Terminal program data structures. ***/

/* Badge length settings. */
struct SysBadgeLengths {
	SysFieldLength maximum;
	SysFieldLength minimum;
	SysFieldLength valid;
	SysFieldLength offset;
	SysInputType type;
};

struct SysSuperPrefix
{
	char entry [SysFieldMaxBadgePrefix+1];
	char recall [SysFieldMaxBadgePrefix+1];
};

/* Function key module data. */
typedef char SysFunctionKey;
struct SysFunctionKeyPair {
	SysFunctionKey setup;
	SysFunctionKey execute;
};
typedef UBin8 SysFunctionLevel;
const char * const SysFunctionLevelChars = "0123456";
typedef Bit8 SysFunctionFlags;
const SysFunctionFlags SysFunctionFlagSupervisor= 0x01;
const SysFunctionFlags SysFunctionFlagProfile	= 0x02;
const SysFunctionFlags SysFunctionFlagAccess	= 0x04;
const SysFunctionFlags SysFunctionFlagMessage	= 0x08;
const SysFunctionFlags SysFunctionFlagPrint		= 0x10;
const SysFunctionFlags SysFunctionFlagDefault	= 0x00;
struct SysFunctionKeyItem {
	char key;
	SysFunctionLevel levels;
	SysFunctionFlags flags;
    char Function[20];
				SysFunctionKeyItem (char key = '0', SysFunctionLevel levels = 1,
					SysFunctionFlags flags = SysFunctionFlagDefault)
					: key (key), levels (levels), flags (flags) { }

	Boolean		Valid (void) const
		{ 
      /*  if( ( ( (key >= '0' && key <= '9') || key == '*' || key == '#' || key == '?') 
                  &&  (levels > 0 && levels <= 6) )
                   || (key == '?' && levels == 0 ) )*/
          if( ( ( (key >= '0' && key <= '9') || key == '*' || key == '#' || key == '?') 
                  &&  (levels > 0 && levels <= 6) )
                   || (key != '0' && levels == 0 ) )
             return 1;
        else
             return 0;
    }   
};
const char * const SysFunctionKeyChars = "0123456789*#?";
const SysIndex SysFunctionKeyLevelsMax = 6;
const SysIndex SysFunctionKeyMax = strlen(SysFunctionKeyChars);
struct SysFunctionDetailItem {
	SysFunctionKey key;
	SysFunctionLevel level;
	char defaultData[SysFieldMaxLevel+1];
	char message [SysFieldMaxPrompt+1];
	SysInputSource source;
	SysInputType type;
	SysDP decimal;
	struct {
		SysFieldLength min, max;
	}
	length;
	struct {
		SysInputValid flags;
		SysInputTable table;
	}
	validation;
				SysFunctionDetailItem (char key = '0',
					SysFunctionLevel level = 1,
					SysInputSource source = SysInputSourceDefault,
					SysInputType type = SysInputTypeDefault,
					SysDP decimal = 0,
					SysFieldLength max = 1, SysFieldLength min = 0,
					SysInputValid flags = SysInputValidNone,
					SysInputTable table = 0)
					: key (key), level (level), source (source), type (type),
						decimal (decimal)
					{ defaultData[0] = '0' ; message[0] = '\0'; length.max = max; length.min = min;
					validation.table = table; validation.flags = flags; }
					
	Boolean		Valid (void) const
	{ 
		
       /* if( ( (key >= '0' && key <= '9') || key == '*' || key == '#' || key == '?') 
                  &&  (level > 0 && level <= 6) && (type >=1 && type <=4) )*/
         if( 
         	( (key >= '0' && key <= '9') || key == '*' || key == '#' || key == '?') 
                  &&  (level > 0 && level <= 6) 
                  && ( (type >= '1' && type <= '4') || (type == ' ') )
           )
             return 1;
        else
             return 0;
    }
};
const SysIndex SysFunctionDetailMax = SysFunctionKeyMax * SysFunctionKeyLevelsMax;

/*** Terminal program option data structures. ***/
struct SysFingerTemplateData {
	char badge [SysFieldMaxBadgeLength+1];
	char fingerindex;
	char threshhold;
	char privilegelevel;
/*	
	char templatepart1[129];
	char templatepart2[129];
	char templatepart3[129];
	char templatepart4[129];*/
};

/* Employee messaging data. */
struct SysMessageBadgeItem {
	Boolean once;
	Boolean initial_swipe;
	SysFunctionKey key;
 	char badge [SysFieldMaxBadgeLength+1];
	char text [SysFieldMaxPrompt+1];
};
const SysIndex SysMessageBadgeMax = 100;

/* Bell schedule item data. */
struct SysBellScheduleItem {
	SysDayOfWeek days;
	SysTime time;
	SysDelayShort duration;
				SysBellScheduleItem (SysDayOfWeek days = 0x00,
					SysTime time = 0, SysDelayShort duration = 0)
					: days (days), time (time), duration (duration) { }
};
const SysIndex SysBellScheduleMax = 64;

/* Access control module data. */
struct SysAccessBadgeItem {
	char badge [SysFieldMaxBadgeLength+1];
};
const SysIndex SysAccessBadgeMax = 250;
struct SysAccessWindowItem
{
	SysTimeWindow time;
	SysDayOfWeek days;
};

/* Profile module data. */
typedef Bit8 SysProfileFlags;
const SysProfileFlags SysProfileFlagAllow		= 0x01;
const SysProfileFlags SysProfileFlagSupervisor	= 0x02;
const SysProfileFlags SysProfileFlagToneAccept	= 0x04;
const SysProfileFlags SysProfileFlagToneReject	= 0x08;
const SysProfileFlags SysProfileFlagToneMask	= 0x0C;
const SysProfileFlags SysProfileFlagValid		= 0x80;
const SysProfileFlags SysProfileFlagDefault		= 0x00;
const char * const SysProfileFlagToneChars		= "NARB";

struct SysProfileMessageItem {
	SysProfileFlags flags;
	SysDelayShort timeout;
	char text [SysFieldMaxPrompt+1];

	Boolean		Valid (void) const
					{ return ((flags & SysProfileFlagValid) ? 1 : 0); }
};
const SysIndex SysProfileMessageMax = 15;
const SysIndex SysProfileMessageDefaults = 2;

enum SysProfileDefault_e {
	SysProfileDefault_NoProfileFound,
	SysProfileDefault_NoBadgeFound,
};

struct SysProfileTimeItem {
	SysIndex message;
	SysTimeWindow window;
};
const SysIndex SysProfileTimeMax = 8;
struct SysProfileEntryItem {
	SysIndex index;
	SysProfileTimeItem time [SysProfileTimeMax];
};
const SysIndex SysProfileEntryMax = 64;

struct SysProfileAssignItem {
	char badge [SysFieldMaxBadgeLength+1];
	SysIndex entry [SysDayOfWeekMax];
};
const SysIndex SysProfileAssignMax = 80;

/* Validation module data. */
struct SysValidateLevelItem {
	SysFieldLength width;
	Boolean include_badge;
	char error_message [SysFieldMaxPrompt+1];
};
const SysIndex SysValidateLevelMax = 4;
const char * const SysValidateLevelChars = "0123";
struct SysValidateEntryItem {
	SysIndex level;
	char data [SysFieldMaxValidate+1];
	char message [SysFieldMaxPrompt+1];
};
const SysIndex SysValidateEntryMax = 64;
struct SysValidateData {
	Boolean descriptions;
	SysValidateLevelItem level [SysValidateLevelMax];
//	SysValidateEntryItem * entry;
//    InterfaceValidateEntryHolder * entry;
    void * entry;
};

/* Print module data. */
const SysIndex SysPrintHeaderMax = 2;

/* Finger print matching data. */
struct SysFingerData {
	Boolean enable;
	UBin8 level;
	SysDelay wait;
	char security [SysFieldMaxSecurity+1];
	struct {
		UBin16 memory;	// Not used.
		SysFieldLength badge_length;
		SysIndex prints;
	}
	table;
};

/* System status reporting function entry and pointer types. */
typedef void SysStatus_f (void * unit_h, Code code);
typedef SysStatus_f * SysStatus_fp;

const SysIndex SysTransactionFlagsMax = 5;

struct SysLevelData
{
    SysInputSource Source;
    char Data[SysFieldMaxLevel+1];
    SysLevelData(SysInputSource Source = 0x00) : Source(Source){}
    Boolean Valid()
    {
    	if( !(Source & SysInputSourceDefault) )
    	{
    		return false;
    	}
    	if(Data == NULL || strlen(Data) == 0)
    	{
    		return false;
    	}
    	return true;
    }
};

/* Class or structure offset class. */
typedef unsigned short Offset_t;
class Offset {

public:
	Offset_t offset;
	static const Offset_t OffsetInvalid = ~0;

public:
				Offset (Offset_t offset = OffsetInvalid) : offset (offset) {};
				Offset (const void * member, const void * base)
					: offset ((Byte *)member - (Byte *)base) {};
	void *		Get (void * base) const
					{ return ((Byte *)base + offset); };
	const void *
				Get (const void * base) const
					{ return ((const Byte *)base + offset); };
	Offset_t	GetOffset (void) const
					{ return (offset); };
	Boolean		Valid (void) const
					{ return ((offset == OffsetInvalid) ? NO : YES); };
};

#endif

