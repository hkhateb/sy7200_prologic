#ifndef INTERFACEBELLSCHEDULEVECTOR_HPP
#define INTERFACEBELLSCHEDULEVECTOR_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "InterfaceBellScheduleHolder.hpp"
#include "TAVector.h"
using namespace std;

class InterfaceBellScheduleVector : public InterfaceBellScheduleHolder{
public:

	InterfaceBellScheduleVector();
	virtual ~InterfaceBellScheduleVector();
	virtual void add(SysBellScheduleItem);
	virtual void remove(SysBellScheduleItem);
	virtual int size();
	virtual void clear();
	virtual SysBellScheduleItem Get(int);

private:
    TAVector<SysBellScheduleItem> vect;
};

#endif // INTERFACEBELLSCHEDULEVECTOR_HPP
