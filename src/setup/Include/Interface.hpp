/*
	Interface.hpp

	Data, class and function declarations for Interface objects.

	Copyright (c) 2000-2002 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:	  Alan J. Luse
	Written on:   20 Nov 2000
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   19 Apr 2002 18:06:10  $
	     $Date: 2005/09/01 22:27:58 $

*/


#if ! defined _INTERFACE_HPP

/* Types and buffers needed from utility definitions. */
#include "Utility.hpp"

/* Interface subsystem status codes. */
enum InterfaceCode {
	SC_INTERFACE_GENERIC		= SC_BASE_INTERFACE,
	SC_INTERFACE_NO_DATA,
	SC_INTERFACE_BAD_DATA,
	SC_INTERFACE_UNKNOWN_TYPE,
	SC_INTERFACE_VALUE_RANGE,
	SC_INTERFACE_ITEM_MISSING,
	SC_INTERFACE_MISSING_FORMAT,
	SC_INTERFACE_MISSING_REF,
	SC_INTERFACE_ITEM_READ_ONLY,
};


/*** Format data type definitions. ***/

/* Boolean choice strings. */
struct InterfaceFormatBoolean {
	const char * label [2];
};

/* Index range and base format data. */
struct InterfaceFormatIndex {
	SysIndexRange range;
	SysIndex base;
};
const InterfaceFormatIndex InterfaceFormatIndexDefault = {{0, 9}, '0'};

/* Numeric min/max pointer parameter. */
struct InterfaceMinMax {
	const void * min_p;
	const void * max_p;
};

/* Fixed point decimal min/max pointer parameter. */
struct InterfaceFixed : public InterfaceMinMax {
	UBin8 decimal;
};

/* Enumerated value interface definitions. */
typedef UBin8 InterfaceEnum_t;
struct InterfaceFormatEnum {
	const InterfaceEnum_t * min_p;
	const InterfaceEnum_t * max_p;
	const char * const * list_p;
	const char * invalid;
	const char * const * alt_p;
	Boolean numeric;

				InterfaceFormatEnum (const InterfaceEnum_t * min_p,
									 const InterfaceEnum_t * max_p,
									 const char * const * list_p,
									 const char * invalid = "?",
									 Boolean numeric = YES,
									 const char * const * alt_p = NULL)
					: min_p (min_p), max_p (max_p), list_p (list_p),
						invalid (invalid), alt_p (alt_p), numeric (numeric) {}
};

/* Character to bit flags interface definitions. */
struct InterfaceFormatFlagsBit8 {
	const char character;
	const Bit8 flag;

				InterfaceFormatFlagsBit8 (char character = '\0', Bit8 flag = 0x00)
					: character (character), flag (flag) { }
};
struct InterfaceFormatFlagsBit16 {
	const char character;
	const Bit16 flag;

				InterfaceFormatFlagsBit16 (char character = '\0', Bit16 flag = 0x00)
					: character (character), flag (flag) { }
};
struct InterfaceFormatFlagsBit32 {
	const char character;
	const Bit32 flag;

				InterfaceFormatFlagsBit32 (char character = '\0', Bit32 flag = 0x00)
					: character (character), flag (flag) { }
};

/* Days of the week bit flags interface definitions. */
struct InterfaceFormatDays {
	const InterfaceFormatBoolean ** boolean;
	SysIndex ndays;

				InterfaceFormatDays (const InterfaceFormatBoolean ** boolean = NULL,
					SysIndex ndays = 7)
					: boolean (boolean), ndays (ndays) { }
};

/* Interface string length type. */
typedef size_t InterfaceFormatStringLength;

/* Standard field seperator character. */
const char InterfaceFieldSeparator = ',';

/* String quoting flag values. */
enum InterfaceStringMode {
	InterfaceStringNoQuote = 0,
	InterfaceStringQuote = 1,
};


/* Interface formatted data buffer class. */
class InterfaceBuffer : public Buffer {
	friend class InterfaceElement;

public:
	char * data_p;

public:
				InterfaceBuffer (size_t length = 64)
					: Buffer (length) { /*printf("Buffer = %d\n",length);*/Reset (); }

	void		ResetPtr (void) { data_p = (char *)data; }
	void		Reset (void) { Buffer::Reset (); ResetPtr (); }
	size_t		DataLeft (void) { return (size - (data_p - (char *)data)); }

	size_t		AddData (const Byte * new_data, size_t length)
					{
					//printf("AddData = %d \n",length);
					size_t len = Buffer::AddData (new_data, length);
					data_p = (char *)data + size;
					return (len);
					}
	size_t		AddString (const char * string, const char * separator = NULL)
					{
					//printf("AddString = %d %s\n",strlen(string),string);
					size_t len = Buffer::AddString (string, separator);
					data_p = (char *)data + size;
					return (len);
					}
	void		AddPad (size_t width, char pad = ' ');

	Code		StringGet (char * string_p, size_t size, size_t width = 0,
					char separator = InterfaceFieldSeparator);
	size_t		StringPut (const char * string_p,
					InterfaceStringMode mode = InterfaceStringNoQuote,
					size_t width = 0);
	Code        CharGet (char & value, const char * valid_chars = NULL);
	size_t      CharPut (const char value) { return (AddChar (value)); }
	Code        ByteGet (Byte & value);
	size_t      BytePut (const Byte value) { return (AddChar (value)); }
	Code        BooleanGet (Boolean & flags,
					const InterfaceFormatBoolean ** format_p = NULL,
					size_t width = 0);
	size_t		BooleanPut (Boolean flags,
					const InterfaceFormatBoolean ** format_p = NULL);
	Code     	IndexGet (SysIndex & index,
					const InterfaceFormatIndex * const format_p = NULL);
	size_t   	IndexPut (SysIndex index,
					const InterfaceFormatIndex * const format_p = NULL);
	Code        IntegerGet (Type_t type, void * value_p,
					const InterfaceMinMax * mm_p = NULL, size_t width = 0);
	size_t		IntegerPut (Type_t type, const void * value_p,
					const InterfaceMinMax * mm_p = NULL, size_t width = 0);

	Code        EnumCharGet (InterfaceEnum_t & state,
					const char * const template_p);
	size_t		EnumCharPut (InterfaceEnum_t state,
					const char * const template_p);
	Code        EnumLabelGet (InterfaceEnum_t & state,
					const InterfaceFormatEnum * const format_p);
	size_t		EnumLabelPut (InterfaceEnum_t state,
					const InterfaceFormatEnum * const format_p);
	Code        FlagsBit8Get (Bit8 & flags,
					const InterfaceFormatFlagsBit8 * template_p,
					size_t width = 0);
	size_t		FlagsBit8Put (Bit8 flags,
					const InterfaceFormatFlagsBit8 * template_p,
					size_t width = 0);
	Code        FlagsBit16Get (Bit16 & flags,
					const InterfaceFormatFlagsBit16 * template_p,
					size_t width = 0);
	size_t		FlagsBit16Put (Bit16 flags,
					const InterfaceFormatFlagsBit16 * template_p,
					size_t width = 0);
	Code        FlagsBit32Get (Bit32 & flags,
					const InterfaceFormatFlagsBit32 * template_p,
					size_t width = 0);
	size_t		FlagsBit32Put (Bit32 flags,
					const InterfaceFormatFlagsBit32 * template_p,
					size_t width = 0);
	Code        DaysGet (SysDayOfWeek & days, const InterfaceFormatDays * format_p,
					SysIndex ndays);
	SysIndex    DaysPut (SysDayOfWeek days, const InterfaceFormatDays * format_p,
					SysIndex ndays);
};


/* Interface element list class. */
class InterfaceElement {

public:
	Type_t type;
	Offset value_o;
	const void * format_p;
	Offset reference_o;

public:
				InterfaceElement (Type_t type = TYPE_NONE,
					Offset value_o = Offset (), const void * format_p = NULL,
					Offset reference_o = Offset ())
					: type (type), value_o (value_o), format_p (format_p),
						reference_o (reference_o) {};
	Code		ParseData (InterfaceBuffer & buffer, void * const obj_p) const;
	Code		FormatData (InterfaceBuffer & buffer, const void * const obj_p) const;
	SysDP       GetDecimal (void * const obj_p) const;

	Code		BaseParse (InterfaceBuffer & buffer, void * const obj_p) const;
	Code	    BaseFormat (InterfaceBuffer & buffer, const void * const obj_p) const;
	Code		ExtendParse (InterfaceBuffer & buffer, void * const obj_p) const;
	Code		ExtendFormat (InterfaceBuffer & buffer, const void * const obj_p) const;
	Code		SystemParse (InterfaceBuffer & buffer, void * const obj_p) const;
	Code		SystemFormat (InterfaceBuffer & buffer, const void * const obj_p) const;
};


/* External global data. */
extern const InterfaceFormatBoolean InterfaceBooleanLabel10;
extern const InterfaceFormatBoolean InterfaceBooleanLabelYN;
extern const InterfaceFormatBoolean InterfaceBooleanLabelTF;
extern const InterfaceFormatBoolean InterfaceBooleanLabelYesNo;
extern const InterfaceFormatBoolean InterfaceBooleanLabelTrueFalse;
extern const InterfaceFormatBoolean * InterfaceBooleanLabels [];
extern const InterfaceFormatBoolean * InterfaceBooleanChars [];

#define _INTERFACE_HPP
#endif

