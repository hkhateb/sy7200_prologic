#ifndef INTERFACEVALIDATEENTRYVECTOR_HPP
#define INTERFACEVALIDATEENTRYVECTOR_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "InterfaceValidateEntryHolder.hpp"
#include "TAVector.h"
using namespace std;

class InterfaceValidateEntryVector : public InterfaceValidateEntryHolder{
public:

	InterfaceValidateEntryVector();
	virtual ~InterfaceValidateEntryVector();
	virtual void add(SysValidateEntryItem);
	virtual void remove(SysValidateEntryItem);
	virtual int size();
	virtual void clear();
	virtual SysValidateEntryItem Get(int);

private:
    TAVector<SysValidateEntryItem> vect;
};

#endif // INTERFACEVALIDATEENTRYVECTOR_HPP
