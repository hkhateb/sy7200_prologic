/*
	Setup.hpp

	Data, class and function declarations for Setup objects.

	Copyright (c) 2004 - Time America, Inc.

	Written by:	  Alan J. Luse
	Written on:   12 May 2004
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime$
	     $Date: 2005/09/01 22:27:58 $

*/


#if ! defined SETUP_HPP
#define SETUP_HPP

/* Interface subsystem elements needed. */
#include "Interface.hpp"

/* Status codes for the Setup subsystem. */
typedef enum {
	SC_SETUP_GENERIC = SC_BASE_SETUP,
	SC_SETUP_NULL,
	SC_SETUP_NO_ITEM,
	}
SetupCode_e;


/* Setup item flags class. */
typedef Bit32 SetupElementFlagBits;
struct SetupElementFlags {
	union {
		SetupElementFlagBits all;
		struct {
			SetupElementFlagBits ReadOnly				: 1;
			SetupElementFlagBits						: 1;
			SetupElementFlagBits						: 1;
			SetupElementFlagBits						: 1;

			SetupElementFlagBits						: 12;

			SetupElementFlagBits SIA_ModuleMessage		: 1;
			SetupElementFlagBits SIA_ModuleBell		: 1;
			SetupElementFlagBits SIA_ModulePrint		: 1;
			SetupElementFlagBits SIA_ModuleAccess		: 1;

			SetupElementFlagBits SIA_ModuleProfile		: 1;
			SetupElementFlagBits SIA_ModuleValidate	: 1;
			SetupElementFlagBits						: 1;
			SetupElementFlagBits						: 1;

			SetupElementFlagBits						: 4;

			SetupElementFlagBits SIA_InterfaceTACWin3	: 1;
			SetupElementFlagBits SIA_InterfaceComm5	: 1;
			SetupElementFlagBits SIA_InterfaceTA		: 1;
			SetupElementFlagBits						: 1;
			}
		b;
		};

				SetupElementFlags (SetupElementFlagBits flags = 0x00000000)
					: all (flags) {};
	Boolean		MatchAny (SetupElementFlags flags) const
					{ return ((all & flags.all) ? YES : NO); };
	Boolean		MatchAll (SetupElementFlags flags) const
					{ return (
					((all & flags.all) == flags.all) ? YES : NO); };
};
static const SetupElementFlagBits SEF_None				= 0x00000000;
static const SetupElementFlagBits SEF_Base				= 0x00000000;
static const SetupElementFlagBits SEF_ReadOnly			= 0x00000001;
static const SetupElementFlagBits SEF_NoDisplay			= 0x00000002;
static const SetupElementFlagBits SEF_Modules			= 0x0FFF0000;
static const SetupElementFlagBits SEF_ModuleMessage		= 0x00010000;
static const SetupElementFlagBits SEF_ModuleBell		= 0x00020000;
static const SetupElementFlagBits SEF_ModulePrint		= 0x00040000;
static const SetupElementFlagBits SEF_ModuleAccess 		= 0x00080000;
static const SetupElementFlagBits SEF_ModuleProfile		= 0x00100000;
static const SetupElementFlagBits SEF_ModuleValidate	= 0x00200000;
static const SetupElementFlagBits SEF_ModuleFinger		= 0x00400000;
static const SetupElementFlagBits SEF_Interfaces		= 0xF0000000;   // Interface flags are currently for informational purposes only.
static const SetupElementFlagBits SEF_InterfaceTACWin3	= 0x10000000;	// Implemented in TACWin32 V 3.0.
static const SetupElementFlagBits SEF_InterfaceComm5	= 0x20000000;	// Implemented in Comm5 interface.
static const SetupElementFlagBits SEF_InterfaceTA_i		= 0x40000000;	// TA500 firmware integer item.
static const SetupElementFlagBits SEF_InterfaceTA_c		= 0x40000000;	// TA500 firmware character item.
static const SetupElementFlagBits SEF_InterfaceTA_s		= 0x40000000;	// TA500 firmware special item.
static const SetupElementFlagBits SEF_InterfaceAcro		= 0x80000000;	// AcroComm compatibility ext.


/* SetupAction object class declaration. */
class SetupElement : public InterfaceElement {
	const char * tag;
	const char * legacy;
	SetupElementFlags flags;

public:
				SetupElement (const char * tag = NULL,
						   const char * legacy = NULL,
						   SetupElementFlags flags = 0x0000,
						   Type_t type = TYPE_NONE,
						   Offset value_o = Offset (),
						   const void * format_p = NULL,
						   Offset reference_o = Offset ())
					: InterfaceElement (type, value_o, format_p, reference_o),
						tag (tag), legacy (legacy), flags (flags) {};

	Boolean		Valid (void) const {return (tag != NULL); };
	const SetupElement *
				Lookup (const char * tag) const;
	const SetupElement *
				LookupLegacy (const char * tag) const;
	void		ListLegacy (FILE * stream, const void * const data_p,
					Boolean mode = YES) const;
};


/* Global external tag lists. */
extern const SetupElement SetupTagsFunction [];
extern const SetupElement SetupTagsConfig [];
extern const SetupElement SetupTagsProgram [];

#endif

