#ifndef INTERFACEACCESSCONTROLHOLDER_HPP
#define INTERFACEACCESSCONTROLHOLDER_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "System.hpp"

class InterfaceAccessControlHolder{
public:

	InterfaceAccessControlHolder(){}
	virtual ~InterfaceAccessControlHolder(){}
	virtual void add(SysAccessBadgeItem)=0;
	virtual void remove(SysAccessBadgeItem)=0;
	virtual int size()=0;
	virtual void clear()=0;
	virtual SysAccessBadgeItem Get(int)=0;
};

#endif // INTERFACEACCESSCONTROLHOLDER_HPP
