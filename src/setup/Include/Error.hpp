/*
	Error.h:

	Standard definitions for the error status code functions.

	Copyright (c) 1991-1992,2000 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   09 Mar 2000
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   27 Nov 2000 18:02:58  $
	     $Date: 2005/09/01 22:27:58 $

*/


#if ! defined ERROR_HPP

/* Function declaration for error notification function entry. */
typedef const char *
			(* ErrorTextEntry) (Code status_code);

/* General external function prototypes. */
const char *
			ErrorTextGet (Code code);
ErrorTextEntry
			ErrorTextFirst (ErrorTextEntry const error_text_entry);
ErrorTextEntry
			ErrorTextLast (ErrorTextEntry const error_text_entry);
const char *
			ErrorTextCheck (Code status_code);
const char *
			ErrorTextGeneric (Code status_code);
const char *
			ErrorTextLibrary (Code status_code);
const char *	
			ErrorTextCLib (Code status_code);

const char *
			ErrorSystem (Code code);

#define ERROR_HPP
#endif


