#ifndef _SYSTEMGLOBALS_H_
#define _SYSTEMGLOBALS_H_

/**
 * ----------------------------------------------------------------
 * ----------------  DEFAULT TCP PORT  ----------------------------
 * ----------------------------------------------------------------
 */
#define DEFAULT_TCP_PORT 3000;

/**
 * ----------------------------------------------------------------
 * ----------------  SYSTEM PATHS  --------------------------------
 * ----------------------------------------------------------------
 */
// Install Directory
#define TA7000_HOME 				"./"

// Clock Program File
#define CLK_PROGRAM_FILE	 		TA7000_HOME "program.clk"

// Used when server programs the clock
#define TMP_CLK_PROGRAM_FILE 		TA7000_HOME "programtmp.clk"
#define BACKUP_CLK_PROGRAM_FILE 	TA7000_HOME "programbackup.clk"

// All transactions are recorded in this file
#define TRANSACTIONS_FILE 			TA7000_HOME "transactions.clk"

// Used when a Transaction is removed from the main transaction file
#define TRANSACTIONS_TMP_FILE 		TA7000_HOME "transactionstmp.clk"

// Clock Transactions that occured during polling are written to this file
#define POLL_TRANSACTIONS_FILE 		TA7000_HOME "polltransactions.clk"

// Resgistry File
#define XML_PROPERTIES_FILE			TA7000_HOME "properties.xml"
#define TMP_XML_PROPERTIES_FILE 		TA7000_HOME "propertiestmp.clk"

// Network Interfaces
#define INTERFACES 					"/mnt/flash/terminal/network/interfaces"

// Used during Clock Reset
#define TA7000_DEFAULTS_DIR			TA7000_HOME	"defaults/"

#define DEFAULT_INTERFACES			TA7000_DEFAULTS_DIR "interfaces"

#define DEFAULT_CLK_PROGRAM_FILE	TA7000_DEFAULTS_DIR "program.clk"

#define DEFAULT_XML_PROPERTIES_FILE	TA7000_DEFAULTS_DIR "properties.xml"

/**
 * ----------------------------------------------------------------
 * ----------------  XML PROPERTIES  ------------------------------
 * ----------------------------------------------------------------
 */

// TCP Port
#define PROPERTY_PORT 				"port"

// Configuration Badge
#define PROPERTY_ACCESS_CODE 		"accesscode"

// Configuration Badge - Reset Only
#define PROPERTY_RESET_ACCESS_CODE	"resetaccesscode"


// Enable / Disable Configuration Badge (not used right now)
#define PROPERTY_ACCS_CODE_ENABLE 	"accscodeenable"

//WatchDog
#define PROPERTY_WATCHDOG 	"watchdog"
#define PROPERTY_AUTORSTRT 	"autorestart"

// ESSID
#define PROPERTY_ESSID		 		"essid"
#define PROPERTY_WEP_TYPE		 	"weptype"
#define PROPERTY_WEP_KEY		 	"wepkey"
#define PROPERTY_WEP_TYPE_64		"64"
#define PROPERTY_WEP_TYPE_128		"128"
#define PROPERTY_WEP_TYPE_NONE		"none"
#define PROPERTY_WIRELESS_ADDRTYPE	"wirelessaddrtype"
#define PROPERTY_WIRELESS_IPADDR	"wirelessipaddr"
#define PROPERTY_WIRELESS_NETMASK	"wirelessnetmask"
#define PROPERTY_ADDRTYEPE_DHCP		"dhcp"
#define PROPERTY_ADDRTYEPE_STATIC	"static"

#endif //_SYSTEMGLOBALS_H_
