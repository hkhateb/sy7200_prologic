/*
	str_def.h:

	Standard definitions and function prototypes for supplementary string
functions.

	Copyright (c) 1992-1994,2000 - Alcom Communications

	Written by:   Alan J. Luse
	Written on:   10 Sep 1992
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   20 Jun 2001 14:57:04  $
	     $Date: 2005/09/01 22:27:58 $

*/

#if ! defined _STR_DEF

#ifdef __cplusplus
extern "C" {
#endif

/* Include standard definitions for size_t, etc. */
#include <stddef.h>

/* Character to name field function type and data structure. */
typedef unsigned char StrCharValue;
typedef struct StrChar_t {
	StrCharValue value;
	char * name;
}
StrChar_t;

/* String cross sectional index data type. */
typedef int StringCSAIndex;

/* String cross sectional array bounds range and special data structure. */
typedef struct StringCSABounds {
	StringCSAIndex low, high;
	struct {
		const char * string;
		StringCSAIndex value;
		}
	special;
	}
StringCSABoundsData;


/* External function prototypes for string subsystem. */
char *	str_lower (char * string);
char *	str_upper (char * string);

char *	str_blank (const char * string);
char *	str_clip (char * string);

char *	str_unquote (char * string);
char *	str_parse (const char ** str_pp, size_t * length_p);
int		str_split (const char * string, char * list);
int		str_argv (const char ** argv, const char * list, int count,
			const char * initial);

const char *
		str_char_lookup (const StrChar_t * sc_list, StrCharValue value);
StrCharValue
		str_char_value (const StrChar_t * sc_list, const char * name);
		
/**
 * Check if the character can be trimmed
 */
bool IsTrimChar(char ch);

/**
 * Trim the String
 */
char * TrimString(char * src);

#ifdef __cplusplus
}
#endif

#define _STR_DEF
#endif

