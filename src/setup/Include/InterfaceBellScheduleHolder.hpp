#ifndef INTERFACEBELLSCHEDULEHOLDER_HPP
#define INTERFACEBELLSCHEDULEHOLDER_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "System.hpp"

class InterfaceBellScheduleHolder{
public:

	InterfaceBellScheduleHolder(){}
	virtual ~InterfaceBellScheduleHolder(){}
	virtual void add(SysBellScheduleItem)=0;
	virtual void remove(SysBellScheduleItem)=0;
	virtual int size()=0;
	virtual void clear()=0;
	virtual SysBellScheduleItem Get(int)=0;
};

#endif // INTERFACEBELLSCHEDULEHOLDER_HPP
