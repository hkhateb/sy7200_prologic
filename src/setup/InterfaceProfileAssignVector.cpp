#include "InterfaceProfileAssignVector.hpp"

InterfaceProfileAssignVector::InterfaceProfileAssignVector()
{
}

InterfaceProfileAssignVector::~InterfaceProfileAssignVector()
{
}

void InterfaceProfileAssignVector::add(SysProfileAssignItem assign)
{
    vect.push_back( assign );
}

void InterfaceProfileAssignVector::remove(SysProfileAssignItem item)
{
    
}

int InterfaceProfileAssignVector::size()
{
    return vect.size();
}

void InterfaceProfileAssignVector::clear()
{
    vect.clear();
}

SysProfileAssignItem InterfaceProfileAssignVector::Get(int index)
{
    SysProfileAssignItem& assign = *vect[index];
    return assign;
}


