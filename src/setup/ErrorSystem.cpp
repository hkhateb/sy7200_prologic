/*
	ErrorSystem:  [Module]

  	The ErrorSystem module returns text messages for system level
software status codes.

	Copyright (c) 1992-2001 - Alcom Communications

	Written by:   Alan J. Luse
	Written on:   27 Nov 2000
	  Based on:   MP90_Error originally written on 11 Oct 1989
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   28 Dec 2001 13:16:42  $
	     $Date: 2005/09/01 22:27:58 $

Description:

	The allowed system status code values are defined in a variety of
header include files.  Codes associated with each subsystem are defined in
the corresponding subsystem external definitions include file (e.g.
Unit.hpp for the display subsystem).  System wide status codes are
defined the the system include file System.hpp.  Status codes for
certain library subsystems and modules are defined in their external
definitions include file.

Notes:

	The allocation of system status code values is done in such a way
that all status codes for the system and the standard libraries are
unique values.  The allocation of values for different groups of
status codes is described in the System Status Codes design note
contained in the Alcom library documentation.  (See the Generic.hpp
generic definitions include file and the System.hpp system
definitions include file to see how this uniqueness is established).

*/


/* Include files for ErrorSystem. */
#include "System.hpp"

#include "Error.hpp"
#include "Setup.hpp"
#include "Legacy.hpp"

/* Constant parameters for ErrorSystem. */

/* Type and structure definitions for ErrorSystem. */

/* Global data for ErrorSystem. */

/* Function prototypes for ErrorSystem. */


/*
	ErrorSystem:

	Return the message string describing associated with a specified
system status code.

Description:

	Check for system status code values and return a status message
string if a match is found.

Notes:

	A NULL pointer value is returned if the supplied system status code
does not have a message defined here.

	The text message is returned via a pointer to a constant and
persistent text string.  The text string may be used indefinitely
without making a copy of it.

See also:  ErrorText

Arguments:
	text_ptr		Text subsystem object handle.

	code			System status code whose associated text
					message is to be returned.

Returns:
	const char *
					Character string pointer to the associated text message
					if defined; otherwise NULL.

*/

		const char *
ErrorSystem (Code code)

{
	char * msg;

	switch (code)
		{

	case SC_OPTION_BAD:
		msg = "Option prefix without option.";
		break;
	case SC_OPTION_VALUE:
		msg = "Value missing from option.";
		break;
	case SC_OPTION_UNKNOWN:
		msg = "Unknown option.";
		break;

	case SC_SETUP_GENERIC:
		msg = "Unknown setup subsystem error.";
		break;
	case SC_SETUP_NULL:
		msg = "Uninitialized setup object.";
		break;
	case SC_SETUP_NO_ITEM:
		msg = "Item not found.";
		break;

	case SC_INTERFACE_GENERIC:
		msg = "Unknown Interface subsystem error.";
		break;
	case SC_INTERFACE_NO_DATA:
		msg = "No interface data left.";
		break;
	case SC_INTERFACE_BAD_DATA:
		msg = "Bad interface data form.";
		break;
	case SC_INTERFACE_UNKNOWN_TYPE:
		msg = "Unknown interface data type.";
		break;
	case SC_INTERFACE_VALUE_RANGE:
		msg = "Interface data value out of range.";
		break;
	case SC_INTERFACE_ITEM_MISSING:
		msg = "Interface item not found.";
		break;
	case SC_INTERFACE_MISSING_FORMAT:
		msg = "Interface element format data missing.";
		break;
	case SC_INTERFACE_MISSING_REF:
		msg = "Interface element reference value missing.";
		break;
	case SC_INTERFACE_ITEM_READ_ONLY:
		msg = "Interface item is read only.";
		break;

	case SC_LEGACY_GENERIC:
		msg = "Unknown Legacy subsystem error.";
		break;
	case SC_LEGACY_NULL:
		msg = "Uninitialized legacy object.";
		break;
	case SC_LEGACY_EMPTY:
		msg = "Program line is empty or a comment.";
		break;
	case SC_LEGACY_FORMAT:
		msg = "Invalid legacy input format (missing '=').";
		break;
	case SC_LEGACY_TAG_FORM:
		msg = "Improper legacy tag form; not 2 characters.";
		break;

	default:
		msg = NULL;
 		}

	return (msg);
}


