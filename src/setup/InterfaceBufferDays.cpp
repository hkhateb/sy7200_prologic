/*
	InterfaceBuffer::Days:  [Module]

	Interface buffer subsystem day of week data type functions.

	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   30 Aug 2004
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   20 Apr 2004 22:27:00  $
	     $Date: 2005/09/01 22:27:58 $

Description:

	The InterfaceBuffer::Days module provides functions to map day of the week
bit flags to or from string formats.

*/


/* Include files for InterfaceBuffer::Days. */
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "System.hpp"
#include "Interface.hpp"

/* Constant parameters for InterfaceBuffer::Days. */

/* Type and structure definitions for InterfaceBuffer::Days. */

/* Internal static data for InterfaceBuffer::Days. */

/* External global data for InterfaceBuffer::Days. */

/* Function prototypes for InterfaceBuffer::Days. */


/*
	InterfaceBuffer::DaysGet:

	Get a days of the week bit field from the data buffer.

Description:

	Each sucessive character in the input data is converted to a bit flag in
the days of the week data value.  Data can be a 7 day week or can include
an 'eighth' day for holiday use.

Notes:

	If an input character does not match any character in the Boolean
characters format template list the message data is considered bad and
reported without making any value update.

	The case of characters in the format data and the input stream has no
effect on this function.

See also:  InterfaceBuffer::DaysCharPut

Arguments:
	days		Reference to SysDayOfWeek value to set based on buffer data.
	
	format_p	Pointer to an array of InterfaceFormatBoolean template
				data pointers containing Boolean value names correspondence.

	ndays		Number of data bytes to examine:
					0 use value in format data;
					5 for week days only;
					7 for days of week only;
					8 for additional holiday flag.

Returns:
	Code		SC_OKAY if a valid days field was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::DaysGet (SysDayOfWeek & days,
	const InterfaceFormatDays * format_p, SysIndex ndays)

{
	Code code = SC_OKAY;
	SysDayOfWeek new_days = 0x00;

	/* If number of days not specified use format data value;
		if value is not valid assume standard 7 day week. */
	if (ndays == 0) ndays = format_p->ndays;
	if (ndays != 5 AND ndays != 7 AND ndays != 8) ndays = 7;

	SysDayOfWeek mask;
	SysIndex nidx;
	for (mask = SysDayMonday, nidx = 0; nidx < ndays; mask <<= 1, ++nidx) {
		Boolean flag;

		code = BooleanGet (flag, format_p->boolean, 1);
		if (code != SC_OKAY) break;

		if (flag) new_days |= mask;
		else new_days &= ~mask;

		}

	/* Set new days value if everything's okay. */
	if (code == SC_OKAY) days = new_days;

	return (code);
}


/*
	InterfaceBuffer::DaysPut:

	Translate a days of the week bit field into Boolean character codes in
the data buffer.

Description:

	For each sucessive bit in the days of the week bit field place a matching
Boolean character flag in the output buffer.  Data can be a 7 day week or
can include an 'eighth' day for holiday use.

Notes:

	Character values in the format data are used directly to set the message
field; no uppercase or lowercase conversion is performed.

See also:  InterfaceBuffer::DaysGet

Arguments:
	days		SysDayOfWeek value to use to place days flags in data buffer.

	format_p	Pointer to an array of InterfaceFormatBoolean template
				data pointers containing Boolean value names correspondence.

	ndays		Number of data bytes to examine:
					0 use value in format data;
					5 for week days only;
					7 for days of week only;
					8 for additional holiday flag.

Returns:
	SysIndex		Number of bytes added to message data string.

*/

		SysIndex
InterfaceBuffer::DaysPut (SysDayOfWeek days,
	const InterfaceFormatDays * format_p, SysIndex ndays)

{
	size_t length = 0;

	/* If number of days not specified use format data value;
		if value is not valid assume standard 7 day week. */
	if (ndays == 0) ndays = format_p->ndays;
	if (ndays != 5 AND ndays != 7 AND ndays != 8) ndays = 7;

	SysDayOfWeek mask;
	SysIndex nidx;
	for (mask = SysDayMonday, nidx = 0; nidx < ndays; mask <<= 1, ++nidx) {
		length += BooleanPut ((Boolean)(days & mask), format_p->boolean);
		}

	return (length);
}


