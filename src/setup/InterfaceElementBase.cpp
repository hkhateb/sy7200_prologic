/*
	InterfaceElement::Base:  [Class]

	The InterfaceElement::Base module provides basic data item format and
parse.

	Copyright (c) 2000-2002 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   18 Aug 2004
	  Based on:   portions of MsgGen*.c originally 27 Nov 2000
	 $Revision: 1.2 $
	   $Author: deepali $
	  $Modtime:   19 Apr 2002 14:09:20  $
	     $Date: 2006/02/15 22:03:01 $

Description:

	This module provides basic data type parse and format member functions for
manipulating InterfaceElement objects.

##############################################################################
######################## Change History ######################################
##############################################################################
Author			Date		Comments
Mahesh S Rai	06/24/05	Changed InterfaceElement::BaseFormat(), to format 	
							Strings without quotes
##############################################################################
*/


/* Compilation options for InterfaceElement::Base. */

/* Include files for InterfaceElement::Base. */
#include <string.h>
#include <stdlib.h>
#include "System.hpp"
#include "Interface.hpp"

/* Constant parameters for InterfaceElement::Base. */

/* Type and structure definitions for InterfaceElement::Base. */

/* Internal static data for InterfaceElement::Base. */

/* Function prototypes for InterfaceElement::Base. */


/*
	InterfaceElement::BaseParse:

	Parse an InterfaceElement from the Buffer provided.

Description:

	Convert the supplied item data to the requested InterfaceElement
format using the data type, value, and format data supplied.

See also:  InterfaceElement::BaseFormat

Arguments:
	buffer		Reference to an InterfaceBuffer from which formatted data
				is parsed.

	obj_p		Pointer to object or region containing data for element.

Returns:
	Code		Base status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;
				SC_INTERFACE_BAD_DATA if item data could not be parsed;

*/

		Code
InterfaceElement::BaseParse (InterfaceBuffer & buffer, void * const obj_p) const

{
	Code code = SC_INTERFACE_BAD_DATA;

	switch (type)
		{

	case TYPE_NONE:
		// Always ignore this data type without error.
		code = SC_OKAY;
		break;
		
	case TYPE_String:
	case TYPE_StringPtr:
		{
		char * value_p;
		if (type == TYPE_String) value_p = (char *)value_o.Get (obj_p);
		else value_p = *(char **)value_o.Get (obj_p);
		*value_p = '\0';
		if (*buffer.data_p == '\0')
			{
			// Empty field yields null string so just exit.
			code = SC_OKAY;
			break;
			}
		code = buffer.StringGet (value_p, (format_p == NULL) ?
			0 : *(InterfaceFormatStringLength *)(format_p));
		}
		code = SC_OKAY;
		break;

	case TYPE_Char:
		{
		char * value_p = (char *)value_o.Get (obj_p);
		code = buffer.CharGet (*value_p, (const char *)format_p);
		}
		if(code == SC_INTERFACE_NO_DATA) code = SC_OKAY;
		
		break;

	case TYPE_CharNum:			// Character numeric index: 'A'=1,'B'=2,...
		{
		UBin8 value;
		if (*buffer.data_p == '\0')	value = 0;
		else {
			unsigned char base;
			// Use base of 'A' unless another base value is specified.
			if (format_p == NULL) base = 'A';
			else base = *(unsigned char *)format_p;
			unsigned char index = *buffer.data_p;
			if (index < base) break;
			value = index - base;
			}
		UBin8 * value_p = (UBin8 *)value_o.Get (obj_p);
		*value_p = value;
		}
		code = SC_OKAY;
		break;

	case TYPE_Boolean:
		{
		Boolean value;
		code = buffer.BooleanGet (value,
			(const InterfaceFormatBoolean ** )format_p);
		if (code != SC_OKAY) break;
		*(Boolean *)value_o.Get (obj_p) = value;
		}
		code = SC_OKAY;
		break;

	case TYPE_Bit8:
	case TYPE_Bit16:
	case TYPE_SBin7:
	case TYPE_SBin15:
	case TYPE_SBin31:
	case TYPE_UBin8:
	case TYPE_UBin16:
	case TYPE_UBin32:
		if (*buffer.data_p == '\0')	break;
		{
		void * value_p = value_o.Get (obj_p);
		code = buffer.IntegerGet (type, value_p,
			(const InterfaceMinMax *)format_p);
		}
		break;

	default:
		code = SC_INTERFACE_UNKNOWN_TYPE;
		break;
		}

	return(code);
}


/*
	InterfaceElement::BaseFormat:

	Format an InterfaceElement into the Buffer provided.

Description:

	Convert the supplied data element to the requested InterfaceElement
format using the data type, value, and format data supplied.

See also:  InterfaceElement::BaseParse

Arguments:
	buffer		Reference to an InterfaceBuffer in which formatted data
				is stored.

	obj_p		Pointer to object or region containing data for element.

Returns:
	Code		Base status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;

*/

		Code
InterfaceElement::BaseFormat (InterfaceBuffer & buffer, const void * const obj_p) const

{
	Code code = SC_OKAY;
	size_t length = 0;

	switch (type)
		{

	case TYPE_String:
		buffer.StringPut ((char *)value_o.Get (obj_p), InterfaceStringNoQuote);
		break;

	case TYPE_StringPtr:
		buffer.StringPut (*(char **)value_o.Get (obj_p), InterfaceStringNoQuote);
		break;

	case TYPE_Char:
		buffer.CharPut (*(char *)value_o.Get (obj_p));
		break;

	case TYPE_CharNum:			// Character numeric index: 'A'=1,'B'=2,...
		{
		unsigned char base;
		if (format_p == NULL) base = 'A';
		else base = *(unsigned char *)format_p;
		unsigned char value = *(UBin8 *)value_o.Get (obj_p);
		length = sprintf (buffer.data_p, "%c", value + base);
		}
		break;

	case TYPE_Boolean:
		{
		buffer.BooleanPut (*(Boolean *)value_o.Get (obj_p) ? 1 : 0,
			(const InterfaceFormatBoolean ** )format_p);
		}
		break;

	case TYPE_Bit8:
	case TYPE_Bit16:
	case TYPE_SBin7:
	case TYPE_SBin15:
	case TYPE_SBin31:
	case TYPE_UBin8:
	case TYPE_UBin16:
	case TYPE_UBin32:
		{
		const void * value_p = value_o.Get (obj_p);
		buffer.IntegerPut (type, value_p, (const InterfaceMinMax *)format_p);
		}
		break;


	default:
		code = SC_INTERFACE_UNKNOWN_TYPE;
		break;
		}

	if (length != 0) buffer.size += length;
	buffer.data_p = buffer.data_p + buffer.size;

	return (code);
}


