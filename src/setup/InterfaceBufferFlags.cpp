/*
	InterfaceBuffer::Flags:  [Module]

	Interface buffer subsystem bit flags data type functions.

	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   23 Aug 2004
	 $Revision: 1.2 $
	   $Author: deepali $
	  $Modtime:   20 Apr 2004 22:27:00  $
	     $Date: 2006/02/15 22:06:26 $

Description:

	The InterfaceBuffer::Flags module provides functions to map character flags
in a string to bit flags.

*/


/* Include files for InterfaceBuffer::Flags. */
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "System.hpp"
#include "Interface.hpp"

/* Constant parameters for InterfaceBuffer::Flags. */

/* Type and structure definitions for InterfaceBuffer::Flags. */

/* Internal static data for InterfaceBuffer::Flags. */

/* External global data for InterfaceBuffer::Flags. */

/* Function prototypes for InterfaceBuffer::Flags. */


/*
	InterfaceBuffer::FlagsBit8Get:

	Get a flags bit field from the data buffer.

Description:

	Each possible character in the input data is converted to a bit flag in
the eight bit data value.  Bit flags default to zero or Off and get set to
1 or On if their corresponding character is present in the data string.

Examples:

	If the character template is "s:0x01, k:0x02 w:0x04" then a message field
of "sk" will be returned as the value 0x01 | 0x02 => 0x03.

Notes:

	If an input character does not match any character in the flags template
list the message data is considered bad and reported without making any
value update.

	The case of characters in the template data and the input stream has no
effect on this function.

	More than one template entry can be used for a single bit flag.  Thus
both 'B' and 'W' can be configured to set the same bit flag.  A single
character flag can also set multiple bit flags such a 'D' setting default
flags or 'A' setting all valid flags.

See also:  InterfaceBuffer::FlagsBit8CharPut

Arguments:
	flags		Reference to bit field value to set based on buffer data.
	
	template_p	Pointer to InterfaceFormatFlagsBit8 template data containing
				bit flag and character flag correspondence.

	width		Maximum number of data bytes to examine;
				0 to examine rest of data buffer.

Returns:
	Code		SC_OKAY if a valid flags field was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::FlagsBit8Get (Bit8 & flags,
	const InterfaceFormatFlagsBit8 * template_p, size_t width)

{
	Code code = SC_OKAY;
	Bit8 new_flags = 0x00;

	/* If width not specified use remaining length of data. */
	if (width == 0) width = size - ((Byte *)data_p - data);

	for (; code == SC_OKAY; data_p++) {

		/* No more data so stop now. */
		if (*data_p == '\0') break;
	
		/* Only examine width bytes. */
		if (width-- == 0) break;

		/* Scan template list to locate next character. */
		const InterfaceFormatFlagsBit8 * tp;
		for (tp = template_p; ; tp++) {

			if (tolower (*data_p) == tolower (tp->character)) {
				new_flags |= tp->flag;
				break;
				}

			if (tp->character == '\0') {
				code = SC_INTERFACE_BAD_DATA;
				break;
				}
			}
	
		}

	/* Set new flags value if everything's okay. */
	if (code == SC_OKAY) flags = new_flags;

	return (code);
}


/*
	InterfaceBuffer::FlagsBit8Put:

	Translate a bit field of flags into character codes in the data buffer.

Description:

	For each bit flag in the bit field value that is set to one or ON the
corresponding character in the template data is added to the output buffer.

Examples:

	If the character template is "S:0x01, K:0x02 W:0x04" then a bit field
value of 0x03 yields a data buffer output of "SK"

Notes:

	Character values in the template data are used directly to set the
message field; no uppercase or lowercase conversion is performed.

	More than one template entry can exist for each bit flag.  Thus
both 'B' and 'W' can be configured to match the same bit flag; the first
template entry encountered that covers a particular bit flag will be used
for display.  Thus if both 'B' and 'W' specify the same bit flag and 'B'
is included first in the template list that bit will be displayed as 'B'
regardless of the character used to set the bit.

	If a single character flag specifies multiple bit flags, such a 'D' for
a set of default flags, that setting will display only if all its bits are
still uncovered at the time that template entry is encountered in the
scan.  Thus if a multiple bit template entry is early in the list the
multiple bit character will be used in preference to the its single bit
characters that may occur later in the template list.  Conversely if the
single bit flags are listed first they will be used in preference to the
multiple bit item.

See also:  InterfaceBuffer::FlagsBit8Get

Arguments:
	flags		Bit8 value to use to place character flags in data buffer.

	template_p	Pointer to InterfaceFormatFlagsBit8 template data containing
				bit flag and character flag correspondence.

	width		Number of data bytes to output; pad with trailing spaces
				to reach width if needed.

Returns:
	size_t		Number of bytes added to message data string.

*/

		size_t
InterfaceBuffer::FlagsBit8Put (Bit8 flags,
	const InterfaceFormatFlagsBit8 * template_p, size_t width)

{
	size_t length = 0;
	Bit8 flags_left = flags;

	/* Scan template list to locate next character. */
	const InterfaceFormatFlagsBit8 * tp;
	for (tp = template_p; tp->character != '\0'; tp++) {
		
		if (width != 0 AND length >= width) break;

		/* If template flag(s) are in flags then add character to buffer. */
		if ((flags_left & tp->flag) == tp->flag) {
			AddChar (tp->character);
			length++;
			flags_left &= ~tp->flag;
		}

	}

	/* Pad field to requested width with spaces. */
	//AddPad (width);
	for (; length < width; length ++)
		AddChar (' '); 
	
	return (length);
}


/*
	InterfaceBuffer::FlagsBit16Get:

	Get a flags bit field from the data buffer.

Description:

	Each possible character in the input data is converted to a bit flag in
the eight bit data value.  Bit flags default to zero or Off and get set to
1 or On if their corresponding character is present in the data string.

Examples:

	If the character template is "s:0x01, k:0x02 w:0x04" then a message field
of "sk" will be returned as the value 0x01 | 0x02 => 0x03.

Notes:

	If an input character does not match any character in the flags template
list the message data is considered bad and reported without making any
value update.

	The case of characters in the template data and the input stream has no
effect on this function.

	More than one template entry can be used for a single bit flag.  Thus
both 'B' and 'W' can be configured to set the same bit flag.  A single
character flag can also set multiple bit flags such a 'D' setting default
flags or 'A' setting all valid flags.

See also:  InterfaceBuffer::FlagsBit16CharPut

Arguments:
	flags		Reference to bit field value to set based on buffer data.
	
	template_p	Pointer to InterfaceFormatFlagsBit16 template data containing
				bit flag and character flag correspondence.

	width		Maximum number of data bytes to examine;
				0 to examine rest of data buffer.

Returns:
	Code		SC_OKAY if a valid flags field was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::FlagsBit16Get (Bit16 & flags,
	const InterfaceFormatFlagsBit16 * template_p, size_t width)

{
	Code code = SC_OKAY;
	Bit16 new_flags = 0x00;

	/* If width not specified use remaining length of data. */
	if (width == 0) width = size - ((Byte *)data_p - data);

	for (; code == SC_OKAY; data_p++) {

		/* No more data so stop now. */
		if (*data_p == '\0') break;
	
		/* Only examine width bytes. */
		if (width-- == 0) break;

		/* Scan template list to locate next character. */
		const InterfaceFormatFlagsBit16 * tp;
		for (tp = template_p; ; tp++) {

			if (tolower (*data_p) == tolower (tp->character)) {
				new_flags |= tp->flag;
				break;
				}

			if (tp->character == '\0') {
				code = SC_INTERFACE_BAD_DATA;
				break;
				}
			}
	
		}

	/* Set new flags value if everything's okay. */
	if (code == SC_OKAY) flags = new_flags;

	return (code);
}


/*
	InterfaceBuffer::FlagsBit16Put:

	Translate a bit field of flags into character codes in the data buffer.

Description:

	For each bit flag in the bit field value that is set to one or ON the
corresponding character in the template data is added to the output buffer.

Examples:

	If the character template is "S:0x01, K:0x02 W:0x04" then a bit field
value of 0x03 yields a data buffer output of "SK"

Notes:

	Character values in the template data are used directly to set the
message field; no uppercase or lowercase conversion is performed.

	More than one template entry can exist for each bit flag.  Thus
both 'B' and 'W' can be configured to match the same bit flag; the first
template entry encountered that covers a particular bit flag will be used
for display.  Thus if both 'B' and 'W' specify the same bit flag and 'B'
is included first in the template list that bit will be displayed as 'B'
regardless of the character used to set the bit.

	If a single character flag specifies multiple bit flags, such a 'D' for
a set of default flags, that setting will display only if all its bits are
still uncovered at the time that template entry is encountered in the
scan.  Thus if a multiple bit template entry is early in the list the
multiple bit character will be used in preference to the its single bit
characters that may occur later in the template list.  Conversely if the
single bit flags are listed first they will be used in preference to the
multiple bit item.

See also:  InterfaceBuffer::FlagsBit16Get

Arguments:
	flags		Bit16 value to use to place character flags in data buffer.

	template_p	Pointer to InterfaceFormatFlagsBit16 template data containing
				bit flag and character flag correspondence.

	width		Number of data bytes to output; pad with trailing spaces
				to reach width if needed.

Returns:
	size_t		Number of bytes added to message data string.

*/

		size_t
InterfaceBuffer::FlagsBit16Put (Bit16 flags,
	const InterfaceFormatFlagsBit16 * template_p, size_t width)

{
	size_t length = 0;
	Bit16 flags_left = flags;

	/* Scan template list to locate next character. */
	const InterfaceFormatFlagsBit16 * tp;
	for (tp = template_p; flags_left AND tp->character != '\0'; tp++) {

		if (width != 0 AND length >= width) break;

		/* If template flag(s) are in flags then add character to buffer. */
		if ((flags_left & tp->flag) == tp->flag) {
			AddChar (tp->character);
			length++;
			flags_left &= ~tp->flag;
			}

		}

	/* Pad field to requested width with spaces. */
	while (width > length) {
		AddChar (' ');
		length++;
		}

	return (length);
}


/*
	InterfaceBuffer::FlagsBit32Get:

	Get a flags bit field from the data buffer.

Description:

	Each possible character in the input data is converted to a bit flag in
the eight bit data value.  Bit flags default to zero or Off and get set to
1 or On if their corresponding character is present in the data string.

Examples:

	If the character template is "s:0x01, k:0x02 w:0x04" then a message field
of "sk" will be returned as the value 0x01 | 0x02 => 0x03.

Notes:

	If an input character does not match any character in the flags template
list the message data is considered bad and reported without making any
value update.

	The case of characters in the template data and the input stream has no
effect on this function.

	More than one template entry can be used for a single bit flag.  Thus
both 'B' and 'W' can be configured to set the same bit flag.  A single
character flag can also set multiple bit flags such a 'D' setting default
flags or 'A' setting all valid flags.

See also:  InterfaceBuffer::FlagsBit32CharPut

Arguments:
	flags		Reference to bit field value to set based on buffer data.
	
	template_p	Pointer to InterfaceFormatFlagsBit32 template data containing
				bit flag and character flag correspondence.

	width		Maximum number of data bytes to examine;
				0 to examine rest of data buffer.

Returns:
	Code		SC_OKAY if a valid flags field was found;
				SC_INTERFACE_BAD_DATA if bad or missing data is found.

*/

		Code
InterfaceBuffer::FlagsBit32Get (Bit32 & flags,
	const InterfaceFormatFlagsBit32 * template_p, size_t width)

{
	Code code = SC_OKAY;
	Bit32 new_flags = 0x00;

	/* If width not specified use remaining length of data. */
	if (width == 0) width = size - ((Byte *)data_p - data);

	for (; code == SC_OKAY; data_p++) {

		/* No more data so stop now. */
		if (*data_p == '\0') break;
	
		/* Only examine width bytes. */
		if (width-- == 0) break;

		/* Scan template list to locate next character. */
		const InterfaceFormatFlagsBit32 * tp;
		for (tp = template_p; ; tp++) {

			if (tolower (*data_p) == tolower (tp->character)) {
				new_flags |= tp->flag;
				break;
				}

			if (tp->character == '\0') {
				code = SC_INTERFACE_BAD_DATA;
				break;
				}
			}
	
		}

	/* Set new flags value if everything's okay. */
	if (code == SC_OKAY) flags = new_flags;

	return (code);
}


/*
	InterfaceBuffer::FlagsBit32Put:

	Translate a bit field of flags into character codes in the data buffer.

Description:

	For each bit flag in the bit field value that is set to one or ON the
corresponding character in the template data is added to the output buffer.

Examples:

	If the character template is "S:0x01, K:0x02 W:0x04" then a bit field
value of 0x03 yields a data buffer output of "SK"

Notes:

	Character values in the template data are used directly to set the
message field; no uppercase or lowercase conversion is performed.

	More than one template entry can exist for each bit flag.  Thus
both 'B' and 'W' can be configured to match the same bit flag; the first
template entry encountered that covers a particular bit flag will be used
for display.  Thus if both 'B' and 'W' specify the same bit flag and 'B'
is included first in the template list that bit will be displayed as 'B'
regardless of the character used to set the bit.

	If a single character flag specifies multiple bit flags, such a 'D' for
a set of default flags, that setting will display only if all its bits are
still uncovered at the time that template entry is encountered in the
scan.  Thus if a multiple bit template entry is early in the list the
multiple bit character will be used in preference to the its single bit
characters that may occur later in the template list.  Conversely if the
single bit flags are listed first they will be used in preference to the
multiple bit item.

See also:  InterfaceBuffer::FlagsBit32Get

Arguments:
	flags		Bit32 value to use to place character flags in data buffer.

	template_p	Pointer to InterfaceFormatFlagsBit32 template data containing
				bit flag and character flag correspondence.

	width		Number of data bytes to output; pad with trailing spaces
				to reach width if needed.

Returns:
	size_t		Number of bytes added to message data string.

*/

		size_t
InterfaceBuffer::FlagsBit32Put (Bit32 flags,
	const InterfaceFormatFlagsBit32 * template_p, size_t width)

{
	size_t length = 0;
	Bit32 flags_left = flags;

	/* Scan template list to locate next character. */
	const InterfaceFormatFlagsBit32 * tp;
	for (tp = template_p; flags_left AND tp->character != '\0'; tp++) {

		if (width != 0 AND length >= width) break;

		/* If template flag(s) are in flags then add character to buffer. */
		if ((flags_left & tp->flag) == tp->flag) {
			AddChar (tp->character);
			length++;
			flags_left &= ~tp->flag;
			}

		}

	/* Pad field to requested width with spaces. */
	while (width > length) {
		AddChar (' ');
		length++;
		}

	return (length);
}


