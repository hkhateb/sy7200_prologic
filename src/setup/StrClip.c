 /*
	str_clip:  [Module]

	The str_clip module provides a function to trim trailing white
space from the end of a supplied string.

	Copyright (c) 1993,2001,2004 - Alcom Communications

	Written by:   Alan J. Luse
	Written on:   24 August 1993
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   24 Jan 2001 16:48:06  $
	     $Date: 2005/09/01 22:27:58 $

*/


/* Include files for str_clip. */
#include <string.h>
#include <ctype.h>
#include "Str_Def.h"

/* Constant parameters for str_clip. */

/* Type and structure definitions for str_clip. */

/* Global data for str_clip. */

/* Function prototypes for str_clip. */



/*
	str_clip:

	Trim any trailing white space from a string.

Description:

	Move the end of string marker ('\0') ahead of any trailing white
space on a string.  White space is defined to be any character for
which the isspace function (macro) returns TRUE.  Return value points
to end of trimmed string.

Notes:

See also:  strspn, str_blank

Arguments:
	string		Pointer to input string to have trailing white
				space trimmmed.

Returns:
	char *		Pointer to trimed string.

*/

		char *
str_clip (char * string)

{
	size_t index;
	char * str;

	index = strlen (string);
	str = string + index;

	for ( ; index > 0 && isspace (*--str); index--) {
		*str = '\0';
		}

	return (string);
}

/**
 * Check if the character can be trimmed
 */
bool IsTrimChar(char ch)
{	
	if( ch == '\n' || ch == '\t' || ch == ' ' || ch == '\r' )
		return true;
	return false;
}

/**
 * Trim the String
 */
char * TrimString(char * src)
{
	if( src == NULL ) return NULL;
	
	// Remove leading white spaces 
	while( IsTrimChar(*src) )
		src++;
	char * str;
	size_t index = strlen(src);
	str = src + index;
	for ( ; index > 0 && IsTrimChar (*--str); index--) 
	{
		*str = '\0';
	}
	return (char *)src;
}

