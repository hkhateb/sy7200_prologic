/*
	InterfaceElement::Extend:  [Class]

The InterfaceElement::Extend module provides extended data item format and
parse.

	Copyright (c) 2000-2002 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   18 Aug 2004
	  Based on:   portions of MsgGen*.c originally 27 Nov 2000
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   19 Apr 2002 14:09:20  $
	     $Date: 2005/09/01 22:27:58 $

Description:

This module provides extended parse and format member functions for
manipulating InterfaceElement objects.

*/


/* Compilation options for InterfaceElement::Extend. */

/* Include files for InterfaceElement::Extend. */
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "System.hpp"
#include "Interface.hpp"
#include "Str_Def.h"

/* Constant parameters for InterfaceElement::Extend. */

/* Type and structure definitions for InterfaceElement::Extend. */

/* Internal static data for InterfaceElement::Extend. */

/* Function prototypes for InterfaceElement::Extend. */


/*
	InterfaceElement::ExtendParse:

	Parse an InterfaceElement using the InterfaceElement data provided.

Description:

	Convert the supplied item data to the requested InterfaceElement
format using the data type, value, and format data supplied.

See also:  InterfaceElement::ExtendPrint

Arguments:
	buffer		Reference to an Buffer in which formatted data is stored.

	obj_p		Pointer to object or region containing data for element.

Returns:
	Code		System status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;
				SC_INTERFACE_BAD_DATA if item data could not be parsed;

*/

		Code
InterfaceElement::ExtendParse (InterfaceBuffer & buffer, void * const obj_p)
		const

{
	Code code = SC_INTERFACE_BAD_DATA;
	Boolean direct = NO;

	switch (type)
		{

	case TYPE_EnumCharUBin8:
	case TYPE_EnumCharUBin16:
	case TYPE_EnumCharUBin32:
		direct = YES;
	case TYPE_EnumLabelUBin8:
	case TYPE_EnumLabelUBin16:
	case TYPE_EnumLabelUBin32:
		{
		InterfaceEnum_t state;
		void * value_p = value_o.Get (obj_p);
		if (direct) code =
			buffer.EnumCharGet (state, (const char * )format_p);
		else code =
			buffer.EnumLabelGet (state, (const InterfaceFormatEnum * )format_p);
		if (code != SC_OKAY) break;
		switch (type)
			{
		case TYPE_EnumCharUBin8:
		case TYPE_EnumLabelUBin8:
			*(UBin8 *)value_p = (UBin8)state;
			break;
		case TYPE_EnumCharUBin16:
		case TYPE_EnumLabelUBin16:
			*(UBin16 *)value_p = (UBin16)state;
			break;
		case TYPE_EnumCharUBin32:
		case TYPE_EnumLabelUBin32:
			*(UBin32 *)value_p = (UBin32)state;
			break;
			}
		}
		break;

	case TYPE_FlagBit8:
		code = buffer.FlagsBit8Get (*(Bit8 *)value_o.Get (obj_p),
			(const InterfaceFormatFlagsBit8 * )format_p);
		break;
	case TYPE_FlagBit16:
		code = buffer.FlagsBit16Get (*(Bit16 *)value_o.Get (obj_p),
			(const InterfaceFormatFlagsBit16 * )format_p);
		break;
	case TYPE_FlagBit32:
		code = buffer.FlagsBit32Get (*(Bit32 *)value_o.Get (obj_p),
			(const InterfaceFormatFlagsBit32 * )format_p);
		break;

	default:
		code = SC_INTERFACE_UNKNOWN_TYPE;
		break;
		}

	/* External parse routines may return SC_FAILURE for parsing failures
		so translate to InterfaceElement subsystem status code. */
	if (code == SC_FAILURE) code = SC_INTERFACE_BAD_DATA;

	return (code);
}


/*
	InterfaceElement::ExtendFormat:

	Format an InterfaceElement using the InterfaceElement data provided.

Description:

	Convert the supplied data element to the requested InterfaceElement
format using the data type, value, and format data supplied.

See also:  InterfaceElement::ExtendPrint

Arguments:
	buffer		Reference to an Buffer in which formatted data is stored.

	obj_p		Pointer to object or region containing data for element.

Returns:
	Code		System status code of SC_OKAY for successful completion;
				SC_INTERFACE_UNKNOWN_TYPE if item type is not known;

*/

		Code
InterfaceElement::ExtendFormat (InterfaceBuffer & buffer, const void * const obj_p)
		const

{
	Code code = SC_OKAY;
	Boolean direct = NO;

	switch (type)
		{

	case TYPE_EnumCharUBin8:
	case TYPE_EnumCharUBin16:
	case TYPE_EnumCharUBin32:
		direct = YES;
	case TYPE_EnumLabelUBin8:
	case TYPE_EnumLabelUBin16:
	case TYPE_EnumLabelUBin32:
		{
		InterfaceEnum_t state = 0;
		const void * value_p = value_o.Get (obj_p);
		switch (type)
			{
		case TYPE_EnumCharUBin8:
		case TYPE_EnumLabelUBin8:
			state = (InterfaceEnum_t)*(UBin8 *)value_p;
			break;
		case TYPE_EnumCharUBin16:
		case TYPE_EnumLabelUBin16:
			state = (InterfaceEnum_t)*(UBin16 *)value_p;
			break;
		case TYPE_EnumCharUBin32:
		case TYPE_EnumLabelUBin32:
			state = (InterfaceEnum_t)*(UBin32 *)value_p;
			break;
			}
		if (direct) buffer.EnumCharPut (state, (const char * )format_p);
		else buffer.EnumLabelPut (state, (const InterfaceFormatEnum * )format_p);
		break;
		}

	case TYPE_FlagBit8:
		buffer.FlagsBit8Put (*(Bit8 *)value_o.Get (obj_p),
			(const InterfaceFormatFlagsBit8 * )format_p);
		break;
	case TYPE_FlagBit16:
		buffer.FlagsBit16Put (*(Bit16 *)value_o.Get (obj_p),
			(const InterfaceFormatFlagsBit16 * )format_p);
		break;
	case TYPE_FlagBit32:
		buffer.FlagsBit32Put (*(Bit32 *)value_o.Get (obj_p),
			(const InterfaceFormatFlagsBit32 * )format_p);
		break;

	default:
		code = SC_INTERFACE_UNKNOWN_TYPE;
		break;
		}

	return (code);
}


/*
	InterfaceElement::GetDecimal:

	Get a decimal point position.

Description:

	Get a decimal point position from the reference offset in the data
structure.  Use default of 2 if no reference offset was provided.

Arguments:
	obj_p		Pointer to object or region containing data for element.

Returns:
	SysDP		Decimal point location (0 = nnnn. or none; 1 = nnn.n; ...).

*/

		SysDP
InterfaceElement::GetDecimal (void * const obj_p) const

{
	SysDP dp;

	if (reference_o.Valid ()) {
		/* Assumes decimal point is in same data structure as value. */
		dp = *(SysDP *)reference_o.Get (obj_p);
		}
	else dp = 2;

	return (dp);
}


