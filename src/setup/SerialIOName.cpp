/*
	SerialIOName:  [Module]

	The SerialIOName module provides name strings for various serial I/O
enumerations and configuration values.


	Copyright (c) 2003 - Alcom Communications
	Copyright (c) 2004 - Time America, Inc.

	Written by:   Alan J. Luse
	Written on:   19 Aug 2004
	  Based on:   SioName.c originally written on 23 Apr 2003
	 $Revision: 1.1 $
	   $Author: mahesh $
	  $Modtime:   23 Apr 2003 11:54:08  $
	     $Date: 2005/09/01 22:27:58 $

*/


/* Include files for SerialIOName. */
#include <stddef.h>
//#include "Generic.hpp"
#include "Gen_Def.h"
#include "UtilityOptions.hpp"
#include "SerialIO.hpp"

/* Compilation option defaults for SerialIOName. */
#if ! defined SIO_NAME_GLOBAL
#define SIO_NAME_GLOBAL			1
#endif
#if ! defined SIO_NAME_PARITY
#define SIO_NAME_PARITY			1
#endif
#if ! defined SIO_NAME_BAUD_ALL
#define SIO_NAME_BAUD_ALL		1
#endif
#if ! defined SIO_NAME_BAUD_SHORT
#define SIO_NAME_BAUD_SHORT		0
#endif

/* Constant parameters for SerialIOName. */
#if SIO_NAME_GLOBAL
#define SIO_NAME_SPACE
#else
#define SIO_NAME_SPACE static
#endif

/* Type and structure definitions for SerialIOName. */

/* Internal static data for SerialIOName. */

/* Function prototypes for SerialIOName. */


/** Names for individual baud rates. **/

SIO_NAME_SPACE const char SioNameBaud75 [] = "75";
SIO_NAME_SPACE const char SioNameBaud150 [] = "150";
SIO_NAME_SPACE const char SioNameBaud300 [] = "300";
SIO_NAME_SPACE const char SioNameBaud600 [] = "600";
SIO_NAME_SPACE const char SioNameBaud1200 [] = "1200";
SIO_NAME_SPACE const char SioNameBaud2400 [] = "2400";
SIO_NAME_SPACE const char SioNameBaud4800 [] = "4800";
SIO_NAME_SPACE const char SioNameBaud9600 [] = "9600";
SIO_NAME_SPACE const char SioNameBaud19200 [] = "19200";
SIO_NAME_SPACE const char SioNameBaud38400 [] = "38400";
SIO_NAME_SPACE const char SioNameBaud76800 [] = "76800";

#if SIO_NAME_BAUD_ALL
SIO_NAME_SPACE const char SioNameBaud50 [] = "50";
SIO_NAME_SPACE const char SioNameBaud110 [] = "110";
SIO_NAME_SPACE const char SioNameBaud134 [] = "134";
SIO_NAME_SPACE const char SioNameBaud200 [] = "200";
SIO_NAME_SPACE const char SioNameBaud1050 [] = "1050";
SIO_NAME_SPACE const char SioNameBaud2000 [] = "2000";

SIO_NAME_SPACE const char SioNameBaud900 [] = "900";
SIO_NAME_SPACE const char SioNameBaud1800 [] = "1800";
SIO_NAME_SPACE const char SioNameBaud3600 [] = "3600";
SIO_NAME_SPACE const char SioNameBaud7200 [] = "7200";
SIO_NAME_SPACE const char SioNameBaud14400 [] = "14400";
SIO_NAME_SPACE const char SioNameBaud28800 [] = "28800";
SIO_NAME_SPACE const char SioNameBaud57600 [] = "57600";
SIO_NAME_SPACE const char SioNameBaud115200 [] = "115200";

SIO_NAME_SPACE const char SioNameBaudMax [] = "Max";
SIO_NAME_SPACE const char SioNameBaudNone [] = "None";
#endif

/* Short names (4 chars max) for high baud rates. */
#if SIO_NAME_BAUD_SHORT
SIO_NAME_SPACE const char SioNameBaud9600Short [] = "9.6";
SIO_NAME_SPACE const char SioNameBaud19200Short [] = "19.2";
SIO_NAME_SPACE const char SioNameBaud38400Short [] = "38.4";
SIO_NAME_SPACE const char SioNameBaud76800Short [] = "76.8";
#if SIO_NAME_BAUD_ALL
SIO_NAME_SPACE const char SioNameBaud14400Short [] = "14.4";
SIO_NAME_SPACE const char SioNameBaud28800Short [] = "28.8";
SIO_NAME_SPACE const char SioNameBaud57600Short [] = "57.6";
SIO_NAME_SPACE const char SioNameBaud115200Short [] = "115.";
#endif
#endif


/* Global baud rate name table. */
const char * const SioNameBaud [] = {
	SioNameBaud75,
	SioNameBaud150,
	SioNameBaud300,
	SioNameBaud600,
	SioNameBaud1200,
	SioNameBaud2400,
	SioNameBaud4800,
	SioNameBaud9600,
	SioNameBaud19200,
	SioNameBaud38400,
	SioNameBaud76800,

#if SIO_NAME_BAUD_ALL
	SioNameBaud50,
	SioNameBaud110,
	SioNameBaud134,
	SioNameBaud200,
	SioNameBaud1050,
	SioNameBaud2000,

	SioNameBaud900,
	SioNameBaud1800,
	SioNameBaud3600,
	SioNameBaud7200,
	SioNameBaud14400,
	SioNameBaud28800,
	SioNameBaud57600,
	SioNameBaud115200,

	SioNameBaudMax,
	SioNameBaudNone,
#endif
};


/* Global baud rate short name table. */
#if SIO_NAME_BAUD_SHORT
const char * const SioNameBaudShort [] = {
	SioNameBaud75,
	SioNameBaud150,
	SioNameBaud300,
	SioNameBaud600,
	SioNameBaud1200,
	SioNameBaud2400,
	SioNameBaud4800,
	SioNameBaud9600Short,
	SioNameBaud19200Short,
	SioNameBaud38400Short,
	SioNameBaud76800Short,

#if SIO_NAME_BAUD_ALL
	SioNameBaud50,
	SioNameBaud110,
	SioNameBaud134,
	SioNameBaud200,
	SioNameBaud1050,
	SioNameBaud2000,

	SioNameBaud900,
	SioNameBaud1800,
	SioNameBaud3600,
	SioNameBaud7200,
	SioNameBaud14400Short,
	SioNameBaud28800Short,
	SioNameBaud57600Short,
	SioNameBaud115200Short,

	SioNameBaudMax,
	SioNameBaudNone
#endif
};


/*** Parity mode names. */
#if SIO_NAME_PARITY

/* Names for individual parity modes. */
SIO_NAME_SPACE const char SioNameParityNone [] = "None";
SIO_NAME_SPACE const char SioNameParityOdd [] = "Odd";
SIO_NAME_SPACE const char SioNameParityEven [] = "Even";
SIO_NAME_SPACE const char SioNameParityOne [] = "One";
SIO_NAME_SPACE const char SioNameParityZero [] = "Zero";

/* Parity modes name list. */
const char * const SioNameParity [] = {
	SioNameParityNone,
	SioNameParityOdd,
	SioNameParityEven,
	SioNameParityOne,
	SioNameParityZero,
};

#endif

#endif

