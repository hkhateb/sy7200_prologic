/**
 * Handles the follwoing menus
 * 	- configuration main menu
 * 	- clock info menu
 * 	- clock reset menu
 */

#ifdef USE_MCHECK
#include <mcheck.h>
#endif

#include "OperationDriver.hpp"
#include "ConfigMgr.h"
#include "Version.h"
#include "SystemGlobals.h"
#include "LCDHandler.h"
#include <ctype.h>
#include <fcntl.h>
#include <stdio.h>
#include "FPOperation.hpp"
#include "XMLProperties.h"

#include "uf_error.h"

extern Logger * logger;


#ifdef client_prologic
#define use_readbadge	1
#define USE_SCANTEMPLATE_PROTOCOL_API
//#define use_superreadbadge 1
#endif


#define DIGITAL_OUT_MAX 4
extern bool fpUnitEnabled;

extern FPOperation fpOpr;
static const char * KB_READ_USER_ID		= "   ENTER USER ID    ";
static const char * ENROLL_FINGER		= "    PLACE FINGER    ";

static const char * ENROLL_ANOTHER_USER             = "ENROLL ANOTHER USER?";


/******************************************************************
 * *************** Configuration Menu *****************************
 * ***************************************************************/

class menudata {
public:

  menudata ( const char* display = 0  , OperationStatusCode (ConfigMgr::*subfunc)() = 0 )
    : m_f ( subfunc ) {if(display != NULL )strcpy(m_d,display);}

  void setMenuData(const char* display = 0){strcpy(m_d,display);}
  void setMenuFun(OperationStatusCode (ConfigMgr::*subfunc)() = 0){m_f=subfunc;}
  char m_d[21] ;
  OperationStatusCode (ConfigMgr ::*m_f) ();
};

namespace {
  class menu {
  public:
    menu ( const menudata* b , const menudata* e , int top = 0 , int sel = 1 )
      : m_b ( b ) , m_e ( e ) , m_top ( top ) , m_sel ( sel ) {}

    OperationStatusCode getuserinput ( char& input , Operation& oper );
	OperationStatusCode getuserinputWithTimeOut ( char& input , Operation& oper , int timeOut );

	void display ();
    void up ();
    void down ();

    const menudata* m_b ;
    const menudata* m_e ;
    int m_top ;
    int m_sel ;
  };

};

/**
 * Configuration Menu Strings
 */
const char* RELVERSIONFILE = "/mnt/flash/Version.txt" ;
const char* ROOTFSVERSIONFILE = "/root/Version.txt" ;
const char* KERNELVERSIONFILE = "/proc/version" ;

menu * relayMenu;
//menu array layout: first line: heading
//subsequent lines: menu items

#ifdef CLIENT_PROLOGIC_showutil

static menudata CONFIG_MENU [] = {
  menudata ("   CONFIGURATION    "),
  menudata ("1-SET DATE & TIME",          & ConfigMgr ::DispatchDates ),
  menudata ("2-COMMUNICATIONS", & ConfigMgr ::ShowSelTCPIPCommScreen ),
  menudata ("3-TERMINAL RESET", & ConfigMgr::ShowTerminalResetScreen),
  menudata ("4-CLOCK INFO",     & ConfigMgr ::ShowClockInfoScreen ),
  menudata ("5-CONFIG ACCESS",  & ConfigMgr ::DispatchConfigAccess ),
  menudata ("6-UTILITIES",      & ConfigMgr ::DispatchUtilities ),
  menudata ("7-FINGER PRINT",      & ConfigMgr ::ShowFingerPrintScreen )
};
static menudata* CONFIG_MENU_E = CONFIG_MENU + sizeof ( CONFIG_MENU ) / sizeof ( CONFIG_MENU [ 0 ] );


#else

static menudata CONFIG_MENU [] = {
  menudata ("   CONFIGURATION    "),
  menudata ("1-SET DATE & TIME",          & ConfigMgr ::DispatchDates ),
  //menudata ("2-DAYLIGHT SAVINGS",& ConfigMgr ::DispatchDaylightSavings ),
  menudata ("2-COMMUNICATIONS", & ConfigMgr ::ShowSelTCPIPCommScreen ),
  //menudata ("4-SET READERS",    & ConfigMgr ::DispatchReaders ),
  menudata ("3-SET READERS",    & ConfigMgr ::Nop ),
  menudata ("4-ACCESSORY TEST", & ConfigMgr ::ShowAccessoryScreen ),
  menudata ("5-TERMINAL RESET", & ConfigMgr::ShowTerminalResetScreen),
  menudata ("6-CLOCK INFO",     & ConfigMgr ::ShowClockInfoScreen ),
  menudata ("7-CONFIG ACCESS",  & ConfigMgr ::DispatchConfigAccess ),
  menudata ("8-UTILITIES",      & ConfigMgr ::DispatchUtilities ),
  menudata ("9-FINGER PRINT",      & ConfigMgr ::ShowFingerPrintScreen )
};
static menudata* CONFIG_MENU_E = CONFIG_MENU + sizeof ( CONFIG_MENU ) / sizeof ( CONFIG_MENU [ 0 ] );

#endif


static menudata FINGERPRINT_MENU[] = {
  menudata ( "    FINGER PRINT    "),
  menudata ( "1-ENROLL            " , & ConfigMgr ::FPEnroll ),
  menudata ( "2-VERIFICATION      " , & ConfigMgr::FPVerification ),
  menudata ( "3-IDENTIFICATION    " , & ConfigMgr::FPIdentification ),
  menudata ( "4-DELETE USER       " , & ConfigMgr::FPDeleteUser ),
  menudata ( "5-DELETE ALL USERS  " , & ConfigMgr::FPDeleteAllUsers ),
  menudata ( "6-TEMPLATES NUMBER  " , & ConfigMgr::FPUGetNumberOffUsers )
};

static menudata* FINGERPRINT_MENU_E = FINGERPRINT_MENU + sizeof ( FINGERPRINT_MENU ) / sizeof ( FINGERPRINT_MENU [ 0 ] );

static menudata ACCESSORYTEST_MENU[] = {
  menudata ( "    RELAY SWITCH    "),
  menudata ( "1-TURN ON RELAY1   " , & ConfigMgr ::SwitchRelay1 ),
  menudata ( "2-TURN ON RELAY2   " , & ConfigMgr ::SwitchRelay2 ),
  menudata ( "3-TURN ON RELAY3   " , & ConfigMgr ::SwitchRelay3 ),
  menudata ( "4-TURN ON RELAY4   " , & ConfigMgr ::SwitchRelay4 )
};

static menudata* ACCESSORYTEST_MENU_E = ACCESSORYTEST_MENU + sizeof ( ACCESSORYTEST_MENU ) / sizeof ( ACCESSORYTEST_MENU [ 0 ] );

static menudata TERMENALRESET_MENU[] = {
  menudata ( "    TERMINAL RESET    "),
  menudata ( "1-TERMINAL REBOOT" , & ConfigMgr ::HandleResetClock ),
  menudata ( "2-APPLICATION RESET" , & ConfigMgr ::HandleResetAPPL ),
};

static menudata* TERMENALRESET_MENU_E = TERMENALRESET_MENU + sizeof ( TERMENALRESET_MENU ) / sizeof ( TERMENALRESET_MENU [ 0 ] );


static menudata CLOCKINFO_MENU [] = {
  menudata ( "       INFO        " ),
  menudata ( "1-VERSION          " , & ConfigMgr ::ShowVersionMenu ),
  menudata ( "2-WIRED SETTINGS   " , & ConfigMgr ::DispatchWiredSettings ),
  menudata ( "3-WIRELESS SETTINGS" , & ConfigMgr ::DispatchWirelessSettings )
};
static menudata* CLOCKINFO_MENU_E = CLOCKINFO_MENU + sizeof ( CLOCKINFO_MENU ) / sizeof (CLOCKINFO_MENU [ 0 ]);

static menudata CLOCKVERSION_MENU [] = {
  menudata ( "     VERSIONS      " ),
  menudata ( "1-APPLICAT. VERSION", & ConfigMgr ::DispatchAppVer ),
  menudata ( "2-RELEASE VERSION  ", & ConfigMgr ::DispatchRelVer ),
  menudata ( "3-ROOT FS VERSION  ", & ConfigMgr ::DispatchRootfsVer ),
  menudata ( "4-KERNEL VERSION   ", & ConfigMgr ::DispatchKernelVer ),
  menudata ( "5-FINGERPRINT VER. ", & ConfigMgr ::DispatchFirmwareVer )
};
static menudata* CLOCKVERSION_MENU_E = CLOCKVERSION_MENU + sizeof ( CLOCKVERSION_MENU ) / sizeof ( CLOCKVERSION_MENU [ 0 ]);

static const char * APPVER_HDNG		 	        = "APPLICATION VERSION";
static const char * RELVER_HDNG                         = "  RELEASE VERSION  ";
static const char * ROOTFSVER_HDNG                      = "  ROOT FS VERSION  ";
static const char * KERNELVER_HDNG                      = "  KERNEL VERSION   ";

static const char * CONFIRM_DELETE_USER                 = "   ARE YOU SURE?   ";
static const char * ENROLL_ANOTHER_TEMPLATE             = "SCAN MORE TEMPLATE?";
static const char * CLOCK_RESET_MENU_HDNG			= " REBOOT TERM (Y/N)  ";
static const char * CLOCK_RESET_DISPLAY1			= "  REBOOT TERMINAL   ";
static const char * CLOCK_RESET_DISPLAY2			= "   PLEASE WAIT...   ";

static const char * APPL_RESET_MENU_HDNG			= " REBOOT APPL (Y/N)  ";
static const char * APPL_RESET_DISPLAY1			= "   REBOOT APPL   ";
static const char * APPL_RESET_DISPLAY2			= "   PLEASE WAIT...   ";

//*******************************************************************
ConfigMgr::ConfigMgr()
{

 int relay ;
 char menuData[21];

 for(relay = 1 ; relay <= DIGITAL_OUT_MAX ;relay++ )
 {
  if(user_digital_get (relay))
  {
    strcpy(menuData,"1-TURN ON RELAY1");
	menuData[0]= 48 + relay;;
	menuData[15]= 48 + relay;;
  }
  else
  {
    strcpy(menuData,"1-TURN OFF RELAY1");
	menuData[0]= 48 + relay;;
	menuData[16]= 48 + relay;;
  }
  ACCESSORYTEST_MENU[relay].setMenuData(menuData);
 }
}


void ConfigMgr::setOperationIODriver(OperationIODriver * operiod)
	{
		this ->operationiod = operiod;
	}


void ConfigMgr::UpdateAccessoryTestMenu()
{
 int relay ;
 char menuData[21];

 for(relay = 1 ; relay <= DIGITAL_OUT_MAX ;relay++ )
 {
  if(user_digital_get (relay))
  {
    strcpy(menuData,"1-TURN OFF RELAY1");
	menuData[0]= 48 + relay;;
	menuData[16]= 48 + relay;;
  }
  else
  {
    strcpy(menuData,"1-TURN ON RELAY1");
	menuData[0]= 48 + relay;;
	menuData[15]= 48 + relay;;
  }
  ACCESSORYTEST_MENU[relay].setMenuData(menuData);
 }

}

OperationStatusCode ConfigMgr::ShowConfigurationMenu(){
  return HandleMenu ( CONFIG_MENU , CONFIG_MENU_E );
}

OperationStatusCode ConfigMgr::ShowVersionMenu(){
  return HandleMenu ( CLOCKVERSION_MENU , CLOCKVERSION_MENU_E );
}

OperationStatusCode ConfigMgr ::ShowAccessoryScreen(){

  UpdateAccessoryTestMenu();
  return HandleMenu ( ACCESSORYTEST_MENU , ACCESSORYTEST_MENU_E );
}

OperationStatusCode ConfigMgr ::ShowFingerPrintScreen()
{
   OperationStatusCode code = SC_Fail;
   //printf("finger print enable = %d\n",fpOpr.EnableFpUnit);
   if(! fpOpr.EnableFpUnit)
   {
     user_output_clear();
     user_output_write(2, 1, "  INACTIVE FP UNIT ");
	 oper.TurnOnLEDYellow();
	 oper.Beep(TYPE_Error);
	 usleep( oper.GetErrorMsgTimeout() * 1000 );
	 user_output_write(3, 1, BLANK_LINE);
	 oper.TurnOffLEDYellow();
	 oper.Beep(TYPE_TurnOff);
	 return code;
   }
   code = HandleMenu ( FINGERPRINT_MENU , FINGERPRINT_MENU_E );
   return code;
}

OperationStatusCode ConfigMgr ::ShowTerminalResetScreen(){
DP("ShowTerminalResetScreen: call handlemenu\n");
  return HandleMenu ( TERMENALRESET_MENU , TERMENALRESET_MENU_E );
}


OperationStatusCode ConfigMgr::ShowClockInfoScreen()
{
  return HandleMenu ( CLOCKINFO_MENU , CLOCKINFO_MENU_E );
}

OperationStatusCode ConfigMgr::DispatchWiredSettings (){
  return tcpipCfg.ShowNetworkInfoMenu( TYPE_ETHERNET );
}

OperationStatusCode ConfigMgr::DispatchWirelessSettings (){
  return tcpipCfg.ShowNetworkInfoMenu( TYPE_WIRELESS );
}

OperationStatusCode ConfigMgr::DispatchAppVer (){
  return ShowAppVersion ();
}

OperationStatusCode ConfigMgr::SwitchRelay1 (){


	  return SwitchRelay (1);

}

OperationStatusCode ConfigMgr::SwitchRelay2 (){

	   return SwitchRelay (2);
}

OperationStatusCode ConfigMgr::SwitchRelay3 (){

	   return SwitchRelay (3);
}

OperationStatusCode ConfigMgr::SwitchRelay4 (){

	   return SwitchRelay (4);
}

OperationStatusCode ConfigMgr::SwitchRelay (int relay){

	  int state = user_digital_get (relay);
	  char menuData[21];
	  char relayStr = 48 + relay;
	  //int top = 0;
	  //menu m(ACCESSORYTEST_MENU , ACCESSORYTEST_MENU_E);

	  switch(state)
	  {
	    case ON:
	        user_digital_set (relay, OFF);
	        strcpy(menuData,"1-TURN ON RELAY1");
	        menuData[0]= relayStr;
	        menuData[15]= relayStr;
	        ACCESSORYTEST_MENU[relay].setMenuData(menuData);
	        //if (relay == 4) top = 1;
	        //m.m_sel = relay;
	        //if (relay == 4) m.m_top = 1;
	        //HandleMenu ( ACCESSORYTEST_MENU , ACCESSORYTEST_MENU_E,top,relay );
	        //m.display();
	        return SC_Success;
	    break;
	    case OFF:
	        user_digital_set (relay, ON);
	        strcpy(menuData,"1-TURN OFF RELAY1");
	        menuData[0]= relayStr;
	        menuData[16]= relayStr;
	        ACCESSORYTEST_MENU[relay].setMenuData(menuData);
	        //if (relay == 4) top = 1;
	        //m.m_sel = relay;
	        //if (relay == 4) m.m_top = 1;
	        //HandleMenu ( ACCESSORYTEST_MENU , ACCESSORYTEST_MENU_E,top,relay );
	        //m.display();
	        return SC_Success;
	    break;
	    default:
	    break;

	  }
	   return SC_Success;
}

OperationStatusCode ConfigMgr::ShowAppVersion(){
	user_output_clear();
	user_output_write(2, 1, APPVER_HDNG);
	int col = ( 20 - strlen(CLOCK_VERSION) ) / 2 + 1;
	user_output_write(3, col, CLOCK_VERSION);
	return HandleVersionUI ();
}

OperationStatusCode ConfigMgr::ShowFirmwareVersion(){
	user_output_clear();
	user_output_write(2, 3, "FIRMWARE VERSION");
	char line[10];
	char * line_p = NULL;

	FILE * fp = fopen("firmware_version", "r");
	if( fp != NULL )
	{
		line_p = fgets (line, sizeof line, fp);
		fclose(fp);
	}

	if(line_p != NULL)
	{
		str_clip (line);
		user_output_write(3, 8, line);
	}

	char input;
	int timeout = oper.GetDataTimeout();
	int tm = 0;
	while(true)
	{
		if (user_input_ready ())
		{
			oper.Beep(50, TYPE_KeyPress);
			input = user_input_get ();
			if( input == ESC || input == '\n') return SC_Clear;
		}
		else
		{
			usleep(10000);
			tm += 20;
			if(tm >= timeout)
			{
				return SC_TIMEOUT;
			}
		}
	}

	return SC_Success;
}

const char* ConfigMgr ::BreakLine ( const char* b , const char* e ){

  const int columns = 19 ;

  const char* i = e ;
  while (( i - b ) > columns ){
    //string doesn't fit on line. Find max that fits on line
    i -- ;
  }

  const char* mx = i ;
  //if string doesn't fit on line ( mx != e ), try to find the last blank
  while ( mx != e && b != i && ! isblank ( * ( i - 1 ))){
    i -- ;
  }
  //if no blank, i is now b. In this case, send out 20 chars
  if ( i == b ){
    i = mx ;
  }

  return i ;
}

/*
void ConfigMgr ::DisplayMultLine ( const char* b , const char* e , int row_begin ){
  const char* i = b ;
  while (( i != e ) && row_begin < 5 ){
    i = DisplayLine ( i , e , row_begin );
    row_begin ++ ;
  }
}
*/

OperationStatusCode ConfigMgr ::MenuFromFile ( const char* heading , const char* filename ){
	char b [ 1000 ];
	static const char* def = "NOT AVAILABLE" ;
	strcpy ( b , def );

	int s = strlen ( b ) ;
	FILE* f = fopen ( filename , "r" );
	if ( f >= 0 ){
	  fgets ( b , sizeof ( b ) - 1 , f );
	  s = strlen ( b );
	  fclose ( f ); f = 0 ;
	}
	//run through line breaker twice. First time to find number of lines to build, second time to actually do the building
	int cnt = 1 ;
	const char* i = b ;
	const char* e = b + s ;
	while ( i != e ){
	  i = BreakLine ( i , e );
	  cnt ++ ;
	}

	//build menu items array
	menudata* md = new menudata [ cnt ];
	char* mc = new char [ cnt * 20 ];

	memset ( mc , 0 , cnt * 20 );
	md [ 0 ] = menudata ( heading , & ConfigMgr ::Nop );

	cnt = 1 ;
	i = b ;
	while ( i != e ){
	  const char* i2 ;
	  i2 = BreakLine ( i , e );
	  memcpy ( & mc [ cnt * 20 ] , i , i2 - i );
	  strcpy(md [ cnt ] .m_d ,& mc [ cnt * 20 ]) ;
	  md [ cnt ] .m_f = & ConfigMgr ::Nop ;
	  i = i2 ;
	  cnt ++ ;
	}

	OperationStatusCode r = HandleMenu ( md , md + cnt );
	delete [] md ;
	delete [] mc ;
	return r ;
}

OperationStatusCode ConfigMgr ::MenuFromBuffer ( const char* heading , const char* buffer ){
	//char b [ 1000 ];
	//static const char* def = "NOT AVAILABLE" ;
	//strcpy ( b , def );

	int s = strlen (buffer) ;
	//run through line breaker twice. First time to find number of lines to build, second time to actually do the building
	int cnt = 1 ;
	const char* i = buffer ;
	const char* e = buffer + s ;
	while ( i != e ){
	  i = BreakLine ( i , e );
	  cnt ++ ;
	}

	//build menu items array
	menudata* md = new menudata [ cnt ];
	char* mc = new char [ cnt * 20 ];

	memset ( mc , 0 , cnt * 20 );
	md [ 0 ] = menudata ( heading , & ConfigMgr ::Nop );

	cnt = 1 ;
	i = buffer ;
	while ( i != e ){
	  const char* i2 ;
	  i2 = BreakLine ( i , e );
	  memcpy ( & mc [ cnt * 20 ] , i , i2 - i );
	  strcpy(md [ cnt ] .m_d ,& mc [ cnt * 20 ]) ;
	  md [ cnt ] .m_f = & ConfigMgr ::Nop ;
	  i = i2 ;
	  cnt ++ ;
	}

	OperationStatusCode r = HandleMenu ( md , md + cnt,0,1,oper.GetMessageTimeout());
	delete [] md ;
	delete [] mc ;
	return r ;
}



OperationStatusCode ConfigMgr::DispatchRelVer (){

	return MenuFromFile ( RELVER_HDNG , RELVERSIONFILE );
}

OperationStatusCode ConfigMgr::DispatchRootfsVer (){
  return MenuFromFile ( ROOTFSVER_HDNG , ROOTFSVERSIONFILE );
}

OperationStatusCode ConfigMgr::DispatchKernelVer (){
  return MenuFromFile ( KERNELVER_HDNG , KERNELVERSIONFILE );
}

OperationStatusCode ConfigMgr::DispatchFirmwareVer (){
  return ShowFirmwareVersion ();
}

OperationStatusCode ConfigMgr ::HandleMenu ( const menudata* b , const menudata* e , int top  ,int sel ,int timeOut ){
  //initialization
  menu m ( b , e, top, sel );

  relayMenu = &m;
  //loop until done with selection, or time out
  while ( 1 )
  {

    m .display ();
    char input ;

	if(!timeOut)
	{
	 if (m .getuserinput ( input , oper ) == SC_TIMEOUT ){
		return SC_TIMEOUT ;
     }
	}
	else
	{
	   if (m .getuserinputWithTimeOut ( input , oper , timeOut ) == SC_TIMEOUT ){
		return SC_TIMEOUT ;
	  }
	}
    //handle numeric selection
    if ( isdigit ( input ))
    {
      //guard against under and overflow
      int s = input - '0' ;
	  if ( s > 0 && s < ( e - b ))
	  {
	    oper.Beep(50, TYPE_KeyPress);
	    m.m_sel = s;
	    if(m.m_sel > 3) m.m_top = m.m_sel - ( 4 - 1 );
	    else m.m_top = 0;
	     //printf("top=%d sel= %d\n",m.m_top,m.m_sel);

	    OperationStatusCode (ConfigMgr::*f) () = ( b + s ) ->m_f ;
	    OperationStatusCode r = (this->*f) ();
	    if ( r != SC_Clear )
	    {
	        continue;
	        //return r ;
	    }
	  }
	  else
	  {
	   oper.Beep(150,TYPE_Reject);
	   oper.displayErrMessage("   NOT AVAILABLE    ");
      }
    }
    else
    {
      OperationStatusCode r ;
      OperationStatusCode (ConfigMgr::*f) ();
      switch(input)
      {
        case '-': // Down Arrow
	        if (m.m_sel < m.m_e - m.m_b - 1 )
	        {
	            oper.Beep(50, TYPE_KeyPress);
	            m .down ();
	           // printf("top=%d sel= %d\n",m.m_top,m.m_sel);
	        }
	        else
	           oper.Beep(50,TYPE_Reject);

	    break ;
        case '+': // Up Arrow
	        if ( m.m_sel > 1 )
	        {
	            oper.Beep(50, TYPE_KeyPress);
	            m .up ();
	            //printf("top=%d\n",m.m_top);
	        }
	        else
	        {
	            oper.Beep(50,TYPE_Reject);
	            m .up ();
	        }
	    break ;

        case '\n':
	        oper.Beep(50, TYPE_KeyPress);
	        f = ( m .m_b + m .m_sel ) ->m_f ;
	        r = (this->*f) ();
	        if ( r != SC_Clear )
	        {
	             continue;
	            //return r ;
	        }
	    break ;
        case ESC:
            oper.Beep(50, TYPE_KeyPress);
	        return SC_Clear;
	    break ;
        default:
           oper.Beep(150,TYPE_Reject);
           oper.displayErrMessage("   NOT AVAILABLE    ");
	    break ;
      } //switch input
    } //else
  }//while
}

/*void ConfigMgr::ShowCfgMenuScreen1()
{
	currentCfgScr = 1;
	user_output_clear();
	user_output_write(1, 1, CONFIG_MENU_HDNG);
	user_output_write(2, 2, CONFIG_MENU_DATES);
	user_output_write(3, 2, CONFIG_MENU_COMM);
	user_output_write(4, 2, CONFIG_MENU_SETREADERS);
}

void ConfigMgr::ShowCfgMenuScreen2()
{
	currentCfgScr = 2;
	user_output_clear();
	user_output_write(1, 2, CONFIG_MENU_DATES);
	user_output_write(2, 2, CONFIG_MENU_COMM);
	user_output_write(3, 2, CONFIG_MENU_SETREADERS);
	user_output_write(4, 2, CONFIG_MENU_ACCTEST);
}

void ConfigMgr::ShowCfgMenuScreen3()
{
	currentCfgScr = 3;
	user_output_clear();
	user_output_write(1, 2, CONFIG_MENU_COMM);
	user_output_write(2, 2, CONFIG_MENU_SETREADERS);
	user_output_write(3, 2, CONFIG_MENU_ACCTEST);
	user_output_write(4, 2, CONFIG_MENU_CLOCKRESET);
}

void ConfigMgr::ShowCfgMenuScreen4()
{
	currentCfgScr = 4;
	user_output_clear();
	user_output_write(1, 2, CONFIG_MENU_SETREADERS);
	user_output_write(2, 2, CONFIG_MENU_ACCTEST);
	user_output_write(3, 2, CONFIG_MENU_CLOCKRESET);
	user_output_write(4, 2, CONFIG_MENU_CLOCKINFO);
}

void ConfigMgr::ShowCfgMenuScreen5()
{
	currentCfgScr = 5;
	user_output_clear();
	user_output_write(1, 2, CONFIG_MENU_ACCTEST);
	user_output_write(2, 2, CONFIG_MENU_CLOCKRESET);
	user_output_write(3, 2, CONFIG_MENU_CLOCKINFO);
	user_output_write(4, 2, CONFIG_MENU_CFGACCESS);
}

void ConfigMgr::ShowCfgMenuScreen6()
{
	currentCfgScr = 6;
	user_output_clear();
	user_output_write(1, 2, CONFIG_MENU_CLOCKRESET);
	user_output_write(2, 2, CONFIG_MENU_CLOCKINFO);
	user_output_write(3, 2, CONFIG_MENU_CFGACCESS);
	user_output_write(4, 2, CONFIG_MENU_UTILITIES);
}
*/


OperationStatusCode ConfigMgr::FPEnroll()
{
	printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());

	bool enroll_another_user = false;
	bool able_to_enroll_another_user = false;
	LCDHandler lcdhdlr;
	OperationStatusCode code = SC_Fail;
   pthread_mutex_lock (&FPOperation::finger_print_mutex);
   int nTemplateId = 0;
   char templateId[3];
   templateId[0] = '\0';

	bool bMustEnrollAtServer = true;
	char mustEnrollAtServer[3]; mustEnrollAtServer[0] = '\0';

//printf("get the mustenrollatserver property\n");
//	LOCKXM;
//	XMLProperties *propparser = new XMLProperties(XML_PROPERTIES_FILE);
//	propparser->GetProperty("MustEnrollAtServer",mustEnrollAtServer);
//	delete propparser;
//	UNLOCKXM;

//	if(strlen(mustEnrollAtServer)>0)
//	{
//		// nothing
//		printf("MustEnrollAtServer is %s\n", mustEnrollAtServer);
//	}
//	else
//	{
//		strcpy(mustEnrollAtServer, "1"); // default.
//		printf("undefined. set MustEnrollAtServer is %s\n", mustEnrollAtServer);
//	}
//	if ( strcmp(mustEnrollAtServer,"1") == 0)
//	{
//		bMustEnrollAtServer = true;
//		printf("bMustEnrollAtServer is true.\n");
//	}
//	else
//	{
//		bMustEnrollAtServer = false;
//		printf("bMustEnrollAtServer is false.\n");
//	}

		bMustEnrollAtServer = true;




repeat_enroll_user:

   nTemplateId = 0;
   sprintf(templateId, "%d", nTemplateId);

   enroll_another_user = false;
   able_to_enroll_another_user = false;
   code = SC_Fail;
   user_output_clear();
   int len = oper.GetMaxBadgeLen();
   //not for prologic: if(len > 10)len = 10; //just 4 byte for User ID
   //int col = ( 20 - len  ) / 2 + 1;
   //int timeout = oper.GetDataTimeout();
   char buffer[ len + 1 ];
   unsigned long int UserId = 0;
   bool overflow = 0;
   buffer[len] = '\0';
StartEnroll:
   user_output_clear();
   user_output_write(2, 1, KB_READ_USER_ID);
   buffer[len] = '\0';

   char responseStatus[2];responseStatus[0] = '\0';
   CURLcode res;

#ifdef use_readbadge

   code = SC_Fail;
   	fpOpr.fpOperation = Ignore_Operation;
printf("call operationiod->readbadge()\n");
	//pthread_mutex_lock( &OperationIODriver::lockDisplay );
   code = operationiod->ReadBadge2(); //wait until badge get from legal source .
   //buffer = operationiod->getBadgeBuffer();
printf("readbadge returns %s\n", operationiod->getBadgeBuffer());
   strcpy(buffer,operationiod->getBadgeBuffer());
printf("after strcpy, buffer is %s\n", buffer);
   UserId =strtoul(buffer,NULL,10);

   // now that UserId has the numerical value of the badge, reuse the buffer to
   // update the buffer with the UserId converted back to a string
   // this will get rid of the leading 0's, which Prologic/TEAMS seems to require
   sprintf(buffer, "%ld", UserId);


#else

   // keypad only
   for( int i=0; i<len; i++ ) buffer[i] = ' ';
   LCD_READ_STATE st = fpOpr.lcdHandler.ReadData(SysInputTypeNumeric, 3, col, timeout, len, 1, buffer);
   if (st == READ_TIMEDOUT)
   {
	  pthread_mutex_unlock (&FPOperation::finger_print_mutex);
	   return SC_TIMEOUT;
   }
   else if(st == READ_CLEAR)
   {
	  pthread_mutex_unlock (&FPOperation::finger_print_mutex);
	   return SC_Clear;
   }
   overflow = ifOverFlow(buffer);
   UserId =strtoul(buffer,NULL,10);

#endif

//testing
   DP("UserId = %lu buffer = %s len = %d\n",UserId,buffer,strlen( buffer ));
//endteset
   if( (int)strlen( buffer ) < 1 ||UserId < 1 || overflow)
   {
	  if(overflow)
	    user_output_write(3, 1, "  OVERFLOW USER ID  ");
	  else
	  	user_output_write(3, 1, "   ILLEGAL USER ID  ");
	  oper.TurnOnLEDYellow();
	  oper.Beep(TYPE_Error);
	  usleep( oper.GetErrorMsgTimeout() * 1000 );
	  user_output_write(3, 1, BLANK_LINE);
	  oper.TurnOffLEDYellow();
	  goto StartEnroll;
   }
   else
   {
	  UF_RET_CODE Result;
	  // move to after server acceptance: fpOpr.DeleteUser(UserId,&Result);
	  bool bAddingTemplateForUser;

	  bAddingTemplateForUser = false;

SCANTEMPLATE:

	  fpOpr.fpOperation = Ignore_Operation;

	  user_output_clear();
	  //user_output_write(2, 1, ENROLL_FINGER);
	  user_output_write(2, 1, "PLACE FINGER");
	  UF_BYTE FPTemplate[FP_TEMPLATESIZE]; // buffer to save the finger print tempalte will received from the sensor
	  UF_UINT32 templ_size = FP_TEMPLATESIZE;


	  //original : code = fpOpr.Enroll(UserId,mTimeout);

#ifdef USE_SCANTEMPLATE_PROTOCOL_API

		UF_UINT32 imageQualityScore = 0;

		// use sendPacket and receivePacket
		// send the ST (ScanTemplate) command
		// receive both:
		// 		fingerprint template using getRawData
		//		image quality score from Receive result
		//
		// report successful scan
		// display Image Quality
		// display Confirmation Dialog to accept the scan template
		//
		// code = SC_SUCCESS if Accepted
		// code = SC_TryAgain if rejected
		// code = SC_Fail if other error reported
		//

	code = fpOpr.ScanTemplateScore(&templ_size,FPTemplate,mTimeout,&Result, &imageQualityScore);
	printf("imageQualityScore = %d\n",imageQualityScore);
	bool cf;
	char * acceptscore;
	acceptscore = (char*)malloc(32);
	sprintf( acceptscore, "Accept Score : %3d", imageQualityScore);
	lcdhdlr.ShowConfirmationDialog( acceptscore, cf);
	if ( cf )
	{
		// accepted.
		code = SC_Success;
	}
	else
	{
		// rejected.
		code = SC_Try_Again;
	}

#else	// USE_SCANTEMPLATE_SDK_API
	code = fpOpr.ScanTemplate(&templ_size,FPTemplate,mTimeout,&Result);
#endif

#ifdef USE_MCHECK
DP("mcheck_check_all after ScanTemplate\n");
mcheck_check_all();
#endif

	  if( code == SC_Success )
	  {
					DP("ScanTemplate success for user %s\n", buffer);

	     			user_output_clear();
		 			user_output_write(2, 1, "   SCAN SUCCEED   ");
					user_output_write(3, 1, "       FOR        ");
					user_output_write(4, 1, buffer);
		 			oper.TurnOnLEDGreen();
		 			oper.Beep(TYPE_Accept);
					int mtm = oper.GetErrorMsgTimeout() * 400 ;
		 			usleep(mtm );
		 			user_output_write(3, 1, BLANK_LINE);
		 			oper.TurnOffLEDGreen();
		 			oper.Beep(TYPE_TurnOff);

	  		string templateStr;
	  		Base64 base64;
	  		base64.encode(FPTemplate, FP_TEMPLATESIZE , templateStr, false);
			const char * encoded_template ;
	  		encoded_template = templateStr.c_str();
#ifdef USE_MCHECK
DP("mcheck_check_all after encoding template\n");
mcheck_check_all();
#endif

//testing
//			 save raw template to local storage
//			{
//				FILE *fptempfile;
//				fptempfile = fopen("myrawtemplate.dat","wb");
//				fwrite (encoded_template, 1, strlen(encoded_template), fptempfile);
//				fclose(fptempfile);
//			}
//endtest
	     	user_output_clear();
		 	user_output_write(2, 1, "   ENROLLING...   ");
			user_output_write(3, 1, buffer);


   time_t optime;
   struct tm * optimeinfo;
   time ( &optime );
   optimeinfo = localtime ( &optime );
   //bool send_transaction = false;
   char dateB[22];dateB[0] = '\0';
   char timeB[22];timeB[0] = '\0';
   strftime(timeB,21,"T%H:%M:%S",optimeinfo); // get the local time in format "T%H:%M:%S" for example T10:30:45
   strftime(dateB,21,"%Y-%m-%d", optimeinfo); // get the local date in format "%Y-%m-%d" for example 2008-05-23

   char utcTime[50];utcTime[0] = '\0';
   strcpy(utcTime,dateB);
   strcat(utcTime,timeB);

	pthread_mutex_lock (&OperationIODriver::soap_request_mutex);

			sprintf(templateId, "%d", nTemplateId);
			if ( OperationIODriver::communication)
			{
DP("setfptemplate: templateid %s for user %s\n", templateId, buffer);
				httpS->SetFPTemplate(ClockId, Account, buffer, utcTime, (char*)templateId, (char*)encoded_template , SOAP_1_1_VER, &res);
			}
			else
			{
				res = (CURLcode)-1;
				DP("no communication, send template to server fails\n");
			}

			if ( bMustEnrollAtServer == false )
			{
				res = (CURLcode)0;
				DP("enrollment at server is forced to off, but continue to enroll locally\n");
			}


	  		if(res)
	  		{

				DP("enrollment at server failed for %s\n. SetFPTemplate fails with code=%d\n", buffer, res);

	  		  	 user_output_clear();
	  			 user_output_write(2, 1, "   SENDING FAILED   ");
	  			 user_output_write(3, 1, " STORE FINGER FAILED");
	  			 oper.TurnOnLEDYellow();
	  			 oper.Beep(TYPE_Error);
	  			 usleep( oper.GetErrorMsgTimeout() * 1000 );
	  			 user_output_write(3, 1, BLANK_LINE);
	  			 oper.TurnOffLEDYellow();
	  			 oper.Beep(TYPE_TurnOff);
	  		}
			else
			{

				if ( bMustEnrollAtServer == false )
				{
					responseStatus[0] = '1';
					responseStatus[1] = '\0';
				}
				else
				{
					XMLProperties *responseParser = new XMLProperties();
					responseParser->GetProperty(httpS->chunk.memory,"Status" , responseStatus);
					delete responseParser;
				}
				if(strncmp(responseStatus, "1", 1 ) == 0)
				{

				DP("enrollment at server succeeds for %s\n. SetFPTemplate ok.\n", buffer);

					if ( bMustEnrollAtServer == true )
					{
						user_output_clear();
		 				user_output_write(2, 1, "   ENROLL SUCCEED   ");
						user_output_write(3, 1, "   TO SERVER        ");
		 				oper.TurnOnLEDGreen();
		 				oper.Beep(TYPE_Accept);
					}

					int mtm = oper.GetErrorMsgTimeout() * 400 ;
		 			usleep(mtm );
		 			user_output_write(3, 1, BLANK_LINE);
		 			oper.TurnOffLEDGreen();
		 			oper.Beep(TYPE_TurnOff);
					able_to_enroll_another_user = true;
					//
					// also enroll onto local FPU (the raw template)

					// delete user if enroll accepted at server
					if ( ! bAddingTemplateForUser )
					{
						DP("removing existing user %ld\n", UserId);

						fpOpr.DeleteUser(UserId,&Result);

////
						if( (int)Result == (int)SC_Success || (int)Result == 0 )
						{
							DP("delete existing user success\n");
							user_output_write(2,1,"DELETE USER SUCCEED ");
						}
						else
						{
							switch(Result)
							{
								case UF_ERR_NOT_FOUND:
									DP("user not found, %s, to delete. Ok. New User?\n", buffer);
									//user_output_write(2, 1, "   USER NOT FOUND   ");
									break;
								case UF_ERR_BUSY:
									DP("cannot remove user %s, FP unit is busy. Try again.\n", buffer);
									break;
								default:
									DP("cannot remove user %s, code = %d", buffer, Result);
									break;
							}

						}
////
					}

					if((fpOpr.AddUser(UserId,(UF_BYTE*)FPTemplate, FP_TEMPLATESIZE, &Result)) != SC_Success ) code = SC_Fail;

// ****************************************************************// ****************************************************************
//testing
//					// compile this to force an error code of MEM_FULL
//					printf("\n\n\n***** INTERNAL TEST Begin ****\n\n");
//					code = SC_Fail;
//					Result = UF_ERR_MEM_FULL;
//					printf("force UF_ERR_MEM_FULL error\n\n");
//					printf("remove this TEST before release\n");
//					printf("\n\n***** INTERNAL TEST End ****\n\n\n");
//endtest
// ****************************************************************// ****************************************************************

					if ( code == SC_Fail )
					{
						// SDK Demo handles only the failure, not the resultcode
						DP("cannot add user %s to FPU\n", buffer);
						user_output_clear();
						user_output_write(2, 1, "ENROLL FINGER FAILED");
						user_output_write(3, 1, "TO FP UNIT          ");
//						oper.TurnOnLEDYellow();
//						oper.Beep(TYPE_Error);
//						usleep( oper.GetErrorMsgTimeout() * 500 );
//						user_output_write(3, 1, BLANK_LINE);
//						oper.TurnOffLEDYellow();
//						oper.Beep(TYPE_TurnOff);
						//
						//////
						// also check the fpu template storage availablity
						// duplicate check, just in case the fpu decides it is full
						//
						//
						// handle the failure AND the resultcode
						//
						switch ( Result )
						{

							case	UF_RET_SUCCESS:
								user_output_write(4, 1, "ret_success         ");
								break;

							case	   UF_ERR_TRY_AGAIN:
								//	The transmitted template is incomplete.
								user_output_write(4, 1, "err_try_again       ");
								break;
							case	   UF_ERR_MEM_FULL:
								//	Flash memory is full.
								user_output_write(4, 1, "err_mem_full        ");
								able_to_enroll_another_user = false;
								break;
							case	   UF_ERR_FINGER_LIMIT:
								//	The number of fingerprint templates enrolled per user_id exceeds its limit.
								user_output_write(4, 1, "err_finger_limit    ");
								break;
							case	   UF_ERR_INVALID_ID:
								//	The user id is invalid.
								user_output_write(4, 1, "err_invalid_id      ");
								break;
							case	   UF_ERR_EXIST_ID:
								//	The user id is already used.
								user_output_write(4, 1, "err_exist_id        ");
								break;
// not supported?
//							case	   UF_ERR_EXIST_FINGER:
//								//	The same finger is already enrolled.
//								user_output_write(4, 1, "err_exist_finger    ");
//								break;
							case	   UF_ERR_BUSY:
								//	Module is processing another command.
								user_output_write(4, 1, "err_busy            ");
								break;
							default:
								user_output_write(4, 1, "unknown err         ");
								break;
						}
						//
						//////

						//user_output_write(3, 1, "TO FP UNIT          ");
						oper.TurnOnLEDYellow();
						oper.Beep(TYPE_Error);
						usleep( oper.GetErrorMsgTimeout() * 1000 );
						user_output_write(3, 1, BLANK_LINE);
						oper.TurnOffLEDYellow();
						oper.Beep(TYPE_TurnOff);

					}
					else
					{
DP("enrollment: added user %ld\n", UserId);
						user_output_clear();
						user_output_write(2, 1, "   ENROLL SUCCEED   ");
						user_output_write(3, 1, "   TO FP UNIT       ");
						oper.TurnOnLEDGreen();
						oper.Beep(TYPE_Accept);
						int mtm = oper.GetErrorMsgTimeout() * 400 ;
						usleep(mtm );
						user_output_write(3, 1, BLANK_LINE);
						oper.TurnOffLEDGreen();
						oper.Beep(TYPE_TurnOff);

   						// more templates for same user...
   						{
   							bool cf;
   							lcdhdlr.ShowConfirmationDialog(ENROLL_ANOTHER_TEMPLATE, cf);
   					 	if(cf)
   							{
   								nTemplateId++; // limit to ten?????
								if ( nTemplateId < 10 )
								{
									bAddingTemplateForUser = true;
									DP("yes, add more templates for this user\n");
									pthread_mutex_unlock (&OperationIODriver::soap_request_mutex);
   							 		goto SCANTEMPLATE;
								}
								else
								{
									// limit has been reached
									DP("Limit of 10 reached. Do not add more templates for this user\n");
									bAddingTemplateForUser = false;

									user_output_clear();
									user_output_write(2, 1, "   USER   TEMPLATE  ");
									user_output_write(3, 1, "   LIMIT REACHED    ");
									oper.TurnOnLEDGreen();
									oper.Beep(TYPE_Accept);
									int mtm = oper.GetErrorMsgTimeout() * 400 ;
									usleep(mtm );
									user_output_write(3, 1, BLANK_LINE);
									oper.TurnOffLEDGreen();
									oper.Beep(TYPE_TurnOff);

								}
   							}
							else
							{
								DP("no, do not add more templates for this user\n");
								bAddingTemplateForUser = false;
							}
   						}



					}
	  			}
				else
	  			{
					DP("enrollment failed for user %s, at the server. SetFPTemplate failed\n", buffer);
	  				user_output_clear();
	  				user_output_write(2, 1, "ENROLL FINGER FAILED");
					user_output_write(3, 1, "   TO SERVER        ");
	  				oper.TurnOnLEDYellow();
	  				oper.Beep(TYPE_Error);
	  				usleep( oper.GetErrorMsgTimeout() * 500 );
	  				user_output_write(3, 1, BLANK_LINE);
	  				oper.TurnOffLEDYellow();
	  				oper.Beep(TYPE_TurnOff);
	  			}
   			}

			pthread_mutex_unlock (&OperationIODriver::soap_request_mutex);

	  }
	  //
	  else if ( code == SC_Try_Again )
	  {
	   	// repeat for same user, new template (in this case, nothing was done, so rescan
		bool cf;
		lcdhdlr.ShowConfirmationDialog(ENROLL_ANOTHER_TEMPLATE, cf);
		if(cf)
		{
				bAddingTemplateForUser = true;
				DP("ScanTemplate, try again to add templates for this user\n");
				pthread_mutex_unlock (&OperationIODriver::soap_request_mutex);
		   		goto SCANTEMPLATE;
		}
	  }
   	  else
   	  {

		DP("ScanTemplate failed for %s, code = %d\n", buffer, code);

   	  	user_output_clear();
   	  	user_output_write(2, 1, "TEMPLATE SCAN FAILED");
   	  	oper.TurnOnLEDYellow();
   	  	oper.Beep(TYPE_Error);
   	  	usleep( oper.GetErrorMsgTimeout() * 500 );
   	  	user_output_write(3, 1, BLANK_LINE);
   	  	oper.TurnOffLEDYellow();
   	  	oper.Beep(TYPE_TurnOff);
   	  }
   }


   if ( able_to_enroll_another_user == true )
   {
   		bool confirm;
   		//lcdhdlr.ShowConfirmationDialog(ENROLL_ANOTHER_TEMPLATE, confirm);
   		//if(confirm) goto SCANTEMPLATE;
		lcdhdlr.ShowConfirmationDialog(ENROLL_ANOTHER_USER, confirm);
		if(confirm)
		{
			pthread_cond_signal(&FPOperation::fprint_cond_flag);//wake up the finger print thread
			nTemplateId = 0; // reset
			DP("Enroll another user. reset template id to 0.\n");
			goto repeat_enroll_user;
		}
   }
   pthread_mutex_unlock (&FPOperation::finger_print_mutex);
   //pthread_mutex_unlock( &OperationIODriver::lockDisplay );
DP(" FPEnroll returning %d\n", code);
   return code;
}

OperationStatusCode ConfigMgr::FPVerification()
{
   pthread_mutex_lock (&FPOperation::finger_print_mutex);
   OperationStatusCode code = SC_Fail;

   user_output_clear();
   int len = oper.GetMaxBadgeLen();
   if(len > 10)len = 10; //just 4 byte for User ID
   //int col = ( 20 - len  ) / 2 + 1;
   //int timeout = oper.GetDataTimeout();
   char buffer[ len + 1 ];
   unsigned long int UserId = 0;
   bool overflow = 0;
   user_output_write(2, 1, KB_READ_USER_ID);

   buffer[len] = '\0';

#ifdef use_readbadge

// this isn't working like it should:
   code = SC_Fail;
   	fpOpr.fpOperation = Ignore_Operation;
printf("call operationiod->readbadge()\n");
	//pthread_mutex_lock( &OperationIODriver::lockDisplay );
   code = operationiod->ReadBadge2(); //wait until badge get from legal source .
   //buffer = operationiod->getBadgeBuffer();
printf("readbadge returns %s\n", operationiod->getBadgeBuffer());
   strcpy(buffer,operationiod->getBadgeBuffer());
printf("after strcpy, buffer is %s\n", buffer);
   //UserId = strtoul(buffer,NULL,10);

#else

   for( int i=0; i<len; i++ ) buffer[i] = ' ';
   LCD_READ_STATE st = fpOpr.lcdHandler.ReadData(SysInputTypeNumeric, 3, col, timeout, len, 1, buffer);
   if (st == READ_TIMEDOUT)
   {
	  pthread_mutex_unlock (&FPOperation::finger_print_mutex);
	   return SC_TIMEOUT;
   }
   else if(st == READ_CLEAR)
   {
	   pthread_mutex_unlock (&FPOperation::finger_print_mutex);
	   return SC_Clear;
   }
   overflow = ifOverFlow(buffer);

#endif

   UserId =strtoul(buffer,NULL,10);
   printf("UserId = %lu buffer = %s len = %d\n",UserId,buffer,strlen( buffer ));
   if( (int)strlen( buffer ) < 1 ||UserId < 1 || overflow)
   {
			if(overflow)
			    user_output_write(3, 1, "  OVERFLOW USER ID  ");
			else
				user_output_write(3, 1, "   ILLEGAL USER ID  ");
			oper.TurnOnLEDYellow();
			oper.Beep(TYPE_Error);
			usleep( oper.GetErrorMsgTimeout() * 1000 );
			user_output_write(3, 1, BLANK_LINE);
			oper.TurnOffLEDYellow();
   }
   else
   {
	  user_output_clear();
	  user_output_write(2, 1, ENROLL_FINGER);
	  UF_RET_CODE Result;
      fpOpr.Verification(UserId,mTimeout,&Result);

	  user_output_clear();
	  if( (int)Result == (int)SC_Success || (int)Result == 0 )
	  {
	    user_output_write(2, 1, "VERIFICATION SUCCEED");
		oper.TurnOnLEDGreen();
		oper.Beep(TYPE_Accept);
		usleep( oper.GetErrorMsgTimeout() * 1000 );
		user_output_write(3, 1, BLANK_LINE);
		oper.TurnOffLEDGreen();
		oper.Beep(TYPE_TurnOff);
	  }
	  else
	  {
		switch(Result)
		{
			case UF_ERR_TRY_AGAIN:
				user_output_write(2, 1, "      TRY AGAIN     ");
		        break;
				case UF_ERR_NOT_MATCH:
				user_output_write(2, 1, "     NOT MATCHED    ");
		        break;
			case UF_ERR_NOT_FOUND:
				user_output_write(2, 1, "   INVALID USER ID  ");
	            break;
			case UF_ERR_TIMEOUT:
				user_output_write(2, 1, "  TIMEOUT TRY AGAIN ");
		        break;
			default:
				user_output_write(2, 1, " VERIFICATION FAILED");
                user_output_write(3, 1, "     TRY AGAIN      ");
				break;
		  }
		// printf("result = %d %d\n",Result,UF_ERR_SCAN_FAILED);
		 oper.TurnOnLEDYellow();
		 oper.Beep(TYPE_Error);
		 usleep( oper.GetErrorMsgTimeout() * 1000 );
		 user_output_write(3, 1, BLANK_LINE);
		 oper.TurnOffLEDYellow();
		 oper.Beep(TYPE_TurnOff);
	  }
   }
   pthread_mutex_unlock (&FPOperation::finger_print_mutex);
   return code;

}

OperationStatusCode ConfigMgr::FPIdentification()
{
   pthread_mutex_lock (&FPOperation::finger_print_mutex);
   OperationStatusCode code = SC_Fail;
   unsigned long int UserId = 0;

   user_output_clear();
   user_output_write(2, 1, ENROLL_FINGER);
   UF_RET_CODE Result;
   fpOpr.Identification(&UserId,mTimeout,&Result);
   user_output_clear();
   if( (int)Result == (int)SC_Success || (int)Result == 0 )
   {
	  char resBuf[21];
	  sprintf(resBuf,"ID :%ld",UserId);
	  int col = ( 20 - strlen(resBuf)  ) / 2 + 1;
	  //user_output_write(2,1," USER BE IDENTIFIED ");
	  user_output_write(2,1," USER IS IDENTIFIED ");
	  user_output_write(3, col,resBuf);
	  oper.TurnOnLEDGreen();
	  oper.Beep(TYPE_Accept);
	  //usleep( oper.GetErrorMsgTimeout() * 1000 );
	  usleep( oper.GetErrorMsgTimeout() * 200 );
	  user_output_write(3, 1, BLANK_LINE);
	  oper.TurnOffLEDGreen();
	  oper.Beep(TYPE_TurnOff);
   }
   else
   {
	  switch(Result)
	  {
	   case UF_ERR_TRY_AGAIN:
			user_output_write(2, 1, "      TRY AGAIN     ");
		    break;
	   case UF_ERR_NOT_MATCH:
			user_output_write(2, 1, "     NOT MATCHED    ");
		    break;
	   case UF_ERR_NOT_FOUND:
			user_output_write(2, 1, "   USER NOT FOUND   ");
	        break;
	   case UF_ERR_TIMEOUT:
		    user_output_write(2, 1, "  TIMEOUT TRY AGAIN ");
		    break;
	   default:
		    user_output_write(2, 1, "IDENTIFICA... FAILED");
            user_output_write(3, 1, "     TRY AGAIN      ");
		    break;
	  }
	 // printf("result = %d %d\n",Result,UF_ERR_SCAN_FAILED);
	  oper.TurnOnLEDYellow();
	  oper.Beep(TYPE_Error);
	  usleep( oper.GetErrorMsgTimeout() * 1000 );
	  user_output_write(3, 1, BLANK_LINE);
	  oper.TurnOffLEDYellow();
	  oper.Beep(TYPE_TurnOff);
   }
   pthread_mutex_unlock (&FPOperation::finger_print_mutex);
   return code;
}

OperationStatusCode ConfigMgr::FPDeleteUser()
{
   pthread_mutex_lock (&FPOperation::finger_print_mutex);
   OperationStatusCode code = SC_Fail;
   unsigned long int UserId = 0;

   user_output_clear();
   int len = oper.GetMaxBadgeLen();
   if(len > 10)len = 10; //just 4 byte for User ID
   //int col = ( 20 - len  ) / 2 + 1;
   //int timeout = oper.GetDataTimeout();
   char buffer[ len + 1 ];
   bool overflow = 0;
   user_output_write(2, 1, KB_READ_USER_ID);

   buffer[len] = '\0';

#ifdef use_readbadge

   code = SC_Fail;
   	fpOpr.fpOperation = Ignore_Operation;
printf("call operationiod->readbadge()\n");
	//pthread_mutex_lock( &OperationIODriver::lockDisplay );
   code = operationiod->ReadBadge2(); //wait until badge get from legal source .
   //buffer = operationiod->getBadgeBuffer();
printf("readbadge returns %s\n", operationiod->getBadgeBuffer());
   strcpy(buffer,operationiod->getBadgeBuffer());
printf("after strcpy, buffer is %s\n", buffer);
   //UserId =strtoul(buffer,NULL,10);

#else

   for( int i=0; i<len; i++ ) buffer[i] = ' ';
   LCD_READ_STATE st = fpOpr.lcdHandler.ReadData(SysInputTypeNumeric, 3, col, timeout, len, 1, buffer);
   if (st == READ_TIMEDOUT)
   {
	  pthread_mutex_unlock (&FPOperation::finger_print_mutex);
	   return SC_TIMEOUT;
   }
   else if(st == READ_CLEAR)
   {
	   pthread_mutex_unlock (&FPOperation::finger_print_mutex);
	   return SC_Clear;
   }

   overflow = ifOverFlow(buffer);

#endif

   UserId =strtoul(buffer,NULL,10);
   printf("UserId = %lu buffer = %s len = %d\n",UserId,buffer,strlen( buffer ));
   if( (int)strlen( buffer ) < 1 ||UserId < 1 || overflow)
   {
			if(overflow)
			    user_output_write(3, 1, "  OVERFLOW USER ID  ");
			else
				user_output_write(3, 1, "   ILLEGAL USER ID  ");
			oper.TurnOnLEDYellow();
			oper.Beep(TYPE_Error);
			usleep( oper.GetErrorMsgTimeout() * 1000 );
			user_output_write(3, 1, BLANK_LINE);
			oper.TurnOffLEDYellow();
   }
   else
   {
		user_output_clear();
		bool confirm = false;
		LCDHandler lcdhdlr;
		LCD_READ_STATE state = fpOpr.lcdHandler.ShowConfirmationDialog(CONFIRM_DELETE_USER, confirm);
		UF_RET_CODE Result;
		if(confirm)
		{
			fpOpr.DeleteUser(UserId,&Result);
			user_output_clear();
			if( (int)Result == (int)SC_Success || (int)Result == 0 )
			{
				user_output_write(2,1,"DELETE USER SUCCEED ");
				oper.TurnOnLEDGreen();
				oper.Beep(TYPE_Accept);
				usleep( oper.GetErrorMsgTimeout() * 1000 );
				user_output_write(3, 1, BLANK_LINE);
				oper.TurnOffLEDGreen();
				oper.Beep(TYPE_TurnOff);
			}
			else
			{
				switch(Result)
				{
					case UF_ERR_NOT_FOUND:
						user_output_write(2, 1, "   USER NOT FOUND   ");
						break;
					case UF_ERR_BUSY:
						user_output_write(2, 1, "   MODULE IS BUSY   ");
						user_output_write(3, 1, "     TRY AGAIN      ");
						break;
				}

				// printf("result = %d %d\n",Result,UF_ERR_SCAN_FAILED);
				oper.TurnOnLEDYellow();
				oper.Beep(TYPE_Error);
				usleep( oper.GetErrorMsgTimeout() * 1000 );
				user_output_write(3, 1, BLANK_LINE);
				oper.TurnOffLEDYellow();
				oper.Beep(TYPE_TurnOff);
			}
		}
   }
   pthread_mutex_unlock (&FPOperation::finger_print_mutex);
   return code;
}

OperationStatusCode ConfigMgr::FPDeleteAllUsers()
{
   bool confirm = false;
   LCDHandler lcdhdlr;
   LCD_READ_STATE state = fpOpr.lcdHandler.ShowConfirmationDialog(CONFIRM_DELETE_USER, confirm);

   pthread_mutex_lock (&FPOperation::finger_print_mutex);
   OperationStatusCode code = SC_Fail;
   if(confirm)
   {

	  UF_RET_CODE Result;
	  fpOpr.DeleteAllUsers(&Result);
      user_output_clear();
      if( Result == SC_Success || Result == 0 )
      {
		user_output_write(2,1,"SUCCESSFUL OPERATION");
		oper.TurnOnLEDGreen();
		oper.Beep(TYPE_Accept);
		usleep( oper.GetErrorMsgTimeout() * 1000 );
		user_output_write(3, 1, BLANK_LINE);
		oper.TurnOffLEDGreen();
		oper.Beep(TYPE_TurnOff);
	  }
	  else
	  {
		switch(Result)
		{
			case UF_ERR_BUSY:
				user_output_write(2, 1, "   MODULE IS BUSY   ");
				user_output_write(3, 1, "     TRY AGAIN      ");
				break;
		}

		// printf("result = %d %d\n",Result,UF_ERR_SCAN_FAILED);
		oper.TurnOnLEDYellow();
		oper.Beep(TYPE_Error);
		usleep( oper.GetErrorMsgTimeout() * 1000 );
		user_output_write(3, 1, BLANK_LINE);
		oper.TurnOffLEDYellow();
		oper.Beep(TYPE_TurnOff);
	  }
   }
   pthread_mutex_unlock (&FPOperation::finger_print_mutex);
   return code;

}

OperationStatusCode ConfigMgr::FPUGetNumberOffUsers()
{
   int timeout = oper.GetDataTimeout();
   user_output_clear();
   if(FPOperation::EnableFpUnit && FPOperation::InitFpuSuc)
   {
	 UF_UINT32 user_count = 0;
	 if( uf_get_all_user_ids( &user_count ) == UF_RET_SUCCESS )
	 {
        char msg[50];
		sprintf(msg," TEMPLATES NUM: %d",user_count);
		user_output_write(2 ,1,msg);
		oper.TurnOnLEDGreen();
		oper.Beep(TYPE_Accept);
	    int mtm = oper.GetErrorMsgTimeout()/2; // get the time out display message in milliseconds
		if(mtm > 500)
		{
				usleep(500000);
				oper.Beep(TYPE_TurnOff); // turn off the buzzer after 0.5 second
				mtm -= 500;
		}
		usleep(mtm * 1000); //sleep for time out seconds
		oper.Beep(TYPE_TurnOff); // make sure that we turn off the buzzer
		oper.TurnOffLEDGreen(); //turn off the red led
		UF_UINT32* user_ids = (UF_UINT32*)malloc( user_count * 4 );
		uf_get_user_data( user_ids, user_count ); // we must get the data else free scan not work after
		free( user_ids );
		uf_cancel();
	 }
	 else
	 {
	   	user_output_write(2 ,1,"CAN'T GET TEM NUMBER");
		oper.TurnOnLEDRed();
		oper.Beep(TYPE_Error);
		int mtm = oper.GetErrorMsgTimeout()/2; // get the time out display message in milliseconds
		if(mtm > 500)
		{
				usleep(500000);
				oper.Beep(TYPE_TurnOff); // turn off the buzzer after 0.5 second
				mtm -= 500;
		}
		usleep(mtm * 1000); //sleep for time out seconds
	    oper.TurnOffLEDGreen();
	    oper.TurnOffLEDRed();
	    oper.Beep(TYPE_TurnOff);
		uf_cancel();
	 }


   }
   user_output_clear();
   return SC_Clear;
}

bool ConfigMgr::ifOverFlow(char* buffer)
{
   char digits[] = "123456789";
   int i = strcspn(buffer,digits);

   if(strlen( buffer+i ) > 10)return true;

	if(strlen( buffer+i ) == 10 && ((buffer[0+i] > '4') || (buffer[1+i] > '2'&& buffer[0+i] == '4')||
	  (buffer[2+i] > '9' && buffer[1+i] == '2'&& buffer[0+i] == '4')||
	  (buffer[3+i] > '4' && buffer[2+i] == '9' && buffer[1+i] == '2'&& buffer[0+i] == '4')||
	  (buffer[4+i] > '9'&& buffer[3+i] == '4' && buffer[2+i] == '9' && buffer[1+i] == '2'&& buffer[0+i] == '4')||
	  (buffer[5+i] > '6' && buffer[4+i] == '9'&& buffer[3+i] == '4' && buffer[2+i] == '9' && buffer[1+i] == '2'&& buffer[0+i] == '4')||
	  (buffer[6+i] > '7' && buffer[5+i] == '6' && buffer[4+i] == '9'&& buffer[3+i] == '4' && buffer[2+i] == '9' && buffer[1+i] == '2'&& buffer[0+i] == '4')||
	  (buffer[7+i] > '2' && buffer[6+i] == '7' && buffer[5+i] == '6' && buffer[4+i] == '9'&& buffer[3+i] == '4' && buffer[2+i] == '9' && buffer[1+i] == '2'&& buffer[0+i] == '4')||
	  (buffer[8+i] > '9' && buffer[7+i] == '2' && buffer[6+i] == '7' && buffer[5+i] == '6' && buffer[4+i] == '9'&& buffer[3+i] == '4' && buffer[2+i] == '9' && buffer[1+i] == '2'&& buffer[0+i] == '4')||
	  (buffer[9+i] > '5' && buffer[8+i] == '9' && buffer[7+i] == '2' && buffer[6+i] == '7' && buffer[5+i] == '6' && buffer[4+i] == '9'&& buffer[3+i] == '4' && buffer[2+i] == '9' && buffer[1+i] == '2'&& buffer[0+i] == '4')))
   {
     return true;
   }
   return false;
}

OperationStatusCode ConfigMgr::ShowSelTCPIPCommScreen()
{
	OperationStatusCode code = tcpipCfg.ShowSelTCPIPCommScreen();
	if( tcpipCfg.TerminalNeedsReboot() )
	{
		user_output_clear();
		user_output_write(2, 1, " CONFIGURING TCP/IP ");
		user_output_write(3, 1, "   PLEASE WAIT...   ");

		/**
		 * Before Rebooting we need to make sure
		 * that the clock is not being rprogrammed.
		 * If so, we need to wait until that is done
		 */
		LegacyTransItem * transitem = LegacyTransItem::GetInstance();
		transitem->GetProgrammingLock();
		#ifdef DEBUG
		printf("Rebooting System...\n");
		#endif
		system("reboot");

		// Rebooting takes some time, sleep for avoiding inconsistency
		usleep(10000000);

		// Really not necessary as we are rebooting, just making sure
		transitem->ReleaseProgrammingLock();
	}
	return code;
}

//20051229 wjm. re-use code to handle user input for version screens

OperationStatusCode ConfigMgr::HandleVersionUI(){
	int timeout = oper.GetDataTimeout();
	int tm = 0;
	char input;
	while(true)
	{
		if (user_input_ready ())
		{
			oper.Beep(50, TYPE_KeyPress);
			input = user_input_get ();
			if( input == ESC ) return SC_Clear;
		}
		else
		{
			usleep(10000);
			tm += 20;
			if(tm >= timeout)
			{
				return SC_TIMEOUT;
			}
		}
	}
	return SC_Success;
}

OperationStatusCode ConfigMgr::HandleResetClock()
{
	bool confirm = false;
	LCDHandler lcdhdlr;
	LCD_READ_STATE state = lcdhdlr.ShowConfirmationDialog(CLOCK_RESET_MENU_HDNG, confirm);

	if(state == READ_TIMEDOUT)
	{
		return SC_TIMEOUT;
	}
	else if(confirm)
	{
		user_output_clear();
		user_output_write(2, 1, CLOCK_RESET_DISPLAY1);
		user_output_write(3, 1, CLOCK_RESET_DISPLAY2);

		//Clear all transactions
		oper.ClearClockTransactions();

		// Replace the clock program file, properties file
		// and interfaces with the defaults
		#ifdef DEBUG
		//printf( "cp " DEFAULT_CLK_PROGRAM_FILE " " CLK_PROGRAM_FILE "\n");
		//printf( "cp " DEFAULT_XML_PROPERTIES_FILE " " XML_PROPERTIES_FILE "\n");
		//printf( "cp " DEFAULT_INTERFACES " " INTERFACES "\n");
		#endif
		//system( "cp " DEFAULT_CLK_PROGRAM_FILE " " CLK_PROGRAM_FILE );
		//system( "cp " DEFAULT_XML_PROPERTIES_FILE " " XML_PROPERTIES_FILE );
		//system( "cp " DEFAULT_INTERFACES " " INTERFACES );
		//system( "cp " DEFAULT_INTERFACES " " INTERFACES );

		//Delete all Records from the finger templates table
//		if(fpUnitEnabled)
//		{
// these 2 tasks are not necessary:
// a. journal file is not used
// b. do not delete the TA database. it may have offline transactions!!!
			//system("rm -fr TA");
			//system("rm -fr TA-*"); //for the journal file

// not necessary:
//			//Delete the templates file if it exists
//			FILE * fp = fopen("FP_INPROGRESS.CLK", "r");
//			if(fp)
//			{
//				fclose(fp);
//				remove("FP_INPROGRESS.CLK");
//			}
//		}

		// Reboot the System
		#ifdef DEBUG
		printf("Rebooting System...\n");
		#endif
		// Save the latest time
		system("hwclock -w");
		system("reboot");

		usleep(2000000);

		// Make the compiler happy
		return SC_Success;
	}
	else
	{
		return SC_Clear;
	}
}

OperationStatusCode ConfigMgr::HandleResetAPPL()
{
	bool confirm = false;
	LCDHandler lcdhdlr;
	LCD_READ_STATE state = lcdhdlr.ShowConfirmationDialog(APPL_RESET_MENU_HDNG, confirm);

	if(state == READ_TIMEDOUT)
	{
		return SC_TIMEOUT;
	}
	else if(confirm)
	{
		user_output_clear();
		user_output_write(2, 1, APPL_RESET_DISPLAY1);
		user_output_write(3, 1, APPL_RESET_DISPLAY2);
		// Reboot the System
		#ifdef DEBUG
		printf("Rebooting Application...\n");
		#endif
		// Save the latest time
		system("./ResetApplication.sh");

		usleep(2000000);

		// Make the compiler happy
		return SC_Success;
	}
	else
	{
		return SC_Clear;
	}
}

OperationStatusCode menu ::getuserinput ( char& input , Operation& oper ){
    int timeout = oper.GetDataTimeout();
    int tm = 0;

    //get user input. Guard against time out here
    while(!user_input_ready () && tm < timeout)
      {
	usleep(10000);
	tm += 20;
      }
    if(tm >= timeout) return SC_TIMEOUT;

    //gotten some input
    //oper.Beep(50, TYPE_KeyPress);
    input = user_input_get ();

    return SC_Success ;

}

OperationStatusCode menu ::getuserinputWithTimeOut ( char& input , Operation& oper , int timeOut ){
    int timeout = timeOut ; //oper.GetMessageTimeout();
    int tm = 0;

    //get user input. Guard against time out here
    if(timeOut > 500)
	{
	   usleep(500000);
	   oper.Beep(TYPE_TurnOff);
	   timeOut -= 500;
	}
    while(!user_input_ready () && tm < timeout)
    {
		usleep(10000);
		tm += 20;
    }
    if(tm >= timeout) return SC_TIMEOUT;

    //gotten some input
    //oper.Beep(50, TYPE_KeyPress);
    input = user_input_get ();

    return SC_Success ;

}

void menu ::down (){

	if ( m_sel < m_e - m_b - 1 ){
	  m_sel ++ ;
	}

	if ( m_top < m_sel - ( 4 - 1 )){
	  m_top = m_sel - ( 4 - 1 );
	}
	//printf("top = %d sel = %d",m_top,m_sel);
}

void menu ::up (){
	if ( m_sel > 1 ){
	  m_sel -- ;
	  if ( m_sel < m_top ){
	    m_top = m_sel ;
	  }
	} else {//sel == 1
	  if ( m_top > 0 ){
	    m_top -- ;
	  }
	}

}

void menu ::display (){
  int i = 0 ;
  user_output_clear ();
  const menudata* b = m_b + m_top ;
  while ( i < 4 && b != m_e ){
    user_output_write ( i + 1 , 2 , b ->m_d );
    if ( b == ( m_b + m_sel )){
      //cursor
      user_output_write ( i + 1 , 1 , ">" );
    }
    i ++ ;
    b ++ ;
  }

}




/* void ConfigMgr ::DisplayMenuItems ( const menudata* b , const menudata* sel , const menudata* e ){
  int i = 0 ;
  user_output_clear ();
  while ( i < 4 && b != e ){
    user_output_write ( i + 1 , 2 , b ->m_d );
    if ( b == sel ){
      //cursor
      user_output_write ( i + 1 , 1 , ">" );
    }
    i ++ ;
    b ++ ;
  }
}
*/

