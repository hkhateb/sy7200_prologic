#include "Str_Def.h"
#include "Operation.hpp"
#include "LegacyTrans.hpp"
#include "Legacy.hpp"

extern LegacySetup SystemSetup; 

/**
 * Gets the message for initial swipe
 */
int Operation::GetEmployeeMessage(const char * badge,char * buff,int buffSize)
{
	if(!buff || buffSize == 0)return 0;
	//static char empmsg[32];
	buff[0] = '\0';
    InterfaceMessageBadgeHolder * holder = SystemSetup.message.badge;
    for(int i = 0; i < holder->size(); i++ )
    {
        const SysMessageBadgeItem& msgitem = holder->Get(i);
        const char * bdg = msgitem.badge;
        if( strcmp( badge, bdg ) == 0 )
        {
            //Check for initial swipe
            if( msgitem.initial_swipe )
            {
            	snprintf(buff,buffSize,"%s",msgitem.text);
            	TrimString(buff);
            	return strlen(buff);
            }
            return 0;
        }    
    }
    return 0;
}

/**
 * Gets the message for the function key
 */
int Operation::GetEmployeeMessage(const char * badge, const char functionkey,char * buff,int buffSize)
{
	if(!buff || buffSize == 0)return 0;
	//static char empmsg[32];
	buff[0] = '\0';
    InterfaceMessageBadgeHolder * holder = SystemSetup.message.badge;
    for(int i = 0; i < holder->size(); i++ )
    {
        const SysMessageBadgeItem& msgitem = holder->Get(i);
        const char * bdg = msgitem.badge;
        if( strcmp( badge, bdg ) == 0 )
        {
            /**
             * Check for initial swipe. If initial swipe is set to
             * true, return null
             */
            if( msgitem.initial_swipe )
            {
                return 0;
            }
            else if( msgitem.key == functionkey )
            {
            	snprintf(buff,buffSize,"%s",msgitem.text);
            	TrimString(buff);
            	return strlen(buff);
            }    
            return 0;
        }  
    }
    return 0;
} 

int Operation::GetPromptMessage(PromptType type,char * buff,int buffSize)
{
	// Max Message size is 16 and Max Display Len / Line = 20
	if(!buff || buffSize == 0)return 0;
	buff[0] = '\0';
	switch(type)
	{
		case TYPE_Idle:
			snprintf(buff,buffSize,"%s",SystemSetup.user.prompt.idle);
			//strcpy(message, SystemSetup.user.prompt.idle);
			break;
		case TYPE_InvalidSource:
			snprintf(buff,buffSize,"%s",SystemSetup.user.prompt.invalid_source);
			//strcpy(message, SystemSetup.user.prompt.invalid_source);
			break;
		case TYPE_InvalidBadge:
			snprintf(buff,buffSize,"%s",SystemSetup.user.prompt.invalid_badge);
			//strcpy(message, SystemSetup.user.prompt.invalid_badge);
			break;
		case TYPE_Try_Again:
			snprintf(buff,buffSize,"%s",SystemSetup.user.prompt.try_again);
			//strcpy(message, SystemSetup.user.prompt.try_again);
			break;
		case TYPE_EnterFunction:
			snprintf(buff,buffSize,"%s",SystemSetup.user.prompt.enter_function);
			//strcpy(message, SystemSetup.user.prompt.enter_function);
			break;
	}
	TrimString(buff);
	return strlen(buff);
} 

/**
 * Returns the message to be displayed for this Function
 */
const char * Operation::GetFunctionMessage(const SysFunctionKey key)
{
	static char fnmsg[21];
	SysFunctionDetailItem * detail_p = SystemSetup.function.detail;
	for(int i=0; i<SysFunctionDetailMax; i++)
	{
		if(detail_p->key == key)
		{
			strcpy(fnmsg, detail_p->message);
			return TrimString(fnmsg);
		}
		detail_p++;
	}
	return NULL;
}

/**
 * Returns the message to be displayed for this Function and the Level
 */
const char * Operation::GetFunctionMessageForLevel(const SysFunctionKey key, const SysFunctionLevel level)
{
	static char fnmsg[21];
	SysFunctionDetailItem * detail_p = SystemSetup.function.detail;
	for(int i=0; i<SysFunctionDetailMax; i++)
	{
		if(detail_p->key == key && detail_p->level == level)
		{
			strcpy(fnmsg, detail_p->message);
			return TrimString(fnmsg);
		}
		detail_p++;
	}
	return NULL;
}

