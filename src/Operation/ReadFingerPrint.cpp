//Copyright (C) 2005 TimeAmerica, Inc. All Rights Reserved
//20051109 wjm
//$Id: ReadFingerPrint.cpp,v 1.9 2007/02/05 22:12:36 walterm Exp $

#include <errno.h>
#include <string.h>
#include <Base64.h>
#include "Operation.hpp"
#include <sqlite3.h>
#include "ReadFingerPrint.hpp"
#include "StdInput.h"
#include "UserInput.hpp"
#include "logger.h"
#include "fpmodule.h"
#include "sqlitedb.h"

extern "C" {
#include "uf_api.h"
#include "uf_serial.h"
#include "uf_module_log.h"
#include "uf_log.h"
#include "uf_packet.h"
#include "uf_command.h"
}

#define FP_TEMPLATESIZE 384
#define ENCODED_TEMPLATESIZE 512

extern bool fpUnitEnabled;
extern bool start_template_read;

extern Logger * logger; 
extern const char* g_tmpmsg ;

using namespace std;

namespace {
  const unsigned int empty_id = 0xffffffff ;

  //********************************************************************
  class guard {
  public:
    guard (){
      pthread_mutex_lock ( & _ );
    }

    ~guard (){
      pthread_mutex_unlock ( & _ );
    }

    static pthread_mutex_t _ ;
  };

  //********************************************************************
  pthread_mutex_t guard ::_ = PTHREAD_MUTEX_INITIALIZER ;

  const UF_UINT32 const_default_userid = 0x7fffffff ;

  class LookasideTbl {
  public:
    LookasideTbl () : db ( 0 ){}
    UF_UINT32 find ( const FpModuleProxy ::FpModuleData );
    UF_UINT32 insert ( const FpModuleProxy ::FpModuleData );
    UF_UINT32 remove ( const FpModuleProxy ::FpModuleData );
    UF_UINT32 remove_by_badge ( const FpModuleProxy ::FpModuleData );//return first one that matches
    void clear ();

    const char* get_badge ( UF_UINT32 id , char* out , int* ix ); //return 0 if none, otherwise fill out and return out

  private:
    LookasideTbl ( const LookasideTbl & );
    LookasideTbl& operator = ( const LookasideTbl & );

    void create ();
    void exec_db ( const char* stm );
    UF_UINT32 exec_db_result ( const char* stm );

    void establishdb (){
      if ( ! db ){
	sqlite3_open( "TA" , & db );
	sqlite3_busy_timeout( db , 5000 );

      }
    }

    sqlite3* db ;
  };

  LookasideTbl* g_lookaside = 0 ;

  LookasideTbl* getFpLookaside (){
    if ( 0 == g_lookaside ){
      g_lookaside = new LookasideTbl ();
    }
    return g_lookaside ;
  }

  void ProcessFpModuleData ( const FpModuleProxy ::FpModuleData d ){
    switch ( d .cmd ){
    case FpModuleProxy ::FpModuleData ::cmd_Ins:{
      //handle db lookaside
      UF_UINT32 id = getFpLookaside () ->insert ( d );
      UF_UINT32 id2 = 0 ;
      //handle uf_...
      unsigned int s = FP_TEMPLATESIZE ;
      unsigned char t1 [ FP_TEMPLATESIZE ] ;
      Base64 b64 ;
      b64 .decode ( d .record .GetTemplate () , strlen ( d .record .GetTemplate ()) , t1 , s );      
      guard g ;
      uf_enroll_by_template ( id + 100 , ( UF_BYTE* ) t1 , sizeof ( t1 ) , ( UF_ENROLL_OPTION ) 0 , & id2 );
      //fixme: what should we do with non-ok return code?
      break ;
    }

    case FpModuleProxy ::FpModuleData ::cmd_Del0: {
      UF_UINT32 id = getFpLookaside () ->remove ( d );
      if ( empty_id != id ){
	guard g ;
	uf_delete_template ( id + 100 );
	//fixme: what should we do with non-ok ret code?
      }
      break ;
    }

    case FpModuleProxy ::FpModuleData ::cmd_Del1: {
      UF_UINT32 id = 0 ;
      while  ( empty_id != ( id =getFpLookaside () ->remove_by_badge ( d ))){
	guard g ;
	uf_delete_template ( id + 100 );
      //fixme: what should we do with non-ok return code?
      }
      break ;
    }

    case FpModuleProxy ::FpModuleData ::cmd_Del2: {
      getFpLookaside () ->clear ();
      guard g ;
      uf_delete_all_templates ();
      break ;
    }

    default:{
      break ;
    }

    }

    /*    
	  UF_UINT32 cnt ;
	  UF_RET_CODE r = uf_get_all_user_ids( & cnt );
	  printf ( "%s %d %d %d\n" , __FILE__ , __LINE__ , ( int ) r , ( int ) cnt );
	  UF_UINT32* d1 = new UF_UINT32 [ 4 * cnt ];
	  r = uf_get_user_data ( d1 , cnt );
	  delete [] d1 ;
    */
  }

  class SetDisplay {
  public:
    SetDisplay ( const char* c ) {
      g_tmpmsg = c ;
    }
    ~SetDisplay (){
      g_tmpmsg = 0 ;
    }
  };

  void ProcessFpModuleData (){
    FpModuleProxy* p = get_fp_module_proxy ();
    if ( 0 != p ->size ()){
      SetDisplay d ( "Busy - Please Wait" );
      while ( 0 != p ->size ()){
	ProcessFpModuleData ( p ->get ());
      }
    }
  }

};

ReadFingerPrint::ReadFingerPrint( int fd_wr )
{
	
  pthread_t fingerThread;
	
  pthread_attr_t fingerAttr;
  pthread_attr_init(&fingerAttr);
  pthread_attr_setstacksize ( & fingerAttr , const_thread_stack_size ());

  logger->info("Create ReadFingerThread");
  int ret = pthread_create(&fingerThread, &fingerAttr, FingerThread, (void*) fd_wr );
  pthread_attr_destroy (&fingerAttr);
  if ( ret != 0)
    {
      if( ret == EAGAIN ) logger->error("fingerThread : Out of Memory");
      else if ( ret == EINVAL ) logger->error( "fingerThread : Invalid Attribute");
#ifdef DEBUG	
      printf("ReadFingerPrint : Failed to Create Start fingerThread");
      printf("\n");
#endif
    }
	
  logger->info("FingerThread created");
	
  /*	pthread_t monitorThread;
	
  pthread_attr_t monitorAttr;
  pthread_attr_init(&monitorAttr);
  pthread_attr_setstacksize ( & monitorAttr , const_thread_stack_size ());

  logger->info("Create monitorThread");
  int ret = pthread_create(&monitorThread, &monitorAttr, MonitorThread, NULL);
  pthread_attr_destroy (&monitorAttr);
  if ( ret != 0)
  {
  if( ret == EAGAIN ) logger->error("monitorThread : Out of Memory");
  else if ( ret == EINVAL ) logger->error( "monitorThread : Invalid Attribute");
  #ifdef DEBUG	
  printf("ReadFingerPrint : Failed to Create Start monitorThread");
  printf("\n");
  #endif
  }
  logger->info("monitorThread created");
  */
	
}

void * ReadFingerPrint::FingerThread(void *arg)
{
  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
  int fd_wr = ( int ) arg ;

  int status;
  UF_RET_CODE ret;
  printf ( "%s %d FingerThread starting\n" , __FILE__ , __LINE__ );
  pthread_detach(pthread_self());
	
  fpUnitEnabled = false;
  if(initFpSdk())
    {
      fpUnitEnabled = true;
    }
  else
    {
      logger->info("Can not initialize fingerprint unit");
      return NULL;
    }

  logger->info("ReadFingerThread - Checking system status");
  ret = uf_check_system_status( (UF_SYSTEM_STATUS*)&status );
  if(ret != UF_RET_SUCCESS)
    {
      uf_cancel();
      logger->info("SS failed - ReadFingerThread Returning");
      return NULL;
    }

  if(status == UF_SYS_ALIVE)
    logger->info("SYSTEM IS ALIVE");
  else if(status == UF_SYS_WAIT)
    logger->info("SYSTEM WAITING FOR DATA");
  else if(status == UF_SYS_FAIL)
    logger->info("SYSTEM FAILURE");
  else if(status == UF_SYS_SENSOR_FAIL)
    logger->info("SYSTEM SENSOR FAILURE");
  else if(status == UF_SYS_BUSY)
    logger->info("SYSTEM BUSY");
  else
    {
      logger->info("STATUS UNKNOWN");
      usleep(10000);
      int status1;
      uf_cancel();

      //      uf_calibrate_sensor ();
      while(true)
	{
	  ret = uf_check_system_status( (UF_SYSTEM_STATUS*)&status1 );
	  if(ret != UF_RET_SUCCESS)
	    {
	      uf_cancel();
	      logger->info("SS failed - ReadFingerThread Returning");
	      return NULL;
	    }
			
	  if(status1 == UF_SYS_ALIVE || status1 == UF_SYS_WAIT || status1 == UF_SYS_FAIL || status1 == UF_SYS_SENSOR_FAIL || status1 == UF_SYS_BUSY )
	    {
	      status = status1;
	      if(uf_cancel() != UF_RET_SUCCESS)
		{
		  usleep(500000);
		}

	      break;
	    }
			
	  usleep(10000);
	}
    }	
			
  if(status == UF_SYS_BUSY) 
    {
      ret = uf_cancel();
      logger->info("SYSTEM BUSY - Cancel the command and retry");
      usleep(50000);
    }
	
  /////module is normal now - get the firmware version
	
  UF_UINT32 fware;
  if( uf_read_system_parameter(UF_SYS_FIRMWARE_VERSION, &fware ) == UF_RET_SUCCESS)
    {
      char fwa[10];
		
      sprintf(fwa, "%#x", ( unsigned int ) fware);
		
      char key[10];
      for(int i = 1; i< 11; i++)
	{
	  unsigned char mnib = fwa[i * 2];
	  if( mnib <= '9' ) mnib -= 48;
	  else mnib -= 55;
	  unsigned char lnib = fwa[i * 2 + 1];
	  if( lnib <= '9' ) lnib -= 48;
	  else lnib -= 55;
	  key[i] = ( mnib << 4 ) |  lnib;
	}		
      char b[5];
		
      //A16A need to be written as 1.6A
      for (int j = 2, m= 0; j < 5; j++, m++)
	{
	  b[m] = key[j];
	  if(m == 0)
	    {
	      m ++;
	      b[m] = '.';
	    }
	}
		
      char * f = b;
      FILE * fpv = fopen("firmware_version", "w");
      if( fpv != NULL )
	{
	  fwrite( f, 1, strlen(f), fpv );
	  fclose(fpv);
	}
    }
	
  const char * encoded_template ;
  UF_UINT32 rparam32_2;
  string templateStr;
  Base64 base64;
  Operation oper;
	
  uf_write_system_parameter(UF_SYS_SENSITIVIY, 0x37 );   		//highest sensitivity
  uf_write_system_parameter(UF_SYS_TIMEOUT, 0x3A );   			//timeout = 10 sec
  UF_RET_CODE result_1 ;
  result_1 = uf_set_read_timeout ( 10000 );
  if ( UF_RET_SUCCESS != result_1 ){
    printf ( "%s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
  }

  uf_write_system_parameter( UF_SYS_AUTO_RESPONSE, 0x30 );	// Auto Response = ON
  uf_write_system_parameter(UF_SYS_FREE_SCAN, 0x30 );  		// free scan = ON

  for(;;){
		
    while ( ! start_template_read ){
      ProcessFpModuleData ();
      usleep ( 100000 );
    }

    ProcessFpModuleData ();

    guard g ;
    UF_RET_CODE result ;
    result = uf_scan_template( &rparam32_2 );

    if ( UF_ERR_TIMEOUT == result || UF_ERR_SCAN_FAILED == result ){
      //nothing to do
    } else if( result == UF_ERR_TRY_AGAIN ){
      UserInput::ScanFailed = true;
      oper.TurnOnLEDYellow();
      oper.Beep(TYPE_Error);
      usleep(200000);
      oper.TurnOffLEDYellow();

    } else if ( result == UF_RET_SUCCESS ){
      printf ( "%s %d\n" , __FILE__ , __LINE__ );
      UF_BYTE* data_buf = (UF_BYTE*)malloc( rparam32_2 );
      ret = uf_get_raw_data( data_buf, rparam32_2 ) ; 

      if(ret != UF_RET_SUCCESS){
      printf ( "%s %d\n" , __FILE__ , __LINE__ );
	UserInput::ScanFailed = true;
      } else {
	start_template_read = false;

	oper.TurnOnLEDGreen();
	oper.Beep(TYPE_Accept);
	usleep(200000);
	oper.Beep(TYPE_TurnOff);
	oper.TurnOffLEDGreen();
	UserInput::PartialScanReady = true;

	base64.encode(data_buf, FP_TEMPLATESIZE , templateStr, false);
				
	encoded_template = templateStr.c_str();
	//	printf ( "%s %d %d %s\n" , __FILE__ , __LINE__ , strlen ( encoded_template ) , encoded_template );
	UserInput::TemplateScanReady = true;
	unsigned int sz = strlen(encoded_template);

	printf ( "%s %d %d\n" , __FILE__ , __LINE__ , ( int )sz );

	write ( fd_wr , & sz , sizeof ( sz ));
	write( fd_wr , encoded_template, sz );
      }

      free( data_buf ); 
    } else {
      printf ( "%s %d %d %d\n" , __FILE__ , __LINE__ , ( int ) result , ( int ) rparam32_2 );
      usleep ( 1000000 );
      uf_cancel ();
    }
  }  	//for loop	 
}

bool ReadFingerPrint::initFpSdk()
{
	
  char logstr[50];
	
  int mBaudRate;
  char mDeviceName[256];
  int mAsciiPacket =0;
  int mNetworkMode =0;
  int mModuleID = 1;
  int mTimeout =5000;   //timeout = 5000 ms
		
  mBaudRate = UF_BAUD115200;
  strcpy(mDeviceName, "/dev/tts/3");
	
  uf_init_log_msg();
		
  UF_LOG_ON( ERR_CH );
	
  UF_RET_CODE result = uf_initialize( mDeviceName, (UF_BAUDRATE)mBaudRate, (UF_BOOL)mAsciiPacket, (UF_BOOL)mNetworkMode, mModuleID, mTimeout );
  sprintf(logstr,"uf_initialize returned %d", result);
  logger->info(logstr);
		
  if( result == UF_RET_SUCCESS || result == 0)
    {
      logger->info("uf_initialize success - making fpUnitEnabled true");
      return true;
    }
  else
    {
      logger->info("uf_initialize failed - making fpUnitEnabled false");
      return false;
    }
}

const char* ReadFingerPrint ::Identify ( const char* tmpl , char* out_badgenr , int* ix ){
  guard g ;
  UF_UINT32 id = 0 ;
  UF_UINT32 subid = 0 ;

  uf_delete_template ( const_default_userid );
  UF_RET_CODE r ;
  if ( UF_RET_SUCCESS == ( r = uf_identify_by_template(( UF_BYTE* ) tmpl , FP_TEMPLATESIZE , & id , & subid ))){
    //look up badge number
    return getFpLookaside () ->get_badge ( id - 100 , out_badgenr , ix );
  } else {
    return 0 ;
  }
}

void ReadFingerPrint ::EnrollDefault ( const char* tmpl ){
  guard g ;
  UF_UINT32 id2 ;
  uf_enroll_by_template ( const_default_userid , ( UF_BYTE* ) tmpl , FP_TEMPLATESIZE , ( UF_ENROLL_OPTION ) 0 , & id2 );
}

bool ReadFingerPrint ::Verify ( const char* tmpl ){
  guard g ;
  UF_RET_CODE rc ;
  UF_UINT32 subid ;
  rc = uf_verify_by_template ( const_default_userid , ( UF_BYTE* ) tmpl , FP_TEMPLATESIZE , & subid );
  return UF_RET_SUCCESS == rc ;
  return false ;
}


//SQL fragments:

#define const_tablename "LOOKASIDE"
#define const_badgeid "BADGE"
#define const_indexid "FINGER"
#define const_userid "USER"
#define const_create "CREATE TABLE IF NOT EXISTS " const_tablename "(" \
    const_badgeid " CHAR(10), " \
    const_indexid " INTEGER, " \
    const_userid " INTEGER PRIMARY KEY AUTOINCREMENT, UNIQUE( " const_badgeid "," const_indexid "))"

#define const_CLEAR "DROP TABLE IF EXISTS " const_tablename 

#define const_insert "INSERT INTO " const_tablename "(" const_badgeid "," const_indexid ") VALUES ( '%s' , %d )"
#define const_find "SELECT " const_userid " FROM " const_tablename " WHERE " const_badgeid "='%s' AND " const_indexid "=%d"
#define const_find2 "SELECT " const_userid " FROM " const_tablename " WHERE " const_badgeid "='%s LIMIT 1"
#define const_delete "DELETE FROM " const_tablename " WHERE " const_userid "=%d" 
#define const_get_badge "SELECT " const_badgeid " ," const_indexid " FROM " const_tablename " WHERE " const_userid "=%d LIMIT 1"

//***************************************************************************************************************
namespace {

//***************************************************************************************************************
  UF_UINT32 LookasideTbl ::find ( const FpModuleProxy ::FpModuleData d ){
    char s [ 1000 ];
    sprintf ( s , const_find , d .record .GetExternalID () , d .record .GetFingerIndex ());
    return exec_db_result ( s );
  }

//***************************************************************************************************************
  UF_UINT32 LookasideTbl ::insert ( const FpModuleProxy ::FpModuleData d ){
    create ();
    //check if record is a replacement
    UF_UINT32 r = find ( d );
    if ( empty_id != r ){
      return r ;
    }

    //new record.
    char s [ 1000 ];
    sprintf ( s , const_insert , d .record .GetExternalID (), d .record .GetFingerIndex ());
    exec_db ( s );
    return find ( d );
  }

//***************************************************************************************************************
  UF_UINT32 LookasideTbl ::remove ( const FpModuleProxy ::FpModuleData d ){
    create ();
    UF_UINT32 r = find ( d );
    if ( empty_id != r ){
      char s [ 100 ];
      sprintf ( s , const_delete , ( int ) r );
      exec_db ( s );
    }

    return r ;
  }

//***************************************************************************************************************
  UF_UINT32 LookasideTbl ::remove_by_badge ( const FpModuleProxy ::FpModuleData d ){
    create ();
    char s [ 1000 ];
    sprintf ( s , const_find2 , d .record .GetExternalID () );
    UF_UINT32 r = exec_db_result ( s );
    if ( empty_id != r ){
      sprintf ( s , const_delete , ( int ) r );
      exec_db ( s );
    }
    
    return r ;
  }

//***************************************************************************************************************
  const char* LookasideTbl ::get_badge ( UF_UINT32 id , char* out , int* ix1 ){
    if ( ! db ){
      establishdb ();
    }

    if ( ! db ){
      return 0 ;
    }

    int num_rows ;
    int num_cols ;
    char** result = 0 ;
    char s [ 100 ];
    sprintf ( s , const_get_badge , ( int ) id );
    int sq_ret ;
    while ( SQLITE_BUSY == ( sq_ret = sqlite_prot_get_table ( db , s , & result , & num_rows , & num_cols , 0 ))){
      if ( result ){
	sqlite3_free_table ( result );
	result = 0 ;
      }
    }

    if ( SQLITE_OK != sq_ret ){
      if ( result ){
	sqlite3_free_table ( result );
	result = 0 ;
      }
      return 0 ;
    }

    if ( num_rows < 1 ){
      if ( result ){
	sqlite3_free_table ( result );
	result = 0 ;
      }
      return 0 ;
    }

    if ( 0 == result ){
      return 0 ;
    }

    int ix = num_rows * num_cols ;
    strcpy ( out , result [ ix ] );
    *ix1 = atoi ( result [ ix + 1 ]);

    if ( result ){
      sqlite3_free_table( result );
      result = 0 ;
    }

    usleep ( 100000 );
    return out ;
  }

//***************************************************************************************************************
  void LookasideTbl ::clear (){
    exec_db ( const_CLEAR );
  }
//***************************************************************************************************************
  void LookasideTbl ::create (){
    exec_db ( const_create );
  }

//***************************************************************************************************************
  void LookasideTbl ::exec_db ( const char* stm ){
    if ( ! db ){
      establishdb ();
    }
    if ( ! db ){
      return ;
    }

    char* zErrMsg ;
    int sq_ret ;
    while ( SQLITE_BUSY == ( sq_ret = sqlite_prot_exec(db, stm, NULL, NULL, &zErrMsg ))){
      sqlite3_free ( zErrMsg );
    }

    if ( SQLITE_OK != sq_ret ){
      sqlite3_free(zErrMsg);
    }
    usleep ( 100000 );
  }

//***************************************************************************************************************
  UF_UINT32 LookasideTbl ::exec_db_result ( const char* stm ){
    if ( ! db ){
      establishdb ();
    }

    if ( ! db ){
      return empty_id ;
    }

    int num_rows ;
    int num_cols ;
    char** result = 0 ;

    int sq_ret ;
    while ( SQLITE_BUSY == ( sq_ret = sqlite_prot_get_table ( db , stm , & result , & num_rows , & num_cols , 0 ))){
      if ( result ){
	sqlite3_free_table ( result );
	result = 0 ;
      }
    }

    if ( SQLITE_OK != sq_ret ){
      if ( result ){
	sqlite3_free_table( result );
	result = 0 ;
      }
      return empty_id ;
    }

    if ( num_rows < 1 ){
      if ( result ){
	sqlite3_free_table( result );
	result = 0 ;
      }
      return empty_id ;
    }

    if ( 0 == result ){
      return empty_id ;
    }

    int ix = num_rows * num_cols ;
    int ret = atoi ( result [ ix ] ) ;

    if ( result ){
      sqlite3_free_table( result );
      result = 0 ;
    }

    usleep ( 100000 );
    return (UF_UINT32) ret ;
  }
};
