#include "Config.h"
#include "SystemGlobals.h"

extern Logger * logger;

/******************************************************************
 * ******************* Utilities Menu *****************************
 * ***************************************************************/

/**
 * Menu Strings
 */
static const char * UTILITIES_HDNG		= "     UTILITIES      ";
static const char * UTILITIES_TELNET	= "1-TELNET           ";
static const char * UTILITIES_WATCHDOG	= "2-WATCHDOG           ";
static const char * UTILITIES_AUTORSTRT	= "3-AUTO RESTART       ";


static const char * UTILITIES_SYNC_TIME	= "2-SYNC TIME        ";
static const char * SYNC_TIME_HDNG = 	  "SYNCHRONIZE TIME   ";
static const char * SYNC_TIME_DISPLAY1 =  "  SYNCHRONIZE TIME  ";
static const char * SYNC_TIME_DISPLAY2 =  "   PLEASE WAIT...   ";
//
static const char * UTILITIES_POPULATE_TEMPLATES=	"3-GET TEMPLATES    ";
static const char * POPULATE_TEMPLATES_HDNG = 		"POPULATE TEMPLATES";
static const char * POPULATE_TEMPLATES_DISPLAY1 =   "POPULATE TEMPLATES";
static const char * POPULATE_TEMPLATES_DISPLAY2 =   "   PLEASE WAIT...   ";

static const char * GETACTIVITIES_DISPLAY1 =  "  GET ACTIVITIES    ";
static const char * GETACTIVITIES_DISPLAY2 =  "   PLEASE WAIT...   ";

static const char * TELNET_HDNG			= "       TELNET       ";
static const char * TELNET_ENABLE		= "1-ENABLE TELNET    ";
static const char * TELNET_DISABLE		= "2-DISABLE TELNET   ";


#ifdef CLIENT_PROLOGIC

// watchdog not supported for this client

#else

static const char * WATCHDOG_HDNG		= "       WATCHDOG      ";
static const char * WATCHDOG_ENABLE		= "1-ENABLE WATCHDOG    ";
static const char * WATCHDOG_DISABLE	= "2-DISABLE WATCHDOG   ";

static const char * AUTORSTRT_HDNG		= "    AUTO RESTART      ";
static const char * AUTORSTRT_ENABLE	= "1-ENABLE  	";
static const char * AUTORSTRT_DISABLE	= "2-DISABLE 	";

static const char * AUTORSTRT_ENABLE_HDNG	= "    AUTO RESTART      ";
static const char * AUTORSTRT_DAILY			= "1-DAILY	";
static const char * AUTORSTRT_WEEKLY		= "2-WEEKLY	";

#endif


#ifdef CLIENT_PROLOGIC_showutil

extern bool bStartingUp;

OperationStatusCode UtilConfig::ShowPrologicScreen()
{
	int timeout;
	char input;
	OperationStatusCode code;
start:
	//timeout = oper.GetSupervisorModeTimeout();
	// big timeout:
	timeout = 3600000; // 1 hour ?
	for(;;)
	{
		user_output_clear();
		user_output_write(1, 1, UTILITIES_HDNG);
		user_output_write(2, 2, UTILITIES_GETACTIVITIES);

		user_output_write(3, 2, UTILITIES_SYNC_TIME);
		user_output_write(4, 2, UTILITIES_POPULATE_TEMPLATES);

		user_output_write(2, 1, CURSOR);

		int numMenuItemMinLoc = 2;
		int numMenuItemMaxLoc = 4;

		int cursorloc = 2;	// number of menu items

		while(true)
		{
			int tm = 0;
			while (!user_input_ready () && tm < timeout)
			{
				usleep(10000);
				tm += 20;
			}
			if(tm >= timeout) return SC_TIMEOUT;
			input = user_input_get();
			switch(input)
			{
				case '1': // Show telnet screen
					oper.Beep(50, TYPE_KeyPress);
					code = ShowTelnetScreen();
					if(code == SC_Clear) goto start;
					else return code;
					break;

				case '2': // force time synchronization :
					oper.Beep(50, TYPE_KeyPress);
					code = ShowSyncTimeScreen();
					if ( code == SC_Clear )
					{
						goto start;
					}
					else
					{
						return code;
					}
					break;
				case '3': // force populate templates :
					oper.Beep(50, TYPE_KeyPress);
					code = ShowPopulateTemplatesScreen();

//pthread_mutex_lock(pwaitforpopulatetemplates_mutex); // wait until that thread has released mutex
//pthread_mutex_unlock(pwaitforpopulatetemplates_mutex); // once locked, release it.


					if ( code == SC_Clear )
					{
						goto start;
					}
					else
					{
						return code;
					}
					break;


				case '-': // Down Arrow
					if(cursorloc < numMenuItemMaxLoc )
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(cursorloc++, 1, BLANKSPACE);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					user_output_write(cursorloc, 1, CURSOR);
					break;
				case '+': // Up Arrow
					if(cursorloc > numMenuItemMinLoc )
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(cursorloc--, 1, BLANKSPACE);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					user_output_write(cursorloc, 1, CURSOR);
					break;
				case '\n':
					oper.Beep(50, TYPE_KeyPress);
					if(cursorloc == 2)
					{
						code = ShowTelnetScreen();
					    if(code == SC_Clear) goto start;
					    else return code;
					}


					else if(cursorloc == 3)
					{
						code = ShowSyncTimeScreen();
					    if(code == SC_Clear) goto start;
					    else return code;
					}
					else if(cursorloc == 4)
					{
						code = ShowPopulateTemplatesScreen();
					    if(code == SC_Clear) goto start;
					    else return code;
					}

					break;
				case ESC:
					oper.Beep(50, TYPE_KeyPress);
					return SC_Clear;
				break;
				default:
				       oper.Beep(150,TYPE_Reject);
                       oper.displayErrMessage("   NOT AVAILABLE    ");
                       goto start;
                 break;
			}
		}
	}
	return SC_Success;
}

#endif

OperationStatusCode UtilConfig::ShowUtilitiesScreen()
{
	int timeout;
	char input;
	OperationStatusCode code;
start:
	//timeout = oper.GetSupervisorModeTimeout();
	// big timeout:
	timeout = 3600000; // 1 hour ?
	for(;;)
	{
		user_output_clear();
		user_output_write(1, 1, UTILITIES_HDNG);
		user_output_write(2, 2, UTILITIES_TELNET);

		user_output_write(3, 2, UTILITIES_SYNC_TIME);
		user_output_write(4, 2, UTILITIES_POPULATE_TEMPLATES);

		user_output_write(2, 1, CURSOR);
		//user_output_write(3, 2, UTILITIES_WATCHDOG);
		//user_output_write(4, 2, UTILITIES_AUTORSTRT);

		//int numMenuItems = 3;
		int numMenuItemMinLoc = 2;
		int numMenuItemMaxLoc = 4;

		int cursorloc = 2;	// number of menu items

		while(true)
		{
			int tm = 0;
			while (!user_input_ready () && tm < timeout)
			{
				usleep(10000);
				tm += 20;
			}
			if(tm >= timeout) return SC_TIMEOUT;
			input = user_input_get();
			switch(input)
			{
				case '1': // Show telnet screen
					oper.Beep(50, TYPE_KeyPress);
					code = ShowTelnetScreen();
					if(code == SC_Clear) goto start;
					else return code;
					break;

				case '2': // force time synchronization :
					oper.Beep(50, TYPE_KeyPress);
					code = ShowSyncTimeScreen();
					if ( code == SC_Clear )
					{
						goto start;
					}
					else
					{
						return code;
					}
					break;
				case '3': // force populate templates :
					oper.Beep(50, TYPE_KeyPress);
					code = ShowPopulateTemplatesScreen();

//pthread_mutex_lock(pwaitforpopulatetemplates_mutex); // wait until that thread has released mutex
//pthread_mutex_unlock(pwaitforpopulatetemplates_mutex); // once locked, release it.


					if ( code == SC_Clear )
					{
						goto start;
					}
					else
					{
						return code;
					}
					break;


				case '-': // Down Arrow
					if(cursorloc < numMenuItemMaxLoc )
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(cursorloc++, 1, BLANKSPACE);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					user_output_write(cursorloc, 1, CURSOR);
					break;
				case '+': // Up Arrow
					if(cursorloc > numMenuItemMinLoc )
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(cursorloc--, 1, BLANKSPACE);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					user_output_write(cursorloc, 1, CURSOR);
					break;
				case '\n':
					oper.Beep(50, TYPE_KeyPress);
					if(cursorloc == 2)
					{
						code = ShowTelnetScreen();
					    if(code == SC_Clear) goto start;
					    else return code;
					}


					else if(cursorloc == 3)
					{
						code = ShowSyncTimeScreen();
					    if(code == SC_Clear) goto start;
					    else return code;
					}
					else if(cursorloc == 4)
					{
						code = ShowPopulateTemplatesScreen();
					    if(code == SC_Clear) goto start;
					    else return code;
					}

					break;
				case ESC:
					oper.Beep(50, TYPE_KeyPress);
					return SC_Clear;
				break;
				default:
				       oper.Beep(150,TYPE_Reject);
                       oper.displayErrMessage("   NOT AVAILABLE    ");
                       goto start;
                 break;
			}
		}
	}
	return SC_Success;
}

OperationStatusCode UtilConfig::ShowTelnetScreen()
{

    int timeout ;
	int cursorloc = 2;
	char input;

start:	user_output_clear();
	user_output_write(1, 1, TELNET_HDNG);
	user_output_write(2, 2, TELNET_ENABLE);
	user_output_write(2, 1, CURSOR);
	user_output_write(3, 2, TELNET_DISABLE);
	timeout = oper.GetSupervisorModeTimeout();

	while(true)
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout)
		{
			usleep(10000);
			tm += 20;
		}
		if(tm >= timeout) return SC_TIMEOUT;
		input = user_input_get();
		switch(input)
		{
			case '1': // Enable telnet
				oper.Beep(50, TYPE_KeyPress);
				return EnableTelnet();
				break;
			case '2': // Network Status
				oper.Beep(50, TYPE_KeyPress);
				return DisableTelnet();
				break;
			case '-': // Down Arrow
				if(cursorloc < 3)
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc++, 1, BLANKSPACE);
				}
				else
				    oper.Beep(50,TYPE_Reject);
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '+': // Up Arrow
				if(cursorloc > 2)
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc--, 1, BLANKSPACE);
				}
				else
				    oper.Beep(50,TYPE_Reject);
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '\n':
				oper.Beep(50, TYPE_KeyPress);
				if(cursorloc == 2)
				{
					return EnableTelnet();
				}
				else if(cursorloc == 3)
				{
					return DisableTelnet();
				}
				break;
			case ESC:
				oper.Beep(50, TYPE_KeyPress);
				return SC_Clear;
			break;
			default:
				oper.Beep(150,TYPE_Reject);
                oper.displayErrMessage("   NOT AVAILABLE    ");
                goto start;
            break;
		}
	}

	return SC_Success;
}

extern bool get_server_time_flag;

OperationStatusCode UtilConfig::ShowSyncTimeScreen()
{
	bool confirm = false;
	LCDHandler lcdhdlr;
	LCD_READ_STATE state = lcdhdlr.ShowConfirmationDialog(SYNC_TIME_HDNG, confirm);
	if(state == READ_TIMEDOUT)
	{
		return SC_TIMEOUT;
	}
	else if(confirm)
	{
		user_output_clear();
		user_output_write(2, 1, SYNC_TIME_DISPLAY1);
		user_output_write(3, 1, SYNC_TIME_DISPLAY2);

		#ifdef DEBUG
		DP("Sync'ing Time...\n");
		#endif

// TODO
		// call the LocalTime web service operation here.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// trigger the condition to get LocalTime requested
		//

		if (pgetservertime_cond_flag)
		{
			pthread_cond_signal(pgetservertime_cond_flag);
			get_server_time_flag = true;
		}
		else
		{
			user_output_clear();
			user_output_write(2, 1,"SyncTime not running.");
			user_output_write(3, 1,"Please try again or reset");
			usleep( 2000000 );
			return SC_Clear;
		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		usleep(2000000);

		// Make the compiler happy
		//return SC_Success;
		return SC_Clear;
	}
	else
	{
		return SC_Clear;
	}

}

OperationStatusCode UtilConfig::ShowPopulateTemplatesScreen()
{
	bool confirm = false;
	LCDHandler lcdhdlr;
	LCD_READ_STATE state = lcdhdlr.ShowConfirmationDialog(POPULATE_TEMPLATES_HDNG, confirm);
	if(state == READ_TIMEDOUT)
	{
		return SC_TIMEOUT;
	}
	else if(confirm)
	{
		user_output_clear();
		user_output_write(2, 1, POPULATE_TEMPLATES_DISPLAY1);
		user_output_write(3, 1, POPULATE_TEMPLATES_DISPLAY2);

		#ifdef DEBUG
		DP("Populate Templates...\n");
		#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// trigger the condition to get PopulateFPTemplates requested
		//
DP("showptscreen: check pt_cond_flag ptr\n");
		if (ppopulatetemplates_cond_flag)
		{
DP("showptscreen: pt_cond_flag ptr is non-zero\n");
			user_output_clear();
			int pcs;

// testing: go ahead and signal
DP("showptscreen: before signaling holder of populatetemplates_cond_flag\n");
			pcs = pthread_cond_signal(ppopulatetemplates_cond_flag);
DP("showptscreen: result of signal pt is %d\n...before lock ppt_mutex\n", pcs);



DP("showptscreen: lock ppt_mutex...");
			usleep(200);
			return SC_Clear;
		}
		else
		{
DP("showptscreen: pt_cond_flag ptr is NULL or 0\n");

			user_output_clear();
			user_output_write(1, 1,"PopulateTemplates");
			user_output_write(2, 1,"is not running.");
			user_output_write(3, 1,"Please try again");
			user_output_write(4, 1,"or reset the clock");
			sleep( 2 );
			return SC_Clear;
		}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//usleep(2000000);
		usleep(2000);

		// Make the compiler happy
		//return SC_Success;
		return SC_Clear;
	}
	else
	{
		return SC_Clear;
	}

}

OperationStatusCode UtilConfig::ShowGetActivitiesScreen()
{
	bool confirm = false;
	LCDHandler lcdhdlr;
	LCD_READ_STATE state = lcdhdlr.ShowConfirmationDialog(POPULATE_TEMPLATES_HDNG, confirm);
	if(state == READ_TIMEDOUT)
	{
		return SC_TIMEOUT;
	}
	else if(confirm)
	{
		user_output_clear();
		user_output_write(2, 1, GETACTIVITIES_DISPLAY1);
		user_output_write(3, 1, GETACTIVITIES_DISPLAY2);

		#ifdef DEBUG
		DP("Get Activities...\n");
		#endif

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		// trigger the condition to get GetActivites requested
		//

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//usleep(2000000);
		usleep(2000);

		// Make the compiler happy
		//return SC_Success;
		return SC_Clear;
	}
	else
	{
		return SC_Clear;
	}

}

OperationStatusCode UtilConfig::EnableTelnet()
{
	system("killall telnetd");
	system("telnetd");
	user_output_clear();
	user_output_write(2, 4, "TELNET ENABLED");
	usleep(500000);
	return SC_Success;
}

OperationStatusCode UtilConfig::DisableTelnet()
{
	system("killall telnetd");
	user_output_clear();
	user_output_write(2, 3, "TELNET DISABLED");
	usleep(500000);
	return SC_Success;
}

#ifdef CLIENT_PROLOGIC

// watchdog is not supported for this client

#else


OperationStatusCode UtilConfig::ShowWatchDogScreen()
{

    int timeout ;
	int cursorloc = 2;
	char input;


start:	user_output_clear();
	user_output_write(1, 1, WATCHDOG_HDNG);
	user_output_write(2, 2, WATCHDOG_ENABLE);
	user_output_write(2, 1, CURSOR);
	user_output_write(3, 2, WATCHDOG_DISABLE);
	timeout = oper.GetSupervisorModeTimeout();
	while(true)
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout)
		{
			usleep(10000);
			tm += 20;
		}
		if(tm >= timeout) return SC_TIMEOUT;
		input = user_input_get();
		switch(input)
		{
			case '1': // Enable watchdog
				oper.Beep(50, TYPE_KeyPress);
				return EnableWatchdog();
				break;
			case '2': // Disable watchdog
				oper.Beep(50, TYPE_KeyPress);
				return DisableWatchdog();
				break;
			case '-': // Down Arrow
				if(cursorloc < 3)
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc++, 1, BLANKSPACE);
				}
				else
				    oper.Beep(50,TYPE_Reject);
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '+': // Up Arrow
				if(cursorloc > 2)
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc--, 1, BLANKSPACE);
				}
				else
				    oper.Beep(50,TYPE_Reject);
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '\n':
				oper.Beep(50, TYPE_KeyPress);
				if(cursorloc == 2)
				{
					return EnableWatchdog();
				}
				else if(cursorloc == 3)
				{
					return DisableWatchdog();
				}
				break;
			case ESC:
				oper.Beep(50, TYPE_KeyPress);
				return SC_Clear;
			break;
			default:
				oper.Beep(150,TYPE_Reject);
                oper.displayErrMessage("   NOT AVAILABLE    ");
                goto start;
            break;
		}
	}

	return SC_Success;
}

OperationStatusCode UtilConfig::ShowAutoRestartScreen()
{

	int timeout ;
	int cursorloc = 2;
	char input;

start:	user_output_clear();
	user_output_write(1, 1, AUTORSTRT_HDNG);
	user_output_write(2, 2, AUTORSTRT_ENABLE);
	user_output_write(2, 1, CURSOR);
	user_output_write(3, 2, AUTORSTRT_DISABLE);
	timeout = oper.GetSupervisorModeTimeout();

	while(true)
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout)
		{
			usleep(10000);
			tm += 20;
		}
		if(tm >= timeout) return SC_TIMEOUT;
		input = user_input_get();
		switch(input)
		{
			case '1': // Enable AutoRestart
				oper.Beep(50, TYPE_KeyPress);
				return EnableAutoRestart();
				break;
			case '2': // Disable AutoRestart
				oper.Beep(50, TYPE_KeyPress);
				return DisableAutoRestart();
				break;
			case '-': // Down Arrow
				if(cursorloc < 3)
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc++, 1, BLANKSPACE);
				}
				else
				 oper.Beep(50,TYPE_Reject);
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '+': // Up Arrow
				if(cursorloc > 2)
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc--, 1, BLANKSPACE);
				}
				else
				    oper.Beep(50,TYPE_Reject);
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '\n':
				oper.Beep(50, TYPE_KeyPress);
				if(cursorloc == 2)
				{
					return EnableAutoRestart();
				}
				else if(cursorloc == 3)
				{
					return DisableAutoRestart();
				}
				break;
			case ESC:
				oper.Beep(50, TYPE_KeyPress);
				return SC_Clear;
			default:
				oper.Beep(150,TYPE_Reject);
                oper.displayErrMessage("   NOT AVAILABLE    ");
                goto start;
            break;
		}
	}

	return SC_Success;
}


OperationStatusCode UtilConfig::EnableWatchdog()
{
	const char * app_file = "/mnt/flash/terminal/applstart.sh";
	char data[58];
	bool found = false;

	//Add tag to properties.xml file
	FILE * fpp = fopen(XML_PROPERTIES_FILE, "r");
	if( !fpp )
	{
		#ifdef DEBUG
		perror("Couldn't open the properties.xml");
		#endif
	}
	else
	{
		FILE * fwp = fopen(TMP_XML_PROPERTIES_FILE, "w");
		if( !fwp )
		{
			#ifdef DEBUG
			perror("Couldn't open the propertiestmp.xml");
			#endif
			fclose( fpp );
		}
		else
		{
			char * line = fgets(data, 58, fpp);
			const char * new_line = "\t<watchdog>ON</watchdog>"  ;
			while(line != NULL)
			{
				if( strstr(line, "</properties>") != NULL )
				{
					fputs(new_line, fwp);
					fputs("\n", fwp);
				}
				if(strstr(line, "watchdog") != NULL)
				{
//					DP("\n ignore this line. DO not write in tmp file");
				}
				else
					fputs(line, fwp);

				line = fgets(data, 58, fpp);
			}

		 	fclose( fpp );
		 	fclose( fwp );

		 	rename(TMP_XML_PROPERTIES_FILE, XML_PROPERTIES_FILE);
	 	}
 	}

	//set the flag to true. Clock program will write the timestamp to file if this flag is true
	//When the clock reboots, set this flag based on properties.xml (on/off)
	oper.SetWatchOption(1);

	//Update applstart.sh by adding watchdog to it.
	//check if the watchdog entry is already there
	FILE * fp = fopen(app_file, "r");
	if( !fp )
	{
	  fprintf(stderr, "Couldn't open the applstart.sh file\n");
	}
	else
	{
 		char * line = fgets(data, 58, fp);
	 	while(line != NULL)
		{
			if( strstr(line, "WatchDog") != NULL )
			{
				found = true;
				break;
			}
			line = fgets(data, 58, fp);
		}
		fclose(fp);
	}

	if(! found)
	{
		//add watchdog entry to the file
		FILE * fw = fopen("/mnt/flash/terminal/applstart.sh", "a");
		if( !fw )
		{
			fprintf(stderr, "Couldn't open the applstart.sh file\n");
		}
		else
		{
			fputs("./mnt/flash/terminal/bin/WatchDog &", fw);
		    fputs("\n", fw);
			fclose(fw);
		}
	}

	system("killall WatchDog");
	system("./WatchDog &");

	user_output_clear();
	user_output_write(2, 3, "WATCHDOG ENABLED");
	usleep(500000);
	return SC_Success;
}

OperationStatusCode UtilConfig::DisableWatchdog()
{
	const char * app_file = "/mnt/flash/terminal/applstart.sh";
	const char * tmp_app_file = "/mnt/flash/terminal/tmp_applstart.sh";
	char data[58];

	//Add tag to properties.xml file
	FILE * fpp = fopen(XML_PROPERTIES_FILE, "r");
	if( !fpp )
	{
	  #ifdef DEBUG
	  DP("Couldn't open the properties.xml");
	  #endif
	}
	else
	{
 		 FILE * fwp = fopen(TMP_XML_PROPERTIES_FILE, "w");
		 if( !fwp )
		 {
			  #ifdef DEBUG
			  DP("Couldn't open the propertiestmp.xml");
			  #endif
			  fclose( fpp );
		 }
		 else
		 {
	 		 char * line = fgets(data, 58, fpp);
		 	 const char * new_line = "\t<watchdog>OFF</watchdog>\n\t<autorestart>OFF</autorestart>"  ;
			 while(line != NULL)
			 {
				if( strstr(line, "</properties>") != NULL )
				{
					fputs(new_line, fwp);
					fputs("\n", fwp);
				}

				if(strstr(line, "watchdog") != NULL || strstr(line, "autorestart") != NULL)
				{
//				 	DP("\n ignore this line. DO not write in tmp file");
			 	}
				else
					fputs(line, fwp);

				line = fgets(data, 58, fpp);
			}

		 	fclose( fpp );
		 	fclose( fwp );
 			rename(TMP_XML_PROPERTIES_FILE, XML_PROPERTIES_FILE);
		}
	}

	//set the flag to true. Clock program will not write the timestamp to file if this flag is false
	//When the clock reboots, set this flag based on properties.xml (on/off)
	oper.SetWatchOption(0);
	oper.SetAutoRestart(0);

	//Update applstart.sh by removing watchdog from it.
	FILE * fp = fopen(app_file, "r");
	if( !fp )
	{
	  fprintf(stderr, "Couldn't open the applstart.sh file\n");
	}
	else
	{
 		 FILE * fw = fopen(tmp_app_file, "w");
		 if( !fw )
		 {
		  	fprintf(stderr,"Couldn't open the tmp_applstart.sh\n");
		  	fclose( fp );
		 }
		 else
		 {
		 	char * line = fgets(data, 58, fp);
			while(line != NULL)
			{
				if( strstr(line, "WatchDog") != NULL )
				{
					//skip this line
					line = fgets(data, 58, fp);
					continue;
				}
				fputs(line, fw);
				line = fgets(data, 58, fp);
			}

		 	fclose( fp );
		 	fclose( fw );
		 	rename(tmp_app_file, app_file);
		}
	}

	system("killall WatchDog");

	//also remove the watched file
	remove("ta.watched");

	user_output_clear();
	user_output_write(2, 3, "WATCHDOG DISABLED");
	usleep(500000);
	return SC_Success;
}


OperationStatusCode UtilConfig::EnableAutoRestart()
{
	char line[100];
	char tstamp[50];
	OperationStatusCode code;
	int timeout ;
	int cursorloc = 2;
	char input;
	bool done = false;

start:	user_output_clear();
	user_output_write(1, 1, AUTORSTRT_ENABLE_HDNG);
	user_output_write(2, 2, AUTORSTRT_DAILY);
	user_output_write(2, 1, CURSOR);
	user_output_write(3, 2, AUTORSTRT_WEEKLY);

	timeout = oper.GetSupervisorModeTimeout();

	while(true)
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout)
		{
			usleep(10000);
			tm += 20;
		}
		if(tm >= timeout) return SC_TIMEOUT;
		input = user_input_get();
		switch(input)
		{
			case '1': // Get Timestamp for daily reboot
				oper.Beep(50, TYPE_KeyPress);
				strcpy(line, "AUTO RESTART DAILY : ");
				code = GetDailyTimestamp(tstamp);
				if(code == SC_Clear) goto start;
			    else if(code != SC_Success)
					    return code;
				strcat(line, tstamp);
				done = true;
				break;
			case '2': // Get Timestamp for weekely reboot
				oper.Beep(50, TYPE_KeyPress);
				strcpy(line, "AUTO RESTART WEEKLY : ");
				code = GetWeeklyTimestamp(tstamp);
				if(code == SC_Clear) goto start;
			    else if(code != SC_Success)
					    return code;
				strcat(line, tstamp);
				done = true;
				break;
			case '-': // Down Arrow
				if(cursorloc < 3)
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc++, 1, BLANKSPACE);
				}
				else
				    oper.Beep(50,TYPE_Reject);
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '+': // Up Arrow
				if(cursorloc > 2)
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc--, 1, BLANKSPACE);
				}
				else
				    oper.Beep(50,TYPE_Reject);
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '\n':
				oper.Beep(50, TYPE_KeyPress);
				if(cursorloc == 2)
				{
					strcpy(line, "AUTO RESTART DAILY : ");
					code = GetDailyTimestamp(tstamp);
					if(code == SC_Clear) goto start;
			         else if(code != SC_Success)
					    return code;
					strcat(line, tstamp);
				}
				else if(cursorloc == 3)
				{
					strcpy(line, "AUTO RESTART WEEKLY : ");
					code = GetWeeklyTimestamp(tstamp);
					if(code == SC_Clear) goto start;
			        else if(code != SC_Success)
					    return code;
					strcat(line, tstamp);
				}
				done = true;
				break;
			case ESC:
				oper.Beep(50, TYPE_KeyPress);
				return SC_Clear;
		    default:
				oper.Beep(150,TYPE_Reject);
                oper.displayErrMessage("   NOT AVAILABLE    ");
                goto start;
            break;
		}

		if(done)
			break;
	}

	//set the flag to true. Clock program will write the timestamp to file if this flag is true
	//When the clock reboots, set this flag based on properties.xml (on/off)
	oper.SetAutoRestart(1);
	oper.SetWatchOption(1);

	FILE * fpt = fopen("ta.watched", "w");
	if( !fpt )
	{
		fprintf(stderr, "Couldn't open the watch file\n");
	}
	else
	{
		fputs(line, fpt);
		fputs("\n", fpt);
		//close the file
		fclose(fpt);
	}

	const char * app_file = "/mnt/flash/terminal/applstart.sh";
	char data[58];
	bool found = false;

	//Add tag to properties.xml file
	FILE * fpp = fopen(XML_PROPERTIES_FILE, "r");
	if( !fpp )
	{
	  #ifdef DEBUG
	  perror("Couldn't open the properties.xml");
	  #endif
	}
	else
	{
	 	 FILE * fwp = fopen(TMP_XML_PROPERTIES_FILE, "w");
		 if( !fwp )
		 {
			  #ifdef DEBUG
			  perror("Couldn't open the propertiestmp.xml");
			  #endif
			  fclose( fpp );
		 }
	 	 else
		 {
		 	 char * line = fgets(data, 58, fpp);
		 	 const char * new_line = "\t<watchdog>ON</watchdog>\n\t<autorestart>ON</autorestart>"  ;
			 while(line != NULL)
			 {
				 if( strstr(line, "</properties>") != NULL )
				 {
				   fputs(new_line, fwp);
				   fputs("\n", fwp);
				 }

				 if(strstr(line, "watchdog") != NULL || strstr(line, "autorestart") != NULL)
				 {
//				 	DP("\n ignore this line. DO not write in tmp file");
				 }
				 else
				 	fputs(line, fwp);

				 line = fgets(data, 58, fpp);
			 }

		 	fclose( fpp );
		 	fclose( fwp );

		 	rename(TMP_XML_PROPERTIES_FILE, XML_PROPERTIES_FILE);
	 	}
 	}


	//Update applstart.sh by adding watchdog to it.
	//check if the watchdog entry is already there
	FILE * fp = fopen(app_file, "r");
	if( !fp )
	{
	  fprintf(stderr, "Couldn't open the applstart.sh file\n");
	}
	else
	{
 		char * line = fgets(data, 58, fp);
	 	while(line != NULL)
		{
			if( strstr(line, "WatchDog") != NULL )
			{
				found = true;
				break;
			}
			line = fgets(data, 58, fp);
		}
		fclose(fp);
	}

	if(! found)
	{
		//add watchdog entry to the file
		FILE * fw = fopen("/mnt/flash/terminal/applstart.sh", "a");
		if( !fw )
		{
			fprintf(stderr, "Couldn't open the applstart.sh file\n");
		}
		else
		{
			fputs("./mnt/flash/terminal/bin/WatchDog &", fw);
		    fputs("\n", fw);
			fclose(fw);
		}
	}

	system("killall WatchDog");
	system("./WatchDog &");

	user_output_clear();
	user_output_write(2, 2, "AUTO RSTRT ENABLED");
	usleep(500000);
	return SC_Success;
}

OperationStatusCode UtilConfig::DisableAutoRestart()
{
	const char * app_file = "/mnt/flash/terminal/applstart.sh";
	const char * tmp_app_file = "/mnt/flash/terminal/tmp_applstart.sh";
	char data[58];

	//Add tag to properties.xml file
	FILE * fpp = fopen(XML_PROPERTIES_FILE, "r");
	if( !fpp )
	{
	  #ifdef DEBUG
	  DP("Couldn't open the properties.xml");
	  #endif
	}
	else
	{
 		 FILE * fwp = fopen(TMP_XML_PROPERTIES_FILE, "w");
		 if( !fwp )
		 {
			  #ifdef DEBUG
			  DP("Couldn't open the propertiestmp.xml");
			  #endif
			  fclose( fpp );
		 }
		 else
		 {
	 		 char * line = fgets(data, 58, fpp);
		 	 const char * new_line = "\t<watchdog>OFF</watchdog>\n\t<autorestart>OFF</autorestart>"  ;
			 while(line != NULL)
			 {
				if( strstr(line, "</properties>") != NULL )
				{
					fputs(new_line, fwp);
					fputs("\n", fwp);
				}

				if(strstr(line, "watchdog") != NULL || strstr(line, "autorestart") != NULL)
				{
//					DP("\n ignore this line. DO not write in tmp file");
				}
				else
					fputs(line, fwp);

				line = fgets(data, 58, fpp);
			}

		 	fclose( fpp );
		 	fclose( fwp );
 			rename(TMP_XML_PROPERTIES_FILE, XML_PROPERTIES_FILE);
		}
	}

	//set the flag to true. Clock program will not write the timestamp to file if this flag is false
	//When the clock reboots, set this flag based on properties.xml (on/off)
	oper.SetAutoRestart(0);
	oper.SetWatchOption(0);

	//Update applstart.sh by removing watchdog from it.
	FILE * fp = fopen(app_file, "r");
	if( !fp )
	{
	  fprintf(stderr, "Couldn't open the applstart.sh file\n");
	}
	else
	{
 		 FILE * fw = fopen(tmp_app_file, "w");
		 if( !fw )
		 {
		  	fprintf(stderr,"Couldn't open the tmp_applstart.sh\n");
		  	fclose( fp );
		 }
		 else
		 {
		 	char * line = fgets(data, 58, fp);
			while(line != NULL)
			{
				if( strstr(line, "WatchDog") != NULL )
				{
					//skip this line
					line = fgets(data, 58, fp);
					continue;
				}
				fputs(line, fw);
				line = fgets(data, 58, fp);
			}

		 	fclose( fp );
		 	fclose( fw );
		 	rename(tmp_app_file, app_file);
		}
	}

	system("killall WatchDog");

	//also remove the watched file
	remove("ta.watched");

	user_output_clear();
	user_output_write(2, 2, "AUTO RSTRT DISABLED");
	usleep(500000);
	return SC_Success;
}

OperationStatusCode UtilConfig::GetDailyTimestamp(char * tstamp)
{
	char buffer[6];

	strcpy(buffer, "00:00");
	int buflen = strlen(buffer);
	buffer[buflen] = '\0';

	int col = 12 - buflen + 1;
	char input;
	int numOfChars = 0;

	user_output_clear();
	user_output_write(2, 2, "ENTER TIME (HH:MM)");
	int timeout = oper.GetSupervisorModeTimeout();

	user_output_write(3, col, buffer);

	static char disp[2];
	disp[1] = '\0';

	if(col > 1)
	{
		user_output_cursor(USER_CURSOR_BOTH);
		user_output_write(3, col - 1, BLANKSPACE);
	}
	while(true)
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout)
		{
			usleep(10000);
			tm += 20;
		}
		if(tm >= timeout)
		{
			oper.Beep(TYPE_TimeOut);
			user_output_cursor(USER_CURSOR_OFF);
			return SC_TIMEOUT;
		}
		input = user_input_get ();
		if( input == '\n' )
		{
			oper.Beep(TYPE_KeyPress);
			break;
		}
		if( input == ESC )
		{
			oper.Beep(TYPE_KeyPress);
			user_output_cursor(USER_CURSOR_OFF);
			return SC_Clear;
		}
		switch(input)
		{
			case '>':
				if(numOfChars < 4)
				{
					oper.Beep(TYPE_KeyPress);
					disp[0] = buffer[numOfChars];
					user_output_write(3, numOfChars + col, disp);
					numOfChars++;
					if(numOfChars == 2 )
					{
						disp[0] = buffer[numOfChars];
						user_output_write(3, numOfChars + col, disp);
						numOfChars++;
					}
				}
				break;
			case '<':
				oper.Beep(TYPE_KeyPress);
				if(numOfChars > 0)
				{
					if(numOfChars == 1)
					{
						if(col > 1)
						{
							user_output_write(3, numOfChars + col - 2, BLANKSPACE);
						}
					}
					else
					{
						disp[0] = buffer[numOfChars - 2];
						user_output_write(3, numOfChars + col - 2, disp);
					}
					numOfChars--;
					if(numOfChars == 2 )
					{
						disp[0] = buffer[numOfChars - 2];
						user_output_write(3, numOfChars + col - 2, disp);
						numOfChars--;
					}
				}
				break;
			default:
				if(input < '0' || input > '9')
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}

				//for hours - can not start with '3' or higher
				if(numOfChars == 0 && input > '2')
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}

				//for hours - can not be > 23
				if(numOfChars == 1 && buffer[0] == '2' && input > '3')
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}

				//for minutes - can not start with 6 or higher
				if(numOfChars == 3 && input > '5')
				{
					oper.Beep(150, TYPE_Reject);
					continue;
				}
				oper.Beep(TYPE_KeyPress);
				buffer[numOfChars] = input;

				disp[0] = input;
				user_output_write(3, numOfChars + col, disp);

				if(numOfChars == 4)
				{
					disp[0] = buffer[numOfChars - 1];
					user_output_write(3, numOfChars + col - 1, disp);
				}

				if(numOfChars < 4)
				{
					numOfChars ++;
				}

				if(numOfChars == 2)
				{
					disp[0] = buffer[numOfChars];
					user_output_write(3, numOfChars + col, disp);
					numOfChars++;
				}
				break;
		}
	}

	user_output_cursor(USER_CURSOR_OFF);
	strcpy(tstamp, buffer);
	return SC_Success;
}

OperationStatusCode UtilConfig::GetWeeklyTimestamp(char * tstamp)
{
	char day[5];
	char input;
	int screen_number = 1;
	bool done = false;
	int cursorloc = 2;

	int timeout;

start:	timeout = oper.GetSupervisorModeTimeout();
	ShowDayMenuScreen1();
	screen_number = 1;
	user_output_write(2, 1, CURSOR);


	while(true)
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout)
		{
			usleep(10000);
			tm += 20;
		}
		if(tm >= timeout) return SC_TIMEOUT;
		input = user_input_get();
		switch(input)
		{
			case '1':
				oper.Beep(50, TYPE_KeyPress);
				strcpy(day, "Sun ");
				done = true;
				break;
			case '2':
				oper.Beep(50, TYPE_KeyPress);
				strcpy(day, "Mon ");
				done = true;
				break;
			case '3':
				oper.Beep(50, TYPE_KeyPress);
				strcpy(day, "Tue ");
				done = true;
				break;
			case '4':
				oper.Beep(50, TYPE_KeyPress);
				strcpy(day, "Wed ");
				done = true;
				break;
			case '5':
				oper.Beep(50, TYPE_KeyPress);
				strcpy(day, "Thu ");
				done = true;
				break;
			case '6':
				oper.Beep(50, TYPE_KeyPress);
				strcpy(day, "Fri ");
				done = true;
				break;
			case '7':
				oper.Beep(50, TYPE_KeyPress);
				strcpy(day, "Sat ");
				done = true;
				break;
			case '-': // Down Arrow
				if(cursorloc < 4)
				{
					oper.Beep(50, TYPE_KeyPress);
					user_output_write(cursorloc++, 1, BLANKSPACE);
				}
				else
				{
					if(screen_number == 1)
					{
						oper.Beep(50, TYPE_KeyPress);
						ShowDayMenuScreen2();
						screen_number = 2;
					}
					else if(screen_number == 2)
					{
						oper.Beep(50, TYPE_KeyPress);
						ShowDayMenuScreen3();
						screen_number = 3;
					}
					else if(screen_number == 3)
					{
						oper.Beep(50, TYPE_KeyPress);
						ShowDayMenuScreen4();
						screen_number = 4;
					}
					else if(screen_number == 4)
					{
						oper.Beep(50, TYPE_KeyPress);
						ShowDayMenuScreen5();
						screen_number = 5;
					}
					else
					    oper.Beep(50,TYPE_Reject);
				}

				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '+': // Up Arrow
				if( screen_number == 1)
				{
					if(cursorloc > 2)
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(cursorloc--, 1, BLANKSPACE);
					}
					else
					   oper.Beep(150,TYPE_Reject);
				}
				else
				{
					if(cursorloc > 1)
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(cursorloc--, 1, BLANKSPACE);
					}
					else
					{
						if(screen_number == 2)
						{
							oper.Beep(50, TYPE_KeyPress);
							ShowDayMenuScreen1();
							screen_number = 1;
							cursorloc = 2;
						}
						else if(screen_number == 3)
						{
							oper.Beep(50, TYPE_KeyPress);
							ShowDayMenuScreen2();
							screen_number = 2;
						}
						else if(screen_number == 4)
						{
							oper.Beep(50, TYPE_KeyPress);
							ShowDayMenuScreen3();
							screen_number = 3;
						}
						else if(screen_number == 5)
						{
							oper.Beep(50, TYPE_KeyPress);
							ShowDayMenuScreen4();
							screen_number = 4;
						}
					}
				}
				user_output_write(cursorloc, 1, CURSOR);
				break;
			case '\n':
				oper.Beep(50, TYPE_KeyPress);
				switch(screen_number)
				{
					case 1:
						if(cursorloc == 2)
						{
							strcpy(day, "Sun ");
						}
						else if(cursorloc == 3)
						{
							strcpy(day, "Mon ");
						}
						else if(cursorloc == 4)
						{
							strcpy(day, "Tue ");
						}
						break;
					case 2:
						if(cursorloc == 1)
						{
							strcpy(day, "Sun ");
						}
						else if(cursorloc == 2)
						{
							strcpy(day, "Mon ");
						}
						else if(cursorloc == 3)
						{
							strcpy(day, "Tue ");
						}
						else if(cursorloc == 4)
						{
							strcpy(day, "Wed ");
						}
						break;
					case 3:
						if(cursorloc == 1)
						{
							strcpy(day, "Mon ");
						}
						else if(cursorloc == 2)
						{
							strcpy(day, "Tue ");
						}
						else if(cursorloc == 3)
						{
							strcpy(day, "Wed ");
						}
						else if(cursorloc == 4)
						{
							strcpy(day, "Thu ");
						}
						break;
					case 4:
						if(cursorloc == 1)
						{
							strcpy(day, "Tue ");
						}
						else if(cursorloc == 2)
						{
							strcpy(day, "Wed ");
						}
						else if(cursorloc == 3)
						{
							strcpy(day, "Thu ");
						}
						else if(cursorloc == 4)
						{
							strcpy(day, "Fri ");
						}
						break;
					case 5:
						if(cursorloc == 1)
						{
							strcpy(day, "WED ");
						}
						else if(cursorloc == 2)
						{
							strcpy(day, "Thu ");
						}
						else if(cursorloc == 3)
						{
							strcpy(day, "Fri ");
						}
						else if(cursorloc == 4)
						{
							strcpy(day, "Sat ");
						}
						break;
				}
				done = true;
				break;
			case ESC:
				oper.Beep(50, TYPE_KeyPress);
				return SC_Clear;
				break;
			default:
				oper.Beep(150,TYPE_Reject);
                oper.displayErrMessage("   NOT AVAILABLE    ");
                goto start;
            break;
		}

		if(done)
			break;
	}

	strcpy(tstamp, day);

	//show the screen to enter time

	char time[10];

	OperationStatusCode code = GetDailyTimestamp(time);
	if(code != SC_Success)
		return code;

	strcat(tstamp, time);

	return SC_Success;
}

void UtilConfig::ShowDayMenuScreen1()
{
	user_output_clear();
	user_output_write(1, 4, "  SELECT DAY  ");
	user_output_write(2, 2, "1-SUNDAY    ");
	user_output_write(3, 2, "2-MONDAY    ");
	user_output_write(4, 2, "3-TUESDAY   ");

}

void UtilConfig::ShowDayMenuScreen2()
{
	user_output_clear();
	user_output_write(1, 2, "1-SUNDAY    ");
	user_output_write(2, 2, "2-MONDAY    ");
	user_output_write(3, 2, "3-TUESDAY   ");
	user_output_write(4, 2, "4-WEDNESDAY ");
}

void UtilConfig::ShowDayMenuScreen3()
{
	user_output_clear();
	user_output_write(1, 2, "2-MONDAY    ");
	user_output_write(2, 2, "3-TUESDAY   ");
	user_output_write(3, 2, "4-WEDNESDAY ");
	user_output_write(4, 2, "5-THURSDAY  ");

}

void UtilConfig::ShowDayMenuScreen4()
{
	user_output_clear();
	user_output_write(1, 2, "3-TUESDAY   ");
	user_output_write(2, 2, "4-WEDNESDAY ");
	user_output_write(3, 2, "5-THURSDAY  ");
	user_output_write(4, 2, "6-FRIDAY    ");

}

void UtilConfig::ShowDayMenuScreen5()
{
	user_output_clear();
	user_output_write(1, 2, "4-WEDNESDAY ");
	user_output_write(2, 2, "5-THURSDAY  ");
	user_output_write(3, 2, "6-FRIDAY    ");
	user_output_write(4, 2, "7-SATURDAY  ");
}

#endif // not CLIENT_PROLOGIC
