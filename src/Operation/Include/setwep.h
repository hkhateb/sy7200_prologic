/*
* Function setWepKey - Sets the wep key on a given interface
*
*  Parameters:
*
*(in) interfaceName - Name of the interface to set the Wep key on (i.e. eth0)
*(in) index - key index 0-4 or 0x80 for key 0 with the unicast transmit key set as well
*(in) key - pointer to buffer containing key material
*(in) keylen - length of the key
*(in/out) errMsg - if not null this may contain a returned message if an error occurs.
*
*Returns:
* true when WEP was set
* false if an error occurs
*
*/

#ifndef __SETWEP_H__

bool setWepKey (const char *interfaceName,const int index,const unsigned char* key,const int keylen);

#define __SETWEP_H__
#endif


