#ifndef _CONFIGMGR_H_
#define _CONFIGMGR_H_


#include "Config.h"
#include "LegacyTrans.hpp"


#include "HttpClient.h"
#include "Base64.h"

class OperationIODriver;

class menudata ;
class ConfigMgr
{
public:
	ConfigMgr();

	void setHttpServer(HttpClient* httpSer,char* LinkWeb,char* ClockId,char* Account)
	{
		httpS = httpSer;
	    this->LinkWeb = LinkWeb;
		this->ClockId = ClockId;
		this->Account = Account;
	}

	virtual ~ConfigMgr(){};
	OperationStatusCode ShowConfigurationMenu();

	OperationIODriver *operationiod;
	void setOperationIODriver(OperationIODriver * operiod);

	OperationStatusCode HandleMenu ( const menudata* b , const menudata* e ,int top = 0 ,int sel = 1 ,int timeOut = 0 );
	//	void ShowCfgMenuScreen1();
	//	void ShowCfgMenuScreen2();
	//	void ShowCfgMenuScreen3();
	//	void ShowCfgMenuScreen4();
	//	void ShowCfgMenuScreen5();
	//	void ShowCfgMenuScreen6();
	OperationStatusCode DispatchDates (){
	  //return dtCfg .ShowDateAndTimeMenuScreen ();
	  return dtCfg.ShowConfigureDateScreen();
	}

    OperationStatusCode DispatchDaylightSavings (){
	  //return dtCfg .ShowDateAndTimeMenuScreen ();
	  return dtCfg.ShowDaylightSavingsScreen();
    }

	OperationStatusCode DispatchConfigAccess (){
	  return accessCfg .ShowConfigAccessScreen ();
	}

	OperationStatusCode DispatchUtilities (){
	  return utilCfg .ShowUtilitiesScreen ();
	}

	OperationStatusCode DispatchReaders (){
	  return rdrCfg.ShowReadersScreen();
	}

	OperationStatusCode SwitchRelay1 ();
	OperationStatusCode SwitchRelay2 ();
	OperationStatusCode SwitchRelay3 ();
	OperationStatusCode SwitchRelay4 ();
    void UpdateAccessoryTestMenu();
	OperationStatusCode SwitchRelay (int relay);

	OperationStatusCode FPEnroll();
    OperationStatusCode FPVerification();
	OperationStatusCode FPIdentification();
	OperationStatusCode FPDeleteUser();
	OperationStatusCode FPDeleteAllUsers();
	OperationStatusCode FPUGetNumberOffUsers();
	bool ifOverFlow(char* buffer);

	OperationStatusCode Nop (){
	  return SC_Clear ;
	}



	OperationStatusCode ShowSelTCPIPCommScreen();
	OperationStatusCode ShowClockInfoScreen();
	OperationStatusCode ShowAppVersion();
	OperationStatusCode HandleResetClock();
	OperationStatusCode HandleResetAPPL();
	OperationStatusCode ShowFirmwareVersion();

	OperationStatusCode ConfigMgr ::ShowAccessoryScreen();
	OperationStatusCode ConfigMgr ::ShowFingerPrintScreen();
	OperationStatusCode ConfigMgr ::ShowTerminalResetScreen();

	OperationStatusCode ShowVersionMenu ();
	OperationStatusCode DispatchWiredSettings ();
	OperationStatusCode DispatchWirelessSettings ();

	OperationStatusCode DispatchAppVer ();
	OperationStatusCode DispatchRelVer ();
	OperationStatusCode DispatchRootfsVer ();
	OperationStatusCode DispatchKernelVer ();
	OperationStatusCode DispatchFirmwareVer ();
	OperationStatusCode MenuFromBuffer ( const char* heading , const char* buffer );




	void SetTimeSyncConditionFlag(pthread_cond_t * servertime_cond_flag )
	{
		utilCfg.SetTimeSyncConditionFlag( servertime_cond_flag);
	}
	void SetPopulateTemplatesConditionFlag(pthread_cond_t * populatetemplates_cond_flag )
	{
		utilCfg.SetPopulateTemplatesConditionFlag( populatetemplates_cond_flag);
	}
	void SetWaitForPopulateTemplatesThreadConditionFlag(pthread_cond_t * waitforpopulatetemplatesthread_cond_flag )
	{
		utilCfg.SetWaitForPopulateTemplatesThreadConditionFlag( waitforpopulatetemplatesthread_cond_flag);
	}
	void SetPopulateTemplatesMutex(pthread_mutex_t * populatetemplates_mutex )
	{
		utilCfg.SetPopulateTemplatesMutex( populatetemplates_mutex );
	}
	void SetWaitForPopulateTemplatesMutex(pthread_mutex_t * waitforpopulatetemplates_mutex )
	{
		utilCfg.SetWaitForPopulateTemplatesMutex( waitforpopulatetemplates_mutex );
	}





private:
	OperationStatusCode HandleVersionUI ();
	//	void DisplayMenuItems ( const menudata* b , const menudata* sel , const menudata* e );

	const char* BreakLine ( const char* b , const char* e );
	OperationStatusCode MenuFromFile ( const char* heading , const char* filename );

	friend class menudata ;
	int currentCfgScr;
	int currentCrsLoc;
	Operation oper;
	AccessConfig accessCfg;
	TCPIPConfig tcpipCfg;
	DateTimeConfig dtCfg;
	UtilConfig utilCfg;
	ReaderConfig rdrCfg;
	HttpClient* httpS;
	char* LinkWeb;
    char* ClockIp;
    char* ClockId;
    char* Account;
};

#endif //_CONFIGMGR_H_
