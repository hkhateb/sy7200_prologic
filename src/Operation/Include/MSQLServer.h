#ifndef _MSQLSERVER_H_
#define _MSQLSERVER_H_

#include <string>
#include <ctpublic.h>
#include "MSQLSDebug.h"
extern "C" {
	#include "common.h"
}

class MSQLServer.h
{

  int maxcol ;
  int colsize ;
  bool connected;
  public:
	struct _col { 
		CS_DATAFMT datafmt;
		CS_INT datalength;
		CS_SMALLINT ind;
		CS_CHAR data[colsize];
	} col[maxcol];
   
  MSQLServer(int maxcol = 10 ,int colsize = 100,bool con = false):maxcol(maxcol),colsize(colsize),connected(con){}
  
  
  CS_RETCODE login();
  CS_RETCODE logout();
  CS_RETCODE get_query_data(string query);
  CS_RETCODE fetch_query_data(CS_COMMAND *cmd);
  CS_RETCODE get_check_query_column(CS_COMMAND *cmd,CS_INT *num_cols,bool is_status_result);
  CS_RETCODE bind_query_data(CS_COMMAND *cmd,CS_INT num_cols,CS_INT datatype,CS_INT format,CS_INT maxlength,CS_INT count,CS_LOCALE *locale);      
}