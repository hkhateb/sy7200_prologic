#ifndef _TA7000INIT_H_
#define _TA7000INIT_H_

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "System.hpp"
#include "Legacy.hpp"
#include "Error.hpp"
#include "Str_Def.h"

class TA7000Init
{
public:
	TA7000Init();	
	virtual ~TA7000Init();
	void InitLegacySetup();
	void CleanLegacySetup();
	Code LoadLegacySetup();
};

#endif //_TA7000INIT_H_
