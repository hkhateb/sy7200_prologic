#ifndef _FPRM_H_
#define _FPRM_H_
#include "FPOperation.hpp"
#include "XMLProperties.h"
#include "Base64.h"

#define ADD_USERS_LIST "GetFPUAddUsersResult"
#define DELETE_USERS_LIST "GetFPUDeleteUsersResult"
#define LIST_ELEMENT "User"

extern FPOperation fpOpr;

class FPRemoteManagement
{
	public:
		FPRemoteManagement(){};
		~FPRemoteManagement(){};
		OperationStatusCode AddUsersListToFPU(const char* xmlAddUsersList);
		OperationStatusCode DeleteUsersListFromFPU(const char* xmlDeleteUsersList);
	private:
} ;
#endif
