#ifndef OPERATIONDRIVER_HPP
#define OPERATIONDRIVER_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include <pthread.h>

#include "Operation.hpp"
#include "System.hpp"
#include "SystemGlobals.h"
#include "Interface.hpp"
#include "LegacyTrans.hpp"
#include "LCDHandler.h"
#include "XMLProperties.h"
#include "Str_Def.h"
#include "ConfigMgr.h"
#include "FPOperation.hpp"
#include <semaphore.h>
#include <signal.h>
#include <valarray>

#ifndef __TA7700_TERMINAL__
#define ESC 'q'
#endif

#define URL_DEFAULT "https://www.schedulesource.net/Comair/teamwork/Services/V200/clock.asmx"
#define TIMEOUT_DEFAULT "15"
#define HEARTBEAT_TIMER_DEFAULT "60"
#define GETPROJECTTASKS_TIMER_DEFAULT "86400"


// note: usage must append ';'
//#define LOCKS		{pthread_mutex_lock  (&OperationIODriver::soap_request_mutex);}
//#define LOCKXM		{pthread_mutex_lock  (&OperationIODriver::xml_parser_mutex);}
//#define UNLOCKS 	{pthread_mutex_lock  (&OperationIODriver::soap_request_mutex);}
//#define UNLOCKXM 	{pthread_mutex_unlock(&OperationIODriver::xml_parser_mutex);}
//
#define LOCKS
#define LOCKXM
#define UNLOCKS
#define UNLOCKXM



class FingerTemplate ;


static sem_t offline_tran_count;
static pthread_cond_t  offline_cond_flag;
static pthread_cond_t  heartbeat_cond_flag;
static pthread_cond_t  getprojects_cond_flag;

static int offline_thread_is_started = 0;

// move this part to OperationIODriver class : static pthread_cond_t  OperationIODriver::getservertime_cond_flag;

static XMLProperties* getWeb ;
static char SwipeAndGoKey = 'A';
static int Contrast = 0x8000;
static int BuzzerVolume = 500;

static bool TurnOnSwipAndGo = false;
static bool EnableSwipAndGo = true;
static char updateTimeClock[10];
static char populateTemplatesTime[10];
// move to class : static bool AllowPopulateTemplates = true;
static char ClockInLevel[4][5];
static char ClockInLevelMassage[4][20];
static int ClockInLevelDataMax[4];
static int reqCount;
static int DataLevelStage[4]; // 0. Project Switch 1. Project Code 2. Task Code 3.Activity Code
int parseMessage(char* prop ,char* line1,char* line2,char* line3,char* line4);
/* A mutex protecting soap requests. */
//*******************************************************************************

enum SaveReason
{
	OFFLINE_MODE = 0,
	SENDIND_FAILED,
	GETPROJECTTASKS_FAILED
};

class OperationDriver : public InterfaceBuffer
{
protected:
  Operation * oper;
  LegacyTransItem * transitem;
public:
  OperationDriver()
  {
    oper = new Operation();
  };
  virtual ~OperationDriver()
  {
    delete oper;
  }
  virtual Code RunIdleDisplayProcess()=0;
  virtual OperationStatusCode ReadBadge()=0;
  virtual OperationStatusCode ReadBadge2()=0;
  virtual OperationStatusCode ReadFunction(BadgeType)=0;


};

//*******************************************************************************
class OperationFileDriver : public OperationDriver
{
public:
  OperationFileDriver();
  virtual ~OperationFileDriver();
  Code Process();
};

//*******************************************************************************
class OperationIODriver : public OperationDriver
{
public:
  OperationIODriver();
  virtual ~OperationIODriver();
  Code RunIdleDisplayProcess();
  OperationStatusCode ReadBadge();
  OperationStatusCode ReadBadge2();
  OperationStatusCode ReadFunction(BadgeType);

  OperationStatusCode ForceActivityCodeFunction(BadgeType);

  Operation* GetOperation(){return oper;}
  static pthread_mutex_t lockDisplay; //Controls access to Display
  static pthread_mutex_t soap_request_mutex;
  static pthread_mutex_t xml_parser_mutex;
  static pthread_mutex_t offline_request_mutex;
  static pthread_mutex_t get_projects_mutex;


  static pthread_cond_t  getservertime_cond_flag;
  static pthread_cond_t  populatetemplatesthread_cond_flag;
  static pthread_cond_t  waitforpopulatetemplatesthread_cond_flag;
  static pthread_cond_t  templatesavailable_cond_flag;

  static pthread_mutex_t populatetemplates_mutex;
  static pthread_mutex_t waitforpopulatetemplates_mutex;

  static pthread_cond_t  GetPinTablethread_cond_flag;
  static pthread_mutex_t waitforGetPinTable_mutex;


#ifdef DEBUGSEGVMT	//-------------------------------------------------

  static pthread_mutex_t mt_sig_mutex;

#endif				//-------------------------------------------------


  static bool			 ptPredicate;
  static pthread_cond_t  waitforstartup_cond_flag;

  void WaitForStartup();


  char* getBadgeBuffer(){return buffer;}
  SysInputSource getSourceBadge(){return source;}
  SysFunctionKey getFunctionKey(){return Function;}


	static bool OfflineModeIsActive;
	static bool OfflineModeIsEnabled;

	void initOfflineMode();

  static bool TurnOnSwipeAndGo;
  static bool communication ;

  static bool AllowPopulateTemplates;

    /**
   * Level Data
   */
  SysLevelData levels[SysFunctionKeyLevelsMax];


  // public for prologic
  OperationStatusCode SuperReadEmpBadge();
  OperationStatusCode ReadPIN();
  char* GetPinCode(){return pinBuffer;}

protected:
  void ReadSource(SourceType);
  OperationStatusCode ReadIdentifyKeys();
  // make this public for prologic: OperationStatusCode SuperReadEmpBadge();
  OperationStatusCode AddSuperTrans(SysDate & date, SysTimeSecs & time);
  OperationStatusCode ReadDate();
  OperationStatusCode ReadTime();

  OperationStatusCode ReadTemplate(FingerTemplate& );

  OperationStatusCode EditTrans();
  OperationStatusCode DisplayTrans();
  static void * RunBellSchedule(void *);
  static void * RunHeartBeat(void *);
  static void *RunOffLineSender(void * id);
  static void *RunGetServerTime(void * id);
  static void *RunWatchDogThread(void * id);
  static void * RunGetProjectTasks (void *);
  static void * CheckAccessWindow(void *);
  static void * ShowIdleScreen(void *);
  static void UnlockDisplay();
  static void * MonitorDateChange(void * id);
  static void * AllowAccess(void * id);
  static void IncrementAccessCount(int cnt);
  static void DecrementAccessCount(int cnt);
  void OperationIODriver::displayMessage(char* msg);
  //static int SaveTransaction(OffLineTranRecord* R,SaveReason sr = OFFLINE_MODE);
 // static int UpdateProjectTasksInDB(const char* projectsXmlList);
//  static int UpdateActitvitiesInDB(const char* actXmlList);
 // static void fromProjectListToRecord( list<ElmData>& projectsList,list<ElmData>::iterator& i,ProjectTaskRecord& R);
 // static void fromActivitiesListToRecord( list<ElmData>& actList,list<ElmData>::iterator& i,ActivityRecord& R);
  /**
   * Variables
   */
  char buffer[128];
  SysFunctionKey Function;
  char sourcechar;
  char supersourcechar;
  SysInputSource source;
  char empbadge [SysFieldMaxBadgeLength+1];
  char superbadge [SysFieldMaxBadgeLength+1];
  struct sigaction sa;
  ConfigMgr cfgMgr;

  /**
   * Transaction Flags
   * flags[0] - Access Flag
   * flags[1] - Profile Flag
   * flags[2] - Profile Number
   * flags[3] - Access Override
   * flags[4] - Access Denied Transactions
   */
  Bit8 flags[SysTransactionFlagsMax];



private:

  char pinBuffer[5];

  bool HasSuperPriv;
  bool HasConfigPriv;
  bool SuperEmployee;

  bool checkOtherPrivileges;
  bool function_read ;

  static pthread_mutex_t lockAccessCount; //Controls Access Count
  static pthread_mutex_t lockDB;
  static int accessCount;
  LCDHandler lcdhdlr;
  void AcquireProgrammingLock();
  void AcquireTemplateLock();
  void AcquireDBLock();

  void HandleClockOutOfMemory();
  void SetEnvHome();
  OperationStatusCode getProjectTaskAndActivity(const char * badge, const SysFunctionKey source,
										          const BadgeType type, SysLevelData * levels,
												  Bit8 * flags ,OperationStatusCode code);
  int UpdateProjectTasksDB(char* projectsXmlList);
  OperationStatusCode CreateFingerprintEnrollTransaction(const char* ExternalID, int FingerIndex, const char* TemplateData, int PrivLevel);
  void SetupProgramClock();
  void SetupFunctionKeys();
  void ResetFunctionKeys();
  void SetClockInFunctionKey(int FunctionKey);
  void SetInPunchFunctionKey(int FunctionKey);
  void SetOutPunchFunctionKey(int FunctionKey);
  static int callback(void *NotUsed, int argc, char **argv, char **azColName)
  {
    if(NotUsed == 0)
      {
	for(int i=0; i<argc; i++){
	  printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
      }
    return 0;
  }

  void SetPinCode(char *pin)
  {
	strcpy(pinBuffer,pin);
  }

};

//*******************************************************************************

#endif // #endif // OPERATIONDRIVER_HPP
