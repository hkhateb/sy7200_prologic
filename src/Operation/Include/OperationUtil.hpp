#ifndef OPERATIONUTIL_HPP
#define OPERATIONUTIL_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "System.hpp"
#include "Interface.hpp"

Code GetSysInputSourceForChar(const char source, SysInputSource & flag);
Code GetCharForSysInputSource(SysInputSource flag, char & source);

#endif // #endif // OPERATIONUTIL_HPP
