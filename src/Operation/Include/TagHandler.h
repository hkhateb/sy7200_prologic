#ifndef _TAGHANDLER_H_
#define _TAGHANDLER_H_

/**
 * The Server sends down the Clock Program to us one tag at
 * a time. Some of the tags sent are not in the format that 
 * we want. This class handles all the tags that need formatting
 */

class TagHandler
{
public:
	TagHandler();
	virtual ~TagHandler();
	/**
	 * Reads the Tag from the packet. 
	 * Formats the tag if needed.
	 * We receive the entire packet sent by the server, 
	 * which contains the STX, ETX and the CRC
	 */
	const char * GetFormattedTag(char * buffer, int pktSize);
	
private:
	const char * GetFormattedBSTag(char * buffer, int pktSize, char * tag);
	const char * GetFormattedBaudRateTag(char * buffer, int pktSize, char * tag);
	const char * GetFormattedFunctionKeyTag(char * buffer, int pktSize, char * tag);
	const char * GetFormattedAWTag(char * buffer, int pktSize, char * tag);
	const char * GetFormattedProfileMsgTag(char * buffer, int pktSize, char * tag);
	const char * GetFormattedEMTag(char * buffer, int pktSize, char * tag);
	
	const char * GetWeekFlagsFromByte(char flag);
};

#endif //_TAGHANDLER_H_
