#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <typeinfo>
#include <errno.h>
#include <time.h>

#include "Str_Def.h"
#include "StdInput.h"
#include "asm/TA7000_IO.h"
#include "Operation.hpp"
#include "LCDHandler.h"

#include "HttpClient.h"

enum ADDRTYPE
{
	TYPE_IP_ADDR = 0,
	TYPE_GATEWAY_ADDR,
	TYPE_NETMASK,
	TYPE_DNS
};

enum WEPKEYTYPE
{
	TYPE_64_BIT = 0,
	TYPE_128_BIT
};

enum NETWORKTYPE
{
	TYPE_ETHERNET = 0,
	TYPE_WIRELESS
};

enum ADDRDEF
{
	TYPE_STATIC = 0,
	TYPE_DHCP
};

class Config
{
public:
	Config(){};
	virtual ~Config(){};
protected:
	LCDHandler lcdHandler;
	Operation oper;
};

class DateTimeConfig : public Config
{
public:
	DateTimeConfig(){};
	virtual ~DateTimeConfig(){};

	OperationStatusCode ShowDateAndTimeMenuScreen();
    OperationStatusCode ShowConfigureDateScreen();
    OperationStatusCode ShowDaylightSavingsScreen();
private:
	OperationStatusCode ShowConfigureTimeScreen(char * date);
};

class AccessConfig : public Config
{
public:
	AccessConfig(){};
	virtual ~AccessConfig(){};

	OperationStatusCode ShowConfigAccessScreen();

private:
	OperationStatusCode ShowReadCodeScreen();
};

class TCPIPConfig : public Config
{
public:
	TCPIPConfig()
	{
		needsReboot = false;
	};
	virtual ~TCPIPConfig(){};

	OperationStatusCode ShowSelTCPIPCommScreen();

	bool TerminalNeedsReboot() { return needsReboot; };


	const char * read_first_resolv_dns_server();

	const char * GetClockIPAddress(NETWORKTYPE nwtype, ADDRTYPE type);

	/**
	 * Displays the IP Adddress, Gateway and Netmask
	 */
	OperationStatusCode ShowNetworkSettings(NETWORKTYPE nwtype);

	/**
	 * Loads the Wireless properties from the properties.xml
	 * and initializes wireless interface
	 */
	OperationStatusCode InitializeWireless();

	/**
	 * Gest the MAC Address of the specified interface
	 */
	const char * GetMACAddress(NETWORKTYPE nwtype);

	/**
	 * Displays the Network status
	 */
	OperationStatusCode ShowNetworkStatus(NETWORKTYPE nwtype);

	OperationStatusCode ShowNetworkInfoMenu(NETWORKTYPE nwtype);
	OperationStatusCode ShowMACAddress(NETWORKTYPE nwtype);

private:
	void ShowSelTCPIPCommScreen1();
	void ShowSelTCPIPCommScreen2();

	void LoadNetworkStatus(NETWORKTYPE nwtype, char * rxstatus, char * txstatus, char * wirstatus);

	OperationStatusCode ShowCfgWirelessNetworkScreen();

	/**
	 * WIRELESS SSID SCREEN
	 */
	OperationStatusCode ShowCfgWirelessSSIDScreen();

	/**
	 * WIRELESS SECURITY SCREENS
	 */
	OperationStatusCode ShowCfgWirelessSecScreen();
	void ShowCfgWirelessSecScreen1();
	void ShowCfgWirelessSecScreen2();
	OperationStatusCode ShowCfgWEPKeyLenScreen();
	OperationStatusCode ShowCfgWEPKeyDataScreen(WEPKEYTYPE keytype);

	//Utility Functions
	char GetNextHexChar(char ch);
	char GetPrevHexChar(char ch);

	/**
	 * TCP/IP PROPERTIES SCREENS
	 */
	OperationStatusCode ShowCfgTCPIPPropsScreen(NETWORKTYPE nwtype);
	OperationStatusCode ShowCfgTCPIPAddrScreen(NETWORKTYPE nwtype, ADDRTYPE type);

	/**
	 * If fmt is 0 : Converts IP Address from "10.0.1.25" to "010.000.001.025" format
	 * Else : Converts IP Address from "010.000.001.025" to "10.0.1.25" format
	 */
	void ConvertIPAddressFormat(int fmt, const char * addr, char * buffer);

	/**
	 * Converts Gateway Address format (0A000001)
	 * read from /proc/net/route to (10.0.0.1) format
	 */
	void ConvertGatewayAddressFormat(const char * srcaddr, char * buffer);


	/**
	 * This function sets the network properties
	 * (IPADDRESS, NETMASK, GATEWAY ADDRESS) of the system.
	 * This is done by modifying the "interfaces" file.
	 * "ifup" uses this file to set the network properties of the system
	 */
	void SetNetworkProperties(NETWORKTYPE nwtype, ADDRDEF def);

	/**
	 * Utility function to check if the line is one of the options
	 * of static or dhcp
	 */
	bool CheckIfStaticOrDhcpOption(char * line);

	/**
	 * Displays the TCP/PORT Screen
	 */
	OperationStatusCode ShowTCPPortScreen();

	/**
	 * Displays the TID Screen
	 */
	OperationStatusCode ShowTIDScreen();

	/**
	 * The IP ADDRESS
	 */
	char ipaddress[16];

	/**
	 * The NETMASK
	 */
	char netmask[16];

	/**
	 * The GATEWAY Address
	 */
	char gatewayaddress[16];
	/**
	* The DNS Server
	*/
    char dns[16];
	/**
	 * The TCP Port
	 */
	int tcpport;

	bool needsReboot;
};

class UtilConfig : public Config
{
public:
	UtilConfig(){
		pgetservertime_cond_flag = NULL;
		ppopulatetemplates_cond_flag = NULL;
		ppopulatetemplates_mutex = NULL;
		pwaitforpopulatetemplates_mutex = NULL;
	}
	virtual ~UtilConfig(){};

	OperationStatusCode ShowUtilitiesScreen();
	OperationStatusCode ShowPrologicScreen();


	void SetTimeSyncConditionFlag(pthread_cond_t * servertime_cond_flag )
	{
		if (servertime_cond_flag)
		{
			printf("setting servertime_cond_flag for utilities access\n");
			pgetservertime_cond_flag = servertime_cond_flag;
		}
		else
		{
			printf("servertime_cond_flag not available to utilities menu\n");
		}
	}
	void SetPopulateTemplatesConditionFlag(pthread_cond_t * populatetemplates_cond_flag )
	{
		if (populatetemplates_cond_flag)
		{
			printf("setting populatetemplates_cond_flag for utilities access\n");
			ppopulatetemplates_cond_flag = populatetemplates_cond_flag;
		}
		else
		{
			printf("populatetemplates_cond_flag not available to utilities menu\n");
		}
	}
	void SetWaitForPopulateTemplatesThreadConditionFlag(pthread_cond_t * waitforpopulatetemplatesthread_cond_flag )
	{
		if (waitforpopulatetemplatesthread_cond_flag)
		{
			printf("setting waitforpopulatetemplatesthread_cond_flag\n");
			pwaitforpopulatetemplatesthread_cond_flag = waitforpopulatetemplatesthread_cond_flag;
		}
		else
		{
			printf("waitforpopulatetemplatesthread_cond_flag not available\n");
		}
	}
	void SetPopulateTemplatesMutex(pthread_mutex_t * populatetemplates_mutex )
	{
		if (populatetemplates_mutex)
		{
			printf("setting populatetemplates_mutex\n");
			ppopulatetemplates_mutex = populatetemplates_mutex;
		}
		else
		{
			printf("populatetemplates_mutex not available\n");
		}
	}
	void SetWaitForPopulateTemplatesMutex(pthread_mutex_t * waitforpopulatetemplates_mutex )
	{
		if (waitforpopulatetemplates_mutex)
		{
			printf("setting waitforpopulatetemplates_mutex\n");
			pwaitforpopulatetemplates_mutex = waitforpopulatetemplates_mutex;
		}
		else
		{
			printf("waitforpopulatetemplates_mutex not available\n");
		}
	}


private:
	OperationStatusCode ShowTelnetScreen();
	OperationStatusCode EnableTelnet();
	OperationStatusCode DisableTelnet();

#ifdef CLIENT_PROLOGIC
#else
	OperationStatusCode ShowWatchDogScreen();
	OperationStatusCode EnableWatchdog();
	OperationStatusCode DisableWatchdog();

	OperationStatusCode ShowAutoRestartScreen();
	OperationStatusCode EnableAutoRestart();
	OperationStatusCode DisableAutoRestart();
	OperationStatusCode GetDailyTimestamp(char * tstamp);
	OperationStatusCode GetWeeklyTimestamp(char * tstamp);
#endif


	pthread_cond_t  * pgetservertime_cond_flag;
	pthread_cond_t  * ppopulatetemplates_cond_flag;
	pthread_cond_t  * pwaitforpopulatetemplatesthread_cond_flag;
	pthread_mutex_t * ppopulatetemplates_mutex;
	pthread_mutex_t * pwaitforpopulatetemplates_mutex;



	OperationStatusCode ShowSyncTimeScreen();
	OperationStatusCode ShowPopulateTemplatesScreen();

	OperationStatusCode ShowGetActivitiesScreen();


	void ShowDayMenuScreen1();
	void ShowDayMenuScreen2();
	void ShowDayMenuScreen3();
	void ShowDayMenuScreen4();
	void ShowDayMenuScreen5();
};

class ReaderConfig : public Config
{
 public:
 	ReaderConfig(){};
 	virtual ~ReaderConfig(){};
 	OperationStatusCode ShowReadersScreen();

 private:
 	OperationStatusCode ShowFPReaderScreen();
 	OperationStatusCode EnableFPEnroll();
 	OperationStatusCode DisableFPEnroll();
};

#endif //_CONFIG_H_
