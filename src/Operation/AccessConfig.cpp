#include "Config.h"

/******************************************************************
 * *************** Config Access Menu *****************************
 * ***************************************************************/

/**
 * Menu Strings
 */
static const char * CFG_ACCS_MENU_HDNG		= "   CONFIG ACCESS    ";
static const char * CFG_ACCS_MENU_KEYPAD	= "1-KEYPAD           ";


static const char * KB_READ_CODE_HDNG		= "     ENTER CODE     ";
static const char * KB_CONFIRM_CODE_HDNG	= "    CONFIRM CODE    ";


OperationStatusCode AccessConfig::ShowConfigAccessScreen()
{	
	int timeout ;
	int tm = 0;
	char input;
	int cursorloc = 2;
	OperationStatusCode code;
	
start:	timeout = oper.GetDataTimeout();	
	for(;;)
	{
		tm = 0;
	    user_output_clear();
		user_output_write(1, 1, CFG_ACCS_MENU_HDNG);
		user_output_write(2, 1, CURSOR);
		user_output_write(2, 2, CFG_ACCS_MENU_KEYPAD);
		
		while(true)
		{
			if (user_input_ready ()) 
			{
				input = user_input_get ();
              
			    switch(input)
		        {
			        case '1': // Do Not Use
				        oper.Beep(50, TYPE_KeyPress);
				        code = ShowReadCodeScreen();
				        if(code == SC_Clear) goto end;
					    else return code;
				    break;
				
			        case ESC:
				        oper.Beep(50, TYPE_KeyPress);
                        return SC_Clear;
				    break;      		    
			        case '\n': // Do Not Use
				        oper.Beep(50, TYPE_KeyPress);
				        code = ShowReadCodeScreen();
				        if(code == SC_Clear) goto end;
					    else return code;
				    break;
				   case '+': // Do Not Use
				        if(cursorloc > 2) 
				        {
					        oper.Beep(50, TYPE_KeyPress);
					        user_output_write(cursorloc--, 1, BLANKSPACE);
					        user_output_write(cursorloc, 1, CURSOR);
				        }
				        else
				            oper.Beep(50,TYPE_Reject);
				    break;
				   case '-': // Do Not Use
				       if(cursorloc  < 2) 
				       {
					        oper.Beep(50, TYPE_KeyPress);
					        user_output_write(cursorloc++, 1, BLANKSPACE); 
					        user_output_write(cursorloc, 1, CURSOR);
				       }
				       else
				            oper.Beep(50,TYPE_Reject); 
				    break;
				    default:
				       oper.Beep(150,TYPE_Reject);
                       oper.displayErrMessage("   NOT AVAILABLE    ");
                       goto start;
                    break;
                 }   
				
				/*if( input == ESC ) return SC_Clear;
				else if( input == '1' || input == '\n' )
				{
					oper.Beep(50, TYPE_KeyPress);
                    OperationStatusCode code = ShowReadCodeScreen();
					if(code == SC_Clear) break;
					else return code;
				}
                else
                { 
                    oper.Beep(150,TYPE_Reject);
                    oper.displayErrMessage("   NOT AVAILABLE    ");
                    goto start;
                } */  
			}
			else
			{
				usleep(10000);
				tm += 20;
				if(tm >= timeout)
				{
					return SC_TIMEOUT;
				}
			}
			
		}	
	 end:;
	}
	
	return SC_Success;
}

OperationStatusCode AccessConfig::ShowReadCodeScreen()
{
	user_output_clear();
	int len = oper.GetMaxBadgeLen();
	int col = ( 20 - len  ) / 2 + 1;
	int timeout = oper.GetDataTimeout();
	char buffer[ len + 1 ];
	for(;;)
	{
		user_output_write(2, 1, KB_READ_CODE_HDNG);
		buffer[len] = '\0';
		for( int i=0; i<len; i++ ) buffer[i] = ' ';
		LCD_READ_STATE st = lcdHandler.ReadData(SysInputTypeNumeric, 3, col, timeout, len, len, buffer);
		if (st == READ_TIMEDOUT)
		{
			return SC_TIMEOUT;
		}	
		else if(st == READ_CLEAR)
		{
			return SC_Clear;
		}
		if( (int)strlen( buffer ) < len )
		{
			user_output_write(3, 1, "    INVALID CODE    ");
			oper.TurnOnLEDYellow();
			oper.Beep(TYPE_Error);
			usleep( oper.GetErrorMsgTimeout() * 1000 );
			user_output_write(3, 1, BLANK_LINE);
			oper.TurnOffLEDYellow();
		}
		else
		{
			oper.Beep(150, TYPE_Accept);
			char temp[len + 1];
			strcpy(temp, buffer);
			buffer[len] = '\0';
			for( int i=0; i<len; i++ ) buffer[i] = ' ';
			user_output_write(2, 1, KB_CONFIRM_CODE_HDNG);
			st = lcdHandler.ReadData(SysInputTypeNumeric, 3, col, timeout, len, len, buffer);
			if (st == READ_TIMEDOUT)
			{
				return SC_TIMEOUT;
			}
			else if(st == READ_CLEAR)
			{
				return SC_Clear;
			}
			if( strcmp(temp, buffer) == 0 ) break;
			else
			{
				user_output_write(3, 1, " CODES DO NOT MATCH ");
				oper.TurnOnLEDYellow();
				oper.Beep(TYPE_Error);
				usleep( oper.GetErrorMsgTimeout() * 1000 );
				user_output_write(3, 1, BLANK_LINE);
				oper.TurnOffLEDYellow();
			}
		}
	}
	oper.SetAccessCode(buffer);
	usleep(1000000);
	return SC_Success;
}

