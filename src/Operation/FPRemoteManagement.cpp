
#include "FPRemoteManagement.hpp"

OperationStatusCode FPRemoteManagement::AddUsersListToFPU(const char* xmlAddUsersList)
{

	 XMLProperties parser;
	 int from,to,id;
	 from = 1; to = 10;
	 char tempUserID[30];
	 list<ElmData>& addUsersList = parser.getListElementData();
	 int countElm;
	 Base64 base64;
	 OperationStatusCode code = SC_Success;
	 do
	 {
	   parser.GetListElements(xmlAddUsersList,ADD_USERS_LIST,LIST_ELEMENT,from,to);
	   list<ElmData>::iterator i;
	   list<ElmData>::iterator j;
       countElm = 0;
	   j = addUsersList.end();
	   for(i=addUsersList.begin(); i != j;)
	   {
		 char* userTemplates[10];
		 for(int j = 0 ; j < 10 ; j++)userTemplates[j] = NULL;
		 int UserIndex = (*i).elmIndexInList;
         int templateNum = 0;
		 while(UserIndex == (*i).elmIndexInList) // scan all the sub elements belong to main element "elmIndexInList" 
		 {
			if(strcmp("ID",(*i).key) == 0) // the User Id sub element 
				strcpy(tempUserID,(*i).data);
			if(strcmp("template",(*i).key) == 0) // the User template sub element 
			{
                if(templateNum < 10)
				{    
					userTemplates[templateNum] =(char*)malloc( ENCODED_TEMPLATESIZE+1 );
					strcpy( userTemplates[templateNum],(*i).data);
					templateNum++;
				}
			}
			if(i == addUsersList.end()) break ;
			i++;
		 }
		 unsigned long int UserIDNum = strtoul(tempUserID,NULL,0);
		 UF_RET_CODE Result;
         fpOpr.DeleteUser(UserIDNum , &Result);
		 unsigned int s = FP_TEMPLATESIZE ;
		 unsigned char template_after_decode [ FP_TEMPLATESIZE+1 ] ;
		 for(int j = 0 ; j < 10 ; j++)
		 {
			if(userTemplates[j] != NULL)
			{
				base64.decode(userTemplates[j],strlen(userTemplates[j]),template_after_decode,s);
				const char * template_final = reinterpret_cast<const char *>(template_after_decode);
				if((fpOpr.AddUser(UserIDNum,(UF_BYTE*)template_final,s,&Result)) != SC_Success ) code = SC_Fail;
				free(userTemplates[j]);
			}
		 }
		 countElm++;
	   }
	   from = from + countElm;
	   to = from + 9 ;	
	 }
     while(countElm == 10);

	 return code;
}

OperationStatusCode FPRemoteManagement::DeleteUsersListFromFPU(const char* xmlDeleteUsersList)
{
  	 XMLProperties parser;
	 int from,to,id;
	 from = 1; to = 10;
	 char tempUserID[100];
	 char tempUserTemplate[ENCODED_TEMPLATESIZE];
	 list<ElmData>& deleteUsersList = parser.getListElementData();
	 OperationStatusCode code = SC_Success;
	 int countElm;
	 //printf("User = %s\n",parser.GetProperty(xmlAddUsersList,"ID"));
	 do
	 {
	   parser.GetListElements(xmlDeleteUsersList,DELETE_USERS_LIST,LIST_ELEMENT,from,to);
	   list<ElmData>::iterator i;
       countElm = 0;
	   //printf("befor for size = %d from = %d to = %d \n",deletesersList.size(),from,to);
	   for(i=deleteUsersList.begin(); i != deleteUsersList.end();)
	   {
	     
		 char* userTemplates[10];
		 for(int j = 0 ; j < 10 ; j++)userTemplates[j] = NULL;
	     int UserIndex = (*i).elmIndexInList;
         int templateNum = 0;
		 while(UserIndex == (*i).elmIndexInList) // scan all the sub elements belong to main element "elmIndexInList" 
		 {
			if(strcmp("ID",(*i).key) == 0) // the User Id sub element 
				strcpy(tempUserID,(*i).data);
			if(strcmp("template",(*i).key) == 0) // the User template sub element 
			{
                if(templateNum < 10)
				{    
					userTemplates[templateNum] =(char*)malloc( ENCODED_TEMPLATESIZE+1 );
					strcpy( userTemplates[templateNum],(*i).data);
					templateNum++;
				}
			}
			if(i == deleteUsersList.end()) break ;
			i++;
		 }
		 unsigned long int UserIDNum = strtoul(tempUserID,NULL,0);
		 UF_RET_CODE* Result; 
         if((fpOpr.DeleteUser(UserIDNum,Result)) != SC_Success)code = SC_Fail;
         
		 for(int j = 0 ; j < 10 ; j++)
		 {
			if(userTemplates[j] != NULL)
			{
				free(userTemplates[j]);
			}
		 }
		 countElm++;
	   }
	   from = from + countElm;
	   to = from + 9 ;	
	 }
     while(countElm == 10);
   
	 return SC_Success;
}
