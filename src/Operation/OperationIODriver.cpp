#include <unistd.h>
#include <stdlib.h>
#include <typeinfo>
#include <errno.h>
#include <ctype.h>
#include <string.h>
#include <poll.h>


#include <Base64.h>

#include "TAString.h"
#include "Str_Def.h"
#include "OperationDriver.hpp"
#include "OperationUtil.hpp"
#include "StdInput.h"
#include "asm/TA7000_IO.h"
#include "UserInput.hpp"
#include "XMLProperties.h"
#include "Operation.hpp"
#include "LegacyTrans.hpp"
#include "Legacy.hpp"
#include "InterfaceValidateEntryHolder.hpp"
#include "StdInput.h"
#include "KeyboardReader.h"
#include "LCDHandler.h"
#include "logger.h"

#include <execinfo.h>


#include "sqlitedb.h"

#include "stringutil.h"

//#define DEBUG 1
#ifdef DEBUG
#undef DEBUG
#endif

//#include <mysql.h>

extern "C" {
#include "User.h"
}

#define FP_TEMPLATESIZE 384


// scheduling priorities
#define SY_PRIO_LOW		10
#define SY_PRIO_MEDIUM	40
#define SY_PRIO_HIGH	90

extern LegacySetup SystemSetup;
#ifdef client_prologic
extern LegacySetup KeypadSetup;
#endif

extern FPOperation fpOpr;
using namespace std;

pthread_mutex_t OperationIODriver::lockDisplay = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t OperationIODriver::lockAccessCount = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t OperationIODriver::soap_request_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t OperationIODriver::xml_parser_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t OperationIODriver::offline_request_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t OperationIODriver::get_projects_mutex = PTHREAD_MUTEX_INITIALIZER;


pthread_cond_t  OperationIODriver::getservertime_cond_flag;
pthread_cond_t  OperationIODriver::templatesavailable_cond_flag;
pthread_cond_t  OperationIODriver::populatetemplatesthread_cond_flag;
pthread_cond_t  OperationIODriver::waitforpopulatetemplatesthread_cond_flag;
pthread_mutex_t OperationIODriver::populatetemplates_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t OperationIODriver::waitforpopulatetemplates_mutex = PTHREAD_MUTEX_INITIALIZER;

pthread_cond_t  OperationIODriver::GetPinTablethread_cond_flag;
pthread_mutex_t OperationIODriver::waitforGetPinTable_mutex = PTHREAD_MUTEX_INITIALIZER;


#ifdef DEBUGSEGVMT	// --------------------------------------------------------------

pthread_mutex_t OperationIODriver::mt_sig_mutex = PTHREAD_MUTEX_INITIALIZER;

#endif				// --------------------------------------------------------------


bool			OperationIODriver::ptPredicate;
pthread_cond_t  OperationIODriver::waitforstartup_cond_flag;

bool OperationIODriver::AllowPopulateTemplates = true;

bool  OperationIODriver::communication ;

bool OperationIODriver::TurnOnSwipeAndGo = false;

bool OperationIODriver::OfflineModeIsActive = false;

bool OperationIODriver::OfflineModeIsEnabled = false;

int OperationIODriver::accessCount = 0;



extern sqlite3 *db;
bool fpUnitEnabled = false;
bool start_template_read = false;

extern Logger * logger;

const char* g_tmpmsg = 0 ;

namespace {
  int fd_pipe [ 2 ] ;
  const int wr = 1 ;
  const int rd = 0 ;
  const char DOT = '.' ;

  //functor to match a fingerprint against the records in the database


  //***************************************************************************************
  bool template_available ( int fd ){
    pollfd d ;
    d .fd = fd ;
    d .events = POLLIN ;
    d .revents = 0 ;
    int r = poll ( & d , 1 , 1 ); //wait for a milli-second, which is practically nothing
    if ( 0 == r ){
      return false ;
    }
    if ( r < 0 ){
      return false ;
    }
    if ( 0 == ( d .revents & POLLIN )){
      return false ;
    }

    return true ;
  }

  //***************************************************************************************
  void adjust_time ( int seconds ){
    time_t cur = time ( 0 );
    cur += seconds ;
    stime ( & cur );
    //hardware clock setting:
    system ( "hwclock -w" );
  }

  //***************************************************************************************
};

//*****************************************************************************************
// 1
// void handler (int signal_number){printf("SIGPIPE was sended\n");}
// 2

void show_iod_stackframe() {
  void *trace[32];
  char **messages = (char **)NULL;
  int i, trace_size = 0;

  trace_size = backtrace(trace, 32);
  messages = backtrace_symbols(trace, trace_size);
  printf("[bt] Execution path:\n");
  for (i=0; i<trace_size; ++i)
	printf("[bt] %s\n", messages[i]);
}


void handler (int signal_number)
{
  void *array[10];
  size_t size;

	printf("SIGPIPE was sent\n");
  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", signal_number);
  //// 1: backtrace_symbols_fd(array, size, 2);
  //// 2:
  show_iod_stackframe();
  //signal(SIGPIPE, SIG_IGN);

}

//***************************************************************************************

void OperationIODriver::initOfflineMode()
{

	SqliteDB sqlitedb;
	char tmp[64];
	bool btmp = false;

	LOCKXM;

	XMLProperties *oparser = new XMLProperties(XML_PROPERTIES_FILE);

	// does this make a diff?
	//padstring(tmp, NULL, 63);
	//tmp[63] = '\0';
	tmp[0] = '\0';

	oparser->GetProperty("OfflineMode",tmp);

	UNLOCKXM;


	if(strlen(tmp)>0 && strcasecmp(tmp,"true") == 0)
	{

	  btmp = true;
	}
	else if(strlen(tmp)>0 && strcmp(tmp,"1") == 0)
	{

	  btmp = true;
	}
	else
	{

	  btmp = false;
	}

	OfflineModeIsEnabled = btmp;
	tmp[0] = '\0';
	delete oparser;

printf("IOD initOffLineMode(). OfflineModeIsEnabled is %d\n", (OfflineModeIsEnabled == true));

}

OperationIODriver::OperationIODriver()
{
  //char* tmp = NULL;

  transitem = LegacyTransItem::GetInstance();
  getWeb = new XMLProperties(XML_PROPERTIES_FILE);



  function_read = false;


//printf("ctor IOD, sem_init ok?. cond_init offline_cond_flag\n");

  if(pthread_cond_init (&offline_cond_flag, NULL))
      fprintf(stderr,"offline condition variable initialize failed\n");
  if(pthread_cond_init (&heartbeat_cond_flag, NULL))
      fprintf(stderr,"heartbeat condition variable initialize failed\n");
  if(pthread_cond_init (&getprojects_cond_flag, NULL))
      fprintf(stderr,"getprojects condition variable initialize failed\n");
  if(pthread_cond_init (&getservertime_cond_flag, NULL))
      fprintf(stderr,"getservertime condition variable initialize failed\n");
//  if(pthread_cond_init (&populatefptemplates_cond_flag, NULL))
//      fprintf(stderr,"populatefptemplates condition variable initialize failed\n");

  offline_thread_is_started = 1;

  if(pthread_cond_init (&populatetemplatesthread_cond_flag, NULL))
      fprintf(stderr,"populatefptemplates condition variable initialize failed\n");
  if(pthread_cond_init (&waitforpopulatetemplatesthread_cond_flag, NULL))
      fprintf(stderr,"waitforpopulatetemplatesthread condition variable initialize failed\n");

  if(pthread_cond_init (&waitforstartup_cond_flag, NULL))
      fprintf(stderr,"waitforstartup condition variable initialize failed\n");



  reqCount = 0;

  memset (&sa, 0, sizeof (sa));
  sa.sa_handler = &handler;
  sigaction (SIGPIPE, &sa, NULL);
  //SetupProgramClock();

  char contrastLev[3];
  char buzzerVolLev[3];
  int contrastLevInt,buzzerVolLevInt;
  getWeb->GetProperty("ContrastLevel",contrastLev);
  contrastLevInt = atoi(contrastLev);
  getWeb->GetProperty("BuzzerVolume",buzzerVolLev);
  buzzerVolLevInt = atoi(buzzerVolLev);
  user_init();
  user_output_initialize();

  user_output_write(1, 2, "Contrast Adjustment");
  if(contrastLevInt == 15){user_output_contrast(0x1000);Contrast = 0x1000;}
  if(contrastLevInt == 14){user_output_contrast(0x2000);Contrast = 0x2000;}
  if(contrastLevInt == 13){user_output_contrast(0x3000);Contrast = 0x3000;}
  if(contrastLevInt == 12){user_output_contrast(0x4000);Contrast = 0x4000;}
  if(contrastLevInt == 11){user_output_contrast(0x5000);Contrast = 0x5000;}
  if(contrastLevInt == 10){user_output_contrast(0x6000);Contrast = 0x6000;}
  if(contrastLevInt == 9){user_output_contrast(0x7000);Contrast = 0x7000;}
  if(contrastLevInt == 8){user_output_contrast(0x8000);Contrast = 0x8000;}
  if(contrastLevInt == 7){user_output_contrast(0x9000);Contrast = 0x9000;}
  if(contrastLevInt == 6){user_output_contrast(0xA000);Contrast = 0xA000;}
  if(contrastLevInt == 5){user_output_contrast(0xB000);Contrast = 0xB000;}
  if(contrastLevInt == 4){user_output_contrast(0xC000);Contrast = 0xC000;}
  if(contrastLevInt == 3){user_output_contrast(0xD000);Contrast = 0xD000;}
  if(contrastLevInt == 2){user_output_contrast(0xE000);Contrast = 0xE000;}
  if(contrastLevInt == 1){user_output_contrast(0xF000);Contrast = 0xF000;}
  BuzzerVolume = 10 * buzzerVolLevInt;
  user_audible_volume(BuzzerVolume);
  getWeb->GetProperty("UpdateTimeClock",updateTimeClock);

//		XMLProperties *parser2 = new XMLProperties(XML_PROPERTIES_FILE);
//		parser2->GetProperty("PopulateTemplatesTime",populateTemplatesTime);
//		delete parser2;
  getWeb->GetProperty("PopulateTemplatesTime", populateTemplatesTime);

  char allowPT[8];
  allowPT[0] = '\0';
  getWeb->GetProperty("AllowPopulateTemplates", allowPT);
	if (strcmp(allowPT, "false") == 0)            //
	{
	    AllowPopulateTemplates = false;
		DP("setting AllowPopulateTemplates to false\n");
	}
	else                                          // default is true if element does not exist
	{
	    AllowPopulateTemplates = true;
		DP("setting AllowPopulateTemplates to true\n");

	}
}

//***************************************************************************************
//void OperationIODriver::SetupProgramClock()
//{
//     char badgeLen[10];
//     char sources[5];
//	 badgeLen[0] = '\0';
//	 sources[0]='N';sources[1]='N';sources[2]='N';sources[3]='N';sources[4]='\0';
//	 SysInputSource so = SysInputSourceInvalid;
//	 getWeb->GetProperty("BadgeLength",badgeLen);
//     getWeb->GetProperty("BadgeSources",sources);
//	 if(strlen(badgeLen) > 0)
//	 {
//		SystemSetup.badge.length.maximum = atoi(badgeLen);
//		SystemSetup.badge.length.minimum = atoi(badgeLen);
//		SystemSetup.badge.length.valid = atoi(badgeLen);
//	 }
//	 else
//	 {
//	    SystemSetup.badge.length.maximum = 10;
//		SystemSetup.badge.length.minimum = 10;
//		SystemSetup.badge.length.valid = 10;
//	 }
//	 if(sources[0] == 'K')
//	    so = so | SysInputSourceKeypad;
//	 if(sources[1] == 'S')
//        so = so | SysInputSourceMagnetic;
//	 if(sources[2] == 'W')
//        so = so | SysInputSourceBarcode;
//	 if(sources[3] == 'P'){}
//	 SystemSetup.source.initial = so;
//	 SystemSetup.source.function = SysInputSourceDefault;
//	 SystemSetup.source.supervisor = so;
//	 SystemSetup.source.super_employee = so;
//	 SystemSetup.source.configuration = so;

//	 SetupFunctionKeys();



//}
//***************************************************************************************
//void OperationIODriver::SetupFunctionKeys()
//{

//  char FunctionKey[4];
//  int  FunctionKeyNum=0;
//  //char FunctionKeyChar;
//  ResetFunctionKeys();

//  for(int i = 0 ; i < 4 ; i++)
//  {
//	if(i == 0) getWeb->GetProperty("ClockIn",FunctionKey);
//	if(i == 1) getWeb->GetProperty("InPunch",FunctionKey);
//    if(i == 2) getWeb->GetProperty("OutPunch",FunctionKey);
//	if(i == 3) getWeb->GetProperty("SwipePunch",FunctionKey);

//	if(strcmp(FunctionKey,"F1")==0){FunctionKeyNum = 1; SwipeAndGoKey = 'A';}
//	if(strcmp(FunctionKey,"F2")==0){FunctionKeyNum = 2;SwipeAndGoKey = 'B';}
//	if(strcmp(FunctionKey,"F3")==0){FunctionKeyNum = 3;SwipeAndGoKey = 'C';}
//	if(strcmp(FunctionKey,"F4")==0){FunctionKeyNum = 4;SwipeAndGoKey = 'D';}
//	if(strcmp(FunctionKey,"F5")==0){FunctionKeyNum = 5;SwipeAndGoKey = 'E';}
//	if(strcmp(FunctionKey,"F6")==0){FunctionKeyNum = 6;SwipeAndGoKey = 'F';}
//	if(strcmp(FunctionKey,"F7")==0){FunctionKeyNum = 7;SwipeAndGoKey = 'G';}
//	if(strcmp(FunctionKey,"F8")==0){FunctionKeyNum = 8;SwipeAndGoKey = 'H';}
//	if(strcmp(FunctionKey,"F9")==0){FunctionKeyNum = 9;SwipeAndGoKey = 'I';}
//	if(strcmp(FunctionKey,"F10")==0){FunctionKeyNum = 10;SwipeAndGoKey = 'J';}
//	if(strcmp(FunctionKey,"F11")==0){FunctionKeyNum = 0;SwipeAndGoKey = 'K';}
//	if(strcmp(FunctionKey,"F12")==0){FunctionKeyNum = 11;SwipeAndGoKey = 'L';}

//    if(i == 0 )
//	{
//		SetClockInFunctionKey(FunctionKeyNum);
//	}
//	if(i == 1 )
//	{
//		SetInPunchFunctionKey(FunctionKeyNum);
//	}
//	if(i == 2 )
//	{
//        SetOutPunchFunctionKey(FunctionKeyNum);
//	}
//    if(i == 3 )
//	{
//	}
//  }

//}
//***************************************************************************************
//void OperationIODriver::ResetFunctionKeys()
//{
//   for (int j=0; j<SysFunctionKeyMax; j++)
//  {
//	  SystemSetup.function.key[j].flags = 0;
//	  SystemSetup.function.key[j].levels = 0;
//	  strcpy(SystemSetup.function.key[j].Function,"");

//   }
//  for(int i = 0 ; i<SysFunctionDetailMax;i++)
//  {
//      SystemSetup.function.detail[i].key = '0';
//	  SystemSetup.function.detail[i].level = 1;
//	  strcpy(SystemSetup.function.detail[i].message,"");
//	  SystemSetup.function.detail[i].source = 15;
//	  SystemSetup.function.detail[i].type = 0;
//	  SystemSetup.function.detail[i].decimal = 0;
//	  SystemSetup.function.detail[i].length.max = 1;
//	  SystemSetup.function.detail[i].length.min = 0;
//	  SystemSetup.function.detail[i].validation.flags = 0;
//	  SystemSetup.function.detail[i].validation.table = 0;
//  }
//}
//***************************************************************************************
//void OperationIODriver::SetClockInFunctionKey(int FunctionKey)
//{
//  int detailIndx = FunctionKey * SysFunctionKeyLevelsMax;

//  getWeb->GetProperty("ClockInLev1",ClockInLevel[0]);
//  getWeb->GetProperty("ClockInLev2",ClockInLevel[1]);
//  getWeb->GetProperty("ClockInLev3",ClockInLevel[2]);
//  getWeb->GetProperty("ClockInLev4",ClockInLevel[3]);

//  DataLevelStage[0] = -1 ;DataLevelStage[1] = -1 ;DataLevelStage[2] = -1 ;DataLevelStage[3] = -1 ;
//  for(int k = 0 ; k < 4 ; k++)
//   for(int L = 0 ; L < 4 ; L++)
//   {
//    if(strcmp(ClockInLevel[L],"NONE") == 0){strcpy(ClockInLevelMassage[L],"");ClockInLevelDataMax[L] = 1;}
//    if(strcmp(ClockInLevel[L],"PS") == 0){strcpy(ClockInLevelMassage[L],"PROJ SWITCH(Y/N)");ClockInLevelDataMax[L] = 1;DataLevelStage[0] = L;}
//	if(strcmp(ClockInLevel[L],"PC") == 0){strcpy(ClockInLevelMassage[L],"PROJECT CODE");ClockInLevelDataMax[L] = 4;DataLevelStage[1] = L;}
//    if(strcmp(ClockInLevel[L],"TC") == 0){strcpy(ClockInLevelMassage[L],"TASK CODE");ClockInLevelDataMax[L] = 4;DataLevelStage[2] = L;}
//	if(strcmp(ClockInLevel[L],"AC") == 0){strcpy(ClockInLevelMassage[L],"ACTIVITY CODE");ClockInLevelDataMax[L] = 4;DataLevelStage[3] = L;}
//   }



//  int i;
//  for(i = 0 ; i < 4  ; i++)
//  {
//	  if(strcmp(ClockInLevel[i],"NONE") == 0){i = i-1; break;}
//  }
//  if(i == 4)i=3;
//  //printf("level = %d %s %s %s %s\n",i+1,ClockInLevel[0],ClockInLevel[1],ClockInLevel[2],ClockInLevel[3]);
//   //printf("maxdata %d %d %d %d\n",ClockInLevelDataMax[0],ClockInLevelDataMax[1],ClockInLevelDataMax[2],ClockInLevelDataMax[3]);
//   //printf("data %s %s %s %s\n",ClockInLevelMassage[0],ClockInLevelMassage[1],ClockInLevelMassage[2],ClockInLevelMassage[3]);
//  SystemSetup.function.key[FunctionKey].levels = i+1;
//  strcpy(SystemSetup.function.key[FunctionKey].Function,"ClockIn");

//  SystemSetup.function.detail[detailIndx].key = 48+FunctionKey;
//  if(FunctionKey == 10)
//         SystemSetup.function.detail[detailIndx].key = '*';
//  if(FunctionKey == 11)
//		 SystemSetup.function.detail[detailIndx].key = '#';
//  SystemSetup.function.detail[detailIndx].level = 1;
//  strcpy(SystemSetup.function.detail[detailIndx].message,ClockInLevelMassage[0]);
//  SystemSetup.function.detail[detailIndx].source = 2;
//  SystemSetup.function.detail[detailIndx].type = 51;
//  SystemSetup.function.detail[detailIndx].decimal = 0;
//  SystemSetup.function.detail[detailIndx].length.max = ClockInLevelDataMax[0];
//  SystemSetup.function.detail[detailIndx].length.min = 0;
//  SystemSetup.function.detail[detailIndx].validation.flags = 0;
//  SystemSetup.function.detail[detailIndx].validation.table = 0;

//  SystemSetup.function.detail[detailIndx+1].key = 48+FunctionKey;
//  if(FunctionKey == 10)
//         SystemSetup.function.detail[detailIndx+1].key = '*';
//  if(FunctionKey == 11)
//		 SystemSetup.function.detail[detailIndx+1].key = '#';
//  SystemSetup.function.detail[detailIndx+1].level = 2;
//  strcpy(SystemSetup.function.detail[detailIndx+1].message,ClockInLevelMassage[1]);
//  SystemSetup.function.detail[detailIndx+1].source = 2;
//  SystemSetup.function.detail[detailIndx+1].type = 51;
//   SystemSetup.function.detail[detailIndx+1].decimal = 0;
//  SystemSetup.function.detail[detailIndx+1].length.max = ClockInLevelDataMax[1];
//  SystemSetup.function.detail[detailIndx+1].length.min = 0;
//  SystemSetup.function.detail[detailIndx+1].validation.flags = 0;
//  SystemSetup.function.detail[detailIndx+1].validation.table = 0;

//  SystemSetup.function.detail[detailIndx+2].key = 48+FunctionKey;
//  if(FunctionKey == 10)
//         SystemSetup.function.detail[detailIndx+2].key = '*';
//  if(FunctionKey == 11)
//		 SystemSetup.function.detail[detailIndx+2].key = '#';
//  SystemSetup.function.detail[detailIndx+2].level = 3;
//  strcpy(SystemSetup.function.detail[detailIndx+2].message,ClockInLevelMassage[2]);
//  SystemSetup.function.detail[detailIndx+2].source = 2;
//  SystemSetup.function.detail[detailIndx+2].type = 51;
//   SystemSetup.function.detail[detailIndx+2].decimal = 0;
//  SystemSetup.function.detail[detailIndx+2].length.max = ClockInLevelDataMax[2];
//  SystemSetup.function.detail[detailIndx+2].length.min = 0;
//  SystemSetup.function.detail[detailIndx+2].validation.flags = 0;
//  SystemSetup.function.detail[detailIndx+2].validation.table = 0;

//  SystemSetup.function.detail[detailIndx+3].key = 48+FunctionKey;
//  if(FunctionKey == 10)
//         SystemSetup.function.detail[detailIndx+3].key = '*';
//  if(FunctionKey == 11)
//		 SystemSetup.function.detail[detailIndx+3].key = '#';
//  SystemSetup.function.detail[detailIndx+3].level = 4;
//  strcpy(SystemSetup.function.detail[detailIndx+3].message,ClockInLevelMassage[3]);
//  SystemSetup.function.detail[detailIndx+3].source = 2;
//  SystemSetup.function.detail[detailIndx+3].type =51;
//   SystemSetup.function.detail[detailIndx+3].decimal = 0;
//  SystemSetup.function.detail[detailIndx+3].length.max = ClockInLevelDataMax[3];
//  SystemSetup.function.detail[detailIndx+3].length.min = 0;
//  SystemSetup.function.detail[detailIndx+3].validation.flags = 0;
//  SystemSetup.function.detail[detailIndx+3].validation.table = 0;

//}
//***************************************************************************************
//void OperationIODriver::SetInPunchFunctionKey(int FunctionKey)
//{
//  int detailIndx = FunctionKey * SysFunctionKeyLevelsMax;

//  SystemSetup.function.key[FunctionKey].levels = 1;
//  strcpy(SystemSetup.function.key[FunctionKey].Function,"InPunch");
//  SystemSetup.function.detail[detailIndx].key = 48+FunctionKey;
//  if(FunctionKey == 10)
//         SystemSetup.function.detail[detailIndx].key = '*';
//  if(FunctionKey == 11)
//		 SystemSetup.function.detail[detailIndx].key = '#';
//  SystemSetup.function.detail[detailIndx].level = 1;
//  strcpy(SystemSetup.function.detail[detailIndx].message,"CLOCKED IN");
//  SystemSetup.function.detail[detailIndx].source = 0;
//  SystemSetup.function.detail[detailIndx].type = 50;

//}
//***************************************************************************************
//void OperationIODriver::SetOutPunchFunctionKey(int FunctionKey)
//{
//  int detailIndx = FunctionKey * SysFunctionKeyLevelsMax;

//  SystemSetup.function.key[FunctionKey].levels = 1;
//  strcpy(SystemSetup.function.key[FunctionKey].Function,"OutPunch");
//  SystemSetup.function.detail[detailIndx].key = 48+FunctionKey;
//  if(FunctionKey == 10)
//         SystemSetup.function.detail[detailIndx].key = '*';
//  if(FunctionKey == 11)
//		 SystemSetup.function.detail[detailIndx].key = '#';
//  SystemSetup.function.detail[detailIndx].level = 1;
//  strcpy(SystemSetup.function.detail[detailIndx].message,"CLOCKED OUT");
//  SystemSetup.function.detail[detailIndx].source = 0;
//  SystemSetup.function.detail[detailIndx].type = 50;
//}
//***************************************************************************************
OperationIODriver::~OperationIODriver()
{
  user_close();
  user_audible_close();
  user_visual_close();
  user_digital_close();
  delete getWeb;
  pthread_cond_destroy(&offline_cond_flag);
  pthread_cond_destroy(&heartbeat_cond_flag);
  pthread_cond_destroy(&getprojects_cond_flag);
  pthread_cond_destroy(&getservertime_cond_flag);
  //pthread_cond_destroy(&populatefptemplates_cond_flag);


  pthread_cond_destroy(&populatetemplatesthread_cond_flag);
  pthread_cond_destroy(&waitforpopulatetemplatesthread_cond_flag);

  pthread_cond_destroy(&waitforstartup_cond_flag);

  sem_destroy(&offline_tran_count);
}
//***************************************************************************************
Code OperationIODriver::RunIdleDisplayProcess()
{

  	pthread_t idleDisplayThread;
	pthread_attr_t idleDisplayAttr;

	pthread_attr_init(&idleDisplayAttr);
	//pthread_attr_setstacksize ( & idleDisplayAttr , const_thread_stack_size ());
	pthread_attr_setstacksize ( & idleDisplayAttr , PTHREAD_STACK_MIN);

	{
		int tpolicy;
		struct sched_param param;

		tpolicy = SCHED_FIFO;
		param.sched_priority = SY_PRIO_LOW;

		pthread_attr_setinheritsched(&idleDisplayAttr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&idleDisplayAttr, tpolicy);
		pthread_attr_setschedparam(&idleDisplayAttr, &param);

	}

	int ret = 0;

	ret = pthread_create(&idleDisplayThread, &idleDisplayAttr, ShowIdleScreen, NULL);
	if ( ret != 0 )
	{
		#ifdef DEBUG
		if( ret == EAGAIN ) printf("Idle Display Thread : Out of Memory\n");
		else if ( ret == EINVAL ) printf( "Idle Display Thread : Invalid Attribute\n");
		printf("Idle Display Thread : Failed to Create Idle Display Thread");
		#endif
    }
	return ret;
}  //End Process()

/**
* Read and Validate the Source
* Used only for simulation
*/
//void OperationIODriver::ReadSource(SourceType type)
//{
//	InitialSource:
//	printf("Input Source (S-Magnetic K-Keypad W-Barcode): ");
//	scanf("%c", &sourcechar);
//	if (sourcechar == ESC) exit(0);
//	Code code = GetSysInputSourceForChar(sourcechar, source);
//	if( code != SC_OKAY)
//	{
//		printf("%s\n", oper->GetPromptMessage(TYPE_InvalidSource));
//		goto InitialSource;
//	}
//	if(type != TYPE_Unknown)
//	{
//		code = oper->ValidateSource(source, type);
//		if( code != SC_OKAY)
//		{
//			printf("%s\n", oper->GetPromptMessage(TYPE_InvalidSource));
//			goto InitialSource;
//		}
//	}
//}

/**
 * Read and Validate Badge
 */
OperationStatusCode OperationIODriver::ReadBadge( )
{

//printf("enter readbadge\n");

BadgeRead:
	// Need to Change this when we add more sources
	//source = SysInputSourceKeypad;
	bool KeypadActive = false;
	#ifdef __TA7700_TERMINAL__
		static char badge[20];
		char * bdg = badge;
		bool displayCleared = false;
		bool reviewMode = false;

		int maxbadgelen = oper->GetMaxBadgeLen();
		int numOfChars = 0;
		int arrowKeyCounter = 0;


		static char disp1[2];
		disp1[1] = '\0';

		int col = ( 20 - maxbadgelen ) / 2 + 1;
		//UserInput ::TimeOut ( oper ->GetDataTimeout () * 1000 );
		//we need to set it to 10 sec - same as finger read timeout
		UserInput ::TimeOut(10000000);

	 	//Flush all the pipes before read

		FPIDMode fpIdMode = oper->GetFPIDMode();

		UserInput ::Flush();

//		if(false)
//	    {

//ReviewMode:
//          UserInput ::TimeOut(10000000);
//          fpIdMode = oper->GetFPIDMode();
//          UserInput ::Flush();
//          UserInput ::ClearBuffer();
//          numOfChars = 0;
//		  arrowKeyCounter = 0;
//		  badge[0]='\0';
//		  bdg = badge;
//		  displayCleared = false;
//		  maxbadgelen = oper->GetMaxBadgeLen();
//          if(!reviewMode)
//            pthread_mutex_lock( &lockDisplay );
//		  user_output_clear();
//		  user_output_write(1, 5,"REVIEW MODE");
//		  user_output_write(2, 1, " ENTER BADGE NUMBER ");
//		  reviewMode = true;
//		  user_output_write(3,col-1,BLANKSPACE);
//		  user_output_cursor(USER_CURSOR_BLINK);
//		}

		while(true)
		{

			char keypress = 0 ;
			//printf("ReadBadge0\n");
			UserInput ::source_t source_temp = UserInput ::OTHER;
			UserInput ::status_t r = UserInput ::GetChar ( keypress, source_temp);
			//printf("ReadBadge1 %s keypress = %c\n",UserInput ::getBuffer(),keypress);
			//printf("ReadBadge1 %s keypress = %d\n",UserInput ::getBuffer(),keypress);
			//Initialise the source
			switch (source_temp)
			{
				case UserInput ::KEYBOARD :
						source = SysInputSourceKeypad;
						KeypadActive = true;
						//printf("key = %c\n",keypress);
						#ifdef DEBUG
							printf("\n source is keypad");
						#endif
						break;
				case UserInput ::PROX :
						source = SysInputSourceProximity;
						#ifdef DEBUG
							printf("\n source is proxy");
						#endif
						KeypadActive = false;
						break;
				case UserInput ::MAG :
						//printf("ReadMAG1\n");
					    source = SysInputSourceMagnetic;
						//pthread_mutex_lock( &lockDisplay );
						//strcpy(buffer,UserInput ::getBuffer());
                        //UserInput ::ClearBuffer();
						KeypadActive = false;
						//return SC_Success;
						#ifdef DEBUG
							printf("\n source is Mag");
						#endif
					    //goto ENDLOOP;
						break;
				case UserInput ::BARCODE :
						source = SysInputSourceBarcode;
						KeypadActive = false;
						#ifdef DEBUG
							printf("\n source is barcode");
						#endif
						break;
				case UserInput ::OTHER :
						source = SysInputSourceDefault;
						KeypadActive = false;
						#ifdef DEBUG
							printf("\n source is default");
						#endif
						break;
				case UserInput ::FPRINT :
						source = SysInputSourceFprint;
						KeypadActive = false;
						#ifdef DEBUG
							printf("\n source is finger print");
						#endif
						break;
			}

			FPIDMode fpIdMode1 = oper->GetFPIDMode();
			if(fpIdMode1 != fpIdMode)
			{
				//mode has been changed. Go to start
				printf("return TimeOut1\n");
				return SC_TIMEOUT;
			}

		//	if(fpIdMode1 == IDENTIFY_MODE)
		//	{
				//In Identify mode, we are in ReadBadge - means database is empty.
				//Check if the templates were sent in the mean time,
				//If yes, do not accept key punch - as we are in identify mode
		//		FptemplateTable fptemplatetbl;
		//		if( fptemplatetbl.RecordsExist() )
		//		{
		//			return SC_TIMEOUT;
		//		}
		//	}

			if ( r == UserInput ::OK )
			{

//#ifdef client_proglogic

				// return for config menu state. ///////////////////////
				if( keypress == 'C')		// 'F3' to prompt for activty code
				{

					if(numOfChars == 0)
					{
						oper->Beep(TYPE_KeyPress);
						buffer[numOfChars]= keypress ; buffer[numOfChars+1]= '\0';
						EnableSwipAndGo = true;
						return SC_ActivityCodePrompt;
                    }
				    else
				    {
						oper->Beep(150, TYPE_Reject);
						continue;
					}
				}

				// return for config menu state. ///////////////////////
				if( keypress == 'L')		// 'F12' to prompt for activty code
				{

					if(numOfChars == 0)
					{
						oper->Beep(TYPE_KeyPress);
						buffer[numOfChars]= keypress ; buffer[numOfChars+1]= '\0';
						EnableSwipAndGo = true;
						return SC_ShowAppVer;
                    }
				    else
				    {
						oper->Beep(150, TYPE_Reject);
						continue;
					}
				}

//#endif

				if( keypress == '-' )
				{
					oper->Beep(150, TYPE_Reject);
					continue;
				}

				if(keypress == '+' )
				{

					if(fpUnitEnabled)
					{

					}
					else
					{
						oper->Beep(150, TYPE_Reject);
						continue;
					}
				}

				if( keypress == '>')
				{
					if(numOfChars == 0)
					{
						oper->Beep(150, TYPE_Reject);
						continue;
					}


				#ifdef DEBUG
					printf("\n\n > pressed");
					printf("\nnumOfChars = %d", numOfChars);
					printf("\arrowKeyCounter = %d", arrowKeyCounter);
					printf("\n maxbadgelen = %d", maxbadgelen );
				#endif
					if(arrowKeyCounter < numOfChars)
					{
						oper->Beep(TYPE_KeyPress);
						disp1[0] = badge[arrowKeyCounter];
						int col1 = col + arrowKeyCounter;
					#ifdef DEBUG
						printf("\nFor > - Writing buffer %c at col %d", disp1[0], col1);
					#endif
						user_output_write(3, col1, disp1);

						*(bdg) ++;
						arrowKeyCounter ++;

						if(arrowKeyCounter == (numOfChars - 1))
						{
						#ifdef DEBUG
							printf("\n Reaching end of length");
							printf("\n Badge length is - %d", strlen(badge));

							printf("\n Badge contains - ");
							for(int i=0; i < strlen(badge) ; i++)
							printf("%c ", badge[i]);
						#endif

							if(numOfChars == maxbadgelen)
							{
								*(bdg) ++;
								arrowKeyCounter ++;
							}
							else
								continue;
						}
					}
					else
					{
						//for the last digit
						oper->Beep(150, TYPE_Reject);
					}
					continue;

				}
				if(keypress == '<')
				{
				#ifdef DEBUG
					printf("\n\n< key pressed");
					printf("\n numOfChars is %d", numOfChars);
				#endif

					if(arrowKeyCounter > 0)
					{
						oper->Beep(TYPE_KeyPress);
						if(arrowKeyCounter == 1)
						{
						#ifdef DEBUG
							printf("\n arrowKeyCounter is 1");
							printf("\nBadge contains - ");
							for(int i=0; badge[i] != '\0' ; i++)
							printf("%c ", badge[i]);
							printf("\nWriting blank space at row 3, col %d", col - 1);
						#endif

							user_output_write(3, col - 1, BLANKSPACE);
						}
						else if(arrowKeyCounter == maxbadgelen)
						{
						#ifdef DEBUG
							printf("\nBadge contains - ");
							for(int i=0; badge[i] != '\0' ; i++)
								printf("%c ", badge[i]);
							printf("\n arrowKeyCounter == maxbadgelen");
						#endif

							disp1[0] = badge[arrowKeyCounter - 3];
							int col1 = arrowKeyCounter + col - 3;
						#ifdef DEBUG
							printf("\nWriting %c at row 3, col %d", disp1[0], col1);
						#endif
							user_output_write(3, col1, disp1);

							*(bdg) --;
							arrowKeyCounter --;
						}
						else
						{
						#ifdef DEBUG
							printf("\nBadge contains - ");
							for(int i=0; badge[i] != '\0' ; i++)
							printf("%c ", badge[i]);

							printf("\n numOfChars is %d, greater than 1 ", numOfChars);
							printf("\n arrowKeyCounter is %d, greater than 1 ", arrowKeyCounter);
						#endif
							disp1[0] = badge[arrowKeyCounter - 2];
							int col1 = arrowKeyCounter + col - 2;
						#ifdef DEBUG
							printf("\nWriting %c at row 3, col %d", disp1[0], col1);
						#endif

							user_output_write(3, col1, disp1);
						}

						*(bdg) --;
						arrowKeyCounter --;
						//printf("\n\n Continue !");
						continue;

					}
					else
					{
//						if(numOfChars == 0)
//						{
//						  oper->Beep(TYPE_KeyPress);
//						  goto ReviewMode;
//                        }
//						else
//						{
						    oper->Beep(150, TYPE_Reject);
						    continue;
//						}
					}
				}
				//esc with an empty buffer: return SC_clear
				//esc with a non-empty buffer: clear the buffer
				if( keypress == ESC )
				{
					oper->Beep(TYPE_KeyPress);
					/*if(reviewMode)
					{
					    user_output_cursor(USER_CURSOR_OFF);
						transitem->ReleaseProgrammingLock();
						return SC_Clear;
					}*/
					#ifdef DEBUG
						printf("\n ESC Key pressed");
						printf("\nBadge contains - ");
							for(int i=0; badge[i] != '\0' ; i++)
						printf("%c ", badge[i]);
					#endif

					if(strlen(badge) == 0)
					{
						user_output_cursor(USER_CURSOR_OFF);
						return SC_Clear;
					}
					else
					{
						numOfChars = 0;
						arrowKeyCounter = 0;

						bdg = badge;
						*(bdg) = '\0';
						user_output_write (3, 1, BLANK_LINE);
						user_output_write(3, col - 1, BLANKSPACE);
						continue;
					}
				}
				//20051229 wjm
				//'.' as input character: reset buffer to 0
				if ( DOT == keypress ){
					numOfChars = 0;
					arrowKeyCounter = 0;
					bdg = badge;
					*(bdg) = '\0';
					user_output_write (3, 1, BLANK_LINE);
					user_output_write(3, col - 1, BLANKSPACE);
					continue;
				}

				//enter with no data in buffer: no op
				//enter with data in buffer: leave loop
				if(keypress == KEY_ENTER)
				{

					oper->Beep(TYPE_KeyPress);
					while(arrowKeyCounter < numOfChars)
					{
						*(bdg) ++;
						arrowKeyCounter ++;
					}
					*(bdg) = '\0';

				#ifdef DEBUG
					printf("\nEnter key pressed - Badge contains - ");
					for(int i=0; badge[i] != '\0' ; i++)
						printf("%c ", badge[i]);
					printf("\n");
				#endif
					if(strlen(badge) == 0) continue;

//					printf("readbadge: IOD::turnonswipeandgo is %d\n", OperationIODriver::TurnOnSwipeAndGo);
//					printf("readbadge: turnonswipandgo is %d\n", TurnOnSwipAndGo);
//					printf("readbadge: enableswipandgo is %d\n", EnableSwipAndGo);
					if ( TurnOnSwipeAndGo && EnableSwipAndGo)
					{
						//printf("readbadge: turnonswipengo is true, enableswipandgo is true\n");
						keypress = SwipeAndGoKey;
						// dont break, fall thru to function key check
					}
					else
					break;
				}

				if( keypress >= 'A' && keypress <= 'L' )
				{
					//function key pressed
					while(arrowKeyCounter < numOfChars)
					{
						*(bdg) ++;
						arrowKeyCounter ++;
					}
					*(bdg) = '\0';

				#ifdef DEBUG
					printf("\nFunction key pressed - Badge contains - ");
					for(int i=0; badge[i] != '\0' ; i++)
						printf("%c ", badge[i]);
				#endif
					if(strlen(badge) == 0)
					{
					    //printf("press = %c  SwipeAndGoKey = %c\n",keypress,SwipeAndGoKey);
						if(keypress == SwipeAndGoKey)
						  EnableSwipAndGo = !EnableSwipAndGo;
						oper->Beep(150, TYPE_Reject);
						continue;
					}
					Function = keypress;
					function_read = true;

					break;
				}

				//displayCleared initially set to false.
				//after the first run through here, displayCleared is true
				if(!displayCleared)
				{
					if(!reviewMode)
					    pthread_mutex_lock( &lockDisplay );
					/**
					* Validate the Initial Sorce
					*/
					Code code = oper->ValidateSource(source, TYPE_EmployeeInputSource);

					//if sorce is invalid, wait for a duration of timeout
					if( code != SC_OKAY)
					{

						user_output_clear();
						char ivs[21];
						int lenIvs = oper->GetPromptMessage(TYPE_InvalidSource,ivs,21);
						if( lenIvs != 0 )
						{
							//printf("333333\n");
							int col = ( 20 - lenIvs ) / 2 + 1;
							user_output_write(2, col, ivs);
						}
						else
						{
							user_output_write(2, 1, "   INVALID SOURCE   ");
						}

						oper->TurnOnLEDYellow();
						oper->Beep(TYPE_Error);

						usleep(oper->GetErrorMsgTimeout() * 1000 );
						oper->TurnOffLEDYellow();
						if(!reviewMode)
						  pthread_mutex_unlock( &lockDisplay );

						UserInput ::ClearBuffer();
						if(source == SysInputSourceFprint)pthread_cond_signal(&FPOperation::fprint_cond_flag);
						//if(reviewMode) goto ReviewMode;
						goto BadgeRead;
					}

					//AcquireProgrammingLock();
//					if(reviewMode)
//					{
//					   user_output_clear();
//		               user_output_write(1, 5,"REVIEW MODE");
//		               user_output_write(2, 1, " ENTER BADGE NUMBER ");
//		               user_output_write(3,col-1,BLANKSPACE);
//		               user_output_cursor(USER_CURSOR_BLINK);
//		            }

				    if(KeypadActive && !reviewMode){
					    user_output_clear();
					    user_output_write(2, 1, " ENTER BADGE NUMBER ");
					    user_output_cursor(USER_CURSOR_BLINK);
				    }
				    displayCleared = true;
			    }

				static char disp[2];
				if(source_temp == UserInput::KEYBOARD)
				{
					oper->Beep(TYPE_KeyPress);
				}

				//avoid overflow of buffer

				if(source == SysInputSourceFprint)
				{
					if(fpOpr.fpOperation == Identify_Operation)
					{
						if( (int)fpOpr.ScanResult == (int)SC_Success || fpOpr.ScanResult == 0 )
						{
							int UserIdLen = strlen(UserInput::getBuffer());
                            int leftZeros = SystemSetup.badge.length.maximum - UserIdLen;
							if(leftZeros > 0)
							{
								for(int i = 0 ; i < leftZeros ; i++)
								{
									if(i == 0)
									   strcpy(buffer,"0");
									else
                                       strcat(buffer,"0");
								}
								strcat(buffer, UserInput::getBuffer());
							}
							else
							{
							  strcpy(buffer, UserInput::getBuffer());
							}
							oper->Beep(TYPE_KeyPress);
							UserInput ::Flush();
							UserInput ::ClearBuffer();
							return  SC_Success;
						}
						else
						{
							UserInput ::Flush();
							UserInput ::ClearBuffer();
                            strcpy(buffer, "");
							return  SC_Fail;
						}
					}
					else
					{
					   UserInput ::Flush();
					   UserInput ::ClearBuffer();
					   strcpy(buffer, "");
					   goto BadgeRead;
					}
				}

				if( arrowKeyCounter == maxbadgelen || source == SysInputSourceFprint )
				{
					if(KeypadActive)
					{
					    badge[arrowKeyCounter-1]= keypress;
						disp[0]=keypress;
						disp[1]='\0';
						//printf("badge = %s\n", badge);
						user_output_write (3, col + arrowKeyCounter - 1,disp);
						disp[0]=badge[arrowKeyCounter-2];
						disp[1]='\0';
						user_output_write (3, col + arrowKeyCounter - 2,disp);
						continue;
					}
					else
					{
						//Invalid badge was swiped with more chars than max size. Clock should reject it.
						//Before this snippet was added, users could even swipe their credit cards and the clock accepted it.
						//The above condition is an exception for config badge (badge with all zeros)
						bool config_bdg = false;
						for( int bl=0; bl < maxbadgelen; bl++)
						{
							if(  badge[bl] != '0' ) break;
							if( bl == maxbadgelen - 1 ) {config_bdg = true; break;}
						}

						if(! config_bdg)
						{
							user_output_clear();
							char ibmsg[21];
							int lenIbmsg = oper->GetPromptMessage(TYPE_InvalidBadge,ibmsg,21);
			                if( lenIbmsg != 0 )
			                {
				                int col = ( 20 - lenIbmsg ) / 2 + 1;
				                user_output_write(2, col, ibmsg);
			                }
			                else
			                {
				                user_output_write(2, 1, "   INVALID BADGE    ");
			                }
							oper->TurnOnLEDYellow();
							oper->Beep(TYPE_Error);
							usleep(oper->GetErrorMsgTimeout() * 1000);
							oper->TurnOffLEDYellow();

							numOfChars = 0;
							arrowKeyCounter = 0;

							//Flush the buffer
							UserInput ::ClearBuffer();

							bdg = badge;
							*(bdg) = '\0';
							user_output_cursor(USER_CURSOR_OFF);
//							if(reviewMode)
//							{
//							    transitem->ReleaseProgrammingLock();
//							    goto ReviewMode;
//							}
							return SC_Clear;
						}
						else
							continue;
					}
				}

				*(bdg++) = keypress;
				disp[0] = keypress;
				disp[1] = '\0';

				if(KeypadActive) user_output_write (3, col + arrowKeyCounter, disp);

				if(arrowKeyCounter == maxbadgelen - 1)
				{

					//printf("max badge\n");
					disp[0] = badge[arrowKeyCounter - 1];
				#ifdef DEBUG
					printf("\nWriting %c at col %d " , disp[0], col + arrowKeyCounter - 1);
				#endif
					if(KeypadActive) user_output_write (3, col + arrowKeyCounter - 1,disp);
					arrowKeyCounter ++;
				}

				if(arrowKeyCounter < maxbadgelen - 1)
				{
					//printf("badge = %s\n", badge);
					if(arrowKeyCounter == numOfChars)
						numOfChars++;
					arrowKeyCounter ++;
				}
				else
				{

					//printf("badge+++ = %s\n", badge);
					continue;
			    }
			#ifdef DEBUG
				printf("\nAfter keypress - numOfChars = %d", numOfChars);
				printf("\nAfter keypress - arrowKeyCounter = %d", arrowKeyCounter);

			#endif
			}
			else if ( displayCleared || reviewMode)
			{
				//printf("TIMEOUT1\n");
				user_output_cursor(USER_CURSOR_OFF);
				oper->Beep(TYPE_TimeOut);;
				//printf("return TimeOut2\n");
				return SC_TIMEOUT;
			}
		}

//ENDLOOP:
		strcpy(buffer, badge);
	#else
		printf("Enter Badge Number : ");
		scanf("%s", buffer);
	#endif
#ifdef client_prologic
printf("validate badge input: buffer is %s\n", buffer);
	Code code;
	if ( getSourceBadge() == SysInputSourceKeypad )
	{
		code = oper->ValidateKeypadBadge(buffer);
	}
	else
	{
		code = oper->ValidateBadge(buffer);
	}
printf("after validation of badge input: buffer is %s\n", buffer);
#else
	Code code = oper->ValidateBadge(buffer);
#endif

	//printf("ReadBadge3\n");
	if( code != SC_OKAY)
	{
		#ifdef __TA7700_TERMINAL__
			printf("befor clear for INVALID BADGE\n ");
			user_output_clear();
			char ibmsg[21];
			int lenIbmsg = oper->GetPromptMessage(TYPE_InvalidBadge,ibmsg,21);
			if( lenIbmsg != 0 )
			{
				int col = ( 20 - lenIbmsg ) / 2 + 1;
				user_output_write(2, col, ibmsg);
			}
			else
			{
				//printf("we are here INVALID BADGE \n");
				user_output_write(2, 1, "   INVALID BADGE    ");
			}
			oper->TurnOnLEDYellow();
			oper->Beep(TYPE_Error);
			usleep(oper->GetErrorMsgTimeout() * 1000);
			oper->TurnOffLEDYellow();
			if(!reviewMode)
			 pthread_mutex_unlock( &lockDisplay );
		#else
			char ibmsg[21];
			oper->GetPromptMessage(TYPE_InvalidBadge,ibmsg,21)
			printf("%s\n", ibmsg);
		#endif

		transitem->ReleaseProgrammingLock();
		//if(reviewMode) goto ReviewMode;
		goto BadgeRead;
	}
#ifdef client_prologic
	if ( getSourceBadge() == SysInputSourceKeypad )
	{
		oper->ReadKeypadOffset(buffer);
	}
	else
	{
		oper->ReadBadgeOffset(buffer);
	}
#else
	oper->ReadBadgeOffset(buffer);
#endif
//    if(reviewMode)
//    {
//       OperationStatusCode code;
//       code = DisplayTrans();
//       if(code == SC_Success || code == SC_Clear)
//       {
//	               // printf("HELLO\n");
//	                transitem->ReleaseProgrammingLock();
//	                goto ReviewMode;

//	   }
//	   else
//	   {
//	    transitem->ReleaseProgrammingLock();
//	    return code;
//	   }

//	}
   // printf("ReadBadge4 badge=%s\n",buffer);
	return SC_Success;
}

OperationStatusCode OperationIODriver::ReadBadge2( )
{

//printf("enter readbadge2\n");

BadgeRead:
	// Need to Change this when we add more sources
	//source = SysInputSourceKeypad;
	bool KeypadActive = false;
	#ifdef __TA7700_TERMINAL__
		static char badge[20];
		char * bdg = badge;
		bool displayCleared = false;
		bool reviewMode = false;

		int maxbadgelen = oper->GetMaxBadgeLen();
		int numOfChars = 0;
		int arrowKeyCounter = 0;


		static char disp1[2];
		disp1[1] = '\0';

		int col = ( 20 - maxbadgelen ) / 2 + 1;
		UserInput ::TimeOut(10000000);

	 	//Flush all the pipes before read
//printf("readbadge2: getfpipmode\n");
		FPIDMode fpIdMode = oper->GetFPIDMode();
//printf("readbadge2: flush\n");
		UserInput ::Flush();
//printf("readbadge2: after flush\n");


//		if(false)
//	    {

//ReviewMode:
//          UserInput ::TimeOut(10000000);
//          fpIdMode = oper->GetFPIDMode();
//          UserInput ::Flush();
//          UserInput ::ClearBuffer();
//          numOfChars = 0;
//		  arrowKeyCounter = 0;
//		  badge[0]='\0';
//		  bdg = badge;
//		  displayCleared = false;
//		  maxbadgelen = oper->GetMaxBadgeLen();
//          if(!reviewMode)
//            pthread_mutex_lock( &lockDisplay );
//		  user_output_clear();
//		  user_output_write(1, 5,"REVIEW MODE");
//		  user_output_write(2, 1, " ENTER BADGE NUMBER ");
//		  reviewMode = true;
//		  user_output_write(3,col-1,BLANKSPACE);
//		  user_output_cursor(USER_CURSOR_BLINK);
//		}

		while(true)
		{

			char keypress = 0 ;
			//printf("ReadBadge0\n");
			UserInput ::source_t source_temp = UserInput ::OTHER;
//printf("readbadge2: getchar\n");
			UserInput ::status_t r = UserInput ::GetChar ( keypress, source_temp);
//printf("\nreadbadge2: after getchar\n");
			//printf("ReadBadge1 %s keypress = %c\n",UserInput ::getBuffer(),keypress);
			//printf("ReadBadge1 %s keypress = %d\n",UserInput ::getBuffer(),keypress);
			//Initialise the source
			switch (source_temp)
			{
				case UserInput ::KEYBOARD :
						source = SysInputSourceKeypad;
						KeypadActive = true;
						//printf("key = %c\n",keypress);
						#ifdef DEBUG
							printf("\n source is keypad\n");
						#endif
						break;
				case UserInput ::PROX :
						source = SysInputSourceProximity;
						#ifdef DEBUG
							printf("\n source is proxy\n");
						#endif
						KeypadActive = false;
						break;
				case UserInput ::MAG :
						//printf("ReadMAG1\n");
					    source = SysInputSourceMagnetic;
						//pthread_mutex_lock( &lockDisplay );
						//strcpy(buffer,UserInput ::getBuffer());
                        //UserInput ::ClearBuffer();
						KeypadActive = false;
						//return SC_Success;
						#ifdef DEBUG
							printf("\n source is Mag\n");
						#endif
					    //goto ENDLOOP;
						break;
				case UserInput ::BARCODE :
						source = SysInputSourceBarcode;
						KeypadActive = false;
						#ifdef DEBUG
							printf("\n source is barcode\n");
						#endif
						break;
				case UserInput ::OTHER :
						source = SysInputSourceDefault;
						KeypadActive = false;
						#ifdef DEBUG
							printf("\n source is default\n");
						#endif
						break;
				case UserInput ::FPRINT :
						source = SysInputSourceFprint;
						KeypadActive = false;
						#ifdef DEBUG
							printf("\n source is finger print\n");
						#endif
						break;
			}
//printf("readbadge2: get IdMode1\n");
			FPIDMode fpIdMode1 = oper->GetFPIDMode();
//printf("readbadge2: after get IdMode1\n");
			if(fpIdMode1 != fpIdMode)
			{
				//mode has been changed. Go to start
				//printf("return TimeOut1\n");
				return SC_TIMEOUT;
			}

		//	if(fpIdMode1 == IDENTIFY_MODE)
		//	{
				//In Identify mode, we are in ReadBadge - means database is empty.
				//Check if the templates were sent in the mean time,
				//If yes, do not accept key punch - as we are in identify mode
		//		FptemplateTable fptemplatetbl;
		//		if( fptemplatetbl.RecordsExist() )
		//		{
		//			return SC_TIMEOUT;
		//		}
		//	}
//printf("readbadge2: check GetChar result: r = %d\n", r);
			if ( r == UserInput ::OK )
			{
//printf("readbadge2: UserInput::OK\n");
//printf("readbadge2: keypress is %d\n", keypress);

				if( keypress == '-' )
				{
					oper->Beep(150, TYPE_Reject);
					continue;
				}

				if(keypress == '+' )
				{

//					if(fpUnitEnabled)
//					{

//					}
//					else
//					{
						oper->Beep(150, TYPE_Reject);
						continue;
//					}
				}

				if( keypress == '>')
				{
//					if(numOfChars == 0)
//					{
						oper->Beep(150, TYPE_Reject);
						continue;
//					}


//				#ifdef DEBUG
//					printf("\n\n > pressed");
//					printf("\nnumOfChars = %d", numOfChars);
//					printf("\arrowKeyCounter = %d", arrowKeyCounter);
//					printf("\n maxbadgelen = %d", maxbadgelen );
//				#endif
//					if(arrowKeyCounter < numOfChars)
//					{
//						oper->Beep(TYPE_KeyPress);
//						disp1[0] = badge[arrowKeyCounter];
//						int col1 = col + arrowKeyCounter;
//					#ifdef DEBUG
//						printf("\nFor > - Writing buffer %c at col %d", disp1[0], col1);
//					#endif
//						user_output_write(3, col1, disp1);

//						*(bdg) ++;
//						arrowKeyCounter ++;

//						if(arrowKeyCounter == (numOfChars - 1))
//						{
//						#ifdef DEBUG
//							printf("\n Reaching end of length");
//							printf("\n Badge length is - %d", strlen(badge));

//							printf("\n Badge contains - ");
//							for(int i=0; i < strlen(badge) ; i++)
//							printf("%c ", badge[i]);
//						#endif

//							if(numOfChars == maxbadgelen)
//							{
//								*(bdg) ++;
//								arrowKeyCounter ++;
//							}
//							else
//								continue;
//						}
//					}
//					else
//					{
//						//for the last digit
//						oper->Beep(150, TYPE_Reject);
//					}
//					continue;

				}

				if(keypress == '<')
				{
//				#ifdef DEBUG
//					printf("\n\n< key pressed");
//					printf("\n numOfChars is %d", numOfChars);
//				#endif

//					if(arrowKeyCounter > 0)
//					{
//						oper->Beep(TYPE_KeyPress);
//						if(arrowKeyCounter == 1)
//						{
//						#ifdef DEBUG
//							printf("\n arrowKeyCounter is 1");
//							printf("\nBadge contains - ");
//							for(int i=0; badge[i] != '\0' ; i++)
//							printf("%c ", badge[i]);
//							printf("\nWriting blank space at row 3, col %d", col - 1);
//						#endif

//							user_output_write(3, col - 1, BLANKSPACE);
//						}
//						else if(arrowKeyCounter == maxbadgelen)
//						{
//						#ifdef DEBUG
//							printf("\nBadge contains - ");
//							for(int i=0; badge[i] != '\0' ; i++)
//								printf("%c ", badge[i]);
//							printf("\n arrowKeyCounter == maxbadgelen");
//						#endif

//							disp1[0] = badge[arrowKeyCounter - 3];
//							int col1 = arrowKeyCounter + col - 3;
//						#ifdef DEBUG
//							printf("\nWriting %c at row 3, col %d", disp1[0], col1);
//						#endif
//							user_output_write(3, col1, disp1);

//							*(bdg) --;
//							arrowKeyCounter --;
//						}
//						else
//						{
//						#ifdef DEBUG
//							printf("\nBadge contains - ");
//							for(int i=0; badge[i] != '\0' ; i++)
//							printf("%c ", badge[i]);

//							printf("\n numOfChars is %d, greater than 1 ", numOfChars);
//							printf("\n arrowKeyCounter is %d, greater than 1 ", arrowKeyCounter);
//						#endif
//							disp1[0] = badge[arrowKeyCounter - 2];
//							int col1 = arrowKeyCounter + col - 2;
//						#ifdef DEBUG
//							printf("\nWriting %c at row 3, col %d", disp1[0], col1);
//						#endif

//							user_output_write(3, col1, disp1);
//						}

//						*(bdg) --;
//						arrowKeyCounter --;
//						//printf("\n\n Continue !");
//						continue;

//					}
//					else
//					{
//						if(numOfChars == 0)
//						{
//						  oper->Beep(TYPE_KeyPress);
//						  goto ReviewMode;
//                        }
//						else
//						{
						    oper->Beep(150, TYPE_Reject);
						    continue;
//						}
//					}
				}
				//esc with an empty buffer: return SC_clear
				//esc with a non-empty buffer: clear the buffer
				if( keypress == ESC )
				{
					oper->Beep(TYPE_KeyPress);
					/*if(reviewMode)
					{
					    user_output_cursor(USER_CURSOR_OFF);
						transitem->ReleaseProgrammingLock();
						return SC_Clear;
					}*/
					#ifdef DEBUG
						printf("\n ESC Key pressed");
						printf("\nBadge contains - ");
							for(int i=0; badge[i] != '\0' ; i++)
						printf("%c ", badge[i]);
					#endif

					if(strlen(badge) == 0)
					{
						user_output_cursor(USER_CURSOR_OFF);
						return SC_Clear;
					}
					else
					{
						numOfChars = 0;
						arrowKeyCounter = 0;

						bdg = badge;
						*(bdg) = '\0';
						user_output_write (3, 1, BLANK_LINE);
						user_output_write(3, col - 1, BLANKSPACE);
						continue;
					}
				}
//				//20051229 wjm
//				//'.' as input character: reset buffer to 0
//				if ( DOT == keypress ){
//					numOfChars = 0;
//					arrowKeyCounter = 0;
//					bdg = badge;
//					*(bdg) = '\0';
//					user_output_write (3, 1, BLANK_LINE);
//					user_output_write(3, col - 1, BLANKSPACE);
//					continue;
//				}

				//enter with no data in buffer: no op
				//enter with data in buffer: leave loop
				if(keypress == KEY_ENTER)
				{

//printf("readbadge2: keypress == KEY_ENTER\n");

					oper->Beep(TYPE_KeyPress);
					while(arrowKeyCounter < numOfChars)
					{
						*(bdg) ++;
						arrowKeyCounter ++;
					}
					*(bdg) = '\0';

				#ifdef DEBUG
					printf("\nEnter key pressed - Badge contains - ");
					for(int i=0; badge[i] != '\0' ; i++)
						printf("%c ", badge[i]);
					printf("\n");
				#endif
					if(strlen(badge) == 0) continue;

//					printf("readbadge: IOD::turnonswipeandgo is %d\n", OperationIODriver::TurnOnSwipeAndGo);
//					printf("readbadge: turnonswipandgo is %d\n", TurnOnSwipAndGo);
//					printf("readbadge: enableswipandgo is %d\n", EnableSwipAndGo);
					if ( TurnOnSwipeAndGo && EnableSwipAndGo)
					{
						//printf("readbadge2: turnonswipengo is true, enableswipandgo is true\n");
						keypress = SwipeAndGoKey;
						// dont break, fall thru to function key check
					}
					else
					break;
				}

				if( keypress >= 'A' && keypress <= 'L' )
				{
					//function key pressed
					while(arrowKeyCounter < numOfChars)
					{
						*(bdg) ++;
						arrowKeyCounter ++;
					}
					*(bdg) = '\0';

				#ifdef DEBUG
					printf("\nFunction key pressed - Badge contains - ");
					for(int i=0; badge[i] != '\0' ; i++)
						printf("%c ", badge[i]);
				#endif
					if(strlen(badge) == 0)
					{
					    //printf("press = %c  SwipeAndGoKey = %c\n",keypress,SwipeAndGoKey);
						if(keypress == SwipeAndGoKey)
						  EnableSwipAndGo = !EnableSwipAndGo;
						oper->Beep(150, TYPE_Reject);
						continue;
					}
					Function = keypress;
					function_read = true;

					break;
				}

				//displayCleared initially set to false.
				//after the first run through here, displayCleared is true
				if(!displayCleared)
				{
//printf("readbadge2: 1st time thru? displaycleared is false\n");
					if(!reviewMode)
					{
//printf("readbadge2: !displaycleared, !reviewmode: lock the display\n");
					    //pthread_mutex_lock( &lockDisplay );
						pthread_mutex_trylock( &lockDisplay );
//printf("readbadge2: !displaycleared, !reviewmode: after lock the display\n");
					}
					/**
					* Validate the Initial Sorce
					*/
//printf("readbadge2: validatesource\n");
					Code code = oper->ValidateSource(source, TYPE_EmployeeInputSource);
//printf("readbadge2: after validatesource\n");

					//if sorce is invalid, wait for a duration of timeout
					if( code != SC_OKAY)
					{
//printf("readbadge2: invalid source. wait for timeout and display msg\n");
						user_output_clear();
						char ivs[21];
						int lenIvs = oper->GetPromptMessage(TYPE_InvalidSource,ivs,21);
						if( lenIvs != 0 )
						{
							//printf("333333\n");
							int col = ( 20 - lenIvs ) / 2 + 1;
							user_output_write(2, col, ivs);
						}
						else
						{
							user_output_write(2, 1, "   INVALID SOURCE   ");
						}

						oper->TurnOnLEDYellow();
						oper->Beep(TYPE_Error);

						usleep(oper->GetErrorMsgTimeout() * 1000 );
						oper->TurnOffLEDYellow();
						if(!reviewMode)
						  pthread_mutex_unlock( &lockDisplay );

						UserInput ::ClearBuffer();
						if(source == SysInputSourceFprint)pthread_cond_signal(&FPOperation::fprint_cond_flag);
						//if(reviewMode) goto ReviewMode;
						goto BadgeRead;
					}

					//AcquireProgrammingLock();
//					if(reviewMode)
//					{
//					   user_output_clear();
//		               user_output_write(1, 5,"REVIEW MODE");
//		               user_output_write(2, 1, " ENTER BADGE NUMBER ");
//		               user_output_write(3,col-1,BLANKSPACE);
//		               user_output_cursor(USER_CURSOR_BLINK);
//		            }

				    if(KeypadActive && !reviewMode){
					    user_output_clear();
					    user_output_write(2, 1, " ENTER BADGE NUMBER ");
					    user_output_cursor(USER_CURSOR_BLINK);
				    }
				    displayCleared = true;
			    }

				static char disp[2];
				if(source_temp == UserInput::KEYBOARD)
				{
					oper->Beep(TYPE_KeyPress);
				}

				//avoid overflow of buffer

				if(source == SysInputSourceFprint)
				{
//					if(fpOpr.fpOperation == Identify_Operation)
//					{
//						if( fpOpr.ScanResult == SC_Success || fpOpr.ScanResult == 0 )
//						{
//							int UserIdLen = strlen(UserInput::getBuffer());
//                            int leftZeros = SystemSetup.badge.length.maximum - UserIdLen;
//							if(leftZeros > 0)
//							{
//								for(int i = 0 ; i < leftZeros ; i++)
//								{
//									if(i == 0)
//									   strcpy(buffer,"0");
//									else
//                                       strcat(buffer,"0");
//								}
//								strcat(buffer, UserInput::getBuffer());
//							}
//							else
//							{
//							  strcpy(buffer, UserInput::getBuffer());
//							}
//							oper->Beep(TYPE_KeyPress);
//							UserInput ::Flush();
//							UserInput ::ClearBuffer();
//							return  SC_Success;
//						}
//						else
//						{
//							UserInput ::Flush();
//							UserInput ::ClearBuffer();
//                            strcpy(buffer, "");
//							return  SC_Fail;
//						}
//					}
//					else
//					{
//					   UserInput ::Flush();
//					   UserInput ::ClearBuffer();
//					   strcpy(buffer, "");
//					   goto BadgeRead;
//					}
//printf("readbadge2: fingerprint was read. Not allowed here\n");
					   UserInput ::Flush();
					   UserInput ::ClearBuffer();
					   strcpy(buffer, "");
					   goto BadgeRead;
				}

 // not needed for rb2 ?
				if( arrowKeyCounter == maxbadgelen || source == SysInputSourceFprint )
				{
//printf("readbadge2: arrowkeycounter == maxbadgelen, or source is fingerprint\n");
					if(KeypadActive)
					{
					    badge[arrowKeyCounter-1]= keypress;
						disp[0]=keypress;
						disp[1]='\0';
						//printf("badge = %s\n", badge);
						user_output_write (3, col + arrowKeyCounter - 1,disp);
						disp[0]=badge[arrowKeyCounter-2];
						disp[1]='\0';
						user_output_write (3, col + arrowKeyCounter - 2,disp);
						continue;
					}
					else
					{
						//Invalid badge was swiped with more chars than max size. Clock should reject it.
						//Before this snippet was added, users could even swipe their credit cards and the clock accepted it.
						//The above condition is an exception for config badge (badge with all zeros)
						bool config_bdg = false;
						for( int bl=0; bl < maxbadgelen; bl++)
						{
							if(  badge[bl] != '0' ) break;
							if( bl == maxbadgelen - 1 ) {config_bdg = true; break;}
						}

						if(! config_bdg)
						{
							user_output_clear();
							char ibmsg[21];
							int lenIbmsg = oper->GetPromptMessage(TYPE_InvalidBadge,ibmsg,21);
			                if( lenIbmsg != 0 )
			                {
				                int col = ( 20 - lenIbmsg ) / 2 + 1;
				                user_output_write(2, col, ibmsg);
			                }
			                else
			                {
				                user_output_write(2, 1, "   INVALID BADGE    ");
			                }
							oper->TurnOnLEDYellow();
							oper->Beep(TYPE_Error);
							usleep(oper->GetErrorMsgTimeout() * 1000);
							oper->TurnOffLEDYellow();

							numOfChars = 0;
							arrowKeyCounter = 0;

							//Flush the buffer
							UserInput ::ClearBuffer();

							bdg = badge;
							*(bdg) = '\0';
							user_output_cursor(USER_CURSOR_OFF);
//							if(reviewMode)
//							{
//							    transitem->ReleaseProgrammingLock();
//							    goto ReviewMode;
//							}
							return SC_Clear;
						}
						else
							continue;
					}
				}

				*(bdg++) = keypress;
				disp[0] = keypress;
				disp[1] = '\0';

				if(KeypadActive) user_output_write (3, col + arrowKeyCounter, disp);

				if(arrowKeyCounter == maxbadgelen - 1)
				{

					//printf("max badge\n");
					disp[0] = badge[arrowKeyCounter - 1];
				#ifdef DEBUG
					printf("\nWriting %c at col %d " , disp[0], col + arrowKeyCounter - 1);
				#endif
					if(KeypadActive) user_output_write (3, col + arrowKeyCounter - 1,disp);
					arrowKeyCounter ++;
				}

				if(arrowKeyCounter < maxbadgelen - 1)
				{
					//printf("badge = %s\n", badge);
					if(arrowKeyCounter == numOfChars)
						numOfChars++;
					arrowKeyCounter ++;
				}
				else
				{

					//printf("badge+++ = %s\n", badge);
					continue;
			    }
			#ifdef DEBUG
				printf("\nAfter keypress - numOfChars = %d", numOfChars);
				printf("\nAfter keypress - arrowKeyCounter = %d", arrowKeyCounter);

			#endif
			}
			else if ( displayCleared || reviewMode)
			{
//printf("readbadge2: not OK. check getchar: displaycleared=true or reviewmode=true\n");
				//printf("TIMEOUT1\n");
				user_output_cursor(USER_CURSOR_OFF);
				oper->Beep(TYPE_TimeOut);;
				//printf("return TimeOut2\n");
				return SC_TIMEOUT;
			}
			else
			{
				//printf("readbadge2: not OK. fall through\n");
			}
		}

//ENDLOOP:
		strcpy(buffer, badge);
	#else
		printf("Enter Badge Number : ");
		scanf("%s", buffer);
	#endif
#ifdef client_prologic
	Code code;
	if ( getSourceBadge() == SysInputSourceKeypad )
	{
		code = oper->ValidateKeypadBadge(buffer);
	}
	else
	{
		code = oper->ValidateBadge(buffer);
	}
#else
	Code code = oper->ValidateBadge(buffer);
#endif
	//printf("ReadBadge3\n");
	if( code != SC_OKAY)
	{
		#ifdef __TA7700_TERMINAL__
			//printf("befor clear for INVALID BADGE\n ");
			user_output_clear();
			char ibmsg[21];
			int lenIbmsg = oper->GetPromptMessage(TYPE_InvalidBadge,ibmsg,21);
			if( lenIbmsg != 0 )
			{
				int col = ( 20 - lenIbmsg ) / 2 + 1;
				user_output_write(2, col, ibmsg);
			}
			else
			{
				//printf("we are here INVALID BADGE \n");
				user_output_write(2, 1, "   INVALID BADGE    ");
			}
			oper->TurnOnLEDYellow();
			oper->Beep(TYPE_Error);
			usleep(oper->GetErrorMsgTimeout() * 1000);
			oper->TurnOffLEDYellow();
			if(!reviewMode)
			 pthread_mutex_unlock( &lockDisplay );
		#else
			//printf("%s\n", oper->GetPromptMessage(TYPE_InvalidBadge));
		#endif

		transitem->ReleaseProgrammingLock();
		//if(reviewMode) goto ReviewMode;
		goto BadgeRead;
	}

#ifdef client_prologic
	if ( getSourceBadge() == SysInputSourceKeypad )
	{
		oper->ReadKeypadOffset(buffer);
	}
	else
	{
		oper->ReadBadgeOffset(buffer);
	}
#else
	oper->ReadBadgeOffset(buffer);
#endif

//    if(reviewMode)
//    {
//       OperationStatusCode code;
//       code = DisplayTrans();
//       if(code == SC_Success || code == SC_Clear)
//       {
//	               // printf("HELLO\n");
//	                transitem->ReleaseProgrammingLock();
//	                goto ReviewMode;

//	   }
//	   else
//	   {
//	    transitem->ReleaseProgrammingLock();
//	    return code;
//	   }

//	}
   // printf("ReadBadge4 badge=%s\n",buffer);
	return SC_Success;
}


//void OperationIODriver::AcquireProgrammingLock()
//{
//	user_output_clear();
//	while( !transitem->TryProgrammingLock() )
//	{
//		//printf("in AcquireProgrammingLock loop\n");
//		user_output_write(2, 1, " PROGRAMMING CLOCK  ");
//		user_output_write(3, 1, "   PLEASE WAIT...   ");
//		usleep(10000); // sleep for 10ms
//	}
//}

//void OperationIODriver::AcquireTemplateLock()
//{
//	user_output_clear();
//	while( !transitem->TryTemplateLock() )
//	{
//		user_output_write(2, 1, "RETRIEVING TEMPLATES");
//		user_output_write(3, 1, "   PLEASE WAIT...   ");
//		usleep(10000); // sleep for 10ms
//	}
//}

//void OperationIODriver::AcquireDBLock()
//{
//	user_output_clear();
//	while( !transitem->TryDBLock() )
//	{
//		user_output_write(2, 1, "UPLOADING TEMPLATES ");
//		user_output_write(3, 1, "   PLEASE WAIT...   ");
//		usleep(10000); // sleep for 10ms
//	}
//}

//void OperationIODriver::HandleClockOutOfMemory()
//{
//	pthread_mutex_lock( &lockDisplay );
//	user_output_clear();
//	user_output_write(2, 4, "CLOCK IS FULL");
//	usleep( oper->GetMessageTimeout() * 1000 );
//	pthread_mutex_unlock( &lockDisplay );
//}


OperationStatusCode OperationIODriver::SuperReadEmpBadge()
{

printf("enter superreadempbadge\n");

	BadgeRead:
	// Need to Change this when we add more sources
	//source = SysInputSourceKeypad;
#ifdef __TA7700_TERMINAL__
	user_output_clear();
	user_output_write(2, 1, " ENTER BADGE NUMBER  ");

	bool KeypadActive = false;
	static char badge[15];
	char * bdg = badge;
	bool timedOut = false, cleared = false;;
	int timeout = oper->GetSupervisorModeTimeout();
	int maxbadgelen = oper->GetMaxBadgeLen();
	int colm = ( 20 - maxbadgelen ) / 2 + 1;
	int numOfChars = 0;
	int arrowKeyCounter = 0;
	UserInput ::TimeOut ( timeout * 1000 );


	user_output_cursor(USER_CURSOR_BLINK);
	user_output_write(3, colm - 1, BLANKSPACE);

	static char disp1[2];
		disp1[1] = '\0';

	//Flush all the pipes before read
	UserInput ::Flush();
	while(true)
	{
		char keypress = 0 ;
		UserInput ::source_t source_temp = UserInput ::OTHER;
		UserInput ::status_t r = UserInput ::GetChar ( keypress, source_temp );
		//Initialise the source
		switch (source_temp)
		{
			case UserInput ::KEYBOARD :
					source = SysInputSourceKeypad;
					KeypadActive = true;
					break;
			case UserInput ::PROX :
					source = SysInputSourceProximity;
					break;
			case UserInput ::MAG :
					source = SysInputSourceMagnetic;
					break;
			case UserInput ::BARCODE :
					source = SysInputSourceBarcode;
					break;
			case UserInput ::OTHER :
					source = SysInputSourceDefault;
					break;
		}

		if (r == UserInput ::OK)
		{

			Code code = oper->ValidateSource(source, TYPE_SupervisorEmployeeInputSource);
	        //if sorce is invalid, wait for a duration of timeout
		    if( code != SC_OKAY)
			{
				user_output_clear();
				char ivs[21];
				int lenIvs =  oper->GetPromptMessage(TYPE_InvalidSource,ivs,21);
				if( lenIvs != 0 )
			    {
					//printf("333333\n");
					int colm = ( 20 - lenIvs ) / 2 + 1;
					user_output_write(2, colm, ivs);
				}
				else
				{
					user_output_write(2, 1, "   INVALID SOURCE   ");
				}

					oper->TurnOnLEDYellow();
					oper->Beep(TYPE_Error);

					usleep(oper->GetErrorMsgTimeout() * 1000 );
					oper->TurnOffLEDYellow();

					//pthread_mutex_unlock( &lockDisplay );

					UserInput ::ClearBuffer();
					goto BadgeRead;
			}


			if( ( keypress >= 'A' && keypress <= 'L' ) || keypress == '+' ||
	      			keypress == '-' )
			{
				oper->Beep(150, TYPE_Reject);
				continue;
			}

			if( keypress == '>')
			{
				if(numOfChars == 0)
				{
					oper->Beep(150, TYPE_Reject);
					continue;
				}


			#ifdef DEBUG
				printf("\n\n > pressed");
				printf("\nnumOfChars = %d", numOfChars);
				printf("\arrowKeyCounter = %d", arrowKeyCounter);
				printf("\n maxbadgelen = %d", maxbadgelen );
			#endif
				if(arrowKeyCounter < numOfChars)
				{
					oper->Beep(TYPE_KeyPress);
					disp1[0] = badge[arrowKeyCounter];
					int col1 = colm + arrowKeyCounter;
				#ifdef DEBUG
					printf("\nFor > - Writing buffer %c at col %d", disp1[0], col1);
				#endif
					user_output_write(3, col1, disp1);

					*(bdg) ++;
					arrowKeyCounter ++;

					if(arrowKeyCounter == (numOfChars - 1))
					{
						//int badgelen = strlen(badge);
					#ifdef DEBUG
						printf("\n Reaching end of length");
						printf("\n Badge length is - %d", strlen(badge));

						printf("\n Badge contains - ");
						for(int i=0; i < strlen(badge) ; i++)
						printf("%c ", badge[i]);
					#endif

						if(numOfChars == maxbadgelen)
						{
							*(bdg) ++;
							arrowKeyCounter ++;
						}
						else
						continue;
					}
				}
				else
				{
					//for the last digit
					oper->Beep(150, TYPE_Reject);

				}
				continue;
			}

			if(keypress == '<')
			{
				#ifdef DEBUG
					printf("\n\n< key pressed");
					printf("\n numOfChars is %d", numOfChars);
				#endif

				if(arrowKeyCounter > 0)
				{
					oper->Beep(TYPE_KeyPress);
					if(arrowKeyCounter == 1)
					{
					#ifdef DEBUG
						printf("\n arrowKeyCounter is 1");
						printf("\nBadge contains - ");
						for(int i=0; badge[i] != '\0' ; i++)
						printf("%c ", badge[i]);
						printf("\nWriting blank space at row 3, col %d", colm - 1);
					#endif

						user_output_write(3, colm - 1, BLANKSPACE);
					}
					else if(arrowKeyCounter == maxbadgelen)
					{
					#ifdef DEBUG
						printf("\nBadge contains - ");
						for(int i=0; badge[i] != '\0' ; i++)
							printf("%c ", badge[i]);
						printf("\n arrowKeyCounter == maxbadgelen");
					#endif

						disp1[0] = badge[arrowKeyCounter - 3];
						int col1 = arrowKeyCounter + colm - 3;
					#ifdef DEBUG
						printf("\nWriting %c at row 3, col %d", disp1[0], col1);
					#endif
						user_output_write(3, col1, disp1);

						*(bdg) --;
						arrowKeyCounter --;
					}
					else
					{
					#ifdef DEBUG
						printf("\nBadge contains - ");
						for(int i=0; badge[i] != '\0' ; i++)
						printf("%c ", badge[i]);

						printf("\n numOfChars is %d, greater than 1 ", numOfChars);
						printf("\n arrowKeyCounter is %d, greater than 1 ", arrowKeyCounter);
					#endif
						disp1[0] = badge[arrowKeyCounter - 2];
						int col1 = arrowKeyCounter + colm - 2;
					#ifdef DEBUG
						printf("\nWriting %c at row 3, col %d", disp1[0], col1);
					#endif

						user_output_write(3, col1, disp1);
					}

					*(bdg) --;
					arrowKeyCounter --;
					printf("\n\n Continue !");
					continue;
				}
				else
				{
					oper->Beep(150, TYPE_Reject);
					continue;
				}
			}

			if( keypress == ESC )
			{
				oper->Beep(TYPE_KeyPress);
				if(strlen(badge) == 0)
				{
					cleared = true;
					break;
				}
				else
				{
					numOfChars = 0;
					arrowKeyCounter = 0;
					bdg = badge;
					*(bdg) = '\0';
					user_output_write (3, 1, BLANK_LINE);
					user_output_write(3, colm - 1, BLANKSPACE);
					continue;
				}
			}
			if(keypress == KEY_ENTER)
			{
				oper->Beep(TYPE_KeyPress);
				int datalength = 0;
				int badgelen = strlen(badge);

				if(numOfChars != (maxbadgelen -1))
				{
					datalength = numOfChars;
				}
				else
					datalength = badgelen;

				if(arrowKeyCounter < datalength)
					{
						while(arrowKeyCounter < datalength)
						{
						*(bdg) ++;
						arrowKeyCounter ++;
					}
				}

				*(bdg) = '\0';
				#ifdef DEBUG
					printf("\nEnter key pressed - Badge contains - ");
					for(int i=0; badge[i] != '\0' ; i++)
						printf("%c ", badge[i]);
				#endif
				if(strlen(badge) == 0) continue;
				break;
			}

			if(source_temp == UserInput::KEYBOARD)
			{
				oper->Beep(TYPE_KeyPress);
			}

			if( arrowKeyCounter == maxbadgelen )
			{
			  if(KeypadActive)
			  {
				badge[arrowKeyCounter-1]= keypress;
				disp1[0]=keypress;
				disp1[1]='\0';
				//printf("badge = %s\n", badge);
				user_output_write (3, colm + arrowKeyCounter - 1,disp1);
				disp1[0]=badge[arrowKeyCounter-2];
				disp1[1]='\0';
				user_output_write (3, colm + arrowKeyCounter - 2,disp1);
				continue;
			  }
			  else
			  {
				//Invalid badge was swiped with more chars than max size. Clock should reject it.
				//Before this snippet was added, users could even swipe their credit cards and the clock accepted it.
				//The above condition is an exception for config badge (badge with all zeros)
				bool config_bdg = false;
				for( int bl=0; bl < maxbadgelen; bl++)
				{
					if(  badge[bl] != '0' ) break;
					if( bl == maxbadgelen - 1 ) {config_bdg = true; break;}
				}

				if(! config_bdg)
				{
					user_output_clear();
					user_output_write(2, 1, "   INVALID BADGE    ");
					oper->TurnOnLEDYellow();
					oper->Beep(TYPE_Error);
					usleep(oper->GetErrorMsgTimeout() * 1000);
					oper->TurnOffLEDYellow();

				    numOfChars = 0;
					arrowKeyCounter = 0;

					//Flush the buffer
					UserInput ::ClearBuffer();

					bdg = badge;
					*(bdg) = '\0';
					user_output_cursor(USER_CURSOR_OFF);
					goto BadgeRead;
				}
				else
					continue;
			  }
		    }
			static char disp[2];
			*(bdg++) = keypress;
			disp[0] = keypress;
			disp[1] = '\0';
			#ifdef DEBUG
				printf("\nAfter keypress - writing %c at col %d ", disp[0], colm + arrowKeyCounter);
			#endif

			user_output_write (3, colm + arrowKeyCounter, disp);

			if(arrowKeyCounter == maxbadgelen - 1)
			{

				disp[0] = badge[arrowKeyCounter - 1];
			#ifdef DEBUG
				printf("\nWriting %c at col %d " , disp[0], colm + arrowKeyCounter - 1);
			#endif
				user_output_write (3, colm + arrowKeyCounter - 1, disp);
				arrowKeyCounter ++;
			}

			if(arrowKeyCounter < maxbadgelen - 1)
			{
				if(arrowKeyCounter == numOfChars)
					numOfChars++;
				arrowKeyCounter ++;
			}
			else
				continue;

			#ifdef DEBUG
				printf("\nAfter keypress - numOfChars = %d", numOfChars);
				printf("\nAfter keypress - arrowKeyCounter = %d", arrowKeyCounter);
			#endif

		}
		else if(r == UserInput::TIMEOUT)
		{
			timedOut = true;
			break;
		}
	}
	if(timedOut)
    {
		oper->Beep(TYPE_TimeOut);
		return SC_TIMEOUT;
    }
	else if(cleared) return SC_Clear;
	strcpy(buffer, badge);
#else
	printf("Enter Badge Number  ");
	scanf("%s", buffer);
#endif

#ifdef client_prologic
	Code code;
	if ( getSourceBadge() == SysInputSourceKeypad )
	{
		code = oper->ValidateKeypadBadge(buffer);
	}
	else
	{
		code = oper->ValidateBadge(buffer);
	}
#else
	Code code = oper->ValidateBadge(buffer);
#endif

	if( code != SC_OKAY)
	{
#ifdef __TA7700_TERMINAL__
		user_output_clear();
		char ibmsg[21];
		int lenIbmsg =  oper->GetPromptMessage(TYPE_InvalidBadge,ibmsg,21);
		if( lenIbmsg != 0 )
		{
			int col = ( 20 - lenIbmsg ) / 2 + 1;
			user_output_write(2, col, ibmsg);
		}
		else
		{
			user_output_write(2, 1, "   INVALID BADGE    ");
		}
		oper->TurnOnLEDYellow();
		oper->Beep(TYPE_Error);
		usleep(oper->GetErrorMsgTimeout() * 1000);
		oper->TurnOffLEDYellow();
#else
      //printf("%s\n", oper->GetPromptMessage(TYPE_InvalidBadge));
#endif
		goto BadgeRead;
    }
#ifdef client_prologic
	if ( getSourceBadge() == SysInputSourceKeypad )
	{
		oper->ReadKeypadOffset(buffer);
	}
	else
	{
		oper->ReadBadgeOffset(buffer);
	}
#else
	oper->ReadBadgeOffset(buffer);
#endif

	return SC_Success;
}

/**
 * Supervisor Edit Transaction
 */
//OperationStatusCode OperationIODriver::EditTrans()
//{
//  //ReadSource(TYPE_SupervisorEmployeeInputSource); //Used only for simulation
//  OperationStatusCode code = SC_Success;
//  source = SysInputSourceKeypad;
//  code = SuperReadEmpBadge();
//  if(code == SC_TIMEOUT || code == SC_Clear) return code;
//  strcpy(empbadge, buffer);
//  LegacyTransItem * transitem = LegacyTransItem::GetInstance();
//  TransFIFO * fifo = transitem->GetFIFO();
//  TAVector<Trans *> * vec = fifo->GetAllTransForBadge(buffer);
//  Trans * trans = NULL;

//  SysTimeSecs time;
//  SysDate date;

//  if( vec->size() > 0 )
//    {
//      int size = vec->size();
//      for( int i=0; i<size; i++ )
//	{
//	  trans = (*(*vec)[i]);
//	  char input;
//#ifdef __TA7700_TERMINAL__
//	DISPLAYTRANS:
//	 // printf("index i = %d",i );
//	  LCD_READ_STATE state = lcdhdlr.DisplayTransaction(trans, oper->GetSupervisorModeTimeout(), &input);
//	  //printf("ipute = %c\n" , input);
//	  if(state == READ_TIMEDOUT)
//	    {
//	      code = SC_TIMEOUT;
//	      goto Cleanup;
//	    }
//	  else if(state == READ_CLEAR)
//	    {
//	      code = SC_Clear;
//	      goto Cleanup;
//	    }
//#else
//	ENTER_AGAIN:
//	  printf("Edit Transactions (0-ADD 1-DELETE 2-NEXT) : ");
//	  scanf("%c", &input);
//	  if(input != '0' && input != '1' && input != '2'
//	     && input != '+' && input != '-' ) goto ENTER_AGAIN;
//#endif

//	  switch (input)
//	    {
//	    case '0': //ADD
//	      //if(oper->GetSupervisorType(superbadge) == TYPE_Supervisor_Entry)
//	      //bypass the above check for SuperEmployee
//          oper->Beep(TYPE_KeyPress);
//          if((oper->GetSupervisorType(superbadge) == TYPE_Supervisor_Entry) || (SuperEmployee))
//		{
//		  AcquireProgrammingLock();
//		  user_output_clear();
//		  for(;;)
//		    {
//		      code = AddSuperTrans(date, time);
//		      if(code == SC_TIMEOUT || code == SC_Clear) goto Cleanup;
//		      bool found = false;

//		      // Check if a Transaction exists for the same date and time
//		      for( int j=0; j<size; j++ )
//			{
//			  bool supertype = false;
//			  Trans * tr = (*(*vec)[j]);
//			  try
//			    {
//			      SuperTrans * suptr = dynamic_cast<SuperTrans *> (tr);
//			      if( suptr != NULL )
//				{
//				  supertype = true;
//				  if(suptr->SuperDate.date == date.date &&
//				     suptr->SuperDate.month == date.month &&
//				     suptr->SuperDate.year == date.year &&
//				     suptr->SuperTime == time)
//				    {
//				      found = true;
//				      break;
//				    }
//				}
//			    }
//			  catch (bad_cast)
//			    {
//			    }
//			  if(!supertype)
//			    {
//			      if(tr->TransDate.date == date.date &&
//				 tr->TransDate.month == date.month &&
//				 tr->TransDate.year == date.year &&
//				 tr->TransTime == time)
//				{
//				  found = true;
//				  break;
//				}
//			    }
//			}
//		      if(found)
//			{
//			  user_output_clear();
//			  user_output_write(1, 1, "TRANSACTION FOUND   FOR THE GIVEN DATE  AND TIME.\nPLEASE TRY AGAIN");
//			  sleep(3);
//			}
//		      else
//			{
//			  break;
//			}
//		    }
//		  if(code == SC_Success)
//		    {
//		      transitem->GetTransactionFileLock();
//		      Code c = transitem->ProcessData((char *)data, false);
//		      transitem->ReleaseTransactionFileLock();
//		      if( c == SC_MEMORY ) code = SC_OutOfMemory;
//		    }
//		  goto Cleanup;
//		}
//	      else
//		{
//#ifdef __TA7700_TERMINAL__
//		  user_output_clear();
//		  user_output_write(2, 1, "    RECALL ONLY     ");
//		  oper->TurnOnLEDYellow();
//		  oper->Beep(TYPE_Error);
//		  usleep(oper->GetErrorMsgTimeout() * 1000);
//		  oper->TurnOffLEDYellow();
//#else
//		  printf("RECALL ONLY\n");
//#endif

//		  code = SC_Fail;
//		  goto Cleanup;
//		}

//	      break;

//	    case '1':
//	      // if(oper->GetSupervisorType(superbadge) == TYPE_Supervisor_Entry)
//	       oper->Beep(TYPE_KeyPress);
//          if((oper->GetSupervisorType(superbadge) == TYPE_Supervisor_Entry) || (SuperEmployee))
//		{
//#ifdef __TA7700_TERMINAL__

//		  /**
//		   * Ask for confirmation
//		   */
//		  bool confirm = false;
//		  lcdhdlr.ShowConfirmationDialog("CONFIRM DELETE", confirm);
//		  if(confirm)
//		    {
//		      user_output_write(1, 1, BLANK_LINE);
//		      user_output_write(3, 1, BLANK_LINE);
//		      user_output_write(4, 1, BLANK_LINE);
//		      user_output_write(2, 1, "TRANSACTION DELETED");
//		      usleep(oper->GetMessageTimeout() * 1000);
//#else
//		      printf("Delete Transaction\n");
//#endif

//		      if( !transitem->RemoveTransactionFromFile(trans) )
//			{
//#ifdef DEBUG
//			  printf("FAILED TO DEL TRANSACTION FROM FILE\n");
//#endif
//			}
//		      // Not needed anymore.
//		      // fifo->RemoveTransaction(trans);
//		      code = SC_Success;
//		      break;
//		    }
//		  else
//		    {
//		      goto DISPLAYTRANS;
//		    }
//		}
//	      else
//		{
//#ifdef __TA7700_TERMINAL__
//		  user_output_clear();
//		  user_output_write(2, 1, "    RECALL ONLY     ");
//		  oper->TurnOnLEDYellow();
//		  oper->Beep(TYPE_Error);
//		  usleep(oper->GetErrorMsgTimeout() * 1000);
//		  oper->TurnOffLEDYellow();
//#else
//		  printf("RECALL ONLY\n");
//#endif

//		  code = SC_Fail;
//		  goto Cleanup;
//		}

//	    case '2':
//	       oper->Beep(TYPE_KeyPress);
//	       if(i==0)i = size;
//          code = SC_Success;
//	      break;
//	    case '+':
//	      oper->Beep(TYPE_KeyPress);
//          if(i==size-1)i = -1;
//          code = SC_Success;
//	      break;
//       case '-':
//          oper->Beep(TYPE_KeyPress);
//          if(i==0)i = size;
//          i=i-2;
//          code = SC_Success;
//	      break;

//        }
//	}
//    }
//  else
//    {
//      //if(oper->GetSupervisorType(superbadge) == TYPE_Supervisor_Entry)
//      if((oper->GetSupervisorType(superbadge) == TYPE_Supervisor_Entry) || (SuperEmployee))
//	{
//#ifdef __TA7700_TERMINAL__

//	  user_output_cursor(USER_CURSOR_OFF);

//	  user_output_write(2, 1, BLANK_LINE);
//	  user_output_write(3, 1, BLANK_LINE);
//	  user_output_write(2, 1, "     NOT FOUND      ");
//	  oper->TurnOnLEDYellow();
//	  oper->Beep(TYPE_Error);
//	  usleep(oper->GetErrorMsgTimeout() * 1000);
//	  oper->TurnOffLEDYellow();
//#else
//	  printf("NOT FOUND\n");
//#endif

//#ifdef __TA7700_TERMINAL__
//	  user_output_write(2, 1, "  ADD TRANSACTION   ");
//	  usleep(oper->GetErrorMsgTimeout() * 1000);
//#else
//	  printf("Add Transaction\n");
//#endif
//	  AcquireProgrammingLock();
//	  user_output_clear();
//	  code = AddSuperTrans(date, time);
//	  if(code == SC_Success)
//	    {
//	      transitem->GetTransactionFileLock();
//	      Code c = transitem->ProcessData((char *)data, false);
//	      transitem->ReleaseTransactionFileLock();
//	      if( c == SC_MEMORY ) code = SC_OutOfMemory;
//	    }
//	}
//      else
//	{
//#ifdef __TA7700_TERMINAL__
//	  user_output_write(3, 1, BLANK_LINE);
//	  user_output_write(2, 1, "    RECALL ONLY     ");
//	  oper->TurnOnLEDYellow();
//	  oper->Beep(TYPE_Error);
//	  usleep(oper->GetErrorMsgTimeout() * 1000);
//	  oper->TurnOffLEDYellow();
//#else
//	  printf("RECALL ONLY\n");
//#endif

//	  code = SC_Fail;
//	  goto Cleanup;
//	}
//    }

// Cleanup:
//  //printf("Reset40\n");
//  Reset();
//  vec->clear();
//  delete vec;
//  vec = NULL;
//  // If we have acquired the programming lock release it

//  transitem->ReleaseProgrammingLock();
//  return code;
//}

/**
 * Add Transaction By Supervisor
 */
//OperationStatusCode OperationIODriver::AddSuperTrans(SysDate & date, SysTimeSecs & time)
//{
//	/**
//	* Reset the Buffer
//	*/
//	//printf("Reset41\n");
//	Reset();

//	if( LegacyTransItem::clockIsFull )
//	{
//		pthread_mutex_unlock( &lockDisplay );
//		HandleClockOutOfMemory();
//		pthread_mutex_lock( &lockDisplay );
//		return SC_TIMEOUT;
//	}

//	/**
//	* Get the Time and add to the transaction
//	*/
//	oper->GetTimeAsString(buffer);
//	//StringPut(buffer);

//	/**
//	* Put the Supervisor Info
//	*/
//	//CharPut(supersourcechar);
//	//StringPut(superbadge);
//	//CharPut(TransFieldSeparatorChar);
//	//CharPut(TransSuperFunctionChar);

//	/**
//	* Get and Validate the Date
//	*/
//	//CharPut('1');

//	OperationStatusCode code = ReadDate();
//	if(code == SC_TIMEOUT || code == SC_Clear) return code;
//	//StringPut(buffer);

//	/**
//	* Parse the String to get the SysDate
//	*/
//	char temp[5];
//	strncpy(temp, buffer, 4);
//	temp[4] = '\0';
//	date.year = strtol(temp, NULL, 10);
//	strncpy(temp, buffer + 4, 2);
//	temp[2] = '\0';
//	date.month = strtol(temp, NULL, 10);
//	strncpy(temp, buffer + 6, 2);
//	temp[2] = '\0';
//	date.date = strtol(temp, NULL, 10);

//	/**
//	* Get and Validate the Time
//	* Add the time to the Transaction Record
//	*/
//	//CharPut('1');
//	code = ReadTime();
//	if (code == SC_TIMEOUT || code == SC_Clear)
//	{
//		return code;
//	}
//	//StringPut(buffer);

//	/**
//	* Parse the Time to build SysTimeSecs
//	*/
//	strncpy(temp, buffer, 2);
//	temp[2] = '\0';
//	int hr = strtol(temp, NULL, 10);
//	strncpy(temp, buffer+2, 2);
//	temp[2] = '\0';
//	int min = strtol(temp, NULL, 10);
//	time = (hr * 60 * 60) + min * 60;

//	//Not necessory.
//	/*
//	const char * msg = oper->GetEmployeeMessage(empbadge);
//	if(msg != NULL && strlen(msg) != 0)
//	{
//		#ifdef __TA7700_TERMINAL__
//			user_output_clear();
//			int col = ( 20 - strlen(msg) ) / 2 + 1;
//			user_output_write(2, col, msg);
//			usleep(oper->GetMessageTimeout() * 1000);
//		#else
//			printf("%s\n", msg);
//		#endif
//	}
//	*/

//	GetCharForSysInputSource(source, sourcechar);
//	//CharPut(sourcechar);
//	//StringPut(empbadge);
//	//CharPut(TransFieldSeparatorChar);
//	/**
//	* Get and validate the Function
//	*/
//	code = ReadFunction(TYPE_Supervisor);
//	if(code == SC_TIMEOUT || code == SC_Clear) return code;
//	if( code == SC_Profile_Denied || code == SC_Access_Denied )
//	{
//		if(!oper->ReportAccesDeniedTransaction())
//		{
//			//printf("Reset42\n");
//			Reset();
//			return code;
//		}
//	}

//	oper->SetAccesDeniedTransFlag(code, flags[4]);
//	//CharPut(Function);

//	/**
//	* Put the Transaction Flags
//	*/
//	for(SysIndex i = 0; i < SysTransactionFlagsMax; i++)
//	{
//		//CharPut(flags[i]);
//	}

//	/**
//	* Add The Level data if Any
//	*/
//	for( int i = 0; i < SysFunctionKeyLevelsMax; i++ )
//	{
//		if(levels[i].Valid())
//		{
//			//CharPut(TransFieldSeparatorChar);
//			FlagsBit8Put (levels[i].Source, _FormatTransactionSource, 1);
//			//StringPut(levels[i].Data);
//		}
//		else
//		{
//			break;
//		}
//	}
//	return SC_Success;
//}

/**
 * Get and Validate the Date
 */
OperationStatusCode OperationIODriver::ReadDate()
{
SuperDateRead:
	#ifdef __TA7700_TERMINAL__
		user_output_clear();
		user_output_write(2, 1, "  DATE (YYYYMMDD)   ");
		//usleep(oper->GetMessageTimeout() * 1000);
		static char date[10];
		LCD_READ_STATE state = lcdhdlr.ReadDate(3, 3, date);
		if (state == READ_TIMEDOUT)
		{
			return SC_TIMEOUT;
		}
		else if(state == READ_CLEAR)
		{
			return SC_Clear;
		}
		strcpy(buffer, date);
	#else
		printf("Enter Date(YYYYMMDD)\n");
		scanf("%s", buffer);
	#endif

	Code code = oper->ValidateDate(buffer);
	if( code != SC_OKAY)
	{
		#ifdef __TA7700_TERMINAL__
			user_output_write(3, 1, "    INVALID DATE    ");
			user_output_write(4, 1, " PLEASE ENTER AGAIN ");
			oper->TurnOnLEDYellow();
			oper->Beep(TYPE_Error);
			usleep(oper->GetErrorMsgTimeout() * 1000);
			oper->TurnOffLEDYellow();
		#else
			printf("Invalid Date. Please Enter Again\n");
		#endif

		goto SuperDateRead;
	}

	return SC_Success;
}

/**
 * Get and Validate the Time
 */
OperationStatusCode OperationIODriver::ReadTime()
{
SuperTimeRead:
	#ifdef __TA7700_TERMINAL__
		char time[6];
		user_output_clear();
		user_output_write(2, 1, "    TIME (HHMM)     ");
		//usleep(oper->GetMessageTimeout() * 1000);
		LCD_READ_STATE state = lcdhdlr.ReadTime(3, 5, time);
		if (state == READ_TIMEDOUT)
		{
			return SC_TIMEOUT;
		}
		else if(state == READ_CLEAR)
		{
			return SC_Clear;
		}
		strcpy(buffer, time);
	#else
		printf("Time (HHMM) : ");
		scanf("%s", buffer);
	#endif

	Code code = oper->ValidateTime(buffer);
	if( code != SC_OKAY)
	{
		#ifdef __TA7700_TERMINAL__
			user_output_write(3, 1, "    INVALID TIME    ");
			user_output_write(4, 1, " PLEASE ENTER AGAIN ");

			oper->TurnOnLEDYellow();
			oper->Beep(TYPE_Error);
			usleep(oper->GetErrorMsgTimeout() * 1000);

			oper->TurnOffLEDYellow();
		#else
			printf("Invalid Time. Please Enter Again\n");
		#endif

		goto SuperTimeRead;
	}

	return SC_Success;
}




/**
 * Get and Validate a PIN number ( 4 digits_
 */
OperationStatusCode OperationIODriver::ReadPIN()
{
	user_output_clear();
	int len = 4;
	int col = ( 20 - len  ) / 2 + 1;
	int timeout = oper->GetDataTimeout();
	char buffer[ len + 1 ];
	for(;;)
	{
		user_output_write(2, 1, "     Enter Pin      ");
		buffer[len] = '\0';
		for( int i=0; i<len; i++ ) buffer[i] = ' ';
		printf("readpin...\n");
		LCD_READ_STATE st = lcdhdlr.ReadData(SysInputTypeNumeric, 3, col, timeout, len, len, buffer);
		if (st == READ_TIMEDOUT)
		{
			printf("readpin: timeout\n");
			return SC_TIMEOUT;
		}
		else if(st == READ_CLEAR)
		{
			printf("readpin: clear and reenter\n");
			return SC_Clear;
		}
		if( (int)strlen( buffer ) < len )
		{
			printf("readpin: pin length less than %d\n", len);
			user_output_write(3, 1, "    INVALID PIN     ");
			oper->TurnOnLEDYellow();
			oper->Beep(TYPE_Error);
			usleep( oper->GetErrorMsgTimeout() * 1000 );
			user_output_write(3, 1, BLANK_LINE);
			oper->TurnOffLEDYellow();
		}
		else
		{
			printf("readpin: accept and return\n");
			oper->Beep(150, TYPE_Accept);
			SetPinCode(buffer);
			usleep(1000000);
			return SC_Allow;
		}
	}
	SetPinCode(buffer);
	usleep(1000000);
	return SC_Success;
}




namespace {
  //read intended number of bytes, unless there is an error.
  int rd1 ( int fd , void* d , unsigned int sz ){
    char* c = ( char* ) d ;
    unsigned int r = 0 ;
    while ( r != sz ){
      int r2 = read ( fd , c + r , sz - r );
      if ( r2 < 0 ){
	if ( errno != EAGAIN ){
	  return r ;
	}
      }
      r += r2 ;
    }
    return r ;
  }

};


/**
 * Get and Validate the Function
 */
OperationStatusCode OperationIODriver::ReadFunction(BadgeType type)
{
	OperationStatusCode code = SC_Allow;
	FunctionRead:
		#ifdef __TA7700_TERMINAL__
			user_output_clear();
			if( ! function_read)
			{
				char efmsg[21];
				int lenbuf = oper->GetPromptMessage(TYPE_EnterFunction,efmsg,21);
				if( lenbuf == 0 )
				{
					user_output_write(2, 1, "   ENTER FUNCTION    ");
				}
				else
				{
					int col = ( 20 - strlen(efmsg) ) / 2 + 1;
					user_output_write(2, col, efmsg);
				}

				fflush(stdout);
				int timeout = oper->GetFunctionTimeout();
				for(;;)
				{
					int tm = 0;
					while (!user_input_ready () && tm < timeout)
					{
						usleep(10000);
						tm += 20;
					}
					if(tm >= timeout)
					{
						oper->Beep(TYPE_TimeOut);
						return SC_TIMEOUT;
					}
					Function = user_input_get();
					if( Function == ESC )
					{
						oper->Beep(TYPE_KeyPress);
						return SC_TIMEOUT;
					}
					else if(Function == '<')
					{
						oper->Beep(TYPE_KeyPress);
						code = DisplayTrans();
						if(code == SC_Success || code == SC_Clear)
						{
							function_read = false;
							goto FunctionRead;
						}
						else
						return code;
					}
					else if( Function >= 'A' && Function <= 'L' ) break;

					else
					{
						oper->Beep(100, TYPE_Reject);
					}
				}
			}

			/**
			* Conversion from TA7000 Function Keys
			* to Legacy Function Keys
			*/
//testing
//			printf("function is %c\n", Function);
//endtest
			switch(Function)
			{
				case 'A':
					Function = '1';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'B':
					Function = '2';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'C':
					Function = '3';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'D':
					Function = '4';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'E':
					Function = '5';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'F':
					Function = '6';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'G':
					Function = '7';
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					EnableSwipAndGo = true;
					break;
				case 'H':
					Function = '8';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'I':
					Function = '9';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'J':
					Function = '*';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'K':
					Function = '0';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'L':
					Function = '#';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case ESC:
					return SC_Clear;
			}
			char empmsg[32];
			oper->GetEmployeeMessage(empbadge, Function,empmsg,32);
			if(empmsg != NULL && strlen(empmsg) != 0)
			{
				user_output_write(2, 1, BLANK_LINE);
				int col = ( 20 - strlen(empmsg) ) / 2 + 1;
				user_output_write(1, col, empmsg);
				//printf("function = %s\n",empmsg);
				usleep(oper->GetMessageTimeout() * 1000);
			}

		#else
			//printf("%s : ", oper->GetPromptMessage(TYPE_EnterFunction));
			//scanf("%c", &Function);
		#endif


		if(code == SC_TIMEOUT || code == SC_Clear) return code;
        if(code == SC_Invalid_Function )
		{

			#ifdef __TA7700_TERMINAL__
        	    char tamsg[21];
        	    int lenbuf = oper->GetPromptMessage(TYPE_Try_Again,tamsg,21);
				user_output_write(2, 1, "   NOT AVAILABLE    ");
				if(lenbuf != 0)
				{
					int col = ( 20 - lenbuf ) / 2 + 1;
					user_output_write(3, 1, BLANK_LINE);
					user_output_write(3, col, tamsg);
					oper->TurnOnLEDYellow();
					oper->Beep(TYPE_Error);
					usleep(oper->GetErrorMsgTimeout() * 1000);
					oper->TurnOffLEDYellow();
				}
				else
				{
					oper->TurnOnLEDYellow();
					user_output_write(3, 1, "  PLEASE TRY AGAIN  ");
					oper->Beep(TYPE_Error);
					usleep(oper->GetErrorMsgTimeout() * 1000);
					oper->TurnOffLEDYellow();
				}
			#else
				//printf("%s\n", oper->GetPromptMessage(TYPE_Try_Again));
			#endif
			function_read = false;
			goto FunctionRead;
		}
		else if(code == SC_Supervisor_Function)
		{
			#ifdef __TA7700_TERMINAL__
			    char tamsg[21];
			    int lenbuf = oper->GetPromptMessage(TYPE_Try_Again,tamsg,21);
				user_output_write(2, 1, "   NOT AVAILABLE    ");
				if(lenbuf != 0)
				{
					int col = ( 20 - lenbuf ) / 2 + 1;
					user_output_write(3, 1, BLANK_LINE);
					user_output_write(3, col, tamsg);
					oper->TurnOnLEDYellow();
					oper->Beep(TYPE_Error);
					usleep(oper->GetErrorMsgTimeout() * 1000);
					oper->TurnOffLEDYellow();
				}
				else
				{
					user_output_write(3, 1, "  PLEASE TRY AGAIN  ");
					oper->TurnOnLEDYellow();
					oper->Beep(TYPE_Error);
					usleep(oper->GetErrorMsgTimeout() * 1000);
					oper->TurnOffLEDYellow();
				}
			#else
				//printf("NOT AVAILABLE\n");
				//printf("%s\n", oper->GetPromptMessage(TYPE_Try_Again));
			#endif

			function_read = false;
			goto FunctionRead;
		}
		else if(code == SC_Allow_Access)
		{
			#ifdef __TA7700_TERMINAL__
				IncrementAccessCount(oper->GetAccessDuration());
				code = SC_Allow;
			#endif
		}
		function_read = false;
        return code;
}


/**
 *  force Function to GetActivityCode
 */
OperationStatusCode OperationIODriver::ForceActivityCodeFunction(BadgeType type)
{
	OperationStatusCode code = SC_Allow;
	FunctionRead:

			Function = 'B'; // force F2 key.
							//
							// F1 = swipe n go
							// F2 = swipe and get activity code
							//

			function_read = false;

//testing
//			printf("function is %c\n", Function);
//endtest
			switch(Function)
			{
				case 'A':
					Function = '1';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'B':
					Function = '2';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'C':
					Function = '3';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'D':
					Function = '4';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'E':
					Function = '5';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'F':
					Function = '6';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'G':
					Function = '7';
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					EnableSwipAndGo = true;
					break;
				case 'H':
					Function = '8';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'I':
					Function = '9';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'J':
					Function = '*';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'K':
					Function = '0';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case 'L':
					Function = '#';
					EnableSwipAndGo = true;
					code = oper->ValidateFunction(empbadge, Function, type, levels, flags);
					break;
				case ESC:
					return SC_Clear;
			}
			char empmsg[32];
			oper->GetEmployeeMessage(empbadge, Function,empmsg,32);
			if(empmsg != NULL && strlen(empmsg) != 0)
			{
				user_output_write(2, 1, BLANK_LINE);
				int col = ( 20 - strlen(empmsg) ) / 2 + 1;
				user_output_write(1, col, empmsg);
				//printf("function = %s\n",empmsg);
				usleep(oper->GetMessageTimeout() * 1000);
			}


		if(code == SC_TIMEOUT || code == SC_Clear) return code;
        if(code == SC_Invalid_Function )
		{

			#ifdef __TA7700_TERMINAL__
        	    char tamsg[21];
        	    int lenbuf = oper->GetPromptMessage(TYPE_Try_Again,tamsg,21);
				user_output_write(2, 1, "   NOT AVAILABLE    ");
				if(lenbuf != 0)
				{
					int col = ( 20 - lenbuf ) / 2 + 1;
					user_output_write(3, 1, BLANK_LINE);
					user_output_write(3, col, tamsg);
					oper->TurnOnLEDYellow();
					oper->Beep(TYPE_Error);
					usleep(oper->GetErrorMsgTimeout() * 1000);
					oper->TurnOffLEDYellow();
				}
				else
				{
					oper->TurnOnLEDYellow();
					user_output_write(3, 1, "  PLEASE TRY AGAIN  ");
					oper->Beep(TYPE_Error);
					usleep(oper->GetErrorMsgTimeout() * 1000);
					oper->TurnOffLEDYellow();
				}
			#else
				//printf("%s\n", oper->GetPromptMessage(TYPE_Try_Again));
			#endif

			function_read = false;
			return SC_Clear;
		}
		else if(code == SC_Supervisor_Function)
		{
			#ifdef __TA7700_TERMINAL__
			    char tamsg[21];
			    int lenbuf = oper->GetPromptMessage(TYPE_Try_Again,tamsg,21);
				user_output_write(2, 1, "   NOT AVAILABLE    ");
				if(lenbuf != 0)
				{
					int col = ( 20 - lenbuf ) / 2 + 1;
					user_output_write(3, 1, BLANK_LINE);
					user_output_write(3, col, tamsg);
					oper->TurnOnLEDYellow();
					oper->Beep(TYPE_Error);
					usleep(oper->GetErrorMsgTimeout() * 1000);
					oper->TurnOffLEDYellow();
				}
				else
				{
					user_output_write(3, 1, "  PLEASE TRY AGAIN  ");
					oper->TurnOnLEDYellow();
					oper->Beep(TYPE_Error);
					usleep(oper->GetErrorMsgTimeout() * 1000);
					oper->TurnOffLEDYellow();
				}
			#else
				printf("NOT AVAILABLE\n");
				//printf("%s\n", oper->GetPromptMessage(TYPE_Try_Again));
			#endif

			function_read = false;
			goto FunctionRead;
		}
		else if(code == SC_Allow_Access)
		{
			#ifdef __TA7700_TERMINAL__
				IncrementAccessCount(oper->GetAccessDuration());
				code = SC_Allow;
			#endif
		}
		function_read = false;
        return code;
}


/**
 * Increments the access count
 * When a user needs to be given access
 * this function is called with the access duration
 */
void OperationIODriver::IncrementAccessCount(int count)
{
	pthread_mutex_lock(&lockAccessCount);
	accessCount += count;
	pthread_mutex_unlock(&lockAccessCount);
}

/**
 * Decrements the access count
 */
void OperationIODriver::DecrementAccessCount(int count)
{
	pthread_mutex_lock(&lockAccessCount);
	accessCount -= count;
	pthread_mutex_unlock(&lockAccessCount);
}

/**
 * Opens the Latch for the specified duration
 * this thread keeps checking the access count
 * When the access count is greater than zero it switches
 * on the digital IO 2. When the count becomes zero it switches it off
 */
void * OperationIODriver::AllowAccess(void * id)
{
  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
	bool onState = false;
	while(true)
	{
		if(accessCount > 0)
		{
			if(!onState)
			{
				#ifdef DEBUG
					printf("Allowing Acccess\n");
				#endif
				onState = true;
				user_digital_set (2, ON);
			}
			DecrementAccessCount(1);
		}
		else
		{
			if(onState)
			{
				#ifdef DEBUG
					printf("Closing Acccess\n");
				#endif
				onState = false;
				user_digital_set (2, OFF);
			}
		}
		usleep(1000000);
	}
}

/**
 * Monitors Date Change in the System
 * Records a Transaction whenever the date changes
 */

//void * OperationIODriver::MonitorDateChange(void * id)
//{
//	Operation oper;
//	int lastDate = oper.GetDayOfMonth();
//	int curDate = lastDate;

//	while(true)
//	{
//		// Check if the Date changed
//		if( curDate != lastDate )
//		{
//			lastDate = curDate;
//			oper.CreateDateTimeChangeTransaction();
//		}
//		usleep(1000000); // Sleep for 1 second
//		//usleep(60000000); // Sleep for 1 minute
//		curDate = oper.GetDayOfMonth();
//	}
//}

// Monitors Bell Schedule - Checks every second
//void *OperationIODriver::RunBellSchedule(void * id)
//{
//  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
//	Operation * oper = new Operation();
//	/**
//	 * Check for bell schedules every minute
//	 */
//	while(true)
//	{
//		oper->CheckBellSchedule();
//		usleep(1000000); // Sleep for 1 second
//		//usleep(60000000); // Sleep for 1 minute
//	}
//}
//void *OperationIODriver::RunHeartBeat(void * id)
//{
//  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
//	//Operation * oper = new Operation();
//	//CURLcode res = (CURLcode)1;
//	/**
//	 * Check for bell schedules every minute
//	 */
//	//system("udhcpc");
//  /*  char HearBeatFreqStr[15];
//	pthread_mutex_lock (&xml_parser_mutex);
//	getWeb->GetProperty("WebService",LinkWeb);
//	getWeb->GetProperty("Account",Account);
//	getWeb->GetProperty("ClockId",ClockId);
//	getWeb->GetProperty("HeartBeatTimer",HearBeatFreqStr);
//	pthread_mutex_unlock (&xml_parser_mutex);
//    int firstTimeServer = 1;
//	int firstTimes = 0;
//	HearBeatFreqStr[0] = '\0';
//	int freq ;
//    struct timespec clockTime;
//    int condRes;
//	while(true)
//    {

//		res = (CURLcode)1;

//		if(strlen(HearBeatFreqStr) > 0)  freq = atol(HearBeatFreqStr);
//		else freq = atol(HEARTBEAT_TIMER_DEFAULT);

//		//printf("%s\n%s\n%s\n%s\n",LinkWeb,Account,ClockId,HearBeatFreqStr);
//		HearBeatFreqStr[0] = '\0';



//		time_t optime;
//		struct tm * optimeinfo;
//		time ( &optime );
//		optimeinfo = localtime ( &optime );

//		char dateB[22];
//		char timeB[22];
//		dateB[0] = '\0';
//        timeB[0] = '\0';
//		strftime(timeB,21,"T%H:%M:%S",optimeinfo);
//		strftime(dateB,21,"%Y-%m-%d", optimeinfo);

//        char utcTime[50];
//		utcTime[0] = '\0';
//        strcpy(utcTime,dateB);
//		strcat(utcTime,timeB);
//		if(strcmp(LinkWeb,httpS->getURL()))
//		{
//		  printf("URL CHANGED\n");
//		  printf("URL1= %s\nURL2= %s\n",LinkWeb,httpS->getURL());
//		  httpS->setURL(LinkWeb);
//		}

//again:  pthread_mutex_lock (&soap_request_mutex);
//		httpS->HeartBeat(ClockId,Account,utcTime,SOAP_1_1_VER,&res);

//        XMLProperties parser;
//		char status[2];status[0] = '\0';
//	    if(!res)
//		{
//			//printf("request - > %s\n\n",httpS->smg->xml);
//	        //printf("_______________________________________________________________\n\n");
//		    //printf("response -> %s\n",httpS->chunk.memory);
//			parser.GetProperty(httpS->chunk.memory,"Status",status);
//			if(strlen(status) > 0)
//			{
//				if(strcmp(status,"1") == 0)
//				{
//					firstTimes = 3;communication = true; pthread_cond_signal (&offline_cond_flag);
//					if(firstTimeServer){pthread_cond_signal(&getservertime_cond_flag); firstTimeServer = 0; }
//				}
//				else
//					communication = false;
//			}
//			else
//			 communication = false;
//		}
//        else
//		{
//			if(firstTimes < 3 && (httpS->getTimeOut())<16){firstTimes++ ;pthread_mutex_unlock (&soap_request_mutex);sleep(1);goto again;}
//			communication = false;
//		}
//		//pthread_mutex_unlock (&soap_request_mutex);
//		//printf("comm = %d res = %d\n",communication,res);
//		//usleep(freq); // Sleep for 10 second
//		clock_gettime(CLOCK_REALTIME, &clockTime);
//		clockTime.tv_sec += freq ;
//		condRes = pthread_cond_timedwait(&heartbeat_cond_flag,&soap_request_mutex,&clockTime);
//		httpS->count++;
//			if(httpS->count > 100){firstTimes = 0;httpS->reseatHttp();}
//		pthread_mutex_unlock (&soap_request_mutex);
//		//usleep(60000000); // Sleep for 1 minute
//	}
//	*/
//	return NULL;
//}

//void *OperationIODriver::RunGetProjectTasks(void * id)
//{
//   printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
////   CURLcode res = (CURLcode)1;
////   SqliteDB sqlitedb;
////   /**
////	 * Check for bell schedules every minute
////	 */

//// /*  char GetProjectTasksFreqStr[30] ;
////   GetProjectTasksFreqStr[0] = '\0';
////   int freq ;
////   struct timespec clockTime;


////   while(true)
////   {
////	res = (CURLcode)1;
////	int dbRes;
////	//HttpClient httpGetProjects(LinkWeb,atol(timeStr),useProxyBool,proxy,proxyPort,useProxyAuthBool,proxyUserName,proxyPassword);
////    XMLProperties parser(XML_PROPERTIES_FILE);
////	OffLineTranRecord R("GetProjectTasks");
////	parser.GetProperty("GetProjectTasksTimer",GetProjectTasksFreqStr);
////	if(GetProjectTasksFreqStr)  freq = atol(GetProjectTasksFreqStr);
////	else freq = atol(GETPROJECTTASKS_TIMER_DEFAULT);

////	//printf("GetProjectTasksFreqStr = %s\n",GetProjectTasksFreqStr);
////	pthread_mutex_lock (&get_projects_mutex);
////	reqCount++;
////	if(communication)
////	{
////		//printf("RunGetProjectTasks -> connect state");
////		httpGetProjects->GetProjectTasks(ClockId,Account,SOAP_1_1_VER,&res);
////		httpGetProjects->count++;
////		if(!res && httpGetProjects->chunk.memory)
////		{
////		  //printf("xml projects accepted\n");
////		  if(UpdateProjectTasksInDB(httpGetProjects->chunk.memory) == 1)
////			if((sqlitedb.offlineTran_tbl.FindRowCount("TRANSACTIONS = 'GetProjectTasks'",&dbRes)) && dbRes == SQLITE_OK)
////			  if(sqlitedb.offlineTran_tbl.DeletRows(&R) == SQLITE_OK )
////				sem_wait (&offline_tran_count);
////		  //printf("xml projects saved in DB\n");
////		}
////		else
////		{
////           //printf("RunGetProjectTasks -> connect state, saved in offline mode\n");
////		   if((!sqlitedb.offlineTran_tbl.FindRowCount("TRANSACTIONS = 'GetProjectTasks'",&dbRes)) && dbRes == SQLITE_OK)
////			if(SaveTransaction(&R,GETPROJECTTASKS_FAILED)==1)
////			{
////				/*printf("befor sem_post getprojects\n");*/
////				/*sem_post(&offline_tran_count);
////				/*printf("after sem_post	  getproject\n");*/
////			/*}
////		}
////        httpGetProjects->GetActivities(ClockId,Account,SOAP_1_1_VER,&res);
////		httpGetProjects->count++;
////		if(!res && httpGetProjects->chunk.memory)
////		{
////		  //printf("xml activities accepted\n");
////		  if(UpdateActitvitiesInDB(httpGetProjects->chunk.memory) == 1)
////			if((sqlitedb.offlineTran_tbl.FindRowCount("TRANSACTIONS = 'GetActivities'",&dbRes)) && dbRes == SQLITE_OK)
////			  if(sqlitedb.offlineTran_tbl.DeletRows(&R) == SQLITE_OK )
////				sem_wait (&offline_tran_count);
////		 // printf("xml activities saved in DB\n");
////		}
////		else
////		{
////           printf("GetActivities Failed -> connect state\n");
////		   //if((!sqlitedb.offlineTran_tbl.FindRowCount("TRANSACTIONS = 'GetProjectTasks'",&dbRes)) && dbRes == SQLITE_OK)
////			//if(SaveTransaction(&R,GETPROJECTTASKS_FAILED)==1)
////			//{printf("befor sem_post getprojects\n");sem_post(&offline_tran_count);printf("after sem_post getprojects\n");}
////		}
////	    pthread_mutex_unlock(&get_projects_mutex);
////		//GetProjectTasksFreqStr = NULL;

////		clock_gettime(CLOCK_REALTIME, &clockTime);
////		clockTime.tv_sec += freq ;
////		pthread_cond_timedwait(&getprojects_cond_flag,&get_projects_mutex,&clockTime);
////		if(httpGetProjects->count > 200) httpGetProjects->reseatHttp();
////		pthread_mutex_unlock(&get_projects_mutex);
////		continue;
////	}
////	else
////	{
////		//printf("RunGetProjectTasks -> dissconnect state ,saved in offline mode\n");
////		if((!sqlitedb.offlineTran_tbl.FindRowCount("TRANSACTIONS = 'GetProjectTasks'",&dbRes)) && dbRes == SQLITE_OK)
////			if(SaveTransaction(&R,GETPROJECTTASKS_FAILED) == 1)
////			{
////				//printf("befor sem_post getprojects\n");
////				sem_post(&offline_tran_count);
////				//printf("after sem_post getprojects\n");
////			}

////		clock_gettime(CLOCK_REALTIME, &clockTime);
////	    clockTime.tv_sec += freq ;
////	    pthread_cond_timedwait(&getprojects_cond_flag,&get_projects_mutex,&clockTime);
////	    pthread_mutex_unlock(&get_projects_mutex);
////	}
////   }

//   return NULL;
//}
//void *OperationIODriver::RunGetServerTime(void * id)
//{
//  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
//	return NULL;
//}

//void *OperationIODriver::RunWatchDogThread(void * id)
//{
//  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
//  //mkfifo("/var/tmp/WatchDog", O_RDWR );
//  //int pipe = open("/var/tmp/WatchDog", O_WRONLY);
//  //int flag = 0;

//  for(;;)
//  {
//    //printf("befor write\n");
//	//write (pipe,"C", 1);
//    //printf("after write\n");

//	sleep(20);
//	pthread_mutex_lock( &lockDisplay );
//	user_output_clear();
//	int col = ( 20 - strlen("ARRANGE MEMORY") ) / 2 + 1;
//	int com1 = ( 20 - strlen("PLEASE WAIT...") ) / 2 + 1;
//	user_output_write(2, col, "ARRANGE MEMORY");
//	user_output_write(3, com1, "PLEASE WAIT...");
//	//if(!flag){system("./WatchDogAppl &"); flag = 1;}
//	system("./ResetApplication.sh");
//    pthread_mutex_unlock( &lockDisplay );
//  }
//	return NULL;
//}

//void *OperationIODriver::RunOffLineSender(void * id)
//{
//    printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
////    int row=0,col=0;
////	SqliteDB sqlitedb;
////	char** res;

////   while(true)
////   {

////	if(communication)
////	{
////		//printf("OffLineMode befor sem_wait\n");
////		sem_wait (&offline_tran_count);
////		reqCount++;
////		//printf("connected OffLineMode send from DB\n");
////		if(!communication){sem_post (&offline_tran_count); continue;}
////		res = sqlitedb.offlineTran_tbl.SelectTable(NULL,10,&row,&col);
////		//printf("row = %d col = %d \n",row,col);
////		for(int i =1 ; i < (row+1) ;i++)
////		{
////			if(strcmp("ClockIn",res[i*col]) == 0)
////			{
////				//printf("Try Send Offline ClockIn date = %s badge = %s\n",res[i*col+1],res[i*col+2]);
////				CURLcode resHttp;

////				XMLProperties parser;
////	            OffLineTranRecord R(res[i*col],res[i*col+1],res[i*col+2],res[i*col+3],res[i*col+4],res[i*col+5],
////					                res[i*col+6],res[i*col+7]);
////				httpOffline->ClockIn(ClockId,Account,res[i*col+2],res[i*col+1],res[i*col+7],res[i*col+3],
////					                 res[i*col+4],res[i*col+5],res[i*col+6],SOAP_1_1_VER,&resHttp);
////				//if(!resHttp)printf("response = %s\n",httpOffline->chunk.memory);
////				if(!resHttp)
////				{
////					char status[2]; status[0] = '\0';
////					parser.GetProperty(httpOffline->chunk.memory,"Status",status);
////					if(strlen(status) > 0 )
////					{
////						if(sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
////						{sem_post (&offline_tran_count); /*printf("failed to delete row\n");*//*}
////						//else
////						//printf("row deleted\n");
////					/*}
////					else
////					{
////						//exception state
////						if(sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
////						{sem_post (&offline_tran_count);}
////					}
////				}
////				else
////				{sem_post (&offline_tran_count);/*printf("failed send ClockIn Offline Tran\n");*//*}
////				//pthread_mutex_unlock (&soap_request_mutex);
////			/*}
////			if(strcmp("GetProjectTasks",res[i*col]) == 0)
////			{
////			  //printf("Try Send Offline GetProjectTasks\n");
////			  CURLcode resHttp;
////              OffLineTranRecord R(res[i*col]);
////			  pthread_cond_signal(&getprojects_cond_flag);
////              if(sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
////				{sem_post (&offline_tran_count); /*printf("failed to delete GETPROJECTS row from offline DB\n");*//*}
////			  //else
////		        // printf("GETPROJECTS rows deleted from offline DB\n");
////			/*}
////			if((strcmp("GetActivities",res[i*col]) == 0) || (strcmp("GetGroups",res[i*col]) == 0) || (strcmp("GetProjectTasks",res[i*col]) == 0))
////			{
////			}
////			if(strcmp("InPunch",res[i*col]) == 0)
////			{
////				//printf("Try Send Offline InPunch date = %s badge = %s\n",res[i*col+1],res[i*col+2]);
////				CURLcode resHttp;

////				XMLProperties parser;
////				//printf("111111\n");
////				OffLineTranRecord R(res[i*col],res[i*col+1],res[i*col+2],res[i*col+3]);
////                //printf("222222  %s %s\n",ClockId,Account);
////				//pthread_mutex_lock (&soap_request_mutex);
////				httpOffline->InPunch(ClockId,Account,res[i*col+2],res[i*col+1],res[i*col+3],SOAP_1_1_VER,&resHttp);
////				//printf("333333\n");
////				//if(!resHttp)printf("response = %s\n",httpOffline->chunk.memory);
////				//printf("444444\n");
////				if(!resHttp)
////				{
////					char status[2]; status[0] = '\0';
////					parser.GetProperty(httpOffline->chunk.memory,"Status",status);
////					//printf("444444a\n");
////					if(strlen(status) > 0 )
////					{
////						if(sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
////						{sem_post (&offline_tran_count);/*printf("failed to delete row\n");*//*}
////						//else
////						//printf("row deleted\n");
////					/*}
////					else
////					{
////						//exception state
////						if(sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
////						{sem_post (&offline_tran_count);}
////					}
////				}
////				else
////				{sem_post (&offline_tran_count);/*printf("failed send Offline Tran\n");*//*}
////				//pthread_mutex_unlock (&soap_request_mutex);
////			}
////			if(strcmp("OutPunch",res[i*col]) == 0)
////			{
////				//printf("Try Send Offline OutPunch\n");
////				CURLcode resHttp;
////				XMLProperties parser;
////				OffLineTranRecord R(res[i*col],res[i*col+1],res[i*col+2]);

////				//pthread_mutex_lock (&soap_request_mutex);
////				httpOffline->OutPunch(ClockId,Account,res[i*col+2],res[i*col+1],SOAP_1_1_VER,&resHttp);
////				//if(!resHttp)printf("response = %s\n",httpOffline->chunk.memory);
////				if(!resHttp)
////				{
////					char status[2]; status[0] = '\0';
////					parser.GetProperty(httpOffline->chunk.memory,"Status",status);
////					if(strlen(status) > 0)
////					{
////						if(sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
////						{sem_post (&offline_tran_count); /*printf("failed to delete row\n");*//*}
////						//else
////							//printf("row deleted\n");
////					}
////					else
////					{
////						//exception state
////						if(sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
////						{sem_post (&offline_tran_count);}
////					}
////				}
////				else
////				{sem_post (&offline_tran_count);/*printf("failed send Offline Tran\n");*//*}
////				//pthread_mutex_unlock (&soap_request_mutex);
////			}
////			if(strcmp("SwipePunch",res[i*col]) == 0)
////			{
////				//printf("Try Send Offline SwipePunch1\n");
////				CURLcode resHttp;
////				XMLProperties parser;
////				OffLineTranRecord R(res[i*col],res[i*col+1],res[i*col+2]);

////				//pthread_mutex_lock (&soap_request_mutex);
////				//printf("Try Send Offline SwipePunch2 utcTime = %s\n",res[i*col+1]);
////				httpOffline->SwipePunch(ClockId,Account,res[i*col+2],res[i*col+1],SOAP_1_1_VER,&resHttp);
////				//printf("Try Send Offline SwipePunch3 utcTime = %s\n",res[i*col+1]);
////				//if(!resHttp)printf("response = %s\n",httpOffline->chunk.memory);
////				//printf("Try Send Offline SwipePunch4\n");
////				if(!resHttp)
////				{
////					char status[2]; status[0] = '\0';
////					//printf("Try Send Offline SwipePunch5\n");
////					parser.GetProperty(httpOffline->chunk.memory,"Status",status);
////					//printf("Try Send Offline SwipePunch6 status = %s\n",status);
////					if(strlen(status) > 0)
////					{
////						//printf("Try Send Offline SwipePunch7\n");
////						if(sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
////						{sem_post (&offline_tran_count); /*printf("failed to delete row\n");*//*}
////						//else
////							//printf("row deleted\n");
////					}
////					else
////					{
////						//exception state
////						if(sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
////						{sem_post (&offline_tran_count);}
////					}
////				}
////				else
////				{sem_post (&offline_tran_count);/*printf("failed send Offline Tran\n");*//*}
////			}
////			if(strcmp("Validate",res[i*col]) == 0)
////			{
////			}
////			if(!communication){/*pthread_mutex_unlock (&soap_request_mutex);*//*break;}
////			if(i < row)sem_wait (&offline_tran_count);
////			//pthread_mutex_unlock (&soap_request_mutex);
////			httpOffline->count++;
////			if(httpOffline->count > 100){httpOffline->reseatHttp();}
////			sleep(2);
////            if(reqCount >= 3000)
////			{
////				  //system("echo | date >> reset.txt");
////				  user_output_clear();
////				  int col = ( 20 - strlen("ARRANGE MEMORY") ) / 2 + 1;
////				  user_output_write(2, col, "ARRANGE MEMORY");
////				  user_output_write(3, 1, "   PLEASE WAIT...   ");
////				  //if(!flag){system("./WatchDogAppl &"); flag = 1;}
////				  system("./ResetApplication.sh");
////		   }
////		}
////    }
////	else
////	{
////		//printf("Disconnected OffLineMode\n");
////		pthread_mutex_lock (&offline_request_mutex);
////        pthread_cond_wait (& offline_cond_flag, &offline_request_mutex);
////	    pthread_mutex_unlock (&offline_request_mutex);
////	}
//// }
//	return NULL;
//}

/**
 * Opens the latch during always open access window
 */
//void *OperationIODriver::CheckAccessWindow(void * id)
//{
//  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
//	Operation oper;
//	bool open = false;
//	while(true)
//	{
//		if(oper.CheckIfInAccessWindow())
//		{
//			if(!open)
//			{
//				open = true;
//				printf("windows open\n");
//				user_digital_set (2, ON);
//			}
//		}
//		else
//		{
//			if(open)
//			{
//				open = false;
//				printf("windows closed\n");
//				user_digital_set (2, OFF);
//			}
//		}
//		//usleep(60000000);
//		usleep(1000000);
//		//printf("ACCESS thREAD WAKEUP\n");
//	}
//}


extern bool get_server_time_flag;
/**
 * Displays the Idle Screen and Updates it every second
 */
void *OperationIODriver::ShowIdleScreen(void * id)
{
  char ip[21] ;
  int len = 0;
  int tempLen = 0;
  int indx = 0;
  static char ipline[21];

  const char* tmpmsg;


  char com[2];

  time_t start_time = time ( 0 );
  //XMLProperties parser(XML_PROPERTIES_FILE);

  Operation * oper = new Operation();

  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());

	//
	// choice: 	do this for every iteration of the loop, or just once before loop begins.
	// 			doing this here, the user can 'force' the clock to (edit properties file)
	//			resync localtime without resetting
	//			otherwise, a reset would need to be done
	// just do it once, and require a reset.


	LOCKXM;

// should be only done in ::ctor
//		XMLProperties *parser1 = new XMLProperties(XML_PROPERTIES_FILE);
//		parser1->GetProperty("UpdateTimeClock",updateTimeClock);
//		delete parser1;

		if(strlen(updateTimeClock))
		{
			// nothing
			printf("UpdateTimeClock is %s\n", updateTimeClock);
		}
		else
		{
			strcpy(updateTimeClock, "02:00:00"); // default.
			printf("undefined. set UpdateTimeClock is %s\n", updateTimeClock);
		}


// this should be in the constructor
//		XMLProperties *parser2 = new XMLProperties(XML_PROPERTIES_FILE);
//		parser2->GetProperty("PopulateTemplatesTime",populateTemplatesTime);
//		delete parser2;
		if(strlen(populateTemplatesTime))
		{
			// nothing
			printf("PopulateTemplatesTime is %s\n", populateTemplatesTime);
		}
		else
		{
			strcpy(populateTemplatesTime, "04:00:00"); // default.
			printf("undefined. set PopulateTemplatesTime is %s\n", populateTemplatesTime);
		}

	UNLOCKXM;



	user_output_clear();
  while(true)
  {
      time_t t = time ( 0 );
      while ( t == time ( 0 )){
	    usleep ( 100000 );
		//usleep ( 80000 );
      }


      //daylight savings time adjustment stuff....
      LegacySetup ::d_s_a_t* adj = & SystemSetup .daylight_savings_adjustment ;
      if ( SystemSetup.clock. daylight_savings ){
	    //foward adjustment:
	    if ( start_time < adj ->forward && t >= adj ->forward ){
	        start_time = t ;
	        adjust_time ( 60 * adj ->skip_min );
	    }

	    //backward adjustment
	    if ( start_time < adj ->backward && t >= adj ->backward ){
	        start_time = t ;
	        adjust_time ( - 60 * adj ->skip_min );
	    }
      }
      pthread_mutex_lock( &lockDisplay );

      len = oper->GetPromptMessage(TYPE_Idle,ip,21);

      if(len == 0)
	  {
    	 snprintf(ipline,21,"%s",BLANK_LINE);
		 //if(TurnOnSwipAndGo && EnableSwipAndGo){ipline[17] = 'S'; ipline[18] = '&';ipline[19] = 'G';}//not necessary in this appl
		 user_output_write(1, 1, ipline);
	  }
	  else
	  {
		snprintf(ipline,21,"%s",BLANK_LINE);

		indx = ( 20 - len ) / 2;
		tempLen = len;
		if( len > (21 - indx )) tempLen = indx; //21 = length of ipline ,

		if(indx< 21)snprintf(ipline + indx,tempLen,"%s",ip);
		//if(TurnOnSwipAndGo && EnableSwipAndGo){ipline[17] = 'S'; ipline[18] = '&';ipline[19] = 'G';} //not necessary in this appl
		user_output_write(1, 1, ipline);
      }

      //temporary
      //replace this:
      //      user_output_write(2, 1, BLANK_LINE);
      //with this:
      // orig: const char* tmpmsg = g_tmpmsg ; //not clean. Should be guarded. Two threads involved.

      tmpmsg = g_tmpmsg ; //not clean. Should be guarded. Two threads involved.

	  if ( 0 == tmpmsg )
	  {
		  user_output_write(2, 1, BLANK_LINE);
      }
	  else
	  {
		  int len1 = 0;
		  snprintf(ipline,21,"%s",BLANK_LINE);
		  len1 =  strlen ( tmpmsg );
		  indx = ( 20 - len1 ) / 2;
		  tempLen = len1;
		  if( len1 > (21 - indx) ) tempLen = indx; //21 = length of ipline ,
		  if(indx<21)snprintf(ipline + indx,tempLen,"%s",tmpmsg);
		  user_output_write ( 2 , 1 , ipline );
      }
      //end replace

      user_output_write(3, 1, oper->GetTimeForIdleDisplay());
      user_output_write(4, 1, oper->GetDateForIdleDisplay());

      int updateTimeLen = strlen(updateTimeClock);
	  char hour[3],min[3],sec[3];
	  hour[0]='\0';min[0]='\0';sec[0]='\0';
	  snprintf(hour,3,"%s",updateTimeClock);
	  if(updateTimeLen>2)snprintf(min,3,"%s",updateTimeClock+3);
	  if(updateTimeLen>5)snprintf(sec,3,"%s",updateTimeClock+6);

	  int hh,mm,ss;
	  hh = atoi(hour);mm = atoi(min);ss = atoi(sec);
	  if(oper->localtimeinfo.tm_hour == hh && oper->localtimeinfo.tm_min == mm && oper->localtimeinfo.tm_sec == ss )
	  {
		  DLOGI("signal the GetServerTime thread to call the LocalTime webservice operation");
		  DLOGI("%d:%d:%d",oper->localtimeinfo.tm_hour,oper->localtimeinfo.tm_min,oper->localtimeinfo.tm_sec);
		  //pthread_cond_signal(&getservertime_cond_flag);
		  get_server_time_flag = true;
	  }
	  if ( AllowPopulateTemplates )
	  {
	    hour[0]='\0';min[0]='\0';sec[0]='\0';
	    int popTempLen = strlen(populateTemplatesTime);
	    snprintf(hour,3,"%s",populateTemplatesTime);
	    if(popTempLen>2)snprintf(min,3,"%s",populateTemplatesTime+3);
	    if(popTempLen>5)snprintf(sec,3,"%s",populateTemplatesTime+6);

	  	hh = atoi(hour);mm = atoi(min);ss = atoi(sec);
	  	if(oper->localtimeinfo.tm_hour == hh && oper->localtimeinfo.tm_min == mm && oper->localtimeinfo.tm_sec == ss )
	  	{
            fpOpr.fpOperation = Ignore_Operation; // turn off the fpu AR & FS while updating the templates

	  		DLOGI("signal the PopulateTemplates thread to call the PopulateTemplates webservice operation");
	  		DLOGI("%d:%d:%d",oper->localtimeinfo.tm_hour,oper->localtimeinfo.tm_min,oper->localtimeinfo.tm_sec);
	  		pthread_cond_signal(&populatetemplatesthread_cond_flag);
	  	}
	  }


	  //char com[2];
	  com[0] = 'C';
	  com[1] = '\0';
      if(communication)
		  user_output_write(1,1,com);
// users did not like this: it was "too alarming"
//		if (OfflineModeIsActive)
//		{
//			user_output_write(1,1,"O");
//			user_output_write(4,1,"O");
//			user_output_write(1, 20, "O");
//			user_output_write(4, 20, "O");
//			user_output_write(2, 2, "UPDATE OFFLINE TRX")  ;
//		}
      pthread_mutex_unlock( &lockDisplay );
      t = time ( 0 );
    }
}


void OperationIODriver::WaitForStartup()
{
	int counter = 0;
	//char a[20], b[20], c[20], d[20];

	pthread_mutex_lock(&lockDisplay);
	while (communication == false)
	{
		counter++;
		user_output_clear();
		//if (a) user_output_write(1, 5, a);
		//if (b) user_output_write(2, 5, b);
		//if (c) user_output_write(3, 5, c);
		//if (d) user_output_write(4, 5, d);
		user_output_write(2,5, "Waiting for");
		user_output_write(3,5, "Heartbeat...");
//testing
		if ( (counter % 20)==0) DP("waiting for heartbeat, comm=false, counter = %d\n", counter);
//endtest
		sleep(1);
		if ( counter == 120 )		// test with 45 seconds // 2 minutes !!!! then abort
		{
			user_output_clear();
			user_output_write(2, 5, "No Heartbeat");
			if ( OfflineModeIsEnabled)
			{
				user_output_write(3, 5, "Using");
				user_output_write(4, 5, "Offline Mode");
			}
			else
			{
				//user_output_write(3, 5, "");
			}
			sleep(2);
			break;
		}
	}
	user_output_clear();
	user_output_write(3, 5, "Configuring...");
	pthread_mutex_unlock(&lockDisplay);

}



/**
 * Unlock the Display
 */
void OperationIODriver::UnlockDisplay()
{
	pthread_mutex_unlock( &lockDisplay );
}


OperationStatusCode OperationIODriver::CreateFingerprintEnrollTransaction(const char* ExternalID,
	int FingerIndex, const char* TemplateData, int PrivLevel)
{

	int len = strlen(TemplateData);
	char findex[2];

	sprintf(findex, "%d", FingerIndex);
	findex[1] = '\0';

	char plevel[2];
	sprintf(plevel, "%d", PrivLevel);
	plevel[1] = '\0';

	char transdata[576];

	strcpy(transdata, oper->GetSystemTime()); //Time
	strcat(transdata, "1"); //1 as source
	strcat(transdata, ExternalID);  //badge
	strcat(transdata, "|");  //field separator
	strcat(transdata, "+"); //function - + for template
	strcat(transdata, "@@@@@"); //mode flags 5 bytes
	strcat(transdata, "|");  //field separator

	strcat(transdata, "1"); //1 as source
	strcat(transdata, "1"); //template part number - default. Not being used anyway.
	strcat(transdata, "|");  //field separator

	strcat(transdata, "1"); //1 as source
	strcat(transdata, findex);  //fingerindex
	strcat(transdata, "|");  //field separator

	strcat(transdata, "1"); //1 as source
	strcat(transdata, TemplateData); //fingertemplate
	strcat(transdata, "|");  //field separator

	strcat(transdata, "1"); //1 as source
	strcat(transdata, plevel);    //privilege level

	len = strlen(transdata);
	transdata[len] = '\0';

	LegacyTransItem * transitem = LegacyTransItem::GetInstance();
	transitem->GetTransactionFileLock();

	Code code = transitem->ProcessData(transdata, false);
	transitem->ReleaseTransactionFileLock();
	if( code == SC_MEMORY ){ return SC_OutOfMemory; }
	return SC_Success;
}

//**********************************************************************************



void OperationIODriver::SetEnvHome()
{
	char *env1 = getenv("HOME");
//	printf("\n env1 = %s", env1);
	setenv("HOME","/mnt/flash/terminal/bin",1); //reset it
	env1 = getenv("HOME");
//	printf("\n new HOME = %s", env1);
	//this value reset is gone after the program finished
}

OperationStatusCode OperationIODriver::DisplayTrans()
{
  	OperationStatusCode code = SC_Success;
	source = SysInputSourceKeypad;

	LegacyTransItem * transitem = LegacyTransItem::GetInstance();
	TransFIFO * fifo = transitem->GetFIFO();
	TAVector<Trans *> * vec = fifo->GetAllTransForBadge(buffer);
	Trans * trans = NULL;

	if( vec->size() > 0 )
	{
		int size = vec->size();
		int i = size - 1;
		char priv_input = '\0';
		while(true)
		{
			trans = (*(*vec)[i]);
			char input;

			LCD_READ_STATE state = lcdhdlr.DisplayTransactionToEmployee(trans, oper->GetDataTimeout(), &input);
			if(state == READ_TIMEDOUT)
			{
				code = SC_TIMEOUT;
				break;
			}
			else if(state == READ_CLEAR)
			{
				code = SC_Clear;
				break;
			}

			if(input == '<')
			{
				oper->Beep(TYPE_KeyPress);
				i --;
				if( i >= 0)
				{
					priv_input = input;
					continue;
				}
				else
					code = SC_Success;
				break;
			}
			else if(input == '>')
			{
				oper->Beep(TYPE_KeyPress);
				i ++;
				if(i <= (size - 1))
				{
					priv_input = input;
					continue;
				}
				else
					code = SC_Success;
				break;
			}
			else if(input == '+')
			{
				//enroll trans.
				if(priv_input == '\0' || priv_input == '<')
				{
					i --;
					if( i >= 0)
						continue;
					else
					code = SC_Success;
				}
				else if(priv_input == '>')
				{
					i ++;
					if(i <= (size - 1))
						continue;
					else
						code = SC_Success;
				}

				break;
			}

			else if(input == ESC)
			{
				oper->Beep(TYPE_KeyPress);
				code = SC_Clear;
				break;
			}
		}
	}

	vec->clear();
	delete vec;
	vec = NULL;
	return code;
}

namespace {
  //**********************************************************************************

  //**********************************************************************************
};

int parseMessage(char* prop ,char* line1,char* line2,char* line3,char* line4)
{
  int res;
  int active = 0;
  int len = strlen(prop);

  snprintf(line1,21,"%s",prop);
  active++;
  res = len - 20;
  if(res > 20)
  {
	if(len > 19)snprintf(line2,21,"%s",prop+20);
	active++;
	res = res-20;
  }
  else
  {
	  if(len > 19)snprintf(line2,21,"%s",prop+20);
	active++;
	return active;
  }
  if(res > 20)
  {
	  if(len > 39)snprintf(line3,21,"%s",prop+40);
	active++;
	res = res-20;
  }
  else
  {
	  if(len > 39)snprintf(line3,21,"%s",prop+40);
	active++;
	return active;
  }
  if(res > 20)
  {
	  if(len > 59)snprintf(line4,21,"%s",prop+60);
	active++;
	res = res-20;
  }
  else
  {
	strcpy(line2,prop+60);
	active++;
	return active;
 }
 return active;
}


/*int OperationIODriver::SaveTransaction(OffLineTranRecord* R,SaveReason sr)
{
        int col1,col2;
        SqliteDB sqlitedb;
        Operation* oper = new Operation();
		if(sr == OFFLINE_MODE )
		{
			col1 = ( 20 - strlen("OFFLINE MODE") ) / 2 + 1;
			user_output_clear();
			user_output_write(2, col1,"OFFLINE MODE");
		}
		if(sr == SENDIND_FAILED)
		{
		    col1 = ( 20 - strlen("SEND FAILED") ) / 2 + 1;
			user_output_clear();
			user_output_write(2, col1,"SEND FAILED");
		}
		if(sr == GETPROJECTTASKS_FAILED)
		{
		  if(sqlitedb.offlineTran_tbl.InsertRow(R) == SQLITE_OK)
			  return 1;
		  else
			  return 0;
		}
		if(sqlitedb.offlineTran_tbl.InsertRow(R) == SQLITE_OK)
		{
			col2 = ( 20 - strlen("TRANSACTION SAVED") ) / 2 + 1;
		    user_output_write(3, col2,"TRANSACTION SAVED");
			oper->TurnOnLEDGreen();
			oper->Beep(TYPE_Accept);
			int mtm = oper->GetMessageTimeout();
			if(mtm > 500)
			{
				usleep(500000);
				oper->Beep(TYPE_TurnOff);
				mtm -= 500;
			}
			usleep(mtm * 1000);
			oper->Beep(TYPE_TurnOff);
			oper->TurnOffLEDGreen();
			return 1;
		}
		else
		{
		    col2 = ( 20 - strlen("TRANSACTION LOST") ) / 2 + 1;
		    user_output_write(3, col2,"TRANSACTION LOST");
			oper->TurnOnLEDYellow();
			oper->Beep(TYPE_Error);
			int mtm = oper->GetMessageTimeout();
			if(mtm > 500)
			{
				usleep(500000);
				oper->Beep(TYPE_TurnOff);
				mtm -= 500;
			}
			usleep(mtm * 1000);
			oper->Beep(TYPE_TurnOff);
			oper->TurnOffLEDYellow();
			return 0;
		}

}*/

/*int OperationIODriver::UpdateProjectTasksInDB(const char* projectsXmlList)
{
  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
  XMLProperties parser;
  int from,to,id;
  SqliteDB sqlitedb;
  list<int> savedProjects;
  list<ElmData>& projectsList = parser.getListElementData();

  from = 1; to = 10;
  int countElm;
  do
  {
    parser.GetListElements(projectsXmlList,"GetProjectTasksResult","ProjectTask",from,to);
    list<ElmData>::iterator i;
	countElm = 0;
	printf("befor for size = %d from = %d to = %d \n",projectsList.size(),from,to);
	for(i=projectsList.begin(); i != projectsList.end();)
	{
	    ProjectTaskRecord R;
        fromProjectListToRecord(projectsList,i,R);
		if(sqlitedb.projectTasks_tbl.InsertRow(&R) == SQLITE_OK)
		{
		  id = sqlitedb.projectTasks_tbl.getID(&R);
		  if(id > 0)
            savedProjects.push_back(id);
		}
		else
		 return -1;
		countElm++;
	}
	from = from + countElm;
	to = from + 9 ;
  }
  while(countElm == 10);

  int MaxID =  sqlitedb.projectTasks_tbl.getMaxID();
  valarray<int> deletedProjects(true,MaxID+1);
  list<int>::iterator itr;

  for(itr=savedProjects.begin(); itr != savedProjects.end(); ++itr)
	deletedProjects[*itr] = false;

  for(int i = MaxID ; i > 0 ; i--)
    if(deletedProjects[i])
		sqlitedb.projectTasks_tbl.DeleteRow(i);
  return 1;
}*/

/*void OperationIODriver::fromProjectListToRecord( list<ElmData>& projectsList,list<ElmData>::iterator& i,ProjectTaskRecord& R)
{
 int projectIndex = (*i).elmIndexInList;
 while(projectIndex == (*i).elmIndexInList)
 {
	if(strcmp("ProjectName",(*i).key) == 0)
		R.SetProjectName((*i).data);
	if(strcmp("ProjectCode",(*i).key) == 0)
		R.SetProjectCode((*i).data);
	if(strcmp("TaskName",(*i).key) == 0)
	    R.SetTaskName((*i).data);
	if(strcmp("TaskCode",(*i).key) == 0)
		R.SetTaskCode((*i).data);
	//printf("\n\n\n");
	//printf("%s -> %s listElmNum = %d dataElmNum = %d \n",(*i).key,(*i).data,(*i).elmIndexInList,(*i).dataIndexInElm);
	//printf("sleep 10\n");sleep(15);
	if(i == projectsList.end()) return ;
	i++;
 }
}*/

/*int OperationIODriver::UpdateActitvitiesInDB(const char* actXmlList)
{
  //printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
  XMLProperties parser;
  int from,to,id;
  SqliteDB sqlitedb;
  list<int> savedActivities;
  list<ElmData>& actList = parser.getListElementData();

  from = 1; to = 10;
  int countElm;
  do
  {
    parser.GetListElements(actXmlList,"GetActivitiesResult","Activity",from,to);
    list<ElmData>::iterator i;
	countElm = 0;
	printf("befor for size = %d from = %d to = %d \n",actList.size(),from,to);
	for(i=actList.begin(); i != actList.end();)
	{
	    ActivityRecord R;
        fromActivitiesListToRecord(actList,i,R);
		if(sqlitedb.activities_tbl.InsertRow(&R) == SQLITE_OK)
		{
		  id = sqlitedb.activities_tbl.getID(&R);
		  if(id > 0)
            savedActivities.push_back(id);
		}
		else
		 return -1;
		countElm++;
	}
	from = from + countElm;
	to = from + 9 ;
  }
  while(countElm == 10);

  int MaxID =  sqlitedb.activities_tbl.getMaxID();
  valarray<int> deletedActivities(true,MaxID+1);
  list<int>::iterator itr;

  for(itr=savedActivities.begin(); itr != savedActivities.end(); ++itr)
	deletedActivities[*itr] = false;

  for(int i = MaxID ; i > 0 ; i--)
    if(deletedActivities[i])
		sqlitedb.activities_tbl.DeleteRow(i);
  return 1;
}
*/
/*
void OperationIODriver::fromActivitiesListToRecord( list<ElmData>& actList,list<ElmData>::iterator& i,ActivityRecord& R)
{
 int actIndex = (*i).elmIndexInList;
 while(actIndex == (*i).elmIndexInList)
 {
	if(strcmp("Name",(*i).key) == 0)
		R.SetActivityName((*i).data);
	if(strcmp("Code",(*i).key) == 0)
		R.SetActivityCode((*i).data);
	//printf("\n\n\n");
	//printf("%s -> %s listElmNum = %d dataElmNum = %d \n",(*i).key,(*i).data,(*i).elmIndexInList,(*i).dataIndexInElm);
	//printf("sleep 10\n");sleep(15);
	if(i == actList.end()) return ;
	i++;
 }
}
*/

/*
OperationStatusCode OperationIODriver::getProjectTaskAndActivity(const char * badge, const SysFunctionKey source,
																   const BadgeType type, SysLevelData * levels,
																   Bit8 * flags,OperationStatusCode code)
{
  SqliteDB sqlitedb;
  char projectCode[5],taskCode[5],activityCode[5];
  projectCode[0]='\0';taskCode[0]='\0';activityCode[0]='\0';

  if(DataLevelStage[1] > -1)
    strcpy(projectCode,levels[DataLevelStage[1]].Data);
  if(DataLevelStage[2] > -1)
    strcpy(taskCode,levels[DataLevelStage[2]].Data);
  if(DataLevelStage[3] > -1)
    strcpy(activityCode,levels[DataLevelStage[3]].Data);

  if(code == SC_TIMEOUT || code == SC_Clear)return code;
  if(code == SC_Invalid_Function ) return code;

  //printf("levels data -> %s %s %s %s  \n",levels[0].Data,levels[1].Data,levels[2].Data,levels[3].Data);
  char whereProject[30] ;
  if(DataLevelStage[1] > -1 || DataLevelStage[2] > -1)
  {
	sprintf(whereProject,"PROJECTCODE = '%s' AND TASKCODE = '%s'",projectCode,taskCode);
	if(!(sqlitedb.projectTasks_tbl.FindRowCount (whereProject)))
	{
		int col1 = ( 20 - strlen("INVALID PROJECT TASK") ) / 2 + 1;
		user_output_write(2, col1,"INVALID PROJECT TASK");
		col1 = ( 20 - strlen("PLEASE TRY AGAIN") ) / 2 + 1;
		user_output_write(3, col1,"PLEASE TRY AGAIN");
		oper->TurnOnLEDYellow();
		oper->Beep(TYPE_Reject);
		int mtm = oper->GetMessageTimeout();
		if(mtm > 500)
		{
			usleep(500000);
			oper->Beep(TYPE_TurnOff);
			mtm -= 500;
		}
		usleep(mtm * 1000);
		oper->Beep(TYPE_TurnOff);
		oper->TurnOffLEDYellow();
		return SC_Try_Again;
	}
  }
  if(DataLevelStage[3] > -1)
  {
	char whereActivity[30] ;
	sprintf(whereActivity,"ACTIVITYCODE = '%s'",activityCode);
	if(!(levels[3].Valid()) || !(sqlitedb.activities_tbl.FindRowCount(whereActivity)))
	{
		int col1 = ( 20 - strlen("INVALID ACTIVITY") ) / 2 + 1;
		user_output_write(2, col1,"INVALID ACTIVITY");
		col1 = ( 20 - strlen("PLEASE TRY AGAIN") ) / 2 + 1;
		user_output_write(3, col1,"PLEASE TRY AGAIN");
		oper->TurnOnLEDYellow();
		oper->Beep(TYPE_Reject);
		int mtm = oper->GetMessageTimeout();
		if(mtm > 500)
		{
			usleep(500000);
			oper->Beep(TYPE_TurnOff);
			mtm -= 500;
		}
		usleep(mtm * 1000);
		oper->Beep(TYPE_TurnOff);
		oper->TurnOffLEDYellow();
		return SC_Try_Again;
	}
  }
return code;
}*/
