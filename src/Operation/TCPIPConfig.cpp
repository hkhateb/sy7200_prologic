#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <net/if.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "Str_Def.h"
#include "Config.h"
#include "Operation.hpp"
#include "setwep.h"
#include "SystemGlobals.h"
#include "XMLProperties.h"

/**
 * Globals
 */
#define TMP_INTERFACES     	"/mnt/flash/terminal/network/tmpinterfaces"
#define ROUTE_TABLE     	"/proc/net/route"
#define WIRELESS_STATUS     "/proc/net/wireless"
#define INTERFACE_STATUS    "/proc/net/dev"
#define DNS_RESOLV_CONF     "/home/terminal/network/resolv.conf"
#define MAX_BUFFER_SIZE 	128
#define TAB 				"\t"
#define NEWLINE				"\n"
#define WIRED_ETH_DEV		"eth0"
#define WIRELESS_ETH_DEV	"eth1"

#define CMD_CFG_SSID		"iwconfig " WIRELESS_ETH_DEV " essid "
#define CMD_CFG_GATEWAY		"route add -net default gw "
#define CMD_CFG_GATEWAY_PARAMS		" dev "
#define CMD_DEL_GATEWAY		"route del default dev "

/********************************************************************************
 * ****************** Globals for Interfaces file *******************************
 * *****************************************************************************/
#define INTERFACES_WIRED_STATIC 		"iface " WIRED_ETH_DEV " inet static"
#define INTERFACES_WIRED_DHCP   		"iface " WIRED_ETH_DEV " inet dhcp"
#define INTERFACES_WIRELESS_STATIC 		"iface " WIRELESS_ETH_DEV " inet static"
#define INTERFACES_WIRELESS_DHCP   		"iface " WIRELESS_ETH_DEV " inet dhcp"
//Static Options
#define INTERFACES_STATIC_ADDRESS		"address"
#define INTERFACES_STATIC_NETMASK		"netmask"
#define INTERFACES_STATIC_GATEWAY		"gateway"
#define INTERFACES_STATIC_BROADCAST		"broadcast"
#define INTERFACES_STATIC_NETWORK		"network"
#define INTERFACES_STATIC_POINTOPOINT	"pointopoint"
//DHCP Options
#define INTERFACES_DHCP_HOSTNAME		"hostname"
#define INTERFACES_DHCP_LEASEHOURS		"leasehours"
#define INTERFACES_DHCP_LEASETIME		"leasetime"
#define INTERFACES_DHCP_VENDOR			"vendor"
#define INTERFACES_DHCP_CLIENT			"client"

/******************************************************************
 * *************** TCP/IP Select Comm *****************************
 * ***************************************************************/
static const char * SEL_TCPIP_COMM_LINE1 = "  SELECT A TCP/IP   ";
static const char * SEL_TCPIP_COMM_LINE2 = " COMMUNICATION TYPE ";
static const char * SEL_TCPIP_COMM_MENU1 = "1-WIRED";
static const char * SEL_TCPIP_COMM_MENU2 = "2-WIRELESS";

/******************************************************************
 * ******************** TERMINAL ID *******************************
 * ***************************************************************/
static const char * CFG_WIRELESS_SSID = "     ENTER SSID     ";

/******************************************************************
 * ****************** Wireless Security ***************************
 * ***************************************************************/
static const char * CFG_WIRELESS_SEC_LINE1 = "   CONFIGURE THE    ";
static const char * CFG_WIRELESS_SEC_LINE2 = " WIRELESS SECURITY  ";
static const char * CFG_WIRELESS_SEC_MENU1 = "1-NONE";
static const char * CFG_WIRELESS_SEC_MENU2 = "2-WEP";

/******************************************************************
 * ****************** WEP Key Length ******************************
 * ***************************************************************/
static const char * CFG_WEP_KEY_LEN_HDNG = "   WEP KEY LENGTH   ";
static const char * CFG_WEP_KEY_LEN_MENU1 = "1-64 bit";
static const char * CFG_WEP_KEY_LEN_MENU2 = "2-128 bit";

/******************************************************************
 * ****************** WEP Key Data ********************************
 * ***************************************************************/
static const char * CFG_WEP_KEY_DATA_HDNG = "   ENTER WEP KEY    ";

/******************************************************************
 * *************** TCP/IP Properties ******************************
 * ***************************************************************/
static const char * CFG_TCPIP_PROPS_HDNG = " TCP/IP PROPERTIES  ";
static const char * CFG_TCPIP_PROPS_MENU1 = "1-DHCP";
static const char * CFG_TCPIP_PROPS_MENU2 = "2-MANUALLY CONFIG";

/******************************************************************
 * *************** TCP/IP Address *********************************
 * ***************************************************************/
static const char * CFG_TCPIP_ADDR_HDNG = "   TCP/IP ADDRESS   ";

/******************************************************************
 * ****************** GATEWAY Address *****************************
 * ***************************************************************/
static const char * CFG_GTWY_ADDR_HDNG  = "  GATEWAY ADDRESS   ";

/******************************************************************
 * ****************** NETMASK *************************************
 * ***************************************************************/
static const char * CFG_NETMASK_HDNG    = "      NETMASK       ";

/******************************************************************
 * ****************** DNS SERVER *************************************
 * ***************************************************************/
static const char * CFG_DNS_HDNG    = "     DNS SERVER     ";

/******************************************************************
 * ******************** TCP PORT **********************************
 * ***************************************************************/
static const char * CFG_TCP_PORT_HDNG    = "      TCP/PORT      ";

/******************************************************************
 * ******************** TERMINAL ID *******************************
 * ***************************************************************/
static const char * CFG_TERMINAL_ID_HDNG = "    TERMINAL ID     ";

static const char * WIRELESS_SETTINGS_HDNG 			= " WIRELESS SETTINGS  ";
static const char * WIRED_SETTINGS_HDNG	 			= "   WIRED SETTINGS   ";

#define MAX_DISP_CHARS_PER_LINE 20
#define LOWER_NIBBLE_MASK 0x0F
#define UPPER_NIBBLE_MASK 0xF0


OperationStatusCode TCPIPConfig::ShowSelTCPIPCommScreen()
{
	OperationStatusCode code = SC_Success;
	char input;
	int timeout = oper.GetDataTimeout();
	int curScreen = 1;

	for(;;)
	{
start:		code = SC_Success;
		curScreen = 1;
		ShowSelTCPIPCommScreen1();
		user_output_write(4, 1, CURSOR);
		while(true)
		{
			int tm = 0;
			while (!user_input_ready () && tm < timeout)
			{
				usleep(10000);
				tm += 20;
			}
			if(tm >= timeout) return SC_TIMEOUT;
			input = user_input_get ();
			switch(input)
			{
				case '1': // WIRED
					oper.Beep(50, TYPE_KeyPress);
                    code = ShowCfgTCPIPPropsScreen(TYPE_ETHERNET);
					if(code != SC_Clear) return code;
					break;
				case '2': // WIRELESS
					oper.Beep(50, TYPE_KeyPress);
					code = ShowCfgWirelessNetworkScreen();
					if(code != SC_Clear) return code;
					break;
				case '-': // Down Arrow
					if(curScreen == 1)
					{
						oper.Beep(50, TYPE_KeyPress);
						ShowSelTCPIPCommScreen2();
						curScreen = 2;
						user_output_write(4, 1, CURSOR);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					break;
				case '+': // Up Arrow
					if(curScreen == 2)
					{
						oper.Beep(50, TYPE_KeyPress);
						ShowSelTCPIPCommScreen1();
						curScreen = 1;
						user_output_write(4, 1, CURSOR);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					break;
				case '\n':
					if(curScreen == 1)
					{
						oper.Beep(50, TYPE_KeyPress);
						code = ShowCfgTCPIPPropsScreen(TYPE_ETHERNET); //WIRED
						if(code != SC_Clear) return code;
					}
					else
					{
                        oper.Beep(50, TYPE_KeyPress);
						code = ShowCfgWirelessNetworkScreen(); // WIRELESS
						if(code != SC_Clear) return code;
					}
					break;
				case ESC:
                    oper.Beep(50, TYPE_KeyPress);
					return SC_Clear;
			    break;
			    default:
			        oper.Beep(150,TYPE_Reject);
                    oper.displayErrMessage("   NOT AVAILABLE    ");
                    goto start;
                break;
			}
			if(code == SC_Clear) break;
			else if(code == SC_TIMEOUT) return code;
		}
	}
	return code;
}

void TCPIPConfig::ShowSelTCPIPCommScreen1()
{
	user_output_clear();
	user_output_write(1, 1, SEL_TCPIP_COMM_LINE1);
	user_output_write(2, 1, SEL_TCPIP_COMM_LINE2);
	user_output_write(4, 2, SEL_TCPIP_COMM_MENU1);
}

void TCPIPConfig::ShowSelTCPIPCommScreen2()
{
	user_output_clear();
	user_output_write(1, 1, SEL_TCPIP_COMM_LINE2);
	user_output_write(3, 2, SEL_TCPIP_COMM_MENU1);
	user_output_write(4, 2, SEL_TCPIP_COMM_MENU2);
}

OperationStatusCode TCPIPConfig::ShowCfgWirelessNetworkScreen()
{
	int timeout = oper.GetDataTimeout();
	int curCursorPos;
	bool initialize;
	char input;
	for(;;)
	{
start:		curCursorPos = 2;

		user_output_clear();
		user_output_write(1, 1, " CONFIGURE WIRELESS ");
		user_output_write(curCursorPos, 1, CURSOR);
		user_output_write(2, 2, "1-SSID");
		user_output_write(3, 2, "2-SECURITY");
		user_output_write(4, 2, "3-IP SETTINGS");

		initialize = false;

		while(true)
		{
			int tm = 0;
			while (!user_input_ready () && tm < timeout)
			{
				usleep(10000);
				tm += 20;
			}
			if(tm >= timeout) return SC_TIMEOUT;
			input = user_input_get ();
			switch(input)
			{
				case '1': // SSID
					oper.Beep(50, TYPE_KeyPress);
					if(ShowCfgWirelessSSIDScreen() == SC_TIMEOUT)
					{
						return SC_TIMEOUT;
					}
					initialize = true;
					break;
				case '2': // WEP
					oper.Beep(50, TYPE_KeyPress);
					if(ShowCfgWirelessSecScreen() == SC_TIMEOUT)
					{
						return SC_TIMEOUT;
					}
					initialize = true;
					break;
				case '3': // IP SETTINGS
					oper.Beep(50, TYPE_KeyPress);
					if(ShowCfgTCPIPPropsScreen(TYPE_WIRELESS) == SC_TIMEOUT)
					{
						return SC_TIMEOUT;
					}
					initialize = true;
					break;
				case '-': // Down Arrow
					if(curCursorPos < 4)
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(curCursorPos++, 1, BLANKSPACE);
						user_output_write(curCursorPos, 1, CURSOR);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					break;
				case '+': // Up Arrow
					if(curCursorPos > 2)
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(curCursorPos--, 1, BLANKSPACE);
						user_output_write(curCursorPos, 1, CURSOR);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					break;
				case '\n':
					oper.Beep(50, TYPE_KeyPress);
					switch(curCursorPos)
					{
						case 2:
							if(ShowCfgWirelessSSIDScreen() == SC_TIMEOUT)
							{
								return SC_TIMEOUT;
							}
							initialize = true;
							break;
						case 3:
							if(ShowCfgWirelessSecScreen() == SC_TIMEOUT)
							{
								return SC_TIMEOUT;
							}
							initialize = true;
							break;
						case 4:
							if(ShowCfgTCPIPPropsScreen(TYPE_WIRELESS) == SC_TIMEOUT)
							{
								return SC_TIMEOUT;
							}
							initialize = true;
							break;
					}
					break;
				case ESC:
				    oper.Beep(50, TYPE_KeyPress);
					return SC_Clear;
				default:
		            oper.Beep(150,TYPE_Reject);
                    oper.displayErrMessage("   NOT AVAILABLE    ");
                    goto start;
                break;
			}
			if(initialize) break;
		}
	}
	return SC_Success;
}

OperationStatusCode TCPIPConfig::ShowCfgWirelessSSIDScreen()
{
	user_output_clear();
	user_output_write(1, 1, CFG_WIRELESS_SSID);
	static char buffer[25];
	buffer[20] = '\0';
	int timeout = oper.GetDataTimeout();
	LCD_READ_STATE state = lcdHandler.ReadSSID(3, 1, timeout, 20, buffer);
	if(state == READ_TIMEDOUT) return SC_TIMEOUT;
	else if(state == READ_CLEAR) return SC_Clear;
	char * ssid = TrimString(buffer);

	if( ssid == NULL || strlen(ssid) == 0 )
	{
		strcpy(buffer, "any");
		ssid = buffer;
	}
	// Set the SSID
	char cmd[strlen(CMD_CFG_SSID) + 22];
	strcpy(cmd, CMD_CFG_SSID);
	strcat(cmd, ssid);
	#ifdef DEBUG
	printf("Setting SSID - %s\n", cmd);
	#endif
	system( cmd );

	XMLProperties props(XML_PROPERTIES_FILE);
	props.SetProperty(PROPERTY_ESSID, ssid);

	return SC_Success;
}

OperationStatusCode TCPIPConfig::ShowCfgWirelessSecScreen()
{
	OperationStatusCode code = SC_Success;
	char input;
	int curScreen = 1;
	int timeout = oper.GetDataTimeout();
	XMLProperties props(XML_PROPERTIES_FILE);

	for(;;)
	{
start:		code = SC_Success;
		curScreen = 1;
		ShowCfgWirelessSecScreen1();
		user_output_write(4, 1, CURSOR);
		while(true)
		{
			int tm = 0;
			while (!user_input_ready () && tm < timeout)
			{
				usleep(10000);
				tm += 20;
			}
			if(tm >= timeout) return SC_TIMEOUT;
			input = user_input_get ();
			switch(input)
			{
				case '1': // NONE
					oper.Beep(50, TYPE_KeyPress);
					props.SetProperty(PROPERTY_WEP_TYPE, PROPERTY_WEP_TYPE_NONE);
					return SC_Success;
					break;
				case '2': // WEP
					oper.Beep(50, TYPE_KeyPress);
					code = ShowCfgWEPKeyLenScreen();
					if(code != SC_Clear) return code;
					break;
				case '-': // Down Arrow
					if(curScreen == 1)
					{
						oper.Beep(50, TYPE_KeyPress);
						ShowCfgWirelessSecScreen2();
						curScreen = 2;
						user_output_write(4, 1, CURSOR);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					break;
				case '+': // Up Arrow
					if(curScreen == 2)
					{
						oper.Beep(50, TYPE_KeyPress);
						ShowCfgWirelessSecScreen1();
						curScreen = 1;
						user_output_write(4, 1, CURSOR);
					}
					else
					   oper.Beep(50,TYPE_Reject);
					break;
				case '\n':
					oper.Beep(50, TYPE_KeyPress);
					if(curScreen == 1) // NONE
					{
						props.SetProperty(PROPERTY_WEP_TYPE, PROPERTY_WEP_TYPE_NONE);
						return SC_Success;
					}
					else
					{
						code = ShowCfgWEPKeyLenScreen(); // WEP
						if(code != SC_Clear) return code;
					}
					break;
				case ESC:
				    oper.Beep(50, TYPE_KeyPress);
					return SC_Clear;
				default:
		            oper.Beep(150,TYPE_Reject);
                    oper.displayErrMessage("   NOT AVAILABLE    ");
                    goto start;
                break;
			}
			if(code == SC_Clear) break;
			else if(code == SC_TIMEOUT) return code;
		}
	}
	return code;
}

void TCPIPConfig::ShowCfgWirelessSecScreen1()
{
	user_output_clear();
	user_output_write(1, 1, CFG_WIRELESS_SEC_LINE1);
	user_output_write(2, 1, CFG_WIRELESS_SEC_LINE2);
	user_output_write(4, 2, CFG_WIRELESS_SEC_MENU1);
}

void TCPIPConfig::ShowCfgWirelessSecScreen2()
{
	user_output_clear();
	user_output_write(1, 1, CFG_WIRELESS_SEC_LINE2);
	user_output_write(3, 2, CFG_WIRELESS_SEC_MENU1);
	user_output_write(4, 2, CFG_WIRELESS_SEC_MENU2);
}

OperationStatusCode TCPIPConfig::ShowCfgWEPKeyLenScreen()
{
	OperationStatusCode code = SC_Success;
	int curCrsLine = 3;
	char input;
	int timeout = oper.GetDataTimeout();

	for(;;)
	{
start:		code = SC_Success;
		curCrsLine = 3;
		user_output_clear();
		user_output_write(1, 1, CFG_WEP_KEY_LEN_HDNG);
		user_output_write(3, 2, CFG_WEP_KEY_LEN_MENU1);
		user_output_write(4, 2, CFG_WEP_KEY_LEN_MENU2);
		user_output_write(curCrsLine, 1, CURSOR);

		while(true)
		{
			int tm = 0;
			while (!user_input_ready () && tm < timeout)
			{
				usleep(10000);
				tm += 20;
			}
			if(tm >= timeout) return SC_TIMEOUT;
			input = user_input_get ();
			switch(input)
			{
				case '1': //64 Bit
					oper.Beep(50, TYPE_KeyPress);
					code = ShowCfgWEPKeyDataScreen(TYPE_64_BIT);
					if(code != SC_Clear) return code;
					break;
				case '2': //128 Bit
					oper.Beep(50, TYPE_KeyPress);
					code = ShowCfgWEPKeyDataScreen(TYPE_128_BIT);
					if(code != SC_Clear) return code;
					break;
				case '-': // Down Arrow
					if(curCrsLine == 3)
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(curCrsLine++, 1, BLANKSPACE);
						user_output_write(curCrsLine, 1, CURSOR);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					break;
				case '+': // Up Arrow
					if(curCrsLine == 4)
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(curCrsLine--, 1, BLANKSPACE);
						user_output_write(curCrsLine, 1, CURSOR);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					break;
				case '\n': //ENTER
					if(curCrsLine == 3)
					{
						oper.Beep(50, TYPE_KeyPress);
						code = ShowCfgWEPKeyDataScreen(TYPE_64_BIT);
						if(code != SC_Clear) return code;
					}
					else
					{
						oper.Beep(50, TYPE_KeyPress);
						code = ShowCfgWEPKeyDataScreen(TYPE_128_BIT);
						if(code != SC_Clear) return code;
					}
					break;
				case ESC:
					oper.Beep(50, TYPE_KeyPress);
					return SC_Clear;
				break;
				default:
		            oper.Beep(150,TYPE_Reject);
                    oper.displayErrMessage("   NOT AVAILABLE    ");
                    goto start;
                break;
			}
			if(code == SC_Clear) break;
			else if(code == SC_TIMEOUT) return code;
		}
	}
	return code;
}

/**
 * Reads the WEP data.
 * Need to move this code to LCD Handler in the next version
 */
OperationStatusCode TCPIPConfig::ShowCfgWEPKeyDataScreen(WEPKEYTYPE keytype)
{
	user_output_clear();
	user_output_write(1, 1, CFG_WEP_KEY_DATA_HDNG);
	static char buffer[30];
	int timeout = oper.GetDataTimeout();
	XMLProperties properties( XML_PROPERTIES_FILE );
	if(keytype == TYPE_64_BIT)
	{
		LCD_READ_STATE state = lcdHandler.Get64BitWEPKey(3, 6, timeout, buffer);
		if(state == READ_CLEAR) return SC_Clear;
		else if(state == READ_TIMEDOUT) return SC_TIMEOUT;
		static unsigned char key[5];
		for(int i=0; i<5; i++)
		{
			unsigned char mnib = buffer[i * 2];
			if( mnib <= '9' ) mnib -= 48;
			else mnib -= 55;
			unsigned char lnib = buffer[i * 2 + 1];
			if( lnib <= '9' ) lnib -= 48;
			else lnib -= 55;
			key[i] = ( mnib << 4 ) |  lnib;
		}
		#ifdef DEBUG
		printf("The WEP Key is %s\n", buffer);
		for(int i=0; i<5; i++) printf("%02Xh ", key[i]);
		printf("\n");
		#endif
		if ( !setWepKey( WIRELESS_ETH_DEV, 0x80, key, 5) )
		{
			#ifdef DEBUG
			printf("Failed to Set WEP Key\n");
			#endif
		}
		else
		{
			properties.SetProperty(PROPERTY_WEP_TYPE, PROPERTY_WEP_TYPE_64);
			properties.SetProperty(PROPERTY_WEP_KEY, buffer);
		}
	}
	else if(keytype == TYPE_128_BIT)
	{
		LCD_READ_STATE state = lcdHandler.Get128BitWEPKey(3, 0, timeout, buffer);
		if(state == READ_CLEAR) return SC_Clear;
		else if(state == READ_TIMEDOUT) return SC_TIMEOUT;
		static unsigned char key[13];
		for(int i=0; i<13; i++)
		{
			unsigned char mnib = buffer[i * 2];
			if( mnib <= '9' ) mnib -= 48;
			else mnib -= 55;
			unsigned char lnib = buffer[i * 2 + 1];
			if( lnib <= '9' ) lnib -= 48;
			else lnib -= 55;
			key[i] = ( mnib << 4 ) |  lnib;
		}
		// 128 bit encryption
		if ( !setWepKey( WIRELESS_ETH_DEV, 0x80, key, 13) )
		{
			#ifdef DEBUG
			printf("Failed to Set WEP Key\n");
			#endif
		}
		else
		{
			properties.SetProperty(PROPERTY_WEP_TYPE, PROPERTY_WEP_TYPE_128);
			properties.SetProperty(PROPERTY_WEP_KEY, buffer);
		}

		#ifdef DEBUG
		printf("The WEP Key is %s\n", buffer);
		for(int i=0; i<13; i++) printf("%02Xh ", key[i]);
		printf("\n");
		#endif
	}
	return SC_Success;
}

char TCPIPConfig::GetNextHexChar(char ch)
{
	if(ch == '9')
	{
		ch = 'A';
	}
	else if(ch == 'F')
	{
		ch = '0';
	}
	else
	{
		ch++;
	}
	return ch;
}

char TCPIPConfig::GetPrevHexChar(char ch)
{
	if(ch == 'A')
	{
		ch = '9';
	}
	else if(ch == '0')
	{
		ch = 'F';
	}
	else
	{
		ch--;
	}
	return ch;
}

OperationStatusCode TCPIPConfig::ShowCfgTCPIPPropsScreen(NETWORKTYPE nwtype)
{
	OperationStatusCode code = SC_Success;
	int curCrsLine = 3;
	char input;
	int timeout = oper.GetDataTimeout();

	for(;;)
	{
start:		code = SC_Success;
		curCrsLine = 3;
		user_output_clear();
		user_output_write(1, 1, CFG_TCPIP_PROPS_HDNG);
		user_output_write(3, 2, CFG_TCPIP_PROPS_MENU1);
		user_output_write(4, 2, CFG_TCPIP_PROPS_MENU2);
		user_output_write(curCrsLine, 1, CURSOR);

		while(true)
		{
			int tm = 0;
			while (!user_input_ready () && tm < timeout)
			{
				usleep(10000);
				tm += 20;
			}
			if(tm >= timeout) return SC_TIMEOUT;
			input = user_input_get ();
			switch(input)
			{
				case '1': //DHCP
					oper.Beep(50, TYPE_KeyPress);
					SetNetworkProperties(nwtype, TYPE_DHCP);
					code = ShowTCPPortScreen();
					if(code != SC_Clear) return code;
					break;
				case '2': //Manual
					oper.Beep(50, TYPE_KeyPress);
					code = ShowCfgTCPIPAddrScreen(nwtype, TYPE_IP_ADDR);
					if(code == SC_TIMEOUT) return code;
					else if(code == SC_Clear) break;
					code = ShowTCPPortScreen();
					if(code != SC_Clear) return code;
					break;
				case '-': // Down Arrow
					if(curCrsLine == 3)
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(curCrsLine++, 1, BLANKSPACE);
						user_output_write(curCrsLine, 1, CURSOR);
					}
					else
					  oper.Beep(50,TYPE_Reject);
					break;
				case '+': // Up Arrow
					if(curCrsLine == 4)
					{
						oper.Beep(50, TYPE_KeyPress);
						user_output_write(curCrsLine--, 1, BLANKSPACE);
						user_output_write(curCrsLine, 1, CURSOR);
					}
					else
					    oper.Beep(50,TYPE_Reject);
					break;
				case '\n': //ENTER
					if(curCrsLine == 3)
					{
						oper.Beep(50, TYPE_KeyPress);
						SetNetworkProperties(nwtype, TYPE_DHCP);
						code = ShowTCPPortScreen();
						if(code != SC_Clear) return code;
					}
					else
					{
						oper.Beep(50, TYPE_KeyPress);
						code = ShowCfgTCPIPAddrScreen(nwtype, TYPE_IP_ADDR);
						if(code == SC_TIMEOUT) return code;
						else if(code == SC_Clear) break;
						code = ShowTCPPortScreen();
						if(code != SC_Clear) return code;
					}
					break;
				case ESC:
				    oper.Beep(50, TYPE_KeyPress);
					return SC_Clear;
				break;
			    default:
		            oper.Beep(150,TYPE_Reject);
                    oper.displayErrMessage("   NOT AVAILABLE    ");
                    goto start;
                break;
			}
			if(code == SC_Clear) break;
		}
	}
	return code;
}


OperationStatusCode TCPIPConfig::ShowCfgTCPIPAddrScreen(NETWORKTYPE nwtype, ADDRTYPE type)
{
	OperationStatusCode code = SC_Success;
	user_output_clear();
	switch(type)
	{
		case TYPE_IP_ADDR:
			user_output_write(1, 1, CFG_TCPIP_ADDR_HDNG);
			break;
		case TYPE_GATEWAY_ADDR:
			user_output_write(1, 1, CFG_GTWY_ADDR_HDNG);
			break;
		case TYPE_NETMASK:
			user_output_write(1, 1, CFG_NETMASK_HDNG);
			break;
        case TYPE_DNS:
            user_output_write(1, 1, CFG_DNS_HDNG);
			break;
    }
	static char ipaddr[16];
	strcpy(ipaddr, "000.000.000.000");

	/****************************************************************
	 **************** GET IP / GATEWAY / NETMASK ********************
	 ***************************************************************/

	const char * ip = GetClockIPAddress(nwtype, type);
	if( ip != NULL && strlen(ip) != 0 )
	{
		ConvertIPAddressFormat(0, ip, ipaddr);
	}

	/***************************************************************/

	int timeout = oper.GetDataTimeout();

	LCD_READ_STATE state = lcdHandler.ReadIPAddress(3, 3, timeout, ipaddr);
	if(state == READ_TIMEDOUT)
	{
		return SC_TIMEOUT;
	}
	else if(state == READ_CLEAR)
	{
		return SC_Clear;
	}

	//Build the IP Address from the data entered
	switch(type)
	{
		case TYPE_IP_ADDR:
			ConvertIPAddressFormat(1, ipaddr, ipaddress);
			code = ShowCfgTCPIPAddrScreen(nwtype, TYPE_GATEWAY_ADDR); // Get the GATEWAY ADDRESS
			break;
		case TYPE_GATEWAY_ADDR:
			ConvertIPAddressFormat(1, ipaddr, gatewayaddress);
			code = ShowCfgTCPIPAddrScreen(nwtype, TYPE_NETMASK); // Get the NETMASK
			break;
		case TYPE_NETMASK:
			ConvertIPAddressFormat(1, ipaddr, netmask);
			code = ShowCfgTCPIPAddrScreen(nwtype, TYPE_DNS); // Get the DNS SERVER
			break;
        case TYPE_DNS:
			ConvertIPAddressFormat(1, ipaddr, dns);
            SetNetworkProperties(nwtype, TYPE_STATIC);
			break;
	}
	return code;
}

/**
 * If fmt is 0 : Converts IP Address from "10.0.1.25" to "010.000.001.025" format
 * Else : Converts IP Address from "010.000.001.025" to "10.0.1.25" format
 */
void TCPIPConfig::ConvertIPAddressFormat(int fmt, const char * srcaddr, char * buffer)
{
	static char * separator = ".";
	if(fmt == 0)
	{
		while(strlen(srcaddr) != 0)
		{
			int numOfChars = 0;
			char * str = strchr(srcaddr, '.');
			if(str == NULL) numOfChars = strlen(srcaddr);
			else numOfChars = str - srcaddr;
			switch(numOfChars)
			{
				case 1:
					*buffer++ = '0';
					*buffer++ = '0';
					*buffer++ = *srcaddr++;
					break;
				case 2:
					*buffer++ = '0';
					*buffer++ = *srcaddr++;
					*buffer++ = *srcaddr++;
					break;
				case 3:
					*buffer++ = *srcaddr++;
					*buffer++ = *srcaddr++;
					*buffer++ = *srcaddr++;
					break;
			}
			if(strlen(srcaddr) != 0)
			{
				*buffer++ = '.';
				srcaddr++;
			}
		}
	}
	else
	{
		long one = strtol(srcaddr, &separator, 10);
		long two = strtol(srcaddr + 4, &separator, 10);
		long three = strtol(srcaddr + 8, &separator, 10);
		long four = strtol(srcaddr + 12, &separator, 10);
		sprintf(buffer, "%ld.%ld.%ld.%ld", one, two, three, four);
	}
}

/**
 * Converts Gateway Address format (0A000001) read from /proc/net/route
 * to 10.0.0.1 format
 */
void TCPIPConfig::ConvertGatewayAddressFormat(const char * srcaddr, char * buffer)
{
	if(strlen(srcaddr) != 8) return;
	char temp[3];
	strncpy(temp, srcaddr, 2);
	temp[2] = '\0';
	long one = strtol(temp, NULL, 16);
	strncpy(temp, srcaddr + 2, 2);
	temp[2] = '\0';
	long two = strtol(temp, NULL, 16);
	strncpy(temp, srcaddr + 4, 2);
	temp[2] = '\0';
	long three = strtol(temp, NULL, 16);
	strncpy(temp, srcaddr + 6, 2);
	temp[2] = '\0';
	long four = strtol(temp, NULL, 16);
	sprintf(buffer, "%ld.%ld.%ld.%ld", one, two, three, four);
}

/**
 * This function sets the network properties (STATIC or DHCP)
 * if STATIC it sets the IPADDRESS, NETMASK, and GATEWAY of the system
 * This is done by modifying the "interfaces" file.
 * "ifconfig" uses this file to set the network properties of the system
 */
void TCPIPConfig::SetNetworkProperties(NETWORKTYPE nwtype, ADDRDEF def)
{
	static char buffer[MAX_BUFFER_SIZE];

	// Open the interfaces File
	char bufCom[64];
	strcpy(bufCom,"echo -n > ");strcat(bufCom,DNS_RESOLV_CONF);
	system(bufCom);
	strcpy(bufCom,"echo nameserver ");strcat(bufCom,dns);strcat(bufCom,">>");strcat(bufCom,DNS_RESOLV_CONF);
    system(bufCom);
	FILE * fp = fopen(INTERFACES, "r");
	if( !fp )
	{
		#ifdef DEBUG
		perror("Couldn't open the interfaces file");
		#endif
		return;
	}

	FILE *fw = fopen(TMP_INTERFACES, "w");
	if( !fw )
	{
		#ifdef DEBUG
		perror("Couldn't open the File for write");
		#endif
		return;
	}

	char * line = fgets(buffer, MAX_BUFFER_SIZE, fp);

	while(line != NULL)
	{
		line = TrimString(line);
		// Check for Blank Line
		if( strlen(line) == 0 )
		{
			fputs(NEWLINE, fw);
			line = fgets(buffer, MAX_BUFFER_SIZE, fp);
			continue;
		}
		// Check for Comment
		else if( *line == '#' )
		{
			fputs(line, fw);
			fputs(NEWLINE, fw);
			line = fgets(buffer, MAX_BUFFER_SIZE, fp);
			continue;
		}
		char * tmp = strstr(buffer, "iface");

		if(tmp == NULL)
		{
			// Add a tab if its an option
			if( CheckIfStaticOrDhcpOption(line) )
			{
				fputs(TAB, fw);
			}
			fputs(line, fw);
			fputs(NEWLINE, fw);
			line = fgets(buffer, MAX_BUFFER_SIZE, fp);
			continue;
		}
		else
		{
			if(nwtype == TYPE_ETHERNET)
				tmp = strstr(buffer, WIRED_ETH_DEV);
			else
				tmp = strstr(buffer, WIRELESS_ETH_DEV);
			if(tmp == NULL)
			{
				fputs(line, fw);
				fputs(NEWLINE, fw);
				line = fgets(buffer, MAX_BUFFER_SIZE, fp);
				continue;
			}
			else
			{
				if(def == TYPE_STATIC) // STATIC
				{
					if(nwtype == TYPE_ETHERNET)
						fputs(INTERFACES_WIRED_STATIC, fw);
					else
						fputs(INTERFACES_WIRELESS_STATIC, fw);
					fputs(NEWLINE, fw);
					strcpy(buffer, TAB INTERFACES_STATIC_ADDRESS BLANKSPACE);
					strcat(buffer, ipaddress);
					fputs(buffer, fw);
					fputs(NEWLINE, fw);
					strcpy(buffer, TAB INTERFACES_STATIC_NETMASK BLANKSPACE);
					strcat(buffer, netmask);
					fputs(buffer, fw);
					fputs(NEWLINE, fw);
					// Add the Gateway address is its not 0.0.0.0
					if( strcmp(gatewayaddress, "0.0.0.0") != 0 )
					{
						strcpy(buffer, TAB INTERFACES_STATIC_GATEWAY BLANKSPACE);
						strcat(buffer, gatewayaddress);
						fputs(buffer, fw);
						fputs(NEWLINE, fw);
					}
				}
				else // DHCP
				{
					if(nwtype == TYPE_ETHERNET)
					{
						fputs(INTERFACES_WIRED_DHCP, fw);
					}
					else
					{
						fputs(INTERFACES_WIRELESS_DHCP, fw);
					}
					fputs(NEWLINE, fw);
				}

				/**
				 * Skip the next lines that are options of the replaced config
				 */
				line = fgets(buffer, MAX_BUFFER_SIZE, fp);
				while(line != NULL)
				{
					line = TrimString(line);
					// Check for Blank Line or Comment
					if( strlen(line) == 0 || *line == '#' )
					{
						line = fgets(buffer, MAX_BUFFER_SIZE, fp);
						continue;
					}
					// Check for all the options in static and dhcp.
					// If found skip those lines
					if( CheckIfStaticOrDhcpOption(line) )
					{
						line = fgets(buffer, MAX_BUFFER_SIZE, fp);
						continue;
					}
					break;
				}
			}
		}
	}
	if(fp)
	{
		fclose(fp);
		fp=NULL;
	}

    if(fw)
    {
		fclose(fw);
		fw=NULL;
    }
	if(rename(TMP_INTERFACES, INTERFACES) == -1)
	{
		#ifdef DEBUG
		perror("Failed to rename file");
		#endif
		return;
	}

	XMLProperties props(XML_PROPERTIES_FILE);

	char command[64];

	/**
	 * Set the Address
	 */
	if( def == TYPE_STATIC )
	{
		if(nwtype == TYPE_ETHERNET)
		{
			strcpy(command, "ifconfig " WIRED_ETH_DEV " ");
		}
		else
		{
			strcpy(command, "ifconfig " WIRELESS_ETH_DEV " ");
			props.SetProperty(PROPERTY_WIRELESS_ADDRTYPE, PROPERTY_ADDRTYEPE_STATIC);
			props.SetProperty(PROPERTY_WIRELESS_IPADDR, ipaddress);
			props.SetProperty(PROPERTY_WIRELESS_NETMASK, netmask);
		}
		strcat(command, ipaddress);
		strcat(command, " netmask ");
		strcat(command, netmask);
		#ifdef DEBUG
		printf("%s\n", command);
		#endif
		system(command);
	}
	else if( def == TYPE_DHCP )
	{
		if(nwtype == TYPE_ETHERNET)
		{
			#ifdef DEBUG
			printf("udhcpc -i " WIRED_ETH_DEV " & " );
			#endif
			system( "udhcpc -i " WIRED_ETH_DEV " & " );
		}
		else
		{
			#ifdef DEBUG
			printf("udhcpc -b -i " WIRELESS_ETH_DEV " & " );
			#endif
			props.SetProperty(PROPERTY_WIRELESS_ADDRTYPE, PROPERTY_ADDRTYEPE_DHCP);
			system( "udhcpc -b -i " WIRELESS_ETH_DEV " & " );
		}
	}

	/**
	 * SET GATEWAY ADDRESS
	 * First we delete the default gateway and then add the new gateway
	 */

	// Delete
	if(nwtype == TYPE_ETHERNET)
	{
		#ifdef DEBUG
		printf(CMD_DEL_GATEWAY WIRED_ETH_DEV);
		#endif
		system(CMD_DEL_GATEWAY WIRED_ETH_DEV);
	}
	else
	{
		#ifdef DEBUG
		printf(CMD_DEL_GATEWAY WIRELESS_ETH_DEV);
		#endif
		system(CMD_DEL_GATEWAY WIRELESS_ETH_DEV);
	}
	// Set the Gateway
	strcpy(command, CMD_CFG_GATEWAY);
	strcat(command, gatewayaddress);
	strcat(command, CMD_CFG_GATEWAY_PARAMS);
	if(nwtype == TYPE_ETHERNET)
	{
		strcat(command, WIRED_ETH_DEV);
		#ifdef DEBUG
		printf("%s\n", command);
		#endif
		system(command);
	}
	else
	{
		strcat(command, WIRELESS_ETH_DEV);
		#ifdef DEBUG
		printf("%s\n", command);
		#endif
		system(command);
	}

	usleep(1000000);

	//needsReboot = true;
}

/**
 * Utility function to check if the line is one of the options
 * of static or dhcp
 */
bool TCPIPConfig::CheckIfStaticOrDhcpOption(char * line)
{
	// Check for static options
	if( strstr(line, INTERFACES_STATIC_ADDRESS)     != NULL ||
		strstr(line, INTERFACES_STATIC_NETMASK)     != NULL ||
		strstr(line, INTERFACES_STATIC_GATEWAY)     != NULL ||
		strstr(line, INTERFACES_STATIC_NETWORK)     != NULL ||
		strstr(line, INTERFACES_STATIC_POINTOPOINT) != NULL ||
		strstr(line, INTERFACES_STATIC_BROADCAST)   != NULL )
	{
		return true;
	}
	// Check for dhcp options
	else if( strstr(line, INTERFACES_DHCP_HOSTNAME) != NULL ||
		strstr(line, INTERFACES_DHCP_LEASEHOURS)    != NULL ||
		strstr(line, INTERFACES_DHCP_LEASETIME)     != NULL ||
		strstr(line, INTERFACES_DHCP_VENDOR)        != NULL ||
		strstr(line, INTERFACES_DHCP_CLIENT)        != NULL )
	{
		return true;
	}
	return false;
}

/**
 * Displays the TCP Port Screen
 */
OperationStatusCode TCPIPConfig::ShowTCPPortScreen()
{
	user_output_clear();
	user_output_write(1, 1, CFG_TCP_PORT_HDNG);
	static char buffer[6];
	strcpy(buffer, "00000");
	int port = oper.GetTCPPort();
	if(port > 9999)
		sprintf(buffer, "%d", port);
	else
		sprintf(buffer+1, "%d", port);

	int timeout = oper.GetDataTimeout();

	LCD_READ_STATE state = lcdHandler.ReadData(SysInputTypeNumeric, 3, 8, timeout, 5, 5, buffer);
	if(state == READ_TIMEDOUT)
	{
		return SC_TIMEOUT;
	}
	else if(state == READ_CLEAR)
	{
		return SC_Clear;
	}

	port = strtol(buffer, NULL, 10);
	oper.SetTCPPort(port);
	#ifdef DEBUG
	printf("TCP PORT = %d\n", port);
	#endif
	return ShowTIDScreen();
}

/**
 * Displays the TID Screen
 */
OperationStatusCode TCPIPConfig::ShowTIDScreen()
{
	user_output_clear();
	user_output_write(1, 1, CFG_TERMINAL_ID_HDNG);
	char ch = oper.GetTerminalId();
	int timeout = oper.GetDataTimeout();
	LCD_READ_STATE state = lcdHandler.ReadTerminalId(3, 10, timeout, ch);
	if(state == READ_TIMEDOUT) return SC_TIMEOUT;
	else if(state == READ_CLEAR) return SC_Clear;
	#ifdef DEBUG
	printf("Setting Terminal Id to %c\n", ch);
	#endif
	oper.SetTerminalId(ch);
	return SC_Success;
}


const char * TCPIPConfig::read_first_resolv_dns_server()
{
	static char ipaddress[18];
	bzero(ipaddress, 16 );
	ipaddress[0] = '\0';
//testing
//	strcpy(ipaddress,"10.10.10.10");
//	return ipaddress;

	{	// read the DNS_RESOLV_CONF file


		static char buffer[MAX_BUFFER_SIZE];
		char dns[16];
		bzero(dns, 16 );

		FILE * fp = fopen(DNS_RESOLV_CONF, "r");
		if( !fp )
		{
			#ifdef DEBUG
			perror("Couldn't open the resolv.conf file");
			#endif
			return ipaddress;
		}

		char * line = fgets(buffer, MAX_BUFFER_SIZE, fp);

		while(line != NULL)
		{
			line = TrimString(line);
			// Check for Blank Line
			if( strlen(line) == 0 )
			{
				line = fgets(buffer, MAX_BUFFER_SIZE, fp);	// next line
				continue;
			}
			// Check for Comment
			else if( *line == '#' )
			{
				line = fgets(buffer, MAX_BUFFER_SIZE, fp); // next line
				continue;
			}
			char * tmp = strcasestr(buffer, "nameserver");

			if ( tmp == NULL )
			{
				line = fgets(buffer, MAX_BUFFER_SIZE, fp);
				continue;
			}
			else
			{
				// skip over whitespace
				char * tok = strtok (tmp,"\t ");
				if( tok == NULL )
				{
					line = fgets(buffer, 128, fp);
					continue;
				}
				if ( strcasecmp(tok, "nameserver") == 0 )
				{
					// still at nameserver. skip over to next token, s/b ip address
					tok = strtok(NULL, "\t ");
				}
				if ( tok == NULL )
				{
 					line = fgets(buffer, 128, fp);
					continue;
 				}
				// rest of line should be address
				tok[strlen(tok)]='\0';
				ConvertIPAddressFormat(0, tok, dns); // from xx.xx.xx.xx to xxx.xxx.xxx.xxx

				strcpy( ipaddress, dns );
				break;
			}
		}
		if(fp)
		{
			fclose(fp);
			fp=NULL;
		}


	}
	return ipaddress;
}


const char * TCPIPConfig::GetClockIPAddress(NETWORKTYPE nwtype, ADDRTYPE type)
{
	static char ipaddress[18];
	bzero(ipaddress, 16 );
	ipaddress[0] = '\0';
	int sfd;
	struct ifreq ifr;
	struct sockaddr_in *sin = (struct sockaddr_in *) &ifr.ifr_addr;

	memset(&ifr, 0, sizeof ifr);

	sfd = socket(AF_INET, SOCK_STREAM, 0);

	if( sfd )
	{
		if(nwtype == TYPE_ETHERNET)
		{
			strcpy(ifr.ifr_name, WIRED_ETH_DEV);
		}
		else
		{
			strcpy(ifr.ifr_name, WIRELESS_ETH_DEV);
		}
		sin->sin_family = AF_INET;
		switch( type )
		{

		    case TYPE_DNS:

			{
				const char * ip = read_first_resolv_dns_server();
				if ( ip != NULL )
				{
//printf("dns address found is %s\n", ip);
					strcpy(ipaddress, ip);
				}
			}

		    break;
			case TYPE_IP_ADDR:
				if (0 == ioctl(sfd, SIOCGIFADDR, &ifr))
				{
					const char * ip = inet_ntoa(sin->sin_addr);
					if( ip != NULL )
					{
						strcpy( ipaddress, ip );
					}
				}
				break;
			case TYPE_NETMASK:
				if (0 == ioctl(sfd, SIOCGIFNETMASK, &ifr))
				{
					const char * nm = inet_ntoa(sin->sin_addr);
					if( nm != NULL )
					{
						strcpy( ipaddress, nm );
					}
				}
				break;
			case TYPE_GATEWAY_ADDR: // Look into struct route_info
				FILE * fp = fopen(ROUTE_TABLE, "r");
				if(fp)
				{
					char buffer[128];
					char gtwaddr[16];
					char * line = fgets(buffer, 128, fp);
					while(line != NULL)
					{
						line = TrimString(line);
						// Check for Blank Line
						if( strlen(line) == 0 )
						{
							line = fgets(buffer, 128, fp);
							continue;
						}
						char * tok = strtok (line,"\t ");
						if( tok == NULL || (strcmp(tok, "Iface") == 0) ) // Header
						{
							line = fgets(buffer, 128, fp);
							continue;
						}
						if( (nwtype == TYPE_ETHERNET && strcmp(tok, WIRED_ETH_DEV) == 0) ||
							(nwtype == TYPE_WIRELESS && strcmp(tok, WIRELESS_ETH_DEV) == 0) )
						{
							tok = strtok (NULL, "\t ");
							if(tok != NULL)
							{
								if(strcmp(tok, "00000000") == 0)
								{
									tok = strtok (NULL, "\t ");
									if(tok != NULL)
									{
										ConvertGatewayAddressFormat(tok, gtwaddr);
										strcpy( ipaddress, gtwaddr );
										break;
									}
								}
							}
						}
						line = fgets(buffer, 128, fp);
					}
					fclose(fp);
				}
				break;

		}

		close( sfd );
	}

	return ipaddress;
}

OperationStatusCode TCPIPConfig::ShowNetworkSettings(NETWORKTYPE nwtype)
{
	user_output_clear();
	if( nwtype == TYPE_WIRELESS )
		user_output_write(1, 1, WIRELESS_SETTINGS_HDNG);
	else
		user_output_write(1, 1, WIRED_SETTINGS_HDNG);

	// Get the IP Address
	user_output_write(2, 1, "IP=");
	const char * ip = GetClockIPAddress(nwtype, TYPE_IP_ADDR);
	if( ip != NULL && strlen( ip ) != 0 ) user_output_write(2, 4, ip);
	else user_output_write(2, 4, "0.0.0.0");

	// Get the Netmask
	user_output_write(3, 1, "NM=");
	ip = GetClockIPAddress(nwtype, TYPE_NETMASK);
	if( ip != NULL && strlen( ip ) != 0 ) user_output_write(3, 4, ip);
	else user_output_write(3, 4, "0.0.0.0");

	// Get the Gateway
	user_output_write(4, 1, "GW=");
	ip = GetClockIPAddress(nwtype, TYPE_GATEWAY_ADDR);
	if( ip != NULL && strlen( ip ) != 0 ) user_output_write(4, 4, ip);
	else user_output_write(4, 4, "0.0.0.0");

	int timeout = oper.GetDataTimeout();
	int tm = 0;
	char input;
	while(true)
	{
		if (user_input_ready ())
		{
			input = user_input_get ();
			if( input == ESC )
			{
				oper.Beep(TYPE_KeyPress);
				return SC_Clear;
			}
			oper.Beep(150, TYPE_Reject);
		}
		else
		{
			usleep(10000);
			tm += 20;
			if(tm >= timeout)
			{
				oper.Beep(TYPE_TimeOut);
				return SC_TIMEOUT;
			}
		}
	}
	return SC_Success;
}

/**
 * Loads the Wireless properties from the properties.xml
 * and initializes the wireless interface
 */
OperationStatusCode TCPIPConfig::InitializeWireless()
{
	XMLProperties props(XML_PROPERTIES_FILE);

	// Set the ESSID
	char essid[50];essid[0]='\0';
	props.GetProperty(PROPERTY_ESSID,essid);
	if( essid != NULL )
	{
		// Set the SSID
		char cmd[strlen(CMD_CFG_SSID) + 22];
		strcpy(cmd, CMD_CFG_SSID);
		strcat(cmd, essid);
		#ifdef DEBUG
		printf("%s\n", cmd);
		#endif
		system( cmd );
	}

	char prop[50];prop[0]='\0';
	props.GetProperty(PROPERTY_WEP_TYPE,prop);

	if( prop != NULL && strlen(prop) != 0 && strcmp(prop, PROPERTY_WEP_TYPE_NONE) != 0 )
	{
		char weptype[4];
		strcpy(weptype, prop);
		prop[0]='\0';
		props.GetProperty(PROPERTY_WEP_KEY,prop);
		if(strlen(prop) != 0 )
		{
			char wepkey[30];
			strcpy(wepkey, prop);
			#ifdef DEBUG
			printf(" Setting WEPTYPE = %s bit and WEP KEY = %s\n", weptype, wepkey);
			#endif
			if( strcmp(weptype, PROPERTY_WEP_TYPE_64) == 0 )
			{
				unsigned char key[5];
				for(int i=0; i<5; i++)
				{
					unsigned char mnib = wepkey[i * 2];
					if( mnib <= '9' ) mnib -= 48;
					else mnib -= 55;
					unsigned char lnib = wepkey[i * 2 + 1];
					if( lnib <= '9' ) lnib -= 48;
					else lnib -= 55;
					key[i] = ( mnib << 4 ) |  lnib;
				}
				if ( !setWepKey( WIRELESS_ETH_DEV, 0x80, key, 5) )
				{
					#ifdef DEBUG
					printf("Failed to Set WEP Key\n");
					#endif
				}
			}
			else if( strcmp(weptype, PROPERTY_WEP_TYPE_128) == 0 )
			{
				unsigned char key[13];
				for(int i=0; i<13; i++)
				{
					unsigned char mnib = wepkey[i * 2];
					if( mnib <= '9' ) mnib -= 48;
					else mnib -= 55;
					unsigned char lnib = wepkey[i * 2 + 1];
					if( lnib <= '9' ) lnib -= 48;
					else lnib -= 55;
					key[i] = ( mnib << 4 ) |  lnib;
				}
				if ( !setWepKey( WIRELESS_ETH_DEV, 0x80, key, 13) )
				{
					#ifdef DEBUG
					printf("Failed to Set WEP Key\n");
					#endif
				}
			}
		}
	}
	char command[64];
	prop[0]='\0';
	props.GetProperty(PROPERTY_WIRELESS_ADDRTYPE,prop);
	if(strlen(prop) != 0 )
	{
		if( strcmp(prop, PROPERTY_ADDRTYEPE_DHCP) == 0 )
		{
			#ifdef DEBUG
			printf( "udhcpc -i " WIRELESS_ETH_DEV " & " );
			#endif
			system( "udhcpc -i " WIRELESS_ETH_DEV " & " );
		}
		else if( strcmp(prop, PROPERTY_ADDRTYEPE_STATIC) == 0 )
		{
			strcpy( command, "ifconfig " WIRELESS_ETH_DEV " " );
			prop[0]='\0';
			props.GetProperty(PROPERTY_WIRELESS_IPADDR,prop);
			strcat( command, prop );
			strcat( command, " netmask ");
			prop[0]='\0';
			props.GetProperty(PROPERTY_WIRELESS_NETMASK,prop);
			strcat( command, prop );
			#ifdef DEBUG
			printf("%s\n", command);
			#endif
			system( command );
		}
	}
	return SC_Success;
}

/**
 * Gest the MAC Address of the specified interface
 */
const char * TCPIPConfig::GetMACAddress(NETWORKTYPE nwtype)
{
	/**
	 * Initialize the MAC Address
	 */
	int sd;
	struct ifreq hwir;

	sd=socket(PF_INET, SOCK_STREAM, 0);
	memset(&hwir, 0, sizeof(hwir));

	if(nwtype == TYPE_ETHERNET)
	{
		strcpy(hwir.ifr_name, WIRED_ETH_DEV);
	}
	else
	{
		strcpy(hwir.ifr_name, WIRELESS_ETH_DEV);
	}
	ioctl(sd, SIOCGIFHWADDR, &hwir);

	close(sd);

	static char macaddress[16];
	char * macaddr = macaddress;

	// Convert the adddress to hex format
	for(int i=0;i<6;i++)
	{
		sprintf(macaddr, "%02X", hwir.ifr_hwaddr.sa_data[i]);
		macaddr += 2;
		if(i != 5)
		{
			sprintf(macaddr, "%s", ":");
			macaddr++;
		}
	}

	#ifdef DEBUG
	if(nwtype == TYPE_ETHERNET)
	{
		printf("The MAC Address of " WIRED_ETH_DEV " is %s\n", macaddress);
	}
	else
	{
		printf("The MAC Address of " WIRELESS_ETH_DEV " is %s\n", macaddress);
	}
	#endif

	return macaddress;
}

OperationStatusCode TCPIPConfig::ShowNetworkStatus(NETWORKTYPE nwtype)
{
	int index = 0;

	user_output_clear();
	if(nwtype == TYPE_WIRELESS)
	{
		user_output_write(1, 3, "WIRELESS STATUS");
	}
	else
	{
		user_output_write(1, 5, "WIRED STATUS")	;
	}

	char rxstatus[128];
	char txstatus[128];
	char wirstatus[128];

	char rxstatdisp[21];
	char txstatdisp[21];
	char wirstatdisp[21];

	rxstatdisp[20] = '\0';
	txstatdisp[20] = '\0';
	wirstatdisp[20] = '\0';

	LoadNetworkStatus(nwtype, rxstatus, txstatus, wirstatus);

	int rxindex = strlen(rxstatus) - 20;
	int txindex = strlen(txstatus) - 20;
	int wirindex = strlen(wirstatus) - 20;

	strncpy(rxstatdisp, rxstatus, 20);
	strncpy(txstatdisp, txstatus, 20);
	strncpy(wirstatdisp, wirstatus, 20);

	user_output_write(2, 1, rxstatdisp);
	user_output_write(3, 1, txstatdisp);
	if(nwtype == TYPE_WIRELESS)
	{
		user_output_write(4, 1, wirstatdisp);
	}

	int refreshtime = 1000;
	char input;

	bool needsShift = false;
	bool refresh = false;
	while(true)
	{
		int tm = 0;
		needsShift = false;
		refresh = false;
		while (!user_input_ready () && tm < refreshtime)
		{
			usleep(10000);
			tm += 20;
		}
		if(tm >= refreshtime)
		{
			LoadNetworkStatus(nwtype, rxstatus, txstatus, wirstatus);
			rxindex = strlen(rxstatus) - 20;
			txindex = strlen(txstatus) - 20;
			wirindex = strlen(wirstatus) - 20;
			refresh = true;
		}
		//if(user_input_ready ())
		    input = user_input_get();
		if(input == ESC)
		{
			oper.Beep(TYPE_KeyPress);
			return SC_TIMEOUT;
		}
		switch(input)
		{
			case '>': // Right Arrow
				if(index < rxindex || index < txindex || index < wirindex)
				{
					oper.Beep(50,TYPE_KeyPress);
					index++;
					needsShift = true;
				}
				else
				    oper.Beep(50,TYPE_Reject);
				break;
			case '<': // Left Arrow
				if(index > 0)
				{
					oper.Beep(50,TYPE_KeyPress);
					index--;
					needsShift = true;
				}
				else
				    oper.Beep(50,TYPE_Reject);
				break;
			default:
			   if(!refresh)
			     oper.Beep(50,TYPE_Reject);
				break;
		}
		if(needsShift || refresh)
		{
			if(index <= rxindex)
			{
				strncpy(rxstatdisp, rxstatus + index, 20);
			}
			else
			{
				strncpy(rxstatdisp, rxstatus + rxindex, 20);
			}

			if(index <= txindex)
			{
				strncpy(txstatdisp, txstatus + index, 20);
			}
			else
			{
				strncpy(txstatdisp, txstatus + txindex, 20);
			}

			if(index <= wirindex)
			{
				strncpy(wirstatdisp, wirstatus + index, 20);
			}
			else
			{
				strncpy(wirstatdisp, wirstatus + wirindex, 20);
			}

			user_output_write(2, 1, rxstatdisp);
			user_output_write(3, 1, txstatdisp);

			if(nwtype == TYPE_WIRELESS)
			{
				user_output_write(4, 1, wirstatdisp);
			}
		}
	}

	return SC_Success;
}

/**
 * Parses the /proc/net/dev and /proc/net/wireless files
 * and loads the status.
 */
void TCPIPConfig::LoadNetworkStatus(NETWORKTYPE nwtype, char * rxstatus, char * txstatus, char * wirstatus)
{
	char buffer[256];
	strcpy(rxstatus, "RX bytes:");
	strcpy(txstatus, "TX bytes:");
	strcpy(wirstatus, "Link=");

	FILE * fp = fopen(INTERFACE_STATUS, "r");
	const char * interface = (nwtype == TYPE_WIRELESS ? WIRELESS_ETH_DEV : WIRED_ETH_DEV);
	if(fp)
	{
		// First two lines are headers
		char * line = fgets(buffer, 128, fp);
		line = fgets(buffer, 128, fp);

		line = fgets(buffer, 128, fp);
		while(line != NULL)
		{
			char * tok = strtok (line,"\t :"); // Interface
			if(strstr(tok, interface) != NULL)
			{
				// Receive Info
				tok = strtok (NULL,"\t :"); // Bytes Received
				strcat(rxstatus, tok);

				tok = strtok (NULL,"\t :"); // Packets Received

				tok = strtok (NULL,"\t :"); // Receive Errors
				strcat(rxstatus, " errors:");
				strcat(rxstatus, tok);

				tok = strtok (NULL,"\t :"); // Receive Drop
				tok = strtok (NULL,"\t :"); // Receive Fifo
				tok = strtok (NULL,"\t :"); // Receive Frame
				tok = strtok (NULL,"\t :"); // Receive Compressed
				tok = strtok (NULL,"\t :"); // Receive Multicast

				// Transmit Info
				tok = strtok (NULL,"\t :"); // Bytes Received
				strcat(txstatus, tok);

				tok = strtok (NULL,"\t :"); // Packets Received

				tok = strtok (NULL,"\t :"); // Receive Errors
				strcat(txstatus, " errors:");
				strcat(txstatus, tok);

				tok = strtok (NULL,"\t :"); // Receive Drop
				tok = strtok (NULL,"\t :"); // Receive Fifo
				tok = strtok (NULL,"\t :"); // Receive Frame
				tok = strtok (NULL,"\t :"); // Receive Compressed
				tok = strtok (NULL,"\t :"); // Receive Multicast
			}
			line = fgets(buffer, 128, fp);
		}
		fclose(fp);
	}

	if(nwtype == TYPE_WIRELESS)
	{
		fp = fopen(WIRELESS_STATUS, "r");
		if(fp)
		{
			// First two lines are headers
			char * line = fgets(buffer, 128, fp);
			line = fgets(buffer, 128, fp);

			line = fgets(buffer, 128, fp);
			line = TrimString(line);
			if(line != NULL)
			{
				char * tok = strtok (line,"\t "); // Interface
				tok = strtok (NULL,"\t "); // Status
				tok = strtok (NULL,"\t "); // Link Quality
				char * endPtr = ".";
				int sigInt = 0;
				char sigChar[33];

				//////////////////////////////////////////////
				//Replace '.' by '/'
				//if(strlen(tok) > 0)
				//{
				//	*(tok + strlen(tok) - 1) = '/';
				//}
				//strcat(wirstatus, tok);
				//strcat(wirstatus, "92 Signal=");
				/////////////////////////////////////////////

				if(strlen(tok) > 0)
				{
					float sigFloat = strtof(tok, &endPtr);
					sigFloat = ((sigFloat / 92) * 100);
					sigInt = (int) sigFloat;
				}

				sprintf(sigChar, "%d", sigInt);
				strcat(wirstatus, sigChar);
				strcat(wirstatus, "% Signal=");

				/**
				 * iwconfig displays Noise Level and Signal level
				 * in either dBm or a fraction by 153.
				 * If the signal level is 0 we display in dBm else as a fraction
				 */

				///////////////////////////////////////////////////////////
				//bool dbmUnits = strcmp(tok, "0/") == 0 ? true : false;
				///////////////////////////////////////////////////////////

				bool sigLooped = false;
				char * sFull = new char[5];
				strcpy(sFull, "Xlnt");

				// Get Signal Level
				tok = strtok (NULL,"\t "); // Signal Level

				int val = strtol(tok, &endPtr, 10);

				if(val >= 100)
				{
					val -=256;
				}
				else if(val <= 30)
				{
					sigLooped = true;
				}
				else
				{
					val = -999;
				}

				/////////////////////////////////////////////////
				// If we need to display in dBm, we subtract 256
				//if(dbmUnits)
				//{
				//	val -= 256;
				//}
				/////////////////////////////////////////////////

				if(sigLooped)
				{
					sprintf(wirstatus + strlen(wirstatus), "%s", sFull);
					strcat(wirstatus, " Noise=");
				}
				else
				{
					sprintf(wirstatus + strlen(wirstatus), "%d", val);
					strcat(wirstatus, "dBm Noise=");
				}

				delete[] sFull;

				// Get Noise Level
				tok = strtok (NULL,"\t "); // Noise Level
				val = strtol(tok, &endPtr, 10);

				val -= 256;


				//////////////////////////////////////////////////////
				// If we need to display in dBm, we subtract 256
				//if(dbmUnits)
				//{
				//	val -= 256;
				//	strcat(wirstatus, "dBm Noise=");
				//}
				//else
				//{
				//	strcat(wirstatus, "/153 Noise=");
				//}
				///////////////////////////////////////////////////////

				sprintf(wirstatus + strlen(wirstatus), "%d", val);
				strcat(wirstatus, "dBm");

				///////////////////////////////////
				//if(dbmUnits)
				//{
				//	strcat(wirstatus, "dBm");
				//}
				//else
				//{
				//	strcat(wirstatus, "/153");
				//}
				///////////////////////////////////
			}
			fclose(fp);
		}
	}
}

OperationStatusCode TCPIPConfig::ShowNetworkInfoMenu(NETWORKTYPE nwtype)
{
	int timeout = oper.GetDataTimeout();
	char input;

	for(;;)
	{
start:		user_output_clear();
		if(nwtype == TYPE_WIRELESS)
		{
			user_output_write(1, 4, "WIRELESS INFO");
		}
		else
		{
			user_output_write(1, 6, "WIRED INFO");
		}
		user_output_write(2, 2, "1-IP SETTINGS");
		user_output_write(3, 2, "2-NETWORK STATUS");
		user_output_write(4, 2, "3-MAC ADDDRESS");

		user_output_write(2, 1, CURSOR);
		int cursorloc = 2;

		while(true)
		{
			bool initialize = false;
			int tm = 0;
			while (!user_input_ready () && tm < timeout)
			{
				usleep(10000);
				tm += 20;
			}
			if(tm >= timeout)
			{
				oper.Beep(TYPE_TimeOut);
				return SC_TIMEOUT;
			}
			input = user_input_get ();
			switch(input)
			{
				case '1': // Network Settings
					oper.Beep(TYPE_KeyPress);
					if(ShowNetworkSettings(nwtype) == SC_TIMEOUT)
					{
						return SC_TIMEOUT;
					}
					initialize = true;
					break;
				case '2': // Network Status
					oper.Beep(TYPE_KeyPress);
					ShowNetworkStatus(nwtype);
					initialize = true;
					break;
				case '3': // MAC Address
					oper.Beep(TYPE_KeyPress);
					if(ShowMACAddress(nwtype) == SC_TIMEOUT)
					{
						return SC_TIMEOUT;
					}
					initialize = true;
					break;
				case '-': // Down Arrow
					if(cursorloc < 4)
					{
						oper.Beep(TYPE_KeyPress);
						user_output_write(cursorloc++, 1, BLANKSPACE);
					}
					else
					   oper.Beep(50, TYPE_Reject);
					user_output_write(cursorloc, 1, CURSOR);
					break;
				case '+': // Up Arrow
					if(cursorloc > 2)
					{
						oper.Beep(TYPE_KeyPress);
						user_output_write(cursorloc--, 1, BLANKSPACE);
					}
					else
					     oper.Beep(50, TYPE_Reject);
					user_output_write(cursorloc, 1, CURSOR);
					break;
				case '\n':
					oper.Beep(TYPE_KeyPress);
					switch(cursorloc)
					{
						case 2:
							if(ShowNetworkSettings(nwtype) == SC_TIMEOUT)
							{
								return SC_TIMEOUT;
							}
							initialize = true;
							break;
						case 3:
							ShowNetworkStatus(nwtype);
							initialize = true;
							break;
						case 4:
							if( ShowMACAddress(nwtype) == SC_TIMEOUT)
							{
								return SC_TIMEOUT;
							}
							initialize = true;
							break;
					}
					break;
				case ESC:
					oper.Beep(TYPE_KeyPress);
					return SC_Clear;
				default:
					oper.Beep(150, TYPE_Reject);
					oper.displayErrMessage("   NOT AVAILABLE    ");
                    goto start;
			}
			if(initialize) break;
		}
	}
	return SC_Success;
}

OperationStatusCode TCPIPConfig::ShowMACAddress(NETWORKTYPE nwtype)
{
	int timeout = oper.GetDataTimeout();
	user_output_clear();

	if(nwtype == TYPE_ETHERNET)
	{
		user_output_write(2, 4, "WIRED MAC ADDR");
	}
	else
	{
		user_output_write(2, 2, "WIRELESS MAC ADDR");
	}

	user_output_write(3, 2, GetMACAddress(nwtype));
	char input;

	while(true)
	{
		int tm = 0;
		while (!user_input_ready () && tm < timeout)
		{
			usleep(10000);
			tm += 20;
		}
		if(tm >= timeout)
		{
			oper.Beep(TYPE_TimeOut);
			return SC_TIMEOUT;
		}
		input = user_input_get();
		if(input == ESC)
		{
			oper.Beep(TYPE_KeyPress);
			return SC_Clear;
		}
		else
		{
			oper.Beep(150, TYPE_Reject);
		}
	}
	return SC_Success;
}
