#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>

#include "Str_Def.h"
#include "Operation.hpp"
#include "LegacyTrans.hpp"
#include "Legacy.hpp"
#include "InterfaceValidateEntryHolder.hpp"
#include "TAString.h"
#include "StdInput.h"
#include "asm/TA7000_IO.h"
#include "KeyboardReader.h"
#include "LCDHandler.h"
#include "XMLProperties.h"
extern LegacySetup SystemSetup;
#ifdef client_prologic
extern LegacySetup KeypadSetup;
#endif
/**
 * Badge Validation
 * 1. Check if badge length is greater than the max length
 * 2. Check if badge length is less than min length if min length not equal to zero
 * 3.
 */
Code Operation::ValidateBadge(char * badge)
{

	int max = SystemSetup.badge.length.maximum;
    //int min = SystemSetup.badge.length.minimum;
    int offset = SystemSetup.badge.length.offset;
    int validlen = SystemSetup.badge.length.valid;
    int type = SystemSetup.badge.length.type;


    // For now default to Numeric
    type = SysInputTypeNumeric;

    int badgelen = strlen(badge);

    if( badgelen == 0 || badgelen > max )
    {
	    return SC_FAILURE;
    }

    /**
     * Badge with all zeroes is a reserved badge. It's the default configuration
     * badge. When the user enters a badge that is less than the allowed badge
     * length we pad it. We should not pad for a badge of all zeroes since the
     * user can go to the configuration mode by accident
     */
    if( badgelen < SystemSetup.badge.length.valid )
    {
		for( int i=0; i<badgelen; i++)
		{
			if( * (badge + i) != '0' ) break;
			if( i == badgelen - 1 ) return SC_FAILURE;
		}
    }

    /*if( badgelen >= validlen + offset )
    {
    	char vbdg[validlen + 1];
    	strncpy(vbdg, badge + offset, validlen);
    	vbdg[validlen] = '\0';
    	strcpy(badge, vbdg);
    }*/

    // We need to pad if the badge length is less than the needed badge
    switch(type)
    {
    	case SysInputTypeNumeric:
    	{
			if(CheckInputIsNumeric(badge))
    		{
    			PadNumeric(badge, validlen + offset);
    		}
    		else  return SC_FAILURE;
    		break;
    	}
    	case SysInputTypeAlphaNumeric:
		{
			if(CheckInputIsAlphaNumeric(badge))
			{
				PadAlpha(badge, validlen + offset);
				return SC_FAILURE;
			}
			else  return SC_FAILURE;
			break;
		}
    }
    return SC_OKAY;
}

#ifdef client_prologic
Code Operation::ValidateKeypadBadge(char * badge)
{

	int max = KeypadSetup.badge.length.maximum;
    //int min = KeypadSetup.badge.length.minimum;
    int offset = KeypadSetup.badge.length.offset;
    int validlen = KeypadSetup.badge.length.valid;
    int type = KeypadSetup.badge.length.type;


    // For now default to Numeric
    type = SysInputTypeNumeric;

    int badgelen = strlen(badge);

    if( badgelen == 0 || badgelen > max )
    {
	    return SC_FAILURE;
    }

    /**
     * Badge with all zeroes is a reserved badge. It's the default configuration
     * badge. When the user enters a badge that is less than the allowed badge
     * length we pad it. We should not pad for a badge of all zeroes since the
     * user can go to the configuration mode by accident
     */
    if( badgelen < KeypadSetup.badge.length.valid )
    {
		for( int i=0; i<badgelen; i++)
		{
			if( * (badge + i) != '0' ) break;
			if( i == badgelen - 1 ) return SC_FAILURE;
		}
    }

    /*if( badgelen >= validlen + offset )
    {
    	char vbdg[validlen + 1];
    	strncpy(vbdg, badge + offset, validlen);
    	vbdg[validlen] = '\0';
    	strcpy(badge, vbdg);
    }*/

    // We need to pad if the badge length is less than the needed badge
    switch(type)
    {
    	case SysInputTypeNumeric:
    	{
			if(CheckInputIsNumeric(badge))
    		{
    			PadNumeric(badge, validlen + offset);
    		}
    		else  return SC_FAILURE;
    		break;
    	}
    	case SysInputTypeAlphaNumeric:
		{
			if(CheckInputIsAlphaNumeric(badge))
			{
				PadAlpha(badge, validlen + offset);
				return SC_FAILURE;
			}
			else  return SC_FAILURE;
			break;
		}
    }
    return SC_OKAY;
}
#endif


/**
 * Validate the Source
 */
Code Operation::ValidateSource(const SysInputSource source, SourceType type)
{
	Code code = SC_FAILURE;

//testing
					printf("ValidateSource::\n");
					switch ( source )
					{
						case SysInputSourceMagnetic :
							printf("badge inputsource is Magnetic\n");
							break;
						case SysInputSourceKeypad :
							printf("badge inputsource is Keypad\n");
							break;
						case SysInputSourceBarcode :
							printf("badge inputsource is Barcode\n");
							break;
						case SysInputSourceProximity :
							printf("badge inputsource is Proximity\n");
							break;
						case SysInputSourceFprint :
							printf("badge inputsource is Fingerprint\n");
							break;
						case SysInputSourceProxMag :
							printf("badge inputsource is ProxMag\n");
							break;
						default:
							printf("badge inputsource is Other: %d\n", source);
							break;

					}

//endtest
	switch(type)
	{
		case TYPE_SupervisorInputSource:
		{
			if ( (SystemSetup.source.supervisor & source) == source )
			{
				code = SC_OKAY;
			}
			else
			{
				code = SC_FAILURE;
			}
			break;
		}
		case TYPE_SupervisorEmployeeInputSource:
		{
			if ( (SystemSetup.source.super_employee & source) == source )
			{
				code = SC_OKAY;
			}
			else
			{
				code = SC_FAILURE;
			}
			break;
		}
		case TYPE_EmployeeInputSource:
		{
			// testing: print source value
//			printf("\n case TYPE_EmployeeInputSource");
//			if( (SystemSetup.source.initial & SysInputSourceKeypad) == SysInputSourceKeypad)
//				printf("\n init source = keypad");
//			if( (SystemSetup.source.initial & SysInputSourceProximity) == SysInputSourceProximity)
//				printf("\n init source = prox");
//			if( (SystemSetup.source.initial & SysInputSourceMagnetic) == SysInputSourceMagnetic)
//				printf("\n init source = mag");

//			if( (SystemSetup.source.initial & SysInputSourceBarcode) == SysInputSourceBarcode)
//				printf("\n init source = barcode");

//			if( (SystemSetup.source.initial & SysInputSourceDefault) == SysInputSourceDefault)
//				printf("\n init source = default");
//			printf("\n");
			//endtest

			if ( (SystemSetup.source.initial & source) == source )
			{
                code = SC_OKAY;
			}
			else
			{
				code = SC_FAILURE;
			}
			break;
		}
		default:
			code = SC_OKAY;
    }
    return code;
}

/**
 * Validates the Function
 * 1. Check if the entered function is valid
 * 2. if the Function is valid, check if it has Access Check and profile Check enabled.
 *    if so check the access and the profile tables
 */
OperationStatusCode Operation::ValidateFunction(const char * badge, const SysFunctionKey source,
													const BadgeType type, SysLevelData * levels,
													Bit8 * flags)
{
	OperationStatusCode code = SC_Invalid_Function;

 	/**
 	 * Check if the entered function is valid
 	 */
 	for(int i=0; SysFunctionKeyChars[i]!='\0'; i++)
 	{
		if(source == SysFunctionKeyChars[i])
		{
			code = SC_Allow;
           	break;
        }
 	}
 	if (code != SC_Allow)
 	{
    	return code;
 	}

 	code = SC_Invalid_Function;

 	//check if it has Access Check and profile check enabled
	SysFunctionKeyItem *keyPtr = SystemSetup.function.key;
 	Boolean accessCheck=false, profileCheck=false, levelCheck=false;
 	for (int j=0; j<SysFunctionKeyMax; j++)
 	{
		if (keyPtr->key == source)
      	{
      		code = SC_Allow;
      		if(SysFunctionFlagSupervisor == ( keyPtr->flags & SysFunctionFlagSupervisor ) && type != TYPE_Supervisor)
      		{
				return SC_Supervisor_Function;
      		}
			if(keyPtr->levels > 0)
          	{
          		levelCheck = true;
          	}
        	if(SysFunctionFlagAccess == ( keyPtr->flags & SysFunctionFlagAccess ))
          	{
            	accessCheck = true;
            	*flags |= ACCESS_REQUESTED;
          	}
        	if(SysFunctionFlagProfile == ( keyPtr->flags & SysFunctionFlagProfile ))
          	{
            	profileCheck = true;
            	*(flags+1) |= PROFILE_REQUESTED;
          	}
          	break;
      	}
    	keyPtr++;
  	}

  	if (code != SC_Allow)
	{
		return code;
	}

  	if(! (levelCheck || accessCheck || profileCheck))
	{
  	  		code = SC_Invalid_Function;
	}

  	if(SystemSetup.profile.mode.override)
  	    if((SystemSetup.profile.message->flags)&SysProfileFlagSupervisor)
  	    {
  		    if (type == TYPE_Supervisor)
  		    {
  		        //printf("override sup allowed \n");
  		        *(flags+3) |= CLOCK_ALLOWS_SUPERVISOR_OVERRIDE;
  	        }
  	    }
	/**
  	 * check profile table
  	 */
  	if(profileCheck)
  	{
    	//printf("we check profile\n");
    	code = CheckProfileForBadge(badge, *(flags+1), *(flags+2),*(flags+3));
  	}
  	/**
  	 * Display and Validate the levels associated with this function
  	 */
	if( code == SC_Allow && levelCheck )
	{
  		//printf("we check levels\n");
        code = CheckLevelsForFunction(keyPtr, badge, levels);
	}

 	/**
  	 * check access table. if profile allows access
  	 */
  	if( code == SC_Allow && accessCheck )
  	{
  		code = CheckAccessForBadge(badge, *(flags), *(flags+3));
	}

    return code;
}

/**
 * Iterate through all the levels for the function and display the message
 */
OperationStatusCode Operation::CheckLevelsForFunction(const SysFunctionKeyItem *keyPtr, const char *badge, SysLevelData * levels)
{
	LCDHandler lcd;

	//printf("keyPtr->levels = %d\n",keyPtr->levels);
	for(SysFunctionLevel level = 1; level <= keyPtr->levels; level++)
	{
		SysFunctionDetailItem *detail = SystemSetup.function.detail;
		for(SysIndex i = 0; i < SysFunctionDetailMax; i++, detail++)
		{
			if (detail->key == keyPtr->key && detail->level == level )
			{
				//printf("detail->message = %s\n",detail->message);
				if( strlen(detail->message) == 0 )
					continue;
				char * fnmsg = TrimString(detail->message);
				int col = ( 20 - strlen(fnmsg) ) / 2 + 1;
			//	printf("\n detail->source = %#x", detail->source);
				if( detail->source != 0x00 )
				{
					int maxlen = (int)detail->length.max;
					int col1 = ( 20 - maxlen ) / 2 + 1;
					SysFieldLength min = detail->length.min;
					// Assumption that the Leveldata is never greater than 24
					char input[25];
					Boolean success = true;
					do
					{
						GetLevelData:
						#ifdef __TA7700_TERMINAL__
						user_output_write(2, 1, BLANK_LINE);
						user_output_write(3, 1, BLANK_LINE);
						user_output_write(4, 1, BLANK_LINE);
				 		//printf("detail->message = %s\n",fnmsg);
				 		user_output_write(2, col, fnmsg);
				 		user_output_write(2, 1, "\n");
				 		Beep(150, TYPE_Accept);
						int timeout = GetDataTimeout();
						LCD_READ_STATE state = READ_SUCCESS;

						// The SysInputType template for FP is a char template now.
						char ch = detail->type;
						if(ch == ' ')
							detail->type =  SysInputTypeDefault;
						if(ch == '1')
							detail->type =  SysInputTypeAlpha;
						if(ch == '2')
							detail->type =  SysInputTypeNumeric;
						if(ch == '3')
							detail->type =  SysInputTypeAlphaNumeric;
						if(ch == '4')
							detail->type =  SysInputTypeDecimal;

						switch(detail->type)
						{
							case SysInputTypeAlpha:
							{
								for(int i=0; i<maxlen; i++) input[i] = ' ';
								input[maxlen] = '\0';
								//state = lcd.ReadLevelData(SysInputTypeAlpha, 3, col1, timeout, maxlen, min, input);
								state = lcd.ReadLevelData(SysInputTypeAlpha, detail->source, 3, col1, timeout, maxlen, min, input);

								if(state == READ_TIMEDOUT)
								{
									return SC_TIMEOUT;
								}
								else if(state == READ_CLEAR)
								{
									return SC_Clear;
								}
								if(!CheckInputIsAlpha(input))
								{
									user_output_write(3, 1, "PLEASE ENTER ALPHABETIC CHARACTERS");
									TurnOnLEDYellow();
									Beep(TYPE_Error);
									usleep(GetErrorMsgTimeout() * 1000);
									TurnOffLEDYellow();

									goto GetLevelData;
								}
								// Make sure if the min number of characters are entered
								if(strlen(input) < min || strlen(input) > maxlen )
								{
									user_output_write(3, 1, "   INVALID ENTRY    ");
									user_output_write(4, 1, " PLEASE ENTER AGAIN ");
									TurnOnLEDYellow();
									Beep(TYPE_Error);

									usleep(GetErrorMsgTimeout() * 1000);
									TurnOffLEDYellow();

									goto GetLevelData;
								}
								break;
							}
							case SysInputTypeNumeric:
							{
								for(int i=0; i<maxlen; i++) input[i] = ' ';
								input[maxlen] = '\0';
								//state = lcd.ReadLevelData(SysInputTypeNumeric, 3, col1, timeout, maxlen, min, input);
								state = lcd.ReadLevelData(SysInputTypeNumeric, detail->source, 3, col1, timeout, maxlen, min, input,detail->defaultData);
								//printf("input = %s\n",input);
								if(state == READ_TIMEDOUT)
								{
									return SC_TIMEOUT;
								}
								else if(state == READ_CLEAR)
								{
									return SC_Clear;
								}
								if(!CheckInputIsNumeric(input))
								{
									user_output_write(3, 1, "PLEASE ENTER NUMERIC CHARACTERS");
									TurnOnLEDYellow();
									Beep(TYPE_Error);
									usleep(GetErrorMsgTimeout() * 1000);
									TurnOffLEDYellow();

									goto GetLevelData;
								}
								// Make sure if the min number of characters are entered
								//printf("input = %s \n",input);
								if(strlen(input) < min || strlen(input)>maxlen )
								{
									user_output_write(3, 1, "   INVALID ENTRY    ");
									user_output_write(4, 1, " PLEASE ENTER AGAIN ");
									TurnOnLEDYellow();
									Beep(TYPE_Error);

									usleep(GetErrorMsgTimeout() * 1000);
									TurnOffLEDYellow();

									goto GetLevelData;
								}
								break;
							}
							case SysInputTypeAlphaNumeric:
							{
								for(int i=0; i<maxlen; i++) input[i] = ' ';
								input[maxlen] = '\0';
								//state = lcd.ReadLevelData(SysInputTypeAlphaNumeric, 3, col1, timeout, maxlen, min, input);
								state = lcd.ReadLevelData(SysInputTypeAlphaNumeric, detail->source, 3, col1, timeout, maxlen, min, input,detail->defaultData);
								//printf("data level return %s \n",input);
								if(state == READ_TIMEDOUT)
								{
									return SC_TIMEOUT;
								}
								else if(state == READ_CLEAR)
								{
									return SC_Clear;
								}
								if(strlen(input) < min)
								if(!CheckInputIsAlphaNumeric(input))
								{
									user_output_write(3, 1, "PLEASE ENTER ALPHANUMERIC CHARACTERS");
									TurnOnLEDYellow();
									Beep(TYPE_Error);
									usleep(GetErrorMsgTimeout() * 1000);
									TurnOffLEDYellow();
									goto GetLevelData;
								}
								// Make sure if the min number of characters are entered
								if(strlen(input) < min || strlen(input)>maxlen)
								{
									user_output_write(3, 1, "   INVALID ENTRY    ");
									user_output_write(4, 1, " PLEASE ENTER AGAIN ");
									TurnOnLEDYellow();
									Beep(TYPE_Error);

									usleep(GetErrorMsgTimeout() * 1000);
									TurnOffLEDYellow();

									goto GetLevelData;
								}
								break;
							}
							case SysInputTypeDecimal:
							{
								for(int i=0; i<maxlen; i++) input[i] = ' ';
								input[maxlen] = '\0';

								//printf("BEFOR READ DECIMAL = %s\n",input);
								state = lcd.ReadDecimal(3, col, timeout, maxlen, min, detail->decimal, input,detail->defaultData);
								//printf("DECIMAL INPUT = %s detail->decimal = %d\n",input,detail->decimal);
								//printf("data level return %s \n",input);
								if(state == READ_TIMEDOUT)
								{
									return SC_TIMEOUT;
								}
								else if(state == READ_CLEAR)
								{
									return SC_Clear;
								}
								if(!CheckInputIsDecimal(input, detail->decimal))
								{
									user_output_write(3, 1, "PLEASE ENTER DECIMAL");
									TurnOnLEDYellow();
									Beep(TYPE_Error);
									usleep(GetErrorMsgTimeout() * 1000);
									TurnOffLEDYellow();
									goto GetLevelData;
								}
								int comma  = (detail->decimal > 0)?1:0;
								if((detail->decimal > 0) && maxlen <= (detail->decimal))
								        comma = (2+(detail->decimal-maxlen));
								if(strlen(input)-comma < min  || strlen(input)-comma > maxlen )
								{
									user_output_write(3, 1, "   INVALID ENTRY    ");
									user_output_write(4, 1, " PLEASE ENTER AGAIN ");
									TurnOnLEDYellow();
									Beep(TYPE_Error);

									usleep(GetErrorMsgTimeout() * 1000);
									TurnOffLEDYellow();

									goto GetLevelData;
								}
								break;
							}

						}
						#else
						printf("%s\n", detail->message);
						scanf("%s", input);
						#endif
						if( detail->validation.flags & SysInputValidBoth )
						{
							switch(detail->type)
							{
								case SysInputTypeAlpha:
								case SysInputTypeAlphaNumeric:
									PadAlpha(input, maxlen);
									break;
								case SysInputTypeNumeric:
									PadNumeric(input, maxlen);
									break;
							}
							//printf("ValidateTable\n");
							success = ValidateTable(badge, input, detail->validation.table);
						}
						if(success)
						{
							levels->Source = SysInputSourceKeypad;
							strcpy(levels->Data, input);
							levels++;
						}
					}while(!success);
				}
				else
				{
					#ifdef __TA7700_TERMINAL__

					char input[25];
					input[0] = ' ';
					input[1] = '\0';

					levels->Source = SysInputSourceKeypad;
					strcpy(levels->Data, input);
					levels++;
					//user_output_write(2, 1, BLANK_LINE);
					//user_output_write(3, 1, BLANK_LINE);

					///////////////////////////////////////
                    /*XMLProperties parser;
					char* prop;
				    prop = parser.GetProperty(http->chunk.memory,"Status");
                    char status[2];
					//printf("xml= %s \n",http->smg.xml);
					printf("prop = %s\n",prop);
					strcpy(status,prop);
                    prop = parser.GetProperty(http->chunk.memory,"Message");
					if(prop && strcmp(status,"1")==0)
					{
							strcpy(fnmsg,prop);
					        col = ( 20 - strlen(fnmsg) ) / 2 + 1;
					}
                    //http->chunk.memory[0] = '/0';*/
					//////////////////////////////////////

					//printf("fun = %s\n",fnmsg);
					//user_output_write(2, col, fnmsg);
					//TurnOnLEDGreen();
					//lastFun[0] = '\0';
                    //strcpy(lastFun,fnmsg);
					//int mtm = GetMessageTimeout();
					//Beep(TYPE_Accept);
					//if(mtm > 500)
					//{
					//	usleep(500000);
					//	Beep(TYPE_TurnOff);
					//	mtm -= 500;
					//}
					//usleep(mtm * 1000);
					//Beep(TYPE_TurnOff);
					//TurnOffLEDGreen();

					#else
					printf("%s\n", detail->message);
					#endif
				}
			}
		}
	}
	return SC_Allow;
}

Boolean Operation::ValidateTable(const char * badge, char * buffer, SysInputTable table)
{
	SysValidateLevelItem * level = SystemSetup.validate.level;
	level += table;
	int len = strlen(buffer);
	//printf("len_badge = %d level_width = %d\n",len,level[0].width);
	//printf("level_width = %d\n",level[1].width);
	//printf("level_width = %d\n",level[2].width);
	//printf("level_width = %d\n",level[3].width);
	if(len != (int)level->width)
	{
		/**
		 * Display the error message
		 */
		#ifdef __TA7700_TERMINAL__
		const char * emsg = TrimString(level->error_message);
		int col = ( 20 - strlen(emsg) ) / 2 + 1;
		user_output_clear();
		user_output_write(2, col, emsg);
		TurnOnLEDYellow();
		Beep(TYPE_Error);
		usleep(GetErrorMsgTimeout() * 1000);
		TurnOffLEDYellow();
		#else
		printf("%s\n",level->error_message);
		#endif
		return false;
	}
	else
	{
		char str[strlen(buffer) + strlen(badge) + 1];
		/**
		 * if the Employee badge was appended in the table data
		 * we need to append the badge before comparison
		 */
		if(level->include_badge)
		{
			//printf("valid include badge\n");
			strcpy(str, buffer);
			strcat(str, badge);
			buffer = str;
		}
		/**
		 * Check is the entry exists
		 */
		InterfaceValidateEntryHolder * holder = (InterfaceValidateEntryHolder *)SystemSetup.validate.entry;
		//printf("entry size = %d\n",holder->size());
		for(int i=0; i<holder->size(); i++)
		{
			SysValidateEntryItem item = holder->Get(i);
			//printf("item.level=%d table=%d strcmp=%d\n",item.level,table,strcmp(buffer,item.data));
			if( item.level == table && strcmp(buffer,item.data) == 0 )
			{
				#ifdef __TA7700_TERMINAL__
				const char * imsg = TrimString(item.message);
				int col = ( 20 - strlen(imsg) ) / 2 + 1;
				user_output_clear();
				user_output_write(2, col, imsg);
				usleep(GetMessageTimeout() * 1000);
				#else
				printf("%s\n", item.message);
				#endif
				return true;
			}
		}
		#ifdef __TA7700_TERMINAL__
		const char * emsg = TrimString(level->error_message);
		int col = ( 20 - strlen(emsg) ) / 2 + 1;
		user_output_clear();
		user_output_write(2, col, emsg);
		TurnOnLEDYellow();
		Beep(TYPE_Error);
		usleep(GetErrorMsgTimeout() * 1000);
		TurnOffLEDYellow();
		#else
		printf("%s\n", level->error_message);
		#endif
		return false;
	}
}

/**
 * Validate the Date
 * Date for mat is MMDDYYYY
 */
Code Operation::ValidateDate(const char * date)
{
    Code code = SC_OKAY;
    int len = strlen(date);
    if(len != 8) return SC_FAILURE;

    // Get the Current Date
    time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );

	// Validate the Year
	TAString str = date;
	TAString yr = str.substr(0,4);
	int year = atoi(yr.c_str());
	if( year == 0 || year > (int)optimeinfo->tm_year + 1900 ) return SC_FAILURE;

	// Validate the Month
	TAString mth = str.substr(4,2);
	int month = atoi(mth.c_str());
	if( month == 0 || month > 12 ) return SC_FAILURE;

	// Validate the Day
	TAString dy = str.substr(6,2);
	int day = atoi(dy.c_str());

	// Check if the Conversion failed
	if( day == 0 )
	{
		return SC_FAILURE;
	}

	if( (month == 4 || month == 6 || month == 9 || month == 11) && day > 30 )
	{
		return SC_FAILURE;
	}
	else if( month == 2 && year % 4 == 0 && day > 29 )
	{
		return SC_FAILURE;
	}
	else if( month == 2 && year % 4 != 0 && day > 28 )
	{
		return SC_FAILURE;
	}
	else if( day > 31 )
	{
		return SC_FAILURE;
	}

    return code;
}

/**
 * Validate the Time
 */
Code Operation::ValidateTime(const char * time)
{
    Code code = SC_OKAY;
    int len = strlen(time);
    if(len != 4) return SC_FAILURE;

    // Validate the Time
	TAString str = time;
	TAString hr = str.substr(0,2);
	int hour = atoi(hr.c_str());
	if( hour < 0 || hour > 23 ) return SC_FAILURE;
	TAString mins = str.substr(2,2);
	int min = atoi(mins.c_str());
	if( min < 0 || min > 59 ) return SC_FAILURE;

	return code;
}

/**
 * Check if the user has a Profile Assigned
 * if so get the Profile Entry and validate the time
 */
OperationStatusCode Operation::CheckProfileForBadge(const char * badge, Bit8 & profileflag, Bit8 & profilenum , Bit8  & supOverride)
{
	OperationStatusCode code = SC_Profile_Denied;
	/* flags used to display default message */
	Boolean badgeFound = false, profileFound = false;
	InterfaceProfileAssignHolder * holder = SystemSetup.profile.assign;
	if(holder->size() > 0)
	{
		//printf("size = %d\n", holder->size());
		profileflag |= PROFILE_TABLE_PRESENT;
	}
    for(int i = 0; i < holder->size(); i++ )
    {
        const SysProfileAssignItem& item = holder->Get(i);
        /**
         * Check if there is a profile for the user
         * if so get the profile entry number for today
         */
        // printf("badge = %s , item_badge = %s\n",badge,item.badge);
        if( strcmp(badge,item.badge) == 0 )
        {
        	badgeFound = true;
        	profileflag |= PROFILE_BADGE_IN_TABLE;
			SysIndex entnum = item.entry[GetDayOfWeek()];
			SysProfileEntryItem * entry = SystemSetup.profile.entry;
			//printf("badges is ok\n");
			//printf("ysProfileEntryMax = %d\n",SysProfileEntryMax);
			for( int i = 0; i < SysProfileEntryMax ; i++ )
			{
				if(entry != NULL && entry->time != NULL && entnum == entry->index)
				{

					for( SysIndex j = 0; j < SysProfileTimeMax ;j++ )
					{
						/**
						 * Check for invalid entries
						 */

						if ( entry->time[j].message == 0 &&
								entry->time[j].window.start == 0 &&
								entry->time[j].window.stop == 0 )
                        {
							//printf("continue in ->%d\n ",j);
							continue;
                        }
                        else
                        {
                        	//printf("")
                        	SysTime curtime = GetTime();
							//printf("J=%d current = %d start = %d end = %d\n",j,curtime,entry->time[j].window.start,entry->time[j].window.stop);
							if(curtime >= entry->time[j].window.start && curtime < entry->time[j].window.stop)
							{
								/**
								 * Get the Message Index and display the message
								 */
								// printf("enty time %d in the current time\n",j);
								if(supOverride & CLOCK_ALLOWS_SUPERVISOR_OVERRIDE)
								{
								    printf("continue\n");
								    continue;

								}
								SysProfileMessageItem * msg_p = SystemSetup.profile.message;
								msg_p += entry->time[j].message;
								profilenum |= entry->time[j].message;
								profileflag |= PROFILE_TIME_COVERED;
								code = CheckProfileMessageFlags(msg_p, profileflag, profilenum);
								profileFound = true;
								return code;
							}
                        }
					}
				}
				entry++;//move the pointer
			}
        }
    }

    /* display default message when badge not found */
    if (!badgeFound)
    {
		profilenum = BADGE_NOT_IN_PROFILE_ASSIGN_TABLE;
    	SysProfileMessageItem * msg_p = SystemSetup.profile.defaults;
    	msg_p++;
    	code = CheckProfileMessageFlags(msg_p, profileflag, profilenum);
    }
    /* display default message when profile not found */
    else if(!profileFound)
    {
    	profilenum = PROFILE_NOT_IN_PROFILE_ASSIGN_TABLE;
		SysProfileMessageItem * msg_p = SystemSetup.profile.defaults;
		code = CheckProfileMessageFlags(msg_p, profileflag, profilenum);
    }

    return code;
}

/**
 * Checks Profile Message Flags
 * 1. Beeps an accept or reject tone
 * 2. Checks if the employee is allowed entry or not
 * 3.
 */
OperationStatusCode Operation::CheckProfileMessageFlags(const SysProfileMessageItem * msgitem, Bit8 & profileflag, Bit8 & profilenum)
{
	OperationStatusCode code = SC_Profile_Denied;
	/**
	 * Check if its an accept tone or reject tone
	 */
	if((msgitem->flags & SysProfileFlagToneMask) == SysProfileFlagToneAccept)
	{
		/**
		 * Beep an Accept Tone
		 */
		TurnOnLEDGreen();
		Beep(500, TYPE_Accept);
	}
	else if((msgitem->flags & SysProfileFlagToneMask) == SysProfileFlagToneReject)
	{
		/**
		 * Beep a Reject Tone
		 */
		TurnOnLEDRed();
		Beep(TYPE_Reject);
	}
	else if((msgitem->flags & SysProfileFlagToneMask) == SysProfileFlagToneMask)
	{
		/**
		 * Beep both tones
		 */
		Beep(500, TYPE_Reject);
	}

	/**
	 * Display the message for the given time
	 */
	#ifdef __TA7700_TERMINAL__
	#ifdef DEBUG
	printf("%s\n", msgitem->text);
	#endif
	char temp[strlen(msgitem->text) + 1];
	strcpy(temp, msgitem->text);
	char * msg = TrimString(temp);
	if( msg != NULL )
	{
		int col = ( 20 - strlen(msg) ) / 2 + 1;
		user_output_clear();
		user_output_write(2, col, msg);
		//usleep(GetMessageTimeout() * 1000);
	}
	#else
	printf("%s\n", msgitem->text);
	#endif

	/**
	 * Check if the the employee is allowed to perform this function
	 */
	if((msgitem->flags & SysProfileFlagAllow) == SysProfileFlagAllow)
	{
		/**
		 * Sleep for timeout/10 seconds
		 */
		sleep(msgitem->timeout/10);
		profileflag |= PROFILE_TRANSACTION_COMPLETED;
		code = SC_Allow;
	}
	/**
	 * Check if the Supervisor can override
	 */
	else if(SystemSetup.profile.mode.override && (msgitem->flags & SysProfileFlagSupervisor) == SysProfileFlagSupervisor)
	{
		char * superbadge = new char[SysFieldMaxBadgeLength+1];

		/* Initialize the file descriptor set. */
		fd_set set;
		FD_ZERO (&set);

		/* Initialize the timeout data structure. */
		struct timeval timeout;
		timeout.tv_sec = msgitem->timeout/10;
		timeout.tv_usec = 0;

		#ifdef __TA7700_TERMINAL__
		mkfifo(PIPE_KEYBOARDREADER, O_WRONLY);
		pthread_t kbReaderThread;
		pthread_attr_t kbReaderThreadAttr;
		pthread_attr_init(&kbReaderThreadAttr);
		pthread_attr_setstacksize ( & kbReaderThreadAttr , const_thread_stack_size ());
		pthread_create (&kbReaderThread, &kbReaderThreadAttr, &keyboardReader, &(timeout.tv_sec));
		int keybrdPipe = open (PIPE_KEYBOARDREADER, O_RDWR);
		if(keybrdPipe >= 0)
		{
			FD_SET (keybrdPipe, &set);

			/* select returns 0 if timeout, 1 if input available, -1 if error. */
			int ret = select (keybrdPipe + 1, &set, NULL, NULL, &timeout);
			if( ret == 1)
			{
				read(keybrdPipe,superbadge,SysFieldMaxBadgeLength+1);
				Code cd = ValidateBadge(superbadge);
				if(cd != SC_OKAY)
				{
					code = SC_Profile_Denied;
				}
				if( GetBadgeType(superbadge) == TYPE_Supervisor )
				{
					code = SC_Allow;
					profileflag |= PROFILE_TRANSACTION_COMPLETED;
					profileflag |= PROFILE_SUPERVISOR_OVERRIDE;
				}
				else
				{
					code = SC_Profile_Denied;
				}
			}
			close(keybrdPipe);
		}
		pthread_kill(kbReaderThread, 0);
		#else
		FD_SET (STDIN_FILENO, &set);

		/* select returns 0 if timeout, 1 if input available, -1 if error. */
		int ret = select (FD_SETSIZE, &set, NULL, NULL, &timeout);
		if( ret == 1)
		{
			read(0,superbadge,SysFieldMaxBadgeLength+1);
			Code cd = ValidateBadge(superbadge);
			if(cd != SC_OKAY)
			{
				code = SC_Profile_Denied;
			}
			if( GetBadgeType(superbadge) == TYPE_Supervisor )
			{
				code = SC_Allow;
				profileflag |= PROFILE_TRANSACTION_COMPLETED;
				profileflag |= PROFILE_SUPERVISOR_OVERRIDE;
			}
			else
			{
				code = SC_Profile_Denied;
			}
		}
		#endif
		delete[] superbadge;
		superbadge = NULL;
	}
	else
	{
		sleep(msgitem->timeout/10);
	}
	Beep(TYPE_TurnOff);
	TurnOffLEDGreen();
	TurnOffLEDRed();
	return code;
}

OperationStatusCode Operation::CheckAccessForBadge(const char * badge, Bit8 & accessflag, Bit8 & accessoverride)
{
	OperationStatusCode code = SC_Access_Denied;

	/**
	 * Check if it access only clock
	 */
	if( SystemSetup.mode.access_only)
	{
		accessoverride |= ACCESS_ONLY_CLOCK;
	}

	/**
	 * Step 1. Check if the time is within the Access Window
	 */
	if(CheckIfInAccessWindow())
	{
		accessoverride |= ( INSIDE_ACCESS_WINDOW | DURING_ALWAYS_OPEN );
		return SC_Allow;
	}
	else
	{
		accessoverride |= DURING_ALWAYS_CLOSED;
	}

	/*
	 * Step 2. Check if the badge is present in the Access table
	 */
	InterfaceAccessControlHolder * holder = SystemSetup.access.badge;
	if(holder->size() > 0)
	{
		accessflag |= ACCESS_TABLE_PRESENT;
	}
    for(int i = 0; i < holder->size(); i++ )
    {
		const SysAccessBadgeItem& item = holder->Get(i);
		/**
         * Check if there is a profile for the user
         * if so get the profile entry number for today
         */
		if( strcmp(item.badge, "A") == 0 )
		{
			accessflag |= ACCESS_BADGE_IN_TABLE;
			return SC_Allow_Access;
		}
        if( strcmp(badge, item.badge) == 0 )
        {
			accessflag |= ACCESS_BADGE_IN_TABLE;
        	return SC_Allow_Access;
        }
    }
    return code;
}
