#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include "Str_Def.h"
#include "Operation.hpp"
#include "LegacyTrans.hpp"
#include "Legacy.hpp"
#include "OperationUtil.hpp"
#include "TAString.h"
#include "SystemGlobals.h"
#include "XMLProperties.h"
#ifdef __TA7700_TERMINAL__
extern "C"
{
	#include "User.h"
}
#endif

FPReaderStatus Operation::fp_reader_status = FP_DISABLED;
WatchDogStatus Operation::watchdogstatus = WATCH_DISABLED;
bool Operation::auto_restart = false;

static char datebuf[30];
static char timebuf[30];
static const char * STR_MON = "MON";
static const char * STR_TUE = "TUE";
static const char * STR_WED = "WED";
static const char * STR_THU = "THU";
static const char * STR_FRI = "FRI";
static const char * STR_SAT = "SAT";
static const char * STR_SUN = "SUN";

static const char * STR_JAN = "JAN";
static const char * STR_FEB = "FEB";
static const char * STR_MAR = "MAR";
static const char * STR_APR = "APR";
static const char * STR_MAY = "MAY";
static const char * STR_JUN = "JUN";
static const char * STR_JUL = "JUL";
static const char * STR_AUG = "AUG";
static const char * STR_SEP = "SEP";
static const char * STR_OCT = "OCT";
static const char * STR_NOV = "NOV";
static const char * STR_DEC = "DEC";


int  Operation::iTimeZone = 0;
char Operation::strDaylightSavingsStart[8]; // MM/DD
char Operation::strDaylightSavingsEnd[8]; // MM/DD
//char Operation::strDateTimeFormat[32];

LegacySetup SystemSetup;
#ifdef client_prologic
LegacySetup KeypadSetup;
#endif

pthread_mutex_t Operation::localtime_mutex = PTHREAD_MUTEX_INITIALIZER;


void Operation::SYClock_GetTime(struct tm * tinfo)
{
 	time_t ztime;
	time(&ztime);

	pthread_mutex_lock(&(Operation::localtime_mutex));
	memcpy(tinfo,localtime(&ztime),sizeof(struct tm));
	pthread_mutex_unlock(&(Operation::localtime_mutex));

	tinfo->tm_hour += Operation::iTimeZone;
	tinfo->tm_isdst = -1;
	mktime(tinfo);
	tinfo->tm_isdst = -1;
	Operation::strDaylightSavingsStart[2] = '\0';
	Operation::strDaylightSavingsEnd[2] = '\0';
	int dstMon = atoi( strDaylightSavingsStart ) - 1;
	int dstDay = atoi( &(Operation::strDaylightSavingsStart[3]) );
	int dstMonEnd = atoi( Operation::strDaylightSavingsEnd ) - 1;
	int dstDayEnd = atoi( &(Operation::strDaylightSavingsEnd[3]) );
	if( (tinfo->tm_mon == dstMon && tinfo->tm_mday > dstDay) ||
		(tinfo->tm_mon == dstMonEnd && tinfo->tm_mday < dstDayEnd) ||
		(tinfo->tm_mon == dstMon && tinfo->tm_mday == dstDay && tinfo->tm_hour >= 2) ||
		(tinfo->tm_mon == dstMonEnd && tinfo->tm_mday == dstDayEnd && tinfo->tm_hour < 2) ||
		(tinfo->tm_mon > dstMon && tinfo->tm_mon < dstMonEnd) )
	{
		tinfo->tm_hour += 1;
		mktime( tinfo );
	}
}

FPIDMode Operation::GetFPIDMode()
{
	if(SystemSetup.fptemplate.mode == '0')
	{
		return VERIFY_MODE;
	}
	else
		return IDENTIFY_MODE;
}

int Operation::GetSecurityLevel()
{
	char s_level = SystemSetup.fptemplate.threshhold;
	int securitylevel = atoi(&s_level);
	return securitylevel;
}

/**
 * Read the Badge from the Offset to BL characters
 */
void Operation::ReadBadgeOffset(char * badge)
{
	int offset = SystemSetup.badge.length.offset;
	int validlen = SystemSetup.badge.length.valid;
	TAString bdg = badge;
	TAString str = bdg.substr(offset,validlen);
	strcpy(badge, str.c_str());
}

#ifdef client_prologic
/**
 * Read the Badge from the Offset to BL characters for the Keypad Badge
 */
void Operation::ReadKeypadOffset(char * badge)
{
	int offset = KeypadSetup.badge.length.offset;
	int validlen = KeypadSetup.badge.length.valid;
	TAString bdg = badge;
	TAString str = bdg.substr(offset,validlen);
	strcpy(badge, str.c_str());
}
#endif

BadgeType Operation::GetBadgeType(const char * badge)
{
	/**
	 * ###########################################################
	 * ################## LATER VERSION ##########################
	 * ###########################################################
	 * Check if it is Configuration badge
	 * The tag for Configuration is CP. Sometimes this
	 * tag is not sent with the clock program file, in this
	 * case we assume badge with all zeroes as configuration badge
	 */
	//char * cfg = SystemSetup.badge.prefix.configuration;
	/*if(cfg != NULL)
	{
		if(strcmp(badge,cfg) == 0 )
		{
			return TYPE_Configuration;
		}
		//Need to check for the condition where length of "CP" is greateer than
		//the badge length. Thew we need to check for only the "BL" characters
		else if(SystemSetup.badge.length.valid < strlen(cfg))
		{
			TAString cfgstr = cfg;
			TAString prefix = cfgstr.substr(0,SystemSetup.badge.length.valid);
			if( strcmp(badge,prefix.c_str()) == 0 )
			{
				return TYPE_Configuration;
			}
		}
	}*/

	int vbl = SystemSetup.badge.length.valid;

	// If the CP is not found check Access Code
	char * accesscode = GetAccessCode();
	if( accesscode == NULL || strlen( accesscode ) == 0 )
	{
		// If the Badge has all zeroes then its a configuration badge
		for( int i=0; i<SystemSetup.badge.length.valid; i++)
		{
			if( * (badge + i) != '0' ) break;
			if( i == SystemSetup.badge.length.valid - 1 )
			 {
			    return TYPE_Configuration;
		     }
		}
	}
	else
	{
		/**
		 * If the badge length is less than the accesscode, we only check for
		 * the badge length characters. If the badge length is greater than the
		 * accesscode, we check for the prefix of badge
		 */
		int aclen = strlen( accesscode );
		if( aclen == vbl )
		{
			if( strcmp( accesscode, badge ) == 0 ) return TYPE_Configuration;
		}
		else if( aclen > vbl )
		{
			TAString acstr = accesscode;
			TAString prefix = acstr.substr(0, vbl);
			if( strcmp(badge, prefix.c_str()) == 0 )
			{
				return TYPE_Configuration;
			}
		}
		else if( aclen < vbl )
		{
			TAString bdgstr = badge;
			TAString prefix = bdgstr.substr(0, aclen);
			if( strcmp(accesscode, prefix.c_str()) == 0 )
			{
				return TYPE_Configuration;
			}
		}
	}

	/**
     * User may have set an access code, hence, a badge with all zeroes
     * becomes invalid (reserved)
     */
	for( int i=0; i<SystemSetup.badge.length.valid; i++)
	{
		if( * (badge + i) != '0' ) break;
		if( i == SystemSetup.badge.length.valid - 1 ) return TYPE_Invalid;
	}

	/**
	 * Check if it is supervisor
	 * The supervisor can be of type Entry or Recall.
	 * Check for both conditions
	 */
	char * entprefix = SystemSetup.badge.prefix.supervisor.entry;
	int len = 0;
	if( entprefix != NULL ) len = strlen(entprefix);
	if(len != 0)
	{
		if( len <= vbl ) // sanity check
		{
			TAString x = badge;
			TAString entbadgeprefix = x.substr(0, len);
			if(strcmp(entprefix,entbadgeprefix.c_str()) == 0 )
			{
                return TYPE_Supervisor;
			}
		}
	}

	// Check for Recall
	char * recprefix = SystemSetup.badge.prefix.supervisor.recall;
	len = 0;
	if( recprefix != NULL ) len = strlen(recprefix);
	if(len != 0)
	{
		if( len <= vbl ) // sanity check
		{
			TAString x = badge;
			TAString recbadgeprefix = x.substr(0,len);
			if(strcmp(recprefix,recbadgeprefix.c_str()) == 0 )
			{
				return TYPE_Supervisor;
			}
		}
	}

	/**
	 * Check if it is Swipe and Go
	 */
	char * sg = SystemSetup.badge.swipe_n_go.limit;
	if( sg != NULL && strcmp(badge, sg) <= 0 )
	{
		return TYPE_Swipe_And_Go;
	}

	/**
	 * Its an employee badge
	 */
	return TYPE_Employee;
}

/**
 * Determine if its a supervisor of type Entry or Recall
 */
SupervisorType Operation::GetSupervisorType(const char * badge)
{
	char * entprefix = SystemSetup.badge.prefix.supervisor.entry;
	int len = strlen(entprefix);
	if(len != 0)
	{
		TAString x = badge;
		TAString entbadgeprefix = x.substr(0,len);
		if(strcmp(entprefix,entbadgeprefix.c_str()) == 0 )
		{
			return TYPE_Supervisor_Entry;
		}
	}

	char * recprefix = SystemSetup.badge.prefix.supervisor.recall;
	len = strlen(recprefix);
	if(len != 0)
	{
		TAString x = badge;
		TAString recbadgeprefix = x.substr(0,len);
		if(strcmp(recprefix,recbadgeprefix.c_str()) == 0 )
		{
			return TYPE_Supervisor_Recall;
		}
	}
	return TYPE_Supervisor_None;
}

/**
 * Returns the Function for Swipe and Go
 */
char Operation::GetSwipeAndGoFunction()
{
	return SystemSetup.badge.swipe_n_go.function;
}

/**
 * Returns the Valid Badge Length
 */
int Operation::GetMaxBadgeLen()
{
	if( SystemSetup.badge.length.maximum > 0 ) return SystemSetup.badge.length.maximum;
	return SystemSetup.badge.length.valid;
}

/**
 * Returns the data timeout in milliseconds
 */
int Operation::GetDataTimeout()
{
	// The system defines timeout inn 10ths of a second
	if( SystemSetup.user.timeout.data <= 0 )
		return 15000; // Default is 15 Seconds
	else
		return SystemSetup.user.timeout.data * 100;
}

/**
 * Returns the Access Duration in Seconds
 */
int Operation::GetAccessDuration()
{
	return SystemSetup.access.duration / 10;
}

/**
 * Returns the function timeout in milliseconds
 */
int Operation::GetFunctionTimeout()
{
	// The system defines timeout inn 10ths of a second
	if( SystemSetup.user.timeout.function <= 0 )
		return 10000; // Default is 10 Seconds
	else
		return SystemSetup.user.timeout.function * 100;
}

/**
 * Returns the message timeout in milliseconds
 */
int Operation::GetMessageTimeout()
{
	// The system defines timeout inn 10ths of a second
	if( SystemSetup.user.timeout.message <= 0 )
		return 2000; // Default is 2 Seconds
	else
		return SystemSetup.user.timeout.message * 100;
}

/**
 * Returns the supervisor mode timeout in milliseconds
 */
int Operation::GetSupervisorModeTimeout()
{
	// The system defines timeout inn 10ths of a second
	if( SystemSetup.user.timeout.data <= 0 )
		return 20000; // Default is 20 Seconds
	else
		return SystemSetup.user.timeout.supervisor * 100;
}

/**
 * Returns the error message timeout in milliseconds
 */
int Operation::GetErrorMsgTimeout()
{
	// The system defines timeout inn 10ths of a second
	if( SystemSetup.user.timeout.error <= 0 )
		return 2000; // Default is 2 Seconds
	else
		return SystemSetup.user.timeout.error * 100;
}

/**
 * Returns the Terminal Id
 */
char Operation::GetTerminalId()
{
	return SystemSetup.id;
}

/**
 * Sets the Terminal Id
 */
void Operation::SetTerminalId(char id)
{
	SystemSetup.id = id;

	// Update the clock program file
	static char tid[6];
	strcpy(tid, "T#=");
	tid[3] = id;
	tid[4] = '\n';
	tid[5] = '\0';

	// As we are updating the clock program file, obtain the programming lock
	LegacyTransItem * transitem = LegacyTransItem::GetInstance();
	char buffer[128];

	transitem->GetProgrammingLock();

	// Open the Clock Program File
	FILE * fp = fopen(CLK_PROGRAM_FILE, "r");
	if( !fp )
	{
		#ifdef DEBUG
		perror("Couldn't open the program.clk");
		#endif
		transitem->ReleaseProgrammingLock();
		return;
	}

	FILE * fw = fopen(TMP_CLK_PROGRAM_FILE, "w");
	if( !fw )
	{
		#ifdef DEBUG
		perror("Couldn't open the program.clk");
		#endif
		transitem->ReleaseProgrammingLock();
		fclose( fp );
		return;
	}

	char * line = fgets(buffer, 128, fp);
	bool found = false;
	while(line != NULL)
	{
		if( strstr(line, "T#=") != NULL )
		{
			fputs(tid, fw);
			found = true;
		}
		else
		{
			fputs(line, fw);
		}
		line = fgets(buffer, 128, fp);
	}

	if( !found ) fputs(tid, fw);

	fclose( fp );
	fclose( fw );

	rename(TMP_CLK_PROGRAM_FILE, CLK_PROGRAM_FILE);
	transitem->ReleaseProgrammingLock();
}

/**
 * Please use this only for debugging.
 * It has some issues
 * 02-13-06 - Issues Resolved. This can be used .
 */
void Operation::UpdateClockFile()
{
	FILE * fp = fopen("program1.clk", "w");
	if(fp)
	{
		SystemSetup.List(fp, 0);
		fclose(fp);
		rename("program1.clk", CLK_PROGRAM_FILE);
	}

}


void Operation::SetWatchOption(int status)
{
	if(status == 1)
		watchdogstatus = WATCH_ENABLED;
	else
		watchdogstatus = WATCH_DISABLED;
}

void Operation::InitWatchDog()
{

	XMLProperties props(XML_PROPERTIES_FILE);
	char wd[20];
	wd[0] = '\0';
	props.GetProperty(PROPERTY_WATCHDOG,wd);
	if(strcmp(wd, "OFF") == 0)
	{
		watchdogstatus = WATCH_DISABLED;
	}
	else
		watchdogstatus = WATCH_ENABLED;

	char autores[20];
	autores[0] = '\0';
	props.GetProperty(PROPERTY_AUTORSTRT,autores);
	if( strcmp(autores, "OFF") == 0)
	{
		auto_restart = false;
	}
	else
		auto_restart = true;

}


void Operation::SetTCPPort(int port)
{
	if(port < 1000 || port > 99999) port = DEFAULT_TCP_PORT;
	static char buf[6];
	sprintf(buf, "%d", port);

	// Check if the port has changed.
	XMLProperties props(XML_PROPERTIES_FILE);
	char prt[50]; prt[0]='\0';
	props.GetProperty(PROPERTY_PORT,prt);
	if(strcmp(buf, prt) != 0 )
	{
		props.SetProperty(PROPERTY_PORT, buf);
	}
}

int Operation::GetTCPPort()
{
	XMLProperties props(XML_PROPERTIES_FILE);
	char prt[50]; prt[0]='\0';
	props.GetProperty(PROPERTY_PORT,prt);
	if( strlen(prt) == 0 ) return DEFAULT_TCP_PORT;
	int port = strtol(prt, NULL, 10);
	if(port < 1000 || port > 99999) return DEFAULT_TCP_PORT;
	return port;
}

char * Operation::GetAccessCode()
{
	if(accessCode[0] == '\0')
	{
		XMLProperties props(XML_PROPERTIES_FILE);
		props.GetProperty(PROPERTY_ACCESS_CODE,accessCode);
		return accessCode;
	}
	else
	{
		return accessCode;
	}
}

void Operation::SetAccessCode(const char * code)
{
	XMLProperties props(XML_PROPERTIES_FILE);
	props.SetProperty(PROPERTY_ACCESS_CODE, code);
}

char * Operation::GetResetAccessCode()
{
	if(resetAccessCode[0] == '\0')
	{
		XMLProperties props(XML_PROPERTIES_FILE);
		props.GetProperty(PROPERTY_RESET_ACCESS_CODE,resetAccessCode);
		return resetAccessCode;
	}
	else
	{
		return resetAccessCode;
	}
}

void Operation::SetResetAccessCode(const char * code)
{
	XMLProperties props(XML_PROPERTIES_FILE);
	props.SetProperty(PROPERTY_RESET_ACCESS_CODE, code);
}

/**
 * Checks if bell has to be rung at this time
 */
void Operation::CheckBellSchedule()
{

	struct tm optimeinfo;

	Operation::SYClock_GetTime(&optimeinfo);


	SysDayOfWeek day = 0x7F & GetSysDayOfWeek(optimeinfo.tm_wday);
	SysTime time = (optimeinfo.tm_hour) * 60 + optimeinfo.tm_min;

	InterfaceBellScheduleHolder * holder = SystemSetup.bell.schedule;
	//printf("holder->size() = %d \n",holder->size());
	for(int i = 0; i < holder->size(); i++ )
	{
		const SysBellScheduleItem & bellitem = holder->Get(i);

		if( bellitem.days & day )
		{
			if( time == bellitem.time )
			{
				//printf("BELL ON bellitem.duration = %d\n",bellitem.duration);
               //printf("BELL TURN ON AT time = %d\n",time);
                user_digital_set (1, ON);
				//printf("DURATION = %d\n",bellitem.duration);
				sleep(bellitem.duration / 10);
				user_digital_set (1, OFF);
                //printf("BELL TRUN OFF AT TIME = %d\n",time);
			}
		}
	}
}

/**
 * Access Window Check
 */
bool Operation::CheckIfInAccessWindow()
{
	/**
	 * Step 1. Check if the time is within the Access Window
	 */
	SysTime time = GetTime();
	SysDayOfWeek day = GetSysDayOfWeek(GetDayOfWeek());
	if( SystemSetup.access.window.days & day )
	{
		if(SystemSetup.access.window.time.start == 0 &&
				SystemSetup.access.window.time.stop == 0)
		{
			// Do Not Use the Access Window
			return false;
		}
		else if ( time >= SystemSetup.access.window.time.start &&
				time < SystemSetup.access.window.time.stop )
		{
			//printf("current = %d start = %d end = %d\n",time,SystemSetup.access.window.time.start,SystemSetup.access.window.time.stop);
			return true;
		}
	}
	return false;
}

/**
 * Returns the appropriate flag based on the day
 */
SysDayOfWeek Operation::GetSysDayOfWeek(int day)
{
    SysDayOfWeek dw = WrkWeekSunday;
    switch(day)
    {
        case 1:
        {
            dw = WrkWeekMonday;
            break;
        }
        case 2:
        {
            dw = WrkWeekTuesday;
            break;
        }
        case 3:
        {
            dw = WrkWeekWednesday;
            break;
        }
        case 4:
        {
            dw = WrkWeekThursday;
            break;
        }
        case 5:
        {
            dw = WrkWeekFriday;
            break;
        }
        case 6:
        {
            dw = WrkWeekSaturday;
            break;
        }
    }
    return dw;
}

/**
 * Returns the day of the month 0-31
 */
int Operation::GetDayOfMonth()
{
	struct tm  optimeinfo;
	Operation::SYClock_GetTime(&optimeinfo);
	return optimeinfo.tm_mday;
}

/**
 * Returns the day of the week
 * (Sun-Sat)0 for Sun and 6 for Sat
 */
int Operation::GetDayOfWeek()
{
	struct tm  optimeinfo;
	Operation::SYClock_GetTime(&optimeinfo);
	return optimeinfo.tm_wday;
}

/*
 * Return the Current Time in hh:mm:ss format
 */
void Operation::GetTimeAsString(char * buffer)
{
	struct tm optimeinfo;
	Operation::SYClock_GetTime(&optimeinfo);
	sprintf(buffer, "%0*d", 2, optimeinfo.tm_hour);
	buffer+=2;
	sprintf(buffer, "%0*d", 2, optimeinfo.tm_min);
	buffer+=2;
	sprintf(buffer, "%0*d", 2, optimeinfo.tm_sec);
}

/**
 * Returns the Time for display in the idle screen
 * in the format 08:54:16 AM
 */
const char * Operation::GetTimeForIdleDisplay()
{

	/*time_t optime;
	time ( &optime );
    localtimeinfo = localtime ( &optime );*/
	struct tm currTime;
	Operation::SYClock_GetTime(&currTime);
	memcpy(&localtimeinfo,&currTime,sizeof(struct tm));

	if(! SystemSetup.clock.twenty_four_hour)
	{
		//strftime(timebuf, 21, "    %I:%M:%S %p     ", localtimeinfo);
		strftime(timebuf, 21, "    %I:%M:%S %p     ", &currTime);
	}
	else
	{
		//military time
		//strftime(timebuf, 21, "     %H:%M:%S       ", localtimeinfo);
		strftime(timebuf, 21, "     %H:%M:%S       ", &currTime);
	}

	//printf("timeB11= %s\n",timeB);
	return timebuf;
}

/**
 * Returns the Time in format hhmmss format
 */
const char * Operation::GetSystemTime()
{
	static char buffer[7];
	struct tm  optimeinfo;
	Operation::SYClock_GetTime(&optimeinfo);
	strftime(buffer, 7, "%H%M%S", &optimeinfo);
	return buffer;
}

/**
 * Returns the Time in format hhmmss format
 */
const char * Operation::GetSystemDate()
{
	static char buffer[9];
	struct tm  optimeinfo;
	Operation::SYClock_GetTime(&optimeinfo);
	strftime(buffer, 9, "%Y%m%d", &optimeinfo);

	return buffer;
}

/**
 * Returns the Date for display in the idle screen
 * in the format "MON AUG 15 2005"
 */
const char * Operation::GetDateForIdleDisplay()
{
	//static char buff[21];
	char * buffer = datebuf;

	/*time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );*/

	struct tm currTime;
	Operation::SYClock_GetTime(&currTime);

	//switch(optimeinfo->tm_wday)
	switch(currTime.tm_wday)
	{
		case 1:
			sprintf(buffer, "  %s ", STR_MON);
			break;
		case 2:
			sprintf(buffer, "  %s ", STR_TUE);
			break;
		case 3:
			sprintf(buffer, "  %s ", STR_WED);
			break;
		case 4:
			sprintf(buffer, "  %s ", STR_THU);
			break;
		case 5:
			sprintf(buffer, "  %s ", STR_FRI);
			break;
		case 6:
			sprintf(buffer, "  %s ", STR_SAT);
			break;
		default:
			sprintf(buffer, "  %s ", STR_SUN);
	}
	buffer+=6;
	//switch(optimeinfo->tm_mon)
	switch(currTime.tm_mon)
	{
		case 0:
			sprintf(buffer, "%s ", STR_JAN);
			break;
		case 1:
			sprintf(buffer, "%s ", STR_FEB);
			break;
		case 2:
			sprintf(buffer, "%s ", STR_MAR);
			break;
		case 3:
			sprintf(buffer, "%s ", STR_APR);
			break;
		case 4:
			sprintf(buffer, "%s ", STR_MAY);
			break;
		case 5:
			sprintf(buffer, "%s ", STR_JUN);
			break;
		case 6:
			sprintf(buffer, "%s ", STR_JUL);
			break;
		case 7:
			sprintf(buffer, "%s ", STR_AUG);
			break;
		case 8:
			sprintf(buffer, "%s ", STR_SEP);
			break;
		case 9:
			sprintf(buffer, "%s ", STR_OCT);
			break;
		case 10:
			sprintf(buffer, "%s ", STR_NOV);
			break;
		default:
			sprintf(buffer, "%s ", STR_DEC);
			break;
	}
	buffer+=4;
	//strftime(buffer, 11, "%d %Y   ", optimeinfo);
	strftime(buffer, 11, "%d %Y   ", &currTime);
	return datebuf;
}

/**
 * Get the Curent Time as SysTime
 */
SysTime Operation::GetTime()
{
	struct tm  optimeinfo;
	Operation::SYClock_GetTime(&optimeinfo);
	return (optimeinfo.tm_hour * 60) + optimeinfo.tm_min;
}

const char* Operation::GetFormattedTime()
{
	struct tm  optimeinfo;
	Operation::SYClock_GetTime(&optimeinfo);
	return asctime( &optimeinfo );
}

/**
 * Emit a Beep Sound
 */
void Operation::Beep(int milliseconds, BeepType type)
{
	#ifdef __TA7700_TERMINAL__
	switch(type)
	{
		case TYPE_Accept:
			if(milliseconds == 0)
			{
				user_audible_set(1000);
			}
			else
			{
				user_audible_set(1000);
				usleep(milliseconds * 1000);
				user_audible_set(0);
			}
			break;
		case TYPE_Reject:
			if(milliseconds == 0)
			{
				user_audible_set(600);
				usleep(100000);
				user_audible_set(0);
				usleep(50000);
				user_audible_set(600);
				usleep(100000);
				user_audible_set(0);
				usleep(50000);
				user_audible_set(600);
				usleep(100000);
				user_audible_set(0);
				usleep(50000);
				user_audible_set(600);
				usleep(100000);
				user_audible_set(0);
			}
			else
			{
				user_audible_set(1500);
				usleep(milliseconds * 1000);
				user_audible_set(0);
			}
			break;
		case TYPE_Error:
			if(milliseconds == 0)
			{
				user_audible_set(750);
				usleep(100000);
				user_audible_set(0);
				usleep(50000);
				user_audible_set(750);
				usleep(100000);
				user_audible_set(0);
				usleep(50000);
				user_audible_set(750);
				usleep(100000);
				user_audible_set(0);
				usleep(50000);
				user_audible_set(750);
				usleep(100000);
				user_audible_set(0);
			}
			else
			{
				user_audible_set(1250);
				usleep(milliseconds * 1000);
				user_audible_set(0);
			}
			break;
		case TYPE_KeyPress:
			user_audible_set(1000);
			usleep(50 * 1000);
			user_audible_set(0);
			break;
		case TYPE_TimeOut:
			user_audible_set(1000);
			usleep(100000);
			user_audible_set(0);
			usleep(50000);
			user_audible_set(1000);
			usleep(100000);
			user_audible_set(0);
			usleep(50000);
			user_audible_set(1000);
			usleep(100000);
			user_audible_set(0);
			break;
		case TYPE_TurnOff:
			user_audible_set(0);
			break;
	}
	#else
	printf("\a\n");
	#endif
}

void Operation::TurnOnLEDRed()
{
	user_visual_set(USER_VISUAL_RED, ON);
}

void Operation::TurnOffLEDRed()
{
	user_visual_set(USER_VISUAL_RED, OFF);
}

void Operation::TurnOnLEDYellow()
{
	user_visual_set(USER_VISUAL_YEL, ON);
}

void Operation::TurnOffLEDYellow()
{
	user_visual_set(USER_VISUAL_YEL, OFF);
}

void Operation::TurnOnLEDGreen()
{
	user_visual_set(USER_VISUAL_GRN, ON);
}

void Operation::TurnOffLEDGreen()
{
	user_visual_set(USER_VISUAL_GRN, OFF);
}


/**
 * Check if Access denied transaction need to be reported
 */
Boolean Operation::ReportAccesDeniedTransaction()
{
	return SystemSetup.profile.mode.report_fails;
}

OperationStatusCode Operation::CreateDateTimeChangeTransaction()
{
	static char transdata[24];
	strcpy(transdata, GetSystemTime());
	strcat(transdata, "4");
	strcat(transdata, GetSystemDate());
	strcat(transdata, "|");
	strcat(transdata, "&");
	LegacyTransItem * transitem = LegacyTransItem::GetInstance();
	transitem->GetTransactionFileLock();
	Code code = transitem->ProcessData(transdata, false);
	transitem->ReleaseTransactionFileLock();
	if( code == SC_MEMORY ) return SC_OutOfMemory;
	return SC_Success;
}

void Operation::SetAccesDeniedTransFlag(OperationStatusCode code, Bit8 & flag)
{
	if(	ReportAccesDeniedTransaction() )
	{
		flag |= RA_CMD_ON;
	}
	if( code == SC_Access_Denied || code == SC_Profile_Denied )
	{
		flag |= PROFILE_ACCESS_DENIED;
	}
}

OperationStatusCode Operation::ClearClockTransactions()
{
	OperationStatusCode code = SC_Success;
	LegacyTransItem * transitem = LegacyTransItem::GetInstance();
	transitem->GetTransactionFileLock();
	TransFIFO * fifo = transitem->GetFIFO();
	fifo->clear();

	//20061214 wjm. Avoid unnecessary accesses to disk.
	/*struct stat f ;
	int r = stat ( TRANSACTIONS_FILE , & f );
	if ( r != -1 && f .st_size != 0 ){

	  // Delete all Transactions from the File through truncation
	  FILE * fp = fopen(TRANSACTIONS_FILE, "w");
	  if(fp){
	    //20070118 wjm put in date code
	    fclose(fp);
	  }
	  sync ();
	}*/

	transitem->ReleaseTransactionFileLock();
	CreateDateTimeChangeTransaction();

	// If the clock was full
	LegacyTransItem::clockIsFull = false;

	return code;
}

void Operation::displayErrMessage(char* msg)
{
  user_output_clear();
  user_output_write(2, 1, msg);
  user_output_write(3, 1, "  PLEASE TRY AGAIN  ");
  TurnOnLEDYellow();
  usleep(GetErrorMsgTimeout() * 1000);
  TurnOffLEDYellow();
}
//
