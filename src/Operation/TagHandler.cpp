/**
 * The Server sends down the Clock Program to us one tag at
 * a time. Some of the tags sent are not in the format that 
 * we want. This class handles all the tags that need formatting
 */
#include <stdio.h>
#include <string.h>
#include "TagHandler.h"

static const unsigned char MASK_SUNDAY = 0x40;
static const unsigned char MASK_MONDAY = 0x20;
static const unsigned char MASK_TUESDAY = 0x10;
static const unsigned char MASK_WEDNESDAY = 0x08;
static const unsigned char MASK_THURSDAY = 0x04;
static const unsigned char MASK_FRIDAY = 0x02;
static const unsigned char MASK_SATURDAY = 0x01;

TagHandler::TagHandler()
{
}

TagHandler::~TagHandler()
{
}

/**
 * Reads the Tag from the packet. 
 * Formats the tag if needed.
 * We receive the entire packet sent by the server, 
 * which contains the STX, ETX and the CRC
 */
const char * TagHandler::GetFormattedTag(char * buffer, int pktSize)
{
	//static char tag[128];
	static char tag[1024];
	if( strstr(buffer, "BS=") != NULL )
	{
		return GetFormattedBSTag(buffer, pktSize, tag);
	}
	else if( strstr(buffer, "RS=") != NULL || strstr(buffer, "LS=") != NULL )
	{
		return GetFormattedBaudRateTag(buffer, pktSize, tag);
	}
	else if( strstr(buffer, "FL=") != NULL || strstr(buffer, "FP=") != NULL)
	{
		return GetFormattedFunctionKeyTag(buffer, pktSize, tag);
	}
	else if( strstr(buffer, "AW=") != NULL )
	{
		return GetFormattedAWTag(buffer, pktSize, tag);
	}
	else if( strstr(buffer, "PX=") != NULL || strstr(buffer, "PM=") != NULL )
	{
		return GetFormattedProfileMsgTag(buffer, pktSize, tag);
	}
	else if( strstr(buffer, "EM=") != NULL )
	{
		return GetFormattedEMTag(buffer, pktSize, tag);
	}
	else
	{
		strncpy(tag, buffer + 1, pktSize - 3);
		tag[pktSize - 3] = '\0';	
		
	}
	return tag;
}

const char * TagHandler::GetFormattedBSTag(char * buffer, int pktSize, char * tag)
{
	if( pktSize != 14 ) return NULL; // Packet needs to be 14 bytes
	strncpy(tag, buffer + 1, 4);
	tag[4] = '\0';
	strcat(tag, GetWeekFlagsFromByte(buffer[9]));
	strncpy(tag + 11, buffer + 5, 4);
	strncpy(tag + 15, buffer + 10, 2);
	tag[17] = '\0';	
	return tag;
}

const char * TagHandler::GetFormattedBaudRateTag(char * buffer, int pktSize, char * tag)
{
	if( pktSize != 7 ) return NULL; // Packet needs to be 14 bytes
	strncpy(tag, buffer + 1, 3);
	tag[3] = '\0';
	switch(buffer[4])
	{
		case '0':
			strcat(tag, "300");
			break;
		case '1':
			strcat(tag, "600");
			break;
		case '2':
			strcat(tag, "1200");
			break;
		case '3':
			strcat(tag, "2400");
			break;
		case '4':
			strcat(tag, "4800");
			break;
		case '5':
			strcat(tag, "9600");
			break;
		case '6':
		default:
			strcat(tag, "19200");
			break;
	}
	return tag;
}

const char * TagHandler::GetFormattedFunctionKeyTag(char * buffer, int pktSize, char * tag)
{
	strncpy(tag, buffer + 1, pktSize - 3);
	tag[pktSize - 3] = '\0';
	switch(tag[3])
	{
		case ':':
			tag[3] = '*';
			break;
		case ';':
			tag[3] = '#';
			break;
		case '<':
			tag[3] = '?';
			break;
	}
	return tag;
}

const char * TagHandler::GetFormattedAWTag(char * buffer, int pktSize, char * tag)
{
	if(pktSize == 6) // We Rcvd AW=
	{
		strncpy(tag, buffer + 1, pktSize - 3);
		tag[pktSize - 3] = '\0';		
		strcat(tag, "00000000");
	}
	else
	{
		strncpy(tag, buffer + 1, 11);
		tag[11] = '\0';
		strcat(tag, GetWeekFlagsFromByte(buffer[12]));
	}
	return tag;
}

const char * TagHandler::GetFormattedProfileMsgTag(char * buffer, int pktSize, char * tag)
{
	if( pktSize != 27 && pktSize != 28 ) return NULL;
	strncpy(tag, buffer + 1, pktSize - 3);
	tag[pktSize - 3] = '\0';
	switch(tag[22])
	{
		case '1':
			tag[22] = 'A';
			break;
		case '2':
			tag[22] = 'R';
			break;
		case '3':
			tag[22] = 'B';
			break;
		case '0':
		default:
			tag[22] = 'N';
			break;
	}
	return tag;
}

const char * TagHandler::GetFormattedEMTag(char * buffer, int pktSize, char * tag)
{
	strncpy(tag, buffer + 1, 4);
	// Get the flag
	unsigned char flag = *(buffer + 5);
	( flag & 0x80 ) ? tag[4] = 'Y' : tag[4] = 0x20;
	( flag & 0x40 ) ? tag[5] = 'Y' : tag[4] = 'N';
	( flag & 0x20 ) ? tag[6] = 'Y' : tag[4] = 'N';
	strncpy(tag + 7, buffer + 6, pktSize - 4 - 1 - 3);
	tag[pktSize - 1] = '\0';
	return tag;
}

const char * TagHandler::GetWeekFlagsFromByte(char flag)
{
	static char week[8];
	if(MASK_SUNDAY & flag) week[0] = 'Y';
	else week[0] = 'N';
	if(MASK_MONDAY & flag) week[1] = 'Y';
	else week[1] = 'N';
	if(MASK_TUESDAY & flag) week[2] = 'Y';
	else week[2] = 'N';
	if(MASK_WEDNESDAY & flag) week[3] = 'Y';
	else week[3] = 'N';
	if(MASK_THURSDAY & flag) week[4] = 'Y';
	else week[4] = 'N';
	if(MASK_FRIDAY & flag) week[5] = 'Y';
	else week[5] = 'N';
	if(MASK_SATURDAY & flag) week[6] = 'Y';
	else week[6] = 'N';
	week[7] = '\0';
	return week;
}



