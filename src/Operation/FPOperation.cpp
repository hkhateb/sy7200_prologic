#include "FPOperation.hpp"
#include "SystemGlobals.h"
#include "logger.h" 
#include "Base64.h"
#include "uf_module.h"


pthread_mutex_t FPOperation::finger_print_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t  FPOperation::fprint_cond_flag;

FPOperation fpOpr;
bool FPOperation::FpScanValid=false;
bool FPOperation::EnableFpUnit = false;
bool FPOperation::InitFpuSuc = false;
UF_RET_CODE FPOperation::ScanResult;
int FPOperation::ScanCount = 0;
extern uf_system_config_t g_sys_config;

namespace FPmutex {
  class FPguard {
  public:
    FPguard (){
      pthread_mutex_lock ( & _ );
    }

    ~FPguard (){
      pthread_mutex_unlock ( & _ );
    }

    static pthread_mutex_t _ ;
  };

  //********************************************************************
  pthread_mutex_t FPguard ::_ = PTHREAD_MUTEX_INITIALIZER ;

};

using namespace FPmutex;

FPOperation::FPOperation()
{
  char logstr[50];
  int mBaudRate;
  char mDeviceName[256];
  int mAsciiPacket =0;
  int mNetworkMode =0;
  int mModuleID = 1;
  fpOperation = Ignore_Operation;
  FpUserID = 0;
  FpScanValid = false;
  ScanResult = UF_ERR_READ_SERIAL_TIMEOUT;
  ScanCount = 0 ;
  //mBaudRate = UF_BAUD115200;
  mBaudRate = UF_BAUD57600;
  FPOperation::FpScanValid=false;
  //XMLProperties xmlParser("properties.xml");
  //char port[10];port[0] = '\0';
  //xmlParser.GetProperty("FPUPort",port);
  //if(strcmp(port,"RS422") == 0)
	//strcpy(mDeviceName, "/dev/tts/5");
  //else
    strcpy(mDeviceName, "/dev/tts/3");

  UF_RET_CODE result = uf_initialize( mDeviceName, (UF_BAUDRATE)mBaudRate, (UF_BOOL)mAsciiPacket, (UF_BOOL)mNetworkMode, mModuleID, mTimeout );
  if( result == UF_RET_SUCCESS || result == 0)
  {
     //EnableFpUnit = true;
	 FPOperation::InitFpuSuc = true;
	 //WriteSystemParameter(UF_SYS_SEND_SCAN_SUCCESS, 0x30);
	 //UF_UINT32 freescam_param_value;
	 //UF_RET_CODE Res1 = uf_read_system_parameter( UF_SYS_SEND_SCAN_SUCCESS, &freescam_param_value );
	// printf("UF_SYS_SEND_SCAN_SUCCESS = %02x  g_sys_config.send_scan_success = %d\n", freescam_param_value,
		 //g_sys_config.send_scan_success);
	 printf("uf_initialize success - making fpUnitEnabled true\n");
  }
  else
  {
      //EnableFpUnit = false;
	  FPOperation::InitFpuSuc = false;
	  printf("uf_initialize failed res = %d\n",result);
  }
  if(pthread_cond_init (&fprint_cond_flag, NULL))
      fprintf(stderr,"getservertime condition variable initialize failed\n");
}

FPOperation::~FPOperation()
{
  pthread_cond_destroy(&fprint_cond_flag);
}

UF_RET_CODE FPOperation::WriteSystemParameter( UF_SYS_PARAM param_id, UF_UINT32 param_value )
{
   	int result;

	UF_BYTE packet[UF_NPACKET_LEN];

	UF_LOG( API_CH, "System Parameter Write(SW): %s(%#02x) %#x\n", uf_get_log_msg( LOG_MSG_SYS_PARAMETER, param_id ), param_id, param_value );

	result = uf_send_packet( UF_COM_SW, 0,  param_value, param_id );

	if( result < 0 )
	{
		return (UF_RET_CODE)result;
	}

	result = uf_receive_packet( packet );

	if( result < 0 )
	{
		return (UF_RET_CODE)result;
	}

	result = uf_get_packet_value( UF_PACKET_FLAG, packet );

	if( result != UF_PROTO_RET_SUCCESS )
	{
		switch( result ) {
			case UF_PROTO_RET_NOT_FOUND:
				UF_LOG( ERR_CH, "Cannot find the parameter: %#x\n", param_id );
				return UF_ERR_NOT_FOUND;

			case UF_PROTO_RET_BUSY:
				UF_LOG( ERR_CH, "System is busy\n" );
				return UF_ERR_BUSY;

			default:
				UF_LOG( ERR_CH, "Unknown error: %#x\n", param_id );
				return UF_ERR_UNKNOWN;
		}
	}

	//
	// Check if g_sys_config should be changed
	//
	switch( param_id ) {
		case UF_SYS_BAUDRATE:
			g_sys_config.baudrate = param_value;
			if( uf_serial_setup( param_value )  < 0 )
			{
				UF_LOG( ERR_CH, "Cannot set baudrate: %#x\n", param_value );
				return UF_ERR_CANNOT_SETUP_SERIAL;
			}
			break;

		case UF_SYS_ASCII_PACKET:
			g_sys_config.ascii_packet = (param_value == 0x30)? UF_FALSE : UF_TRUE;
			break;

		case UF_SYS_NETWORK_MODE:
			g_sys_config.network_mode = (param_value == 0x30)? UF_FALSE : UF_TRUE;
			break;

		case UF_SYS_ENROLL_MODE:
			g_sys_config.enroll_mode = (UF_ENROLL_MODE)param_value;
			break;

		case UF_SYS_ENCRYPTION_MODE:
			g_sys_config.encrypt = (param_value == 0x30)? UF_FALSE : UF_TRUE;
			break;

		case UF_SYS_SENSOR_TYPE:
			g_sys_config.sensor_type = (UF_SENSOR_TYPE)param_value;
			// calibrate sensor?
			break;

		case UF_SYS_IMAGE_FORMAT:
			g_sys_config.image_type = (UF_IMAGE_TYPE)param_value;
			break;

		case UF_SYS_SEND_SCAN_SUCCESS:
			g_sys_config.send_scan_success = (param_value == 0x31) ? UF_TRUE : UF_FALSE;
			break;
			
		default:
			break;
	}

	return UF_RET_SUCCESS;
}

UF_RET_CODE FPOperation::FPU_Initialize()
{
  char logstr[50];
  int mBaudRate;
  char mDeviceName[256];
  int mAsciiPacket =0;
  int mNetworkMode =0;
  int mModuleID = 1;
  FpUserID = 0;
  FpScanValid = false;
  ScanResult = UF_ERR_READ_SERIAL_TIMEOUT;
  ScanCount = 0 ;
  mBaudRate = UF_BAUD57600;
  //mBaudRate = UF_BAUD115200;
  strcpy(mDeviceName, "/dev/tts/3");
  printf("FPU_Initialize1\n");
  UF_RET_CODE result = uf_initialize( mDeviceName, (UF_BAUDRATE)mBaudRate, (UF_BOOL)mAsciiPacket, (UF_BOOL)mNetworkMode, mModuleID, mTimeout );
  printf("FPU_Initialize2\n");
  if( result == UF_RET_SUCCESS || result == 0)
  {
     //EnableFpUnit = true;
	 FPOperation::InitFpuSuc = true;
	 printf("FPU_Initialize3\n");
	 printf("%s success - making fpUnitEnabled true\n",__FUNCTION__);
	 printf("FPU_Initialize4\n");
	 return result;
  }
  else
  {
      //EnableFpUnit = false;
	  FPOperation::InitFpuSuc = false;
	  printf("FPU_Initialize5\n");
	  printf("%s failed res = %d\n",__FUNCTION__,result);
	  printf("FPU_Initialize6\n");
	  return result;
  }
}

UF_RET_CODE FPOperation::FPU_Finalize()
{
  
  UF_RET_CODE result = uf_finalize();
  if( result == UF_RET_SUCCESS || result == 0)
  {
     InitFpuSuc = false;
	 printf("%s success - making fpUnitEnabled false\n",__FUNCTION__);
	 return result;
  }
  else
  {
      InitFpuSuc = true;
	  printf("%s failed res = %d - making fpUnitEnabled true\n",result,__FUNCTION__);
	  return result;
  }
}

UF_RET_CODE FPOperation::FPU_Reset()
{
  
  return uf_send_packet( 0xd0, 0, 0, 0 ); 
}

OperationStatusCode FPOperation::Enroll(unsigned long int UserId ,int timeOut )
{
  
  FPguard g ;
  UF_UINT32 user_id=0;
  //printf("please place you finger\n");

   //printf("%d %d %d %d %d %d %d %d %d %d %d %d\n",UF_RET_SUCCESS,UF_ERR_SCAN_FAILED,UF_ERR_TIMEOUT,UF_ERR_TRY_AGAIN,UF_ERR_MEM_FULL,UF_ERR_FINGER_LIMIT,UF_ERR_INVALID_ID,UF_ERR_EXIST_ID,UF_ERR_NOT_MATCH,UF_ERR_UNSUPPORTED,UF_ERR_BUSY,UF_ERR_CANCELED );
    
  uf_cancel();
  UF_RET_CODE result_1 ;
  result_1 = uf_set_read_timeout ( timeOut );
  if ( UF_RET_SUCCESS != result_1 )
  {
    printf ( "uf_set_read_timeout %s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
  }
  UF_RET_CODE resultEn =  uf_enroll_by_scan( (UF_UINT32)UserId,( UF_ENROLL_OPTION)UF_ENROLL_ADD_NEW, &user_id );
  //UF_UINT32 param_value;
  //uf_read_system_parameter( UF_SYS_IMAGE_QUALITY, &param_value );
  //printf("UF_SYS_IMAGE_QUALITY = %d\n",param_value);
  //printf("uf_enroll_by_scan returned %d new_user = %d TIME_OUT = %d\n",resultEn,user_id,UF_ERR_TIMEOUT);
  int status;
  UF_RET_CODE ret;
  if( resultEn == UF_RET_SUCCESS || resultEn == 0)
  {
      uf_cancel();
	  //printf("uf_enroll_by_scan success\n ");
	  uf_set_read_timeout ( mTimeout );
	  return SC_Success;
  }
  else
  {
      uf_cancel();
	  //printf("uf_enroll_by_scan failed\n ");
	  uf_set_read_timeout ( mTimeout );
	  return SC_Fail;
  }
}

OperationStatusCode FPOperation::Verification(unsigned long int UserId ,int timeOut,UF_RET_CODE* Result )
{
  FPguard g ;
  UF_UINT32 sub_id=0;
 // printf("FPConfig::Verification\n");
 // printf("befor uf_verify_by_scan\n");
  uf_cancel();
  UF_RET_CODE result_1 ;
  result_1 = uf_set_read_timeout ( timeOut );
  if ( UF_RET_SUCCESS != result_1 )
  {
    printf ( "uf_set_read_timeout %s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
  }
  *Result =  uf_verify_by_scan( (UF_UINT32)UserId,&sub_id );
  //printf("uf_verify_by_scan returned %d sub_id = %d TIME_OUT = %d\n",*Result,sub_id,UF_ERR_TIMEOUT);
  
  if( *Result == UF_RET_SUCCESS || *Result == 0)
  {
      uf_cancel();
	  //printf("uf_verify_by_scan success\n ");
	  uf_set_read_timeout ( mTimeout );
	  return SC_Success;
  }
  else
  {
      uf_cancel();
	  //printf("uf_verify_by_scan failed\n ");
	  uf_set_read_timeout ( mTimeout );
	  return SC_Fail;
  }
}

OperationStatusCode FPOperation::Identification(unsigned long int* UserId,int timeOut,UF_RET_CODE* Result)
{
  
  FPguard g ;
  UF_UINT32 sub_id=0;
  //printf("FPConfig::Identification\n");

  //printf("befor uf_identify_by_scan\n");
  *UserId = 0 ;
  uf_cancel();
  UF_RET_CODE result_1 ;
  //result_1 = uf_set_read_timeout ( timeOut );
  //if ( UF_RET_SUCCESS != result_1 )
 // {
   // printf ( "uf_set_read_timeout %s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
  //}
  *Result =  uf_identify_by_scan( (UF_UINT32*)UserId,&sub_id );
  //pthread_mutex_unlock(&scan_finger_mutex);
  //printf("uf_identify_by_scan returned %d userID = %d TIME_OUT = %d\n",*Result,*UserId,UF_ERR_TIMEOUT);
  
  if( *Result == UF_RET_SUCCESS || *Result == 0)
  {
      uf_cancel();
		FpUserID = *UserId;
		//FpScanValid = true;
		ScanResult = *Result;
		ScanCount ++ ;
	    //printf("uf_identify_by_scan success\n ");
	return SC_Success;
  }
  else
  {
      uf_cancel();
		FpUserID = *UserId;
		//if(*Result != UF_ERR_READ_SERIAL_TIMEOUT)FpScanValid = true;
		ScanResult = *Result;
		ScanCount ++ ;
		//printf("uf_identify_by_scan failed\n ");
	  return SC_Fail;
  }
}

OperationStatusCode FPOperation::ScanTemplate(UF_UINT32* templ_size,UF_BYTE* templ_data , int timeOut ,UF_RET_CODE* Result)
{

  FPguard g ;
  uf_cancel();
  UF_RET_CODE result_1 ;
  result_1 = uf_set_read_timeout ( timeOut );
  if ( UF_RET_SUCCESS != result_1 )
  {
    printf ( "uf_set_read_timeout %s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
  }
  *Result = uf_scan_template( templ_size );
  if( *Result == UF_RET_SUCCESS )
  {
    UF_RET_CODE ret; 
	ret = uf_get_raw_data( templ_data, *templ_size );
	if ( UF_RET_SUCCESS != ret )
    { 
      printf ( "uf_get_raw_data %s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
    }
    uf_cancel();
	uf_set_read_timeout ( mTimeout );
    return SC_Success;
   }
   else
   {
      uf_cancel();
	  uf_set_read_timeout ( mTimeout );
	  return SC_Fail;
   }
}


OperationStatusCode FPOperation::IdentifyByTemplate(UF_BYTE* templ_data, UF_UINT32* templ_size, 
													UF_UINT32* UserId ,int timeOut ,UF_RET_CODE* Result)
{
  FPguard g ;
  UF_UINT32 sub_id=0;

  *UserId = 0 ;
  uf_cancel();
  UF_RET_CODE result_1 ;
  result_1 = uf_set_read_timeout ( timeOut );
  if ( UF_RET_SUCCESS != result_1 )
  {
    printf ( "uf_set_read_timeout %s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
  }
  //printf("IdentifyByTemplate1\n"); 
  *Result =  uf_identify_by_template(templ_data,*templ_size,(UF_UINT32*)UserId,&sub_id );
  //printf("IdentifyByTemplate2\n");
  if( *Result == UF_RET_SUCCESS || *Result == 0)
  {
      uf_cancel();
	  FpUserID = *UserId;
	  //FpScanValid = true;
	  ScanResult = *Result;
	  ScanCount ++ ;
	  //printf("uf_identify_by_scan success\n ");
	  uf_set_read_timeout ( mTimeout );
	return SC_Success;
  }
  else
  {
      uf_cancel();
	  FpUserID = *UserId;
	  //if(*Result != UF_ERR_READ_SERIAL_TIMEOUT)FpScanValid = true;
	  ScanResult = *Result;
	  ScanCount ++ ;
	  //printf("uf_identify_by_scan failed\n ");
	  uf_set_read_timeout ( mTimeout );
	 return SC_Fail;
  }
}

OperationStatusCode VerifyByTemplate(UF_BYTE* templ_data, UF_UINT32* templ_size, UF_UINT32 UserId ,int timeOut ,
									 UF_RET_CODE* Result)

{
  FPguard g ;
  UF_UINT32 sub_id=0;

  uf_cancel();
  UF_RET_CODE result_1 ;
  result_1 = uf_set_read_timeout ( timeOut );
  if ( UF_RET_SUCCESS != result_1 )
  {
    printf ( "uf_set_read_timeout %s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
  }
  *Result =  uf_verify_by_template( (UF_UINT32)UserId,templ_data,*templ_size,&sub_id );
  
  if( *Result == UF_RET_SUCCESS || *Result == 0)
  {
      uf_cancel();
	  uf_set_read_timeout ( mTimeout );
	  return SC_Success;
  }
  else
  {
      uf_cancel();
	  return SC_Fail;
  }

}

OperationStatusCode FPOperation::DeleteUser(unsigned long int UserId ,UF_RET_CODE* Result )
{
  FPguard g ;
  uf_cancel();
  UF_RET_CODE result_1 ;
  result_1 = uf_set_read_timeout ( mTimeout );
  if ( UF_RET_SUCCESS != result_1 )
  {
    printf ( "uf_set_read_timeout %s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
  }
  *Result =  uf_delete_template(UserId);
  //pthread_mutex_unlock(&scan_finger_mutex);
  //printf("uf_identify_by_scan returned %d userID = %d TIME_OUT = %d\n",*Result,*UserId,UF_ERR_TIMEOUT);
  
  if( *Result == UF_RET_SUCCESS || *Result == 0)
  {
     uf_cancel();
	 return SC_Success;
  }
  else
  {
      uf_cancel();
	  return SC_Fail;
  }
  
}

OperationStatusCode FPOperation::DeleteAllUsers(UF_RET_CODE* Result )
{
  FPguard g ;
  uf_cancel();
  UF_RET_CODE result_1 ;
  result_1 = uf_set_read_timeout ( mTimeout );
  if ( UF_RET_SUCCESS != result_1 )
  {
    printf ( "uf_set_read_timeout %s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
  }
  *Result =  uf_delete_all_templates();
  //pthread_mutex_unlock(&scan_finger_mutex);
  //printf("uf_identify_by_scan returned %d userID = %d TIME_OUT = %d\n",*Result,*UserId,UF_ERR_TIMEOUT);
  
  if( *Result == UF_RET_SUCCESS || *Result == 0)
  {
     uf_cancel();
	 return SC_Success;
  }
  else
  {
      uf_cancel();
	  return SC_Fail;
  }
  
}

OperationStatusCode FPOperation::AddUser(UF_UINT32 UserId ,UF_BYTE* templ,UF_UINT32 templ_size,UF_RET_CODE* Result)
{
  FPguard g ;
  uf_cancel();
  UF_RET_CODE result_1 ;
  result_1 = uf_set_read_timeout ( mTimeout );
  if ( UF_RET_SUCCESS != result_1 )
  {
    printf ( "uf_set_read_timeout %s %d %d\n" , __FILE__ , __LINE__ , ( int ) result_1 );
  }
  unsigned long int user_id;

  *Result =  uf_enroll_by_template(UserId,templ,templ_size,UF_ENROLL_ADD_NEW,&user_id);
  //pthread_mutex_unlock(&scan_finger_mutex);
  //printf("uf_identify_by_scan returned %d userID = %d TIME_OUT = %d\n",*Result,*UserId,UF_ERR_TIMEOUT);
  
  if( *Result == UF_RET_SUCCESS || *Result == 0)
  {
     uf_cancel();
	 return SC_Success;
  }
  else
  {
      uf_cancel();
	  return SC_Fail;
  }
}

UF_RET_CODE FPOperation::SendPacket(UF_BYTE* packet,UF_BYTE packet_len)
{
    int i;
	int write_len;
	

    /*printf("\nsend_packet -> ");
    for( i = 0; i < g_sys_config.packet_len; i++ )
	{
		printf("%02x ", packet[i] );
	}*/

	write_len = uf_serial_write( packet, packet_len );

	if( write_len < 0 )
	{
		printf("Cannot write data to the serial port\n" );
		return UF_ERR_CANNOT_WRITE_SERIAL;
	}
	else if( write_len < packet_len )
	{
		printf("Write data timeout: %#x %#x\n", write_len, packet_len );
		return UF_ERR_WRITE_SERIAL_TIMEOUT;
	}

	/*UF_LOG( COM_CH, "Send: " );
	for( i = 0; i < g_sys_config.packet_len; i++ )
	{
		UF_LOG_CONT( COM_CH, "%02x ", packet[i] );
	}
	UF_LOG_CONT( COM_CH, "\n" );*/
	
	return UF_RET_SUCCESS;

}

//
// get an enroll template and a quality score
//
OperationStatusCode FPOperation::ScanTemplateScore(UF_UINT32* templ_size,UF_BYTE* templ_data , int timeOut ,UF_RET_CODE* Result, UF_UINT32* p_iqs)
{
	OperationStatusCode res;
	UF_RET_CODE ocresult;
	UF_BYTE packet[UF_PACKET_LEN];
	UF_UINT32 TemplateSize = 0;
	UF_UINT32 ImageQualityScore = 0;
printf("enroll-score : high sensitivity and 10s timeout\n");
	uf_write_system_parameter(UF_SYS_SENSITIVIY, 0x37 );
	uf_write_system_parameter(UF_SYS_TIMEOUT, 0x3A );
	//Reset();
printf("enroll-score : turn off auto and free\n");
	uf_write_system_parameter( UF_SYS_AUTO_RESPONSE, 0x30 );
	uf_write_system_parameter( UF_SYS_FREE_SCAN, 0x30 );
	//
printf("enroll-score : scan template\n");
	uf_cancel();
	uf_send_packet( UF_COM_ST, 0, 0, 0 );    // scan template command
	/*ocresult = uf_receive_packet(packet);
	if ( ocresult == UF_RET_SUCCESS && packet[UF_PACKET_COMMAND_POS] == UF_COM_ST && ((UF_BYTE)packet[UF_PACKET_FLAG_POS]) == UF_PROTO_RET_SCAN_SUCCESS)
	{
printf("enroll-score : receive packet\n");
		ocresult = uf_receive_packet(packet);
		if ( ocresult == UF_RET_SUCCESS )
		{
			// get image quality score
			ImageQualityScore = ((UF_UINT32)packet[UF_PACKET_PARAM_POS]);
			ImageQualityScore = ImageQualityScore | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+1])<< 8;
			ImageQualityScore = ImageQualityScore | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+2])<< 16;
			ImageQualityScore = ImageQualityScore | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+3])<< 24;
printf("enroll-score: Image Quality Score is %ld \n", ImageQualityScore);
			*p_iqs = ImageQualityScore;
			// get template size
			TemplateSize = 0;
			TemplateSize = ((UF_UINT32)packet[UF_PACKET_SIZE_POS]);
			TemplateSize = TemplateSize | ((UF_UINT32)packet[UF_PACKET_SIZE_POS+1])<< 8;
			TemplateSize = TemplateSize | ((UF_UINT32)packet[UF_PACKET_SIZE_POS+2])<< 16;
			TemplateSize = TemplateSize | ((UF_UINT32)packet[UF_PACKET_SIZE_POS+3])<< 24;
		}
	}*/

	if( g_sys_config.send_scan_success )
	{
			ocresult = uf_receive_packet(packet);
			if ( ocresult == UF_RET_SUCCESS && packet[UF_PACKET_COMMAND_POS] == UF_COM_ST 
				&& ((UF_BYTE)packet[UF_PACKET_FLAG_POS]) == UF_PROTO_RET_SCAN_SUCCESS)
			{
				//printf("enrollquality : receive\n");
				ocresult = uf_receive_packet(packet);
				if ( res == UF_RET_SUCCESS )
				{
					// get image quality score
					ImageQualityScore = ((UF_UINT32)packet[UF_PACKET_PARAM_POS]);
					ImageQualityScore = ImageQualityScore | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+1])<< 8;
					ImageQualityScore = ImageQualityScore | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+2])<< 16;
					ImageQualityScore = ImageQualityScore | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+3])<< 24;
					//printf("Image Quality Score is %ld \n", ImageQualityScore);
					*p_iqs = ImageQualityScore;
					// get template size
					TemplateSize = 0;
					TemplateSize = ((UF_UINT32)packet[UF_PACKET_SIZE_POS]);
					TemplateSize = TemplateSize | ((UF_UINT32)packet[UF_PACKET_SIZE_POS+1])<< 8;
					TemplateSize = TemplateSize | ((UF_UINT32)packet[UF_PACKET_SIZE_POS+2])<< 16;
					TemplateSize = TemplateSize | ((UF_UINT32)packet[UF_PACKET_SIZE_POS+3])<< 24;
					//printf("Template size is %d bytes\n", TemplateSize);
				}
			}
		}
		else
		{
		    ocresult = uf_receive_packet(packet);
			if ( ocresult == UF_RET_SUCCESS && packet[UF_PACKET_COMMAND_POS] == UF_COM_ST 
				&& ((UF_BYTE)packet[UF_PACKET_FLAG_POS]) == UF_PROTO_RET_SUCCESS)
			{
			   		// get image quality score
					ImageQualityScore = ((UF_UINT32)packet[UF_PACKET_PARAM_POS]);
					ImageQualityScore = ImageQualityScore | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+1])<< 8;
					ImageQualityScore = ImageQualityScore | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+2])<< 16;
					ImageQualityScore = ImageQualityScore | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+3])<< 24;
					//printf("Image Quality Score is %ld \n", ImageQualityScore);
					*p_iqs = ImageQualityScore;
					// get template size
					TemplateSize = 0;
					TemplateSize = ((UF_UINT32)packet[UF_PACKET_SIZE_POS]);
					TemplateSize = TemplateSize | ((UF_UINT32)packet[UF_PACKET_SIZE_POS+1])<< 8;
					TemplateSize = TemplateSize | ((UF_UINT32)packet[UF_PACKET_SIZE_POS+2])<< 16;
					TemplateSize = TemplateSize | ((UF_UINT32)packet[UF_PACKET_SIZE_POS+3])<< 24;
					//printf("Template size is %d bytes\n", TemplateSize);
			}
		}

	if( ocresult == UF_RET_SUCCESS)
	{
printf("enroll-score : get raw data\n");
		ocresult = uf_get_raw_data( templ_data, FP_TEMPLATESIZE );
printf("enroll-score : get raw data response %d\n", res);
		if ( ocresult == UF_RET_SUCCESS )
		{
			res = SC_Success;
		}
		else
		{
			res = SC_Fail;
		}

	}
	else
	{
		res = SC_Fail;
	}
	//Reset();
	uf_cancel();
printf("enroll-score : Finished %d\n",res);
	*Result = ocresult;
	return res;
}