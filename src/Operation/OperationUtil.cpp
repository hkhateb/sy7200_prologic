#include "OperationUtil.hpp"

Code GetSysInputSourceForChar(const char source, SysInputSource & flag)
{
    Code code = SC_OKAY;
    switch(source)
    {
        case 'S':
        {
            flag = SysInputSourceMagnetic;
            break;
        }
        case 'K':
        {
            flag = SysInputSourceKeypad;
            break;
        }
        case 'W':
        {
            flag = SysInputSourceBarcode;
            break;
        }
        case '4':
        {
        	flag = SysInputSourceDate;
        	break;
        }
        case '0':
        {
        	flag = SysInputSourceInvalid;
        	break;
        }
        default:
        {
            code = SC_FAILURE;
        }        
    }    
    return code;
}   
 
Code GetCharForSysInputSource(SysInputSource flag, char & source){
  Code code = SC_OKAY;
  switch(flag){
  case SysInputSourceMagnetic:
    source = '1';
    break;

  case SysInputSourceKeypad:
    source = '2';
    break;

  case SysInputSourceBarcode:
    source = '3';
    break;

  case SysInputSourceProximity:
    source = '8';
    break ;

    /*  default:
    source = 'U';
    break ;*/
  }    

  return code;
}  

 
