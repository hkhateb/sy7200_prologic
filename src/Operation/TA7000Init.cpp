#include "TA7000Init.h"
#include "InterfaceProfileAssignVector.hpp"
#include "InterfaceMessageBadgeVector.hpp"
#include "InterfaceBellScheduleVector.hpp"
#include "InterfaceValidateEntryVector.hpp"
#include "InterfaceAccessControlVector.hpp"
#include "LegacyTrans.hpp"
#include "OperationDriver.hpp"
#include "SystemGlobals.h"

extern LegacySetup SystemSetup; 

TA7000Init::TA7000Init()
{
	
}

TA7000Init::~TA7000Init()
{
	
}

void TA7000Init::InitLegacySetup()
{
	static struct 
	{
		struct 
		{
			InterfaceMessageBadgeHolder * badge;
		}
		message;
		struct 
		{
			SysFunctionKeyItem key [SysFunctionKeyMax];
			SysFunctionDetailItem detail [SysFunctionDetailMax];
		}					
		function;
		struct 
		{
			InterfaceBellScheduleHolder * schedule;
		}
		bell;
		struct 
		{
			InterfaceAccessControlHolder * badge;
		}
		access;
		struct 
		{
			SysProfileMessageItem message [SysProfileMessageMax];
			SysProfileMessageItem defaults [SysProfileMessageDefaults];
			SysProfileEntryItem entry [SysProfileEntryMax];
            InterfaceProfileAssignHolder * assign;
		}
		profile;
		struct 
		{
			InterfaceValidateEntryHolder * entry;
		}
		validate;
	}
	tables;
	tables.message.badge = new InterfaceMessageBadgeVector();
	SystemSetup.message.badge = tables.message.badge;
	SystemSetup.function.key = tables.function.key;
	SystemSetup.function.detail = tables.function.detail;
	tables.bell.schedule = new InterfaceBellScheduleVector();
	SystemSetup.bell.schedule = tables.bell.schedule;
	tables.access.badge = new InterfaceAccessControlVector();
	SystemSetup.access.badge = tables.access.badge;
	SystemSetup.profile.defaults = tables.profile.defaults;
	SystemSetup.profile.message = tables.profile.message;
	SystemSetup.profile.entry = tables.profile.entry;
	tables.profile.assign = new InterfaceProfileAssignVector();
	SystemSetup.profile.assign = tables.profile.assign;
	tables.validate.entry = new InterfaceValidateEntryVector();
 	SystemSetup.validate.entry = tables.validate.entry;
}

Code TA7000Init::LoadLegacySetup()
{
	Code code = SC_OKAY;
	FILE * fp = fopen(CLK_PROGRAM_FILE, "r");
 	if(fp)
 	{
 		//char input_buffer [128];
 		char input_buffer [1024];
 		LegacySetupItem item;
		char * buf_p;
 		for(;;)
 		{
			/* Read into buffer. */
			buf_p = fgets (input_buffer, sizeof input_buffer, fp);
			/* Nothing read so must be done. */
			if (buf_p == NULL) break;	
			
			#ifdef DEBUG
			printf("LoadLegacySetup() : %s", input_buffer);
			#endif
			
			/* Remove trailing whitespace including line feed. */
			str_clip (input_buffer);
			
			/* Process the clock program line. */
			code = item.Parse (input_buffer);
			if (code == SC_LEGACY_EMPTY) continue;
			if (code != SC_OKAY) 
			{
				#ifdef DEBUG
				printf("LoadLegacySetup() : Parse Failure for Tag %s\n", input_buffer);
				#endif
				break;	
			}
	
			code = item.Process (SystemSetup);
			if (code != SC_OKAY)
			{
				#ifdef DEBUG
				printf("LoadLegacySetup() : Process Failure for Tag %s\n", input_buffer);
				#endif
				
				fprintf(stderr, "The above bad command is ignored %s\n", input_buffer);
				continue;
				//break;	
			}	
 		}
 		fclose(fp);
 		return code;
 	}
 	return SC_FAILURE;
}

void TA7000Init::CleanLegacySetup()
{
	if(SystemSetup.message.badge != NULL)
	{
		SystemSetup.message.badge->clear();	
	}
	if(SystemSetup.bell.schedule != NULL)
	{
		SystemSetup.bell.schedule->clear();
	}
	if(SystemSetup.access.badge != NULL)
	{
		SystemSetup.access.badge->clear();
	}
	if(SystemSetup.profile.assign != NULL)
	{
		SystemSetup.profile.assign->clear();
	}
	if(SystemSetup.validate.entry != NULL)
	{
		((InterfaceValidateEntryVector *)SystemSetup.validate.entry)->clear();
	}
	// Reinitialize all the arrays
	for(int i=0; i<SysFunctionKeyMax; i++)
	{
		SystemSetup.function.key[i].key = 0x00;
		SystemSetup.function.key[i].flags = 0x00;
	}
	for(int i=0; i<SysFunctionDetailMax; i++)
	{
		SystemSetup.function.detail[i].key = 0x00;
		SystemSetup.function.detail[i].level = 0x00;
	}
	for(int i=0; i<SysProfileMessageMax; i++)
	{
		SystemSetup.profile.message[i].flags = 0x00;
	}
	for(int i=0; i<SysProfileMessageDefaults; i++)
	{
		SystemSetup.profile.defaults[i].flags = 0x00;
	}
	for(int i=0; i<SysProfileEntryMax; i++)
	{
		SystemSetup.profile.entry[i].index = 0x00;
		for(int j=0; j<SysProfileTimeMax; j++)
		{
			SystemSetup.profile.entry[i].time[j].message = 0x00;
			SystemSetup.profile.entry[i].time[j].window.start = 0x0000;
			SystemSetup.profile.entry[i].time[j].window.stop = 0x0000;
		}
	}
}



