#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "ProjectTasksTable.h"
//#include "fpmodule.h"
#include "sqlitedb.h"
//#include "TAString.h"
//#include "Str_Def.h"

extern sqlite3 *db;

ProjectTaskRecord::ProjectTaskRecord(const char* pName, const char* pCode, const char* tName, const char * tCode)
{
    int len = strlen ( pName );
	ProjectName =(char*) malloc(len+1);
	strcpy(ProjectName,pName);
	ProjectName[len]= '\0';
	len = strlen ( pCode );
	ProjectCode =(char*) malloc(len+1);
	strcpy(ProjectCode,pCode);
	ProjectCode[len]= '\0';
	len = strlen ( tName );
	TaskName = (char*) malloc(len+1);
	strcpy(TaskName,tName);
	TaskName[len]= '\0';
	len = strlen ( tCode );
	TaskCode = (char*) malloc(len+1);
	strcpy(TaskCode,tCode);
	TaskCode[len]= '\0';
}
ProjectTaskRecord::~ProjectTaskRecord()
{
   if(ProjectName) free(ProjectName);
   if(ProjectCode) free(ProjectCode);
   if(TaskName) free(TaskName);
   if(TaskCode) free(TaskCode);
}

void ProjectTaskRecord::SetProjectName(const char* pName)
{
  ProjectName = (char*) realloc(ProjectName,strlen(pName)+1);
  strcpy(ProjectName,pName);
}
void ProjectTaskRecord::SetProjectCode(const char* pCode)
{
  ProjectCode = (char*) realloc(ProjectCode,strlen(pCode)+1);
  strcpy(ProjectCode,pCode);
}
void ProjectTaskRecord::SetTaskName(const char* tName)
{
  TaskName = (char*) realloc(TaskName,strlen(tName)+1);
  strcpy(TaskName,tName);
}
void ProjectTaskRecord::SetTaskCode(const char * tCode)
{
  TaskCode = (char*) realloc(TaskCode,strlen(tCode)+1);
  strcpy(TaskCode,tCode);
}
 ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int ProjectTasksTable::CreateTable()
{
  char* sql_stmt = NULL;
  rc = SQLITE_OK;

  //sprintf(sql_stmt,"CREATE TABLE IF NOT EXISTS FINGERDATA(EXTERNALID CHAR(10), FINGERINDEX INTEGER, PRIVILEGELEVEL INTEGER,  SECURITYLEVEL INTEGER, TEMPLATE CHAR(512),  PRIMARY KEY(EXTERNALID, FINGERINDEX))");
  sql_stmt = (char*) realloc(sql_stmt,strlen("CREATE TABLE IF NOT EXISTS PROJECTTASKS(ID INT,PROJECTNAME TEXT, PROJECTCODE TEXT, TASKNAME TEXT,  TASKCODE TEXT )")+1);
  sprintf(sql_stmt,"CREATE TABLE IF NOT EXISTS PROJECTTASKS(ID INT,PROJECTNAME TEXT, PROJECTCODE TEXT, TASKNAME TEXT,  TASKCODE TEXT )");

  char* zErrMsg ;
  rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
  if( rc != SQLITE_OK ){
    fprintf(stderr, "Can't create table PROJECTTASKS : %s\n", sqlite3_errmsg(db));
    sqlite3_free(zErrMsg);
    if(sql_stmt)free(sql_stmt);
	return rc;
  }
  if(sql_stmt)free(sql_stmt);
  return rc;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int ProjectTasksTable::DeleteTable()
{
   //Drop the table
  char* sql_stmt = NULL;
  sql_stmt =(char*) realloc(sql_stmt,strlen("DROP TABLE IF EXISTS PROJECTTASKS")+1);
  sprintf(sql_stmt,"DROP TABLE IF EXISTS PROJECTTASKS");
  rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
  if( rc != SQLITE_OK ){
    fprintf(stderr, "Can't drop table PROJECTTASKS: %s\n", sqlite3_errmsg(db));
    sqlite3_free(zErrMsg);
    if(sql_stmt)free(sql_stmt);
	return rc;
  }

  if(sql_stmt)free(sql_stmt);
  return rc;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int ProjectTasksTable::InsertRow(const ProjectTaskRecord* p_row)
{

  int num_rows = 0 ;
  int num_cols = 0 ;
  char** result = NULL ;

  const char * pName;
  const char * pCode;
  const char * tName;
  const char * tCode;
  char sql_stmt[1000];

  pName = p_row->GetProjectName();
  pCode = p_row->GetProjectCode();
  tName = p_row->GetTaskName();
  tCode = p_row->GetTaskCode();

  rc = SQLITE_OK;
  debugFX(100,"pName = %s pCode= %s tName = %s tCode = %s\n",pName, pCode , tName , tCode);
  sprintf ( sql_stmt , "PROJECTCODE = '%s' AND TASKCODE = '%s'" ,pCode , tCode);
  debugFX(100,"sql_stmt = %s\n",sql_stmt);
  if(FindRowCount (sql_stmt))
  {
    debugFX(100,"the row exist\n");
	return SQLITE_OK;
  }
  else
  {

	sprintf(sql_stmt,"select MAX(ID) from PROJECTTASKS");
	debugFX(100,"sql_stmt = %s\n",sql_stmt);
	rc = sqlite_prot_get_table( db , sql_stmt , & result , & num_rows , & num_cols , 0 );
	if ( SQLITE_OK != rc )
	{
		fprintf(stderr, "sqlite_prot_get_table FAILED In InsertRow Function \n");
		debugFX(100,"sqlite_prot_get_table FAILED In InsertRow Function \n");
		sqlite3_free_table( result );
		return rc ;
	}
	if ( num_rows < 1 || num_cols < 1 || result == NULL)
	{
        fprintf(stderr, "sqlite_prot_get_table FAILED In num_rows < 1 || num_cols < 1 ||  0 == result \n");
		debugFX(100,"sqlite_prot_get_table FAILED num_rows < 1 || num_cols < 1 ||  0 == result \n");
		sqlite3_free_table( result );
		return rc ;
	}

	int ix = num_rows * num_cols ;
	int id;
	if(result [ ix ] == NULL)
		id = 0;
	else
	    id = atoi ( result [ ix ] );
	id++;
    sprintf(sql_stmt, "INSERT INTO PROJECTTASKS (ID,PROJECTNAME, PROJECTCODE , TASKNAME , TASKCODE) VALUES (%d,'%s', '%s', '%s', '%s')",id,pName, pCode , tName , tCode);
	debugFX(100,"sql_stmt = %s\n",sql_stmt);
	rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
    if( rc != SQLITE_OK ){
	    fprintf(stderr, "Can't insert record in PROJECTTASKS: %s\n", sqlite3_errmsg(db));
       sqlite3_free(zErrMsg);
       sqlite3_free_table( result );
	   return rc ;
	}
	sqlite3_free_table( result );
	return rc;

  }
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int ProjectTasksTable::DeleteRow(int ID)
{
    char sql_stmt[1000];

	rc = SQLITE_OK;
	sprintf ( sql_stmt , "ID = %d" ,ID);
	debugFX(100,"sql_stmt = %s\n",sql_stmt);
	if(FindRowCount (sql_stmt))
	{

		sprintf ( sql_stmt ,"delete from PROJECTTASKS where ID = %d" ,ID);
        rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
		if( rc != SQLITE_OK )
		{
			fprintf(stderr, "delete from PROJECTTASKS : Can't delete row in PROJECTTASKS: %s\n", sqlite3_errmsg(db));
			sqlite3_free(zErrMsg);
			return rc ;
		}
		sprintf ( sql_stmt ,"update PROJECTTASKS SET ID = %d where ID = (select MAX(ID) from PROJECTTASKS) AND ID > %d",ID,ID);
		rc = sqlite_prot_exec(db, sql_stmt, NULL, NULL, &zErrMsg );
		if( rc != SQLITE_OK )
		{
			fprintf(stderr, "update PROJECTTASKS : Can't delete row in PROJECTTASKS: %s\n", sqlite3_errmsg(db));
			sqlite3_free(zErrMsg);
			return rc ;
		}
        return rc;
	}
	else
     fprintf(stderr, "ID row in PROJECTTASKS Out Off Range\n");
	return rc;
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int ProjectTasksTable::getMaxID()
{
  int num_rows = 0 ;
  int num_cols = 0 ;
  char** result = NULL ;
  char sql_stmt[100];

  sprintf(sql_stmt,"select MAX(ID) from PROJECTTASKS");
  debugFX(100,"sql_stmt = %s\n",sql_stmt);
  rc = sqlite_prot_get_table( db , sql_stmt , & result , & num_rows , & num_cols , 0 );
  if ( SQLITE_OK != rc )
  {
	fprintf(stderr, "sqlite_prot_get_table FAILED In getMaxID Function \n");
	debugFX(100,"sqlite_prot_get_table FAILED\n");
	sqlite3_free_table( result );
	return -1 ;
  }
  if ( num_rows < 1 || num_cols < 1 || result == NULL)
  {
    fprintf(stderr, "sqlite_prot_get_table FAILED In num_rows < 1 || num_cols < 1 ||  0 == result \n");
	debugFX(100,"sqlite_prot_get_table FAILED num_rows < 1 || num_cols < 1 ||  0 == result \n");
	sqlite3_free_table( result );
	return -1 ;
  }

	int ix = num_rows * num_cols ;
	int id;
	if(result [ ix ] == NULL)
		id = 0;
	else
	    id = atoi ( result [ ix ] );
	sqlite3_free_table( result );
	return id;

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int ProjectTasksTable::getID(const ProjectTaskRecord* p_row)
{
  int num_rows = 0 ;
  int num_cols = 0 ;
  char** result = NULL ;

  const char * pCode;
  const char * tCode;
  char sql_stmt[1000];
  pCode = p_row->GetProjectCode();
  tCode = p_row->GetTaskCode();
  rc = SQLITE_OK;

  debugFX(100,"pCode= %s tCode = %s\n",pCode , tCode);
  sprintf ( sql_stmt , "PROJECTCODE = '%s' AND TASKCODE = '%s'" ,pCode , tCode);
  debugFX(100,"sql_stmt = %s\n",sql_stmt);
  if(FindRowCount (sql_stmt))
  {
	sprintf(sql_stmt,"select ID from PROJECTTASKS where PROJECTCODE = '%s' AND TASKCODE = '%s'" ,pCode , tCode);
	debugFX(100,"sql_stmt = %s\n",sql_stmt);
	rc = sqlite_prot_get_table( db , sql_stmt , & result , & num_rows , & num_cols , 0 );
	if ( SQLITE_OK != rc )
	{
		fprintf(stderr, "sqlite_prot_get_table FAILED In getID Function \n");
		debugFX(100,"sqlite_prot_get_table FAILED\n");
		sqlite3_free_table( result );
		return -1 ;
	}
	if ( num_rows < 1 || num_cols < 1 || result == NULL)
	{
		fprintf(stderr, "sqlite_prot_get_table FAILED In num_rows < 1 || num_cols < 1 ||  0 == result \n");
		debugFX(100,"sqlite_prot_get_table FAILED num_rows < 1 || num_cols < 1 ||  0 == result \n");
		sqlite3_free_table( result );
		return -1 ;
	}

	int ix = num_rows * num_cols ;
	int id;
	if(result [ ix ] == NULL)
		id = -1;
	else
	    id = atoi ( result [ ix ] );
	sqlite3_free_table( result );
	return id;

  }
  else
  {
	  sqlite3_free_table( result );
	  return -1;
  }
}
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
int ProjectTasksTable::FindRowCount ( const char* where_clause )
{
  int num_rows ;
  int num_cols ;
  char** result ;
  char sql [ 1000 ] ;
  if ( 0 == where_clause ){
    strcpy ( sql , const_sql_cnt_projecttasks );
  } else {
    sprintf ( sql , "%s WHERE %s" , const_sql_cnt_projecttasks , where_clause );
	debugFX(100,"sql = %s\n",sql);
  }
  if ( SQLITE_OK != sqlite_prot_get_table( db , sql , & result , & num_rows , & num_cols , 0 )){
     debugFX(100,"sqlite_prot_get_table FAILED \n");
	 return 0 ;
  }
  debugX(100,"num_rows = %d num_cols = %d \n ",num_rows,num_cols);

  if ( num_rows < 1 ){
	  return 0 ;
  }

  if ( 0 == result ){
	  return 0 ;
  }

  int ix = num_rows * num_cols ;
  int ret = atoi ( result [ ix ] ) ;
  sqlite3_free_table( result );

  return ret ;
}
