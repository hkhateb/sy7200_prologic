#ifndef PROJECTTASKSTABLE_H
#define PROJECTTASKSTABLE_H

#include <stdio.h>
#include <string.h>

#include <sqlite3.h>

#define SQLSPROJECTDEBUG                  1
#if defined (SQLSPROJECTDEBUG) && SQLSPROJECTDEBUG > 0
	#define debug(fmt, args...) printf(fmt, ## args)
	#define debugF(fmt, args...) printf("%s: " fmt, __func__, ## args)
	#define debugX(lvl, fmt, args...) if (SQLSPROJECTDEBUG >= lvl) debug(fmt, ## args);
	#define debugFX(lvl, fmt, args...) if (SQLSPROJECTDEBUG >= lvl) debugF(fmt, ## args);
#else
	#define debug(fmt, args...)
	#define debugF(fmt, args...)
	#define debugX(lvl, fmt, args...)
	#define debugFX(lvl, fmt, args...)
#endif

#define projecttasks_table "PROJECTTASKS" 
#define const_sql_all_projecttasks "SELECT * FROM " projecttasks_table 
#define const_sql_cnt_projecttasks "SELECT COUNT(*) FROM " projecttasks_table 

class ProjectTaskRecord
{
 private:
  char* ProjectName;
  char* ProjectCode;
  char* TaskName;
  char* TaskCode;
	
 public:
  ProjectTaskRecord() : 
    ProjectName ( NULL ),
    ProjectCode ( NULL ),
    TaskName ( NULL ),
	TaskCode( NULL ){};

  ProjectTaskRecord(const char* pName, const char* pCode, const char* tName, const char * tCode);
  //Fptemplate_record ( char** fields );//used when re-constituting a row from a select callback
  ~ProjectTaskRecord();
	
  const char* GetProjectName() const		{return ProjectName; 	};
  const char* GetProjectCode() const 		{return ProjectCode; 	};
  const char* GetTaskName() const		{return TaskName; };
  const char* GetTaskCode() const	{return TaskCode; 	};
	
  void SetProjectName(const char* pName);
  void SetProjectCode(const char* pCode);
  void SetTaskName(const char* tName);
  void SetTaskCode(const char * tCode);

};

class ProjectTasksTable
{
 public:
  class QueryCallback {
  public:
    virtual int OnData ( const ProjectTaskRecord& r ) = 0 ; //non-zero return: abort traversal
  };

 private:
  int rc ;
  char* zErrMsg ;
  //callback from sql: re-format record data into correct type
  static int callback(void* p , int argc, char **argv, char **azColName);

  

public:
  ProjectTasksTable():zErrMsg(NULL){};
  ~ProjectTasksTable(){};
  int FindRowCount(const char* where_clause = "" );
  int CreateTable();
  int DeleteTable();
  int InsertRow(const ProjectTaskRecord* p_row);
  int DeleteRow(int ID);
  int getMaxID(); 
  int getID(const ProjectTaskRecord* p_row);
};

#endif   //PROJECTTASKSTABLE_H
