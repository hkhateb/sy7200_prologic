#include <stdio.h>
#include <string.h>


extern "C" 
{
	#include <ctpublic.h>
	#include "common.h"
}

class ServerDataBaseConnection
{
	private:

		CS_CONTEXT *ctx;
		CS_CONNECTION *conn;
		CS_COMMAND *cmd;

    public:
        CS_RETCODE connectionResult;
		enum {maxcol=20, colsize=100};
	
		struct _col 
		{ 
			CS_DATAFMT datafmt;
			CS_INT datalength;
			CS_SMALLINT ind;
			CS_CHAR data[colsize];
		} col[maxcol];

        ServerDataBaseConnection(char* configFile);
		~ServerDataBaseConnection();
		CS_RETCODE RunQueryWithoutResult(char* query);
		CS_RETCODE RunQueryWithResult(char* query);
        CS_RETCODE GetRowResult(int& row_count , int& num_cols);

};
