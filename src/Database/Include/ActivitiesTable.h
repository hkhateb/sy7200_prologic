#ifndef ACTIVITIESTABLE_H
#define ACTIVITIESTABLE_H

#include <stdio.h>
#include <string.h>

#include <sqlite3.h>

#define activities_table "ACTIVITIESTABLE" 
#define const_sql_all_activities "SELECT * FROM " activities_table 
#define const_sql_cnt_activities "SELECT COUNT(*) FROM " activities_table 

class ActivityRecord
{
 private:
  char* ActivityName;
  char* ActivityCode;
 public:
  ActivityRecord() : 
    ActivityName ( NULL ),
    ActivityCode ( NULL ){};

  ActivityRecord(const char* AName, const char* ACode);
  //Fptemplate_record ( char** fields );//used when re-constituting a row from a select callback
  ~ActivityRecord();
	
  const char* GetActivityName() const		{return ActivityName; 	};
  const char* GetActivityCode() const 		{return ActivityCode; 	};
	
  void SetActivityName(const char* AName);
  void SetActivityCode(const char* ACode);

};

class ActivitiesTable
{
 public:
  class QueryCallback {
  public:
    virtual int OnData ( const ActivityRecord& r ) = 0 ; //non-zero return: abort traversal
  };

 private:
  int rc ;
  char* zErrMsg ;
  //callback from sql: re-format record data into correct type
  static int callback(void* a , int argc, char **argv, char **azColName);

  

public:
  ActivitiesTable():zErrMsg(NULL){};
  ~ActivitiesTable(){};
  int FindRowCount(const char* where_clause = "" );
  int CreateTable();
  int DeleteTable();
  int InsertRow(const ActivityRecord* a_row,int *id);
  int DeleteRow(int ID);
  int getMaxID(); 
  int getID(const ActivityRecord* a_row);
};

#endif
