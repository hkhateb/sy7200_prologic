#ifndef OFFLINETRANTABLE_H
#define OFFLINETRANTABLE_H

#include <stdio.h>
#include <string.h>

#include <sqlite3.h>

/*#define OLTDEBUG                   100

#if defined (OLTDEBUG) && OLTDEBUG > 0
	#define debug(fmt, args...) printf(fmt, ## args)
	#define debugF(fmt, args...) printf("%s: " fmt, __func__, ## args)
	#define debugX(lvl, fmt, args...) if (OLTDEBUG >= lvl) debug(fmt, ## args);
	#define debugFX(lvl, fmt, args...) if (OLTDEBUG >= lvl) debugF(fmt, ## args);
#else
	#define debug(fmt, args...)
	#define debugF(fmt, args...)
	#define debugX(lvl, fmt, args...)
	#define debugFX(lvl, fmt, args...)
#endif*/

#define offlinetran_table "OFFLINETRANTABLE" 
#define const_sql_all_offlinetran "SELECT * FROM " offlinetran_table 
#define const_sql_cnt_offlinetran "SELECT COUNT(*) FROM " offlinetran_table 

class OffLineTranRecord
{
 private:
  char* Transaction;
  char* Time;
  char* EmployeeId;
  char* Group;
  char* Project;
  char* Task;
  char* Activity;
  char* ProjectSwitch;

public:
    OffLineTranRecord() : 
    Transaction ( NULL ),
    Time( NULL ),
    EmployeeId ( NULL ),
	Group( NULL ),
	Project( NULL ) ,
	Task( NULL ),
	Activity( NULL ),
	ProjectSwitch( NULL ){};

  OffLineTranRecord(const char* Tran, const char* T = NULL, const char* EmpId = NULL, const char * Gro = NULL, const  char * Pro = NULL, const char * Ta = NULL, const char * Act = NULL ,const char * PSwitch = NULL );
  //Fptemplate_record ( char** fields );//used when re-constituting a row from a select callback
  ~OffLineTranRecord();
	
  const char* GetTransaction() const		{return Transaction; 	};
  const char* GetTime() const 		{return Time; 	};
  const char* GetEmployeeId() const		{return EmployeeId; };
  const char* GetGroup() const	{return Group; 	};
  const char* GetProject() const	{return Project; 	};
  const char* GetTask() const	{return Task; 	};
  const char* GetActivity() const	{return Activity; 	};
  const char* GetProjectSwitch() const	{return ProjectSwitch; 	};

  void SetTransaction(const char* Tran);
  void SetTime(const char* T);
  void SetEmployeeId(const char* EmpId);
  void SetGroup(const char * Gro);
  void SetProject(const char * Pro);
  void SetTask(const char * Ta);
  void SetActivity(const char * Act);
  void SetProjectSwitch(const char * PSwitch);

};

class OffLineTranTable
{
 public:
  class QueryCallback {
  public:
    virtual int OnData ( const OffLineTranRecord& r ) = 0 ; //non-zero return: abort traversal
  };

 private:
  int rc ;
  char* zErrMsg ;
  char** resultTable ;
  int tableValidation;
  //callback from sql: re-format record data into correct type
  static int callback(void* p , int argc, char **argv, char **azColName);

  

public:
  OffLineTranTable():zErrMsg(NULL){tableValidation = 0;};
		
  ~OffLineTranTable(){if(tableValidation) sqlite3_free_table(resultTable);};
  int FindRowCount(const char* where_clause = "" , int* frc = NULL);
  int CreateTable();
  int DeleteTable();
  int InsertRow(const OffLineTranRecord* p_row);
  int DeletRows(const OffLineTranRecord* p_row);
  char** getRsultTable(){return resultTable;};
  char** SelectTable(const char* where_clause ,unsigned int limit ,int* num_rows , int* num_cols);

};

#endif   //OFFLINETRANTABLE
