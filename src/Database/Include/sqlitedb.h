#ifndef _SQLITEDB_H_
#define _SQLITEDB_H_

#include <stdio.h>
#include <sqlite3.h>
#include "ProjectTasksTable.h"
#include "OffLineTranTable.h"
#include "ActivitiesTable.h" \

#ifdef USE_PIN_SUPPORT
#include "PinTable.h"
#endif

enum TableName
{
	TBL_ProjectTasksTable = 0 ,
	TBL_OffLineTranTable ,
	TBL_ActivitiesTable ,
	TBL_PinTable
};

int sqlite_prot_exec(
  sqlite3*,                     /* An open database */
  const char *sql,              /* SQL to be executed */
  sqlite3_callback,             /* Callback function */
  void *,                       /* 1st argument to callback function */
  char **errmsg                 /* Error msg written here */
  );

int sqlite_prot_get_table(
  sqlite3*,               /* An open database */
  const char *sql,       /* SQL to be executed */
  char ***resultp,       /* Result written to a char *[]  that this points to */
  int *nrow,             /* Number of result rows written here */
  int *ncolumn,          /* Number of result columns written here */
  char **errmsg          /* Error msg written here */
);

class SqliteDB
{
private:
		//sqlite3 *db;
		int rc;
		char *zErrMsg;

public:
	    ProjectTasksTable projectTasks_tbl;
		OffLineTranTable  offlineTran_tbl;
		ActivitiesTable   activities_tbl;
#ifdef USE_PIN_SUPPORT
		PinTable   pin_tbl;
#endif
		SqliteDB() :
			projectTasks_tbl()
			,offlineTran_tbl()
			,activities_tbl()
#ifdef USE_PIN_SUPPORT
			,pin_tbl()
#endif
		{zErrMsg = 0;};

		~SqliteDB(){};
		int intialise_db();
		int create_table(enum TableName table_name);
		int delete_table(enum TableName table_name);
		int close_db();
};

#endif  //_SQLITEDB_H_
