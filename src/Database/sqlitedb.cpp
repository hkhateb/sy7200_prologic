#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include "sqlitedb.h"

namespace {
  class guard {
  public:
    guard (){
      pthread_mutex_lock ( & _ );
    }

    ~guard (){
      pthread_mutex_unlock ( & _ );
    }

    static pthread_mutex_t _ ;
  };

  //********************************************************************
  pthread_mutex_t guard ::_ = PTHREAD_MUTEX_INITIALIZER ;

};

sqlite3 *db;    //defined globally. All other classes shud refer to it using 'extern'

int SqliteDB::intialise_db()
{
	rc = sqlite3_open("TA", &db);
	if( rc ){
	    fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(db));
	    sqlite3_close(db);
	}
	sqlite3_busy_timeout( db , 5000 );
	//Open db in Sync mode - for faster speed.
	sqlite3_exec(db, "PRAGMA cache_size=1000", NULL, NULL, NULL);
	sqlite3_exec(db, "PRAGMA default_cache_size=1000", NULL, NULL, NULL);

    return rc;
}

int SqliteDB::close_db()
{
	return sqlite3_close(db);
}

int SqliteDB::create_table(enum TableName table_name)
{

	switch(table_name)
	{
		case TBL_ProjectTasksTable:
			rc = projectTasks_tbl.CreateTable();
        case TBL_OffLineTranTable:
			rc = offlineTran_tbl.CreateTable();
        case TBL_ActivitiesTable:
			rc = activities_tbl.CreateTable();
#ifdef USE_PIN_SUPPORT
        case TBL_PinTable:
			rc = pin_tbl.CreateTable();
#endif
		default:
			break;

	}
	return rc;
}

int SqliteDB::delete_table(enum TableName table_name)
{
	switch(table_name)
	{
		case TBL_ProjectTasksTable:
            rc = projectTasks_tbl.DeleteTable();
		case TBL_OffLineTranTable:
            rc = offlineTran_tbl.CreateTable();
		case TBL_ActivitiesTable:
            rc = activities_tbl.CreateTable();
#ifdef USE_PIN_SUPPORT
		case TBL_PinTable:
			rc = pin_tbl.DeleteTable();
#endif
		default:
			break;

	}
	return rc;
}


int sqlite_prot_exec(
  sqlite3* p1,                     /* An open database */
  const char *sql,              /* SQL to be executed */
  sqlite3_callback p3,             /* Callback function */
  void * p4,                       /* 1st argument to callback function */
  char **errmsg                 /* Error msg written here */
  ){
  guard g ;
  return sqlite3_exec ( p1 , sql , p3 , p4 , errmsg );
}

int sqlite_prot_get_table(
  sqlite3* p1,               /* An open database */
  const char *sql,       /* SQL to be executed */
  char ***resultp,       /* Result written to a char *[]  that this points to */
  int *nrow,             /* Number of result rows written here */
  int *ncolumn,          /* Number of result columns written here */
  char **errmsg          /* Error msg written here */
  ){
  guard g ;
  return sqlite3_get_table ( p1 , sql , resultp , nrow , ncolumn , errmsg );
}

