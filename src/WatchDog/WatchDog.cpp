#include <mcheck.h>
#include <time.h>
#include <pthread.h>
#include <unistd.h>
//#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include "Include/WatchDog.h"

using namespace std;

/**
 *      Main class of the Watchdog command program
 *
 */

#define WATCH_FILE     "/mnt/flash/terminal/bin/ta.watched"
#define WATCHDOG_FILE     "/mnt/flash/terminal/bin/ta.watchdog"

int main(int argc, char* argv[])
{
	WatchDog watchdog;
	sleep(3); //To make sure that watchdog time is greater than the last updated tstamp in ta.watched file
	watchdog.Run();

	return 1;
}


WatchDog::WatchDog()
{
	auto_restart = false;
}

WatchDog::~WatchDog()
{
}

/**
 *  Main function of WatchDog command
 */
void WatchDog::Run()
{
	FILE * fp;
	char line[58];
	char systime[60];

	for(;;)
	{
		fp = fopen(WATCH_FILE, "r");
		if( !fp )
		{
		#ifdef DEBUG
		   printf("Couldn't open the watch file");
		   //may be the clock app is not up yet
		#endif
		   //sleep for 2 sec, and try again
		   sleep(2);
		   continue;
		 }

		 //go to the end of the file.
		 //Read the last line

		// fseek(fp, -25L, SEEK_END);
		 char * tstamp = fgets(line, 58, fp);
	 	 #ifdef DEBUG
		 	printf("\n WatchDog :line from file = %s", tstamp);
		 #endif
		 //close the file
		 fclose(fp);
		 auto_restart = false;

		 if(strstr(line, "AUTO RESTART") != NULL)
		 {
			auto_restart = true;
			//This is auto restart timestamp.
			if(strstr(line, "DAILY") != NULL)
			{
				//AUTO RESTART DAILY : 23:25
				char daily_tst[6];
				strcpy(daily_tst, tstamp + 21);
				daily_tst[strlen(daily_tst)] = '\0';
				#ifdef DEBUG
					printf("\ndaily_tst = %s", daily_tst);
				#endif
				strcpy(systime, getFormattedTimeOfDay());
				#ifdef DEBUG
					printf("\n WatchDog: systime = %s", systime);
				#endif

				if( ! CompareTimeForRestartDaily(daily_tst, systime))
				{
					#ifdef DEBUG
					 	printf("\n clock needs reboot. - break");
					 	printf("\n sleep for 1 min");
					 	printf("\n");
				 	#endif
				// 	sleep(60); //let that minute pass.
					break;
				}

			}
			else if(strstr(line, "WEEKLY") != NULL)
			{
				//AUTO RESTART WEEKLY : Mon 23:25
				char weekly_tst[12];
				strcpy(weekly_tst, tstamp + 22);
				weekly_tst[strlen(weekly_tst)] = '\0';
				#ifdef DEBUG
					printf("\nweekly_tst = %s", weekly_tst);
				#endif
				strcpy(systime, getFormattedTimeOfWeek());
				#ifdef DEBUG
					printf("\n WatchDog: systime = %s", systime);
				#endif

				if( ! CompareTimeForRestartWeekly(weekly_tst, systime))
				{
					#ifdef DEBUG
					 	printf("\n clock needs reboot. - break");
					 	printf("\n sleep for 1 min");
					 	printf("\n");
				 	#endif
				 //	sleep(60); //let that minute pass.
					break;
				}
			}
		 }
		 else
		 {
			 //Read the current time
			 strcpy(systime, getFormattedTime());
			 #ifdef DEBUG
			 	printf("\n WatchDog: systime = %s", systime);
			 #endif

		 	 //compare the times. The difference should be less than 15 sec
			 if( ! CompareTimeStamps(tstamp, systime))
			 {
			 	//clock needs reboot
			 	#ifdef DEBUG
				 	printf("\n clock needs reboot. - break");
				 	printf("\n");
			 	#endif

				break;
			 }
	 	}

		 #ifdef DEBUG
			 printf("\n sleep for 5 sec");
			 printf("\n");
		 #endif

		 sleep(5);  //sleep for 5 sec**/
	}

	RebootClock();
}

bool WatchDog::CompareTimeStamps(const char * tstamp, const char * systime)
{
	struct tm c_tm;
	struct tm w_tm;
	time_t c_t;
	time_t w_t;

 #ifdef DEBUG
	printf("\n WatchDog: tstamp = %s", tstamp);
	printf("\n WatchDog: systime = %s", systime);
	printf("\n convert the tstamp string to time");
 #endif

	if (strptime(tstamp, "%a %b %d %H:%M:%S %Y", &c_tm) == NULL)
	{
		#ifdef DEBUG
    		printf("\n can not convert time");
    	#endif

    	return true;
	}

#ifdef DEBUG
	printf("\nyear: %d; month: %d; day: %d;\n",
        c_tm.tm_year, c_tm.tm_mon, c_tm.tm_mday);
	printf("\nhour: %d; minute: %d; second: %d\n",
	        c_tm.tm_hour, c_tm.tm_min, c_tm.tm_sec);
	printf("\nweek day: %d; year day: %d\n", c_tm.tm_wday, c_tm.tm_yday);
#endif

	c_tm.tm_isdst = -1;      /* Not set by strptime(); tells mktime()
	                          to determine whether daylight saving time
	                          is in effect */
	c_t = mktime(&c_tm);
	if (c_t == -1)
	{
		#ifdef DEBUG
			printf("\n mktime failed");
		#endif

		return true;
	}
#ifdef DEBUG
	printf("\nseconds since the Epoch: %ld\n", (long) c_t);
	printf("\n convert the systime string to time");
#endif

	if (strptime(systime, "%a %b %d %H:%M:%S %Y", &w_tm) == NULL)
	{
   	 	#ifdef DEBUG
			printf("\n can not convert time");
		#endif

		return true;
	}

#ifdef DEBUG
	printf("\nyear: %d; month: %d; day: %d;\n",
        w_tm.tm_year, w_tm.tm_mon, w_tm.tm_mday);
	printf("\nhour: %d; minute: %d; second: %d\n",
	        w_tm.tm_hour, w_tm.tm_min, w_tm.tm_sec);
	printf("\nweek day: %d; year day: %d\n", w_tm.tm_wday, w_tm.tm_yday);
#endif

	w_tm.tm_isdst = -1;      /* Not set by strptime(); tells mktime()
	                          to determine whether daylight saving time
	                          is in effect */
	w_t = mktime(&w_tm);
	if (w_t == -1)
	{
		#ifdef DEBUG
			printf("\n mktime failed");
		#endif

		return true;
	}

#ifdef DEBUG
	printf("\nseconds since the Epoch: %ld\n", (long) w_t);
#endif

	int time_diff = w_t - c_t;

#ifdef DEBUG
	printf("\n Time Difference = %d", time_diff);
#endif

	if(time_diff > 15)
	{
		#ifdef DEBUG
			printf("\n time diff > 15. Clock needs reboot");
		#endif

		return false;
	}

	return true;
}

bool WatchDog::CompareTimeForRestartWeekly(const char * tstamp, const char * systime)
{

	struct tm c_tm;
	struct tm w_tm;
	time_t c_t;
	time_t w_t;

 #ifdef DEBUG
	printf("\n WatchDog: tstamp = %s", tstamp);
	printf("\n WatchDog: systime = %s", systime);
	printf("\n convert the tstamp string to time");
 #endif

	if (strptime(tstamp, "%a %H:%M", &c_tm) == NULL)
	{
		#ifdef DEBUG
    		printf("\n can not convert time");
    	#endif

    	return true;
	}

#ifdef DEBUG
	printf("\nhour: %d; minute: %d; week day: %d\n",
	        c_tm.tm_hour, c_tm.tm_min, c_tm.tm_wday);
	printf("\n convert the systime string to time");
#endif

	if (strptime(systime, "%a %H:%M:%S", &w_tm) == NULL)
	{
		#ifdef DEBUG
    		printf("\n can not convert time");
    	#endif

    	return true;
	}

	#ifdef DEBUG
	printf("\nhour: %d; minute: %d; week day: %d\n",
	        w_tm.tm_hour, w_tm.tm_min, w_tm.tm_wday);
	#endif


	if(w_tm.tm_wday == c_tm.tm_wday)
	{
		#ifdef DEBUG
			printf("\n days match");
			printf("\n");
		#endif
		if(w_tm.tm_hour == c_tm.tm_hour)
		{
			#ifdef DEBUG
				printf("\n hours match");
				printf("\n");
			#endif
			if(c_tm.tm_min == 0)
			{
				if(w_tm.tm_min == c_tm.tm_min)
				{
					//sleep(60);
					int sleeptime = 60 - w_tm.tm_sec;
				//	printf("\n sleeptime = %d", sleeptime);
				//	printf("\n");
					sleep(sleeptime);
					return false;
				}
			}
			if(w_tm.tm_min == (c_tm.tm_min - 1))
			{
				#ifdef DEBUG
					printf("\n minutes match");
					printf("\n");
				#endif
			//	printf("\n sys seconds = %d", w_tm.tm_sec);

				int sleeptime = 60 - w_tm.tm_sec;
			//	printf("\n sleeptime = %d", sleeptime);
			//	printf("\n");
				sleep(sleeptime);

				//needs autoreboot
				return false;
			}
		}
	}
	return true;
}


bool WatchDog::CompareTimeForRestartDaily(const char * tstamp, const char * systime)
{
	struct tm c_tm;
	struct tm w_tm;
	time_t c_t;
	time_t w_t;

 #ifdef DEBUG
	printf("\n WatchDog: tstamp = %s", tstamp);
	printf("\n WatchDog: systime = %s", systime);
	printf("\n convert the tstamp string to time");
 #endif

	if (strptime(tstamp, "%H:%M", &c_tm) == NULL)
	{
		#ifdef DEBUG
    		printf("\n can not convert time");
    	#endif

    	return true;
	}

#ifdef DEBUG
	printf("\nhour: %d; minute: %d\n",
	        c_tm.tm_hour, c_tm.tm_min);
	printf("\n convert the systime string to time");
#endif

	if (strptime(systime, "%H:%M:%S", &w_tm) == NULL)
	{
   	 	#ifdef DEBUG
			printf("\n can not convert time");
		#endif

		return true;
	}

#ifdef DEBUG
	printf("\nhour: %d; minute: %d\n",
        w_tm.tm_hour, w_tm.tm_min);
#endif

	if(w_tm.tm_hour == c_tm.tm_hour)
	{
		#ifdef DEBUG
			printf("\n hours match");
			printf("\n");
		#endif
		if(c_tm.tm_min == 0)
		{
			if(w_tm.tm_min == c_tm.tm_min)
			{
				//sleep(60);
				int sleeptime = 60 - w_tm.tm_sec;
			//	printf("\n sleeptime = %d", sleeptime);
			//	printf("\n");
				sleep(sleeptime);
				return false;
			}
		}
		else if(w_tm.tm_min == (c_tm.tm_min - 1))
		{
			#ifdef DEBUG
				printf("\n minutes match");
				printf("\n");
			#endif

		//	printf("\n sys seconds = %d", w_tm.tm_sec);

			int sleeptime = 60 - w_tm.tm_sec;
		//	printf("\n sleeptime = %d", sleeptime);
		//	printf("\n");
			sleep(sleeptime);
			//needs autoreboot
			return false;
		}
	}
	return true;
}


void WatchDog::RebootClock()
{
	char msg[128];
	long maxFileSize = 1024; //default - 1 Kb
	bool file_ok = true;
	if( ! auto_restart)
	{
		//delete the watched file. if not deleted the watchdog will read this file after clock reboots,
		//and the time_diff will always be > 15
		if( remove(WATCH_FILE) == -1 )
		{
			#ifdef DEBUG
	    		printf( "Error deleting watch file" );
	    	#endif
	    	return;
		}

	  	#ifdef DEBUG
	    	printf( "Watch File successfully deleted" );
		#endif
	}

	//save the time in a file

	FILE * fp = fopen(WATCHDOG_FILE, "a");
	if( !fp )
	{
		fprintf(stderr, "Couldn't open the watchdog file\n");
		//return;
	}
	else
	{
		// Check the file size to check if we need to rename
		fseek(fp, 0, SEEK_END);
	    long size = ftell(fp);
	    if(size > maxFileSize)
	    {
		    #ifdef DEBUG
	    		printf("\n MAX file size reached. rewind file");
	    	#endif
		    fclose(fp);
	    	remove(WATCHDOG_FILE);
	    	fp = fopen(WATCHDOG_FILE, "a");
	    	if( !fp )
			{
				fprintf(stderr, "Couldn't open the watchdog file\n");
				file_ok = false;
			}
		}

		if(file_ok)
		{
	    	strcpy(msg, "CLOCK REBOOT : ");
			strcat(msg, getFormattedTime());
			fputs(msg, fp);
			fputs("\n", fp);
			fclose(fp);
		}
	}

	// Reboot the System
#ifdef DEBUG
	printf("Rebooting System...\n");
#endif

	// Save the latest time
	//system("hwclock -w");


	system("/home/terminal/bin/Resetapplication.sh");
	sleep(10);
}

// Gets formatted time
char * WatchDog::getFormattedTime()
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	char * time = asctime( optimeinfo );

	// Remove the "\n" at the end
	*(time + strlen(time) - 1) = '\0';
	return time;
}

char * WatchDog::getFormattedTimeOfDay()
{
	static char buffer[9];
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	strftime(buffer, 9, "%H:%M:%S", optimeinfo);
	return buffer;
}

char * WatchDog::getFormattedTimeOfWeek()
{
	static char buffer[14];
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	strftime(buffer, 14, "%a %H:%M:%S", optimeinfo);
	return buffer;
}


