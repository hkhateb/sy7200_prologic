#include <stdlib.h>
#include <typeinfo>
#include <ctype.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "Str_Def.h"
#include "LegacyTrans.hpp"
#include "Legacy.hpp"
#include "TransUtil.hpp"
#include "SystemGlobals.h"

using namespace std;

//extern int errno;

extern LegacySetup SystemSetup; 
Code _TimeSecondsGet (InterfaceBuffer & buffer, SysTimeSecs & time);
size_t _TimeSecondsPut (InterfaceBuffer & buffer, SysTimeSecs time);
Code _TimeGet (InterfaceBuffer & buffer, SysTime & time);
size_t	_TimePut (InterfaceBuffer & buffer, SysTime time);
Code _DateGet (InterfaceBuffer & buffer, SysDate & date);
size_t _DatePut (InterfaceBuffer & buffer, SysDate & date);

pthread_mutex_t LegacyTransItem::pollingLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t LegacyTransItem::programmingLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t LegacyTransItem::transactionFileLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t LegacyTransItem::templateLock = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t LegacyTransItem::DBLock = PTHREAD_MUTEX_INITIALIZER;

LegacyTransItem * LegacyTransItem::myinstance = NULL;

bool LegacyTransItem::clockIsFull = false;

LegacyTransItem * LegacyTransItem::GetInstance()
{
  if( myinstance == NULL )
    myinstance = new LegacyTransItem();
  return myinstance;
}    

Code LegacyTransItem::Process()
{
  Code code = SC_OKAY;
  return code;
}

/**
 * This function is not thread safe.
 * Please do not call thsi function without acquiring
 * the Transaction File Lock
 */
Code LegacyTransItem::ProcessData(const char * line, bool isLoad)
{
#ifdef DEBUG
  printf("Processing Data - %s\n", line);
#endif
  Code code = SC_OKAY;
  const char * lp = line;
    
  for (;;) 
    {
      lp = str_blank (lp);
      if (*lp == '\0' OR NOT isalnum (*lp)) 
        {
	  break;
	}
      // Add the data
      Reset ();
      AddString (lp);
      ResetPtr ();
      //Parse the data into Transaction records
      code = ParseData(isLoad);
      break;
    }
    
  return code;
}

Code LegacyTransItem::ParseData( bool isLoad )
{
  clockIsFull = false;
  Code code = SC_OKAY;
  Trans * transaction = NULL;
  for (;;) 
    {
      transaction = new Trans();
      char separator;
      /**
       * Read the Time of the Transaction
       */
      code = _TimeSecondsGet(*this, transaction->TransTime);
      if (code != SC_OKAY){

	break;
      }
        
      /**
       * Read the source
       */
      code = FlagsBit8Get(transaction->Source, _FormatTransactionSource, 1);
      if (code != SC_OKAY){
	break;
      }
		
      if(transaction->Source == SysInputSourceDate){
	//Get the Date
	code = _DateGet(*this, transaction->TransDate);
	if (code != SC_OKAY){
	  break;
	}
            
	/**
	 * Read the Separator
	 */
	code = CharGet (separator, TransFieldSeparatorChars);
        if (code != SC_OKAY){
	  break;
	}
	        
	/**
	 * Read the Function
	 */
	code = CharGet (transaction->Function, "&");
        if (code != SC_OKAY){
	  break;
	}
	    
	goto ADDTOFIFO;
      }
        
      /**
       * Read the Badge
       */
      code = StringGet (transaction->Badge, sizeof transaction->Badge, SystemSetup.badge.length.valid);
      if (code != SC_OKAY){
	break;
      }
		
      /**
       * Read the Separator
       */
      code = CharGet (separator, TransFieldSeparatorChars);
      if (code != SC_OKAY){
	break;
      }
        
      /**
       * Read the Function
       */
      code = CharGet (transaction->Function, TransFunctionKeyChars);
      if (code != SC_OKAY){

	break;
      }
      //        if (code != SC_OKAY) break;
        
      /**
       * Check if it is a Supervisor Function
       */
      if( transaction->Function == '@' )
        {
#ifdef DEBUG
	  printf("Processing Supervisor Transaction\n");
#endif
	  /**
	   * Create a Supervisor Transaction
	   */
	  SuperTrans * suptrans = new SuperTrans();
            
	  /**
	   * Copy the read values to this object
	   */
	  suptrans->TransTime = transaction->TransTime;
	  suptrans->SuperSource = transaction->Source;
	  for( size_t i = 0; i <= SysFieldMaxBadgeLength; i++ )
            {
	      suptrans->SuperBadge[i] = transaction->Badge[i];    
            }
	  suptrans->SuperFunction = transaction->Function;
            
	  //Get the Date Source
	  code = FlagsBit8Get (suptrans->DateSource, _FormatTransactionSource, 1);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  //Get the Date
	  code = _DateGet(*this, suptrans->SuperDate);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  //Get the Time Source
	  code = FlagsBit8Get (suptrans->TimeSource, _FormatTransactionSource, 1);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  //Get the Time
	  SysTime t;
	  _TimeGet(*this,t);
	  if (code != SC_OKAY){
	    break;
	  }
	  suptrans->SuperTime = t * SecondsPerMinute;
            
	  /**
	   * The Employee info starts here
	   */
	  /**
	   * Read the Source 
	   */
	  code = FlagsBit8Get (suptrans->Source, _FormatTransactionSource, 1);
	  if (code != SC_OKAY){
	    break;
	  }
     		
	  /**
	   * Read the Badge
	   */
	  code = StringGet (suptrans->Badge, sizeof suptrans->Badge, SystemSetup.badge.length.valid);
	  if (code != SC_OKAY){
	    break;
	  }
	       	
	  /**
	   * Read the Separator
	   */
	  code = CharGet (separator, TransFieldSeparatorChars);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  /**
	   * Read the Function
	   */
	  code = CharGet (suptrans->Function, TransFunctionKeyChars);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  /**
	   * Cleanup
	   */
	  delete transaction;
	  transaction = suptrans;
	  suptrans = NULL;
        }
      /**
       * Read the Flags
       */
      code = _TransactionFlagsGet( *this, transaction );
      if (code != SC_OKAY){
	break;
      }
        
      if(transaction->Function == '+')
        {
	  //switcharoo: we are dealing with a bio template transaction. Therefore switch out our transaction here
	  BioTrans* b = new BioTrans ( * transaction );
	  delete transaction ;
	  transaction = b ;

	  char source_chr; //We don't use this anyway, just parse and throw it out. It appears in front of every field,after separator
	  /**
	   * Read the Separator
	   */
	  code = CharGet (separator, TransFieldSeparatorChars);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  //Get the source char
	  code = CharGet (source_chr);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  //template transa
	  code = CharGet (b ->TemplatePartNumber, TransTemplatePartNumberChars);
	  if (code != SC_OKAY){
	    break;
	  }
        	
	  /**
	   * Read the Separator
	   */
	  code = CharGet (separator, TransFieldSeparatorChars);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  //Get the source char
	  code = CharGet (source_chr);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  code = CharGet (b ->FingerIndex, TransFingerIndexChars);
	  if (code != SC_OKAY){
	    break;
	  }
	  /**
	   * Read the Separator
	   */
	  code = CharGet (separator, TransFieldSeparatorChars);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  //Get the source char
	  code = CharGet (source_chr);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  code = StringGet ( b ->FingerTemplateData, sizeof b ->FingerTemplateData, 512);
	  if (code != SC_OKAY){
	    break;
	  }
	  /**
	   * Read the Separator
	   */
	  code = CharGet (separator, TransFieldSeparatorChars);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  //Get the source char
	  code = CharGet (source_chr);
	  if (code != SC_OKAY){
	    break;
	  }
            
	  //Read the authority level
	  code = CharGet (b ->PrivLevel, TransPrivLevelChars);
	  if (code != SC_OKAY){
	    break;
	  }
      	}
      else
      	{
	  /**
	   * Read the Level data
	   */
	  code = _TransactionLevelsGet(*this, transaction);
	  if (code != SC_OKAY){
	    break;
	  }
	}
    ADDTOFIFO:
      if ( EBUSY != pthread_mutex_trylock(&pollingLock))
	{
	  /**
	   * Add The Transaction to the FIFO and the Transaction File
	   */
	 /* if( !isLoad )
	    {
	      code = WriteTransactionToFile(GetTransData());
	      if( code != SC_MEMORY ) 
		{
		  fifo->AddTransaction( transaction );
#ifdef DEBUG
		  transaction->DisplayTrans();
#endif
		}		
	    }
	  else
	    {
	      fifo->AddTransaction( transaction );
#ifdef DEBUG
	      transaction->DisplayTrans();
#endif
	    }*/
	  pthread_mutex_unlock( &pollingLock );
	}
      else // The clock is being polled
	{
#ifdef DEBUG
	  printf("Adding Transaction to Polling Transaction File\n");
#endif
	 FILE * fp = fopen(POLL_TRANSACTIONS_FILE, "a");
	  if(fp)
	    {
	     /* int ret = fprintf(fp, "%s\n", GetTransData());
	      if( ret <  0 )
		{
#ifdef DEBUG
		  perror("Failed to Write Transaction");
#endif
		  if( errno == ENOSPC || errno == ENOMEM ) // out of memory
		    {
		      code = SC_MEMORY;
		      clockIsFull = true;
		    }
		}
	      fclose(fp);*/
	    }
	  else if( errno == ENOSPC || errno == ENOMEM ) // out of memory
	    {
	      code = SC_MEMORY;
	      clockIsFull = true;
	    }
	  delete transaction ;
	  transaction = NULL ;
	}
        
      transaction = NULL;
      break;
    }
  transaction = NULL;
  return code;
}

/**
 * If the Transaction was successful, store this in
 * the transactions.clk file. Check if this function was called when 
 * loading the transactions from the file. If so, we do not need to 
 * add the transaction again into the file
 * 
 * We also need to check if the memory is full
 * Sometimes fprintf doesn't throw an error if there is no memory left.
 * To fix this we also check the file size before wite and after write 
 * to check if the transaction was actually written
 * 
 */
Code LegacyTransItem::WriteTransactionToFile( const char * transdata )
{
 /* struct stat file;
  off_t fileSizeBeforeWrite = 0;
  if( stat(TRANSACTIONS_FILE,&file) != -1 )
    {
      fileSizeBeforeWrite = file.st_size;
#ifdef DEBUG
      printf("The File Size before Write = %ld\n", fileSizeBeforeWrite );
#endif
    }
	
  FILE * fp = fopen(TRANSACTIONS_FILE, "a");
  if(fp)
    {
      int ret = fprintf(fp, "%s\n", transdata);
      if( ret <  0 )
	{
#ifdef DEBUG
	  perror("Failed to Write Transaction");
#endif
	  if( errno == ENOSPC || errno == ENOMEM ) // out of memory
	    {
	      fclose(fp);
	      clockIsFull = true;
	      return SC_MEMORY;
	    }
	}
      fclose(fp);
    }
  else if( errno == ENOSPC || errno == ENOMEM ) // out of memory
    {
#ifdef DEBUG
      perror("Failed to Open File to Write");
#endif
      clockIsFull = true;
      return SC_MEMORY;
    }
  else
    {
#ifdef DEBUG
      perror("Failed to Open File to Write");
#endif
    }
	
  off_t fileSizeAfterWrite = 0;
  if( stat(TRANSACTIONS_FILE,&file) != -1 )
    {
      fileSizeAfterWrite = file.st_size;
#ifdef DEBUG
      printf("The File Size after Write = %ld\n", fileSizeAfterWrite );
#endif
		
      if( fileSizeAfterWrite <= fileSizeBeforeWrite )
	{
	  clockIsFull = true;
	  return SC_MEMORY;
	}
    }*/
	
  return SC_OKAY;
}

void LegacyTransItem::GetPollingLock()
{
  pthread_mutex_lock( &pollingLock );
}

bool LegacyTransItem::TryPollingLock()
{
  return ( EBUSY != pthread_mutex_trylock( &pollingLock ) );
}

void LegacyTransItem::ReleasePollingLock()
{
  pthread_mutex_unlock( &pollingLock );
}

void LegacyTransItem::GetProgrammingLock()
{
  pthread_mutex_lock( &programmingLock );
}

bool LegacyTransItem::TryProgrammingLock()
{
  return ( EBUSY != pthread_mutex_trylock( &programmingLock ) );
}

void LegacyTransItem::ReleaseProgrammingLock()
{
  pthread_mutex_unlock( &programmingLock );
}

void LegacyTransItem::GetTemplateLock()
{
  pthread_mutex_lock( &templateLock );
}

bool LegacyTransItem::TryTemplateLock()
{
  return ( EBUSY != pthread_mutex_trylock( &templateLock ) );
}

void LegacyTransItem::ReleaseTemplateLock()
{
  pthread_mutex_unlock( &templateLock );
}

void LegacyTransItem::GetDBLock()
{
  pthread_mutex_lock( &DBLock );
}

bool LegacyTransItem::TryDBLock()
{
  return ( EBUSY != pthread_mutex_trylock( &DBLock ) );
}

void LegacyTransItem::ReleaseDBLock()
{
  pthread_mutex_unlock( &DBLock );
}

void LegacyTransItem::GetTransactionFileLock()
{
  pthread_mutex_lock( &transactionFileLock );
}

void LegacyTransItem::ReleaseTransactionFileLock()
{
  pthread_mutex_unlock( &transactionFileLock );
}

Code LegacyTransItem::Dump()
{
  Code code = SC_OKAY;
  return code;
}

/**
 * Puts all the Transaction data to the buffer
 * 
 */
Code LegacyTransItem::FormatData(Trans * transaction)
{
  Code code = SC_OKAY;
  Reset();
    
  //Put the transaction time into the buffer
  _TimeSecondsPut( *this, transaction->TransTime );
    
  try 
    {
      /**
       * Check if its a Supervisor Transaction
       */
      SuperTrans * suptrans = dynamic_cast<SuperTrans *>(transaction);
      if( suptrans != NULL )
        {
	  /**
	   * Put the badge source
	   */
	  FlagsBit8Put (suptrans->SuperSource, _FormatTransactionSource, 1);
	  /**
	   * Put the badge
	   */
	  StringPut (suptrans->SuperBadge);
	  /**
	   * Put the Field Separator
	   */
	  CharPut(TransFieldSeparatorChar);
	  /**
	   * Put the Function
	   */
	  CharPut(suptrans->SuperFunction);
	  /**
	   * Put the Date source
	   */
	  FlagsBit8Put (suptrans->DateSource, _FormatTransactionSource, 1);
	  /**
	   * Put the Date
	   */
	  _DatePut(*this, suptrans->SuperDate);
	  /**
	   * Put the Time Source
	   */
	  FlagsBit8Put (suptrans->TimeSource, _FormatTransactionSource, 1);
	  /**
	   * Put the Time 
	   * Note : Convert it to minutes as _TimePut converts to hh:mm
	   */
	  SysTime t = ( suptrans->SuperTime ) / SecondsPerMinute;
	  _TimePut(*this,t);
        }
    }
  catch (bad_cast) 
    {
#ifdef DEBUG
      printf("Caught: bad_cast exception. It's not a Supervisor Transaction.\n");
#endif
    }
    
  /**
   * Put the Employee Transaction Info
   * Put the badge source
   */
  FlagsBit8Put (transaction->Source, _FormatTransactionSource, 1);  
    
  /**
   * Put the badge
   */
  StringPut (transaction->Badge);
  /**
   * Put the Field Separator
   */
  CharPut(TransFieldSeparatorChar);
  /**
   * Put the Function
   */
  CharPut(transaction->Function);
  /**
   * Put the Flags
   */
  _TransactionFlagsPut(*this, transaction);
    
  /**
   * Put The Levels
   */
  _TransactionLevelsPut(*this, transaction); 
    
  return code;
}

/**
 * Puts all the Transaction data to the buffer
 * 
 */
Code LegacyTransItem::FormatDataForServer(Trans * transaction)
{
#ifdef _TA620_VER6_
  const char FieldSeparatorChar = ',';
#else
  const char FieldSeparatorChar = 0x1C;
#endif
  Code code = SC_OKAY;
  Reset();
    
  //Put the transaction time into the buffer
  _TimeSecondsPut( *this, transaction->TransTime );
    
#ifdef _TA620_VER6_
  /**
   * Put the Field Separator
   */
  CharPut(FieldSeparatorChar);
#endif
    
  try 
    {
      /**
       * Check if its a Supervisor Transaction
       */
      SuperTrans * suptrans = dynamic_cast<SuperTrans *>(transaction);
      if( suptrans != NULL )
        {
	  /**
	   * Put the badge source
	   */
	  FlagsBit8Put (suptrans->SuperSource, _FormatTransactionSource, 1);
            
#ifdef _TA620_VER6_
	  /**
	   * Put the Field Separator
	   */
	  CharPut(FieldSeparatorChar);
#endif
		    
	  /**
	   * Put the badge
	   */
	  StringPut (suptrans->SuperBadge);
		    
	  /**
	   * Put the Field Separator
	   */
	  CharPut(FieldSeparatorChar);
            
	  /**
	   * Put the Function
	   */
	  CharPut(suptrans->SuperFunction);
            
#ifdef _TA620_VER6_
	  /**
	   * Put the Field Separator
	   */
	  CharPut(FieldSeparatorChar);
#endif
		    
	  /**
	   * Put the Date source
	   */
	  FlagsBit8Put (suptrans->DateSource, _FormatTransactionSource, 1);
            
#ifdef _TA620_VER6_
	  /**
	   * Put the Field Separator
	   */
	  CharPut(FieldSeparatorChar);
#endif
		    
	  /**
	   * Put the Date
	   */
	  _DatePut(*this, suptrans->SuperDate);
            
#ifdef _TA620_VER6_
	  /**
	   * Put the Field Separator
	   */
	  CharPut(FieldSeparatorChar);
#endif
		    
	  /**
	   * Put the Time Source
	   */
	  FlagsBit8Put (suptrans->TimeSource, _FormatTransactionSource, 1);
            
#ifdef _TA620_VER6_
	  /**
	   * Put the Field Separator
	   */
	  CharPut(FieldSeparatorChar);
#endif
		    
	  /**
	   * Put the Time 
	   * Note : Convert it to minutes as _TimePut converts to hh:mm
	   */
	  SysTime t = ( suptrans->SuperTime ) / SecondsPerMinute;
	  _TimePut(*this,t);
            
#ifdef _TA620_VER6_
	  /**
	   * Put the Field Separator
	   */
	  CharPut(FieldSeparatorChar);
#endif
        }
    }
  catch (bad_cast) 
    {
#ifdef DEBUG
      printf("Caught: bad_cast exception. It's not a Supervisor Transaction.\n");
#endif
    }
    
  /**
   * Put the Employee Transaction Info
   * Put the badge source
   */
  FlagsBit8Put (transaction->Source, _FormatTransactionSource, 1);
    
#ifdef _TA620_VER6_    
  /**
   * Put the Field Separator
   */
  CharPut(FieldSeparatorChar);
#endif
    
  /**
   * Check if this a Date/Time Change Transaction
   */
  if( transaction->Source == SysInputSourceDate )
    {
      /**
       * Put the Date
       */
      _DatePut(*this, transaction->TransDate);	
        
      /**
       * Put the Field Separator
       */
      CharPut(FieldSeparatorChar);
		
      /**
       * Put the Function
       */
      CharPut(transaction->Function);
	    
      return code;
    }
    
  /**
   * Put the badge
   */
  StringPut (transaction->Badge);
  /**
   * Put the Field Separator
   */
  CharPut(FieldSeparatorChar);
  /**
   * Put the Function
   */
  CharPut(transaction->Function);
    
#ifdef _TA620_VER6_
  /**
   * Put the Field Separator
   */
  CharPut(FieldSeparatorChar);
#endif
    
    
  /**
   * Put the Flags
   */
  _TransactionFlagsPutForServer(*this, transaction);
    
  if(transaction->Function == '+')
    {   
      BioTrans* b = dynamic_cast < BioTrans* > ( transaction );
      if ( b ){
	//Put the templatepartnumber
	FlagsBit8Put (b ->Source, _FormatTransactionSource, 1);  //put the source - no matter what it is. Any char would do
	CharPut(b ->TemplatePartNumber);
	    
	//Put the FingerIndex
	CharPut(FieldSeparatorChar);
	FlagsBit8Put (b ->Source, _FormatTransactionSource, 1);
	CharPut(b ->FingerIndex);
	   
	//put template
	CharPut(FieldSeparatorChar);
	FlagsBit8Put (b ->Source, _FormatTransactionSource, 1);
	StringPut (b ->FingerTemplateData);
	    
	//put PrivLevel
	CharPut(FieldSeparatorChar);
	FlagsBit8Put ( b ->Source, _FormatTransactionSource, 1);
	CharPut( b ->PrivLevel);
      } else {
	printf ( "%s %d: KELLERALARM!\n" , __FILE__ , __LINE__ );
      }
    }
  else
    {
      /**
       * Put the Level data
       */
      _TransactionLevelsPutForServer(*this, transaction);
    }	    
  return code;
}

TransFIFO * LegacyTransItem::GetFIFO()
{
  return fifo;
}

/*TransFIFO * LegacyTransItem::GetTemplateFIFO()
  {
  return template_fifo;
  }
*/
const char * LegacyTransItem::GetTransData()
{
  //static char tdata[256];
  static char tdata[50];
  memcpy(tdata, data, size);
  tdata[size] = '\0';
  return tdata;
}


/**
 * Transactions are stored in transactions.clk file, to
 * recover when there is a power loss in the clock.
 * This function loads all the stored transactions from file
 * to the memory.
 */
void LegacyTransItem::LoadStoredTransactions()
{
  /*GetTransactionFileLock();
  FILE * fp = fopen(TRANSACTIONS_FILE, "r");
  if(fp)
    {
      LegacyTransItem * titem = LegacyTransItem::GetInstance();
      //static char buffer[256];
      static char buffer[640];
      char * transdata = fgets(buffer, sizeof buffer, fp);
      while(transdata != NULL)
	{
	  // Trim the data
	  transdata = TrimString(transdata);  
	  if(transdata != NULL)
	    {
	      titem->ProcessData(transdata, true);	
	    }
	  transdata = fgets(buffer, sizeof(buffer), fp);
	}
      fclose(fp);
    }
	
  ReleaseTransactionFileLock();*/
}

/**
 * Loads transactions, which took place during polling
 * When this function is called, the caller needs to absolutely make sure that
 * the pollingLock is relesased. Else we will enter into an infinite loop.
 */
void LegacyTransItem::LoadPolledTransactions()
{
  GetTransactionFileLock();
  FILE * fp = fopen(POLL_TRANSACTIONS_FILE, "r");
  if(fp)
    {
#ifdef DEBUG
      printf("Adding Transactions that occured during polling\n");
#endif
		
      LegacyTransItem * titem = LegacyTransItem::GetInstance();
      //static char buffer[256];
      static char buffer[640];
      char * transdata = fgets(buffer, sizeof buffer, fp);
      while(transdata != NULL)
	{
	  // Trim the data
	  transdata = TrimString(transdata);
	  if(transdata != NULL)
	    {
	      Code code = titem->ProcessData(transdata, false);
	      if( code == SC_MEMORY ) 
		{
		  ReleaseTransactionFileLock();
		  fclose(fp);
		  return;
		}
	    }
	  transdata = fgets(buffer, sizeof(buffer), fp);
	}
      fclose(fp);
      // Delete all Transactions from the File
      FILE * fp = fopen(POLL_TRANSACTIONS_FILE, "w");
      if(fp) fclose(fp);
		
      // Delete the File itself.  Just to be sure
      if( remove(POLL_TRANSACTIONS_FILE) != 0 )
	{
#ifdef DEBUG
	  perror("Failed to Remove Transactions File");
#endif
	}
		
    }
  ReleaseTransactionFileLock();
}

/**
 * Removes a transaction from the file
 */
bool LegacyTransItem::RemoveTransactionFromFile( Trans * trans )
{
  FILE * fp = fopen(TRANSACTIONS_FILE, "r");
  FILE * fp1 = fopen(TRANSACTIONS_TMP_FILE, "w");
  if(fp && fp1)
    {
      LegacyTransItem * titem = LegacyTransItem::GetInstance();
      titem->FormatData(trans);
      const char * transdata = titem->GetTransData();
      static char tdata[50];
      char * tptr = tdata;
      strcpy(tptr, transdata);
      tptr = TrimString(tptr);
		
#ifdef DEBUG
      printf("In RemoveTransactionFromFile() - TRANSACTION DATA = [%s]\n", transdata);
#endif
		
      //static char buffer[256];
      static char buffer[50];
      char * line = fgets(buffer, sizeof buffer, fp);
      while(line != NULL)
	{
	  // Trim the data
	  line = TrimString(line);
#ifdef DEBUG
	  printf("\n line = [%s]", line);
#endif
	  // Change the Source of the transaction to invalid
	  //if(strcmp(line, transdata) == 0)
	  if(strstr(line, tptr) != NULL)
	    {
	      trans->Source = SysInputSourceInvalid;
	      try
		{	
		  SuperTrans * strans = dynamic_cast<SuperTrans *>(trans);
		  if( strans != NULL )
		    {  
		      strans->SuperSource = SysInputSourceInvalid;
		    }
		}
	      catch(bad_cast)
		{
		}	
	      titem->FormatData(trans);
	      transdata = titem->GetTransData();
	      fputs(transdata, fp1);
	      fputs("\n", fp1);
	    }
	  else
	    {
	      fputs(line, fp1);
	      fputs("\n", fp1);
	    }
	  line = fgets(buffer, sizeof(buffer), fp);
	}
      fclose(fp1);
      fclose(fp);
      if( rename(TRANSACTIONS_TMP_FILE, TRANSACTIONS_FILE) != -1 )
	{
	  return true;
	}
    }
  return false;
}

