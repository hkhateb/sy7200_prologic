#include "Trans.hpp"
#include "TransUtil.hpp"

/**
 * Parses the Flags data
 */
Code _TransactionFlagsGet (InterfaceBuffer & buffer, Trans * trans)
{
    Code code = SC_OKAY;
    for( SysIndex i = 0 ; i < SysTransactionFlagsMax ; i++ )
    {
        char x;       
        code = buffer.CharGet (x);
        if (code != SC_OKAY) break;
        trans->FlagBytes[i] = (Bit8)x;
    }
    return code;
}

/**
 * Puts the Flags data into the buffer
 */
size_t _TransactionFlagsPut (InterfaceBuffer & buffer, const Trans * trans)
{
    size_t length = 0;
    for( SysIndex i = 0 ; i < SysTransactionFlagsMax ; i++ )
    {
         length += buffer.CharPut(trans->FlagBytes[i]);   
    }
    return length;
}

/**
 * The server expects the flags in Hexadecimal format
 */
size_t _TransactionFlagsPutForServer(InterfaceBuffer & buffer, const Trans * trans)
{
	#ifdef _TA620_VER6_
	static char buf[3];
	#endif
	size_t length = 0;
	for( SysIndex i = 0 ; i < SysTransactionFlagsMax ; i++ )
	{
	#ifdef _TA620_VER6_
		sprintf(buf, "%2x", trans->FlagBytes[i]);
		buf[2] = '\0';
		length += buffer.StringPut(buf);
		if(i != SysTransactionFlagsMax - 1) 
		{
			length += buffer.CharPut(',');   
		}
	#else
		length += buffer.CharPut(trans->FlagBytes[i]);
	#endif
	}
	return length;
}

/**
 * Parses the Levels data
 */
Code _TransactionLevelsGet (InterfaceBuffer & buffer, Trans * trans)
{
    Code code = SC_OKAY;
	char separator;
    for( SysIndex i = 0; i < SysFunctionKeyLevelsMax; i++ )
    {
		if(buffer.DataLeft())
    	{
			/**
			 * Read the Separator
			 */
			if( i == 0 )
			{
				code = buffer.CharGet (separator, TransFieldSeparatorChars);
				if (code != SC_OKAY) break;		
			}
			
			/**
			 * Read the source
			 */
			code = buffer.FlagsBit8Get (trans->Levels[i].Source, _FormatTransactionSource, 1);
			if (code != SC_OKAY) break;
			
			/**
			 * Read the Data
			 */
			code = buffer.StringGet(trans->Levels[i].Data, sizeof trans->Levels[i].Data, 0, TransFieldSeparatorChar);
		}    	
    }
    return code;
}

/**
 * Puts the Levels data into the buffer
 */
size_t _TransactionLevelsPut (InterfaceBuffer & buffer, Trans * trans)
{
    size_t length = 0;
    for( SysIndex i = 0; i < SysFunctionKeyLevelsMax; i++ )
    {
    	if(trans->Levels[i].Valid())
    	{
    		length += buffer.CharPut(TransFieldSeparatorChar);
			length += buffer.FlagsBit8Put (trans->Levels[i].Source, _FormatTransactionSource, 1);
			length += buffer.StringPut(trans->Levels[i].Data);
    	}
    }
    return length;
}

/**
 * Puts the Levels data into the buffer
 */
size_t _TransactionLevelsPutForServer (InterfaceBuffer & buffer, Trans * trans)
{
    size_t length = 0;
    for( SysIndex i = 0; i < SysFunctionKeyLevelsMax; i++ )
    {
    	if(trans->Levels[i].Valid())
    	{
    	#ifdef _TA620_VER6_
    		length += buffer.CharPut(',');
			length += buffer.FlagsBit8Put (trans->Levels[i].Source, _FormatTransactionSource, 1);
			length += buffer.CharPut(',');
			length += buffer.StringPut(trans->Levels[i].Data);
		#else
			if( i != 0 ) length += buffer.CharPut(0x1C);
			length += buffer.FlagsBit8Put (trans->Levels[i].Source, _FormatTransactionSource, 1);
			length += buffer.StringPut(trans->Levels[i].Data);
		#endif
    	}
    }
    return length;
}


