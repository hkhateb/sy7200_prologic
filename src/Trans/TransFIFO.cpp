#include <unistd.h>
#include "TransFIFO.hpp"
namespace {
  int cnt = 0 ;
  class Guard {
  public:
    Guard () { 
      if ( cnt != 0 ){
	printf ( "%s  %d really bad!!\n" , __FILE__ , __LINE__ );
      }
      cnt ++ ;
    }

    ~Guard (){
      if ( cnt != 1 ){
	printf ( "%s %d really bad!!\n" , __FILE__ , __LINE__ );
      }
      cnt -- ;
    }

  };
};

TransFIFO::TransFIFO()
{
	//fifo = new TAVector<Trans *>();
}

TransFIFO::~TransFIFO()
{
  Guard g ;
  clear();
}

void TransFIFO::AddTransaction(Trans * trans)
{
  Guard g ;
  fifo.push_back(trans);
}

void TransFIFO::clear()
{
  Guard g ;
  int size = fifo.size();
  for( int i=0; i<size; i++ )
    {
      delete *fifo[i];
    }
  fifo.clear();
}
 
bool TransFIFO::empty()
{
  Guard g ;
  return fifo.size() == 0;
}

Trans * TransFIFO::Get()
{
  Guard g ;
  if(fifo.size() <= 0) return NULL;
  Trans * trans = *(fifo[0]);
  fifo.erase(0);
  return trans;
}

Trans * TransFIFO::Get(int index)
{
  Guard g ;
  if ( fifo[index] == NULL ) return NULL;
  else return *fifo[index];
}

/**
 * Removes the Transaction from the FIFO and also deletes it.
 */
void TransFIFO::RemoveTransaction(Trans * trans)
{
  Guard g ;
  int size = fifo.size();
  for( int i=0; i<size; i++ )
    {
      if( *fifo[i] == trans)
	{
#ifdef DEBUG
	  printf("DELETING TRANSACTION\n");
	  (*fifo[i])->DisplayTrans();
#endif
	  delete *fifo[i];
	  fifo.erase(i);
	  return;
	}	
    }
}

TAVector<Trans *> * TransFIFO::GetAllTransForBadge(const char * badge)
{
  Guard g ;
  TAVector<Trans *> * vec = new TAVector<Trans *>();
  int size = fifo.size();
  for( int i=0; i<size; i++ )
    {
      Trans * trans = *fifo[i];
      // Skip Date / Time Change Transactions
      if(trans->Source == SysInputSourceDate || trans->Source == SysInputSourceInvalid) continue;
      if(strcmp(trans->Badge, badge) == 0 && !(trans->FlagBytes[4] & PROFILE_ACCESS_DENIED))
	{
	  vec->push_back(trans);
	}
    }
  return vec;
}

int TransFIFO::size()
{
  Guard g ;
  return fifo.size();
}
