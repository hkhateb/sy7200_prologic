#ifndef TRANSUTIL_HPP
#define TRANSUTIL_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "System.hpp"
#include "Interface.hpp"
#include "Trans.hpp"
#include "LegacyTrans.hpp"

Code _TransactionFlagsGet (InterfaceBuffer & buffer, Trans * trans);
size_t _TransactionFlagsPut (InterfaceBuffer & buffer, const Trans * trans);
size_t _TransactionFlagsPutForServer (InterfaceBuffer & buffer, const Trans * trans);

Code _TransactionLevelsGet (InterfaceBuffer & buffer, Trans * trans);
size_t _TransactionLevelsPut (InterfaceBuffer & buffer, Trans * trans);
size_t _TransactionLevelsPutForServer (InterfaceBuffer & buffer, Trans * trans);

#endif // #endif // TRANSUTIL_HPP
