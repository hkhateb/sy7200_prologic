#ifndef LEGACYTRANS_HPP
#define LEGACYTRANS_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include <pthread.h>
#include "System.hpp"
#include "Interface.hpp"
#include "TransFIFO.hpp"

static const InterfaceFormatFlagsBit8 _FormatTransactionSource [] = {
	InterfaceFormatFlagsBit8 ('1', SysInputSourceMagnetic),
	InterfaceFormatFlagsBit8 ('2', SysInputSourceKeypad),
	InterfaceFormatFlagsBit8 ('3', SysInputSourceBarcode),
	InterfaceFormatFlagsBit8 ('4', SysInputSourceDate),
	InterfaceFormatFlagsBit8 ('8', SysInputSourceProximity),
	InterfaceFormatFlagsBit8 ('0', SysInputSourceInvalid)
};
const char * const TransFunctionKeyChars = "0123456789*#?@+"; //added + for enroll template
const char * const TransTemplatePartNumberChars = "1234";
const char * const TransFingerIndexChars = "0123456789";
const char * const TransPrivLevelChars = "012";

const char * const TransFieldSeparatorChars = "|";
const char TransFieldSeparatorChar = '|';
const char TransSuperFunctionChar = '@';
const char DefaultSwipeAndGoFunctionChar = '?';

static const InterfaceFormatFlagsBit8 _BadgeSourceTemplate [] = {
	InterfaceFormatFlagsBit8 ('1', SysInputSourceMagnetic),
	InterfaceFormatFlagsBit8 ('2', SysInputSourceKeypad),
	InterfaceFormatFlagsBit8 ('3', SysInputSourceBarcode),
	InterfaceFormatFlagsBit8 ('8', SysInputSourceProximity)
};

class LegacyTransItem : public InterfaceBuffer
{
    private:
        TransFIFO * fifo;
        
        static LegacyTransItem * myinstance;
        // Used to accept transactions during polling
        static pthread_mutex_t pollingLock; 
        // Used to block transactions when the clock is programmed
        static pthread_mutex_t programmingLock; 
        
        // The Transaction File is modified by the main and the 
        // DateMonitor threads. This lock prevents the threads writing 
        // to the file at the same time
        static pthread_mutex_t transactionFileLock; 
        
        static pthread_mutex_t templateLock; 
        static pthread_mutex_t DBLock; 
        
        LegacyTransItem (void) : InterfaceBuffer (300) 
        {
            fifo = new TransFIFO();
        };
        
    public:
		~LegacyTransItem()
		{ 	
			delete fifo; 
			myinstance = NULL;
        };
        
		static LegacyTransItem * GetInstance();				
        Boolean		Valid (void) const {return 1; };
        Code        Process();
        Code		ProcessData (const char * line, bool isLoad);
        Code        ParseData(bool isLoad);
        Code        FormatData(Trans * trans);
        Code        FormatDataForServer(Trans * transaction);
        Code        Dump();
        TransFIFO * GetFIFO();
        const char * GetTransData();
        
        void GetPollingLock();
        bool TryPollingLock();
		void ReleasePollingLock();
		
		void GetProgrammingLock();
		bool TryProgrammingLock();
		void ReleaseProgrammingLock();
		
		void GetTemplateLock();
		bool TryTemplateLock();
		void ReleaseTemplateLock();
		
		void GetDBLock();
		bool TryDBLock();
		void ReleaseDBLock();
		
		void GetTransactionFileLock();
		void ReleaseTransactionFileLock();
		
		static bool clockIsFull;
		
		/**
		 * Loads stored transactions from file to memory
		 */
		void LoadStoredTransactions();
		
		/**
		 * Loads transactions, which took place during polling
		 * When this function is called, the caller needs to absolutely make sure that
		 * the pollingLock is relesased. Else we will enter into an infinite loop.
		 */
		void LoadPolledTransactions();
		
		/**
		 * Removes a transaction from the file
		 */
		bool RemoveTransactionFromFile( Trans * trans );
		
		Code WriteTransactionToFile( const char * transdata );
};    

#endif // #endif // LEGACYTRANS_HPP
