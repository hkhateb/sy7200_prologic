#ifndef TRANS_HPP
#define TRANS_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "System.hpp"
#include "Legacy.hpp"

using namespace std;

/**
 * Transaction Flag Constants
 */
const Bit8 DEFAULT_FLAG = 0x40;

/**
 * Access Flag
 */
const Bit8 ACCESS_TABLE_PRESENT = 0x01;
const Bit8 ACCESS_BADGE_IN_TABLE = 0x02;
const Bit8 ACCESS_REQUESTED = 0x04;
const Bit8 ACCESS_GIVEN = 0x08;
const Bit8 ACCESS_SUPERVISOR_OVERRIDE = 0x20;

/**
 * Profile Flag
 */
const Bit8 PROFILE_TABLE_PRESENT = 0x01;
const Bit8 PROFILE_TRANSACTION_COMPLETED = 0x02;
const Bit8 PROFILE_REQUESTED = 0x04;
const Bit8 PROFILE_TIME_COVERED = 0x08;
const Bit8 PROFILE_BADGE_IN_TABLE = 0x10;
const Bit8 PROFILE_SUPERVISOR_OVERRIDE = 0x20;

/**
 * Profile Number
 */
const Bit8 PROFILE_NOT_IN_PROFILE_ASSIGN_TABLE = 0x4F;
const Bit8 BADGE_NOT_IN_PROFILE_ASSIGN_TABLE = 0x50;

/**
 * Access Override
 */
const Bit8 INSIDE_ACCESS_WINDOW = 0x01;
const Bit8 DURING_ALWAYS_OPEN = 0x02;
const Bit8 DURING_ALWAYS_CLOSED = 0x04;
const Bit8 ACCESS_ONLY_CLOCK = 0x08;
const Bit8 CLOCK_ALLOWS_SUPERVISOR_OVERRIDE = 0x10;

/**
 * Report Access Denied Transactions
 */
const Bit8 RA_CMD_ON = 0x01;
const Bit8 PROFILE_ACCESS_DENIED = 0x02;

class Trans
{
public:
  /**
   * Transaction Time
   */
  SysTimeSecs     TransTime;
  SysInputSource  Source;
  char            Badge [SysFieldMaxBadgeLength+1];
  SysFunctionKey  Function;
  	    
  /**
   * Transaction Flags
   * flags[0] - Access Flag
   * flags[1] - Profile Flag
   * flags[2] - Profile Number
   * flags[3] - Access Override
   * flags[4] - Access Denied Transactions
   */
  Bit8 FlagBytes[SysTransactionFlagsMax];
		
  /**
   * Level Data
   */
  SysLevelData Levels[SysFunctionKeyLevelsMax];
		
  char PrivLevel;
				
  Trans();
  virtual ~Trans();
  char GetCharForIS(SysInputSource flag);
  virtual void DisplayTrans();
  const char * GetTransactionTime();
  const char * GetTransactionDate();
        
  // The Transaction Date
  SysDate TransDate;
};

class BioTrans : public Trans {
public:
  virtual ~BioTrans (){}
  BioTrans () { memset ( this , 0 , sizeof ( BioTrans ));}

  BioTrans ( const Trans& r ) : Trans ( r ){}

public:
  char FingerTemplateData[513];
  char FingerIndex;
  char TemplatePartNumber;

};

class SuperTrans : public Trans
{
public:
  SysInputSource  SuperSource;
  char            SuperBadge [SysFieldMaxBadgeLength+1];
  SysFunctionKey  SuperFunction;
  SysInputSource  DateSource;
  SysDate         SuperDate;
  SysInputSource  TimeSource;
  SysTimeSecs     SuperTime; 
 	    
  SuperTrans(){}
  virtual ~SuperTrans(){};
  virtual void DisplayTrans();
  const char * GetSuperTime();
  const char * GetSuperDate();
};

#endif // #endif // TRANS_HPP

