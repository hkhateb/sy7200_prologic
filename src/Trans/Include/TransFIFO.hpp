#ifndef TRANSFIFO_HPP
#define TRANSFIFO_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "Trans.hpp"
#include "TAVector.h"

class TransFIFO
{
    public:
        TransFIFO();
        virtual ~TransFIFO();
        
		/**
         * Adds a Transaction to the FIFO
         */
        virtual void AddTransaction(Trans *);
        virtual void clear();
        virtual bool empty();
        virtual Trans * Get();
        virtual Trans * Get(int index);
        
        /**
		 * Removes the Transaction from the FIFO and also deletes it.
		 */
        virtual void RemoveTransaction(Trans *);
        
        /**
         * Returns the Size of the FIFO
         */
        virtual int size();
        virtual TAVector<Trans *> * GetAllTransForBadge(const char *);
        
    private:
        TAVector<Trans *> fifo;
};


#endif // TRANSFIFO_HPP
