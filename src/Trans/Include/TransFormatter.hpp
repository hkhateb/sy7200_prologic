#ifndef TRANSFORMATTER_HPP
#define TRANSFORMATTER_HPP

#if ! defined __cplusplus
#error Not compiling a C++ source file.
#endif

#include "Trans.hpp"

class TransFormatter
{
    virtual void FormatTransactionRecord(Trans * trans)=0;
};


#endif // TRANSFORMATTER_HPP
