#include <execinfo.h>
#include <stdio.h>
#include <time.h>
#include "Trans.hpp"

  const unsigned int psize = 30 ;
  void* p [ psize ];

  void issue_backtrace (){
    printf ( "@ %d [" , ( int ) time ( 0 ));
    int n = backtrace ( p , psize );
    if ( n > 0 ){
      for ( int i = n - 1 ; i != 0 ; i -- ){
	printf ( "%p." , p [ i ] );
      }
    }
    printf ( "] ");
  }

Trans::Trans()
{
	time_t optime;
	struct tm * optimeinfo;
	time ( &optime );
	optimeinfo = localtime ( &optime );
	TransDate.month = optimeinfo->tm_mon + 1;
	TransDate.date = optimeinfo->tm_mday;
	TransDate.year = optimeinfo->tm_year + 1900;
	//	issue_backtrace ();
	//	printf ( " + %p %x\n" , this , 100 );
}

Trans ::~Trans (){
  //	issue_backtrace ();
  //	printf ( " - %p\n" , this );
}

char Trans::GetCharForIS(SysInputSource flag)
{
	char source = 'S';
    switch(flag)
    {
        case SysInputSourceMagnetic:
        {
            source = 'S';
            break;
        }    
        case SysInputSourceKeypad:
        {
            source = 'K';
            break;
        }  
        case SysInputSourceBarcode:
        {
            source = 'W';
            break;
        }  
        case SysInputSourceDate:
        {
        	source = '4';
        	break;
        }
        case SysInputSourceInvalid:
        {
        	source = '0';
        	break;
        }
    }    
    return source;
}

void Trans::DisplayTrans()
{
	/**
	 * Check if it's a Date / Time Change Transaction
	 */
	if( Source == SysInputSourceDate )
	{
		printf("XXXXXXX Date / Time Change Transaction XXXXXXX\n");
		printf("Time = %s\n", GetTransactionTime());
		printf("Source = %c\n", GetCharForIS(Source));
		printf("Date = %s\n", GetTransactionDate());
		printf("Function = %c\n", Function);
		printf("XXXXX END XXXXX\n");
		return;
	}
	
	printf("XXXXXXX Employee Transaction Record XXXXXXX\n");
    printf("Employee Badge %s\n", Badge);
    printf("Employee Function %c\n", Function);
    printf("Employee Source %c\n", GetCharForIS(Source));
    
   for( SysIndex i = 0 ; i < 5 ; i++ )
	{
		printf("Flag %d %c\n", (int)i, FlagBytes[i]);
    }
	for( SysIndex i = 0 ; i < SysFunctionKeyLevelsMax ; i++ )
	{
		if(Levels[i].Valid())
		{
			printf("Level Data %d - %s\n", (int)i, Levels[i].Data);
		}
	}
	printf("XXXXX END XXXXX\n");
}

const char * Trans::GetTransactionTime()
{
	static char time[8];
	int minutes, hours, seconds;
	hours = ( TransTime / SecondsPerMinute ) / MinutesPerHour ;
	hours = hours % HoursPerDay;
	minutes = ( TransTime / SecondsPerMinute ) % MinutesPerHour;
	seconds = TransTime % SecondsPerMinute;
	sprintf(time, "%0*d", 2, hours);
	sprintf(time + 2, "%0*d", 2, minutes);
	sprintf(time + 4, "%0*d", 2, seconds);
	time[6] = '\0';
	return time;
}

const char * Trans::GetTransactionDate()
{
	static char date[8];
	sprintf(date, "%0*d", 2, TransDate.month);
	sprintf(date + 2, "%0*d", 2, TransDate.date);
	int year = TransDate.year - 1900;
	if( year >= 100 ) year -= 100;
	sprintf(date + 4, "%0*d", 2, year);
	date[6] = '\0';
	return date;
}

const char * SuperTrans::GetSuperTime()
{
	static char time[6];
	int minutes, hours;
	hours = ( SuperTime / SecondsPerMinute ) / MinutesPerHour ;
	hours = hours % HoursPerDay;
	minutes = ( SuperTime / SecondsPerMinute ) % MinutesPerHour;
	sprintf(time, "%0*d", 2, hours);
	sprintf(time + 2, "%0*d", 2, minutes);
	time[4] = '\0';
	return time;
}

const char * SuperTrans::GetSuperDate()
{
	static char date[8];
	sprintf(date, "%0*d", 2, SuperDate.month);
	sprintf(date + 2, "%0*d", 2, SuperDate.date);
	int year = SuperDate.year - 1900;
	if( year >= 100 ) year -= 100;
	sprintf(date + 4, "%0*d", 2, year);
	date[6] = '\0';
	return date;
}

void SuperTrans::DisplayTrans()
{
    printf("XXXXXXX Supervisor Transaction Record XXXXXXX\n");
    printf("Supervisor Badge %s\n", SuperBadge);
    printf("Supervisor Source %c\n", GetCharForIS(SuperSource));
    printf("Supervisor Date %d/%d/%d\n", SuperDate.month, SuperDate.date, SuperDate.year);
    
    printf("Employee Badge %s\n", Badge);
    printf("Employee Function %c\n", Function);
    printf("Employee Source %c", GetCharForIS(Source));
    
	for( SysIndex i = 0 ; i < 5 ; i++ )
	{
		printf("Flag %d %c\n", (int)i, FlagBytes[i]);
    }
	for( SysIndex i = 0 ; i < SysFunctionKeyLevelsMax ; i++ )
	{
		if(Levels[i].Valid())
		{
			printf("Level Data %d - %s\n", (int)i, Levels[i].Data);
		}
	}
	printf("XXXXX END XXXXX\n");
}

