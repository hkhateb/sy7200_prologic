#!/bin/sh
# Define application directory and name for startup script
APPL_DIR=/home/terminal/bin
APPL_NAME=SY7000Demo
APPL_ARGS=
/home/terminal/bin/run_webServer.sh
#
# kill processes not being used to free up some memory
# kill the /watchdog/WatchDog, if running
killall -9 WatchDog
killall -9 enabler
#
# not used ./mnt/flash/terminal/bin/WatchDog &
#
# optional. if not using the Web Configuration interface:
# remove or comment out the above ".../run_webServer.sh" line
# kill the web server if it is running
# killall -9 webs
#
TELNET=Yes

