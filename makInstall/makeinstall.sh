#!/bin/sh
#
# create the install packages and script files
# no versional control
#

# requirements:
#		makInstall folder is located under the SY7000-SDK folder
#		this makeinstall script is located in the makInstall folder
#		DEMO-WebServer folder is located under the SY7000-SDK folder
#		SY7000-SDK folder is svn root for the project
#



#		svn as source of files to be packaged.
# 	svn repository must be up to date.

#	get to SY7000-SDK
cd ..

#svn export DEMO-WebServer/ws031202/web makInstall/web
if [ ! -d makInstall/web ]; then
    mkdir makInstall/web
fi
cp -pP -r DEMO-WebServer/ws031202/web/* makInstall/web

if [ ! -d makInstall/LINUX ]; then
	mkdir makInstall/LINUX
fi

#echo pwd is $(pwd)

#svn export DEMO-WebServer/ws031202/LINUX/webs makInstall/LINUX/webs
cp DEMO-WebServer/ws031202/LINUX/webs makInstall/LINUX/webs

#svn export SY7000Demo makInstall/SY7000Demo
cp SY7000Demo makInstall/SY7000Demo

cd makInstall

#svn update ResetApplication
#svn update applstart

#svn update properties.xml
# use existing properties.xml from one level up
cp ../properties.xml .
#
# use existing install script
#svn update timeamerica.sh

rm *.tar

tar --exclude="*.pdf" --remove-files -z -c -f demoweb.tar web
#tar --exclude="*.pdf" -z -c -f demoweb.tar web

#tar --remove-files -z -c -f demowebdocs.tar web


#tar --remove-files -zcf demowebsapp.tar LINUX/webs
tar -zcf demowebsapp.tar LINUX/webs

tar --remove-files -zcf SY7000Demo.tar SY7000Demo
#tar -zcf SY7000Demo.tar SY7000Demo

rm -r web
rm -r LINUX
rm SY7000Demo
