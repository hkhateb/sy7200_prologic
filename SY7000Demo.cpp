//* Include files for Testlegacy. */

#ifndef client_prologic
#error "client_prologic is not defined"

#endif

#ifndef CLIENT_PROLOGIC
#error "CLIENT_PROLOGIC is not defined"

#endif


#ifdef USE_MCHECK
#include <mcheck.h>
#endif

#include "SY7000Demo.h" ////// with SUPPORT_SSL
#include "stringutil.h"

#ifdef USE_MEMCHECK
#include <memcheck.h>
#endif

extern uf_system_config_t g_sys_config;

static char FILE_HTTP_FP[32]	= "/home/terminal/bin/httpfp.tmp";
static char FILE_HTTP_PIN[32]	= "/home/terminal/bin/httppin.tmp";

#define UF_SYSTEM_STATUS UF_SYSTEM_STAUTS



// scheduling priorities
#define SY_PRIO_LOW		10
#define SY_PRIO_MEDIUM	40
#define SY_PRIO_HIGH	90

//XML parsing finger print template xml
struct XML_UDFP{
	char Elm[64];
	char Badge[32];
	//char Templ[520];
	char Templ[528];
	//FPM * fp;
	Base64 * dec;
};


void XML_ParseFP(FILE *);
static void XMLCALL XML_StartFP(void *, const char *, const char **);
static void XMLCALL XML_EndFP(void *, const char *);
static void XMLCALL XML_CharFP(void *, const char *, int);

#ifdef client_prologic

#ifdef USE_SELECTIVE_FPV
bool SY7000Demo::useKeypadFPV	= true;
bool SY7000Demo::useCardFPV		= true;
#endif

#endif


#ifdef USE_PIN_SUPPORT	// -----------------------------------------------------------------------------------

//
// PIN TABLE xml:
//
// <EmployeeInfo>
//		<BadgeId>string<BadgeId>
//		<VerifySource>string</VerifySource>
//		<PinCode>string</PinCode>
//
struct XML_UDPIN {
	char Elm[64];
	char BadgeId[32];
	char VerifySource[32];
	char PinCode[32];
};

void XML_ParsePIN(FILE *);
static void XMLCALL XML_StartPIN(void *, const char *, const char **);
static void XMLCALL XML_EndPIN(void *, const char *);
static void XMLCALL XML_CharPIN(void *, const char *, int);
static int UpdatePinTableInDBFromFile( char * BadgeId, char * VerifySource, char * PinCode);

#endif 		// -----------------------------------------------------------------------------------------------

struct XML_ACTIVITY {
	char Elm[64];
	char Name[100];
	char Code[20];
	int  ReqCode;
	char * FoundNameAct;
};

void XML_FindActivity(char * fileName,int actCode,char * actName);
static void XMLCALL XML_StartActivities(void *, const char *, const char **);
static void XMLCALL XML_EndActivities(void *, const char *);
static void XMLCALL XML_CharActivities(void *, const char *, int);

Logger * logger;


#include <assert.h>
#include <execinfo.h>
char SY7000Demo::LinkWeb[100];
char SY7000Demo::Account[30];
char SY7000Demo::ClockId[30];
int SY7000Demo::heartbeatFrequency;

bool SY7000Demo::ok_for_this_thread;
bool SY7000Demo::ok_for_next_thread;

int SY7000Demo::getActivitiesFrequency;
int SY7000Demo::TemplatesAvailableFrequency;

bool SY7000Demo::DontSendPunches;

HttpClient* SY7000Demo::httpS;
HttpClient* SY7000Demo::httpOffline;


#ifdef SUPPORT_SSL


#warning ""
#warning "SUPPORT_SSL is defined"
#warning ""

#endif
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// FP Verfication. Check Local.
// Check server if local fails
#define support_server_verification
// #undef support_server_verification

// 19f release:
#define ALLOW_UNVERIFIED_BADGES_OFFLINE

// 21m release:  (in makefile ?)
//#define USE_PIN_SUPPORT

///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

#ifdef DEBUGSEGVMT

void * mt_sigsegv_handler(void *);
int handled_signal = -1;

// operationiodriver.cpp / operationdriver.hpp:
// pthread_mutex_t mt_sig_mutex = PTHREAD_MUTEX_INITIALIZED;

void * mt_sigsegv_handler( void * arg)
{
	sigset_t signal_set;
	int sig;

	printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());

	for(;;) {
	        /* wait for any and all signals */
	        sigfillset( &signal_set );
	        sigwait( &signal_set, &sig );

	        /* when we get this far, we've
	         * caught a signal */

	        switch( sig )
	        {

			/* whatever you need to do on
	         * SIGSEGV */
	        case SIGSEGV:
	          pthread_mutex_lock(&OperationIODriver::mt_sig_mutex);
	          handled_signal = SIGSEGV;
	          pthread_mutex_unlock(&OperationIODriver::mt_sig_mutex);
	          break;

	        /* whatever you need to do on
	         * SIGQUIT */
	        case SIGQUIT:
	          pthread_mutex_lock(&OperationIODriver::mt_sig_mutex);
	          handled_signal = SIGQUIT;
	          pthread_mutex_unlock(&OperationIODriver::mt_sig_mutex);
	          break;

	        /* whatever you need to do on
	         * SIGINT */
	         case SIGINT:
	          pthread_mutex_lock(&OperationIODriver::mt_sig_mutex);
	          handled_signal = SIGINT;
	          pthread_mutex_unlock(&OperationIODriver::mt_sig_mutex);
	          break;

	        /* whatever you need to do for
	         * other signals */
	        default:
	          pthread_mutex_lock(&OperationIODriver::mt_sig_mutex);
	          handled_signal = 0;
	          pthread_mutex_unlock(&OperationIODriver::mt_sig_mutex);
	          break;
	        }
	{
		void* array[16];
    	size_t size;

    	// get void*'s for all entries on the stack
    	size = backtrace(array, 32);

    	// print out all the frames to stderr
    	//fprintf(stderr, "Error: signal %d:\n", sig);
		write(2, "Error: dump stack\n", 18);
    	backtrace_symbols_fd(array, size, 2);
    	exit(86);
	}

	}
	return (void*)0;
}

void mt_signal_watcher()
{
    //for (;;)
	{

		/* grab the mutex before looking
         * at handled_signal */
        pthread_mutex_lock( &OperationIODriver::mt_sig_mutex );

        /* look to see if any signals have
         * been caught */
        switch ( handled_signal )
        {
        case -1:
          /* no signal has been caught
           * by the signal handler */
          break;

        case 0:
        printf("The signal handler caught"
        " a signal I'm not interested in "
        "(%d)\n",
         handled_signal );
         handled_signal = -1;
         break;

        case SIGQUIT:
        printf("The signal handler caught"
        " a SIGQUIT signal!\n" );
         handled_signal = -1;
         break;

        case SIGINT:
        printf(
        "The signal handler caught"
        " a SIGINT signal!\n" );
         handled_signal = -1;
         break;
        }
        /* remember to release mutex */
        pthread_mutex_unlock(&OperationIODriver::mt_sig_mutex);
    }
}

#endif


///////////////////////////////////////////////////////
///////////////////////////////////////////////////////

#ifdef DEBUGSEGV

void sigsegv_handler( int sig )
    {
    void* array[16];
    size_t size;

#ifdef USE_MTRACE
	muntrace();
#endif

    // get void*'s for all entries on the stack
    size = backtrace(array, 16);

    // print out all the frames to stderr
    //fprintf(stderr, "Error: signal %d:\n", sig);
	write(2, "Error: signal 11\n", 17);
    backtrace_symbols_fd(array, size, 2);
    exit(1);
    }

void show_stackframe()
    {
    void* trace[16];
    char ** messages = (char ** )NULL;
    int i, trace_size = 0;

    trace_size = backtrace(trace, 16);
    messages = backtrace_symbols(trace, trace_size);
    printf("[bt] Execution path:\n");

    for ( i = 0; i < trace_size; ++i )
        printf("[bt] %s\n", messages[i]);
    }


#endif

#ifdef USE_MCHECK
void syAbortFunction(enum mcheck_status ems);

void syAbortFunction(enum mcheck_status ems)
{
	int lfd;
	lfd = open("/home/terminal/bin/mcheck.log", O_CREAT | O_APPEND ) ;
	write(lfd, "**** MCHECK has detected an inconsistency ****\n", 47 );
    close(lfd);

	//printf("abort function receives mcheckstatus of %d\n", ems);

}


#endif


///////////////////////////////////////////////////////
///////////////////////////////////////////////////////



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

SY7000Demo * SY7000Demo::staticPointer;
int main( int argc, char *argv [] )
{

    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());

#ifdef USE_MCHECK

		void (*ptrSyAbortFunction)(enum mcheck_status);

		ptrSyAbortFunction = &syAbortFunction;

		int mcr = 0;
		//mcr = mcheck(NULL);
		mcr = mcheck(ptrSyAbortFunction);
		if ( mcr == 0 )
		{
			printf("mcheck initiailized ok\n");
		}
		else if ( mcr == -1 )
		{
			printf("mcheck failed. too late, malloc already performed\n");
		}
		else
		{
			printf("mcheck failed for some other reason\n");
		}
#endif
		int tcount = 0;

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#ifdef DEBUGSEGVMT

	sigset_t signal_set;
	pthread_t sig_thread;
	//
    pthread_attr_t mt_sigsegv_ThreadAttr;
    unsigned int s;
    pthread_attr_init(&mt_sigsegv_ThreadAttr);
    pthread_attr_getstacksize(&mt_sigsegv_ThreadAttr, &s);
    //pthread_attr_setstacksize ( & mt_sigsegv_ThreadAttr , const_thread_stack_size ());
    pthread_attr_setstacksize(&mt_sigsegv_ThreadAttr, PTHREAD_STACK_MIN);

	{
		int tpolicy;
		struct sched_param param;

		tpolicy = SCHED_FIFO;
		param.sched_priority = SY_PRIO_MEDIUM;

		pthread_attr_setinheritsched(&mt_sigsegv_ThreadAttr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&mt_sigsegv_ThreadAttr, tpolicy);
		pthread_attr_setschedparam(&mt_sigsegv_ThreadAttr, &param);

	}


	//block all signals
	sigfillset( NULL );
	// create the signal handling thread
	printf("Create mt_sigsegv_handler\n");
	if ( 0 != pthread_create( &sig_thread, &mt_sigsegv_ThreadAttr, mt_sigsegv_handler, NULL ) )
	{
		printf("main: could not create mt_sigsegv_handler\n");
	}

#endif


#ifdef DEBUGSEGV
    signal(SIGSEGV, sigsegv_handler); // install our handler
	printf("SIGSEGV handler installed\n");
#ifdef USE_MTRACE
	mtrace();
	printf("MTRACE enabled\n");
#endif
#endif

#ifdef IGNORE_SIGPIPE
	sigignore(SIGPIPE);
#endif


	logger = Logger::getLogger();
	logger->setAppLogLevel(FileAppender::LEVEL_DEBUG);


//	DLOGE("Have a nice punch 1");
//	DLOGE("Have a nice punch 2");
//	DLOGE("Have a nice punch 3");
//	DLOGE("Have a nice punch 4");



    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    //OperationIODriver * oper = new OperationIODriver(); // init the  I/O operation system object

    SY7000Demo demo; //create the demo object , this object contain all the methods we need for the demo application
    SY7000Demo::staticPointer = &demo;
    demo.initDB();   // init the local data base tables
    //	printf("create IOD.\n");
    OperationIODriver* oper = new OperationIODriver(); // init the  I/O operation system object
    OperationStatusCode code;
    demo.initSetupSystemDataStructure();               //init the data structure for the system setup configuration
	printf("%s %d\n",__FUNCTION__,__LINE__);
    demo.SetupSystem();                                // init the setup configuration values
	printf("%s %d\n",__FUNCTION__,__LINE__);
    demo.initHttpClient();                             // init the http client which we use to send transaction
	printf("%s %d\n",__FUNCTION__,__LINE__);
	demo.SYClock_GetProps();
	printf("%s %d\n",__FUNCTION__,__LINE__);
    demo.initCfgIODriver(oper);
	printf("%s %d\n",__FUNCTION__,__LINE__);
    demo.initFpuConfiguration();                       // init fpu config from xml file
	printf("%s %d\n",__FUNCTION__,__LINE__);

	demo.initSwipeAndGo();
	printf("%s %d\n",__FUNCTION__,__LINE__);

#ifdef USE_MCHECK
DP("mcheck_check_all after init structures\n");
mcheck_check_all();
#endif

    demo.initOfflineMode(oper);

#ifdef USE_MCHECK
DP("mcheck_check_all after init offline mode\n");
mcheck_check_all();
#endif

    // try setting this now...
    demo.cfgMgr.SetTimeSyncConditionFlag(&OperationIODriver::getservertime_cond_flag);
	if ( OperationIODriver::AllowPopulateTemplates )
	{
    	demo.cfgMgr.SetPopulateTemplatesConditionFlag(&OperationIODriver::populatetemplatesthread_cond_flag);
    	demo.cfgMgr.SetWaitForPopulateTemplatesThreadConditionFlag(&OperationIODriver::waitforpopulatetemplatesthread_cond_flag);
	}

	// use pt mutex or soap_request_mutex?
    //demo.cfgMgr.SetPopulateTemplatesMutex(&OperationIODriver::populatetemplates_mutex);
    demo.cfgMgr.SetPopulateTemplatesMutex(&OperationIODriver::soap_request_mutex );
    demo.cfgMgr.SetWaitForPopulateTemplatesMutex(&OperationIODriver::waitforpopulatetemplates_mutex );

#ifdef USE_MCHECK
DP("mcheck_check_all after setting up cond_flags and mutexes\n");
mcheck_check_all();
#endif



//create finger print thread which activate the identification/verification mode when the user place finger in idle display.
    if (FPOperation::EnableFpUnit == true)
        {
        // done in initfpu: demo.EnableFpVerifyMode == true;
        demo.RunFingerPrintThread(oper);
        }
    else
        {
        demo.EnableFpVerifyMode = false;
        }

#ifdef USE_MCHECK
DP("mcheck_check_all starting fingerprint thread\n");
mcheck_check_all();
#endif

	 //OperationIODriver::communication = true;
    demo.RunHeartBeatThread(); //creat thread for heart beat web service
    DP("Heartbeat thread started?\n");

#ifdef USE_MCHECK
DP("mcheck_check_all after starting heartbeat thread\n");
mcheck_check_all();
#endif


    // wait until Heartbeat is active
    //oper->WaitForStartup(); // no need to wait for heartBeat

#ifdef USE_MCHECK
DP("mcheck_check_all before IdleDisplayProcess");
mcheck_check_all();
#endif

   
	/*SY7000Demo::ok_for_next_thread = false;


#ifdef USE_GETACT_SINGLETHREAD_RUN
	demo.RunGetActivitiesThread();		// run and die
#else
	demo.RunGetActivitiesThread();		// run and sleep until wakeup
#endif

	tcount = 0;
	pthread_mutex_lock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.
	while ( SY7000Demo::ok_for_next_thread == false )
	{
		
		int col = (20 - strlen("Get Activities...")) / 2 + 1;
		user_output_clear();
        user_output_write(2, col, "Get Activities...");

		tcount++;
		sleep(1);
		DLOGI("waiting for GetActivities thread...");
		if ( tcount > 30 ) // 30 seconds
		{
			DLOGI("Timeout waiting for GetActivities Thread");
			SY7000Demo::ok_for_next_thread = true;
		}
	}
	DLOGI("Done waiting for GetActivities thread...");
	pthread_mutex_unlock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.*/

	SY7000Demo::ok_for_next_thread = false;

#ifdef USE_PIN_SUPPORT
	// just do it.
	demo.DoGetPinTableFunction();

	tcount = 0;
	pthread_mutex_lock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.
	while ( SY7000Demo::ok_for_next_thread == false )
	{
		tcount++;
		int col = (20 - strlen("Get Pins Table...")) / 2 + 1;
		user_output_clear();
        user_output_write(2, col, "Get Pins Table...");
		sleep(1);
		printf("waiting for GetPinTable thread...\n");
		if ( tcount > 120 ) // 120 second wait for 4000 employee database to be downloaded and processed ???
		{
			printf("Timeout waiting for GetPinTable Thread\n");
			SY7000Demo::ok_for_next_thread = true;
		}
	}
	printf("Done waiting for GetPinTable thread...\n");
	pthread_mutex_unlock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.

#endif

    /*if (FPOperation::EnableFpUnit == true)
    {

	  if ( OperationIODriver::AllowPopulateTemplates )
	  {
		SY7000Demo::ok_for_next_thread = false;

		DP("AllowPopulateTemplates is true.\n");
		demo.RunPopulateTemplatesThread();	// this just starts and waits for signal before first cycle

		tcount = 0;
		pthread_mutex_lock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.
		while ( SY7000Demo::ok_for_next_thread == false )
		{
			tcount++;
			int col = (20 - strlen("Populate Templates..")) / 2 + 1;
			user_output_clear();
			user_output_write(2, col, "Populate Templates..");
			pthread_mutex_lock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.
			sleep(1);
			printf("waiting for PopulateTemplates thread...\n");
			if ( tcount > 30 ) // 30 seconds
			{
				printf("Timeout waiting for Thread\n");
				SY7000Demo::ok_for_next_thread = true;
			}
		}
		printf("Done waiting for OfflineSender thread...\n");
		pthread_mutex_unlock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.

	  }
	  else
	  {
		DP("AllowPopulateTemplates is false.\n");
	  }


        //demo.RunTemplatesAvailableThread(); // not now
    }*/



    // start the offline thread after the display thread, so the Home screen is displayed.
    //
    if (OperationIODriver::OfflineModeIsEnabled)
    {
		//SY7000Demo::ok_for_next_thread = false;

#ifdef USE_MCHECK
DP("mcheck_check_all before OfflineSender thread\n");
mcheck_check_all();
#endif

        /*//demo.RunOfflineSenderThread();

		tcount = 0;
		pthread_mutex_lock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.
		while ( SY7000Demo::ok_for_next_thread == false )
		{
			tcount++;
			int col = (20 - strlen("Send Offline..")) / 2 + 1;
			user_output_clear();
			user_output_write(2, col, "Send Offline..");
			sleep(1);
			printf("waiting for OfflineSender thread...\n");
			if ( tcount > 30 ) // 30 seconds
			{
				printf("Timeout waiting for OfflineSender Thread\n");
				SY7000Demo::ok_for_next_thread = true;
			}
		}
		printf("Done waiting for OfflineSender thread...\n");
		pthread_mutex_unlock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.*/


#ifdef USE_MCHECK
DP("mcheck_check_all after waitinng for OfflineSender thread\n");
mcheck_check_all();
#endif



    }

 oper->RunIdleDisplayProcess(); // creat the thread of the idle display .

#ifdef USE_MCHECK
DP("mcheck_check_all after IdleDisplayProcess\n");
mcheck_check_all();
#endif


    //ServerDataBaseConnection* SDBConnection = NULL;

	user_audible_init();    // init the LEDs controller
    user_visual_init();     // init the audio controller buzzer
    user_audible_volume(0); // set the buzzer volume to level 100.

    // move this above: oper->RunIdleDisplayProcess(); // creat the thread of the idle display .

    Operation* MyOperation = oper->GetOperation();

    bool swipeWithActivityCode = false;

    while (true)
    {
        swipeWithActivityCode = false;

        //init the finger print mode to identification mode in case finger template scann was accepted
        if (FPOperation::EnableFpUnit == true)
        {
            fpOpr.fpOperation = Ignore_Operation;                // verify mode
            pthread_cond_signal(&FPOperation::fprint_cond_flag); //wake up the finger print thread
        }
        //printf("call readbadge with turnonswipandgo set to %d", TurnOnSwipAndGo);
        code = oper->ReadBadge(); //wait until badge get from legal source .
        //if the badge is the configuration menue number
        //if(code ==SC_Success && strcmp("00000000000000",oper->getBadgeBuffer()) == 0)
        //DP("code is success, %d\n", code == SC_Success);
        //DP("badgebuffer is %s\n", oper->getBadgeBuffer());
		DLOGI("%s %d read badge badgebuffer is %s code = %d",__FUNCTION__,__LINE__,oper->getBadgeBuffer(),code);
        if (code == SC_Success && strcmp(MyOperation->GetAccessCode()/*"00000000000000"*/, oper->getBadgeBuffer()) == 0)
        {

            //In technical mode we set the finger print mode to Ignore_Operation to ignore any finger template scanning
            fpOpr.fpOperation = Ignore_Operation;

            demo.cfgMgr.ShowConfigurationMenu();                   // show the technical mode.

            pthread_mutex_unlock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.
            oper->Reset();                                         // reset the buffer and continue .
            continue;
        }
		//DLOGI("%s %d read badge badgebuffer is %s code = %d\n",__FUNCTION__,__LINE__,oper->getBadgeBuffer(),code);
		if (code == SC_Success && strcmp(MyOperation->GetResetAccessCode()/*"00000000000000"*/, oper->getBadgeBuffer()) == 0)
        {
            swipeWithActivityCode = false;
            fpOpr.fpOperation = Ignore_Operation;
            // not needed?:
            // pthread_mutex_trylock(&OperationIODriver::lockDisplay);
            demo.cfgMgr.ShowTerminalResetScreen();                         // show the technical mode.
            pthread_mutex_unlock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.
            oper->Reset();                                         // reset the buffer and continue .
			continue;
        }
		//DLOGI("%s %d read badge badgebuffer is %s code = %d\n",__FUNCTION__,__LINE__,oper->getBadgeBuffer(),code);
        if (code == SC_ActivityCodePrompt)
        {
            swipeWithActivityCode = true;
            fpOpr.fpOperation = Ignore_Operation;
            pthread_mutex_lock(&OperationIODriver::lockDisplay);
            // this works: code = oper->SuperReadEmpBadge();
            code = oper->SuperReadEmpBadge();
            user_output_clear();

            //
        }
		//DLOGI("%s %d read badge badgebuffer is %s code = %d\n",__FUNCTION__,__LINE__,oper->getBadgeBuffer(),code);
        if (code == SC_ShowAppVer)
        {
            swipeWithActivityCode = false;
            fpOpr.fpOperation = Ignore_Operation;

            // try this:
            pthread_mutex_lock(&OperationIODriver::lockDisplay);
            demo.cfgMgr.ShowVersionMenu();                         // show the technical mode.
            pthread_mutex_unlock(&OperationIODriver::lockDisplay); // unlock the LCD mutex and continue the next loop.
            oper->Reset();                                         // reset the buffer and continue .

            //
        }
		//DLOGI("%s %d read badge badgebuffer is %s code = %d\n",__FUNCTION__,__LINE__,oper->getBadgeBuffer(),code);
        if (code != SC_Success)
        {
            // in case time out or failed to get badge reset the buffers and continue .
            // ReadBadge clock the LCD mutex therefore we must unclock it every time we call ReadBadge.
            if (oper->getSourceBadge() == SysInputSourceFprint)
                {
                //if the source if the FPU display the error regarding to the fpOpr.ScannResult value
                demo.DispalyFpPromptError(oper, code);
                }
            pthread_mutex_unlock(&OperationIODriver::lockDisplay);
            oper->Reset();
            continue;
        }
        else
        {
			DLOGI("%s %d read badge badgebuffer is %s code = %d",__FUNCTION__,__LINE__,oper->getBadgeBuffer(),code);
            //in case we get badge from readers we need to dispaly it , from the keyboard the badge displayed while
            //we insert the badge .
            // time out to disdlay the badge, if there specific message for the employee in the setup system view it
            //instead of the badge number.
			char msg[32];
			int lenMsg = 0;
			lenMsg = oper->GetOperation()->GetEmployeeMessage(oper->getBadgeBuffer(),msg,32);

            if (lenMsg == 0 && oper->getSourceBadge() != SysInputSourceKeypad)
            {
                snprintf(msg,32,"%s",oper->getBadgeBuffer());
                lenMsg = strlen(msg);
            }

            printf("%s %d msg = %s len = %d\n",__FUNCTION__,__LINE__,msg,lenMsg);

            if (lenMsg != 0)
            {
                user_output_clear();
                int col = (20 - strlen("BADGE NUMBER")) / 2 + 1;

                if (msg == oper->getBadgeBuffer())
                    user_output_write(2, col, "BADGE NUMBER");
                col = (20 - lenMsg) / 2 + 1;
                user_output_write(3, col, msg);
                // GetMessageTimeout return the time out in milliseconds
                usleep(oper->GetOperation()->GetMessageTimeout() * 200); // usleep accept time in microseconds
             }


            // ---------
            // ----------------- employee validation
            // ---------
//				{
//					SysInputSource tsrc = oper->getSourceBadge();
//					switch ( tsrc )
//					{
//						case SysInputSourceMagnetic :
//							DP("badge inputsource is Magnetic\n");
//							break;
//						case SysInputSourceKeypad :
//							DP("badge inputsource is Keypad\n");
//							break;
//						case SysInputSourceBarcode :
//							DP("badge inputsource is Barcode\n");
//							break;
//						case SysInputSourceProximity :
//							DP("badge inputsource is Proximity\n");
//							break;
//						case SysInputSourceFprint :
//							DP("badge inputsource is Fingerprint\n");
//							break;
//						case SysInputSourceProxMag :
//							DP("badge inputsource is ProxMag\n");
//							break;
//						default:
//							DP("badge inputsource is Other: %d\n", tsrc);
//							break;

//					}

//				}

			DLOGI("%s %d Check if need to verify User by FPU",__FUNCTION__,__LINE__);
            if (demo.UserNeedsFPV(oper) && FPOperation::EnableFpUnit
                && demo.EnableFpVerifyMode) //TODO: fetch all three from Properties
            {
				DLOGI("%s %d Required verify user by FPU Check ValidateUser",__FUNCTION__,__LINE__);
                if (demo.ValidateUser(oper))
				{
                    // Fingerprint Verification
                    if (oper->getSourceBadge() != SysInputSourceFprint && FPOperation::EnableFpUnit && demo.EnableFpVerifyMode)
                    {
                        DP("verify badge with fp\n");
// if the source badge that received not from FPU and we set the verification flag to true then we set
//the mode of the FPU to Ignore_Operation to ignore any template scanned from the idle display
//while we perform verification to the user by live scann.
                        fpOpr.fpOperation = Ignore_Operation;

                        if (!(demo.FpVerifyUser(oper))) // verify the user, if the user not exist , display suitable massenge
                        {
                            DP("verifyuser fails\n");
                            // unlock the LCD and reset the buffer and continue to next loop
                            pthread_mutex_unlock(&OperationIODriver::lockDisplay);
                            oper->Reset();
                            continue;
                        }
                        else
                        {
							char vs[128];
							sprintf(vs, "Verified Badge #: %s", oper->getBadgeBuffer() );
                            //DP("verifyuser succeeds\n");
							DLOGI(vs);
                        }
                    }
				}
				else
                {
                    pthread_mutex_unlock(&OperationIODriver::lockDisplay);
                    oper->Reset();
                    continue;
                }
			}
            else
			{
                // validate without FPVerification
				DLOGI("%s %d No need verify user by FPU Check ValidateUser",__FUNCTION__,__LINE__);
                if (demo.ValidateUser(oper))	// call IsValidEmployee or always return True.
                {


#ifdef USE_PIN_SUPPORT

                    // Ok.
					//
					// all users must be verified. if not fpverify, then use PIN entry
					//
					if ( oper->ReadPIN() != SC_Allow )
					{
						DP("ReadPIN fails.\n");
                        pthread_mutex_unlock(&OperationIODriver::lockDisplay);
						DLOGI("%s %d - before oper->Reset()", __FUNCTION__,__LINE__);
                        oper->Reset();
                        continue;
					}
					DLOGI("%s %d - ValidatePinEntry()", __FUNCTION__,__LINE__);
					if ( demo.ValidatePinEntry(oper) )
					{
						char vpe[128];
						sprintf(vpe, "Verified Badge #: %s with Pin Entry", oper->getBadgeBuffer() );
						DLOGI(vpe);
					}
					else
					{
                        DP("validatepinentry fails\n");
                        // unlock the LCD and reset the buffer and continue to next loop
                        pthread_mutex_unlock(&OperationIODriver::lockDisplay);
						DLOGI("%s %d - before oper->Reset()", __FUNCTION__,__LINE__);
                        oper->Reset();
                        continue;
					}

#endif // use_pin_support

					//
                }
                else
                {
                    pthread_mutex_unlock(&OperationIODriver::lockDisplay);
                    oper->Reset();
                    continue;
                }
            }


            // ---------
            // ----------------- readfunction must react to swipeandgo settings and pass functionkey through
            // ---------

            OperationStatusCode code1;

            if (swipeWithActivityCode == true)
            {
                code1 = oper->ForceActivityCodeFunction(TYPE_Employee);
            }
            else
            {
                // after we display the badge ,we wait for function .
				//printf("before oper->ReadFunction\n");
                code1 = oper->ReadFunction(TYPE_Employee);
				//printf("after oper->ReadFunction\n");
            }

            if (code1 == SC_Allow)
            {

				//printf("before SendSwipePunchWithActivityCode\n");
                demo.SendSwipePunchWithActivityCode(oper, NULL);
				//printf("after SendSwipePunchWithActivityCode\n");
                // turnoff001 }
                // unlock the LCD mutex that locked by the ReadBadge.
                pthread_mutex_unlock(&OperationIODriver::lockDisplay);
                // pthread_cond_signal(&FPOperation::fprint_cond_flag);// wake finger print thread.
                //if(oper->getSourceBadge() == SysInputSourceFprint)pthread_cond_signal(&FPOperation::fprint_cond_flag);
                oper->Reset(); //reset the buffers;
            }
            else
                {
                //if the function not defined unlock the LCD mutex and continue to the next badge.
                pthread_mutex_unlock(&OperationIODriver::lockDisplay);

                if (oper->getSourceBadge() == SysInputSourceFprint)
                    pthread_cond_signal(&FPOperation::fprint_cond_flag);
                oper->Reset(); //reset the buffers;
                }
        }
    }
}



/*

initServerDBTables:

    this methos check if there is Transactions table and Users table in the data base server
    if not it's create the tables.

Description:

      this methos check if there is transactions table and User table in the server data base
    if not it's create the tables .


Arguments:
    SDBConnection - server data base connection

Returns:
    void-
*/

//void SY7000Demo::initServerDBTables( ServerDataBaseConnection* SDBConnection )
//    {
//    int row_count = 0;
//    int num_cols = 0;
//    CS_RETCODE res;

//    if (SDBConnection->connectionResult == CS_SUCCEED) //if there is connection with the data base server
//        {
//        //sql quert return the tables with name Users , to check if the Users table exist in the data base server
//        res = SDBConnection->RunQueryWithResult("SELECT name FROM sysobjects WHERE (name = 'Users')");

//        while (res
//            == CS_SUCCEED)                                          // get rows result until last row , the first row we received is the titles of the table.
//            res = SDBConnection->GetRowResult(row_count, num_cols); // the row columns saved in SDBConnection->col array

//        if (strcmp("Users", SDBConnection->col[0].data))            // Users table not exist in the Data Base
//            {
//            //run sql query that create Users table in the data base server.
//            res = SDBConnection->RunQueryWithoutResult("CREATE TABLE Users(Name varchar(30), Badge int)");

//            if (res == CS_SUCCEED)
//                printf("Users table was created in the server DB\n");
//            else
//                printf("Error in create Users table\n");
//            }
//        res = SDBConnection->RunQueryWithResult("SELECT name FROM sysobjects WHERE (name = 'Transactions')");

//        while (res == CS_SUCCEED)
//            res = SDBConnection->GetRowResult(row_count, num_cols);

//        if (strcmp("Transactions", SDBConnection->col[0].data)) // Transactions table not exist in the Data Base
//            {
//            //run sql query that create Transactions table in the data base server.
//            res =
//                SDBConnection->RunQueryWithoutResult(
//                    "CREATE TABLE Transactions(UserName varchar(30),Badge int, Date varchar(40), FunctionKey varchar(20))");

//            if (res == CS_SUCCEED)
//                printf("Transactions table was created in the server DB\n");
//            else
//                printf("Error in create Transactions table\n");
//            }
//        }
//    else
//        printf("Can't init Server Data Base Tables please check connection\n");
//    }
/*
    initSetupSystemDataStructure:

    allocate the setup system configuration tables we need to the demo .

Description:

    allocate the setup system configuration tables we need to the demo.


Arguments:
    void

Returns:
    void

*/

void SY7000Demo::initSetupSystemDataStructure( void )
    {

    //define static struct that contain the tables we need to the allocate to the setup system configuration.
    static struct
        {
        struct
            {
            InterfaceMessageBadgeHolder * badge; // table for employee messages
            } message;

        struct
            {
            //table that define the keys item
            //the maximum keys number is 13  ,from F1 to F12 and virtual key for swipe and go state
            //the characteristic char for every keys similar to a telephone keypad ,
            //F1='1' F2='2' ......F9='9' F11='0' F10='*' F12='#' (virtual)Swipe&GO = '?'
            SysFunctionKeyItem key[SysFunctionKeyMax];
            //table that define the levels detailes for every function key
            //length of the SysFunctionDetailItem table is the (function keys number)*(the maximum level for every key)
            //SysFunctionDetailMax = SysFunctionKeyMax * SysFunctionKeyLevelsMax = 13*6
            SysFunctionDetailItem detail[SysFunctionDetailMax];
            } function;
        } tables;

    tables.message.badge = new InterfaceMessageBadgeVector();
    //submit the employees masseges table in the setup system data structure
    SystemSetup.message.badge = tables.message.badge;
    //submit the function keys tables in the setup system data structure
    SystemSetup.function.key = tables.function.key;
    SystemSetup.function.detail = tables.function.detail;

    return;
    }

/*
    ResetFunctionKeys:

    reset the functions keys tables .

Description:

    reset the functions keys tables .


Arguments:
    void

Returns:
    void

*/

void SY7000Demo::ResetFunctionKeys()
    {
    for ( int j = 0; j < SysFunctionKeyMax; j++ )
        {
        SystemSetup.function.key[j].flags = 0;
        SystemSetup.function.key[j].levels = 0;
        strcpy(SystemSetup.function.key[j].Function, "");
        }

    for ( int i = 0; i < SysFunctionDetailMax; i++ )
        {
        SystemSetup.function.detail[i].key = '0';
        SystemSetup.function.detail[i].level = 0;
        strcpy(SystemSetup.function.detail[i].message, "");
        strcpy(SystemSetup.function.detail[i].defaultData, "");
        SystemSetup.function.detail[i].source = 15;
        SystemSetup.function.detail[i].type = 0;
        SystemSetup.function.detail[i].decimal = 0;
        SystemSetup.function.detail[i].length.max = 0;
        SystemSetup.function.detail[i].length.min = 0;
        SystemSetup.function.detail[i].validation.flags = 0;
        SystemSetup.function.detail[i].validation.table = 0;
        }
    }

/*
    SetOutPunchFunctionKey:

    set the OutPunch function to the key specify in FunctionKey.

Description:

    set the OutPunch function to the key specify in FunctionKey .


Arguments:
    FunctionKey - the key index in the  SysFunctionKeyItem table.
                  F11 = 0 ,F1 = 1 , F2=2 , .....,F9 = 9 ,F10= 10 , F12 = 11 , swipe&go = 12

Returns:
    void

*/
void SY7000Demo::SetOutPunchFunctionKey( int FunctionKey )
    {
    //calculate the start index of the first level in SysFunctionDetailItem table
    int detailIndx = FunctionKey * SysFunctionKeyLevelsMax;

    SystemSetup.function.key[FunctionKey].levels = 1; // levels number
    SystemSetup.function.key[FunctionKey].flags = 0;

    // the characteristic char for every keys similar to a telephone key pad
    //F1='1' F2='2' ......F9='9' F11='0' F10='*' F12='#' (virtual)Swipe&GO = '?'
    SystemSetup.function.key[FunctionKey].key = 48 + FunctionKey;       //calculate the characteristic char ascii
    SystemSetup.function.detail[detailIndx].key = 48 + FunctionKey;

    strcpy(SystemSetup.function.key[FunctionKey].Function, "OutPunch"); //set the function name.

    if (FunctionKey == 10)                                              // for F10
        {
        SystemSetup.function.key[FunctionKey].key = '*';
        SystemSetup.function.detail[detailIndx].key = '*';
        }

    if (FunctionKey == 11) //for F12
        {
        SystemSetup.function.key[FunctionKey].key = '#';
        SystemSetup.function.detail[detailIndx].key = '#';
        }
    SystemSetup.function.detail[detailIndx].level = 1;                      //set the first level details
    strcpy(SystemSetup.function.detail[detailIndx].message, "PUNCH OUT");   //message displayed if there is data need
    SystemSetup.function.detail[detailIndx].source = SysInputSourceInvalid; //no need to data source
    SystemSetup.function.detail[detailIndx].type = SysInputTypeDefault;     //no need to insert data
    }

/*
    SetInPunchFunctionKey:

    set the InPunch function to the key specify in FunctionKey.

Description:

    set the InPunch function to the key specify in FunctionKey .


Arguments:
    FunctionKey - the key index in the  SysFunctionKeyItem table.
                  F11 = 0 ,F1 = 1 , F2=2 , .....,F9 = 9 ,F10= 10 , F12 = 11 , swipe&go = 12

Returns:
    void

*/
void SY7000Demo::SetInPunchFunctionKey( int FunctionKey )
    {
    //calculate the start index of the first level of the key in SysFunctionDetailItem table
    int detailIndx = FunctionKey * SysFunctionKeyLevelsMax;

    SystemSetup.function.key[FunctionKey].levels = 1; //levels number
    SystemSetup.function.key[FunctionKey].flags = 0;

    // the characteristic char for every keys similar to a telephone key pad
    //F1='1' F2='2' ......F9='9' F11='0' F10='*' F12='#' (virtual)Swipe&GO = '?'
    SystemSetup.function.key[FunctionKey].key = 48 + FunctionKey;
    SystemSetup.function.detail[detailIndx].key = 48 + FunctionKey;

    strcpy(SystemSetup.function.key[FunctionKey].Function, "InPunch"); //set the function name.

    if (FunctionKey == 10)                                             // for F10
        {
        SystemSetup.function.key[FunctionKey].key = '*';
        SystemSetup.function.detail[detailIndx].key = '*';
        }

    if (FunctionKey == 11) // for F12
        {
        SystemSetup.function.key[FunctionKey].key = '#';
        SystemSetup.function.detail[detailIndx].key = '#';
        }
    SystemSetup.function.detail[detailIndx].level = 1;                   //set the first level details
    strcpy(SystemSetup.function.detail[detailIndx].message, "PUNCH IN"); //no need to insert data
    strcpy(SystemSetup.function.detail[detailIndx].defaultData, "1234");

    SystemSetup.function.detail[detailIndx].source = SysInputSourceInvalid;
    ;                                                                   //no need to data source
    SystemSetup.function.detail[detailIndx].type = SysInputTypeDefault; //no need to insert data

    //This field indicate the decimal digits number in case decimal input "SysInputTypeDecimal" type.
    SystemSetup.function.detail[detailIndx].decimal = 0;
    SystemSetup.function.detail[detailIndx].length.max = 0; // maximum data length
    SystemSetup.function.detail[detailIndx].length.min = 0; // minimum data length
    SystemSetup.function.detail[detailIndx].validation.flags = 0;
    SystemSetup.function.detail[detailIndx].validation.table = 0;
    }

/*
    SetSwipePunchFunctionKey:

    set the SwipePunch function to the key specify in FunctionKey.

Description:

    set the SwipePunch function to the key specify in FunctionKey .


Arguments:
    FunctionKey - the key index in the  SysFunctionKeyItem table.
                  F11 = 0 ,F1 = 1 , F2=2 , .....,F9 = 9 ,F10= 10 , F12 = 11 , swipe&go = 12

Returns:
    void

*/
void SY7000Demo::SetSwipePunchFunctionKey( int FunctionKey )
    {
    //calculate the start index of the first level of the key in SysFunctionDetailItem table
    int detailIndx = FunctionKey * SysFunctionKeyLevelsMax;

    SystemSetup.function.key[FunctionKey].levels = 1; //levels number
    SystemSetup.function.key[FunctionKey].flags = 0;

    // the characteristic char for every keys similar to a telephone key pad
    //F1='1' F2='2' ......F9='9' F11='0' F10='*' F12='#' (virtual)Swipe&GO = '?'
    SystemSetup.function.key[FunctionKey].key = 48 + FunctionKey;
    SystemSetup.function.detail[detailIndx].key = 48 + FunctionKey;

    strcpy(SystemSetup.function.key[FunctionKey].Function, "SwipePunch"); //set the function name.

    if (FunctionKey == 10)                                                // for F10
        {
        SystemSetup.function.key[FunctionKey].key = '*';
        SystemSetup.function.detail[detailIndx].key = '*';
        }

    if (FunctionKey == 11) // for F12
        {
        SystemSetup.function.key[FunctionKey].key = '#';
        SystemSetup.function.detail[detailIndx].key = '#';
        }

    if (FunctionKey == 12) // for F12
        {
        SystemSetup.function.key[FunctionKey].key = '&';
        SystemSetup.function.detail[detailIndx].key = '&';
        }

    if (FunctionKey == 13) // for virtual key '?'
        {
        SystemSetup.function.key[FunctionKey].key = '?';
        SystemSetup.function.detail[detailIndx].key = '?';
        }

    //SystemSetup.function.detail[detailIndx].level = 1; //set the first level details
    //strcpy(SystemSetup.function.detail[detailIndx].message,"SWIPE AND GO");//no need to insert data
    //strcpy(SystemSetup.function.detail[detailIndx].defaultData,"1234");

    //set level 1 details
    SystemSetup.function.detail[detailIndx].level = 1;                       //set the first level details
    strcpy(SystemSetup.function.detail[detailIndx].message, "SWIPE AND GO"); //the display prompt when insert the data
    SystemSetup.function.detail[detailIndx].source = SysInputSourceInvalid;  //insert data just by keypad source
    SystemSetup.function.detail[detailIndx].type = SysInputTypeNumeric;      //the data type is Alpha Numeric
    //This field indicate the decimal digits number in case decimal input "SysInputTypeDecimal" type.
    SystemSetup.function.detail[detailIndx].decimal = 0;
    SystemSetup.function.detail[detailIndx].length.max = 4; // maximum data length
    SystemSetup.function.detail[detailIndx].length.min = 0; // minimum data length
    SystemSetup.function.detail[detailIndx].validation.flags = 0;
    SystemSetup.function.detail[detailIndx].validation.table = 0;
    }

void SY7000Demo::SetSwipePunchWithActivityCodeFunctionKey( int FunctionKey )
    {
    //calculate the start index of the first level of the key in SysFunctionDetailItem table
    int detailIndx = FunctionKey * SysFunctionKeyLevelsMax;

    SystemSetup.function.key[FunctionKey].levels = 1; //levels number
    SystemSetup.function.key[FunctionKey].flags = 0;

    // the characteristic char for every keys similar to a telephone key pad
    //F1='1' F2='2' ......F9='9' F11='0' F10='*' F12='#' (virtual)Swipe&GO = '?'
    SystemSetup.function.key[FunctionKey].key = 48 + FunctionKey;
    SystemSetup.function.detail[detailIndx].key = 48 + FunctionKey;

    strcpy(SystemSetup.function.key[FunctionKey].Function, "SwipePunch"); //set the function name.

    if (FunctionKey == 10)                                                // for F10
        {
        SystemSetup.function.key[FunctionKey].key = '*';
        SystemSetup.function.detail[detailIndx].key = '*';
        }

    if (FunctionKey == 11) // for F12
        {
        SystemSetup.function.key[FunctionKey].key = '#';
        SystemSetup.function.detail[detailIndx].key = '#';
        }

    if (FunctionKey == 12) // for F12
        {
        SystemSetup.function.key[FunctionKey].key = '&';
        SystemSetup.function.detail[detailIndx].key = '&';
        }

    if (FunctionKey == 13) // for virtual key '?'
        {
        SystemSetup.function.key[FunctionKey].key = '?';
        SystemSetup.function.detail[detailIndx].key = '?';
        }

    //SystemSetup.function.detail[detailIndx].level = 1; //set the first level details
    //strcpy(SystemSetup.function.detail[detailIndx].message,"SWIPE AND GO");//no need to insert data
    //strcpy(SystemSetup.function.detail[detailIndx].defaultData,"1234");

    //set level 1 details
    SystemSetup.function.detail[detailIndx].level = 1;                        //set the first level details
    strcpy(SystemSetup.function.detail[detailIndx].message, "ACTIVITY CODE"); //the display prompt when insert the data
    SystemSetup.function.detail[detailIndx].source = SysInputSourceKeypad;    //insert data just by keypad source
    SystemSetup.function.detail[detailIndx].type = SysInputTypeNumeric;       //the data type is Alpha Numeric
    //This field indicate the decimal digits number in case decimal input "SysInputTypeDecimal" type.
    SystemSetup.function.detail[detailIndx].decimal = 0;
    SystemSetup.function.detail[detailIndx].length.max = 6; // maximum data length
    SystemSetup.function.detail[detailIndx].length.min = 0; // minimum data length
    SystemSetup.function.detail[detailIndx].validation.flags = 0;
    SystemSetup.function.detail[detailIndx].validation.table = 0;
    }

void SY7000Demo::SetClockInFunctionKey( int FunctionKey )
{

    //calculate the start index of the first level of the key in SysFunctionDetailItem table
    int detailIndx = FunctionKey * SysFunctionKeyLevelsMax;

    SystemSetup.function.key[FunctionKey].levels = 2; //levels number
    SystemSetup.function.key[FunctionKey].flags = 0;

    // the characteristic char for every keys similar to a telephone key pad
    //F1='1' F2='2' ......F9='9' F11='0' F10='*' F12='#' (virtual)Swipe&GO = '?'
    SystemSetup.function.key[FunctionKey].key = 48 + FunctionKey;
    SystemSetup.function.detail[detailIndx].key = 48 + FunctionKey;

    strcpy(SystemSetup.function.key[FunctionKey].Function, "ClockIn"); //set the function name.

    if (FunctionKey == 10)                                             // for F10
        {
        SystemSetup.function.key[FunctionKey].key = '*';
        SystemSetup.function.detail[detailIndx].key = '*';
        }

    if (FunctionKey == 11) // for F12
        {
        SystemSetup.function.key[FunctionKey].key = '#';
        SystemSetup.function.detail[detailIndx].key = '#';
        }


    //set level 1 details
    SystemSetup.function.detail[detailIndx].level = 1;                        //set the first level details
    strcpy(SystemSetup.function.detail[detailIndx].message, "ACTIVITY CODE"); //the display prompt when insert the data
    SystemSetup.function.detail[detailIndx].source = SysInputSourceKeypad;    //insert data just by keypad source
    SystemSetup.function.detail[detailIndx].type = SysInputTypeNumeric;       //the data type is Alpha Numeric
    //This field indicate the decimal digits number in case decimal input "SysInputTypeDecimal" type.
    SystemSetup.function.detail[detailIndx].decimal = 0;
    SystemSetup.function.detail[detailIndx].length.max = 4; // maximum data length
    SystemSetup.function.detail[detailIndx].length.min = 0; // minimum data length
    SystemSetup.function.detail[detailIndx].validation.flags = 0;
    SystemSetup.function.detail[detailIndx].validation.table = 0;
}
/*
    SetupSystem:

    setup the system configuration .

Description:

    setup the system configuration .


Arguments:
    void

Returns:
    void

*/

void SY7000Demo::SetupSystem()
{

	char HearBeatFreqStr[16];
	char GetActivitiesTimerStr[16];

    XMLProperties xmlParser("properties.xml");

    //setup prompt types.

    char title[20];
    xmlParser.GetProperty("Title", title);

    if (strlen(title) > 0)
        {
        strcpy(SystemSetup.user.prompt.idle, title); // Idle Prompt Display 20 characters max
        }
    else
        {
        strcpy(SystemSetup.user.prompt.idle, "Prologic SY7200");      // Idle Prompt Display 20 characters max
        }

    strcpy(SystemSetup.user.prompt.invalid_source, "INVALID SOURCE"); //Invalid Source Message 20 characters max
    strcpy(SystemSetup.user.prompt.invalid_badge, "INVALID BADGE");   //Invalid Badge Message 20 characters max
    strcpy(SystemSetup.user.prompt.try_again, "PLEASE TRY AGAIN");    //Please Try Again 20 characters max
    strcpy(SystemSetup.user.prompt.enter_function, "ENTER FUNCTION"); //Enter Function Message 20 characters max

    //setup time out
    SystemSetup.user.timeout.data = 90;     //General Data Input time-out tenths of seconds
    SystemSetup.user.timeout.function = 50; //Enter Function time-out	tenths of seconds
    SystemSetup.user.timeout.message = 30;  //Message time-out	tenths of seconds
    SystemSetup.user.timeout.error = 30;    //Error message time-out tenths of seconds

    char badgeLen[10];
    char sources[6];
#ifdef client_prologic
	char badgeOffset[8];
	char badgeValid[8];
	//
	char keypadOffset[8];
	char keypadValid[8];
	char keypadLen[10];

	keypadLen[0] = '\0';
	keypadOffset[0] = '\0';
	keypadValid[0] = '\0';

#endif
    badgeLen[0] = '\0';
    sources[0] = 'N';
    sources[1] = 'N';
    sources[2] = 'N';
    sources[3] = 'N';
    sources[4] = 'N';
    sources[5] = '\0';
    SysInputSource so = SysInputSourceInvalid;
    xmlParser.GetProperty("BadgeLength", badgeLen);
    xmlParser.GetProperty("BadgeSources", sources);

    if (strlen(badgeLen) > 0)
    {
        //setup badge length from xml file configuration
        SystemSetup.badge.length.maximum = atoi(badgeLen);
        SystemSetup.badge.length.minimum = atoi(badgeLen);
        SystemSetup.badge.length.valid = atoi(badgeLen);
    }
    else
    {
        //
        SystemSetup.badge.length.maximum = 10;
        SystemSetup.badge.length.minimum = 10;
        SystemSetup.badge.length.valid = 10;
	}


// get badge sources before configuring for single/multiple badge parameters

    //setup badge source from the xml file configuration.
    if (sources[0] == 'K')
        so = so | SysInputSourceKeypad;

    if (sources[1] == 'S')
        so = so | SysInputSourceMagnetic;

    if (sources[2] == 'W')
        so = so | SysInputSourceBarcode;

    if (sources[3] == 'P')
        {
        so = so | SysInputSourceProximity;
        }

    if (sources[4] == 'F')
        so = so | SysInputSourceFprint;
    SystemSetup.source.initial = so;
    SystemSetup.source.function = SysInputSourceDefault;
    SystemSetup.source.supervisor = so;
    SystemSetup.source.super_employee = so;
    SystemSetup.source.configuration = so;


#ifdef client_prologic
	// prologic card reader badge parameters
	xmlParser.GetProperty("BadgeOffset", badgeOffset);
	if ( strlen(badgeOffset) > 0 )
	{
		SystemSetup.badge.length.offset = atoi(badgeOffset);
	}
	else
	{
		// default to 0
		SystemSetup.badge.length.offset = 0;
	}

	xmlParser.GetProperty("BadgeValid", badgeValid);
	if ( strlen(badgeValid) > 0 )
	{
		SystemSetup.badge.length.valid = atoi(badgeValid);
	}
	else
	{
		// default to badgeLength as initialized length.valid
		//SystemSetup.badge.length.valid = atoi(badgeLen);
	}

#endif

#ifdef client_prologic
	// prologic keypad badge parameters


	// only if keypad is enabled as an input source

	if ( (SystemSetup.source.configuration & SysInputSourceKeypad ) == SysInputSourceKeypad )
	{
printf("keypad is a valid input source\n");

    	xmlParser.GetProperty("KeypadLength", keypadLen);

printf("keypad input length is %s\n", keypadLen);

    	if (strlen(keypadLen) > 0)
    	{
    	    //setup badge length from xml file configuration
    	    KeypadSetup.badge.length.maximum = atoi(keypadLen);
    	    KeypadSetup.badge.length.minimum = atoi(keypadLen);
    	    KeypadSetup.badge.length.valid = atoi(keypadLen);
    	}
    	else
    	{
    	    // default to SystemSetup.badge values
    	    KeypadSetup.badge.length.maximum = SystemSetup.badge.length.maximum;
    	    KeypadSetup.badge.length.minimum = SystemSetup.badge.length.minimum;
    	    KeypadSetup.badge.length.valid =   SystemSetup.badge.length.valid;
		}

		xmlParser.GetProperty("KeypadOffset", keypadOffset);
		if ( strlen(keypadOffset) > 0 )
		{
			KeypadSetup.badge.length.offset = atoi(keypadOffset);
		}
		else
		{
			// default to 0. start at the beginning
			KeypadSetup.badge.length.offset = 0;
		}

		xmlParser.GetProperty("KeypadValid", keypadValid);
		if ( strlen(keypadValid) > 0 )
		{
			KeypadSetup.badge.length.valid = atoi(keypadValid);
		}
		else
		{
			// default to initialized length.valid value
			//KeypadSetup.badge.length.valid = SystemSetup.badge.length.valid;
		}

   	    //printf("keypadsetup.badge.length.maximum is %d\n", KeypadSetup.badge.length.maximum)  ;
   	    //printf("keypadsetup.badge.length.minimum is %d\n", KeypadSetup.badge.length.minimum );
		//printf("keypadsetup.badge.length.valid is %d\n", KeypadSetup.badge.length.valid    );
		//printf("keypadsetup.badge.length.offset is %d\n", KeypadSetup.badge.length.offset    );

	}
	else
	{
		printf("keypad is not valid input source or is not detected \n");
	}

#endif


#ifdef USE_SELECTIVE_FPV

	// default to true

	char selectivefpv_keypad[8];
    xmlParser.GetProperty("KeypadNeedsFPV", selectivefpv_keypad);
    if ( selectivefpv_keypad && strcmp(selectivefpv_keypad,"false")==0 )
    {
		useKeypadFPV = false;
    }
    else
    {
		useKeypadFPV = true;
	}

	// there is generally only one card reader attached.
	char selectivefpv_card[8];
    xmlParser.GetProperty("CardNeedsFPV", selectivefpv_card);
    if ( selectivefpv_card && strcmp(selectivefpv_card,"false")==0 )
    {
		useCardFPV = false;
    }
    else
    {
		useCardFPV = true;
	}

#endif


    //setup function key.
    ResetFunctionKeys();         //reset the function keys table

    SetSwipePunchFunctionKey(1); //set swipe-n-go function to virtual Key
    SetSwipePunchWithActivityCodeFunctionKey(2);
    // F3 key is forcibly set to handle Activity Code
    // sequence would be F3 -> Activity Code -> Badge Swipe -> Send
    //
    // todo:
    // F12 key is forcibly set to handle ShowApplicationVersion
    // or require a badge swipe, and handle F12.
    // SetXXXXXKey() would be required.
    //


/////////////  load other properties here

	HearBeatFreqStr[0] = '\0';
	GetActivitiesTimerStr[0] = '\0';

	// get the timer frequency of the heart beat from  the xml file "properties.xml"
    xmlParser.GetProperty("HeartBeatTimer", HearBeatFreqStr);
    if (strlen(HearBeatFreqStr) > 0)
        heartbeatFrequency = atol(HearBeatFreqStr);
    else
        heartbeatFrequency = 5;

    heartbeatFrequency = 1000000 * heartbeatFrequency;
    // get the timer of the get activities in seconds
DP("setup: get GetActivitesTimer\n");
	xmlParser.GetProperty("GetActivitiesTimer", GetActivitiesTimerStr);
	if(GetActivitiesTimerStr == NULL )
	{
		 printf("setup: GetActivitiesTimerStr is NULL\n");
	}

    if (strlen(GetActivitiesTimerStr) > 0)
	{
        getActivitiesFrequency = atol(GetActivitiesTimerStr);
DP("setup: GetActivitiesTimer is read as %s, value is %d\n", GetActivitiesTimerStr, getActivitiesFrequency);
	}
    else
	{
        getActivitiesFrequency = 86400;
DP("setup: GetActivitiesTimer property not found, default value is %d\n", getActivitiesFrequency);
	}

	// special debug properties
	char DSP[8];
	DSP[0] = '\0';
	xmlParser.GetProperty("DontSendPunches", DSP);
	if (strcmp(DSP, "true") == 0)            //
	{
		DontSendPunches = true;
		DP("setting DontSendPunches to true\n");
	}
	else                                          // default is false if element does not exist
	{
		DontSendPunches = false;
		DP("setting DontSendPunches to false\n");

	}

}

void SY7000Demo::initCfgIODriver( OperationIODriver* oper )
    {
    cfgMgr.setOperationIODriver(oper);
    }


void SY7000Demo::SYClock_GetProps()
{
   XMLProperties xmlParser("properties.xml");

   printf("Getting Properties\n");
   char tmp[256];tmp[0]='\0';
   
   	tmp[0]='\0';
	xmlParser.GetProperty("TimeZone",tmp);
	printf("timezone = %s\n",tmp);
	Operation::iTimeZone = atoi(tmp);

	xmlParser.GetProperty("DaylightSavingsStart", Operation::strDaylightSavingsStart);
	xmlParser.GetProperty("DaylightSavingsEnd", Operation::strDaylightSavingsEnd);
}

/*
    initHttpClient:

    This function get the parameters from "properties.xml" xml file that we need to connect to the web service and get
    parameters that decide if we communicate whith the web service or with the data base server.

Description:

    This function get the parameters from "properties.xml" file that we need to connect to the web service and
    parameters that decide if we communicate whith the web service or with the data base server.


Arguments:
    void

Returns:
    void

*/

void SY7000Demo::initHttpClient()
{
    //creat xml parser to get the web service parameters we need to connect to it
    //"properties.xml" must be located in the same directory of the demo application in "/home/terminal/bin"
    XMLProperties xmlParser("properties.xml");


	// allocate memory for link path and use that in call to GetProperty.
	// otherwise, GetProperty returns a pointer to an internal 64 byte buffer.
	// This is probably the cause of the memory overrun.
	//char* tmp = NULL;
	char tmp[256];		

    UseSqlServer = false;
    //tmp = NULL;
	tmp[0] = '\0';

    //tmp = xmlParser->GetProperty("WebService"); // get the link of th web service
	//xmlParser->GetProperty("WebService", tmp); // get the link of th web service

	tmp[0] = '\0';
	xmlParser.GetProperty("WebService",tmp); // get the link of th web service; use internal buffer


DP("inithttp: WebService to contact is %s, len=%d\n", tmp, strlen(tmp));

    if (strlen(tmp))
        strcpy(LinkWeb, tmp);
    else
        strcpy(LinkWeb,
            "http://212.179.32.124:446/Clocks/Service.asmx"); //the default web service URL in case no Link in
    //the xml file
    xmlParser.GetProperty("Account", Account); //get account that we need to insert in the request
    xmlParser.GetProperty("ClockId", ClockId); //get clock id that we need to insert in the request
    tmp[0] = '\0';
	xmlParser.GetProperty("TimeOut",tmp);    // get request time out sending
    if (strlen(tmp))
        strcpy(timeOut, tmp);
    else
        strcpy(timeOut, "5");

	long timeOutLong = atol(timeOut);
	if(timeOutLong>10)timeOutLong=10; //limit the time out to 10;

	tmp[0] = '\0';
    xmlParser.GetProperty("UseSoapActionHeader",tmp); //some web services ask the Soap Action Header
    //to the 	http post like:
    //SOAPAction: "Synel/Services/InPunch"
    //Synel web serivce need this header
    if (strcmp(tmp, "true") == 0)
        {
        useSoapActionHeader = true;
		tmp[0] = '\0';
        xmlParser.GetProperty("SoapActionHeader",tmp);
        strcpy(soapActionHeader, tmp); // get the soap action header if required
        }
    else
    {
        useSoapActionHeader = false;

        if (strcmp(LinkWeb, "http://212.179.32.124:446/Clocks/Service.asmx")
            == 0) // connection to the synel default web																					 //service need soap action header.
        {
            useSoapActionHeader = true;
            strcpy(soapActionHeader, "Synel/Services");
        }
    }

//    if (strcmp("true", xmlParser->GetProperty("TurnOnSwipAndGo")) == 0)
//    {
//        TurnOnSwipAndGo = true;
//        DP("swipe and go is TRUE\n");
//        // warning: must have operationiodriver set the static variable from within
//        // sy7000demo.cpp is not able to set any static varaibles within operationiodriver.cpp
//        // sy7000demo.cpp only sees their initialzied values.

//    }
//    else
//    {
//        TurnOnSwipAndGo = false;
//        DP("swipe and go is FALSE\n");
//    }

    xmlParser.GetProperty("UseProxy",useProxy);                   //get if the connection need proxy server
    xmlParser.GetProperty("Proxy",proxy);                         //get proxy server
    xmlParser.GetProperty("ProxyPort",proxyPort);                 //get the proxy server port
    xmlParser.GetProperty("UseProxyAuthentication",useProxyAuth); //get if we need Authentication to the proxy
    xmlParser.GetProperty("ProxyUserName",proxyUserName);         //get proxy user name
    xmlParser.GetProperty("ProxyPassword",proxyPassword);         // get proxy password

    if (strcmp(useProxy, "true") == 0)                                      //set the proxy server parameters
        {
        useProxyBool = true;

        if (strcmp(useProxyAuth, "true") == 0)
            useProxyAuthBool = true;
        else
            useProxyAuthBool = false;
        }
    else
        {
        useProxyBool = false; // if no need to connect through proxy server
        useProxyAuthBool = false;
        }


    // create the http client .
   httpS =
        new HttpClient(LinkWeb, timeOutLong, useProxyBool, proxy, proxyPort, useProxyAuthBool, proxyUserName,
           proxyPassword, useSoapActionHeader, soapActionHeader);
    httpOffline =
        new HttpClient(LinkWeb, timeOutLong, useProxyBool, proxy, proxyPort, useProxyAuthBool, proxyUserName,
            proxyPassword, useSoapActionHeader, soapActionHeader);

    cfgMgr.setHttpServer(httpS, LinkWeb, ClockId, Account); // set cfgMgr internal members
	// 3/9/2010 Fix potential memory link?
}

void SY7000Demo::initFpuConfiguration()
    {
    bool bPropertyEnabledFPU = false;

    XMLProperties xmlParser("properties.xml");
    char tmp[128];

    // check properties
	tmp[0]='\0';
	xmlParser.GetProperty("EnableFpUnit",tmp); 
	
	if(strcmp(tmp,"true") == 0 && FPOperation::InitFpuSuc)
	{
		FPOperation::EnableFpUnit = true;
        bPropertyEnabledFPU = true;
	}
	else
	{
		FPOperation::EnableFpUnit = false;
		bPropertyEnabledFPU = false;
	}

    // check IOD::FPOp (hardware-based value)
    if (FPOperation::EnableFpUnit == false)
    {
        bPropertyEnabledFPU = false;
    }
	else
	{
		if ( bPropertyEnabledFPU == false )
		{
			FPOperation::EnableFpUnit = false; // override hardware from property value
		}
	}

    if (bPropertyEnabledFPU == true)
        {

        tmp[0]='\0';
        xmlParser.GetProperty("FPverification",tmp);

        if (strcmp(tmp, "true") == 0)
            EnableFpVerifyMode = true;
        else
            EnableFpVerifyMode = false;
        }
    else
        {
        EnableFpVerifyMode = false;
        }
    }

void SY7000Demo::initSwipeAndGo()
{
	char turnon[16];

	//LOCKXM;

	DP("initswipeandgo, begin. new parser.\n");
    XMLProperties xmlParser("properties.xml");

	turnon[0] = '\0';
	xmlParser.GetProperty("TurnOnSwipAndGo", turnon);
    //if (strcmp("true", xmlParser->GetProperty("TurnOnSwipAndGo")) == 0)
	if (strcmp("true", turnon ) == 0)
    {
        TurnOnSwipAndGo = true;
        DP("init: swipe and go is TRUE\n");
        OperationIODriver::TurnOnSwipeAndGo = true;
    }
    else
    {
        TurnOnSwipAndGo = false;
        DP("swipe and go is FALSE\n");
        OperationIODriver::TurnOnSwipeAndGo = false;
    }

	//UNLOCKXM;

}

void SY7000Demo::initOfflineMode( OperationIODriver* oper )
    {
    DP("sy7000demo::initofflinemode()\n");
    oper->initOfflineMode();
    }
/*
 initDB:
 Description:

    This function create local sqlite data base tables if not exist , Project Task Table and Activities Table.


 Arguments:
    void-

 Returns:
    void-
*
*/
void SY7000Demo::initDB()
    {
    SqliteDB sqlitedb;
    int ret = sqlitedb.intialise_db();

    if (ret != 0)
        {
        printf("SQL Error : Can't open database - %s", sqlite3_errmsg(db));
        return;
        }
    else
        {
        ret = sqlitedb.create_table(TBL_OffLineTranTable);

        if (ret != SQLITE_OK)
            fprintf(stderr, "SQL error while create offline transaction table table: %s\n", sqlite3_errmsg(db));
        ret = sqlitedb.create_table(TBL_ActivitiesTable); // create activities table if not exist

        if (ret != SQLITE_OK)
            fprintf(stderr, "SQL error while create activities table : %s\n", sqlite3_errmsg(db));
        }
    }


///////////////////////////////////////////////////   Prologic punch operations  ///////////////////////////////////////

//void SY7000Demo::SendSwipePunch( OperationIODriver* oper, ServerDataBaseConnection* SDBConnection )
void SY7000Demo::SendSwipePunch( OperationIODriver* oper, char* SDBConnection )
    {
    //time_t optime;
    //struct tm* optimeinfo;
    //time(&optime);
    //optimeinfo = localtime(&optime);
    bool send_transaction = false;
    //char dateB[22];
    //char timeB[22];
    //dateB[0] = '\0';
    //timeB[0] = '\0';
    //strftime(timeB, 21, "T%H:%M:%S", optimeinfo); // get the local time in format "T%H:%M:%S" for example T10:30:45
    //strftime(dateB, 21, "%Y-%m-%d", optimeinfo);  // get the local date in format "%Y-%m-%d" for example 2008-05-23

    char utcTime[50];
    utcTime[0] = '\0';

	struct tm currTime;
	Operation::SYClock_GetTime(&currTime);
	strftime(utcTime, 20, "%Y-%m-%dT%H:%M:%S", &currTime);
    //strcpy(utcTime, dateB);
    //strcat(utcTime, timeB); // the date and time we send with the request for example "2008-05-23T10:30:45"
    char defaultMessage[21];
    CURLcode res;

    {
        user_output_clear();
        int col;
        col = (20 - strlen("SEND TRANSACTION")) / 2 + 1; //display suitable message while sending in punch tran
        user_output_write(2, col, "SEND TRANSACTION");
        strcpy(defaultMessage, "SWIPE AND GO");

//send in punch request
// if res = 0 we success to send the request if res != 0 send request failed check connection or parameters              //connection
//clock the request mutex to prevent another thread to destroy the response of the current request befor
// we finish and display the correct response result.
        pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
		if(DontSendPunches==false)
		{
        	httpS->SwipePunchWithActivityCode(ClockId, Account, oper->getBadgeBuffer(), NULL, utcTime, SOAP_1_1_VER, &res);
		}
		else
		{
			res = CURLE_OK;
		}
        send_transaction = true; //indicate that we find function and we send transaction
    }

    if (send_transaction)        // there is transaction was sent to the web service , we need to display the response
    {
        if (res || !httpS->chunk.memory)           // failed to send the transcation
        {
            int col;
            user_output_clear();                                  //clear the LCD
            col = (20 - strlen("SEND FAILED")) / 2 + 1;
            user_output_write(2, col, "SEND FAILED");             //display "SEND FAILED" message
            oper->GetOperation()->Beep(TYPE_Error);               // beep error tone
            oper->GetOperation()->TurnOnLEDRed();                 //turn on the red led
            int mtm = oper->GetOperation()->GetErrorMsgTimeout(); // get the time out display message in milliseconds

            if (mtm > 500)
                {
                usleep(500000);
                oper->GetOperation()->Beep(TYPE_TurnOff); // turn off the buzzer after 0.5 second
                mtm -= 500;
                }
            usleep(mtm * 1000);                                           //sleep for time out seconds
            oper->GetOperation()->Beep(TYPE_TurnOff);                     // make sure that we turn off the buzzer
            oper->GetOperation()->TurnOffLEDRed();                        //turn off the red led
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex); // unlock the send request mutex
            return;
        }
        else //if res == 0 we success to send the transaction and we accept response
        {
            // the response saved in the httpS->chunk.memory buffer , if the response have message to display
            // we display it else we display the "defaultMessage" value
			if(DontSendPunches==false)
			{
            	ParseAndDisplayResponse(oper, httpS->chunk.memory, defaultMessage);
			}
			else
			{
				// DontSendPunches==true, accept
                char response[150];
                strcpy(response,
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?><res><Status>1</Status><Message>Punch Test Accepted</Message></res>");
                ParseAndDisplayResponse(oper, response, "INVALID BADGE");
				DP("Punch Test Accepted for %s\n", oper->getBadgeBuffer());
			}
            // unlock the send request mutex
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
            return;
            }
        }
    }

//void SY7000Demo::SendSwipePunchWithActivityCode( OperationIODriver* oper, ServerDataBaseConnection* SDBConnection )
void SY7000Demo::SendSwipePunchWithActivityCode( OperationIODriver* oper, char* SDBConnection )
{
    //time_t optime;
    //struct tm* optimeinfo;
    //time(&optime);
    //optimeinfo = localtime(&optime);
    bool send_transaction = false;
    //char dateB[22];
    //char timeB[22];
    //dateB[0] = '\0';
    //timeB[0] = '\0';
    //strftime(timeB, 21, "T%H:%M:%S", optimeinfo); // get the local time in format "T%H:%M:%S" for example T10:30:45
    //strftime(dateB, 21, "%Y-%m-%d", optimeinfo);  // get the local date in format "%Y-%m-%d" for example 2008-05-23

    char utcTime[50];
    utcTime[0] = '\0';

	struct tm currTime;
	Operation::SYClock_GetTime(&currTime);
	strftime(utcTime, 20, "%Y-%m-%dT%H:%M:%S", &currTime);

    //strcpy(utcTime, dateB);
    //strcat(utcTime, timeB); // the date and time we send with the request for example "2008-05-23T10:30:45"
    char defaultMessage[21];
    bool prevCommValue = false;
    savetranstype_enum savetranstype = otSwipePunch;
    SaveReason savereason = OFFLINE_MODE; // TODO: add to SYxxx or OPxxx Class


    CURLcode res;

    if (oper->getFunctionKey() == 0)
	{

        // note: this may not be necessary:

        savetranstype = otSwipePunch;
        prevCommValue = OperationIODriver::communication;
        user_output_clear();
        int col;
        col = (20 - strlen("SEND TRANSACTION")) / 2 + 1; //display suitable message while sending in punch tran
        user_output_write(2, col, "SEND TRANSACTION");
        strcpy(defaultMessage, "SWIPE AND GO");

        if (OperationIODriver::communication)
            {
//send in punch request
// if res = 0 we success to send the request if res != 0 send request failed check connection or parameters              //connection
//clock the request mutex to prevent another thread to destroy the response of the current request befor
// we finish and display the correct response result.
            pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
			if(DontSendPunches==false)
			{
				httpS->SwipePunchWithActivityCode(ClockId, Account, oper->getBadgeBuffer(), NULL, utcTime, SOAP_1_1_VER,
                &res);
			}
			else
			{
				res = CURLE_OK;
			}
            send_transaction = true; //indicate that we find function and we send transaction
            savereason = SENDIND_FAILED;
            }
        else
            {
            res = (CURLcode)1;                         // force failure
            savereason = OFFLINE_MODE;
            pthread_cond_signal(&heartbeat_cond_flag); // TODO: which condition should be flagged?
            }

        OperationIODriver::communication = prevCommValue;
        }
    else
        {
        for ( int i = 0; i < SysFunctionDetailMax;
            i++ ) //scann the function keys details table  to recognize the function key
            {

            if (SystemSetup.function.detail[i].key == oper->getFunctionKey() && // recognize in punch function
            strcmp(SystemSetup.function.detail[i].message, "SWIPE AND GO") == 0)
                {
                savetranstype = otSwipePunch;

                user_output_clear();
                int col;
                col = (20 - strlen("SEND TRANSACTION")) / 2 + 1; //display suitable message while sending in punch tran
                user_output_write(2, col, "SEND TRANSACTION");
                strcpy(defaultMessage, "SWIPE AND GO");

//send in punch request
// if res = 0 we success to send the request if res != 0 send request failed check connection or parameters              //connection
//clock the request mutex to prevent another thread to destroy the response of the current request befor
// we finish and display the correct response result.
                if (OperationIODriver::communication)
                    {
                    pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
//httpS->SwipePunchWithActivityCode(ClockId,Account,oper->getBadgeBuffer(),NULL, utcTime,SOAP_1_1_VER,&res);
					if(DontSendPunches==false)
					{
						printf("before SipePunch\n");
						httpS->SwipePunch(ClockId, Account, oper->getBadgeBuffer(), utcTime, SOAP_1_1_VER, &res);
						printf("after SipePunch\n");
					}
					else
					{
						res = CURLE_OK;
					}
                    send_transaction = true; //indicate that we find function and we send transaction
                    savereason = SENDIND_FAILED;
                    }
                else
                    {
                    res = (CURLcode)1; // force failure
                    savereason = OFFLINE_MODE;
                    pthread_cond_signal(&heartbeat_cond_flag);
                    }
                }

            if (SystemSetup.function.detail[i].key == oper->getFunctionKey() && // recognize clock in function
            strcmp(SystemSetup.function.detail[i].message, "ACTIVITY CODE") == 0
                && SystemSetup.function.detail[i].level == 1)                   // recognize clock in just in level 1 .
                {
                if (!(checkActivityValidity(oper)))
                    {

                    return;
                    }

                savetranstype = otClockIn;
                user_output_clear();
                int col;
                col = (20 - strlen("SEND TRANSACTION")) / 2 + 1;
                user_output_write(2, col,
                    "SEND TRANSACTION"); //display suitable message while sending out punch function
                strcpy(defaultMessage, "CLOCK IN");

                if (OperationIODriver::communication)
                    {
//clock the request mutex to prevent another thread to destroy the response of the current request befor
// we finish and display the correct response result
                    pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
                    //send clock in request after check if the project code and task code exist in the DB
                    // if res = 0 we success to send the request if res != 0 send request failed check connection or
                    //parameters  connection
                    // --
                    // customized Prologic ClockIn operation
                    // --
					if(DontSendPunches==false)
					{
						//printf("before SwipePunchWithActivityCode\n");
						httpS->SwipePunchWithActivityCode(ClockId, Account, oper->getBadgeBuffer(), oper->levels[0].Data,
                        utcTime, SOAP_1_1_VER, &res);
						//printf("After SwipePunchWithActivityCode\n");
					}
					else
					{
						res = CURLE_OK;
					}
                    send_transaction = true;
                    savereason = SENDIND_FAILED;
                    }
                else
                    {
                    res = (CURLcode)1;                         // force failure
                    savereason = OFFLINE_MODE;
                    pthread_cond_signal(&heartbeat_cond_flag); // TODO: which condition should be flagged?
                }
            }
        } // for
    }

    if (send_transaction)
    {
        if (res || !httpS->chunk.memory) // failed to send the transcation
        {
            if (OperationIODriver::OfflineModeIsEnabled)
            {
                SaveOfflineTransaction(oper, utcTime, savereason, savetranstype);
            }
            else
            {
                int col;
                user_output_clear();                            //clear the LCD
                col = (20 - strlen("SEND FAILED")) / 2 + 1;
                user_output_write(2, col, "SEND FAILED");       //display "SEND FAILED" message
                oper->GetOperation()->Beep(TYPE_Error);         // beep error tone
                oper->GetOperation()->TurnOnLEDRed();           //turn on the red led
                int mtm =
                    oper->GetOperation()->GetErrorMsgTimeout(); // get the time out display message in milliseconds

                if (mtm > 500)
                    {
                    usleep(500000);
                    oper->GetOperation()->Beep(TYPE_TurnOff); // turn off the buzzer after 0.5 second
                    mtm -= 500;
                    }
                usleep(mtm * 1000);                                           //sleep for time out seconds
                oper->GetOperation()->Beep(TYPE_TurnOff);                     // make sure that we turn off the buzzer
                oper->GetOperation()->TurnOffLEDRed();                        //turn off the red led
                pthread_mutex_unlock(&OperationIODriver::soap_request_mutex); // unlock the send request mutex
            }
            return;
        }
        else //if res == 0 we success to send the transaction and we accept response
        {
            // the response saved in the httpS->chunk.memory buffer , if the response have message to display
            // we display it else we display the "defaultMessage" value
			if(DontSendPunches==false)
			{
            	ParseAndDisplayResponse(oper, httpS->chunk.memory, defaultMessage);
			}
			else
			{
				// DontSendPunches==true, accept
                char response[150];
                strcpy(response,
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?><res><Status>1</Status><Message>Punch Test Accepted</Message></res>");
                ParseAndDisplayResponse(oper, response, "INVALID BADGE");
				DP("Punch Test Accepted for %s\n", oper->getBadgeBuffer());
			}
            // unlock the send request mutex
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
            return;
        }
    }
    else
    {
        // no communication. ws-request not sent.

        // transaction not sent. offline?
        if (OperationIODriver::OfflineModeIsEnabled)
        {

            SaveOfflineTransaction(oper, utcTime, savereason, savetranstype);
        }
        else
        {
            // TODO: call parseanddisplay here, instead ?
            int col;
            user_output_clear();                                  //clear the LCD
            col = (20 - strlen("SEND FAILED")) / 2 + 1;
            user_output_write(2, col, "SEND FAILED");             //display "SEND FAILED" message
            oper->GetOperation()->Beep(TYPE_Error);               // beep error tone
            oper->GetOperation()->TurnOnLEDRed();                 //turn on the red led
            int mtm = oper->GetOperation()->GetErrorMsgTimeout(); // get the time out display message in milliseconds

            if (mtm > 500)
            {
                usleep(500000);
                oper->GetOperation()->Beep(TYPE_TurnOff); // turn off the buzzer after 0.5 second
                mtm -= 500;
            }
            usleep(mtm * 1000);                                           //sleep for time out seconds
            oper->GetOperation()->Beep(TYPE_TurnOff);                     // make sure that we turn off the buzzer
            oper->GetOperation()->TurnOffLEDRed();                        //turn off the red led
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex); // unlock the send request mutex
        }
    }
}


/*
* Validate User
*/

bool SY7000Demo::ValidateUser( OperationIODriver* oper )
{

#ifdef USE_ISVALID_EMPLOYEE_OPERATION

    //time_t optime;
    //struct tm* optimeinfo;
    //time(&optime);
    //optimeinfo = localtime(&optime);
    //bool send_transaction = false;
    //char dateB[22];
    //char timeB[22];
    //dateB[0] = '\0';
    //timeB[0] = '\0';
    //strftime(timeB, 21, "T%H:%M:%S", optimeinfo); // get the local time in format "T%H:%M:%S" for example T10:30:45
    //strftime(dateB, 21, "%Y-%m-%d", optimeinfo);  // get the local date in format "%Y-%m-%d" for example 2008-05-23

    char utcTime[50];
    utcTime[0] = '\0';

	struct tm currTime;
	Operation::SYClock_GetTime(&currTime);
	strftime(utcTime, 20, "%Y-%m-%dT%H:%M:%S", &currTime);

    //strcpy(utcTime, dateB);
    //strcat(utcTime, timeB); // the date and time we send with the request for example "2008-05-23T10:30:45"
    char defaultMessage[21];
    CURLcode res;

    user_output_clear();
    //int col;
    //col = ( 20 - strlen("VALIDATE EMPLOYEE") ) / 2 + 1;  //display suitable message while sending in punch tran
    //user_output_write(2, col,"VALIDATE EMPLOYEE");
    strcpy(defaultMessage, "VALIDATED");

    if (OperationIODriver::communication)
    {

// if res = 0 we success to send the request if res != 0 send request failed check connection or parameters              //connection
//clock the request mutex to prevent another thread to destroy the response of the current request befor
// we finish and display the correct response result.
        pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
        httpS->IsValidEmployee(ClockId, Account, oper->getBadgeBuffer(), utcTime, SOAP_1_1_VER, &res);

        if (res || !httpS->chunk.memory) // failed to send the transcation
        {
            int col;
            user_output_clear();                                  //clear the LCD
            col = (20 - strlen("SEND FAILED")) / 2 + 1;
            user_output_write(2, col, "SEND FAILED");             //display "SEND FAILED" message
            oper->GetOperation()->Beep(TYPE_Error);               // beep error tone
            oper->GetOperation()->TurnOnLEDRed();                 //turn on the red led
            int mtm = oper->GetOperation()->GetErrorMsgTimeout(); // get the time out display message in milliseconds

            if (mtm > 500)
                {
                usleep(500000);
                oper->GetOperation()->Beep(TYPE_TurnOff); // turn off the buzzer after 0.5 second
                mtm -= 500;
                }
            usleep(mtm * 1000);                                           //sleep for time out seconds
            oper->GetOperation()->Beep(TYPE_TurnOff);                     // make sure that we turn off the buzzer
            oper->GetOperation()->TurnOffLEDRed();                        //turn off the red led
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex); // unlock the send request mutex
            return false;
        }
        else //if res == 0 we success to send the transaction and we accept response
        {
            // the response saved in the httpS->chunk.memory buffer , if the response have message to display
            // we display it else we display the "defaultMessage" value
            //ParseAndDisplayResponse(oper,httpS->chunk.memory,defaultMessage);
            XMLProperties responseParser; //create xml parser.
            char status[2];
            status[0] = '\0';
            char message[64];
            message[0] = '\0';
            char fnmsg[128];
            fnmsg[0] = '\0';

            bool isvalidemployeeresult = false;

            // true or false is simply pass back from TEAMS web service
            responseParser.GetProperty(httpS->chunk.memory, "IsValidEmployeeResult", status);

            if (strcmp(status, "0") == 0)
                {
                isvalidemployeeresult = false;
                }
            else if (strcasecmp(status, "true") == 0)
                {
                isvalidemployeeresult = true;
                }
            else if (strcasecmp(status, "false") == 0)
                {
                isvalidemployeeresult = false;
                }
            else
                isvalidemployeeresult = false;

            if (isvalidemployeeresult == false)
                {
                char response[150];
                strcpy(response,
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?><res><Status>0</Status><Message></Message></res>");
                ParseAndDisplayResponse(oper, response, "INVALID BADGE");
                }
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex); // unlock the send request mutex
            return isvalidemployeeresult;

            return true;
        }
    } // if comm
    else
    {
        // no comm. accept user (for offline mode, if enabled ? )
        return true;
    }

#else

	return true;

#endif

}

/*
* Validate Pin Entry
*/

bool SY7000Demo::ValidatePinEntry( OperationIODriver* oper )
{

    //time_t optime;
    //struct tm* optimeinfo;
    //time(&optime);
    //optimeinfo = localtime(&optime);
    //bool send_transaction = false;
    //char dateB[22];
    //char timeB[22];
    //dateB[0] = '\0';
    //timeB[0] = '\0';
    //strftime(timeB, 21, "T%H:%M:%S", optimeinfo); // get the local time in format "T%H:%M:%S" for example T10:30:45
    //strftime(dateB, 21, "%Y-%m-%d", optimeinfo);  // get the local date in format "%Y-%m-%d" for example 2008-05-23

    char utcTime[50];
    utcTime[0] = '\0';

	struct tm currTime;
	Operation::SYClock_GetTime(&currTime);
	strftime(utcTime, 20, "%Y-%m-%dT%H:%M:%S", &currTime);

    //strcpy(utcTime, dateB);
    //strcat(utcTime, timeB); // the date and time we send with the request for example "2008-05-23T10:30:45"
    char defaultMessage[21];
    CURLcode res;

    user_output_clear();
    //int col;
    //col = ( 20 - strlen("VALIDATE EMPLOYEE") ) / 2 + 1;  //display suitable message while sending in punch tran
    //user_output_write(2, col,"VALIDATE EMPLOYEE");
    strcpy(defaultMessage, "VALIDATED");

    if (OperationIODriver::communication)
    {

// if res = 0 we success to send the request if res != 0 send request failed check connection or parameters              //connection
//clock the request mutex to prevent another thread to destroy the response of the current request befor
// we finish and display the correct response result.
        pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
        httpS->ValidEmployeePin(ClockId, Account, oper->getBadgeBuffer(), oper->GetPinCode(), utcTime, SOAP_1_1_VER, &res);
		DLOGI("%s %d - Online - Send Valid Emlpoye Pin Request = %s response result = %d", __FUNCTION__,__LINE__,httpS->smg->xml,res);

        if (res || !httpS->chunk.memory) // failed to send the transcation
        {
            int col;
            user_output_clear();                                  //clear the LCD
            col = (20 - strlen("SEND FAILED")) / 2 + 1;
            user_output_write(2, col, "SEND FAILED");             //display "SEND FAILED" message
            oper->GetOperation()->Beep(TYPE_Error);               // beep error tone
            oper->GetOperation()->TurnOnLEDRed();                 //turn on the red led
            int mtm = oper->GetOperation()->GetErrorMsgTimeout(); // get the time out display message in milliseconds

            if (mtm > 500)
                {
                usleep(500000);
                oper->GetOperation()->Beep(TYPE_TurnOff); // turn off the buzzer after 0.5 second
                mtm -= 500;
                }
            usleep(mtm * 1000);                                           //sleep for time out seconds
            oper->GetOperation()->Beep(TYPE_TurnOff);                     // make sure that we turn off the buzzer
            oper->GetOperation()->TurnOffLEDRed();                        //turn off the red led
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex); // unlock the send request mutex
            return false;
        }
        else //if res == 0 we success to send the transaction and we accept response
        {
			DLOGI("%s %d - Online - Send Valid Emlpoye Pin Response = %s", __FUNCTION__,__LINE__,httpS->chunk.memory);
            // the response saved in the httpS->chunk.memory buffer , if the response have message to display
            // we display it else we display the "defaultMessage" value
            //ParseAndDisplayResponse(oper,httpS->chunk.memory,defaultMessage);
            XMLProperties responseParser; //create xml parser.
            char status[2];
            status[0] = '\0';
            char message[64];
            message[0] = '\0';
            char fnmsg[128];
            fnmsg[0] = '\0';

            bool validemployeepinresult = false;

            // true or false is simply pass back from TEAMS web service
            responseParser.GetProperty(httpS->chunk.memory, "ValidEmployeePinResult", status);

            if (strcmp(status, "0") == 0)
                {
                validemployeepinresult = false;
                }
            else if (strcasecmp(status, "true") == 0)
                {
                validemployeepinresult = true;
                }
            else if (strcasecmp(status, "false") == 0)
                {
                validemployeepinresult = false;
                }
            else
                validemployeepinresult = false;

            if (validemployeepinresult == false)
                {
                char response[150];
                strcpy(response,
                    "<?xml version=\"1.0\" encoding=\"utf-8\"?><res><Status>0</Status><Message></Message></res>");
                ParseAndDisplayResponse(oper, response, "INVALID BADGE");
                }
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex); // unlock the send request mutex
            return validemployeepinresult;

            return true;
        }
    } // if comm
    else
    {
        // no comm. accept user (for offline mode, if enabled ? )
		DLOGI("%s %d - Offline - return success no need to validition",__FUNCTION__,__LINE__);
        return true;
    }


	return true;


}

/*
ParseAndDisplayResponse:

    This function parse the xml response format and display the message if exist if no it's display
    default massege .

Description:

    This function parse the xml response format and display the message if exist if no it's display
    default massege .

Arguments:
    oper - pointer to operation IO object to access the LCD ,Buzzer & LEDs...
    response - pointer to the buffer the contain the xml response .
    defaultMessage - the default message we display in case no message to display in the response

Returns:
    void

*/

void SY7000Demo::ParseAndDisplayResponse( OperationIODriver* oper, char* response, char* defaultMessage )
    {

    int col;
    col = (20 - strlen(defaultMessage)) / 2 + 1;
    char fnmsg[21];
    XMLProperties responseParser; //create xml parser.
    char status[2];
    status[0] = '\0';
    char message[64];
    message[0] = '\0';
    responseParser.GetProperty(response, "Status", status);   // get the response status element
    responseParser.GetProperty(response, "Message", message); // get the message from the response

    if (strcmp(status, "0")
        == 0) // if status == 0 , no permission  for the user to the transaction
        {
        strcpy(fnmsg, "ACCESS DENIED");                       //later we display "ACCESS DENIED" message
        col = (20 - strlen(fnmsg)) / 2 + 1;
        }

    if (!(strcmp(status, "1")))                  // if status == 0 , transaction was performed.
        {
        oper->GetOperation()->TurnOnLEDGreen();  // tunn on green led .
        oper->GetOperation()->Beep(TYPE_Accept); // beep accept tone buzzer .
        }
    else
        {
        oper->GetOperation()->TurnOnLEDYellow();            // if status == 0 turn on yellow led.
        oper->GetOperation()->Beep(TYPE_Reject);            // beep reject tone buzzer.
        }

    if (strlen(status) > 0 && strlen(message) > 0)          // if we accept message in the response .
        {
        cfgMgr.MenuFromBuffer("EMPLOYEE MESSAGE", message); // we use MenuFromBuffer method to display the message
        // with the ability turning pages in case large message
        }
    else // in case no message or no status response
        {
        user_output_clear();

        if (strlen(status) == 0)                 // if no status we get exception response
            {
            strcpy(fnmsg, "EXCEPTION RESPONSE"); // display "EXCEPTION RESPONSE"
            col = (20 - strlen(fnmsg)) / 2 + 1;
            user_output_write(2, col, fnmsg);
            char trymsg[21];
            int lenTryMsg = oper->GetOperation()->GetPromptMessage(TYPE_Try_Again,trymsg,21);
            int col1 = (20 - lenTryMsg) / 2 + 1;
            // display "PLEASE TRY AGAIN"
            user_output_write(3, col1,trymsg);
            }
        else // if there is status and no message display the default message
            {
            if (defaultMessage && strlen(defaultMessage) > 0)
                {
                cfgMgr.MenuFromBuffer("EMPLOYEE MESSAGE", defaultMessage);
                }
            else
                {
                col = (20 - strlen(fnmsg)) / 2 + 1;
                user_output_write(2, col, fnmsg);
                }
            }
        int mtm = oper->GetOperation()->GetMessageTimeout(); // display message for time out seconds

        if (mtm > 500)
            {
            //usleep(500000);
            usleep(50000);
            oper->GetOperation()->Beep(TYPE_TurnOff);
            mtm -= 500;
            }
        //usleep(mtm * 1000);
        usleep(mtm * 500);
        }
    user_output_clear();                      // clean the LCD
    oper->GetOperation()->Beep(TYPE_TurnOff); // turn off the buzzer
    oper->GetOperation()->TurnOffLEDGreen();  // trun off the green LED
    oper->GetOperation()->TurnOffLEDYellow(); // trun off the yellow LED
    }

bool SY7000Demo::checkActivityValidity( OperationIODriver* oper )
{
   // SqliteDB sqlitedb;
   // char whereActivity[30]; // buffer of the sql queue

    //sprintf(whereActivity,"PROJECTCODE = '%s' AND TASKCODE = '%s'",oper->levels[0].Data,oper->levels[1].Data);
    //sprintf(whereActivity, "ACTIVITYCODE = '%s'", oper->levels[0].Data);

    char activityName[100];activityName[0] = '\0';
    //printf("%s %d\n",__FUNCTION__,__LINE__);
    //pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
    XML_FindActivity("Activities.xml",atoi(oper->levels[0].Data),activityName);
    //pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
    //printf("%s %d activityName = %s\n",__FUNCTION__,__LINE__,activityName);
    /*if (!(sqlitedb.activities_tbl.FindRowCount(whereActivity)))*/ // check if the activitiy exist in the local DB
    if(activityName[0] == '\0') // check if the activitiy exist in the xml file
    {
        user_output_clear();                                    //clear the LCD
        int col1 = (20 - strlen("INVALID ACTIVITY")) / 2 + 1;   // activity not exist in DB dispaly message
        user_output_write(2, col1, "INVALID ACTIVITY");
        //display try agian message
        char trymsg[21];
        int lenTryMsg = oper->GetOperation()->GetPromptMessage(TYPE_Try_Again,trymsg,21);
        col1 = (20 - lenTryMsg) / 2 + 1;
        user_output_write(3, col1, trymsg);
        oper->GetOperation()->TurnOnLEDYellow();
        oper->GetOperation()->Beep(TYPE_Reject);
        int mtm = oper->GetOperation()->GetMessageTimeout();

        if (mtm > 500)
            {
            usleep(500000);
            oper->GetOperation()->Beep(TYPE_TurnOff);
            mtm -= 500;
            }
        usleep(mtm * 1000);
        oper->GetOperation()->Beep(TYPE_TurnOff);
        oper->GetOperation()->TurnOffLEDYellow();
        return false;
        }

    DLOGI("%s %d Activity Name = %s",__FUNCTION__,__LINE__,activityName);
    return true;
    }


/*
RunHeartBeatThread:

    This function create thread to send heart beat and check if there connection between the clock and the web service
    the executing start_routine  we supply to the thread is RunHeartBeatFunction.

Description:

    This function create thread to send heart beat and check if there connection between the clock and the web service
    the executing start_routine  we supply to the thread is RunHeartBeatFunction.


Arguments:
     void-

Returns:
    void-

*/

bool get_server_time_flag = true;
bool reset_http_client = false;

void SY7000Demo::RunHeartBeatThread()
    {
    pthread_t HeartBeatThread;
    pthread_attr_t HeartBeatThreadAttr;

    unsigned int s;
    pthread_attr_init(&HeartBeatThreadAttr);
    pthread_attr_getstacksize(&HeartBeatThreadAttr, &s);
    //pthread_attr_setstacksize ( & HeartBeatThreadAttr , const_thread_stack_size ());
    pthread_attr_setstacksize(&HeartBeatThreadAttr, PTHREAD_STACK_MIN + 16384);

	{
		int tpolicy;
		struct sched_param param;

		tpolicy = SCHED_FIFO;
		param.sched_priority = SY_PRIO_LOW;

		pthread_attr_setinheritsched(&HeartBeatThreadAttr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&HeartBeatThreadAttr, tpolicy);
		pthread_attr_setschedparam(&HeartBeatThreadAttr, &param);

	}


    int ret = 0;
    // create thread to send heart beat and check if there connection between the clock and the web service
    ret = pthread_create(&HeartBeatThread, &HeartBeatThreadAttr, RunHeartBeatFunction, NULL);

    if (ret != 0)
        {
        if (ret == EAGAIN)
            printf("HeartBeat Thread : Out of Memory\n");

        else if (ret == EINVAL)
            printf("HeartBeat Thread : Invalid Attribute\n");
        printf("HeartBeat Thread : Failed to Create HeartBeat Thread\n");
        }
    }


#define	MATCH(line, name) \
	(!strncmp(line, name, sizeof(name) - 1) && \
	(line[sizeof(name) - 1] == ' ' || \
	 line[sizeof(name) - 1] == '\t'))

void update_nameservers()
{
	FILE *fp;
	char *cp;
	char buf[MAXDNAME];
	int nserv = 0;    /* number of nameserver records read from file */

	if ((fp = fopen("/etc/resolv.conf", "r")) != NULL)
	{
	    /* read the config file */
	    while (fgets(buf, sizeof(buf), fp) != NULL)
	    {
		/* skip comments */
		if (*buf == ';' || *buf == '#')
			continue;
		/* read nameservers to query */
		if (MATCH(buf, "nameserver") && nserv < MAXNS)
		{
		    char *q;
		    struct addrinfo hints, *res;
		    char pbuf[NI_MAXSERV];

		    cp = buf + sizeof("nameserver") - 1;
		    while (*cp == ' ' || *cp == '\t')
			cp++;
		    if ((*cp == '\0') || (*cp == '\n'))
			continue;
		    for (q = cp; *q; q++) {
			if (isspace(*q)) {
			    *q = '\0';
			    break;
			}
		    }
		    memset(&hints, 0, sizeof(hints));
		    hints.ai_flags = AI_NUMERICHOST;
		    hints.ai_socktype = SOCK_DGRAM;
		    snprintf(pbuf, sizeof(pbuf), "%d", NAMESERVER_PORT);
		    if (getaddrinfo(cp, pbuf, &hints, &res) == 0 &&
			    res->ai_next == NULL)
		   {
			if (res->ai_addrlen <= sizeof(_res.nsaddr_list[nserv])) {
			    memcpy(&_res.nsaddr_list[nserv], res->ai_addr,
				res->ai_addrlen);
			} else {
			    memset(&_res.nsaddr_list[nserv], 0,
				sizeof(_res.nsaddr_list[nserv]));
			}
			if (res->ai_addrlen <= sizeof(_res.nsaddr_list[nserv])) {
			    memcpy(&_res.nsaddr_list[nserv], res->ai_addr,
				res->ai_addrlen);
			} else {
			    memset(&_res.nsaddr_list[nserv], 0,
				sizeof(_res.nsaddr_list[nserv]));
			}
			nserv++;
		    }
		    if (res)
			    freeaddrinfo(res);
		    continue;
		}
	     }
	}
	if (nserv > 1)
		_res.nscount = nserv;
}

/*
RunHeartBeatFunction:

    This function implement the executing start_routine  we supply to the heart beat thread , it's send heartbeat to the	web service and check if we accept response with permission status to access the web service and set the
    communication flag in the OperationIODriver class to true , else the communication flag set to false   .

Description:

    This function implement the executing start_routine  we supply to the heart beat thread , it's send heartbeat to the	web service and check if we accept response with permission status to access the web service and set the
    communication flag in the OperationIODriver class to true , else the communication flag set to false   .


Arguments:
     void-

Returns:
    void-

*/

void *SY7000Demo::RunHeartBeatFunction( void* id )
{
	printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());
    int firstTimes = 0;

    bool firstHeartBeatOn = false;
    bool firstHeartBeatOnForTemplates = false;
    //char HearBeatFreqStr[15];

    //get the time and date in format "%Y-%m-%dT%H:%M:%S"
    time_t optime;
    struct tm* optimeinfo;
    char dateB[22];
    char timeB[22];
    char utcTime[50];

    //XMLProperties parser;
    XMLProperties parser("/tmp/HeartBeat.xml");
	char status[2];

	//HearBeatFreqStr[0] = '\0';
	status[0] = '\0';

    //int hbi = 101;
	int hbi = 10; // change to check every 10 seconds
    int hbc = 10; // check heartbeat after 'hbc' iterations
    CURLcode res = (CURLcode)1;


    while (true)
    {
        // init the result of heart beat send .
        res = (CURLcode)1;

        //get the time and date in format "%Y-%m-%dT%H:%M:%S"
        //time(&optime);
        //optimeinfo = localtime(&optime);

        //dateB[0] = '\0';
        //timeB[0] = '\0';
        //strftime(timeB, 21, "T%H:%M:%S", optimeinfo);
        //strftime(dateB, 21, "%Y-%m-%d", optimeinfo);

        utcTime[0] = '\0';
		struct tm currTime;
		Operation::SYClock_GetTime(&currTime);
		strftime(utcTime, 20, "%Y-%m-%dT%H:%M:%S", &currTime);

        //strcpy(utcTime, dateB);
        //strcat(utcTime, timeB);

        // clock the soap mutex request to send heartbeat.
 again:


        if(reset_http_client)
        {

        	reset_http_client = false;
        }
    //LOCKXM;
		//DP("%s %d before HeartBeat time out = %d",__FUNCTION__,__LINE__,httpS->getTimeOut());

        unlink("/tmp/HeartBeat.xml");
        FILE * hFile = fopen("/tmp/HeartBeat.xml", "w");
        if ( hFile == NULL )
        {
        	DLOGE("Get HeartBeat: could not open fp file for save HeartBeat Response.");
        	usleep(heartbeatFrequency);
        	continue;
        }
        else
        {
        	DLOGI("%s %d pthread_mutex_lock(&OperationIODriver::soap_request_mutex)", __FUNCTION__,__LINE__);
        	pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
        	//if(httpS->count > 100)
        	//{
        		//DLOGN("%s %d reset httpS counter", __FUNCTION__,__LINE__);
        		//httpS->count = 0;
        		//httpS->resetHttp();
        	//}
        	//DLOGN("%s %d before Send Heart Beat)", __FUNCTION__,__LINE__);
        	httpS->HeartBeatFile(ClockId, Account, utcTime, SOAP_1_1_VER, &res,hFile);
        	//DLOGN("%s %d after Send Heart Beat res = %d)", __FUNCTION__,__LINE__,res);
        	//printf("%s %d after Send Heart Beat res = %d \n", __FUNCTION__,__LINE__,res);
        	fclose(hFile);
        }

		//DP("%s %d after HeartBeat",__FUNCTION__,__LINE__);
    //UNLOCKXM;


        if(res == 6)
        {
        	update_nameservers();
        }

        hbi++;

        //if (!res && httpS->chunk.memory) // if succeed to send hearbeat
        if (!res)
		{

			//printf("%s %d after HeartBeat response %s\n",__FUNCTION__,__LINE__,httpS->chunk.memory);

///
			// move the decl up?
            // create xml parser to parse the response .
//            XMLProperties parser;
//            char status[2];
            status[0] = '\0';
///
    //LOCKXM;
            int resPars = parser.GetProperty("Status", status); // get status heartbeat response
    //UNLOCKXM;
			
            DLOGI("%s %d succeed to send hearbeat: res = %d Status=%s\n", __FUNCTION__,__LINE__,res,status);
            if (resPars && strlen(status) > 0)
            {
				//printf("%s %d\n",__FUNCTION__,__LINE__);
                // if status = 1 there is permission to the clock to connect the web service
                if (strcmp(status, "1") == 0)
                {
                    //printf("Heartbeat is found\n");
					if ( OperationIODriver::communication == false )
					{
						//DP("Heartbeat: turn communication flag ON\n");
						DLOGI("%s %d Heartbeat: turn communication flag ON",__FUNCTION__,__LINE__);
					}
                    OperationIODriver::communication = true;

                    firstTimes = 3;

                    if(get_server_time_flag)
                    {
                    	SY7000Demo::staticPointer->GetServerTimeFunction();
                    	get_server_time_flag = false;
                    }

                    if(!firstHeartBeatOn)
                    {
                    	DLOGI("%s %d RunGetActivitiesThread", __FUNCTION__,__LINE__);
                    	#ifdef USE_GETACT_SINGLETHREAD_RUN
                                       SY7000Demo::staticPointer->RunGetActivitiesThread();		// run and die
                    	#else
                                        SY7000Demo::staticPointer->RunGetActivitiesThread();		// run and sleep until wakeup
                    	#endif
                        firstHeartBeatOn = true;
                    }
                    if(!firstHeartBeatOnForTemplates)
                    {
                    	if (FPOperation::EnableFpUnit == true)
                    	{
                    		if ( OperationIODriver::AllowPopulateTemplates )
                    		{
                    			SY7000Demo::staticPointer->RunPopulateTemplatesThread();	// this just starts and waits for signal before first cycle

                    		}
                    		else
                    		{
                    		   DLOGI("%s %d Allow Populate Templates is false.",__FUNCTION__,__LINE__);
                    		}
                    	}
                    	else
                    	{
                    		DLOGI("%s %d No Populate Templates FPU Disabled.",__FUNCTION__,__LINE__);
                    	}
                    	firstHeartBeatOnForTemplates = true;
                    }
                    if (offline_thread_is_started == 2)
					{
                    	 DLOGI("%s %d offline_thread_is_started == 2", __FUNCTION__,__LINE__);
						 pthread_cond_signal(&offline_cond_flag);
					}
                    //printf("HeartBeat: OfflineSender SIGNALED.\n");
                }
                else // no permission to the clock to connect the web service
                {
                    DLOGI("%s %d hb: Found but no permission Status != 1",__FUNCTION__,__LINE__);

                    OperationIODriver::communication = false;
                }
            }
            else
            {
            	if(!resPars)DLOGI("%s %d Failed to parse the HeartBeat Xml File",__FUNCTION__,__LINE__);
            	DLOGI("%s %d hb: No Status Value Found",__FUNCTION__,__LINE__);
                OperationIODriver::communication = false;

                if (hbi > hbc)
                {
                    //					DP("Send heartbeat successful, but no permission. : res = %d\nset comm = false\n", res);
                    hbi = 0;
                }
            }
        }
        else // failed to send heart beat
        {
        	DLOGI("%s %d failed to send heartbeat: res = %d", __FUNCTION__,__LINE__,res);
			//printf("%s %d\n",__FUNCTION__,__LINE__);
            if (hbi > hbc)
            {
                DLOGI("%s %d failed to send heartbeat: res = %d", __FUNCTION__,__LINE__,res);
                hbi = 0;
                //				DP("hb: sleep for 1 seconds\n");
                //sleep(1); // sleep , so retries at connecting web services do not clog
                usleep(1000000);
            }


			if ( OperationIODriver::communication == true )
			{
				//DP("Heartbeat: turn communcation flag OFF\n");
				DLOGI("%s %d Heartbeat: turn communcation flag OFF",__FUNCTION__,__LINE__);
			}

            OperationIODriver::communication = false;

            if (firstTimes < 3 && (httpS->getTimeOut()) < 16)
            {
                firstTimes++;
                //		DP("hb: unlock soap mutex; firsttime is %d and < 3\n", firstTimes);


				DLOGI("%s %d HeartBeat: firstTimes is %d && (httpS->getTimeOut()) < 16",__FUNCTION__,__LINE__,firstTimes);

                pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
                //sleep(1);
                //				DP("hb: goto again\n");
                usleep(1000000);
                goto again;
            }
            DLOGI("%s %d failed to send heartbeat: res = %d", __FUNCTION__,__LINE__,res);
    	}
        //		DP("hb: unlock soap mutex\n");
        pthread_mutex_unlock(&OperationIODriver::soap_request_mutex); // unlock the soap mutex request.
        //DLOGI("%s %d usleep heartbeatFrequency = %d", __FUNCTION__,__LINE__,heartbeatFrequency);
        usleep(heartbeatFrequency);
        //usleep(500000);
    }
}

void SY7000Demo::RunGetActivitiesThread()
{

    pthread_t GetActivitiesThread;
    pthread_attr_t GetActivitiesThreadAttr;

    unsigned int s;
    pthread_attr_init(&GetActivitiesThreadAttr);
    pthread_attr_getstacksize(&GetActivitiesThreadAttr, &s);
    //pthread_attr_setstacksize ( & GetActivitiesThreadAttr , const_thread_stack_size ());
    pthread_attr_setstacksize(&GetActivitiesThreadAttr, PTHREAD_STACK_MIN + 65536);

	{
		int tpolicy;
		struct sched_param param;

		tpolicy = SCHED_FIFO;
		param.sched_priority = SY_PRIO_LOW;

		pthread_attr_setinheritsched(&GetActivitiesThreadAttr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&GetActivitiesThreadAttr, tpolicy);
		pthread_attr_setschedparam(&GetActivitiesThreadAttr, &param);

	}

    int ret = 0;
    ret = pthread_create(&GetActivitiesThread, &GetActivitiesThreadAttr, RunGetActivitiesFunction, NULL);

    if (ret != 0)
        {
        if (ret == EAGAIN)
            printf("GetActivities Thread : Out of Memory\n");

        else if (ret == EINVAL)
            printf("GetActivities Thread : Invalid Attribute\n");
        printf("GetActivities Thread : Failed to Create Get Activities Thread\n");
        }
}

void *SY7000Demo::RunGetActivitiesFunction( void* id )
{

    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());

    CURLcode res = (CURLcode)1;
	FILE * actFile;
	int firstTime = true;

    while (true)
    {
        if (OperationIODriver::OfflineModeIsActive == false)
        {

            res = (CURLcode)1;

            if (OperationIODriver::communication)
            {
                // clock the soap mutex request to send heartbeat.
                DP("getact: get lock on offline_request_mutex...");
                pthread_mutex_lock(&(OperationIODriver::offline_request_mutex));
                DP("getact: offline_request_mutex locked. GetActivities()");


                //unlink("Activities.xml");
				//DLOGI("Previous httpfp.tmp file removed");

				actFile = fopen("Activities.xml", "w");
				if ( actFile == NULL )
				{
					//TODO: could not open file
					//DP("testing: RunPopulateTemplates could not open fp file for writing\n");
					DLOGE("Get Activities: could not open fp file for save activities.");
				}
				else
				{
					DP("getact: Send GetActivities Request");
					httpOffline->GetActivitiesFile(ClockId, Account, SOAP_1_1_VER, &res,actFile,60); // send get project tasks web service
					DP("getact: GetActivities Request Finished");
				    DLOGI("%s %d GetActivities response result = %d",__FUNCTION__,__LINE__,res);
				    fclose(actFile);
				    sync();
				}

				if (!res)
				{
					DLOGI("%s %d Get Activities Success",__FUNCTION__,__LINE__);
				}
				else
				{
					DLOGI("%s %d Get Activities Failed res = %d",__FUNCTION__,__LINE__,res);
				}
                DP("getact: offline_request_mutex unlocked. GetActivities()");
                pthread_mutex_unlock(&(OperationIODriver::offline_request_mutex));
            }
            else
            {
                // wait for communication
                DP("getact: no comm. just goto sleep\ngetact");
                DP("getact: goto sleep...");

				SY7000Demo::ok_for_next_thread = true;

                //TODO: convert to wait/signal model (check in idle screen loop / or use timedwait)
                sleep(heartbeatFrequency * 10); // if we are in disconnect sleep for heart beat timer , and after try again
                DP("getact: wake up...");

                continue;
            }
        } // offline mode is actively performing tasks, wait unit it is done
        else
        {
			SY7000Demo::ok_for_next_thread = true;

			sleep(heartbeatFrequency * 2); // if we are in disconnect sleep for heart beat timer , and after try again
            continue;
        }

        if (firstTime && OperationIODriver::OfflineModeIsEnabled)
        {
        	SY7000Demo::staticPointer->RunOfflineSenderThread();
            firstTime = false;
        }
        DP("getact: reached this point...");

		SY7000Demo::ok_for_next_thread = true;

#ifdef USE_GETACT_SINGLETHREAD_RUN
		DP("GetActivities thread finished.");
		//pthread_exit(NULL);
		return NULL;
		//DP("GetActivities thread call pthread_exit(NULL) done.");
#else
        sleep(getActivitiesFrequency); // we success to get project tasks sleep for get project tasks timer.
        DP("getact: wake up 2");
#endif
    }



}

void SY7000Demo::DoGetActivitiesFunction()
{

	// perform the operation if there is communication, or just skip it

    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());

    CURLcode res = (CURLcode)1;
    FILE * actFile;
        if (OperationIODriver::OfflineModeIsActive == false)
        {
            res = (CURLcode)1;
            if (OperationIODriver::communication)
            {
            	// clock the soap mutex request to send heartbeat.
            	DP("DoGetActivitiesFunction: get lock on offline_request_mutex...");
            	pthread_mutex_lock(&(OperationIODriver::offline_request_mutex));
            	DP("DoGetActivitiesFunction: offline_request_mutex locked. GetActivities()");


            	//unlink("Activities.xml");
               //DLOGI("Previous httpfp.tmp file removed");

            	actFile = fopen("Activities.xml", "w");
            	if ( actFile == NULL )
            	{
            			//TODO: could not open file
            			//DP("testing: RunPopulateTemplates could not open fp file for writing\n");
            			DLOGE("DoGetActivitiesFunction: could not open fp file for save activities.");
            	}
            	else
            	{
            			DP("DoGetActivitiesFunction: Send GetActivities Request");
            			httpOffline->GetActivitiesFile(ClockId, Account, SOAP_1_1_VER, &res,actFile,60); // send get project tasks web service
            			DP("DoGetActivitiesFunction: GetActivities Request Finished");
            			DLOGI("%s %d GetActivities response result = %d",__FUNCTION__,__LINE__,res);
            			fclose(actFile);
            	        sync();
            	}
            	if (!res)
            	{
            		DLOGI("%s %d Get Activities Success",__FUNCTION__,__LINE__);
            	}
            	else
            	{
            		DLOGI("%s %d Get Activities Failed res = %d",__FUNCTION__,__LINE__,res);
            	}
            	DP("DoGetActivitiesFunction: offline_request_mutex unlocked. GetActivities()");
            	pthread_mutex_unlock(&(OperationIODriver::offline_request_mutex));
            }
            else
            {
                // wait for communication
                DP("%s getact: no comm. skip\ngetact: unlock on soap mutex...",__FUNCTION__);
                //pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);

				SY7000Demo::ok_for_next_thread = true;
                // no, just skip it. XcontinueX;
            }
        } // offline mode is actively performing tasks, wait unit it is done
        else
        {

            //pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
			SY7000Demo::ok_for_next_thread = true;
            // no, just skip it. Xcontinue;
        }

        DP("%s reached this point...",__FUNCTION__);


		SY7000Demo::ok_for_next_thread = true;

}


int SY7000Demo::UpdateActivitiesInDB( const char *actXmlList )
{
    //printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
    XMLProperties parser;
    int from, to, id;
    SqliteDB sqlitedb;
    list<int> savedActivities;

	int countElm;
	ActivityRecord R;

	list<ElmData> &actList = parser.getListElementData();

    from = 1;
    to = 100;

    FILE * actFile = fopen ("Activities.xml","w");
    if(actFile)
    {
    	fprintf (actFile, "%s",actXmlList);
    	fclose(actFile);
    }

    do
    {
        parser.GetListElements(actXmlList, "GetActivitiesResult", "Activity", from, to);
        list<ElmData>::iterator i;
        countElm = 0;

        //	printf("befor for size = %d from = %d to = %d \n",actList.size(),from,to);
        for ( i = actList.begin(); i != actList.end(); )
        {
            //ActivityRecord R;
            fromActivitiesListToRecord(actList, i, R);

            if (sqlitedb.activities_tbl.InsertRow(&R,&id) == SQLITE_OK)
            {

                //id = sqlitedb.activities_tbl.getID(&R);

                if (id > 0)
                    savedActivities.push_back(id);
            }
            else
                return -1;
           countElm++;
        }
        from = from + countElm;
        to = from + (100-1);
        //usleep(200000);
    } while ( countElm == 100 );

    int MaxID = sqlitedb.activities_tbl.getMaxID();
	MaxID = sqlitedb.activities_tbl.getMaxID();
    valarray<int> deletedActivities(true, MaxID + 1);
    list<int>::iterator itr;

    for ( itr = savedActivities.begin(); itr != savedActivities.end(); ++itr )
        deletedActivities[*itr] = false;

    for ( int i = MaxID; i > 0; i-- )
        if (deletedActivities[i])
            sqlitedb.activities_tbl.DeleteRow(i);
    return 1;
}

void SY7000Demo::fromActivitiesListToRecord( list<ElmData> &actList, list<ElmData>::iterator &i, ActivityRecord &R )
    {
    int actIndex = (*i).elmIndexInList;

    while (actIndex == (*i).elmIndexInList)
        {
        if (strcmp("Name",(*i).key) == 0)
            R.SetActivityName((*i).data);

        if (strcmp("Code",(*i).key) == 0)
            R.SetActivityCode((*i).data);

        if (i == actList.end())
            return;
        i++;
        }
    }


/*=============================================================================================================*/

int SY7000Demo::SaveTransaction( OffLineTranRecord* R, SaveReason sr )
    {

    //printf("entering SaveTransaction. Validate record.\n");

    /*
      const char* GetTransaction() const		{return Transaction; 	};
      const char* GetTime() const 		{return Time; 	};
      const char* GetEmployeeId() const		{return EmployeeId; };
      const char* GetGroup() const	{return Group; 	};
      const char* GetProject() const	{return Project; 	};
      const char* GetTask() const	{return Task; 	};
      const char* GetActivity() const	{return Activity; 	};
      const char* GetProjectSwitch() const	{return ProjectSwitch; 	};
     */
    //printf("ST: is Transaction null? %d\n", (R->GetTransaction()==NULL));
    //printf("ST: is Time null? %d\n", (R->GetTime()==NULL));
    //printf("ST: is EmployeeId null? %d\n", (R->GetEmployeeId()==NULL));
    //printf("ST: is Group null? %d\n", (R->GetGroup()==NULL));
    //printf("ST: is Project null? %d\n", (R->GetProject()==NULL));
    //printf("ST: is Task null? %d\n", (R->GetTask()==NULL));
    //printf("ST: is Activity null? %d\n", (R->GetActivity()==NULL));
    //printf("ST: is ProjectSwitch null? %d\n", (R->GetProjectSwitch()==NULL));

    int col1, col2;
    SqliteDB sqlitedb;
    Operation* oper = new Operation();

    if (sr == OFFLINE_MODE)
        {
        //			col1 = ( 20 - strlen("OFFLINE MODE") ) / 2 + 1;
        //			user_output_clear();
        //			user_output_write(2, col1,"OFFLINE MODE");
        }

    if (sr == SENDIND_FAILED)
        {
        col1 = (20 - strlen("SEND FAILED")) / 2 + 1;
        user_output_clear();
        user_output_write(2, col1, "SEND FAILED");
        }

    if (sr == GETPROJECTTASKS_FAILED)
        {
        if (sqlitedb.offlineTran_tbl.InsertRow(R) == SQLITE_OK)
            return 1;
        else
            return 0;
        }

    //printf("offline mode: InsertRow\n");

    if (sqlitedb.offlineTran_tbl.InsertRow(R) == SQLITE_OK)
        {

        //printf("offline mode: InsertRow success\n");

        //			col2 = ( 20 - strlen("TRANSACTION SAVED") ) / 2 + 1;
        //		    user_output_write(3, col2,"TRANSACTION SAVED");
        //			oper->TurnOnLEDGreen();
        //			oper->Beep(TYPE_Accept);
        //			int mtm = oper->GetMessageTimeout();
        //			if(mtm > 500)
        //			{
        //				usleep(500000);
        //				oper->Beep(TYPE_TurnOff);
        //				mtm -= 500;
        //			}
        //			usleep(mtm * 1000);
        oper->Beep(TYPE_TurnOff);
        oper->TurnOffLEDGreen();
        return 1;
        }
    else
        {

        printf("offline mode: InsertRow FAILED\n");

        col2 = (20 - strlen("TRANSACTION LOST")) / 2 + 1;
        user_output_write(3, col2, "TRANSACTION LOST");
        oper->TurnOnLEDYellow();
        oper->Beep(TYPE_Error);
        int mtm = oper->GetMessageTimeout();

        if (mtm > 500)
            {
            usleep(500000);
            oper->Beep(TYPE_TurnOff);
            mtm -= 500;
            }
        usleep(mtm * 1000);
        oper->Beep(TYPE_TurnOff);
        oper->TurnOffLEDYellow();
        return 0;
        }
    }


void setFingerParam()
{
    if(uf_cancel() != UF_RET_SUCCESS)
	{
		printf("uf_cancel 1 filed\n");
		sleep(1);
		if(uf_cancel() != UF_RET_SUCCESS)
		{
			printf("uf_cancel 2 filed\n");
			sleep(1);
			if(uf_cancel() != UF_RET_SUCCESS)
				printf("uf_cancel 3 filed\n");
		} 
	}
 
	//fpOpr.WriteSystemParameter(UF_SYS_SENSITIVIY, 0x37);	//highest sensitivity
	XMLProperties xmlParser("properties.xml");
	char temp[3];temp[0]='\0';
	xmlParser.GetProperty("SendScanScuccess",temp);
	if(strcmp(temp,"Y")==0)
          fpOpr.WriteSystemParameter(UF_SYS_SEND_SCAN_SUCCESS, 0x31); //ON
	else
		  fpOpr.WriteSystemParameter(UF_SYS_SEND_SCAN_SUCCESS, 0x30); //OFF
	//fpOpr.WriteSystemParameter(UF_SYS_MATCHING_TIMEOUT,0x3A); //timeout = 10 sec;
	//fpOpr.WriteSystemParameter(UF_SYS_TIMEOUT, 0x3B);//timeout = 11 sec
	fpOpr.WriteSystemParameter(UF_SYS_AUTO_RESPONSE, 0x30);// Auto Response = OFF
	fpOpr.WriteSystemParameter(UF_SYS_FREE_SCAN, 0x30);// free scan = OFF
  
	if(uf_save_system_parameter() != UF_RET_SUCCESS)
	{
		sleep(1);
		if(uf_save_system_parameter() != UF_RET_SUCCESS)
		{
			sleep(1);
			if(uf_save_system_parameter() != UF_RET_SUCCESS)
				printf("save parameters failed\n");
		}
	}

}

/*
RunFingerPrintThread:

Description:

    This function create thread to scan finger print template in the idle display and identify the user. .

Arguments:
    OperationIODriver * oper - operation object to access the LCD ,Buzzer & LEDs...

Returns:
    void-

*/

void SY7000Demo::RunFingerPrintThread( OperationIODriver* oper )
{
    pthread_t FingerPrintThread;
    pthread_attr_t FingerPrintThreadAttr;

    unsigned int s;
    pthread_attr_init(&FingerPrintThreadAttr);
    pthread_attr_getstacksize(&FingerPrintThreadAttr, &s);
    //pthread_attr_setstacksize ( & FingerPrintThreadAttr , const_thread_stack_size ());
    pthread_attr_setstacksize(&FingerPrintThreadAttr, PTHREAD_STACK_MIN + 16384);

	{
		int tpolicy;
		struct sched_param param;

		tpolicy = SCHED_FIFO;
		param.sched_priority = SY_PRIO_HIGH;

		pthread_attr_setinheritsched(&FingerPrintThreadAttr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&FingerPrintThreadAttr, tpolicy);
		pthread_attr_setschedparam(&FingerPrintThreadAttr, &param);

	}


    int ret = 0;

    // if the FPU in enable state
    if (FPOperation::EnableFpUnit)
    {
		setFingerParam();
        //create thread and execute FingerPrintFunction function
		ret = 0;
		//no need finger thread in case we work just in verify mode
        //ret = pthread_create(&FingerPrintThread, &FingerPrintThreadAttr, FingerPrintFunction, (void *)oper);
        
        if (ret != 0)
        {
            if (ret == EAGAIN)
                printf("FingerPrint Thread : Out of Memory\n");

            else if (ret == EINVAL)
                printf("FingerPrint Thread : Invalid Attribute\n");
            printf("FingerPrint Thread : Failed to Create Finger Print Thread\n");
        }
        else
        {
            // get the firmware version for later
            ////
            UF_UINT32 fware;

            if (uf_read_system_parameter(UF_SYS_FIRMWARE_VERSION, &fware) == UF_RET_SUCCESS)
            {
                char fwa[10];
                sprintf(fwa, "%#x", (unsigned int)fware);
                char key[10];

                for ( int i = 1; i < 11; i++ )
                {
                    unsigned char mnib = fwa[i * 2];

                    if (mnib <= '9')
                        mnib -= 48;
                    else
                        mnib -= 55;
                    unsigned char lnib = fwa[i * 2 + 1];

                    if (lnib <= '9')
                        lnib -= 48;
                    else
                        lnib -= 55;
                    key[i] = (mnib << 4) | lnib;
                }
                char b[16];

                //A16A need to be written as 1.6A
                for ( int j = 2, m = 0; j < 5; j++, m++ )
                {
                    b[m] = key[j];

                    if (m == 0)
                    {
                        m++;
                        b[m] = '.';
                    }
                }
                b[4] = 0;


				// also get the memory of the card:
				// UF_SYS_ENROLLED_FINGER		= 0x73,
				// UF_SYS_AVAILABLE_FINTER		= 0x74,
				UF_UINT32 enrolledfinger_param_value = 0;
				UF_UINT32 availablefinger_param_value = 0;
				UF_UINT32 templatememory_value = 0;
				UF_RET_CODE Res1 = UF_RET_SUCCESS;
				Res1 = uf_read_system_parameter(UF_SYS_ENROLLED_FINGER, &enrolledfinger_param_value);
				Res1 = uf_read_system_parameter(UF_SYS_AVAILABLE_FINTER, &availablefinger_param_value); // yes, that's a typo, but it's there...
				templatememory_value = enrolledfinger_param_value + availablefinger_param_value;
				b[4] = '-';
				switch ( templatememory_value )
				{
					case 0x776:		// 1M
						b[5] = '1';
						break;
					case 0x2576 :	// 4M
						b[5] = '4';
						break;
					default:		// other
						b[5] = '?';
						break;
				}
				b[6] = 'M';
				b[7] = '\0';

///////////////////////////////

                char* f = b;
                FILE* fpv = fopen("firmware_version", "w");

                if (fpv != NULL)
                {
                    fwrite(f, 1, strlen(f), fpv);
                    fclose(fpv);
                }
            }
			else
			{
                	FILE* fpvna = fopen("firmware_version", "w");

                	if (fpvna != NULL)
                	{
                	    fwrite("NOT AVAILABLE", 1, 13 , fpvna);
                	    fclose(fpvna);
                	}
            }
		}
            ////
    }
    else
    {
        // if the FPU in disable state .
        DP("Inactive finger print unit\n");
    }
}


/*
FingerPrintFunction:



Description:

    This function implement the executing start_routine  we supply to Finger Print thread , it's perform scan template
    and indicate if finger print template wa received from the user , the function react according to the value in the      variable fpOpr.fpOperation , this variable options are :
                    1. Identify_Operation  (the finger template was scanned for idintefcation ).
                    2. Verify_Operation    (the finger template was scanned for verification ).
                    3. Ignore_Operation    (ignore the finger template scanned).


Arguments:
     void* id

Returns:
    void-

*/

/*void *SY7000Demo::FingerPrintFunction( void* id )
    {
    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());
    OperationIODriver* oper = (OperationIODriver *)id;

    fpOpr.WriteSystemParameter(UF_SYS_SENSITIVIY, 0x37);    //highest sensitivity
    fpOpr.WriteSystemParameter(UF_SYS_TIMEOUT, 0x3A);       //timeout = 10 sec
    fpOpr.WriteSystemParameter(UF_SYS_AUTO_RESPONSE, 0x30); // Auto Response = ON
    fpOpr.WriteSystemParameter(UF_SYS_FREE_SCAN, 0x31);     // free scan = ON
    fpOpr.WriteSystemParameter(UF_SYS_AUTO_RESPONSE, 0x31); // Auto Response = ON


	{
		// enroll mode - One Time = single template
		//
		UF_UINT32 enroll_mode_value;
		UF_RET_CODE ResEM = uf_read_system_parameter(UF_SYS_ENROLL_MODE, &enroll_mode_value);
    	if (ResEM != UF_RET_SUCCESS || (int)enroll_mode_value != 48)
    	{
    	    fpOpr.WriteSystemParameter(UF_SYS_ENROLL_MODE, 0x30); // enroll mode = One Time
    	    ResEM = uf_read_system_parameter(UF_SYS_ENROLL_MODE, &enroll_mode_value);
    	}
	}

	{
		// matching timoeut - 1000's of templates - make it really long
		//
		UF_UINT32 matching_timeout_value;
		UF_RET_CODE ResEM = uf_read_system_parameter(UF_SYS_MATCHING_TIMEOUT, &matching_timeout_value);
    	if (ResEM != UF_RET_SUCCESS || (int)matching_timeout_value != 48)
    	{
    	    fpOpr.WriteSystemParameter(UF_SYS_MATCHING_TIMEOUT, 0x3a); // matching timeout = 10 seconds
    	    ResEM = uf_read_system_parameter(UF_SYS_MATCHING_TIMEOUT, &matching_timeout_value);
    	}
	}



    while (true)
    {
        if (fpOpr.fpOperation == Verify_Operation) // in case of verify user
        {
            // I perfom the user verify case in main function by calling the method 'FpVerifyUser'
            fpOpr.WriteSystemParameter(UF_SYS_FREE_SCAN, 0x30);     // free scan = OFF
            fpOpr.WriteSystemParameter(UF_SYS_AUTO_RESPONSE, 0x30); // Auto Response = OFF
            pthread_cond_wait(&FPOperation::fprint_cond_flag, &FPOperation::finger_print_mutex);
            fpOpr.WriteSystemParameter(UF_SYS_FREE_SCAN, 0x31);     // free scan = ON
            fpOpr.WriteSystemParameter(UF_SYS_AUTO_RESPONSE, 0x31); // Auto Response = ON
        }

        if (fpOpr.fpOperation == Identify_Operation)
        {

            UF_RET_CODE Res;
            UF_BYTE packet[PACKET_LEN];
            UF_UINT32 freescam_param_value;
            UF_UINT32 autoresponse_param_value;
            UF_RET_CODE Res1 = uf_read_system_parameter(UF_SYS_FREE_SCAN, &freescam_param_value);

            if (Res1 != UF_RET_SUCCESS || (int)freescam_param_value != 49)
            {
                fpOpr.WriteSystemParameter(UF_SYS_FREE_SCAN, 0x31); // free scan = ON
                Res1 = uf_read_system_parameter(UF_SYS_FREE_SCAN, &freescam_param_value);
            }
            UF_RET_CODE Res2 = uf_read_system_parameter(UF_SYS_AUTO_RESPONSE, &autoresponse_param_value);

            if (Res2 != UF_RET_SUCCESS || autoresponse_param_value != 49)
            {
                fpOpr.WriteSystemParameter(UF_SYS_AUTO_RESPONSE, 0x31); // free scan = ON
                Res2 = uf_read_system_parameter(UF_SYS_AUTO_RESPONSE, &autoresponse_param_value);
            }

            Res = uf_receive_packet(packet); // get the scan template packet in the identification process from the FPU
            //if there is check sum error in the serial port reset the FPU and continue
            //printf("uf_receive_packet Res = %d\n",Res);
            if (Res == UF_ERR_CHECKSUM_ERROR)
            {
                fpOpr.FPU_Reset();
                sleep(5);
                continue;
            }

            if (Res == UF_RET_SUCCESS)                                           // if we accept packet from the FPU
            {
                if (packet[UF_PACKET_COMMAND_POS]
                    == UF_COM_IS) // check if the packet is identification response
                {
                    if (packet[UF_PACKET_FLAG_POS] == UF_PROTO_RET_SCAN_SUCCESS) //check if scan template success
                    {
                        Res =
                            uf_receive_packet(
                                packet); //accept the second packet in the identification process  from the FPU

                        if (Res == UF_ERR_CHECKSUM_ERROR)
                        {
                            fpOpr.FPU_Reset();
                            sleep(5);
                            continue;
                        }

                        if (Res
                            == UF_RET_SUCCESS) // if we accept the second packet from the FPU
                        {
                            if (packet[UF_PACKET_FLAG_POS] == UF_PROTO_RET_SUCCESS) //check if success identify the user
                            {
                                UF_UINT32 UserID = 0;
                                UserID =
                                    ((UF_UINT32)packet[UF_PACKET_PARAM_POS]); // get the fist byte in the user ID
                                UserID = UserID | ((UF_UINT32)packet[UF_PACKET_PARAM_POS + 1]) << 8; //byte 2 in user ID
                                UserID =
                                    UserID | ((UF_UINT32)packet[UF_PACKET_PARAM_POS + 2]) << 16;     //byte 3 in user ID
                                UserID =
                                    UserID | ((UF_UINT32)packet[UF_PACKET_PARAM_POS + 3]) << 24;     //byte 4 in user ID
                                fpOpr.FpUserID = UserID;
                                fpOpr.FpScanValid =
                                    true; // tell the badge read function about the new user ID
                                fpOpr.ScanResult = UF_RET_SUCCESS;
                                fpOpr.ScanCount++;
                                fpOpr.WriteSystemParameter(UF_SYS_FREE_SCAN, 0x30); // free scan = OFF
                                //stop until the read badge get the user ID and send signal to continue
                                pthread_cond_wait(&FPOperation::fprint_cond_flag, &FPOperation::finger_print_mutex);
                                fpOpr.WriteSystemParameter(UF_SYS_FREE_SCAN, 0x31); // free scan = ON
                            }
                            else                                                    // if the user not found in the FPU or error occure in the identification process
                            {
                                pthread_mutex_lock(&OperationIODriver::lockDisplay);
                                DispalyIdentifyFpuError(oper, packet[UF_PACKET_FLAG_POS]); // display the error
                                pthread_mutex_unlock(&OperationIODriver::lockDisplay);
                            }
                        }
                    }
                    else //if we failed to scan template , display the error
                    {
                        pthread_mutex_lock(&OperationIODriver::lockDisplay);
                        DispalyIdentifyFpuError(oper, packet[UF_PACKET_FLAG_POS]); // display the error
                        pthread_mutex_unlock(&OperationIODriver::lockDisplay);
                    }
                }
            }
        }

        if (fpOpr.fpOperation == Ignore_Operation)
        {
            //ignore the current finger print template
            // sleep until signal will be sent from the main function to active the template scanning again .
            fpOpr.WriteSystemParameter(UF_SYS_FREE_SCAN, 0x30);     // free scan = OFF
            fpOpr.WriteSystemParameter(UF_SYS_AUTO_RESPONSE, 0x30); // Auto Response = OFF
            pthread_cond_wait(&FPOperation::fprint_cond_flag, &FPOperation::finger_print_mutex);
            fpOpr.WriteSystemParameter(UF_SYS_FREE_SCAN, 0x31);     // free scan = ON
            fpOpr.WriteSystemParameter(UF_SYS_AUTO_RESPONSE, 0x31); // Auto Response = ON
        }
    }
}*/

/*
FingerPrintFunction:



Description:

	This function implement the executing start_routine  we supply to Finger Print thread , it's perform scan template 
	and indicate if finger print template wa received from the user , the function react according to the value in the      variable fpOpr.fpOperation , this variable options are :
					1. Identify_Operation  (the finger template was scanned for idintefcation ).
					2. Verify_Operation    (the finger template was scanned for verification ).
					3. Ignore_Operation    (ignore the finger template scanned).


Arguments:
     void* id

Returns:
	void-

*/
void* SY7000Demo::FingerPrintFunction(void* id)
{
  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
  OperationIODriver* oper = (OperationIODriver*)id;

  int count = 0;
  while(true)
  {
	 if(fpOpr.fpOperation == Verify_Operation) // in case of verify user
	 {
		 // I perfom the user verify case in main function by calling the method 'FpVerifyUser'

		 pthread_cond_wait(&FPOperation::fprint_cond_flag,&FPOperation::finger_print_mutex);
   
     }
	 if(fpOpr.fpOperation == Identify_Operation)
	 {
			int result;
			UF_BYTE packet[UF_NPACKET_LEN];
            memset(packet,0,UF_PACKET_LEN);
			
			if(uf_cancel() != UF_RET_SUCCESS)
			{
				printf("uf_cancel 1 filed\n");
				sleep(1);
				if(uf_cancel() != UF_RET_SUCCESS)
				{
					printf("uf_cancel 2 filed\n");
					sleep(1);
					if(uf_cancel() != UF_RET_SUCCESS)
						printf("uf_cancel 3 filed\n");
				} 
			}
            result = uf_send_packet( UF_COM_IS, 0, 0, 0 );


			if( result < 0 )
			{
				printf("send IS Command %d\n",result);
				continue;
			}

			if( g_sys_config.send_scan_success )
			{
				result = uf_receive_packet( packet );

				if(result == UF_RET_SUCCESS && packet[UF_PACKET_COMMAND_POS] == UF_PROTO_RET_TIME_OUT)count = 0;
				if(result == UF_RET_SUCCESS && packet[UF_PACKET_COMMAND_POS] == UF_COM_IS && 
					packet[UF_PACKET_FLAG_POS] == UF_PROTO_RET_BUSY && count == 2)
				{
					// reset FPU 
					if(uf_cancel() != UF_RET_SUCCESS)
					{
						printf("line = %d uf_cancel 1 filed\n",__LINE__);
						sleep(1);
						if(uf_cancel() != UF_RET_SUCCESS)
						{
							printf("line = %d uf_cancel 2 filed\n",__LINE__);
							sleep(1);
							if(uf_cancel() != UF_RET_SUCCESS)
								printf("line = %d uf_cancel 3 filed\n",__LINE__);
						} 
					}
					uf_send_packet( 0xd0, 0, 0, 0 );
					sleep(5);	
				}

                if(result == UF_RET_SUCCESS) // if we accept packet from the FPU
				{
					if(packet[UF_PACKET_FLAG_POS] == UF_PROTO_RET_BUSY)count++;
					printf("Receive1 \n");
					for(int i = 0; i < UF_PACKET_LEN; i++ )
					{
						printf("%02x ", packet[i] );
					}
					printf("\n");	
					//check if scan template success
				    if(packet[UF_PACKET_COMMAND_POS] == UF_COM_IS &&
						packet[UF_PACKET_FLAG_POS] == UF_PROTO_RET_SCAN_SUCCESS ) 
					{
						result = uf_receive_packet( packet );

						if(result == UF_RET_SUCCESS)
						{
							//printf("Receive2 \n");
							//for(int i = 0; i < UF_PACKET_LEN; i++ )
							//{	
								//printf("%02x ", packet[i] );
							//}
							//printf("\n");
							if(packet[UF_PACKET_COMMAND_POS] == UF_COM_IS)
							{
								//check if scan template success
								if(packet[UF_PACKET_FLAG_POS] == UF_PROTO_RET_SUCCESS) 
								{
									UF_UINT32 UserID=0;
									UserID = ((UF_UINT32)packet[UF_PACKET_PARAM_POS]); //get the fist byte in the userID
									UserID = UserID | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+1])<< 8;//byte 2 in user ID
									UserID = UserID | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+2])<< 16;//byte 3 user ID
									UserID = UserID | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+3])<< 24;//byte 4 user ID
									fpOpr.FpUserID = UserID; 
									FPOperation::FpScanValid = true; //tell the badge read function about  new user ID
									FPOperation::ScanResult = UF_RET_SUCCESS;  
									FPOperation::ScanCount ++ ;			
									//stop until the read badge get the user ID and send signal to continue
									pthread_cond_wait(&FPOperation::fprint_cond_flag,&FPOperation::finger_print_mutex);
								}
								else
								{
									if(packet[UF_PACKET_FLAG_POS] != UF_PROTO_RET_CANCELED && 
										packet[UF_PACKET_FLAG_POS] != UF_PROTO_RET_TIME_OUT)
									{
										pthread_mutex_lock( &OperationIODriver::lockDisplay ); 
										DispalyIdentifyFpuError(oper,packet[UF_PACKET_FLAG_POS]); // display the error
										pthread_mutex_unlock( &OperationIODriver::lockDisplay );
									}
								}
							}
						}
						else
						{
						 
						}
					}
				}
			}
			else
			{
				//printf("befor recive \n");
			   	result = uf_receive_packet( packet );
				//printf("after recive %d\n",result);
                

				if(result == UF_RET_SUCCESS && packet[UF_PACKET_COMMAND_POS] == UF_PROTO_RET_TIME_OUT)count = 0;
				if(result == UF_RET_SUCCESS && packet[UF_PACKET_COMMAND_POS] == UF_COM_IS && 
					packet[UF_PACKET_FLAG_POS] == UF_PROTO_RET_BUSY && count == 2)
				{
				  // reset FPU  
				  if(uf_cancel() != UF_RET_SUCCESS)
				  {
						printf("line = %d uf_cancel 1 filed\n",__LINE__);
						sleep(1);
						if(uf_cancel() != UF_RET_SUCCESS)
						{
							printf("line = %d uf_cancel 2 filed\n",__LINE__);
							sleep(1);
							if(uf_cancel() != UF_RET_SUCCESS)
								printf("line = %d uf_cancel 3 filed\n",__LINE__);
						} 
					}
                  uf_send_packet( 0xd0, 0, 0, 0 );
				  sleep(5);	

				}
               
				if(result == UF_RET_SUCCESS)
				{
					//printf("Receive2 \n");
					//for(int i = 0; i < UF_PACKET_LEN; i++ )
					//{	
						//printf("%02x ", packet[i] );
					//}
					//printf("\n");

					if(packet[UF_PACKET_FLAG_POS] == UF_PROTO_RET_BUSY)count++;

					if(packet[UF_PACKET_COMMAND_POS] == UF_COM_IS)
					{
						//check if scan template success
						if(packet[UF_PACKET_FLAG_POS] == UF_PROTO_RET_SUCCESS) 
						{
							UF_UINT32 UserID=0;
							UserID = ((UF_UINT32)packet[UF_PACKET_PARAM_POS]); //get the fist byte in the userID
							UserID = UserID | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+1])<< 8;//byte 2 in user ID
							UserID = UserID | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+2])<< 16;//byte 3 user ID
							UserID = UserID | ((UF_UINT32)packet[UF_PACKET_PARAM_POS+3])<< 24;//byte 4 user ID
							fpOpr.FpUserID = UserID; 
							FPOperation::FpScanValid = true; //tell the badge read function about  new user ID
							FPOperation::ScanResult = UF_RET_SUCCESS;  
							FPOperation::ScanCount ++ ;			
							//stop until the read badge get the user ID and send signal to continue
							pthread_cond_wait(&FPOperation::fprint_cond_flag,&FPOperation::finger_print_mutex);
						}
						else
						{
		 
							if(packet[UF_PACKET_FLAG_POS] != UF_PROTO_RET_CANCELED &&
								packet[UF_PACKET_FLAG_POS] != UF_PROTO_RET_TIME_OUT)
							{
								pthread_mutex_lock( &OperationIODriver::lockDisplay ); 
								DispalyIdentifyFpuError(oper,packet[UF_PACKET_FLAG_POS]); // display the error
								pthread_mutex_unlock( &OperationIODriver::lockDisplay );
							}
			
						}
					}
				}
			}
					
	 }
	 if(fpOpr.fpOperation == Ignore_Operation)
	 {
       //ignore the current finger print template 
       // sleep until signal will be sent from the main function to active the template scanning again .
	   
	   if(uf_cancel() != UF_RET_SUCCESS)
	   {
			printf("uf_cancel 1 filed\n");
			sleep(1);
            if(uf_cancel() != UF_RET_SUCCESS)
			{
				printf("uf_cancel 2 filed\n");
				sleep(1);
				if(uf_cancel() != UF_RET_SUCCESS)
                              printf("uf_cancel 3 filed\n");
			} 
	   }

	   printf("IN Ignore_Operation\n");
	   pthread_cond_wait(&FPOperation::fprint_cond_flag,&FPOperation::finger_print_mutex);
	   printf("BACK FROM Ignore_Operation\n");

     }

  }
}
/*
DispalyIdentifyFpuError:



Description:

    this function responsible the display the error returned from the FPU in the identification process .


Arguments:
     void* id

Returns:
    void-

*/

void SY7000Demo::DispalyIdentifyFpuError( OperationIODriver* oper, UF_BYTE Error )
    {

    user_output_clear();

    if (Error == UF_PROTO_RET_SCAN_FAIL)
        {
        user_output_write(2, 1, "     SCAN FAIL      ");
        user_output_write(3, 1, "     TRY AGAIN      ");
        }
    else if (Error == UF_PROTO_RET_TIME_OUT)
        {
        user_output_write(2, 1, " TIME OUT TRY AGAIN  ");
        }
    else if (Error == UF_PROTO_RET_TRY_AGAIN)
        {
        user_output_write(2, 1, "     TRY AGAIN       ");
        }
    else if (Error == UF_PROTO_RET_NOT_FOUND)
        {
        user_output_write(2, 1, "   USER NOT FOUND    ");
        }
    else if (Error == UF_PROTO_RET_TIMEOUT_MATCH)
        {
        user_output_write(2, 1, "   TIME OUT MATCH    ");
        user_output_write(3, 1, "     TRY AGAIN       ");
        }
    else if (Error == 0x90)
        {
        user_output_write(2, 1, "     REJECTED ID     ");
        }
    else if (Error == 0x91)
        {
        user_output_write(2, 1, "    DURESS FINGER    ");
        }
    else if (Error == 0x94)
        {
        user_output_write(2, 1, "    ENTRANCE LIMIT   ");
        }
    else
        {
        user_output_write(2, 1, "IDENTIFICA... FAILED");
        user_output_write(3, 1, "     TRY AGAIN      ");
        }

    oper->GetOperation()->TurnOnLEDYellow();
    oper->GetOperation()->Beep(TYPE_Error);
    usleep(oper->GetOperation()->GetErrorMsgTimeout() * 1000);
    user_output_write(3, 1, BLANK_LINE);
    oper->GetOperation()->TurnOffLEDYellow();
    oper->GetOperation()->Beep(TYPE_TurnOff);
    }
/*
DispalyFpPromptError:



Description:

    this function display in the LCD the FPU identification , verification ...  result according to the value of the
    variable fpOpr.ScanResult this variable can have this options :

            1.UF_ERR_TRY_AGAIN
            2.UF_ERR_NOT_MATCH
            3.UF_ERR_NOT_FOUND
            4.UF_ERR_TIMEOUT

Arguments:
     OperationIODriver * oper - operation object to access the LCD ,Buzzer & LEDs...
     OperationStatusCode code - the result that returned by fpOpr method SC_Fail or SC_Success

Returns:
    void-

*/

void SY7000Demo::DispalyFpPromptError( OperationIODriver* oper, OperationStatusCode code )
    {
    if (code == SC_Fail)
        {
        user_output_clear();

        switch( fpOpr.ScanResult )
            {
            case UF_ERR_TRY_AGAIN:
                user_output_write(2, 1, "      TRY AGAIN     ");
                break;

            case UF_ERR_NOT_MATCH:
                user_output_write(2, 1, "     NOT MATCHED    ");
                break;

            case UF_ERR_NOT_FOUND:
                user_output_write(2, 1, "   USER NOT FOUND   ");
                break;

            case UF_ERR_TIMEOUT:
                user_output_write(2, 1, "  TIMEOUT TRY AGAIN ");
                break;

            default:
                user_output_write(2, 1, "IDENTIFICA... FAILED");
                user_output_write(3, 1, "     TRY AGAIN      ");
                break;
            }
        oper->GetOperation()->TurnOnLEDYellow();
        oper->GetOperation()->Beep(TYPE_Error);
        usleep(oper->GetOperation()->GetErrorMsgTimeout() * 1000);
        user_output_write(3, 1, BLANK_LINE);
        oper->GetOperation()->TurnOffLEDYellow();
        oper->GetOperation()->Beep(TYPE_TurnOff);
        }
    }


/*
FpVerifyUser:
Description:

    this function verify the user ' it's ask the user to place finger print to verify if the badge was insert to the
    correct user who was enrolled in the FPU

Arguments:
     OperationIODriver * oper - operation object to access the LCD ,Buzzer & LEDs...

Returns:
    bool- return true of the verify proccess success else return false

*/

bool SY7000Demo::FpVerifyUserDisplay( OperationIODriver* oper )
    {
    bool overflow = false;
    unsigned long int UserBadge = 0;
    static char digits [] = "123456789";
    int i = strcspn(oper->getBadgeBuffer(), digits);
    overflow =
        cfgMgr.ifOverFlow(oper->getBadgeBuffer()); // check if the badge buffer is overflow from 4 byte size.
    UserBadge =
        strtoul(
            oper->getBadgeBuffer() + i, NULL, 0); // convert the badge buffer to long integer without left zero's

    user_output_clear();

    if ((int)strlen(oper->getBadgeBuffer()) < 1 || UserBadge < 1 || overflow) // check if the user ID is legal
        {
        if (UserBadge == 0)
            {
            return false;
            }
        user_output_write(2, 1, "  FP VERIFICATION   ");

        if (overflow)
            user_output_write(3, 1, "  OVERFLOW USER ID  "); // if the user ID is overflow
        else
            user_output_write(3, 1, "   ILLEGAL USER ID  ");
        oper->GetOperation()->TurnOnLEDYellow();
        oper->GetOperation()->Beep(TYPE_Error);
        usleep(oper->GetOperation()->GetErrorMsgTimeout() * 1000);
        user_output_write(3, 1, BLANK_LINE);
        oper->GetOperation()->TurnOffLEDYellow();
        oper->Reset();
        return false;
        }
    // if the user ID is legal ask to place finger to verify user.
    //OperationStatusCode FPcode = SC_Fail;
    user_output_write(2, 1, "  FP VERIFICATION   ");
    user_output_write(3, 1, "   PLACE FINGER     ");
    return true;
    }


OperationStatusCode SY7000Demo::FpVerifyUserLocally( OperationIODriver* oper, unsigned long int UserBadge,
	UF_BYTE * liveRawFPTemplate,
    UF_RET_CODE * pResult ) // same as FpVerifyUser, but no error reporting
{

    OperationStatusCode FPcode = SC_Fail;

	// verify by scan:
    FPcode = fpOpr.Verification(UserBadge, mTimeout, pResult); // verify user

//testing
printf("after local verification, fpcode = %d,  result = %d\n", FPcode, *pResult);
//endtest

    if (FPcode != SC_Success)                                  // if scantemplate failed display suitable message
    {
        return FPcode;
    }
	else
	{
		DP("fpverifyuserlocally: verification succeeds\n");
	}


    //if verify succuss wake the finger print thread and return true
    oper->GetOperation()->Beep(TYPE_KeyPress);
    //pthread_mutex_unlock (&FPOperation::finger_print_mutex);
    return FPcode;
}


OperationStatusCode SY7000Demo::FpVerifyUserFromServer(
	OperationIODriver* oper,
	unsigned long int UserBadge,
    UF_BYTE * liveRawFPTemplate,
	UF_RET_CODE  * pResult ) // VerifyByTemplate, from server
{
    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());

    UF_RET_CODE resultCode = UF_RET_SUCCESS;
    OperationStatusCode sccode = SC_Fail;

    CURLcode res = (CURLcode)1;
    //struct timespec clockTime;
    //int freq = 0;

	// encoded template from FPU
    UF_BYTE liveEncodedFPTemplate[ENCODED_TEMPLATESIZE+ 1]; // buffer to save the finger print tempalte will received from the sensor
    //UF_UINT32 live_encoded_templ_size;
    memset(&liveEncodedFPTemplate[0], 0x20, ENCODED_TEMPLATESIZE);   // bytes 0-511
    liveEncodedFPTemplate[ENCODED_TEMPLATESIZE] = '\0';              // byte 512




    char message[200];
    message[0] = '\0';


    //get the time and date in format "%Y-%m-%dT%H:%M:%S"
    //time_t optime;
    //struct tm* optimeinfo;
    //time(&optime);
    //optimeinfo = localtime(&optime);

    //char dateB[22];
    //char timeB[22];
    //dateB[0] = '\0';
    //timeB[0] = '\0';
    //strftime(timeB, 21, "T%H:%M:%S", optimeinfo);
    //strftime(dateB, 21, "%Y-%m-%d", optimeinfo);

    char utcTime[50];
    utcTime[0] = '\0';

	struct tm currTime;
	Operation::SYClock_GetTime(&currTime);
	strftime(utcTime, 20, "%Y-%m-%dT%H:%M:%S", &currTime);

    //strcpy(utcTime, dateB);
    //strcat(utcTime, timeB);

    // just to let the user know what's going on ....
    user_output_clear();
    user_output_write(2, 1, "  FP VERIFICATION   ");
    user_output_write(3, 1, "  By                ");
    user_output_write(4, 1, "  Server Template   ");
    oper->GetOperation()->Beep(TYPE_KeyPress);

    DP("fpvuserfromserver: getbadgebuffer is %s\n", oper->getBadgeBuffer());
    DP("fpvuserfromserver: userbadge is      %ld\n", UserBadge);

    res = (CURLcode)1;
    //reqCount++;
    if (OperationIODriver::communication)
	{
        pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
        //DLOGI("GetFPTemplate");
		httpS->GetFPTemplate(ClockId, Account, oper->getBadgeBuffer(), utcTime, SOAP_1_1_VER, &res);
        //httpS->count++; // <-- ????
        XMLProperties parser;
        char status[2];
        status[0] = '\0';

		DLOGI("%s %d Get FP Template from Server result = %d ClockId=%s Account=%s Badge=%s response-size = %d",__FUNCTION__,__LINE__,res,ClockId,Account,oper->getBadgeBuffer());

        if (OperationIODriver::communication && res == CURLE_OK && httpS->chunk.memory)
		{
			//DLOGI("%s %d Get FP Template from Server response = %s",__FUNCTION__,__LINE__,httpS->chunk.memory);
			DLOGI("%s %d response-size = %d",__FUNCTION__,__LINE__,strlen(httpS->chunk.memory));
            char* begintag = NULL;
            char* endtag = NULL;
            char* cdatabegin = NULL;
            char* cdataend = NULL;

            begintag = strstr(httpS->chunk.memory, "<GetFPTemplateResult"); //for example <GetFPTemplateResponse xmlns="Synel/TeamsWS">

            if (begintag != NULL)
            {
                endtag = strpbrk(begintag, ">");

                if (endtag != NULL)
                {
                    cdatabegin = endtag + 1; // next char
                    begintag = strstr(cdatabegin, "</GetFPTemplateResult>");

                    if (begintag != NULL)
                    {
                        endtag = strpbrk(begintag, ">");

                        if (endtag != NULL)
                        {
                            cdataend = begintag - 1;
                            message[0] = 'O';
                            message[1] = 'K';
                            message[2] = '\0';
                        }
                        else
                        {
                            // not well-formed xml
                            message[0] = '\0';
                        }
                    }
                    else
                    {
                        // not well-formed xml
                        message[0] = '\0';
                    }
                }
                else
                {
                    // not well-formed xml
                    message[0] = '\0';
                }
            }
            else
            {
                begintag = strstr(httpS->chunk.memory, "<GetFPTemplateResult />");

                if (begintag != NULL)
                {
                    // empty element
                    // no templates available
                    DP("GetFPTemplateResult is empty element : No Templates\n");
                }
                else
                {
                    // element not found
                    // no templates available
                    DP("GetFPTemplateResult element not found: No Templates\n");
                }
                message[0] = '\0';
                sccode = SC_Fail;
				DLOGI("%s %d\n resultCode = UF_ERR_NOT_FOUND",__FUNCTION__,__LINE__);
                resultCode = UF_ERR_NOT_FOUND;
            }
        }
        else //if (OperationIODriver::communication && res == CURLE_OK && httpS->chunk.memory)
        {
            //TODO: handle case of no templates
            sccode = SC_Fail;
			DLOGI("%s %d\n resultCode = UF_ERR_EXIST_ID",__FUNCTION__,__LINE__);
            resultCode = UF_ERR_EXIST_ID;
        }

		// xml is ok. parse and verify

    	user_output_write(3, 1, "                    ");
    	user_output_write(4, 1, "   Place Finger     ");
    	oper->GetOperation()->Beep(TYPE_KeyPress);

		if ( strlen(message) > 0  && httpS->chunk.memory)
		{
			sccode = (OperationStatusCode)VerifyUserFromServerTemplates( UserBadge, httpS->chunk.memory, liveRawFPTemplate );
			//sccode = SC_Fail;
			if ( sccode != SC_Success )
			{
				DLOGI("%s %d\n resultCode = UF_ERR_NOT_FOUND",__FUNCTION__,__LINE__);
				resultCode = UF_ERR_NOT_FOUND;
			}
		}
		pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
	}
    else //offline
    {
        if (OperationIODriver::communication == false)
        {
            user_output_clear();
            user_output_write(2, 1, "VerifyByServer");
            user_output_write(3, 1, "Not Available");
            sccode = SC_Fail;
            resultCode = UF_ERR_EXIST_ID;
        }
    }
	*pResult = resultCode;
//DP("fpverifyuserfromserver says result code is %d\n", resultCode);
//DP("fpverifyuserfromserver says sccode is %d\n", sccode);
    return sccode;
}


/*----------------- FpVerifyUser ----------------
 *
 *
 * 1. get verification mode
 * 		a. check local templates
 * 		b. check server templates
 * 		c. check local, then server
 * 		d. check server, then local
 * 		e. no check
 * 2. if no verification mode defined: local check only
 *
 * --------------------------------------------------*/

bool SY7000Demo::FpVerifyUser( OperationIODriver* oper )
    {

    //bool userfound;
    unsigned long int UserBadge;
    OperationStatusCode FPcode = SC_Fail;
    UF_RET_CODE Result;

	bool userIsLocal = false;

	//logger = Logger::getLogger();
	//logger->setAppLogLevel(FileAppender::LEVEL_DEBUG);

	// encoded template from FPU
    UF_BYTE liveEncodedFPTemplate[ENCODED_TEMPLATESIZE+ 1]; // buffer to save the finger print tempalte will received from the sensor
    //UF_UINT32 live_encoded_templ_size;

    // raw template from FPU
    UF_BYTE liveRawFPTemplate[FP_TEMPLATESIZE + 1];
    //UF_UINT32 live_raw_template_size;

    memset(&liveEncodedFPTemplate[0], 0x20, ENCODED_TEMPLATESIZE);   // bytes 0-511
    liveEncodedFPTemplate[ENCODED_TEMPLATESIZE] = '\0';              // byte 512
    memset(&liveRawFPTemplate[0], 0x20, FP_TEMPLATESIZE);
    liveRawFPTemplate[FP_TEMPLATESIZE] = '\0';



    char digits [] = "123456789";
    int i = strcspn(oper->getBadgeBuffer(), digits);
    UserBadge =
        strtoul(oper->getBadgeBuffer() + i, NULL, 0); // convert the badge buffer to long integer without left zero's

    DP("fpvuserlocally: getbadgebuffer is %s\n", oper->getBadgeBuffer());
    DP("fpvuserlocally: userbadge is      %ld\n", UserBadge);

    UF_UINT32 checkresult;
    UF_UINT32 numTemplates;

    checkresult = uf_check_user_id((UF_UINT32)UserBadge, &numTemplates);

    if (checkresult == (UF_UINT32)UF_ERR_BUSY || checkresult)
    {

#ifdef ALLOW_UNVERIFIED_BADGES_OFFLINE

       if (OperationIODriver::communication == false )
       {
			DP("fpverifyuser: allow unverified badges in offline mode\n");
			userIsLocal = true;
	   }
	   else
	   {
			DP("fpverifyuser: user not found locally, fall thru and try server\n");
			userIsLocal = false;
	   }
#else

        // local verification errors:
        user_output_clear();
#ifdef DEBUG

        user_output_write(2, 1, "  check_user_id   ");

#else

        user_output_write(2, 1, "  FP VERIFICATION   ");

#endif

        //user_output_write(3, 1, "   INVALID USER ID  ");
		user_output_write(3, 1, "   INVALID USER     ");
        oper->GetOperation()->TurnOnLEDYellow();
        oper->GetOperation()->Beep(TYPE_Error);
        usleep(oper->GetOperation()->GetErrorMsgTimeout() * 1000);
        user_output_write(3, 1, BLANK_LINE);
        user_output_write(4, 1, BLANK_LINE);
        oper->GetOperation()->TurnOffLEDYellow();
        //pthread_mutex_unlock (&FPOperation::finger_print_mutex);
        oper->Reset();
        DP("fpverifyuser: roaming badges NOT Allowed\n");
        return false;

#endif


    }
    else
    {
        DP("fpverifyuser: user exists on clock\n");
		userIsLocal = true;
    }

	if ( userIsLocal == true )
	{
		// use original code
    	if (FpVerifyUserDisplay(oper)) // overflow/initial badge check handled by xxxDisplay()
    	{
			DLOGI("%s %d Verify FPU User  Locally",__FUNCTION__,__LINE__);
    	    if ((FPcode = FpVerifyUserLocally(oper, UserBadge, (UF_BYTE *)&liveRawFPTemplate[0], &Result)) == SC_Success)
    	    {
    	        // Ok.
    	        DP("fpverifyuser: user is verified locally\n");
				DP("fpverifyuser: local return code is %d\n", FPcode);
				DP("fpverifyuser: local result is %d\n", Result);
    	        oper->GetOperation()->Beep(TYPE_KeyPress);
    	        return true;
    	    }
    	    else
    	    {

				char *emsg = (char*)malloc(128);
				emsg[0] = '\0';
				// handle local verification errors:
				DP("fpverifyuser: handle local verify errors #1\n");
    	        user_output_clear();
    	        user_output_write(2, 1, "  FP VERIFICATION   ");

				//DLOGE("----------------");
				//DLOGE("Verification for ");
				strcat(emsg,"Verification for ");
				//DLOGE(oper->getBadgeBuffer());
				strcat(emsg, oper->getBadgeBuffer());
				strcat(emsg," : ");

    	        switch( Result )
    	            {
    	            case UF_ERR_TRY_AGAIN:
    	                user_output_write(3, 1, "      TRY AGAIN     ");
						//DLOGE("Try Again #2");
						strcat(emsg, "Try Again #2");
    	                break;

    	            case UF_ERR_NOT_MATCH:
    	                user_output_write(3, 1, "     NOT MATCHED    ");
						DLOGE("Not Matched #2");
						strcat(emsg,"Not Matched #2");
    	                break;

    	            case UF_ERR_NOT_FOUND:
    	                //user_output_write(3, 1, "   INVALID USER ID  ");
						user_output_write(3, 1, "   INVALID USER     ");
						//DLOGE("Invalid User #2, Not Enrolled");
						strcat(emsg, "Invalid User #2, Not Enrolled");
    	                break;

    	            case UF_ERR_SCAN_FAILED:
						user_output_write(3, 1, "   SCAN FAILED      ");
						//DLOGE("Scan Failed #2");
						strcat(emsg,"Scan Failed #2");
    	                break;

    	            case UF_ERR_TIMEOUT:
    	                user_output_write(3, 1, "  TIMEOUT TRY AGAIN ");
						//DLOGE("Timeout #2");
						strcat(emsg,"Timeout #2");
    	                break;

    	            default:
    	                user_output_write(3, 1, " VERIFICATION FAILED");
    	                user_output_write(4, 1, "     TRY AGAIN      ");
						//DLOGE("Verify failed #2, %d", Result);
						//DLOGE("Verify failed #2");
						strcat(emsg,"Verify failed #2");
    	                break;
    	            }
				//DLOGE("----------------");

				DLOGE(emsg);
				free(emsg);

				if (	OperationIODriver::OfflineModeIsEnabled == true  &&
						OperationIODriver::communication == false )
				{
					user_output_write(4,1, "Accept Unverified");
				}

    	        oper->GetOperation()->TurnOnLEDYellow();
    	        oper->GetOperation()->Beep(TYPE_Error);
    	        usleep(oper->GetOperation()->GetErrorMsgTimeout() * 1000);
    	        user_output_write(3, 1, BLANK_LINE);
    	        user_output_write(4, 1, BLANK_LINE);
    	        oper->GetOperation()->TurnOffLEDYellow();
    	        //pthread_mutex_unlock (&FPOperation::finger_print_mutex);
    	        oper->Reset();

				if (	OperationIODriver::OfflineModeIsEnabled == true  ) DP("fpverifyuser: offlinemode is enabled\n");
				if (	OperationIODriver::OfflineModeIsEnabled == false ) DP("fpverifyuser: offlinemode is false\n");


				if (	OperationIODriver::OfflineModeIsEnabled == true  &&
						OperationIODriver::communication == false )
				{
					DP("FPVerifyUser() returns true\n");
					return true;  // accept as verified
				}
				else
				{
					DP("FPVerifyUser() returns false\n");
					return false;
				}

    	    }
    	}
    	else
    	{
    	    return false;
    	}

	}
	else		// user not found locally, try server
	{
        // not in local fpu table. Try server

        if (OperationIODriver::communication == true)
        {
            DP("fpverifyuser: call fpverifyuserfromserver\n");
			DLOGI("%s %d Verify FPU User  From Server",__FUNCTION__,__LINE__);
			if ((FPcode = FpVerifyUserFromServer(oper, UserBadge, (UF_BYTE *)&liveRawFPTemplate[0], &Result)) == SC_Success)
            {
                DP("fpverifyuserfromserver returns SC_Success\n");
                DP("fpverifyuserfromserver results is %d\n", Result);
                // Ok.
                oper->GetOperation()->Beep(TYPE_KeyPress);
                return true;
            }
            else
            {

				char * emsg = (char*)malloc(128);
				emsg[0] = '\0';
                DP("fpverifyuserfromserver returns failure, %d\n", FPcode);
                DP("fpverifyuserfromserver results is %d\n", Result);

                // not found. errorcode in Result
                user_output_clear();
                user_output_write(2, 1, "  FP VERIFICATION   ");


				strcat(emsg,"Verification for ");
				strcat(emsg, oper->getBadgeBuffer());
				strcat(emsg," : ");

                switch( Result )
                {
                    case UF_ERR_TRY_AGAIN:
                        user_output_write(3, 1, "      TRY AGAIN     ");
						strcat(emsg,"Try Again #3");
                        break;

                    case UF_ERR_NOT_MATCH:
                        user_output_write(3, 1, "     NOT MATCHED    ");
						strcat(emsg,"Not Matched #3");
                        break;

                    case UF_ERR_NOT_FOUND:
                        //user_output_write(3, 1, "   INVALID USER ID  ");
						user_output_write(3, 1, "   INVALID USER     ");
						strcat(emsg,"Invalid User #3, Not Enrolled, Not in Database");
						break;

                    case UF_ERR_SCAN_FAILED:
						user_output_write(3, 1, "   SCAN FAILED      ");
						strcat(emsg, "Scan Failed #3");
                        break;

                    case UF_ERR_TIMEOUT:
                        user_output_write(3, 1, "  TIMEOUT TRY AGAIN ");
						strcat(emsg, "Timeout #3");
                        break;

                    default:
                        user_output_write(3, 1, " VERIFICATION FAILED");
                        user_output_write(4, 1, "     TRY AGAIN      ");

						strcat(emsg,"Verify Failed #3");
                        break;
                }
				DLOGE(emsg);
				free(emsg);
				oper->GetOperation()->TurnOnLEDYellow();
                oper->GetOperation()->Beep(TYPE_Error);
                usleep(oper->GetOperation()->GetErrorMsgTimeout() * 1000);
                user_output_write(3, 1, BLANK_LINE);
                user_output_write(4, 1, BLANK_LINE);
                oper->GetOperation()->TurnOffLEDYellow();
                //pthread_mutex_unlock (&FPOperation::finger_print_mutex);
                oper->Reset();
                return false;
            }
        }
        else
        {
			// no communication with the server....

            //PEND: OfflineMode: SaveTransaction, so failed attempts can be Logged.
			// if offline mode is enabled, then report errors,
			// but display additional "Accept Unverified"
			// return true, as verified, allowing for SwipePunch transaction to also
			// detect there is no communcation and save as offline transaction
			// (research/todo: add additional types for unverified transactions?


			// handle local verification errors:
DP("fpverifyuser: handle local verify errors #2, no server communication\n");
            user_output_clear();
            user_output_write(2, 1, "  FP VERIFICATION   ");

			char *emsg = (char*)malloc(128);
			emsg[0] = '\0';
			strcat(emsg,"Verification for ");
			strcat(emsg,oper->getBadgeBuffer());
			strcat(emsg," : ");

            switch( Result )
                {
                case UF_ERR_TRY_AGAIN:
                    user_output_write(3, 1, "      TRY AGAIN     ");
					strcat(emsg,"Try Again #4");
                    break;

                case UF_ERR_NOT_MATCH:
                    user_output_write(3, 1, "     NOT MATCHED    ");
					strcat(emsg,"Not Matched #4");
                    break;

                case UF_ERR_NOT_FOUND:
                    //user_output_write(3, 1, "   INVALID USER ID  ");
					user_output_write(3, 1, "   INVALID USER     ");
					strcat(emsg,"Invalid User #4");
                    break;

				case UF_ERR_SCAN_FAILED:
					user_output_write(3, 1, "   SCAN FAILED      ");
					strcat(emsg,"Scan Failed #4");
                    break;

                case UF_ERR_TIMEOUT:
                    user_output_write(3, 1, "  TIMEOUT TRY AGAIN ");
					strcat(emsg,"Timeout #4");
                    break;

                default:
                    user_output_write(3, 1, " VERIFICATION FAILED");
                    user_output_write(4, 1, "     TRY AGAIN      ");
					strcat(emsg,"Verify Failed #4");
                    break;
                }
				DLOGE(emsg);
				free(emsg);

			if (	OperationIODriver::OfflineModeIsEnabled == true  &&
					OperationIODriver::communication == false )
			{
				user_output_write(4,1, "Accept Unverified");
			}

            oper->GetOperation()->TurnOnLEDYellow();
            oper->GetOperation()->Beep(TYPE_Error);
            usleep(oper->GetOperation()->GetErrorMsgTimeout() * 1000);
            user_output_write(3, 1, BLANK_LINE);
            user_output_write(4, 1, BLANK_LINE);
            oper->GetOperation()->TurnOffLEDYellow();
            //pthread_mutex_unlock (&FPOperation::finger_print_mutex);
            oper->Reset();

			if (	OperationIODriver::OfflineModeIsEnabled == true  ) DP("fpverifyuser: offlinemode is enabled\n");
			if (	OperationIODriver::OfflineModeIsEnabled == false ) DP("fpverifyuser: offlinemode is false\n");


			if (	OperationIODriver::OfflineModeIsEnabled == true  &&
					OperationIODriver::communication == false )
			{
				DP("FPVerifyUser() returns true\n");
				return true;  // accept as verified
			}
			else
			{
				DP("FPVerifyUser() returns false\n");
				return false;
			}




        }

    }
}

// original code from SDK:
//bool SY7000Demo::FpVerifyUser(OperationIODriver * oper)
//{
//  bool overflow = false;
//  unsigned long int UserBadge = 0;
//  char digits[] = "123456789";
//  int i = strcspn(oper->getBadgeBuffer(),digits);
//  overflow = cfgMgr.ifOverFlow(oper->getBadgeBuffer()); // check if the badge buffer is overflow from 4 byte size.
//  UserBadge =strtoul(oper->getBadgeBuffer()+i,NULL,0);// convert the badge buffer to long integer without left zero's

//  user_output_clear();
//  if( (int)strlen( oper->getBadgeBuffer() ) < 1 ||UserBadge < 1 || overflow) // check if the user ID is legal
//  {
//	if(UserBadge == 0)
//	{
//	    return false;
//	}
//	user_output_write(2, 1, "  FP VERIFICATION   ");
//	if(overflow)
//		user_output_write(3, 1, "  OVERFLOW USER ID  ");    // if the user ID is overflow
//	else
//		user_output_write(3, 1, "   ILLEGAL USER ID  ");
//	oper->GetOperation()->TurnOnLEDYellow();
//	oper->GetOperation()->Beep(TYPE_Error);
//	usleep( oper->GetOperation()->GetErrorMsgTimeout() * 1000 );
//	user_output_write(3, 1, BLANK_LINE);
//	oper->GetOperation()->TurnOffLEDYellow();
//	oper->Reset();
//	return false;
//  }
//  // if the user ID is legal ask to place finger to verify user.
//  OperationStatusCode FPcode = SC_Fail;
//  user_output_write(2, 1, "  FP VERIFICATION   ");
//  user_output_write(3, 1, "   PLACE FINGER     ");
//  UF_RET_CODE Result;


// // pthread_mutex_lock (&FPOperation::finger_print_mutex);
//  FPcode = fpOpr.Verification(UserBadge,mTimeout,&Result); // verify user
//  if(FPcode != SC_Success) // if verify failed display suitable message
//  {
//    user_output_clear();
//	user_output_write(2, 1, "  FP VERIFICATION   ");
//	switch(Result)
//	{
//		case UF_ERR_TRY_AGAIN:
//			 user_output_write(3, 1, "      TRY AGAIN     ");
//		     break;
//		case UF_ERR_NOT_MATCH:
//			 user_output_write(3, 1, "     NOT MATCHED    ");
//		     break;
//		case UF_ERR_NOT_FOUND:
//		case UF_ERR_SCAN_FAILED:
//			 user_output_write(3, 1, "   INVALID USER ID  ");
//	         break;
//		case UF_ERR_TIMEOUT:
//			 user_output_write(3, 1, "  TIMEOUT TRY AGAIN ");
//		     break;
//		default:
//			user_output_write(3, 1, " VERIFICATION FAILED");
//			user_output_write(4, 1, "     TRY AGAIN      ");
//			break;
//	}
//	oper->GetOperation()->TurnOnLEDYellow();
//	oper->GetOperation()->Beep(TYPE_Error);
//	usleep(oper->GetOperation()->GetErrorMsgTimeout() * 1000 );
//	user_output_write(3, 1, BLANK_LINE);
//	user_output_write(4, 1, BLANK_LINE);
//	oper->GetOperation()->TurnOffLEDYellow();
//	//pthread_mutex_unlock (&FPOperation::finger_print_mutex);
//	oper->Reset();
//	return  false;

//  }
//  //if verify succuss wake the finger print thread and return true
//  oper->GetOperation()->Beep(TYPE_KeyPress);
//  //pthread_mutex_unlock (&FPOperation::finger_print_mutex);
//  return true;
//}


/*
checkUserInServerDB:.

Description:

    this function send query to the data base server , to check if the specific user defined in the Users table , if yes	it get here name .

Arguments:

    UserName - string pointer to save the user name that was got from the data base server .
    OperationIODriver * oper - operation object to access the LCD ,Buzzer & LEDs...
    ServerDataBaseConnection* SDBConnection - data base connection for send query .

Returns:
    bool- return true if the user exist in the Users table in server data base else return false.

*/

//bool SY7000Demo::checkUserInServerDB( char* UserName, OperationIODriver* oper, ServerDataBaseConnection* SDBConnection )
//    {

//    char query[50];
//    sprintf(query, "select Name from Users where Badge = %s", oper->getBadgeBuffer());
//    CS_RETCODE res;

//    // send query to check if the user exist in the Users table in the server.
//    res = SDBConnection->RunQueryWithResult(query);
//    int row_count = 0;
//    int num_cols = 0;

//    while (res == CS_SUCCEED) // get rows result until last row , the first row we received is the titles of the table.
//        {
//        // get the row result of the query , the columns saved in SDBConnection->col array
//        res = SDBConnection->GetRowResult(row_count, num_cols);
//        }

//    if (row_count > 0) // if the user exist in Users table then get the column name of the user
//        {
//        char response[150];
//        strcpy(response, "<?xml version=\"1.0\" encoding=\"utf-8\"?><res><Status>1</Status><Message></Message></res>");
//        strcpy(UserName, SDBConnection->col[0].data);
//        // display the user name and return true.
//        ParseAndDisplayResponse(oper, response, UserName);
//        return true;
//        }
//    else // if the user not exist in the Users table in the server data base
//        {
//        char response[150];
//        strcpy(response, "<?xml version=\"1.0\" encoding=\"utf-8\"?><res><Status>0</Status><Message></Message></res>");
//        // display the message "Employee does not exist" and return false.
//        ParseAndDisplayResponse(oper, response, "Employee does not exist");
//        return false;
//        }
//    }


/*
FingerPrintFunction:



Description:

    This function implement the executing start_routine  we supply to Finger Print thread , it's perform scan template
    and indicate if finger print template wa received from the user , the function react according to the value in the      variable fpOpr.fpOperation , this variable options are :
                    1. Identify_Operation  (the finger template was scanned for idintefcation ).
                    2. Verify_Operation    (the finger template was scanned for verification ).
                    3. Ignore_Operation    (ignore the finger template scanned).


Arguments:
     void* id

Returns:
    void-

*/

/*void* SY7000Demo::FingerPrintFunction(void* id)
{
  printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
  UF_UINT32 sub_id=0;
  unsigned long int UserId;
  UF_BYTE FPtempalte[400]; // buffer to save the finger print tempalte will received from the sensor
  UF_UINT32 templ_size;
  UF_RET_CODE Result1;
  UF_RET_CODE Result2;
  int firstTime = 1;
  OperationIODriver* oper = (OperationIODriver*)id;

  while(true)
  {
     //scan finger print template , return UF_ERR_READ_SERIAL_TIMEOUT every 5000 ms in case no finger placed in the
     //sensor
      printf("1111\n");
     fpOpr.WriteSystemParameter(UF_SYS_SENSITIVIY, 0x37);	//highest sensitivity
     fpOpr.WriteSystemParameter(UF_SYS_TIMEOUT, 0x3A);//timeout = 10 sec
     fpOpr.WriteSystemParameter(UF_SYS_AUTO_RESPONSE, 0x30);// Auto Response = ON
     fpOpr.WriteSystemParameter(UF_SYS_FREE_SCAN, 0x31);// free scan = ON

     fpOpr.ScanTemplate(&templ_size,FPtempalte,5000,&Result1);

     if(Result1 != UF_ERR_READ_SERIAL_TIMEOUT && Result1 != UF_RET_SUCCESS)
     {
        pthread_mutex_lock( &OperationIODriver::lockDisplay );
        user_output_clear();
        switch(Result1)
        {
                case UF_ERR_TRY_AGAIN:
                    user_output_write(2, 1, "   POOR TEMPLATE    ");
                    user_output_write(3, 1, "     TRY AGAIN      ");
                break;
                case UF_ERR_NOT_FOUND:
                    user_output_write(2, 1, "SCAN TEMPLATE FAILED");
                    user_output_write(3, 1, "     TRY AGAIN      ");
                break;
                case UF_ERR_BUSY:
                    user_output_write(2, 1, "   MODULE IS BUSY   ");
                    user_output_write(3, 1, "     TRY AGAIN      ");
                break;
                case UF_ERR_CANCELED:
                    user_output_write(2, 1, "   SCAN CANCELED    ");
                    user_output_write(3, 1, "     TRY AGAIN      ");
                break;
                default:
                    user_output_write(2, 1, "TEMPLATE SCAN FAILED");
                    user_output_write(3, 1, "     TRY AGAIN      ");
                    UF_SYSTEM_STATUS status;
                    UF_RET_CODE stret = uf_check_system_status( &status );
                    printf("befor -> stret = %d status = %d \n",stret,status);
                    fpOpr.Identification(&UserId ,1000,&Result2);
                    printf("Result1 = %d Result2 = %d\n",Result1,Result2);
                    stret = uf_check_system_status( &status );
                    printf("AFTER-> stret = %d status = %d UF_SYS_ALIVE=%d UF_SYS_WAIT=%d UF_SYS_BUSY=%d UF_SYS_FAIL=%d \n",stret,status,UF_SYS_ALIVE,UF_SYS_WAIT,UF_SYS_BUSY,UF_SYS_FAIL);
                     //printf("UF_SENSOR_FAIL = %d\n",UF_SENSOR_FAIL);
                break;
        }
        oper->GetOperation()->TurnOnLEDYellow();
        oper->GetOperation()->Beep(TYPE_Error);
        usleep(oper->GetOperation()->GetErrorMsgTimeout() * 1000 );
        user_output_write(3, 1, BLANK_LINE);
        oper->GetOperation()->TurnOffLEDYellow();
        oper->GetOperation()->Beep(TYPE_TurnOff);
        pthread_mutex_unlock( &OperationIODriver::lockDisplay );
        uf_cancel();
        continue;
     }
     printf("1111\n");
     UF_RET_CODE Res;
     UF_BYTE packet[13];
     Res = uf_send_packet( 0xd0, 0, 0, 0 );
     UF_RET_CODE Res1 = uf_receive_packet(packet );
     if(!Res1)
     {
           for(int i = 0; i < 13; i++ )
        {
            printf("%02x ", packet[i] );
        }
     }
     printf("\n1111 Res1 = %d \n",Res1);
     fpOpr.FPU_Finalize();
     printf("2222 uf_send_packet_Res = %d\n",Res);
     sleep(3);
     for(;;)
     {
        printf("222 %d %d\n",fpOpr.fpOperation,Identify_Operation);
        Res = fpOpr.FPU_Initialize();
        if(!Res)break;
        printf("3333\n");
        sleep(3);
     }
     printf("4444\n");
     if(fpOpr.fpOperation == Verify_Operation) // in case of verify user
     {
         // I perfom the user verify case in main function by calling the method 'FpVerifyUser'
         pthread_cond_wait(&FPOperation::fprint_cond_flag,&FPOperation::finger_print_mutex);
     }
     if(fpOpr.fpOperation == Identify_Operation)
     {

       // the current finger print template was scanned for identification , call identify method

       if(Result1 == UF_RET_SUCCESS)
       {

            fpOpr.IdentifyByTemplate(FPtempalte,&templ_size,&UserId ,5000,&Result2);
            //printf("Result1 = %d Result2 = %d TIMEOUT = %d SUCCESS = %d\n",Result1,Result2,UF_ERR_READ_SERIAL_TIMEOUT,UF_RET_SUCCESS);

            fpOpr.FpUserID = UserId;  // set current User ID wat identify
            fpOpr.FpScanValid = true; // valid the  FPU scan result
    // set the result of the FPU identification if the result is SC_Success the ReadBadge fucntion return the User Id
            fpOpr.ScanResult = Result2;
            fpOpr.ScanCount ++ ;
            // sleep until signal will be sent from the main function to active the template scanning again .
            pthread_cond_wait(&FPOperation::fprint_cond_flag,&FPOperation::finger_print_mutex);
            uf_cancel();
       }
     }
     if(fpOpr.fpOperation == Ignore_Operation)
     {
      //ignore the current finger print template
      // sleep until signal will be sent from the main function to active the template scanning again .
      pthread_cond_wait(&FPOperation::fprint_cond_flag,&FPOperation::finger_print_mutex);
     }
  }
}*/
int SY7000Demo::SaveOfflineTransaction( OperationIODriver* oper, char* utcTime, SaveReason sr,
    savetranstype_enum savetranstype )
    {

    //printf("offline mode: enter SaveOfflineTransaction\n");

    //printf("create OfflineTranRecord R\n");
    OffLineTranRecord R;

    //todo EnableSwipeAndGo = true; // TODO is this needed ????
    //TODO: use a variable to hold transaction type (or name):


    //  void SetTransaction(const char* Tran);
    //  void SetTime(const char* T);
    //  void SetEmployeeId(const char* EmpId);
    //  void SetGroup(const char * Gro);
    //  void SetProject(const char * Pro);
    //  void SetTask(const char * Ta);
    //  void SetActivity(const char * Act);
    //  void SetProjectSwitch(const char * PSwitch);

    switch( savetranstype )
        {
        case otSwipePunch:
            DP("offline mode: SaveOfflineTransaction, transtype is SwipePunch, Badge=%s\n", oper->getBadgeBuffer());
            //OffLineTranRecord R("SwipePunch", utcTime, oper->getBadgeBuffer(), "0");
            R.SetTransaction("SwipePunch");
            R.SetTime(utcTime);
            R.SetEmployeeId(oper->getBadgeBuffer());
            R.SetGroup(NULL);
            R.SetProject(NULL);
            R.SetTask(NULL);
            R.SetActivity(NULL);
            R.SetProjectSwitch(NULL);
            break;

        case otInPunch:
            //printf("offline mode: SaveOfflineTransaction, transtype is InPunch\n");
            //OffLineTranRecord R("InPunch", utcTime, oper->getBadgeBuffer(), "0");
            R.SetTransaction("InPunch");
            R.SetTime(utcTime);
            R.SetEmployeeId(oper->getBadgeBuffer());
            R.SetGroup("0");
            break;

        case otOutPunch:
            //printf("offline mode: SaveOfflineTransaction, transtype is OutPunch\n");
            //OffLineTranRecord R("OutPunch", utcTime, oper->getBadgeBuffer(), NULL);
            //printf("offline mode: SOT, SetTransaction\n");
            R.SetTransaction("OutPunch");
            //printf("offline mode: SOT, SetTime\n");
            R.SetTime(utcTime);
            //printf("offline mode: SOT, set EmployeeId\n");
            R.SetEmployeeId(oper->getBadgeBuffer());
            //printf("offline mode: SOT, set Group\n");
            R.SetGroup(NULL);
            break;

        case otClockIn:
            //printf("offline mode: SaveOfflineTransaction, transtype is ClockIn\n");
            //						//OffLineTranRecord R("ClockIn",utcTime,oper->getBadgeBuffer(),"0",
            //												//projectCode, taskCode, activityCode, projectSwitch);
            //												oper->levels[0].Data,
            //												oper->levels[1].Data,
            //												"A001",
            //												"0");

            R.SetTransaction("ClockIn");
            R.SetTime(utcTime);
            R.SetEmployeeId(oper->getBadgeBuffer());
            R.SetGroup(NULL);
            R.SetProject(NULL);
            R.SetTask(NULL);
            R.SetActivity(oper->levels[0].Data);
            R.SetProjectSwitch(NULL);

            break;

        // move this to its proper function:
        //					case otGetProjectTasks :
        //						OffLineTranRecord R("GetProjectTasks");	// accepts defaults
        //						break;
        default:
            break;
        }

    int otc_value;

    if (SaveTransaction(&R, sr))
        {
			DP("Saved Transaction to DB\n");
        sem_post(&offline_tran_count);       DP("sem_post offline_tran_count\n");
        sem_getvalue(&offline_tran_count, &otc_value);

        DP("offline tran_count is %d\n", otc_value);
        if (sr == SENDIND_FAILED)
            {
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex); // unlock the send request mutex
            }

        // normally, the web service call would return a 'message'
        // todo: check if setupsystem sets up some defaults.
        char msg[32];
        int msgLen = 0;
        msgLen = oper->GetOperation()->GetEmployeeMessage(oper->getBadgeBuffer(),msg,32); // TESTING: get an offline message
        char response[100];
        char msg2[50];
        snprintf(response,80,"<?xml version=\"1.0\" encoding=\"utf-8\"?><res><Status>0</Status><Message>");
        //strcpy(response, "<?xml version=\"1.0\" encoding=\"utf-8\"?><res><Status>0</Status>");
        //strcat(response, "<Message>");


        //if(msg == NULL && oper->getSourceBadge() != SysInputSourceKeypad)
        if (msgLen == 0)
        {
            strcpy(msg2, oper->getBadgeBuffer());

            switch( savetranstype )
                {
                case otSwipePunch:
                    strcat(msg2, " ");
                    strcat(msg2, "Swipe");
                    break;

                case otInPunch:
                    strcat(msg2, " ");
                    strcat(msg2, "InPunch");
                    break;

                case otOutPunch:
                    strcat(msg2, " ");
                    strcat(msg2, "OutPunch");
                    break;

                case otClockIn:
                    strcat(msg2, " ");
                    strcat(msg2, "ClockIn");
                    break;

                default:
                    strcat(msg2, " ");
                    strcat(msg2, "?Punch?");
                    break;
                }

            	strcat(msg2, " saved offline");
            	strcat(response, msg2);
         }

        if (msgLen != 0)
        {
            strcat(response, msg);
        }

        strcat(response, "</Message></res>");
        ParseAndDisplayResponse(oper, response, "No Message to Display");
        }
    //printf("offline mode: SaveOfflineTransaction returns True.\n");
    return true;
    }

void SY7000Demo::RunOfflineSenderThread()
{

    pthread_t ZDTUThread;
    pthread_attr_t ZDTUThreadAttr;

    int frc;
    SqliteDB sqlitedb;

    int sdbcallresults = 0;
    printf("ctor IOD, offlineTran_tbl FindRowCount\n");
    sdbcallresults = sqlitedb.offlineTran_tbl.FindRowCount(NULL, &frc);

    printf("ctor IOD, FindRowCount call returns %d\n", frc);
    printf("ctor IOD, sem_init offline_tran_count is %d\n", sdbcallresults);
    if (sem_init(&offline_tran_count, 0, sdbcallresults))
    {
        //printf("ctor IOD, sem_init failed\n");
        fprintf(stderr, "semaphore initialize failed\n");
    }
    else
    {
        printf("ctor IOD, sem_init_offline_tran_count is ok\n");
    }

    unsigned int s;
    pthread_attr_init(&ZDTUThreadAttr);
    pthread_attr_getstacksize(&ZDTUThreadAttr, &s);
    //pthread_attr_setstacksize ( & ZDTUThreadAttr , const_thread_stack_size ());
    pthread_attr_setstacksize(&ZDTUThreadAttr, /*PTHREAD_STACK_MIN*/ 65536 );

	{
		int tpolicy;
		struct sched_param param;

		tpolicy = SCHED_FIFO;
		param.sched_priority = SY_PRIO_LOW;

		pthread_attr_setinheritsched(&ZDTUThreadAttr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&ZDTUThreadAttr, tpolicy);
		pthread_attr_setschedparam(&ZDTUThreadAttr, &param);

	}

    int ret = 0;
    ret = pthread_create(&ZDTUThread, &ZDTUThreadAttr, RunOfflineSenderFunction, NULL);

    if (ret != 0)
    {
        if (ret == EAGAIN)
            printf("OfflineSender Thread : Out of Memory\n");

        else if (ret == EINVAL)
            printf("OfflineSender : Invalid Attribute\n");
        printf("OfflineSenderThread : Failed to Create OfflineSenderThread\n");
    }
	else
	{

	}
}

void *SY7000Demo::RunOfflineSenderFunction( void* id )
{
    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());
    int row = 0, col = 0;
    SqliteDB sqlitedb;
    char ** res;

    int otc_value = 0;
    int sem_result = 0;

	XMLProperties parser;

	offline_thread_is_started = 2;

    while (true)
    {
        if (OperationIODriver::communication)
        {
        	int scount = 0 ;
        	sem_getvalue(&offline_tran_count,&scount);
        	DLOGI("%s %d offline sem count = %d\n",__FUNCTION__,__LINE__,scount);
        	sem_wait (&offline_tran_count);
        	DLOGI("%s %d offline sem wakeup\n",__FUNCTION__,__LINE__);

        	if(!OperationIODriver::communication){sem_post (&offline_tran_count); continue;}

            OperationIODriver::OfflineModeIsActive = true;
            DP("offlinemode is active\n");
            res = sqlitedb.offlineTran_tbl.SelectTable(NULL, 10, &row, &col);
            DP("Offline Mode table, row = %d col = %d \n", row, col);

            for ( int i = 1; i < (row + 1); i++ )
            {

                DP("offline trans type is %s\n", res[i * col]);

                if (strcmp("ClockIn",res[i*col]) == 0)
                {
                    //printf("Try Send Offline ClockIn date = %s badge = %s\n",res[i*col+1],res[i*col+2]);
                    CURLcode resHttp;
                    //
                    //XMLProperties parser;
            		//LOCKXM;
                    OffLineTranRecord
                    R(res[i*col],res[i*col+1],res[i*col+2],res[i*col+3],res[i*col+4],res[i*col+5],
                                    res[i*col+6],res[i*col+7]);

                    //printf("offline clockin called\n");

					if(DontSendPunches==false)
					{

						pthread_mutex_lock(&(OperationIODriver::offline_request_mutex));
                    	httpOffline->SwipePunchWithActivityCode(ClockId, Account,
                            res[i*col+2],
                            res[i*col+6],
                            res[i*col+1], SOAP_1_1_VER,&resHttp);



					}
					else
					{
						resHttp = CURLE_OK;
					}

                    //if (!resHttp)
                    //    DP("response = %s\n", httpOffline->chunk.memory);

                    if (!resHttp && httpOffline->chunk.memory  && strlen(httpOffline->chunk.memory))
                    {
                        //printf("offline clockin returns 0. Process...\n");





                        char status[2];
                        status[0] = '\0';
						if(DontSendPunches==false)
						{
                        	parser.GetProperty(httpOffline->chunk.memory, "Status", status);
                        	pthread_mutex_unlock(&(OperationIODriver::offline_request_mutex));
						}
						else
						{
							status[0] = '1'; status[1]='\0';
						}

                        if (strlen(status) > 0)
                        {
                            if (sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
                            {
                                sem_post(&offline_tran_count);
                                printf("failed to delete row\n");
                            }
                            else
                            {
                                //printf("row deleted\n");
                            }
                        }
                        else
                        {
                             //exception state
                        	 FILE* tf=fopen ("ExceptionTrans.txt","a");
                        	 if(tf)
                        	 {
                        	 	fprintf(tf,"%s|%s|%s|%s|%s|%s|%s|%s\n",
                        	 				res[i*col],res[i*col+1],res[i*col+2],res[i*col+3],res[i*col+4],res[i*col+5],res[i*col+6],res[i*col+7]);
                        	 	fclose(tf);
                        	 }
                        	 else
                        	 {
                        	 		DLOGI("%s %d error - can't open ExceptionTrans.txt file\n",__FUNCTION__,__LINE__);
                        	 }
                            if (sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
                            {
                                sem_post(&offline_tran_count);
                            }
                        }
                  }
                  else //send failed
                  {
                	  if(DontSendPunches==false)
                		  pthread_mutex_unlock(&(OperationIODriver::offline_request_mutex));
                      //printf("offline clockin returns !0, failed to send, call sem_post\n");
                      sem_post(&offline_tran_count);
                      /*printf("failed send ClockIn Offline Tran\n");*/
                  }
                    //pthread_mutex_unlock (&soap_request_mutex);
            		//UNLOCKXM;


                }


               /* if ( (strcmp("GetActivities",res[i*col]) == 0) || (strcmp("GetGroups",res[i*col]) == 0) )
                {
                    //printf("offline GetActivities called\n");
                }*/

//                if (strcmp("InPunch",res[i*col]) == 0)
//                {
//                    //printf("offline InPunch called\n");
//                    //				printf("Try Send Offline InPunch date = %s badge = %s\n",res[i*col+1],res[i*col+2]);
//                    CURLcode resHttp;
//                    //
//                    //XMLProperties parser;
//                    LOCKXM;
//					parser = new XMLProperties();
//                    //printf("111111\n");
//                    OffLineTranRecord
//                    R(res[i*col],res[i*col+1],res[i*col+2],res[i*col+3]);
//                    //printf("222222  %s %s\n",ClockId,Account);
//                    //pthread_mutex_lock (&soap_request_mutex);
//					if(DontSendPunches==false)
//					{
//						httpOffline->InPunch(ClockId,Account,res[i*col+2],res[i*col+1],res[i*col+3],SOAP_1_1_VER,&resHttp);
//					}
//					else
//					{
//						resHttp = CURLE_OK;
//					}

//                    //printf("333333\n");
//                    //if(!resHttp)printf("response = %s\n",httpOffline->chunk.memory);
//                    //printf("444444\n");
//                    if (!resHttp)
//                    {
//                        //printf("offline Inpunch returns 0. Process...\n");
//                        char status[2];
//                        status[0] = '\0';
//						if(DontSendPunches==false)
//						{
//                        	parser->GetProperty(httpOffline->chunk.memory, "Status", status);
//						}
//						else
//						{
//							status[0]='1'; status[1]='\0';
//						}

//                        //printf("444444a\n");
//                        if (strlen(status) > 0)
//                            {
//                            if (sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
//                                {
//                                sem_post(&offline_tran_count); /*printf("failed to delete row\n");*/
//                                }
//                            //else
//                            //printf("row deleted\n");
//                            }
//                        else
//                            {
//                            //exception state
//                            if (sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
//                                {
//                                sem_post(&offline_tran_count);
//                                }
//                            }
//                    }
//                    else
//                    {
//                        //printf("offline InPunch returns !0, failed to send, call sem_post\n");
//                        sem_post(&offline_tran_count);
//                        /*printf("failed send Offline Tran\n");*/
//                    }
//                    //pthread_mutex_unlock (&soap_request_mutex);

//					delete parser;
//                  	UNLOCKXM;

//                }

//                if (strcmp("OutPunch",res[i*col]) == 0)
//                {
//                    //printf("Try Send Offline OutPunch\n");
//                    CURLcode resHttp;
//                    //XMLProperties parser;
//                    LOCKXM;
//					parser = new XMLProperties();
//                    OffLineTranRecord
//                    R(res[i*col],res[i*col+1],res[i*col+2]);
//                    //
//                    //pthread_mutex_lock (&soap_request_mutex);
//                    //printf("offline OutPunch called\n");
//					if(DontSendPunches==false)
//					{
//						httpOffline->OutPunch(ClockId,Account,res[i*col+2],res[i*col+1],SOAP_1_1_VER,&resHttp);
//					}
//					else
//					{
//						resHttp = CURLE_OK;
//					}

//                    //if(!resHttp)printf("response = %s\n",httpOffline->chunk.memory);
//                    if (!resHttp)
//                        {
//                        //printf("offline OutPunch returns 0. Process...\n");
//                        char status[2];
//                        status[0] = '\0';
//						if(DontSendPunches==false)
//						{
//							parser->GetProperty(httpOffline->chunk.memory, "Status", status);
//						}
//						else
//						{
//							status[0] = '1'; status[1] = '\0';
//						}

//                        if (strlen(status) > 0)
//                            {
//                            if (sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
//                                {
//                                sem_post(&offline_tran_count); /*printf("failed to delete row\n");*/
//                                }
//                            //else
//                            //printf("row deleted\n");
//                            }
//                        else
//                            {
//                            //exception state
//                            if (sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
//                                {
//                                sem_post(&offline_tran_count);
//                                }
//                            }
//                        }
//                    else
//                        {
//                        //printf("offline OutPunch returns !0, failed to send, call sem_post\n");
//                        sem_post(&offline_tran_count);
//                        /*printf("failed send Offline Tran\n");*/
//                        }
//                    //pthread_mutex_unlock (&soap_request_mutex);

//					delete parser;
//                    UNLOCKXM;


//                }

                if (strcmp("SwipePunch",res[i*col]) == 0)
                {
                    //printf("Try Send Offline SwipePunch1\n");
                    CURLcode resHttp;
                    //XMLProperties parser;
                    //LOCKXM;
                    OffLineTranRecord
                    R(res[i*col],res[i*col+1],res[i*col+2]);
                    //
                    //pthread_mutex_lock (&soap_request_mutex);
                    //				printf("Try Send Offline SwipePunch utcTime = %s\n",res[i*col+1]);
                    //printf("offline SwipePunch called\n");
					if(DontSendPunches==false)
					{
						pthread_mutex_lock(&(OperationIODriver::offline_request_mutex));
						httpOffline->SwipePunch(ClockId,Account,res[i*col+2],res[i*col+1],SOAP_1_1_VER,&resHttp);
					}
					else
					{
						resHttp = CURLE_OK;
					}

                    //printf("Try Send Offline SwipePunch3 utcTime = %s\n",res[i*col+1]);
                    //if (!resHttp)
                    //    printf("response = %s\n", httpOffline->chunk.memory);

                    //printf("Try Send Offline SwipePunch4\n");
                    if (!resHttp && httpOffline->chunk.memory && strlen(httpOffline->chunk.memory))
                    {
                        //printf("offline SwipePunch returns 0. Process...\n");
                        char status[2];
                        status[0] = '\0';
                        //printf("Try Send Offline SwipePunch5\n");
						if(DontSendPunches==false)
						{
							parser.GetProperty(httpOffline->chunk.memory, "Status", status);
							pthread_mutex_unlock(&(OperationIODriver::offline_request_mutex));
						}
						else
						{
							status[0]='1'; status[1]='\0';
						}

                        if (strlen(status) > 0)
                        {
                            if (sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
                                {
                                sem_post(&offline_tran_count);
								DP("OLDB: failed to delete row\n");
                                }
                            //else
                            //printf("row deleted\n");
                        }
                        else
                        {
                        	//exception state
                        	FILE* tf=fopen ("ExceptionTrans.txt","a");
                        	if(tf)
                        	{
                        	   fprintf(tf,"%s|%s|%s\n",res[i*col],res[i*col+1],res[i*col+2]);
                        	   fclose(tf);
                        	 }
                        	 else
                        	 {
                        	    DLOGI("%s %d error - can't open ExceptionTrans.txt file\n",__FUNCTION__,__LINE__);
                        	 }
                             if (sqlitedb.offlineTran_tbl.DeletRows(&R) != SQLITE_OK)
                             {
                                sem_post(&offline_tran_count);
                             }
                        }
                    }
                    else
                    {
                    	if(DontSendPunches==false)
                    		pthread_mutex_unlock(&(OperationIODriver::offline_request_mutex));
                        sem_post(&offline_tran_count);
                        /*printf("failed send Offline Tran\n");*/
                    }
                    //UNLOCKXM;

                }

                /*((if (strcmp("Validate",res[i*col]) == 0)
                {
                }*/

                //
                // just print out transaction types
                //			printf("offlinesender transaction #%d\t\ttype= %s\n", i, res[i*col] );

                if (!OperationIODriver::communication)
                {
                    /*pthread_mutex_unlock (&soap_request_mutex);*/
                    //printf("OfflineSender. Break if communication is OFF during update.????\n");

					SY7000Demo::ok_for_next_thread = true;

                    break;
                }

                if (i < row)
                {
                    //printf("step thru offline transaction\n");
                    sem_getvalue(&offline_tran_count, &otc_value);
                    DP("%s %d after row, before wait %d, sem count is %d",__FUNCTION__,__LINE__,i, otc_value);
                    sem_wait(&offline_tran_count);
                    sem_getvalue(&offline_tran_count, &otc_value);
                    DP("%s %d after wait %d, sem count is %d", __FUNCTION__,__LINE__,i, otc_value);
                }
                //pthread_mutex_unlock (&soap_request_mutex);

                //httpOffline->count++; //done in request function

                if (httpOffline->count > 100)
                {
                    //printf("offline sender, reseat http.\n");
                	httpOffline->count = 0;
                    httpOffline->resetHttp();
					pthread_yield();
                }
                //sleep(2);
				pthread_yield();
            }
            DP("offline mode is Not active, Update done.\n");
            OperationIODriver::OfflineModeIsActive = false;
			SY7000Demo::ok_for_next_thread = true;

        }
        else
        {
            DP("offline mode is Not active, no comm.\n");
            OperationIODriver::OfflineModeIsActive = false;
            DP("Disconnected OffLineMode\n");
            pthread_mutex_lock(&OperationIODriver::offline_request_mutex);

			SY7000Demo::ok_for_next_thread = true;

            DP("OfflineSender: waiting for signal...\n");
            pthread_cond_wait(&offline_cond_flag, &OperationIODriver::offline_request_mutex);
            pthread_mutex_unlock(&OperationIODriver::offline_request_mutex);
            DP("OfflineSender: signaled to wakeup.\n");
        }
    }
}

void SY7000Demo::GetServerTimeFunction()
{
	printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());

	char status[2];
	char message[25];
	char buffer[25];
	int lenMsg;

    XMLProperties parser;

	CURLcode res = (CURLcode)1;
    //struct timespec clockTime;
    //int freq = 0;


    res = (CURLcode)1;
    //reqCount++;

    if (OperationIODriver::communication)
    {
		//DP("getlocaltime: get lock on soap mutex\n");
        //pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
		DP("getlocaltime: Make the LocalTime Request");
        httpS->LocalTime(ClockId, Account, SOAP_1_1_VER, &res);
        //httpS->count++;

        //LOCKXM;

        status[0] = '\0';


        if (!res && httpS->chunk.memory && strlen(httpS->chunk.memory) )
        {

                //
                // input date string:
                //
                //		.net or java: 	currdts = DateTime.Now.ToString("yyyy-MM-dd\THH:mm:ss.fffzzz")
                //		(same as clock app utcTime format)
                //
                //						<string>2009-07-22T06:46:26.984-07:00</string>
                //
                // convert to linux date format:  MMDDhhmm[[CC]YY][.ss]]
                //
                // TimeZone not supported yet.

                //char message[25];
                message[0] = '\0';
                //char buffer[25] = "date ";
                parser.GetProperty(httpS->chunk.memory, "LocalTimeResult", message);
                lenMsg = strlen(message);
                if (lenMsg > 0)
                {
                    char month[3], day[3], hour[3], min[3], year[5], sec[2];
                    month[0] = '\0'; day[0] = '\0'; hour[0] = '\0'; min[0] = '\0'; year[0] = '\0'; sec[0] = '\0';

                    snprintf(year,5,"%s",message); //include '\0'

                    if(lenMsg > 4)
                    {
                    	snprintf(month,3,"%s",message + 5); //include '\0'
                    }

                    if(lenMsg > 7)
                    {
                    	snprintf(day,3,"%s",message + 8); //include '\0'
                    }

                    if(lenMsg > 10)
                    {
                    	snprintf(hour,3,"%s",message + 11); //include '\0'
                    }

                    if(lenMsg > 13)
                    {
                    	snprintf(min,3,"%s",message + 14); //include '\0'
                    }

                    if(lenMsg > 16)
                    {
                    	snprintf(sec,3,"%s",message + 17); //include '\0'
                    }

                    snprintf(buffer,25,"date %s%s%s%s%s.%s",month,day,hour,min,year,sec);

                    printf("%s %s %s %s %s %s mass = %s buffer = %s\n", month, day, hour, min, year, sec, message,
                        buffer);
					DP("getlocaltime: call system Date\n");
                    system(buffer);
					DP("getlocaltime: call system hwclock -w\n");
                    system("hwclock -w");

                }
          }
          else
          {
			DP("getlocaltime: request failed, code = %d\n", res);
          }

          //UNLOCKXM;

		  /*SY7000Demo::ok_for_next_thread = true;

		  DP("getlocaltime: wait to be signaled\n");
          pthread_cond_wait(&OperationIODriver::getservertime_cond_flag,
                &OperationIODriver::soap_request_mutex); //from show IDLE we can wake the thread
		  DP("getlocaltime: wake up\n");
		  DP("getlocaltime: unlock soap mutex\n");
          pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);*/

    }
    else
    {
            user_output_clear();
            user_output_write(2, 1, "TimeSync    ");
            user_output_write(3, 1, "Not Available");
            sleep(2);

			/*DP("getlocaltime: get lock on soap mutex\n");
			pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
			DP("getlocaltime: soap mutex locked\n");
			SY7000Demo::ok_for_next_thread = true;
			DP("getlocaltime: wait to be signaled\n");
            pthread_cond_wait(&OperationIODriver::getservertime_cond_flag, &OperationIODriver::soap_request_mutex);
			DP("getlocaltime: wake up.\n");
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);*/
     }
}

// ----------------------------------  fingerprint management - Phase I

// httpclient calls:
//
// called by FPEnroll():
//char* SetFPTemplate(char* ClockId ,char* Account,char* empId, char* utcTime, char* templateId, char* fptemplate, char* soapVer,CURLcode* res);
//
//char* PopulateFPTemplates(char* ClockId ,char* Account, char* soapVer,CURLcode* res);
//char* TemplatesAvailable(char* ClockId ,char* Account, char* utcTime,char* soapVer,CURLcode* res);

void SY7000Demo::RunTemplatesAvailableThread()
    {
    pthread_t templatesAvailableThread;
    pthread_attr_t templatesAvailableThreadAttr;

    unsigned int s;
    pthread_attr_init(&templatesAvailableThreadAttr);
    pthread_attr_getstacksize(&templatesAvailableThreadAttr, &s);

    pthread_attr_setstacksize(&templatesAvailableThreadAttr, PTHREAD_STACK_MIN + 16384 );

	{
		int tpolicy;
		struct sched_param param;

		tpolicy = SCHED_FIFO;
		param.sched_priority = SY_PRIO_MEDIUM;

		pthread_attr_setinheritsched(&templatesAvailableThreadAttr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&templatesAvailableThreadAttr, tpolicy);
		pthread_attr_setschedparam(&templatesAvailableThreadAttr, &param);

	}


    int ret = 0;
    ret = pthread_create(&templatesAvailableThread, &templatesAvailableThreadAttr, RunTemplatesAvailableFunction, NULL);

    if (ret != 0)
        {
        if (ret == EAGAIN)
            printf("TemplatesAvailable Thread : Out of Memory\n");

        else if (ret == EINVAL)
            printf("TemplatesAvailable Thread: Invalid Attribute\n");
        printf("TemplatesAvailableThread : Failed to Create TemplatesAvailableThread");
        }
    }

void *SY7000Demo::RunTemplatesAvailableFunction( void* id )
    {
    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());
    CURLcode res = (CURLcode)1;
    //struct timespec clockTime;
    //int freq = 0;

    int nMutexOpResult = 0;

    //time_t optime;
    //struct tm* optimeinfo;
    //time(&optime);
    //optimeinfo = localtime(&optime);
    //bool send_transaction = false;
    //char dateB[22];
    //char timeB[22];
    //dateB[0] = '\0';
    //timeB[0] = '\0';
    //strftime(timeB, 21, "T%H:%M:%S", optimeinfo); // get the local time in format "T%H:%M:%S" for example T10:30:45
    //strftime(dateB, 21, "%Y-%m-%d", optimeinfo);  // get the local date in format "%Y-%m-%d" for example 2008-05-23
    char utcTime[50];
    utcTime[0] = '\0';

	struct tm currTime;
	Operation::SYClock_GetTime(&currTime);
	strftime(utcTime, 20, "%Y-%m-%dT%H:%M:%S", &currTime);

    //strcpy(utcTime, dateB);
    //strcat(utcTime, timeB); // the date and time we send with the request for example "2008-05-23T10:30:45"

    char TemplatesAvailableTimerStr[15];
    TemplatesAvailableTimerStr[0] = '\0';
    // get the timer frequency from  the xml file "properties.xml"
    XMLProperties confParser("properties.xml");
    confParser.GetProperty("TemplatesAvailableTimer", TemplatesAvailableTimerStr);

    // get the timer of the get activities in seconds
    if (strlen(TemplatesAvailableTimerStr) > 0)
        TemplatesAvailableFrequency = atol(TemplatesAvailableTimerStr);
    else
        TemplatesAvailableFrequency = 86400; // 24 hours


    while (true)
        {
        res = (CURLcode)1;
        //reqCount++;

        if (OperationIODriver::communication)
            {
            DP("taop: comm=true. lock soap_request_mutex\n");
            nMutexOpResult = pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
            DP("taop: comm=true. lock result is %d\n", nMutexOpResult);
            httpS->TemplatesAvailable(ClockId, Account, utcTime, SOAP_1_1_VER, &res);
            //httpS->count++;
            DP("taop: TA returns\n");


            //XMLProperties parser;
            char status[2];
            status[0] = '\0';

            if (!res && httpS->chunk.memory)
                {
                //XMLProperties parser;
                char message[25];
                message[0] = '\0';
                //char buffer[25] = "";
                //parser.GetProperty(httpS->chunk.memory,"Status",message);
                char* s;
                s = &httpS->chunk.memory[0];

                while (*s != 0)
                    {
                    if (*s == '\'')
                        * s = '"';
                    s++;
                    }
                DP("new chunk memory: \n %s\n", httpS->chunk.memory);

                XMLProperties taParser;
                taParser.GetProperty(httpS->chunk.memory, "Status", message);


                // chunk.memory no longer needed, release
                //DP("taop: comm=true. unlock soap_request_mutex before calling ptop\n");

                //				nMutexOpResult = pthread_mutex_unlock (&OperationIODriver::soap_request_mutex);
                //DP("taop: comm=true. unlock result for soap_request_mutex is %d\n", nMutexOpResult);

                if (strlen(message) > 0)
                    {
                    if (strcmp(message, "1") == 0)
                        {
                        DP("taf: templatesAvailable status is 1\n");

                        DP("taf: 1 before lock populatetemplates_mutex\n");
                        pthread_mutex_lock(&OperationIODriver::populatetemplates_mutex);
                        DP("taf: 1 populatetemplates_mutex locked. call PT\n");
                        PopulateTemplates();
                        DP("taf: 1 PT returns\n");

                        DP("taf: 1 before unlock populatetemplates_mutex\n");
                        pthread_mutex_unlock(&OperationIODriver::populatetemplates_mutex);
                        DP("taf: 1 after unlock populatetemplates_mutex\n");
                        }
                    }
                }
            else
                {
                // note: mutexes not locked at this point
                DP("taf: 0 templatesAvailable status is 0\n");
                DP("taf: 0 before unlock populatetemplates_mutex\n");
                //pthread_mutex_unlock (&OperationIODriver::soap_request_mutex);
                DP("taf: 0 after unlock populatetemplates_mutex\n");
                }

            DP("taf: fallthru, before unlock soap_request_mutex\n");
            //pthread_mutex_unlock (&OperationIODriver::soap_request_mutex);
            nMutexOpResult = pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
            DP("taf: fallthru, comm=true. unlock result for soap_request_mutex is %d\n", nMutexOpResult);

            // go back to sleep
            DP("taf: go to sleep\n");
            sleep(TemplatesAvailableFrequency);
            continue;
            }
        else
            {
//pthread_cond_wait(&OperationIODriver::templatesavailable_cond_flag,&OperationIODriver::soap_request_mutex);
            DP("taf: no comm. before unlock soap_request_mutex\n");
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
            DP("taf: no comm. unlocked soap_request_mutex\n");
            DP("taf: no comm. go to sleep\n");
            sleep(TemplatesAvailableFrequency);
            }
        } // endwhile
    }

void SY7000Demo::RunPopulateTemplatesThread()
{
    pthread_t PFPUThread;
    pthread_attr_t PFPUThreadAttr;

    unsigned int s;
    pthread_attr_init(&PFPUThreadAttr);
    pthread_attr_getstacksize(&PFPUThreadAttr, &s);
    //pthread_attr_setstacksize(&PFPUThreadAttr, const_thread_stack_size());
    pthread_attr_setstacksize ( & PFPUThreadAttr , PTHREAD_STACK_MIN + 128*1024);

	{
		int tpolicy;
		struct sched_param param;

		tpolicy = SCHED_FIFO;
		param.sched_priority = SY_PRIO_HIGH;

		pthread_attr_setinheritsched(&PFPUThreadAttr, PTHREAD_EXPLICIT_SCHED);
		pthread_attr_setschedpolicy(&PFPUThreadAttr, tpolicy);
		pthread_attr_setschedparam(&PFPUThreadAttr, &param);

	}

    int ret = 0;
    ret = pthread_create(&PFPUThread, &PFPUThreadAttr, RunPopulateTemplatesFunction, NULL);

    if (ret != 0)
        {
        if (ret == EAGAIN)
            printf("PopulateTemplates Thread : Out of Memory\n");

        else if (ret == EINVAL)
            printf("PopulateTemplates Thread: Invalid Attribute\n");
        printf("RunPopulateTemplatesThread : Failed to Create RunPopulateTemplatesThread");
        }
    }

bool bStartingUp = true;

void *SY7000Demo::RunPopulateTemplatesFunction( void* id )
{
    // Phase I
    //
    // current mode:
    // run once and then wait for signal to run again
    //
    // optional modes:
    //
    // * only run if signaled (initial wait, looped wait)

    //
    //
    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());

    CURLcode res = (CURLcode)1;
    //struct timespec clockTime;
    //int freq = 0;

	FILE * fOut = NULL;
	//XMLProperties parser;
	char status[2];
    char message[200];
    message[0] = '\0';

    while (true)
	{
        res = (CURLcode)1;
        //reqCount++;

        printf("%s %d\n",__FUNCTION__,__LINE__);
        if (OperationIODriver::communication && !bStartingUp)
        {

			fpOpr.fpOperation = Ignore_Operation;
			printf("%s %d\n",__FUNCTION__,__LINE__);
			pthread_mutex_lock(&OperationIODriver::waitforpopulatetemplates_mutex);
			printf("%s %d\n",__FUNCTION__,__LINE__);

			//

			pthread_mutex_lock( &OperationIODriver::lockDisplay );
			printf("%s %d\n",__FUNCTION__,__LINE__);
			user_output_clear();
			#ifdef USE_GETACT_FUNCTION
			user_output_write(1,1,"Populate Templates");
			user_output_write(2,1,"Call GetActivities");
			DoGetActivitiesFunction();		// run and die
			user_output_write(1,1,"Populate Templates");
			user_output_write(2,1,"GetActivities Done");
			sleep(2);
			#endif
			//
			user_output_clear();
            user_output_write(1,1,"Populate Templates");
            user_output_write(2,1,"Sending Request");

			DLOGN("RunPopulateTemplates  has begun ...");

            DP("RunPopulateTemplates: thread running after Non-Startup Signal\n");
            //DP("RunPopulateTemplates. lock soap_request_mutex\n");
			DLOGN("RunPopulateTemplates. lock soap_request_mutex");
//
// possible area of trouble?
// is mutex already locked????
            // origianl : pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
			if (pthread_mutex_trylock (&OperationIODriver::soap_request_mutex) )
			{
			    DLOGN("RunPopulateTemplates: ... soap_request_mutex already locked...");
				DLOGN("RPT: try again for 20 seconds...");
				{
					int tc = 1;
					int mr = 0;
					bool deadoralive = false;
					do
					{
						mr = pthread_mutex_trylock(&OperationIODriver::soap_request_mutex);
						switch(mr)
						{
							case 0 :		// ok. got it!
								DLOGW("RPT: Got the lock on the soap_request_mutex");
								deadoralive = true;
								break;
							case EINVAL:
								DLOGW("RPT: trylock on soap_request_mutex returns EINVAL");
								deadoralive = false;
								break;
							case EBUSY :
								DLOGW("RPT: trylock on soap_request_mutex returns EBUSY");
								deadoralive = false;
								break;
							case EAGAIN :
								DLOGW("RPT: trylock on soap_request_mutex returns EAGAIN");
								deadoralive = false;
								break;
							case EDEADLK : // only for lock
								DLOGW("RPT: trylock on soap_request_mutex returns EDEADLK");
								deadoralive = false;
								break;
							case EPERM :	// only for unlock
								DLOGI("RPT: trylock on soap_request_mutex returns EPERM");
								deadoralive = false;
								break;
							default:
								DLOGW("RPT: trylock on soap_request_mutex returns unknown code");
								deadoralive = false;
								break;
						}
						sleep(1);
						tc++;
						if ( deadoralive == true )
						{
							break; // the do..while looop
						}
					} while (tc <= 20);
					if ( deadoralive == true )
					{
						DLOGN("RunPopulateTemplates: ... locked soap_request_mutex...");
					}
					else
					{
						// can't get it. give up. blow.
						DLOGN("RunPopulateTemplates: Could not get lock on soap_request_mutex");
						goto waittobesignaled;
					}
				}
			}
			else
			{
				DLOGN("RunPopulateTemplates: ... locked soap_request_mutex...");
			}

//			// since extended memory is probably not being used, remove previous file
			DLOGN("Remove previous httptmp.tmp file");
			unlink(FILE_HTTP_FP);
			DLOGN("Previous httpfp.tmp file removed");

			fOut = fopen(FILE_HTTP_FP, "w");
			if ( fOut == NULL )
			{
				//TODO: could not open file
				//DP("testing: RunPopulateTemplates could not open fp file for writing\n");
				DLOGN("RunPopulateTemplates: could not open fp file for writing.");
			}
			else
			{

				DLOGN("RunPopulateTemplates: open file to receive templates from server.");
				DLOGN("RunPopulateTemplates: receiving file from server ...");
				//httpS->PopulateFPTemplates(ClockId, Account, SOAP_1_1_VER, &res);
				httpS->PopulateFPTemplatesFile(ClockId, Account, SOAP_1_1_VER, &res, fOut,300);
				DLOGN("RunPopulateTemplates: PopulateFPTemplatesFile returned result = %d",res);
                
				// keep this open until curl code is checked.  fclose(fOut);

				status[0] = '\0';


	////////////	process http response in file

				//fOut = fopen(FILE_HTTP_FP, "r");
				user_output_write(2,1,"Processing Request");
				if (OperationIODriver::communication && res == CURLE_OK && fOut != NULL )
				{
					DLOGN("RunPopulateTemplates: received templates file from server.");
					DLOGN("RunPopulateTempaltes: close templates file.");
					fclose(fOut);
					DP("testing poptemps: check httpfp file\n");
					DLOGN("RunPopulateTemplates: reopen templates file for reading.");
					fOut = fopen(FILE_HTTP_FP,"r");
					if ( fOut == NULL )
					{
						// file read error.
						//DP("RunPopulateTemplates: error reading templates file.\n");
						DLOGN("RunPopulateTemplates: error reading templates file.");
					}
					else
					{
						DLOGN("RunPopulateTemplates. call UpdateTemplatesInFPU\n");
            	        //UpdateTemplatesInFPU(httpS->chunk.memory, true); // Yes. do display progress
						UpdateTemplatesInFPUFromFile(fOut, true); // Yes. do display progress
						DLOGN("RunPopulateTemplates. done UpdateTemplatesInFPU\n");

//						user_output_write(2,1,"Enrollment Done.   ");
//						user_output_write(3,1,"Wait one moment... ");
						user_output_write(2,1,"Enrollment Done.   ");
						user_output_write(3,1,"Wait 2 min for ");
						user_output_write(4,1,"FPU Maintenance");

						fclose(fOut);

						sync();

						DLOGN("RunPopulateTemplates Sleep for 2 minutes, FPU maintenance.");

						sleep(60*2);

						DLOGN("RunPopulateTemplates Done Sleeping.");

						DLOGN("RunPopulateTemplates is done. Reset FPU and-or Clock...");


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


 						{ // reset fpu

							int repeatStatusCheck = 0;
							UF_RET_CODE ufret = UF_RET_SUCCESS;
							//UF_BYTE packet[13];
							UF_BYTE packet[15];
							//bool mustReboot = false;
////////////////////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/////////////////////////////////////////////////////////////
							// just reset :
							bool mustReboot = true;
////////////////////////!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!////////////////////////////////////////////////////////////


							if ( (ufret = uf_send_packet(0xd0,0,0,0) ) < 0 )
							{
								printf("FPError: could not send reset packet #1 : %d\n", ufret);
								mustReboot = true;
							}
							else
							{
								// Sent reset command. check if ok.
								// sleep(5);
								if ( (ufret = uf_receive_packet(packet) ) < 0 )
								{
									printf("FPError: could not receive reset response. Try again. : %d\n", ufret);
									if ( (ufret = uf_send_packet(0xd0,0,0,0)) < 0 )
									{
										printf("FPError: could not send 2nd reset packet\n");
								 		mustReboot = true;
									}

									sleep(2);
								}
								else
								{

// after the reset, wait until fpu is ready?
									printf("check system status after Reset...\n");
									UF_SYSTEM_STAUTS strm; // yes, it should be UF_SYSTEM_STATUS
									uf_check_system_status(&strm);
									repeatStatusCheck = 0;
									while ( strm != UF_SYS_ALIVE )
									{
										switch ( strm )
										{
											case UF_SYS_WAIT : printf("... FPU is Waiting...\n"); break;
											case UF_SYS_FAIL : printf("... FPU has failed.\n"); break;
											case UF_SYS_SENSOR_FAIL : printf("... FPU Sensor has failed.\n"); break;
											case UF_SYS_BUSY : printf("... FPU is busy.\n"); break;
											default: 		   printf("... FPU status is unknown. code = %d\n", strm); break;
										}
										sleep(1);
										uf_check_system_status(&strm);
										repeatStatusCheck++;
										if ( repeatStatusCheck > 20 )
										{
											mustReboot = true;
											DLOGE("RunPopulateTemplates: Must reboot after unknown state of FPU");
											system("reboot");
										}
									}
									printf("... FPU is Alive.\n");



									// does the above send need a receive packet response?
									// note:	after this is sent, the fpu may return an error after the
									//          the 1st reset.  Reset again. (testing)
									sleep(1);

									// make sure these parameters are set (for this client's application)
									uf_write_system_parameter(UF_SYS_AUTO_RESPONSE, 0x30);//autoresponse = OFF
									uf_write_system_parameter(UF_SYS_FREE_SCAN, 0x30);// free scan = OFF
		    	    				uf_write_system_parameter(UF_SYS_ENROLL_MODE, 0x30); // free scan = ON
									printf("RunPopulateTemplates: FPU successfully reset \n");
									uf_set_read_timeout(5000);	// ???
								}
							}

							if ( mustReboot == true )
							{
								DLOGN("FPError: Could not reset FPU. Must reboot.");

								user_output_write(1,1,"Enrollment Done.   ");
								user_output_write(2,1,"Cannot reset FPU   ");
								user_output_write(3,1,"Must Restart Clock ");
								user_output_write(4,1,"in 10 seconds.     ");
								sleep(10);
								DLOGN("RunPopulateTemplates: Finished. Reboot after updating FPU");
								system("reboot");
							}

						} // end reset fpu

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

						user_output_write(2,1,"Enrollment Done.   ");
						user_output_write(3,1,"Finished.          ");
						DLOGN("RunPopulateTemplates Finished");
						unlink(FILE_HTTP_FP);
						sleep(3);
					}


				}
            	else
            	{
            		DLOGN("RunPopulateTemplates: error receiving templates file");
					if (fOut != NULL )
					{
						DLOGN("RunPopulateTempaltes: close templates file opened for writing.");
						fclose(fOut);
					}
					if ( OperationIODriver::communication == false )
					{
						DLOGN("RunPopulateTemplates: Could not receive templates file: no communication");
					}
					if ( res != CURLE_OK )
					{
						sprintf(message, "RunPopulateTemplates: Could not receive templates file, code = %d", res);
						DLOGN(message);
					}

            	}

			} // if fp file is opened ok


waittobesignaled:

            // done with this thread, signal the waiting thread (Utilities menu or TemplatesAvailable)
DLOGN("RunPopulateTemplates. waiting _mutex is unlocked. Done with this thread.");
pthread_mutex_unlock(&OperationIODriver::waitforpopulatetemplates_mutex);

			// is this cond_signal needed?
//			int pcs;
//            DP("RunPopulateTemplates. before signal waitforpopulatetemplates\n");
//            pcs = pthread_cond_signal(&OperationIODriver::waitforpopulatetemplatesthread_cond_flag);
//            //			pcs = pthread_cond_broadcast(&OperationIODriver::waitforpopulatetemplatesthread_cond_flag);
//            DP("RunPopulateTemplates. result for signal waitforpopulatetemplatesthread_cond_flag is %d\n", pcs);

			pthread_mutex_unlock( &OperationIODriver::lockDisplay );
			SY7000Demo::ok_for_next_thread = true;

			DLOGN("RunPopulateTemplates. wait to be signaled.");

            pthread_cond_wait(&OperationIODriver::populatetemplatesthread_cond_flag,
                &OperationIODriver::soap_request_mutex);

            DP("RunPopulateTemplates. done waiting. signaled.\n");
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
            DP("RunPopulateTemplates. unlocked soap mutex\n");
            bStartingUp = false;

            continue;
        }
        else
            {
            bStartingUp = false;
            DP("RunPopulateTemplates, startup, will wait to be signaled. signal waitforpopulatetemplatesthread\n");

			DP("RunPopulateTemplates, startup, try to get lock on mutex before wait\n");
			pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
            DP("RunPopulateTemplates. startup, soap_request_mutex is locked.\n");

			SY7000Demo::ok_for_next_thread = true;

			DP("RunPopulateTemplates, wait for signal...\n");
			DLOGN("RunPopulateTemplates, wait for signal")
            pthread_cond_wait(&OperationIODriver::populatetemplatesthread_cond_flag,
                &OperationIODriver::soap_request_mutex);

            DP("RunPopulateTemplates, signaled\n");
            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
            DP("RunPopulateTemplates, unlocked soap mutex\n");

            if (OperationIODriver::communication == false)
                {
                user_output_clear();
                user_output_write(2, 1, "PopulateTemplates");
                user_output_write(3, 1, "Not Available");
                sleep(2);
				DLOGI("RunPopulateTemplates: no communication after being signaled. Not available");
                }
            }

        } // endwhile

    return (void *)NULL;
}

/////
//
// 2/2/2010 Do not use until code is matched up with RunPopulateTemplatesFunction
// Standalone routine.
// this routine should perform the same tasks as RunPopulateTemplatesFunction

void *SY7000Demo::PopulateTemplates()
    {

    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());
	DLOGI("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());
    CURLcode res = (CURLcode)1;

    char message[200];
    message[0] = '\0';

    res = (CURLcode)1;

    if (OperationIODriver::communication)
        {
        DP("is soap_request_mutex already locked here:\n");
        DP("poptemps: lock soap_request_mutex...\n");

        //		if (pthread_mutex_trylock (&OperationIODriver::soap_request_mutex) )
        //		{
        //			DP("poptemps: ... soap_request_mutex already locked...\n");
        //		}
        //		else
        //		{
        //			DP("poptemps: ... locked soap_request_mutex...\n");
        //		}

        DP("poptemps: ... get lock on soap_request_mutex...\n");
        pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
        DP("poptemps: ... locked soap_request_mutex\n");

        httpS->PopulateFPTemplates(ClockId, Account, SOAP_1_1_VER, &res);
        //httpS->count++; // <-- ???? //done in request function

        //XMLProperties parser;
        //XMLProperties* taParser = new XMLProperties();
        //taParser->GetProperty(httpS->chunk.memory,"Status",message);
        //delete taParser;

        char status[2];
        status[0] = '\0';

        if (OperationIODriver::communication && res == CURLE_OK && httpS->chunk.memory)
		{
            char* begintag = NULL;
            char* endtag = NULL;
            char* cdatabegin = NULL;
            char* cdataend = NULL;

            begintag = strstr(httpS->chunk.memory, "<PopulateFPTemplatesResult>");

            if (begintag != NULL)
                {
                endtag = strpbrk(begintag, ">");

                if (endtag != NULL)
                    {
                    cdatabegin = endtag + 1; // next char
                    begintag = strstr(cdatabegin, "</PopulateFPTemplatesResult>");

                    if (begintag != NULL)
                        {
                        endtag = strpbrk(begintag, ">");

                        if (endtag != NULL)
                            {
                            cdataend = begintag - 1;
                            message[0] = 'O';
                            message[1] = 'K';
                            message[2] = '\0';
                            }
                        else
                            {
                            // not well-formed xml
                            message[0] = '\0';
                            }
                        }
                    else
                        {
                        // not well-formed xml
                        message[0] = '\0';
                        }
                    }
                else
                    {
                    // not well-formed xml
                    message[0] = '\0';
                    }
                }
            else
                {
                begintag = strstr(httpS->chunk.memory, "<PopulateFPTemplatesResult />");

                if (begintag != NULL)
                    {
                    // empty element
                    // no templates available
                    DP("PopulateFPTemplatesResult is empty element : No Templates\n");
                    }
                else
                    {
                    // element not found
                    // no templates available
                    DP("PopulateFPTemplatesResult element not found: No Templates\n");
                    }
                message[0] = '\0';
                }
//bypassPFPT3:
            if (strlen(message) > 0)
                {
                DP("poptemps: call updatetemps\n");
                UpdateTemplatesInFPU(httpS->chunk.memory, false); // yes, display progress
                DP("poptemps: updatetemps returns\n");
                }
        }
        else
        {
            //TODO: handle case of no templates
            // error sending PFPT request, offline?
        }

        DP("poptemps: unlock soap_request_mutex...\n");
        pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
        DP("poptemps: ... unlocked soap_request_mutex...\n");
        }
    else
        {
        // no communication
        //TODO: handle this case
        }
    return (void *)NULL;
    }

#define ADD_TEMPLATES_LIST  "PopulateFPTemplatesResult"
#define LIST_ELEMENT    "User"
#define LIST_SUBELEMENT_KEY "BadgeId"
#define LIST_SUBELEMENT_DATA    "Template"

int SY7000Demo::UpdateTemplatesInFPU( const char *xmlAddUsersList, bool displayprogress )
    {

    DLOGI("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());
    XMLProperties parser;
    int from, to;
	//int id;
    from = 1;
    to = 10;
    char tempUserID[30];
    list<ElmData> &addUsersList = parser.getListElementData();
    int countElm;
    Base64 base64;
    OperationStatusCode code = SC_Success;

    do
    {
        parser.GetListElements(xmlAddUsersList, ADD_TEMPLATES_LIST, LIST_ELEMENT, from, to);
        list<ElmData>::iterator i;
        list<ElmData>::iterator j;
        countElm = 0;
        j = addUsersList.end();

DP("Enrolling user .");

        for ( i = addUsersList.begin(); i != j; )
            {
            char* userTemplates[10];

            for ( int j = 0; j < 10; j++ )
                userTemplates[j] = NULL;

            int UserIndex = (*i).elmIndexInList;
            int templateNum = 0;

            while (UserIndex == (*i).elmIndexInList) // scan all the sub elements belong to main element "elmIndexInList"
            {
                if (strcmp(LIST_SUBELEMENT_KEY,(*i).key) == 0)  // the User Id sub element
                    strcpy(tempUserID,(*i).data);

                if (strcmp(LIST_SUBELEMENT_DATA,(*i).key) == 0) // the User template sub element
                {
                    if (templateNum < 10)
                    {
                        userTemplates[templateNum] = (char *)malloc(ENCODED_TEMPLATESIZE + 1);
                        strcpy(userTemplates[templateNum],(*i).data);
                        templateNum++;
                    }
                }

                if (i == addUsersList.end())
                    break;
                i++;
            }

            unsigned long int UserIDNum = strtoul(tempUserID, NULL, 0);
            UF_RET_CODE Result;

            // if need to display enroll progress:
            if (displayprogress == true)
                {
                user_output_write(2, 1, " Enroll: ");
				user_output_write(2, 9, "                    "); // clear the line with 20 spaces
				user_output_write(2, 9, tempUserID);
                }

            fpOpr.DeleteUser(UserIDNum, &Result);

	{
		// enroll mode - One Time = single template
		//
		uf_write_system_parameter(UF_SYS_ENROLL_MODE, 0x30);
	}

            unsigned int s = FP_TEMPLATESIZE;
            unsigned char template_after_decode[FP_TEMPLATESIZE + 1];

			code = SC_Success;
			int j = 0;

//DP("Enrolling user %ld ... ", UserIDNum);
DP(".");


            for ( j = 0; j < 10; j++ )
            {
                if (userTemplates[j] != NULL)
                {
                    base64.decode(userTemplates[j], strlen(userTemplates[j]), template_after_decode, s);
                    const char *template_final = reinterpret_cast<const char *>(template_after_decode);

                    if ((fpOpr.AddUser(UserIDNum, (UF_BYTE *)template_final, s, &Result)) != SC_Success)
					{
                        code = SC_Fail;
					}
					else
					{
						//DP("%d ", j);
						DP(".");
					}
                    free(userTemplates[j]);
                }
				if ( code == SC_Fail )
				{
					break; // the for loop
				}
            }

			// free the rest of the 10 templates if AddUser had returned SC_Fail
			if ( j < 10 && code == SC_Fail )
			{
				while ( j < 10 )
				{
					free(userTemplates[j]);
					j++;
				}
			}
DP("\n");
            countElm++;
        }
        from = from + countElm;
        to = from + 9;
    } while ( countElm == 10 );

    return code;
}

int SY7000Demo::UpdateTemplatesInFPUFromFile( FILE *xmlTemplatesFile, bool displayprogress )
{
    UF_RET_CODE Result;

	DLOGI("UpdateTemplates: first, remove all templates from the fingerprint unit.");

	fpOpr.DeleteAllUsers(&Result);
	usleep(500);

	DLOGI("UpdateTemplates: done removing all templates before sync");



// enable this if there are problems detected:
	DLOGI("UpdateTemplates: reset fingerprint unit after removing all users...");
	{
		// reset fpu after deleting all users
		//
		UF_BYTE rmpkt[15];
		UF_RET_CODE rmres2;
		bool rmres;
		rmres2 = uf_send_packet(0xd0,0,0,0);
		if ( rmres2 < 0 )
		{
			//printf("...failed to send reset\n");
			rmres = false;
		}
		else
		{
			//printf("...send reset: ok\n");
			rmres2 = uf_receive_packet(rmpkt);             				// <---- is this necessary?
			if ( rmres2 < 0 )
			{
				//printf("...failed to receive reset packet response\n");
				rmres = false;
				usleep(500);
			}
		}
		sleep(3);

		uf_write_system_parameter(UF_SYS_AUTO_RESPONSE, 0x30);//autoresponse = OFF
		uf_write_system_parameter(UF_SYS_FREE_SCAN, 0x30);// free scan = OFF
		//uf_write_system_parameter(UF_SYS_ENROLL_MODE, 0x30); // enroll mode = single

		{
			printf("check system status after Remove All Templates...");
			UF_SYSTEM_STATUS strm;
			uf_check_system_status(&strm);
			while ( strm != UF_SYS_ALIVE )
			{
				switch ( strm )
				{
					case UF_SYS_WAIT : printf("... FPU is Waiting...\n"); break;
					case UF_SYS_FAIL : printf("... FPU has failed.\n"); break;
					case UF_SYS_SENSOR_FAIL : printf("... FPU Sensor has failed.\n"); break;
					case UF_SYS_BUSY : printf("... FPU is busy.\n"); break;
					default: 		   printf("... FPU status is unknown.\n"); break;
				}
				sleep(1);
				uf_check_system_status(&strm);
			}
			printf("... FPU is Alive.\n");
		}

		DLOGI("UpdateTemplates: Done: reset fingerprint unit, after removing all users");

	}

	{
		// enroll mode - One Time = single template
		//
		uf_write_system_parameter(UF_SYS_ENROLL_MODE, 0x30);
		usleep(500);
	}

	DLOGI("UpdateTemplates: begin processing the templates file");
	XML_ParseFP(xmlTemplatesFile);
	DLOGI("UpdateTemplates: done processing the templates file");
	return SC_Success;
}


//
//Get the finger print template data from the xml
//
void XML_ParseFP(FILE * fIn)
{
	//FPM * fp;
	//Base64 * dec;
	Base64 dec;
	char * cbuff = new char[1024];
	//int cbufflen = 0;
	//char * cbuff;
	//cbuff = (char *)malloc(1024);
	//
	XML_Parser p = XML_ParserCreate(NULL);
	XML_UDFP xmlUD;
	memset(&xmlUD,0,sizeof(XML_UDFP));
	XML_SetUserData(p, &xmlUD);
	XML_SetElementHandler(p,XML_StartFP,XML_EndFP);
	XML_SetCharacterDataHandler(p,XML_CharFP);

	// TODO: Remove All Users from Fingerprint Unit

	//DP("xml_parsefp: before new base64\n");
	//dec = new Base64();
	//DP("xml_parsefp: after new base64 \n");
	//xmlUD.fp = fp;
	xmlUD.dec = &dec;
	//int cbufflimit = 60;
	while(fgets(cbuff,1024,fIn) != NULL)
	{
		XML_Parse(p,cbuff,strlen(cbuff),false);

//		cbufflimit--;
//		if ( cbufflimit == 0 )
//		{
//			break;
//		}

	}
	cbuff[0] = '\0';
	XML_Parse(p,cbuff,0,true);
	//delete fp;
	//delete dec;
	delete[] cbuff;
	//free(cbuff);
	//
	XML_ParserFree(p);
}

//
//callback when element is started
//
static void XMLCALL XML_StartFP(void *data, const char *el, const char **attr)
{
	XML_UDFP * ud = (XML_UDFP *)data;
	if(strcasecmp(el,"BadgeId")==0)
	{
		strcpy(ud->Elm,el);
		ud->Badge[0] = '\0';
		ud->Templ[0] = '\0';
   }
   else if(strcasecmp(el,"Template")==0)
   {
  		strcpy(ud->Elm,el);
		ud->Templ[0] = '\0';
   }
	else if(strcasecmp(el,"User")==0)
   {
		strcpy(ud->Elm,el);
		ud->Badge[0] = '\0';
		ud->Templ[0] = '\0';
   }
	else{//ignore
		ud->Badge[0] = '\0';
		ud->Templ[0] = '\0';
		ud->Elm[0] = '\0';
	}
}
//
//Callback when element ends
//
static void XMLCALL XML_EndFP(void *data, const char *el)
{
	unsigned int b64size = FP_TEMPLATESIZE;
	unsigned char * b64buf = new unsigned char[FP_TEMPLATESIZE + 1]; // 384+1 , try 400
	//unsigned char * b64buf = (unsigned char*)malloc(400); // 384+1 , try 400

	char vs3[128];
	char vs2[128];
	char vs1[128];

	XML_UDFP * ud = (XML_UDFP *)data;

	if(strcasecmp(el,"Template")==0)
	{
		//unsigned int b64size = FP_TEMPLATESIZE;
		//unsigned char * b64buf = new unsigned char[FP_TEMPLATESIZE + 1];
		//ud->dec->decode(ud->Templ,(unsigned int)512,b64buf,b64size);
		ud->dec->decode(ud->Templ,(unsigned int)512,b64buf,(size_t&)b64size);
		//printf("Bdg = %s / Enroll = %d\n", ud->Badge,ud->fp->Enroll( strtoul(ud->Badge,NULL,10) , b64buf, b64size));
		//TODO: Enroll the id/template

		//DP("todo: Enroll template for userid of %s\n", ud->Badge);

/////////////////////////////////////////////////////////////////////////////////

			unsigned long int UserIDNum = strtoul(ud->Badge, NULL, 0);
            UF_RET_CODE Result;


            user_output_write(2, 1, " Enroll: ");
			user_output_write(2, 9, "                    "); // clear the line with 20 spaces
			user_output_write(2, 9, ud->Badge);

			int retry_enroll = 0;
			//UF_BYTE packet[13];
			UF_BYTE packet[15];

            unsigned int s = FP_TEMPLATESIZE;
            //unsigned char template_after_decode[FP_TEMPLATESIZE + 1];

			//code = SC_Success;
			//int j = 0;

			retry_enroll = 0;


DP("Enrolling user %ld ... ", UserIDNum);

//            base64.decode(userTemplates[j], strlen(userTemplates[j]), template_after_decode, s);
//            const char *template_final = reinterpret_cast<const char *>(template_after_decode);

retryEnrollment:

            //if ((fpOpr.AddUser(UserIDNum, (UF_BYTE *)template_final, s, &Result)) != SC_Success)
			if ((fpOpr.AddUser(UserIDNum, (UF_BYTE *)b64buf, s, &Result)) != SC_Success)
			{
                //code = SC_Fail;
				DP(" Failed.\n");
				if ( Result == UF_ERR_CHECKSUM_ERROR || Result == UF_ERR_READ_SERIAL_TIMEOUT )
				{
					uf_set_read_timeout( 5000 );
					printf("   Reset FPU and retry enrollment\n");
                    {
						//char vs1[128];
						sprintf(vs1, "Error enrolling Badge #: %ld\n", UserIDNum );
						DLOGE(vs1);
                    }

					uf_cancel();
					Result = uf_send_packet( 0xd0, 0, 0, 0 );
					if ( Result < 0 )
					{
						//printf("*** could not send Reset Module packet, code = %d\n", Result);
						//FPUError = (unsigned long int)Result;
						//FPUEnrollError = (unsigned long int)Result;
					}
					sleep(5);

					Result = uf_receive_packet(packet);
					if ( Result < 0 )
					{
						//printf("*** could not receive reset packet response\n");
						sleep(5);
						// try again 1 more time
						Result = uf_send_packet( 0xd0, 0, 0, 0 );
						if ( Result < 0 )
						{
							//printf("*** could not send 2nd Reset Module packet, code = %d\n", Result);
							//FPUError = (unsigned long int)Result;
							//FPUEnrollError = (unsigned long int)Result;
							sleep(5);
						}
					}

					uf_write_system_parameter(UF_SYS_AUTO_RESPONSE, 0x30);//autoresponse = OFF
					uf_write_system_parameter(UF_SYS_FREE_SCAN, 0x30);// free scan = OFF
    				uf_write_system_parameter(UF_SYS_ENROLL_MODE, 0x30); // free scan = ON

					uf_set_read_timeout(5000);

					uf_cancel();
					//FPUError = 0;
					//FPUEnrollError = 0;
					if ( retry_enroll < 3 )
					{
						retry_enroll++;
                    	{
							//char vs2[128];
							sprintf(vs2, "Retry enrolling Badge #: %ld. Retry\n", UserIDNum );
							DLOGI(vs2);
                    	}
						goto retryEnrollment;
					}
					else
					{
                    	{
							//char vs3[128];
							sprintf(vs3, "Could not enroll Badge #: %ld. Retry\n", UserIDNum );
							DLOGE(vs3);
                    	}
					}
					sleep(2);

				}
				else
				{
					if ( Result == UF_ERR_SCAN_FAILED )
					{
						// why this error? the template gets stored properly. verified with ufcmd CT and RT util
						DP(" Success.\n");
					}
					else
					{
						// some other error?
						//char vs3[128];
						sprintf(vs3, "Could not enroll Badge #: %ld. code:%d\n", UserIDNum, Result );
						DLOGE(vs3);
					}
				}

			}
			else
			{
				DP(" Success.\n");
			}

//			if ( code == SC_Fail )
//			{
//				// break; // the for loop
//			}


/////////////////////////////////////////////////////////////////////////////////


		//delete[] b64buf;
	}
	delete[] b64buf;
	//free(b64buf);
}
//
//callback for character data
//
static void XMLCALL XML_CharFP(void *data, const char *s, int len)
{
	XML_UDFP * ud = (XML_UDFP *)data;
	int l = 0;
	if(strcasecmp(ud->Elm,"BadgeId")==0)
	{
		l = strlen(ud->Badge);
		memcpy(&(ud->Badge[l]),s,len);
		ud->Badge[l+len]='\0';
	}else if(strcasecmp(ud->Elm,"Template")==0)
	{
		l = strlen(ud->Templ);
		memcpy(&(ud->Templ[l]),s,len);
		ud->Templ[l+len]='\0';
	}
}


#define USER_TEMPLATES_LIST  "GetFPTemplateResult"
#define USER_LIST_ELEMENT    "User"
#define USER_LIST_SUBELEMENT_KEY "BadgeId"
#define USER_LIST_SUBELEMENT_DATA    "Template"

// verify_by_host_template version
int SY7000Demo::VerifyUserFromServerTemplates( const int UserBadge, const char *xmlUserTemplateList, UF_BYTE *liveTemplate )
{

    XMLProperties parser;
    int from, to;
	//int id;

	////////////////////////////////////////////////////
	// only the first user element is needed.
    from = 1;
    to = 1;
	///////////////////////////////////////////////////

    char tempUserID[30];
	
    list<ElmData> &userTemplateList = parser.getListElementData();
	
    int countElm;
    Base64 base64;
    OperationStatusCode code = SC_Success;

	bool foundmatch = false;

//testing
	DP("UserBadge for match against is %d\n", UserBadge);
//	DP("VUfromserver: xmlusertemplatelist is %s\n", xmlUserTemplateList);
//endtest
    do
    {
		//DLOGI("%s %d",__FUNCTION__,__LINE__);
		parser.GetListElements(xmlUserTemplateList, USER_TEMPLATES_LIST, USER_LIST_ELEMENT, from, to);
		//DLOGI("%s %d",__FUNCTION__,__LINE__);
        list<ElmData>::iterator i;
        list<ElmData>::iterator j;
        countElm = 0;
        j = userTemplateList.end();


		if ( userTemplateList.size() <= 0 )
		{
			// no user and/or no templates for this BadgeId
			code = SC_Fail;
			break; // get out of the Do/While loop
		}

        char* userTemplates[10];			// maximum of ten templates can be defined for each user: FPU hardware requirement

        for ( i = userTemplateList.begin(); i != j; )		// should only be one iteration for USER_LIST_ELEMENT
        {
			//DLOGI("%s %d key = %s data = %s",__FUNCTION__,__LINE__,(*i).key,(*i).data);
            for ( int k = 0; k < 10; k++ )
                userTemplates[k] = NULL;

            int UserIndex = (*i).elmIndexInList;
//testing
			DP("VUfromserver: UserIndex is %d\n", UserIndex);
//endteset
            int templateNum = 0;

			int templateSizeInBase64 = 0;
            while (UserIndex == (*i).elmIndexInList) // scan all the sub elements belong to main element "elmIndexInList"
            {
				//DLOGI("%s %d",__FUNCTION__,__LINE__);
                if (strcmp(USER_LIST_SUBELEMENT_KEY,(*i).key) == 0)  // the User Id sub element
                    strcpy(tempUserID,(*i).data);
				//DLOGI("%s %d",__FUNCTION__,__LINE__);

//testing
			DP("template found for user #%s\n", tempUserID);
//endtest

			    //DLOGI("%s %d USER_LIST_SUBELEMENT_DATA = %s key = %c",__FUNCTION__,__LINE__,USER_LIST_SUBELEMENT_DATA,((*i).key)[0]);
                if (strcmp(USER_LIST_SUBELEMENT_DATA,(*i).key) == 0) // the User template sub element
                {
					//DLOGI("%s %d\n",__FUNCTION__,__LINE__);
                    if (templateNum < 10)
                    {
						//DLOGI("%s %d templateSizeInBase64 = %d",__FUNCTION__,__LINE__,templateSizeInBase64);
						templateSizeInBase64 = strlen((*i).data);
						DLOGI("%s %d templateSizeInBase64 = %d",__FUNCTION__,__LINE__,templateSizeInBase64);
						if(templateSizeInBase64 > ENCODED_TEMPLATESIZE) // 20 byte for empy charachter 
						{
							DLOGI("ERROR - template over the limit size - byte in base64 = %d",templateSizeInBase64);
						}
						else
						{
							userTemplates[templateNum] = (char *)malloc(ENCODED_TEMPLATESIZE + 1);
							snprintf(userTemplates[templateNum],ENCODED_TEMPLATESIZE + 1,"%s", (*i).data);
							//strcpy(userTemplates[templateNum],(*i).data);
							templateNum++;
						}
                    }
                }
				//DLOGI("%s %d",__FUNCTION__,__LINE__);
                if (i == userTemplateList.end())
                    break;
                i++;
            }

//testing
			templateNum++; // make it 1-based
			DP("user has %d templates\n", templateNum);
//endtest

            //unsigned long int UserIDNum = strtoul(tempUserID, NULL, 0);
            //UF_RET_CODE Result;


            unsigned int s = FP_TEMPLATESIZE;
            unsigned char template_after_decode[FP_TEMPLATESIZE + 1];

			unsigned char * templatelist;

			templatelist = (unsigned char *)malloc(FP_TEMPLATESIZE * templateNum);

            for ( int j = 0; j < 10; j++ )
            {
                if (userTemplates[j] != NULL)
                {

					// decode and build contiguous fingerprint template list

					if(userTemplates[j] != NULL)
					{
						//DLOGI("%s %d",__FUNCTION__,__LINE__);
						base64.decode(userTemplates[j],strlen(userTemplates[j]),template_after_decode,s);
						//DLOGI("%s %d",__FUNCTION__,__LINE__);
						//const char * template_final = reinterpret_cast<const char *>(template_after_decode);
						//if((fpOpr.AddUser(UserIDNum,(UF_BYTE*)template_final,s,&Result)) != SC_Success ) code = SC_Fail;

						memcpy(templatelist+(FP_TEMPLATESIZE*j), template_after_decode, s);

					}
                }
            }


			// TODO: uf_verify_host_template_by_scan
  			uf_cancel();
			uf_set_read_timeout ( 15000 );

			int tn = 0;
			tn = templateNum - 1;

			UF_RET_CODE res = uf_verify_host_template_by_scan( (UF_INT32)tn, (UF_BYTE *)templatelist, (UF_INT32)FP_TEMPLATESIZE );
			DLOGI("%s %d uf_verify_host_template_by_scan result = %d",__FUNCTION__,__LINE__,res);
			if(res == UF_RET_SUCCESS)
			{
				foundmatch = true;
			}
			else
			{
				DP("verify_host_template_by_scan fails with code %d", res);
				foundmatch = false;
			}

			uf_cancel();

			free(templatelist);

            countElm++;
        }


		// free memory held by templates
        for ( int j = 0; j < 10; j++ )
        {
            if (userTemplates[j] != NULL)
            {
				free(userTemplates[j]);
			}
		}


        from = from + countElm;
        to = from + 9;
    } while ( countElm == 10 );

//testing
	DP("VUfromserver: elements counted as a user is %d\n", countElm);
//endtest

	if ( foundmatch == true )
	{
		code  = SC_Success;
	}
	else
	{
		code = SC_Fail;
	}

	return code;

}



bool SY7000Demo::UserNeedsFPV( OperationIODriver* oper )
{
	bool needsfpv = true;


#ifdef USE_SELECTIVE_FPV

	//					badgeid from keypad requires fingerprint verification
	//					badgeid from prox card requires no verification

    if (oper->getSourceBadge() == SysInputSourceProximity)
    {
        // proximity card reader requires no further verification
		needsfpv = useCardFPV;
    }
	else if ( (oper->getSourceBadge() & SysInputSourceMagnetic ) == SysInputSourceMagnetic)
	{
		// any mag or prox reader requires no further verification
		needsfpv = useCardFPV;
	}
	else if ( (oper->getSourceBadge() & SysInputSourceProxMag ) == SysInputSourceProxMag)
	{
		// any mag or prox reader requires no further verification
		needsfpv = useCardFPV;
	}
	else if (oper->getSourceBadge() == SysInputSourceKeypad)
    {
		// keypad input requires fingerprint verification
		needsfpv = useKeypadFPV;
    }
	else
	{
		// default to use fingerprint verification
		needsfpv = true;
	}

#endif

	return needsfpv;

}

// not used
//void SY7000Demo::RunOnceGetPinTableThread()
//{
//    pthread_t PGPTThread;
//    pthread_attr_t PGPTThreadAttr;

//    unsigned int s;
//    pthread_attr_init(&PGPTThreadAttr);
//    pthread_attr_getstacksize(&PGPTThreadAttr, &s);
//    //pthread_attr_setstacksize(&PGPTThreadAttr, const_thread_stack_size());
//    pthread_attr_setstacksize ( & PGPTThreadAttr , PTHREAD_STACK_MIN + 128*1024);

//	{
//		int tpolicy;
//		struct sched_param param;

//		tpolicy = SCHED_FIFO;
//		param.sched_priority = SY_PRIO_HIGH;

//		pthread_attr_setinheritsched(&PGPTThreadAttr, PTHREAD_EXPLICIT_SCHED);
//		pthread_attr_setschedpolicy(&PGPTThreadAttr, tpolicy);
//		pthread_attr_setschedparam(&PGPTThreadAttr, &param);

//	}

//    int ret = 0;
//    ret = pthread_create(&PGPTThread, &PGPTThreadAttr, RunOnceGetPinTableFunction, NULL);

//    if (ret != 0)
//        {
//        if (ret == EAGAIN)
//            printf("RunOnceGetPinTable Thread : Out of Memory\n");

//        else if (ret == EINVAL)
//            printf("RunOnceGetPinTableThread: Invalid Attribute\n");
//        printf("RunOnceGetPinTableThread : Failed to Create RunOnceGetPinTableThread");
//        }
//}

// not used:
//void *SY7000Demo::RunOnceGetPinTableFunction( void* id )
//{
//    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());

//    CURLcode res = (CURLcode)1;
//    bool bStartingUp = true;
//	FILE * fOut = NULL;
//	char status[2];
//    char message[200];
//    message[0] = '\0';

//    while (true)
//    {
//        res = (CURLcode)1;

//        if (OperationIODriver::communication && !bStartingUp)
//        {
//			fpOpr.fpOperation = Ignore_Operation;
//			pthread_mutex_lock(&OperationIODriver::waitforpopulatetemplates_mutex);
//			user_output_clear();

//user_output_clear();
//user_output_write(1,1,"Get Pin Table ... ");
//user_output_write(2,1,"Sending Request");

//			DLOGI("RunOnceGetPinTable has begun ...");

//            DP("RunOnceGetPinTable: thread running after Non-Startup Signal\n");
//			DLOGI("RunOnceGetPinTable. lock soap_request_mutex\n");

//			if (pthread_mutex_trylock (&OperationIODriver::soap_request_mutex) )
//			{
//			    DLOGI("RunOnceGetPinTable: ... soap_request_mutex already locked...\n");
//				DLOGI("RPT: try again for 20 seconds...");
//				{
//					int tc = 1;
//					int mr = 0;
//					bool deadoralive = false;
//					do
//					{
//						mr = pthread_mutex_trylock(&OperationIODriver::soap_request_mutex);
//						switch(mr)
//						{
//							case 0 :		// ok. got it!
//								DLOGW("RPT: Got the lock on the soap_request_mutex");
//								deadoralive = true;
//								break;
//							case EINVAL:
//								DLOGW("RPT: trylock on soap_request_mutex returns EINVAL");
//								deadoralive = false;
//								break;
//							case EBUSY :
//								DLOGW("RPT: trylock on soap_request_mutex returns EBUSY");
//								deadoralive = false;
//								break;
//							case EAGAIN :
//								DLOGW("RPT: trylock on soap_request_mutex returns EAGAIN");
//								deadoralive = false;
//								break;
//							case EDEADLK : // only for lock
//								DLOGW("RPT: trylock on soap_request_mutex returns EDEADLK");
//								deadoralive = false;
//								break;
//							case EPERM :	// only for unlock
//								DLOGI("RPT: trylock on soap_request_mutex returns EPERM");
//								deadoralive = false;
//								break;
//							default:
//								DLOGW("RPT: trylock on soap_request_mutex returns unknown code");
//								deadoralive = false;
//								break;
//						}
//						sleep(1);
//						tc++;
//						if ( deadoralive == true )
//						{
//							break; // the do..while looop
//						}
//					} while (tc <= 20);
//					if ( deadoralive == true )
//					{
//			    		DLOGI("RunOnceGetPinTable: ... locked soap_request_mutex...\n");
//					}
//					else
//					{
//						// can't get it. give up. blow.
//						DLOGW("RunOnceGetPinTable: Could not get lock on soap_request_mutex");
//						goto waittobesignaled;
//					}
//				}
//			}
//			else
//			{
//			    DLOGI("RunOnceGetPinTable: ... locked soap_request_mutex...");
//			}

////			// since extended memory is probably not being used, remove previous file
//			DLOGI("Remove previous httppin.tmp file");
//			unlink(FILE_HTTP_PIN);
//			DLOGI("Previous httppin.tmp file removed");

//			fOut = fopen(FILE_HTTP_PIN, "w");
//			if ( fOut == NULL )
//			{
//				//TODO: could not open file
//				//DP("testing: RunOnceGetPinTable could not open fp file for writing\n");
//				DLOGE("RunOnceGetPinTable: could not open fp file for writing.");
//			}
//			else
//			{

//				DLOGI("RunOnceGetPinTable: open file to receive templates from server.");
//				DLOGI("RunOnceGetPinTable: receiving file from server ...");
//				httpS->GetPinTableFromFile(ClockId, Account, SOAP_1_1_VER, &res, fOut);
//				DLOGI("RunOnceGetPinTable: GetPinTable returned\n");

//				// keep this open until curl code is checked.  fclose(fOut);

//				status[0] = '\0';


//	////////////	process http response in file

//				//fOut = fopen(FILE_HTTP_FP, "r");
//				user_output_write(2,1,"Processing Request");
//				if (OperationIODriver::communication && res == CURLE_OK && fOut != NULL )
//				{
//					DLOGI("RunOnceGetPinTable: received templates file from server.");
//					DLOGI("RunPopulateTempaltes: close templates file.");
//					fclose(fOut);
//					DP("testing GetPinTable: check httppin file\n");
//					DLOGI("RunOnceGetPinTable: reopen pin table file for reading.");
//					fOut = fopen(FILE_HTTP_PIN,"r");
//					if ( fOut == NULL )
//					{
//						// file read error.
//						//DP("RunOnceGetPinTable: error reading templates file.\n");
//						DLOGE("RunOnceGetPinTable: error reading pin table file.");
//					}
//					else
//					{
//            	    	DP("RunOnceGetPinTable. parse PinTable\n");
//						UpdatePinTableInDBFromFile(fOut);
//            	        DP("RunOnceGetPinTable. done parsing PinTable\n");

//						user_output_write(2,1,"Update Done.   ");
//						user_output_write(3,1,"                   ");
//						user_output_write(4,1,"                   ");

//						fclose(fOut);


//						DLOGI("RunOnceGetPinTable is done.");

//						sleep(3);
//					}

//				}
//            	else
//            	{
//					DLOGE("RunOnceGetPinTable: error receiving pin table file");
//					if (fOut != NULL )
//					{
//						DLOGI("RunPopulateTempaltes: close pin table file opened for writing.");
//						fclose(fOut);
//					}
//					if ( OperationIODriver::communication == false )
//					{
//						DLOGE("RunOnceGetPinTable: Could not receive pin table file: no communication");
//					}
//					if ( res != CURLE_OK )
//					{
//						sprintf(message, "RunOnceGetPinTable: Could not receive pin table file, code = %d", res);
//						DLOGE(message);
//					}

//            	}

//			} // if fp file is opened ok


//waittobesignaled:

//            // done with this thread, signal the waiting thread (Utilities menu or TemplatesAvailable)
//DLOGI("RunOnceGetPinTable. waiting _mutex is unlocked. Done with this thread.\n");
//pthread_mutex_unlock(&OperationIODriver::waitforGetPinTable_mutex);

//			SY7000Demo::ok_for_next_thread = true;

//			DLOGI("RunOnceGetPinTable. wait to be signaled.\n");

//            pthread_cond_wait(&OperationIODriver::GetPinTablethread_cond_flag,
//                &OperationIODriver::soap_request_mutex);

//            DP("RunOnceGetPinTable. done waiting. signaled.\n");
//            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
//            DP("RunOnceGetPinTable. unlocked soap mutex\n");
//            bStartingUp = false;

//            continue;
//        }
//        else
//        {
//            bStartingUp = false;
//            DP("RunOnceGetPinTable, startup, will wait to be signaled. signal waitforGetPinTablethread\n");

//			DP("RunOnceGetPinTable, startup, try to get lock on mutex before wait\n");
//			pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
//            DP("RunOnceGetPinTable. startup, soap_request_mutex is locked.\n");

//			SY7000Demo::ok_for_next_thread = true;

//			DP("RunOnceGetPinTable, wait for signal...\n");
//			DLOGI("RunOnceGetPinTable, wait for signal")
//            pthread_cond_wait(&OperationIODriver::GetPinTablethread_cond_flag,
//                &OperationIODriver::soap_request_mutex);

//            DP("RunOnceGetPinTable, signaled\n");
//            pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
//            DP("RunOnceGetPinTable, unlocked soap mutex\n");

//            if (OperationIODriver::communication == false)
//            {
//                user_output_clear();
//                user_output_write(2, 1, "GetPinTable");
//                user_output_write(3, 1, "Not Available");
//                sleep(2);
//				DLOGI("RunOnceGetPinTable: no communication after being signaled. Not available");
//            }
//        }
//    } // endwhile

//    return (void *)NULL;
//}


#ifdef USE_PIN_SUPPORT

// non-threaded
//
void SY7000Demo::DoGetPinTableFunction()
{

	// perform the operation if there is communication, or just skip it

    printf("%s %d: %s %d\n", __FILE__, __LINE__, __FUNCTION__, getpid());

    CURLcode res = (CURLcode)1;
	FILE * fOut = NULL;

        if (OperationIODriver::OfflineModeIsActive == false)
        {
            res = (CURLcode)1;
            if (OperationIODriver::communication)
            {

				// since extended memory is probably not being used, remove previous file
				DLOGI("DoGetPinTable: Remove previous httppin.tmp file");
				unlink(FILE_HTTP_PIN);
				DLOGI("DoGetPinTable: Previous httppin.tmp file removed");

				DLOGI("DoGetPinTable: open file to receive Pin Table from server.");
				fOut = fopen(FILE_HTTP_PIN, "w");
				if ( fOut == NULL )
				{
					DLOGE("DoGetPinTable: could not open pin file for writing.");
				}
				else
				{

                	// clock the soap mutex request to send heartbeat.
                	pthread_mutex_lock(&OperationIODriver::soap_request_mutex);
                	DP("GetPinTable: soap mutex locked. GetPinTable()\n");
					DP("GetPinTable: Make GetPinTable Request\n");
					//
					// use file-based xml document processing
                	httpS->GetPinTableFromFile(ClockId, Account, SOAP_1_1_VER, &res, fOut); // send get project tasks web service
                	if (res == CURLE_OK  && fOut != NULL )
                	{
						DP("getact: GetPinTable Response ok.\n");
						DLOGI("DoGetPinTable: received pin table from server.");

						fclose(fOut); // close file opened for writing
						fOut = fopen(FILE_HTTP_PIN, "r"); //repopen for reading only
						if ( fOut == NULL )
						{
							DLOGE("DoGetPinTable: error reading pin table file.");
						}
						else
						{
							int dbret;
							// delete table first, then refill it thru the parsing
							SqliteDB sqlitedb;
							DP("DoGetPinTable: delete the existing table\n");
							dbret = sqlitedb.delete_table(TBL_PinTable);
							if ( dbret != SQLITE_OK )
							{
								DP("DoGetPinTable: could not delete pin table: %d\n", dbret);
							}
							DP("DoGetPinTable: re-create the existing table\n");
							dbret = sqlitedb.create_table(TBL_PinTable);
							if ( dbret != SQLITE_OK )
							{
								DP("DoGetPinTable: could not delete pin table: %d\n", dbret);
							}
system("cp TA TA2");
							DLOGI("DoGetPinTable: parse pin table file.");
							XML_ParsePIN(fOut);
							DLOGI("DoGetPinTable: Done parsing pin table file.");
						}
						fclose(fOut);

                	}
                	else
                	{
                	    DP("exception response in GetPinTable\n"); // no response was accepted
                	}

				}

            }
            else
            {
                // wait for communication
                DP("GetPinTable: no comm. skip\ngetact: unlock on soap mutex...\n");
                //pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);

				SY7000Demo::ok_for_next_thread = true;
                // no, just skip it. XcontinueX;
            }
        } // offline mode is actively performing tasks, wait unit it is done
        else
        {

            //pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
			SY7000Demo::ok_for_next_thread = true;
            // no, just skip it. Xcontinue;
        }

        DP("DoGetPinTable: unlock soap mutex...\n");
        pthread_mutex_unlock(&OperationIODriver::soap_request_mutex);
		DP("DoGetPinTable: soap_request_mutex unlocked\n");
        //DP("DoGetPinTable: goto sleep 2\n");

		SY7000Demo::ok_for_next_thread = true;

}
#endif // use_pin_support

#ifdef USE_PIN_SUPPORT
int UpdatePinTableInDBFromFile( char * BadgeId, char * VerifySource, char * PinCode)
{
	char fakename[2];
	PinTableRecord R;
	SqliteDB sqlitedb;

	DP("UpdatePinTable: copy values to pin table row struct\n");

	DP("UpdatePinTable, copy BadgeId\n");
	R.SetPinBadgeId(BadgeId);

	fakename[0] = '\0';
	fakename[1] = '\0';
	DP("UpdatePinTable, copy empty PinName\n");
	R.SetPinName(fakename);

	DP("UpdatePinTable, copy verifysource\n");
	R.SetPinVerifySource(VerifySource);

	DP("UpdatePinTable, copy PinCode\n");
	R.SetPinCode(PinCode);

	DP("UpdatePinTable: insert the row\n");
    if (sqlitedb.pin_tbl.InsertRow(&R) == SQLITE_OK)
    {
		DP("UpdatePinTable: row inserted\n");

    }
    else
	{
		DP("UpdatePinTable: failed to update row\n");
        return -1;
	}


	return 0;
}

#endif // use_pin_support


// not used
//int SY7000Demo::UpdatePinTableInDB( const char *pinXmlList )
//{
//    //printf ( "%s %d: %s %d\n" , __FILE__ , __LINE__ , __FUNCTION__ , getpid ());
//    XMLProperties parser;
//    int from, to, id;
//    SqliteDB sqlitedb;
//    list<int> savedPinRecords;

//	int countElm;
//	PinTableRecord R;
//	int MaxID;

//	list<ElmData> &pinList = parser.getListElementData();

//    from = 1;
//    to = 10;


//    do
//    {
//        parser.GetListElements(pinXmlList, "GetPinTableResult", "EmployeeInfo", from, to);
//        list<ElmData>::iterator i;
//        countElm = 0;

//        //	printf("befor for size = %d from = %d to = %d \n",actList.size(),from,to);
//        for ( i = pinList.begin(); i != pinList.end(); )
//        {
//            //PinTableRecord R;
//            fromPinTableListToRecord(pinList, i, R);

//            if (sqlitedb.pin_tbl.InsertRow(&R) == SQLITE_OK)
//                {
//                id = sqlitedb.pin_tbl.getID(&R);

//                if (id > 0)
//                    savedPinRecords.push_back(id);
//                }
//            else
//                return -1;
//            countElm++;
//        }
//        from = from + countElm;
//        to = from + 9;
//    } while ( countElm == 10 );

//    //int MaxID = sqlitedb.activities_tbl.getMaxID();
//	MaxID = sqlitedb.pin_tbl.getMaxID();
//    valarray<int> deletedPinRecords(true, MaxID + 1);
//    list<int>::iterator itr;

//    for ( itr = savedPinRecords.begin(); itr != savedPinRecords.end(); ++itr )
//        deletedPinRecords[*itr] = false;

//    for ( int i = MaxID; i > 0; i-- )
//        if (deletedPinRecords[i])
//            sqlitedb.pin_tbl.DeleteRow(i);
//    return 1;
//}

// not used
//void SY7000Demo::fromPinTableListToRecord( list<ElmData> &PinList, list<ElmData>::iterator &i, PinTableRecord &R )
//{
//    int pinIndex = (*i).elmIndexInList;

//    while (pinIndex == (*i).elmIndexInList)
//        {

//        if (strcmp("BadgeId",(*i).key) == 0)
//            R.SetPinBadgeId((*i).data);

//		if (strcmp("PinName",(*i).key) == 0)
//            R.SetPinName((*i).data);

//		if (strcmp("VerifySource",(*i).key) == 0)
//            R.SetPinVerifySource((*i).data);

//		if (strcmp("PinCode",(*i).key) == 0)
//            R.SetPinCode((*i).data);

//        if (i == PinList.end())
//            return;
//        i++;
//        }
//}

void XML_FindActivity(char * fileName,int actCode,char * actName)
{
    if(!actName)return;
    if(!fileName)return;
    FILE * fIn;

    fIn = fopen(fileName, "r");
    if ( fIn == NULL )
    {
    	DLOGE("%s %d can't open activities xml file",__FUNCTION__,__LINE__);
    	return;
    }

	char * cbuff = new char[512];
	XML_Parser p = XML_ParserCreate(NULL);
	XML_ACTIVITY xmlUD;
	memset(&xmlUD,0,sizeof(XML_ACTIVITY));
	XML_SetUserData(p, &xmlUD);
	XML_SetElementHandler(p,XML_StartActivities,XML_EndActivities);
	XML_SetCharacterDataHandler(p,XML_CharActivities);

	xmlUD.Code[0] ='\0';
	xmlUD.Name[0] = '\0';
	xmlUD.ReqCode = actCode;
	actName[0] = '\0';
	xmlUD.FoundNameAct = actName;

	//int cbufflimit = 60;
	while(fgets(cbuff,512,fIn) != NULL)
	{
		XML_Parse(p,cbuff,strlen(cbuff),false);

//		cbufflimit--;
//		if ( cbufflimit == 0 )
//		{
//			break;
//		}

	}
	cbuff[0] = '\0';
	XML_Parse(p,cbuff,0,true);
	delete[] cbuff;
	XML_ParserFree(p);
}

static void XMLCALL XML_StartActivities(void *data, const char *el, const char **attr)
{
	// only clear out the user-defined struct if the start-tag is EmployeeInfo
	// for each member of the user-defined struct, if the start-tag matches that member, do not clear out the other members.
	//
	//DP("XML_StartActivities: %s", el)
	XML_ACTIVITY * ud = (XML_ACTIVITY *)data;
	if(strcasecmp(el,"Name")==0)
	{
		strcpy(ud->Elm,el);
		ud->Name[0] = '\0';
		//ud->VerifySource[0] = '\0';
		//ud->PinCode[0] = '\0';
   }
   else if(strcasecmp(el,"Code")==0)
   {
  		strcpy(ud->Elm,el);
		//ud->BadgeId[0] = '\0';
		ud->Code[0] = '\0';
		//ud->PinCode[0] = '\0';
   }
   //this command heavy will cause application crash
   //DP("XML_StartActivities: %s", el);
}
//
//Callback when element ends
//
static void XMLCALL XML_EndActivities(void *data, const char *el)
{

	XML_ACTIVITY * ud = (XML_ACTIVITY *)data;

	// only process the user-defined data when the end-tag for EmployeeInfo is reached.

	//DP("XML_EndActivities: %s", el)
	if(strcasecmp(el,"Activity")==0)
	{
		//DP("name=%s code=%s ud->ReqCode = %d",ud->Name,ud->Code,ud->ReqCode);
		int codeInt = atoi(ud->Code);
		if(codeInt == ud->ReqCode)
		{
			if(ud->FoundNameAct)snprintf(ud->FoundNameAct,100,"%s",ud->Name);
		}
	}
	//DP("XML_EndActivities: %s", el)
	/*DP("XML_EndActivities: Endtag: %s\n", el);

	if ( strcasecmp(el, "Code") == 0)
	{
		DP("XML_EndActivities: Code is %s\n", ud->Code);
	}
	else if ( strcasecmp(el, "Name") == 0)
	{
		DP("XML_EndActivities: Name is %s\n", ud->Name);
	}*/

}
//
//callback for character data
//
static void XMLCALL XML_CharActivities(void *data, const char *s, int len)
{
	XML_ACTIVITY * ud = (XML_ACTIVITY *)data;
	int l = 0;
	//DP("XML_CharActivities: %s", ud->Elm);
	if(strcasecmp(ud->Elm,"Name")==0)
	{
		l = strlen(ud->Name);
		if(l+len < 100)
		{
			memcpy(&(ud->Name[l]),s,len);
			ud->Name[l+len]='\0';
		}
		else
		{
			DLOGI("ERROR %s %d Larg Activity Name",__FUNCTION__,__LINE__);
		}
	}else if(strcasecmp(ud->Elm,"Code")==0)
	{
		l = strlen(ud->Code);
		if(l+len < 20)
		{
			memcpy(&(ud->Code[l]),s,len);
			ud->Code[l+len]='\0';
		}
		else
		{
			DLOGI("ERROR %s %d Larg Activity Code",__FUNCTION__,__LINE__);
		}
	}
	//DP("XML_CharActivities: %s", ud->Elm);
}

#ifdef USE_PIN_SUPPORT

// conditional covers: xml_parsepin, xml_startpin, xml_endpin, xml_charpin

// xml parsing of Pin Table data


//
//Get the finger print template data from the xml
//
void XML_ParsePIN(FILE * fIn)
{

	char * cbuff = new char[1024];
	XML_Parser p = XML_ParserCreate(NULL);
	XML_UDPIN xmlUD;
	memset(&xmlUD,0,sizeof(XML_UDPIN));
	XML_SetUserData(p, &xmlUD);
	XML_SetElementHandler(p,XML_StartPIN,XML_EndPIN);
	XML_SetCharacterDataHandler(p,XML_CharPIN);


	//int cbufflimit = 60;
	while(fgets(cbuff,1024,fIn) != NULL)
	{
		XML_Parse(p,cbuff,strlen(cbuff),false);

//		cbufflimit--;
//		if ( cbufflimit == 0 )
//		{
//			break;
//		}

	}
	cbuff[0] = '\0';
	XML_Parse(p,cbuff,0,true);
	delete[] cbuff;
	XML_ParserFree(p);
}

//
//callback when element is started
//
static void XMLCALL XML_StartPIN(void *data, const char *el, const char **attr)
{
	// only clear out the user-defined struct if the start-tag is EmployeeInfo
	// for each member of the user-defined struct, if the start-tag matches that member, do not clear out the other members.
	//

	XML_UDPIN * ud = (XML_UDPIN *)data;
	if(strcasecmp(el,"BadgeId")==0)
	{
		strcpy(ud->Elm,el);
		ud->BadgeId[0] = '\0';
		//ud->VerifySource[0] = '\0';
		//ud->PinCode[0] = '\0';
   }
   else if(strcasecmp(el,"VerifySource")==0)
   {
  		strcpy(ud->Elm,el);
		//ud->BadgeId[0] = '\0';
		ud->VerifySource[0] = '\0';
		//ud->PinCode[0] = '\0';
   }
	else if(strcasecmp(el,"PinCode")==0)
   {
		strcpy(ud->Elm,el);
		//ud->Badge[0] = '\0';
		//ud->VerifySource[0] = '\0';
		ud->PinCode[0] = '\0';
   }
	else if(strcasecmp(el,"EmployeeInfo")==0)
   {
		strcpy(ud->Elm,el);
		ud->BadgeId[0] = '\0';
		ud->VerifySource[0] = '\0';
		ud->PinCode[0] = '\0';
   }
	else{//ignore
		//ud->BadgeId[0] = '\0';
		//ud->VerifySource[0] = '\0';
		//ud->PinCode[0] = '\0';
		ud->Elm[0] = '\0';
	}

	DP("XML_StartPIN: %s\n", el);
}
//
//Callback when element ends
//
static void XMLCALL XML_EndPIN(void *data, const char *el)
{

	XML_UDPIN * ud = (XML_UDPIN *)data;


	// only process the user-defined data when the end-tag for EmployeeInfo is reached.

	if(strcasecmp(el,"EmployeeInfo")==0)
	{
		unsigned long int UserIDNum = strtoul(ud->BadgeId, NULL, 0);
		char vsource[2];
		vsource[0] = ud->VerifySource[0];
		vsource[1] = '\0';
		char pincode[5];
		snprintf(pincode,5,"%s",ud->PinCode);// first 4 digts

        UF_RET_CODE Result;

		// TODO: update database with BadgeId,VerifySource,PinCode
		DP("XML_EndPin: call UpdatePinTable\n");
		UpdatePinTableInDBFromFile(ud->BadgeId, vsource, pincode);
		DP("XML_EndPin: returned from UpdatePinTable\n");

	}



	DP("XML_EndPin: Endtag: %s\n", el);

	if ( strcasecmp(el, "BadgeId") == 0)
	{
		DP("XML_EndPin: BadgeId is %s\n", ud->BadgeId);
	}
	else if ( strcasecmp(el, "VerifySource") == 0)
	{
		DP("XML_EndPin: VerifySource is %s\n", ud->VerifySource);
	}
	else if ( strcasecmp(el, "PinCode") == 0)
	{
		DP("XML_EndPin: PinCode is %s\n", ud->PinCode);
	}

}
//
//callback for character data
//
static void XMLCALL XML_CharPIN(void *data, const char *s, int len)
{
	XML_UDPIN * ud = (XML_UDPIN *)data;
	int l = 0;
	if(strcasecmp(ud->Elm,"BadgeId")==0)
	{
		l = strlen(ud->BadgeId);
		memcpy(&(ud->BadgeId[l]),s,len);
		ud->BadgeId[l+len]='\0';
	}else if(strcasecmp(ud->Elm,"VerifySource")==0)
	{
		l = strlen(ud->VerifySource);
		memcpy(&(ud->VerifySource[l]),s,len);
		ud->VerifySource[l+len]='\0';
	}else if(strcasecmp(ud->Elm,"PinCode")==0)
	{
		l = strlen(ud->PinCode);
		memcpy(&(ud->PinCode[l]),s,len);
		ud->PinCode[l+len]='\0';
	}

}

#endif // use_pin_support


//// eof ////
